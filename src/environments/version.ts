// IMPORTANT: THIS FILE IS AUTO GENERATED! DO NOT MANUALLY EDIT OR CHECKIN!
/* tslint:disable */
export const VERSION = {
    "dirty": false,
    "raw": "v0.0.1626-3-gacaf91da8",
    "hash": "gacaf91da8",
    "distance": 3,
    "tag": "v0.0.1626",
    "semver": {
        "options": {
            "loose": false,
            "includePrerelease": false
        },
        "loose": false,
        "raw": "v0.0.1626",
        "major": 0,
        "minor": 0,
        "patch": 1626,
        "prerelease": [],
        "build": [],
        "version": "0.0.1626"
    },
    "suffix": "3-gacaf91da8",
    "semverString": "0.0.1626+3.gacaf91da8",
    "version": "0.0.1627"
};
/* tslint:enable */
