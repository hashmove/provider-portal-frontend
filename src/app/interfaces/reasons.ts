export interface Reasons {
    bookingID: number,
    bookingStatus: string,
    bookingStatusRemarks: string,
    createdBy: any,
    modifiedBy: any,
    approverID: number,
    approverType: string,
    reasonID: number,
    providerName?: any;
    emailTo?: any;
    userName?: any;
    hashMoveBookingNum?: any;
    reasonText?: any;
    phoneTo?: any;
    userCountryPhoneCode?: number;
    bookingType?: string;
    currentBookingStatus?: string;
    providerID?: number;
}
