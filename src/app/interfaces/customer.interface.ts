export interface ICustomers {
    mapID?: any;
    companyID: number;
    companyName: string;
    companyLogo?: any;
    companyStatus: string;
    statusUpdatedByID?: any;
    statusUpdatedBy: string;
    statusUpdatedDateTime: string;
    userStatus: string;
    createdDate: string;
    cityID: number;
    cityName: string;
    countryID: number;
    countryCode: string;
    countryName: string;
    contactPerson: string;
    primaryPhone: string;
    primaryEmail: string;
    customerCount?: any;
    searchField?: any
    bookingCount: number;
    userID?: number
  }
  