

export interface LogisticService {
    ProvLogServID: number;
    LogServID: number;
    LogServCode: string;
    LogServName: string;
    ImageName: string;
    LogServImages: string;
    IsLogServImageUploaded: boolean;
    LogServSelectedImageID?: number;
    LogServSelectedImage: string;
    IsSelected: boolean;
    ServiceType: string;
    IsRemovable: boolean;
    IsRealEstate?: boolean;
    IsServiceOnProfile: boolean;
}

export interface ValueAddedService {
    ProvLogServID: number;
    LogServID: number;
    LogServCode?: any;
    LogServName: string;
    ImageName: string;
    LogServImages?: any;
    IsLogServImageUploaded: boolean;
    LogServSelectedImageID?: any;
    LogServSelectedImage?: any;
    IsSelected: boolean;
    ServiceType?: any;
    IsRemovable: boolean;
    IsRealEstate?: any;
    IsServiceOnProfile: boolean;
}

export interface Association {
    ProviderAssnID: number;
    AssnWithID: number;
    AssnWithName: string;
    AssnWithImage: string;
    IsSelected: boolean;
}

export interface SocialMedia {
    SocialMediaPortalsID: number;
    SocialMediaCode: string;
    Title: string;
    LinkURL: string;
}

export interface Gallery {
    DocumentTypeID: number;
    DocumentTypeCode: string;
    DocumentTypeName: string;
    DocumentTypeNameOL?: any;
    DocumentTypeDesc: string;
    SortingOrder: number;
    DocumentNature: string;
    DocumentSubProcess?: any;
    DocumentID?: any;
    UserID?: any;
    BookingID?: any;
    CompanyID?: any;
    ProviderID?: any;
    WHID?: any;
    VesselID?: any;
    DocumentName: string;
    DocumentDesc: string;
    DocumentFileName?: any;
    DocumentFile?: any;
    DocumentFileContent: string;
    DocumentUploadedFileType?: any;
    DocumentLastStatus?: any;
    ExpiryStatusCode: string;
    ExpiryStatusMessage: string;
    DocumentUploadDate?: any;
    IsDownloadable: boolean;
    IsApprovalRequired: boolean;
    BusinessLogic: string;
    CopyOfDocTypeID?: any;
    ReasonID?: any;
    ReasonCode?: any;
    ReasonName?: any;
    DocumentStausRemarks?: any;
    IsUploadable?: any;
    MetaInfoKeysDetail?: any;
    FileContent?: any;
    StatusAction?: any;
    ProviderName?: any;
    EmailTo?: any;
    PhoneTo?: any;
    UserName?: any;
    UserCompanyID?: any;
    UserCompanyName?: any;
    UserCountryPhoneCode: number;
    HashMoveBookingNum?: any;
    ContainerLoadType?: any;
    LoginUserID?: any;
    IsProvider: boolean;
    JsonUploadSource?: any;
    AllowedFileTypes: string;
    OtherID?: any;
    OtherType?: any;
}

export interface AwardCertificate {
    DocumentTypeID: number;
    DocumentTypeCode: string;
    DocumentTypeName: string;
    DocumentTypeNameOL?: any;
    DocumentTypeDesc: string;
    SortingOrder: number;
    DocumentNature: string;
    DocumentSubProcess?: any;
    DocumentID?: any;
    UserID?: any;
    BookingID?: any;
    CompanyID?: any;
    ProviderID?: any;
    WHID?: any;
    VesselID?: any;
    DocumentName: string;
    DocumentDesc: string;
    DocumentFileName?: any;
    DocumentFile?: any;
    DocumentFileContent: string;
    DocumentUploadedFileType?: any;
    DocumentLastStatus?: any;
    ExpiryStatusCode: string;
    ExpiryStatusMessage: string;
    DocumentUploadDate?: any;
    IsDownloadable: boolean;
    IsApprovalRequired: boolean;
    BusinessLogic: string;
    CopyOfDocTypeID?: any;
    ReasonID?: any;
    ReasonCode?: any;
    ReasonName?: any;
    DocumentStausRemarks?: any;
    IsUploadable?: any;
    MetaInfoKeysDetail?: any;
    FileContent?: any;
    StatusAction?: any;
    ProviderName?: any;
    EmailTo?: any;
    PhoneTo?: any;
    UserName?: any;
    UserCompanyID?: any;
    UserCompanyName?: any;
    UserCountryPhoneCode: number;
    HashMoveBookingNum?: any;
    ContainerLoadType?: any;
    LoginUserID?: any;
    IsProvider: boolean;
    JsonUploadSource?: any;
    AllowedFileTypes: string;
    OtherID?: any;
    OtherType?: any;
}

export interface MarketingGallery {
    DocumentTypeID: number;
    DocumentTypeCode: string;
    DocumentTypeName: string;
    DocumentTypeNameOL?: any;
    DocumentTypeDesc: string;
    SortingOrder: number;
    DocumentNature: string;
    DocumentSubProcess?: any;
    DocumentID?: any;
    UserID?: any;
    BookingID?: any;
    CompanyID?: any;
    ProviderID?: any;
    WHID?: any;
    VesselID?: any;
    DocumentName: string;
    DocumentDesc: string;
    DocumentFileName?: any;
    DocumentFile?: any;
    DocumentFileContent: string;
    DocumentUploadedFileType?: any;
    DocumentLastStatus?: any;
    ExpiryStatusCode: string;
    ExpiryStatusMessage: string;
    DocumentUploadDate?: any;
    IsDownloadable: boolean;
    IsApprovalRequired: boolean;
    BusinessLogic: string;
    CopyOfDocTypeID?: any;
    ReasonID?: any;
    ReasonCode?: any;
    ReasonName?: any;
    DocumentStausRemarks?: any;
    IsUploadable?: any;
    MetaInfoKeysDetail?: any;
    FileContent?: any;
    StatusAction?: any;
    ProviderName?: any;
    EmailTo?: any;
    PhoneTo?: any;
    UserName?: any;
    UserCompanyID?: any;
    UserCompanyName?: any;
    UserCountryPhoneCode: number;
    HashMoveBookingNum?: any;
    ContainerLoadType?: any;
    LoginUserID?: any;
    IsProvider: boolean;
    JsonUploadSource?: any;
    AllowedFileTypes: string;
    OtherID?: any;
    OtherType?: any;
}

export interface CompanyLogo {
    DocumentTypeID: number;
    DocumentTypeCode: string;
    DocumentTypeName: string;
    DocumentTypeNameOL?: any;
    DocumentTypeDesc: string;
    SortingOrder: number;
    DocumentNature: string;
    DocumentSubProcess: string;
    DocumentID?: any;
    UserID?: any;
    BookingID?: any;
    CompanyID?: any;
    ProviderID?: any;
    WHID?: any;
    VesselID?: any;
    DocumentName: string;
    DocumentDesc: string;
    DocumentFileName?: any;
    DocumentFile?: any;
    DocumentFileContent: string;
    DocumentUploadedFileType?: any;
    DocumentLastStatus?: any;
    ExpiryStatusCode: string;
    ExpiryStatusMessage: string;
    DocumentUploadDate?: any;
    IsDownloadable: boolean;
    IsApprovalRequired: boolean;
    BusinessLogic: string;
    CopyOfDocTypeID?: any;
    ReasonID?: any;
    ReasonCode?: any;
    ReasonName?: any;
    DocumentStausRemarks?: any;
    IsUploadable?: any;
    MetaInfoKeysDetail?: any;
    FileContent?: any;
    StatusAction?: any;
    ProviderName?: any;
    EmailTo?: any;
    PhoneTo?: any;
    UserName?: any;
    UserCompanyID?: any;
    UserCompanyName?: any;
    UserCountryPhoneCode: number;
    HashMoveBookingNum?: any;
    ContainerLoadType?: any;
    LoginUserID?: any;
    IsProvider: boolean;
    JsonUploadSource?: any;
    AllowedFileTypes: string;
    OtherID?: any;
    OtherType?: any;
}

export interface ProfileLogo {
    DocumentTypeID: number;
    DocumentTypeCode: string;
    DocumentTypeName: string;
    DocumentTypeNameOL?: any;
    DocumentTypeDesc: string;
    SortingOrder: number;
    DocumentNature: string;
    DocumentSubProcess: string;
    DocumentID?: any;
    UserID?: any;
    BookingID?: any;
    CompanyID?: any;
    ProviderID?: any;
    WHID?: any;
    VesselID?: any;
    DocumentName: string;
    DocumentDesc: string;
    DocumentFileName?: any;
    DocumentFile?: any;
    DocumentFileContent: string;
    DocumentUploadedFileType?: any;
    DocumentLastStatus?: any;
    ExpiryStatusCode: string;
    ExpiryStatusMessage: string;
    DocumentUploadDate?: any;
    IsDownloadable: boolean;
    IsApprovalRequired: boolean;
    BusinessLogic: string;
    CopyOfDocTypeID?: any;
    ReasonID?: any;
    ReasonCode?: any;
    ReasonName?: any;
    DocumentStausRemarks?: any;
    IsUploadable?: any;
    MetaInfoKeysDetail?: any;
    FileContent?: any;
    StatusAction?: any;
    ProviderName?: any;
    EmailTo?: any;
    PhoneTo?: any;
    UserName?: any;
    UserCompanyID?: any;
    UserCompanyName?: any;
    UserCountryPhoneCode: number;
    HashMoveBookingNum?: any;
    ContainerLoadType?: any;
    LoginUserID?: any;
    IsProvider: boolean;
    JsonUploadSource?: any;
    AllowedFileTypes: string;
    OtherID?: any;
    OtherType?: any;
}

export interface MetaInfoKeysDetail {
    DateModel?: any;
    DocumentMetaInfoKeyID: number;
    KeyName: string;
    KeyNameDesc: string;
    KeyValue?: any;
    IsMandatory: boolean;
    DataType: string;
    SortingOrder: number;
    FieldLength?: number;
    DocumentTypeID: number;
    DocumentID?: any;
}

export interface TradeLicense {
    DocumentTypeID: number;
    DocumentTypeCode: string;
    DocumentTypeName: string;
    DocumentTypeNameOL?: any;
    DocumentTypeDesc: string;
    SortingOrder: number;
    DocumentNature: string;
    DocumentSubProcess: string;
    DocumentID?: any;
    UserID?: any;
    BookingID?: any;
    CompanyID?: any;
    ProviderID?: any;
    WHID?: any;
    VesselID?: any;
    DocumentName: string;
    DocumentDesc: string;
    DocumentFileName?: any;
    DocumentFile?: any;
    DocumentFileContent: string;
    DocumentUploadedFileType?: any;
    DocumentLastStatus?: any;
    ExpiryStatusCode: string;
    ExpiryStatusMessage: string;
    DocumentUploadDate?: any;
    IsDownloadable: boolean;
    IsApprovalRequired: boolean;
    BusinessLogic: string;
    CopyOfDocTypeID?: any;
    ReasonID?: any;
    ReasonCode?: any;
    ReasonName?: any;
    DocumentStausRemarks?: any;
    IsUploadable?: any;
    MetaInfoKeysDetail: MetaInfoKeysDetail[];
    FileContent?: any;
    StatusAction?: any;
    ProviderName?: any;
    EmailTo?: any;
    PhoneTo?: any;
    UserName?: any;
    UserCompanyID?: any;
    UserCompanyName?: any;
    UserCountryPhoneCode: number;
    HashMoveBookingNum?: any;
    ContainerLoadType?: any;
    LoginUserID?: any;
    IsProvider: boolean;
    JsonUploadSource?: any;
    AllowedFileTypes: string;
    OtherID?: any;
    OtherType?: any;
}

export interface UploadedGallery {
    DocumentID?: any;
    DocumentFileID: number;
    DocumentFileName: string;
    DocumentFile?: any;
    DocumentUploadedFileType?: any;
    ProviderLogo?: any;
    DocumentTypeID?: any;
    BookingID?: any;
}

export interface UploadedAwardCertificate {
    DocumentID?: any;
    DocumentFileID: number;
    DocumentFileName: string;
    DocumentFile?: any;
    DocumentUploadedFileType?: any;
    ProviderLogo?: any;
    DocumentTypeID?: any;
    BookingID?: any;
}

export interface UploadedMarketingGallery {
    DocumentID?: any;
    DocumentFileID: number;
    DocumentFileName: string;
    DocumentFile?: any;
    DocumentUploadedFileType?: any;
    ProviderLogo?: any;
    DocumentTypeID?: any;
    BookingID?: any;
}

export interface UploadedCompanyLogo {
    DocumentID?: any;
    DocumentFileID: number;
    DocumentFileName: string;
    DocumentFile?: any;
    DocumentUploadedFileType?: any;
    ProviderLogo?: any;
    DocumentTypeID?: any;
    BookingID?: any;
}

export interface UploadedProfileLogo {
    DocumentID?: any;
    DocumentFileID: number;
    DocumentFileName: string;
    DocumentFile?: any;
    DocumentUploadedFileType?: any;
    ProviderLogo?: any;
    DocumentTypeID?: any;
    BookingID?: any;
}

export interface ProfileBanner {
    DocumentTypeID: number;
    DocumentTypeCode: string;
    DocumentTypeName: string;
    DocumentTypeNameOL?: any;
    DocumentTypeDesc: string;
    SortingOrder: number;
    DocumentNature: string;
    DocumentSubProcess?: any;
    DocumentID?: any;
    UserID?: any;
    BookingID?: any;
    CompanyID?: any;
    ProviderID?: any;
    WHID?: any;
    VesselID?: any;
    DocumentName: string;
    DocumentDesc: string;
    DocumentFileName?: any;
    DocumentFile?: any;
    DocumentFileContent: string;
    DocumentUploadedFileType?: any;
    DocumentLastStatus?: any;
    ExpiryStatusCode: string;
    ExpiryStatusMessage: string;
    DocumentUploadDate?: any;
    IsDownloadable: boolean;
    IsApprovalRequired: boolean;
    BusinessLogic: string;
    CopyOfDocTypeID?: any;
    ReasonID?: any;
    ReasonCode?: any;
    ReasonName?: any;
    DocumentStausRemarks?: any;
    IsUploadable?: any;
    MetaInfoKeysDetail?: any;
    FileContent?: any;
    StatusAction?: any;
    ProviderName?: any;
    EmailTo?: any;
    PhoneTo?: any;
    UserName?: any;
    UserCompanyID?: any;
    UserCompanyName?: any;
    UserCountryPhoneCode: number;
    HashMoveBookingNum?: any;
    ContainerLoadType?: any;
    LoginUserID?: any;
    IsProvider: boolean;
    JsonUploadSource?: any;
    AllowedFileTypes: string;
    OtherID?: any;
    OtherType?: any;
}

export interface UploadedProfileBanner {
    DocumentID?: any;
    DocumentFileID: number;
    DocumentFileName: string;
    DocumentFile?: any;
    DocumentUploadedFileType?: any;
    ProviderLogo?: any;
    DocumentTypeID?: any;
    BookingID?: any;
}

export interface LogisticServiceDoc {
    DocumentTypeID: number;
    DocumentTypeCode: string;
    DocumentTypeName: string;
    DocumentTypeNameOL?: any;
    DocumentTypeDesc: string;
    SortingOrder: number;
    DocumentNature: string;
    DocumentSubProcess: string;
    DocumentID?: any;
    UserID?: any;
    BookingID?: any;
    CompanyID?: any;
    ProviderID?: any;
    WHID?: any;
    VesselID?: any;
    DocumentName: string;
    DocumentDesc: string;
    DocumentFileName?: any;
    DocumentFile?: any;
    DocumentFileContent: string;
    DocumentUploadedFileType?: any;
    DocumentLastStatus?: any;
    ExpiryStatusCode: string;
    ExpiryStatusMessage: string;
    DocumentUploadDate?: any;
    IsDownloadable: boolean;
    IsApprovalRequired: boolean;
    BusinessLogic: string;
    CopyOfDocTypeID?: any;
    ReasonID?: any;
    ReasonCode?: any;
    ReasonName?: any;
    DocumentStausRemarks?: any;
    IsUploadable?: any;
    MetaInfoKeysDetail?: any;
    FileContent?: any;
    StatusAction?: any;
    ProviderName?: any;
    EmailTo?: any;
    PhoneTo?: any;
    UserName?: any;
    UserCompanyID?: any;
    UserCompanyName?: any;
    UserCountryPhoneCode: number;
    HashMoveBookingNum?: any;
    ContainerLoadType?: any;
    LoginUserID?: any;
    IsProvider: boolean;
    JsonUploadSource?: any;
    AllowedFileTypes: string;
    OtherID?: any;
    OtherType?: any;
}

export interface UploadedLogisticServiceDoc {
    LogServID: number;
    DocumentID: number;
    DocumentFileID: number;
    DocumentFileName: string;
    DocumentFile?: any;
    DocumentUploadedFileType?: any;
    ProviderLogo?: any;
    DocumentTypeID: number;
    BookingID?: any;
}

export interface StatisticsSetting {
    ProviderStatisticID: number;
    StatisticName: string;
    StatisticImage: string;
    BusinessLogic: string;
    MinValue: string;
    MaxValue: string;
    StatisticValue: string;
}

export interface IndustrySetting {
    IndustryID: number;
    IndustryName: string;
    IndustryImage: string;
    IsSelected: boolean;
}

export interface BannerSettings {
    BannerSelectedImageID: number;
    BannerSelectedImage: string;
    BannerImages: string;
}

export interface RateExpirySetting {
    ModeOfTrans: string;
    IsEnable: boolean;
    ExtendedDays: number;
    IncreaseRatePercentage: number;
    BeforeExpiryDay: number;
}

export interface RateExpiryNotificationSettings {
    IsEnable: boolean;
    ExpireInDays: number;
}

export interface IUserSettings {
    FeatureToggleJson: any;
    LoginID: string;
    FirstName: string;
    LastName: string;
    Password: string;
    JobTitle: string;
    CityID: number;
    CountryID: number;
    CountryCode: string;
    PrimaryPhone: string;
    CountryPhoneCode: string;
    PhoneCodeCountryID: number;
    RegionID: number;
    CurrencyID: number;
    ProviderID: number;
    ProviderName: string;
    ProviderImage: string;
    ProviderAddress: string;
    POBox: string;
    ProviderCityID: number;
    ProviderPhone: string;
    ProviderWebAdd: string;
    ProviderCountryPhoneCode: string;
    ProviderPhoneCodeCountryID: number;
    ProfileID: string;
    About: string;
    IsPrivateMode: boolean;
    ProfileImage: string;
    AlternateEmails: string;
    BusinessEmail: string;
    IsMarketPlace: boolean;
    IsExclChargesAllow: boolean;
    IsCustomerApprovalRequired: boolean;
    IsTrackingQualityRequired: boolean;
    LogisticService: LogisticService[];
    ValueAddedServices: ValueAddedService[];
    Association: Association[];
    SocialMedia: SocialMedia[];
    Gallery: Gallery;
    AwardCertificate: AwardCertificate;
    MarketingGallery: MarketingGallery;
    CompanyLogo: CompanyLogo;
    ProfileLogo: ProfileLogo;
    TradeLicense: TradeLicense;
    UploadedGallery: UploadedGallery[];
    UploadedAwardCertificate: UploadedAwardCertificate[];
    UploadedMarketingGallery: UploadedMarketingGallery[];
    UploadedCompanyLogo: UploadedCompanyLogo[];
    UploadedProfileLogo: UploadedProfileLogo[];
    UploadedTradeLicense?: any;
    ProfileBanner: ProfileBanner;
    UploadedProfileBanner: UploadedProfileBanner[];
    LogisticServiceDoc: LogisticServiceDoc;
    UploadedLogisticServiceDoc: UploadedLogisticServiceDoc[];
    UserCity: string;
    ProviderCity: string;
    IsBankInfoRequired: boolean;
    MarketingTagLineHeading: string;
    MarketingTagLineContent: string;
    IsShowWarehouseOnProfile: boolean;
    IsWHBookEndAlert: boolean;
    WHBookEndAlertDays: number;
    IsShowSeaRateOnProfile: boolean;
    IsShowSeaLCLRateOnProfile: boolean;
    IsShowAirRateOnProfile: boolean;
    IsShowGroundRateOnProfile: boolean;
    IsShowStatisticsOnProfile: boolean;
    CompanyNamePlacementOnProfile: string;
    StatisticsSettings: StatisticsSetting[];
    IndustrySettings: IndustrySetting[];
    IsShowLocationOnProfile: boolean;
    IsShowSearchBoxOnProfile: boolean;
    DefaultTaglineBackGroundColor: string;
    TaglineBackGroundColor: string;
    DefaultStatisticBackGroundColor: string;
    StatisticBackGroundColor: string;
    IsMarketingTagLineRequired: boolean;
    IsShowOfficeOnProfile: boolean;
    IsShowServiceOnProfile: boolean;
    IsShowNewsOnProfile: boolean;
    DefaultIndustryIconBorderColor: string;
    IndustryIconBorderColor: string;
    DefaultOfficePinBorderColor: string;
    OfficePinBorderColor: string;
    DefaultFooterStripColor: string;
    FooterStripColor: string;
    IsShowBannerText: boolean;
    BannerText: string;
    DefaultBannerTextColor: string;
    BannerTextColor: string;
    BannerSettings: BannerSettings;
    RateExpirySettings: RateExpirySetting[];
    RateExpiryNotificationSettings: RateExpiryNotificationSettings;
}



export interface IAssociation {
    providerID: number;
    createdBy: number;
    modifiedBy: number;
    assnWithID: number;
    providerAssnID: number;
    isActive: boolean;
}
export interface IEmailNotif {
    moduleBL: string;
    isMode?: boolean;
    moduleTitle: string;

    moduleID?: number;
    hasErrors?: boolean;
    hasTouched?: boolean;
    mailList?: IMail[];
    isMail?: boolean;
    styleClass?: string;
    mailInput?: any;
}

export interface IMail {
    e_mail: string;
    isRemovable: boolean;
}

export interface IEmailNotifResp {
    ProviderID: number;
    ModuleBL: string;
    ModuleID: number;
    EmailAddresses: string[];
}

export interface IRateNotification {
    BeforeExpiryDay: number;
    ExtendedDays: number;
    IncreaseRatePercentage: number;
    IsEnable: boolean;
    ModeOfTrans: string;
}

export interface Document {
    BookingID: number
    DocumentFile: string
    DocumentFileID: number
    DocumentFileName: string
    DocumentID: number
    DocumentTypeID: number
    DocumentUploadedFileType: string
    ProviderLogo: string
}

export interface AssociationModel {
    AssnWithID?: number;
    AssnWithImage?: string;
    AssnWithName?: string;
    IsSelected?: boolean;
    ProviderAssnID?: number;
    AssnWithRefNo?: string;
    AssnWithWebAdd?: string
    AssnWithBL: string
    AssnWithCode: string

    IsValidRef: boolean;
    IsValidName: boolean;
    IsValidWeb: boolean;
    IsEdit: boolean;
    IsAdded: boolean;
    AssInpName?: string
}
export interface FreightModel {
    ImageName?: string;
    IsRealEstate?: boolean;
    IsRemovable?: boolean;
    IsSelected?: boolean;
    LogServCode?: string;
    LogServID?: number;
    LogServName?: string;
    ProvLogServID?: number;
    ServiceType?: string;
    LogServImages?: string;
    LogServImagesArr?: string[];
    LogServSelectedImage?: string;
    isUploadOpen?: boolean;
    IsServiceOnProfile?: boolean;
}


export interface ICityResp {
    cityID: number;
    cityCode: string;
    cityName: string;
    cityShortName: string;
    countryID: number;
    stateID: number;
    isDelete: boolean;
    isActive: boolean;
    createdBy: string;
    createdDateTime: string;
    modifiedBy: string;
    modifiedDateTime: string;
}
export interface IBankInfo {
    provBankID: number;
    providerID: number;
    bankID: number;
    bankName: string;
    bankDetail: string;
    accountTitle: string;
    accountNo?: any;
    swiftCode: string;
    branchName: string;
    branchAddress: string;
    branchCityID: number;
    branchCityDetail: any;
}


export interface IStatistic {
    BusinessLogic: string //"NumberofPorts"
    MaxValue: any //"500"
    MinValue: any //"1"
    ProviderStatisticID: number //101
    StatisticImage: string //""
    StatisticName: string //"Number of Ports"
    StatisticValue: string //"150"
}

export interface IIndustry {
    IndustryID: number;
    IndustryImage: string;
    IndustryName: string;
    IsSelected: boolean;
}

export interface IAddressInfo {
    AddressLine1: string;
    AddressLine2: string;
    CityID: number;
    CityName: string;
    CountryCode: string;
    CountryName: string;
    CountryPhoneCode: string;
    OfficeName: string;
    OfficePhone: string;
    PhoneCodeCountryID: number;
    ProviderOfficeID: number;
    PhoneCountryCode: string;
}

export interface INews {
    CreatedBy: string;
    CreatedDateTime: string;
    EndDateTime: string;
    IsActive: boolean;
    IsDelete: boolean;
    ModifiedBy: any;
    ModifiedDateTime: any;
    NewsID: number;
    NewsTitle: string;
    NewsUrl: string;
    OrderBy: number;
    ProviderID: number;
    StartDateTime: string;
}