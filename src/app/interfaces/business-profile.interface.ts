import { MetaInfoKeysDetail } from "../components/pages/user-desk/settings/settings.interface";

export interface ManagementInfo {
    jobTitleID: number;
    firstName: string;
    lastName: string;
}

export interface DirectorInfo {
    jobTitleID: number;
    firstName: string;
    lastName: string;
    email: string;
    mobileNo: string;
}

export interface BusinessProfileBL {
    licenseNo: string;
    issueDate: string;
    expiryDate: string;
    vatNo: string;
    organizationTypeID: number;
    organizationName: string;
    addressLine1: string;
    addressLine2: string;
    city: string;
    poBox: string;
    telephone: string;
    faxNo: string;
    managementInfo: ManagementInfo[];
    directorInfo: DirectorInfo[];
}

export interface ManagementInfo2 {
    jobTitleID: number;
    firstName: string;
    lastName: string;
}

export interface DirectorInfo2 {
    jobTitleID: number;
    firstName: string;
    lastName: string;
    email: string;
    mobileNo: string;
}

export interface BusinessProfileOL {
    licenseNo: string;
    issueDate: string;
    expiryDate: string;
    vatNo: string;
    organizationTypeID: number;
    organizationName: string;
    addressLine1: string;
    addressLine2: string;
    city: string;
    poBox: string;
    telephone: string;
    faxNo: string;
    managementInfo: ManagementInfo2[];
    directorInfo: DirectorInfo2[];
}

export interface SocialAccount {
    providerSocialMediaAccountsID: number;
    providerSocialMediaCode: string;
    shortName: string;
    providerID: number;
    socialMediaPortalsID: number;
    carrierID: number;
    companyID: number;
    userID: number;
    linkURL: string;
    isDelete: boolean;
    isActive: boolean;
    createdBy: string;
    createdDateTime: string;
    modifiedBy: string;
    modifiedDateTime: string;
}

export interface ProviderLogisticServiceList {
    serviceID: number;
    serviceNameBaseLang: string;
    serviceNameOtherLang: string;
}

export interface BusinessLocation {
    latitude: string;
    longitude: string;
    addressLine1: string;
    addressLine2: string;
    city: string;
}

export interface KeyValue {
}


export interface FileContent {
    documentID: number;
    documentFileID: number;
    documentFileName: string;
    documentFile: string;
    documentUploadedFileType: string;
}

export interface Doc {
    documentTypeID: number;
    documentTypeCode: string;
    documentTypeName: string;
    documentTypeNameOL: string;
    documentTypeDesc: string;
    sortingOrder: number;
    documentNature: string;
    documentSubProcess: string;
    documentID: number;
    userID: number;
    bookingID: number;
    companyID: number;
    providerID: number;
    documentName: string;
    documentDesc: string;
    documentFileName: string;
    documentFileContent: string;
    documentUploadedFileType: string;
    documentLastStatus: string;
    expiryStatusCode: string;
    expiryStatusMessage: string;
    documentUploadDate: string;
    isDownloadable: boolean;
    isUploadable: boolean;
    isApprovalRequired: boolean;
    businessLogic: string;
    copyOfDocTypeID: number;
    metaInfoKeysDetail: MetaInfoKeysDetail[];
    fileContent: FileContent[];
}
export interface Container {
    ContainerSpecID: number;
    ContainerSpecCode: string;
    ContainerSpecDesc: string;
    ContainerSpecImage: string;
    ContainerSizeID: number;
    ContainterLength: number;
    ContainterWidth: number;
    ContainterHeight: number;
    IsActive: boolean;
    SortingOrder: number;
    ContainerLoadType: string;
    ContainerSubDetail?: any;
    MaxGrossWeight: number;
    MinGWT: number;
    MaxGWT: number;
    DimensionUnit: string;
    WeightUnit: string;
    JsonContainerSpecProp: string;
}

export interface ShippingCriteriaSubCat {
    ShippingSubCatID: number;
    ShippingSubCatCode: string;
    ShippingSubCatName: string;
    ShippingSubCatImage: string;
    ShippingSubCatDesc: string;
    SortingOrder: number;
    Containers: Container[];
}

export interface ShippingCriteriaCat {
    ShippingCatID: number;
    ShippingCatCode: string;
    ShippingCatName: string;
    ShippingCatImage: string;
    ShippingCatDesc: string;
    SortingOrder: number;
    ShippingCriteriaSubCat: ShippingCriteriaSubCat[];
}

export interface ShippingCategories {
    CustomerID: number;
    CustomerType: string;
    BusinessLogic: string;
    ShippingModeID: number;
    ShippingModeCode: string;
    ShippingModeName: string;
    ShippingModeImage: string;
    ShippingModeDesc: string;
    SortingOrder: number;
    ShippingCriteriaCat: ShippingCriteriaCat[];
}






export interface CargoTypes {
    id: number;
    code: string;
    title: string;
    shortName?: any;
    imageName: string;
    desc: string;
    webURL?: any;
    sortingOrder: number;
    type?: any;
    lastUpdate?: any;
    latitude?: any;
    longitude?: any;
    isBaseCurrency?: any;
    jsonServices?: any;
    isChinaGateway: boolean;
    shortPortCode?: any;
    portCountryCode?: any;
    roundingOff?: any;
}