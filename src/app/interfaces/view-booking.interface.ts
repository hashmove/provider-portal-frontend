import { BookingDocumentDetail } from "./booking.interface";

export interface FooterDetails {
  HMSupportID: number;
  HMPrivacyURL?: string;
  HMTermsURL?: string;
}
export interface RouteDetail {
  PolPodCode: string;
  EtdUtc?: any;
  EtdLcl?: any;
  EtaUtc?: any;
  EtaLcl?: any;
  FlightNo?: any;
  AirCraftInfo?: any;
  VesselCode?: any;
  VesselName?: any;
  VoyageRefNum?: any;
  TruckType?: any;
  TruckCode?: any;
  TruckName?: any;
  TruckNo: string;
  PolID: number;
  PolCode: string;
  PolName: string;
  PolType: string;
  PodID: number;
  PodCode: string;
  PodName: string;
  PodType: string;
}

export interface PriceDetail {
  SurchargeType: string;
  SurchargeID: number;
  SurchargeCode: string;
  SurchargeName: string;
  ContainerSpecID?: number;
  CurrencyID: number;
  CurrencyCode: string;
  BaseCurrencyID: number;
  BaseCurrencyCode: string;
  TotalAmount: number;
  BaseCurrTotalAmount: number;
  IndividualPrice?: number;
  SortingOrder: number;
  TransMode: string;
  BaseCurrIndividualPrice: number;
  ExchangeRate?: number;
  ActualIndividualPrice?: number;
  ActualTotalAmount?: number;
  LogServShortName?: any;
  IsChecked?: boolean;
  ShowInsurance?: boolean;
}

export interface EnquiryDetail {
  BookingEnquiryID: number;
  BookingEnquiryType: string;
  BookingEnquiryDate: string;
  BookingID: number;
  BookingEnquiryAckDate?: string;
  BookingEnquiryPrice?: number;
  CurrencyID?: number;
  CurrencyCode?: string;
  BookingEnquiryAckRemarks?: string;
  ProviderID: number;
  ProviderCode: string;
  ProviderName: string;
  ProviderShortName: string;
  ProviderImage: string;
  ProviderEmail: string;
}
export interface docDetail {
  DocumentTypeID: number,
  DocumentTypeCode: string,
  DocumentTypeName: string,
  DocumentTypeNameOL: string,
  DocumentTypeDesc: string,
  SortingOrder: number,
  DocumentNature: string,
  DocumentSubProcess: string,
  DocumentID: number,
  UserID: number,
  BookingID: number,
  CompanyID: number,
  ProviderID: number,
  DocumentName: string,
  DocumentDesc: string,
  DocumentFileName: string,
  DocumentFileContent: any,
  DocumentUploadedFileType: string,
  DocumentLastStatus: string,
  ExpiryStatusCode: string,
  ExpiryStatusMessage: string,
  DocumentUploadDate: null,
  IsDownloadable: boolean,
  IsUploadable: boolean,
  IsApprovalRequired: false,
  BusinessLogic: string,
  CopyOfDocTypeID: any,
  MetaInfoKeysDetail: any,
  FileContent: any
}
export interface BookingDetails {
  ContainerSpecShortName?: string;
  JsonSearchCriteria?: any;
  BookingJsonDetail?: any;
  WHAddress?: string;
  WHMedia?: any;
  WHLatitude?: string;
  WHLongitude?: string;
  WHCountryCode?: string;
  WHGLocName?: string;
  WHCityName?: string;
  WHCountryName?: string;
  ActualScheduleDetail?: any;
  BookingDesc?: string;
  BookingID: number;
  HashMoveBookingNum: string;
  HashMoveBookingDate: string;
  ShippingCatID: number;
  ShippingSubCatID: number;
  ShippingModeID: number;
  ShippingModeName: string;
  ShippingModeCode: string;
  ShippingCatName: string;
  ShippingSubCatName: string;
  origin?: string;
  destination?: string;
  PolID: number;
  PolCode: string;
  PolName: string;
  PolCountry: string;
  PodID: number;
  PodCode: string;
  PodName: string;
  PodCountry: string;
  PolModeOfTrans: string;
  PodModeOfTrans: string;
  ContainerLoad: string;
  ContainerCount: number;
  EtdUtc: string;
  EtdLcl: string;
  EtaUtc: string;
  EtaLcl: string;
  EtaInDays?: number;
  TransitTime: number;
  PortCutOffUtc: string;
  PortCutOffLcl: string;
  FreeTimeAtPort: number;
  ProviderID: number;
  ProviderName: string;
  ProviderImage: any;
  ProviderEmail: string;
  ProviderPhone: string;
  PolLatitude: string;
  PolLongitude: string;
  PodLatitude: string;
  PodLongitude: string;
  CarrierID: number;
  CarrierName: string;
  CarrierImage: string;
  VesselCode: string;
  VesselName: string;
  VoyageRefNum: string;
  IsInsured: boolean;
  ProviderInsurancePercent: number;
  InsuredGoodsPrice: number;
  InsuredGoodsCurrencyID: number;
  InsuredGoodsCurrencyCode: string;
  IsInsuredGoodsBrandNew: boolean;
  InsuredGoodsProviderID: number;
  InsuredStatus: string;
  IsInsuranceProvider: boolean;
  IsAnyRestriction: boolean;
  CurrencyID: number;
  CurrencyCode: string;
  BookingTotalAmount: number;
  BookingContainerDetail: any[];
  BookingRouteDetail: RouteDetail[];
  BookingPriceDetail: PriceDetail[];
  BookingEnquiryDetail: EnquiryDetail[];
  BookingDocumentDetail: docDetail[];
  BookingStatus: string;
  ShippingStatus: string;
  DiscountPrice?: number;
  DiscountPercent?: number;
  IDlist?: string;
  InsuredGoodsBaseCurrPrice: number;
  InsuredGoodsBaseCurrencyID: number;
  InsuredGoodsBaseCurrencyCode: string;
  InsuredGoodsActualPrice: number;
  InsuredGoodsExchangeRate: number;
  BaseCurrencyID: number;
  BaseCurrencyCode: string;
  BaseCurrTotalAmount: number;
  ExchangeRate: number;
  ProviderDisplayImage?: string;
  CarrierDisplayImage?: string;
  UserName: string;
  UserCountryName: string;
  UserCityName: string;
  BookingUserInfo: any;
  JsonShippingOrgInfo: any;
  JsonShippingDestInfo: any;
  JsonAgentOrgInfo: any;
  JsonAgentDestInfo: any;
  UserCountryPhoneCodeID?: number;
  ProviderCountryPhoneID?: any;
  LastActivity?: string
  StatusID?: any
  BLNo?: any
  MinTransitDays?: any
  MaxTransitDays?: any
  PolType: any
  PolInputAddress: any
  BookingSourceVia: any
  CarrierCode?: string
  AircraftTypeCode?: string
  IsVirtualAirLine?: boolean
  IsSpecialRequest?: boolean
  IsBookedByProvider?: boolean
  IsLumpSum?: boolean
  Commodity: string;
  CommodityVariant: string;
  HSCode: string;
  ContainerWeight: number,
  LinkID: number,
  CustomerOrderNo: string;
  ComInvNo: string;
  CusClearNo: string;
  GDeclareNo: string;
  ExpFormNo: string;
  PartnerName
  PartnerAddress
  JsonPortVia
  PriceBasisType
  BookingNote
  ProductName
  PortCutOffDays
  JsonDepartureDays
  ContainerTrackingURL: string
  BLTrackingURL: string
  TotalDocCharges: number
  CurCodeDocCharges: string
}

export interface BookingContainerTypeDetail {
  BookingContTypeQty: number;
  ContainerSpecID: number;
  ContainerSpecCode?: any;
  ContainerSpecDesc?: any;
}

export interface BookingSurChargeDetail {
  SurchargeType: string;
  SurchargeID: number;
  SurchargeCode: string;
  SurchargeName: string;
  ContainerSpecID: number;
  CurrencyID: number;
  CurrencyCode: string;
  BaseCurrencyID: number;
  BaseCurrencyCode: string;
  TotalAmount: number;
  BaseCurrencyPrice: number;
  IndividualPrice: number;
  SortingOrder: number;
  SortOrder: number;
  TransMode: string;
  BaseCurrTotalAmount: number;
  BaseCurrIndividualPrice: number;
  ActualTotalAmount: number;
  ActualIndividualPrice: number;
  ExchangeRate: number;
  SurchargeBasis?: string;
  BaseTotalAmount?: number
  Price?: number
  ActualPrice?: number
}

export interface SaveBookingObject {
  BookingID: number;
  UserID: number;
  BookingSource: string;
  BookingStatus: string;
  MovementType: string;
  ShippingSubCatID: number;
  ShippingModeID: number;
  CarrierID: number;
  CarrierImage: string;
  CarrierName: string;
  ProviderID: number;
  ProviderImage: string;
  ProviderName: string;
  PolID: number;
  PodID: number;
  PolName: string;
  PodName: string;
  EtdUtc: string;
  EtdLcl: string;
  EtaUtc: string;
  EtaLcl: string;
  PortCutOffUtc: string;
  PortCutOffLcl: string;
  TransitTime: number;
  ContainerLoad: string;
  FreeTimeAtPort: number;
  IsInsured: boolean;
  IsInsuranceProvider: boolean;
  InsuredGoodsPrice?: any;
  InsuredGoodsCurrencyID: number;
  InsuredGoodsCurrencyCode: string;
  IsInsuredGoodsBrandNew: boolean;
  InsuredGoodsProviderID: number;
  InsuredStatus: string;
  IsAnyRestriction: boolean;
  PolModeOfTrans: string;
  PodModeOfTrans: string;
  VesselCode: string;
  VesselName: string;
  VoyageRefNum: string;
  CreatedBy: string;
  ModifiedBy: string;
  IDlist: string;
  InsuredGoodsBaseCurrPrice: number;
  InsuredGoodsBaseCurrencyID: number;
  InsuredGoodsBaseCurrencyCode: string;
  InsuredGoodsActualPrice: number;
  InsuredGoodsExchangeRate: number;
  BookingPriceDetail: BookingPriceDetail[];
  BookingContainerTypeDetail: BookingContainerTypeDetail[];
  BookingSurChargeDetail: BookingSurChargeDetail[];
  BookingEnquiryDetail: any[];
}

export interface InsuranceProvider {
  ProviderID: number;
  ProviderCode: string;
  ProviderName: string;
  ProviderShortName: string;
  ProviderImage: string;
  ProviderEmail: string;
  isChecked: boolean;
}


export interface AdditionalOptions {
  VASID: number;
  VASCode: string;
  VASName: string;
  VASDesc?: any;
  ModeOfTrans: string;
  VASBasis: string;
  LogServID: number;
  LogServName: string;
  LogServShortName: string;
  SurchargeType: string;
  SurchargeID: number;
  SurchargeName: string;
  SurchargeCode: string;
  ProviderID: number;
  PortID: number;
  ImpExpFlag: string;
  CurrencyID: number;
  VASChargeType: string;
  VASCharges: number;
  ProviderCode: string;
  ProviderName: string;
  ProviderImage: string;
  PortCode: string;
  PortName: string;
  CountryID: number;
  CountryCode: string;
  CountryName: string;
  CurrencyCode: string;
  CurrencyName: string;
  BaseCurrencyID: number;
  BaseCurrencyCode: string;
  ExchangeRate: number;
  BaseCurrVASCharges: number;
  IsChecked: boolean;
  TotalAmount: number;
}


export interface IDispatchItem {
  dispatchID: number;
  dispatchDate: string;
  vehicleNo: string;
  driverName: string;
  driverMobile: string;
  countryPhoneCode: string;
  countryCode: string;
  dispatchQuantity: number;
  dispatchQuantityUOM: number;
  goodsWeight: number;
  goodsWeightUOM: string;
}

export interface IDispatchSummary {
  bookingID: number;
  bookingNumber: string;
  wHName: string;
  dispatchItems: IDispatchItem[];
}


export interface IReceiptItem {
  goodsWeightUOMID: number;
  emptyWeightUOMID: number;
  loadedWeightUOMID: number;
  eachQtyWeight: any;
  eachQtyWeightUOMID: any;
  totalQtyWeight: any;
  totalQtyWeightUOMID: any;
  receiptID: number;
  receiptDate: string;
  vehicleNo: string;
  isChecked?: boolean;
  driverName: string;
  driverMobile: string;
  countryPhoneCode: string;
  countryCode: string;
  receiptQty: number;
  receiptQtyUOM: string;
  loadedWeight: number;
  loadedWeightUOM: string;
  emptyWeight: number;
  emptyWeightUOM: string;
  goodsWeight: number;
  goodsWeightUOM: string;
}

export interface IReceiptSummary {
  bookingID: number;
  bookingNumber: string;
  wHName: string;
  receiptItems: IReceiptItem[];
}

export interface BookingRouteDetail {
  PolPodCode: string;
  EtdUtc?: any;
  EtdLcl?: any;
  EtaUtc?: any;
  EtaLcl?: any;
  FlightNo?: any;
  AirCraftInfo?: any;
  VesselCode?: any;
  VesselName?: any;
  VoyageRefNum?: any;
  TruckType?: any;
  TruckCode?: any;
  TruckName?: any;
  TruckNo: string;
  PolID: number;
  PolCode: string;
  PolName: string;
  PolType: string;
  PodID: number;
  PodCode: string;
  PodName: string;
  PodType: string;
}
export interface BookingRouteMapInfo {
  Route: string;
  TransitTime: number;
  CarrierName: string;
  CarrierImage: string;
  FreeTimeAtPort: number;
  RouteInfo: RouteInfo[];
  JsonTransferVesselsDetails?: any;
}

export interface BookingUserInfo {
  FirstName: string;
  LastName: string;
  PrimaryEmail: string;
  PrimaryPhone: string;
  UserImage: string;
  UserType: string;
  CompanyName: string;
  CompanyPhone?: any;
  CompanyEmail: string;
  ContactNumber: string;
  CompanyID: number;
}

export interface ViewBookingDetails {
  PolImage?: string;
  PodImage?: string;
  ActualScheduleDetail?: any;
  BookingID: number;
  HashMoveBookingNum: string;
  HashMoveBookingDate: string;
  ShippingModeName: string;
  ShippingModeCode: string;
  ShippingCatName: string;
  ShippingSubCatName: string;
  PolID: number;
  PolType: string;
  PolModeOfTrans: string;
  PolCode: string;
  PolName: string;
  PolCity: string;
  PolCountry: string;
  PolInputAddress: string;
  PolAddress?: any;
  PodID: number;
  PodType: string;
  PodModeOfTrans: string;
  PodCode: string;
  PodName: string;
  PodCity?: any;
  PodCountry: string;
  PodInputAddress: string;
  PodAddress?: any;
  ContainerLoad: string;
  ContainerCount: number;
  EtdUtc?: any;
  EtdLcl?: any;
  EtaUtc?: any;
  EtaLcl?: any;
  TransitTime: number;
  PortCutOffUtc?: any;
  PortCutOffLcl?: any;
  FreeTimeAtPort: number;
  ProviderID: number;
  ProviderName: string;
  ProviderCountryPhoneID: number;
  ProviderImage: string;
  ProviderEmail: string;
  ProviderPhone: string;
  CarrierID: number;
  CarrierCode: string;
  CarrierName: string;
  CarrierImage: string;
  VesselCode: string;
  VesselName: string;
  VoyageRefNum: string;
  IsInsured: boolean;
  IsAnyRestriction: boolean;
  EtaInDays?: any;
  IDlist: string;
  CurrencyID: number;
  CurrencyCode: string;
  BookingTotalAmount: number;
  BaseCurrencyID: number;
  BaseCurrencyCode: string;
  BaseCurrTotalAmount: number;
  ExchangeRate: number;
  DiscountPercent?: any;
  DiscountPrice?: any;
  ShippingModeID: number;
  ShippingSubCatID: number;
  ShippingCatID: number;
  InsuredGoodsPrice: number;
  InsuredGoodsCurrencyID?: any;
  InsuredGoodsCurrencyCode?: any;
  IsInsuredGoodsBrandNew: boolean;
  InsuredGoodsProviderID?: any;
  InsuredStatus: string;
  ProviderInsurancePercent: number;
  IsInsuranceProvider: boolean;
  InsuredGoodsBaseCurrencyID?: any;
  InsuredGoodsBaseCurrPrice?: any;
  InsuredGoodsActualPrice?: any;
  InsuredGoodsExchangeRate?: any;
  BookingStatus: string;
  UserName: string;
  UserID: number;
  UserCountryName: string;
  UserCityName: string;
  UserCountryPhoneCodeID: string;
  UserEmail: string;
  UserPhone: string;
  CommodityType?: any;
  DiscountedPrice?: any;
  PolLatitude: any;
  PolLongitude: any;
  PodLatitude: any;
  PodLongitude: any;
  FlightNo?: any;
  AirCraftInfo?: any;
  JsonSearchCriteria: string;
  BookingJsonDetail?: any;
  BookingDesc: string;
  BookingAcknowledgment: boolean;
  JsonParametersOfSensor: string;
  IncoID?: any;
  isExcludeExp: boolean;
  isExcludeImp: boolean;
  WHID?: any;
  WHName?: any;
  WHCityCode?: any;
  WHCityName?: any;
  WHCountryCode?: any;
  WHCountryName?: any;
  WHGLocCode?: any;
  WHGLocName?: any;
  WHAddress?: any;
  WHLatitude?: any;
  WHLongitude?: any;
  WHImages?: any;
  WHMedia?: any;
  StoredFromUtc?: any;
  StoredUntilUtc?: any;
  StoredFromLcl?: any;
  StoredUntilLcl?: any;
  LoadPickupDate?: any;
  ContactPersonUserID?: any;
  WHContactInfo?: any;
  JsonBookingLocation: string;
  TruckType?: any;
  TruckCode?: any;
  TruckName?: any;
  TruckNumber: string;
  ShippingStatus: string;
  JsonAgentDestInfo?: any;
  JsonAgentOrgInfo?: any;
  JsonShippingDestInfo?: any;
  JsonShippingOrgInfo?: any;
  ProfileID: string;
  IsSpecialRequest: boolean;
  JSONSpecialRequestLogs: string;
  SpecialRequestDesc: string;
  SpecialRequestStatus: string;
  JsonCustomerSettting: string;
  PickupFrom?: any;
  PickupTo?: any;
  ChargeableWeight?: any;
  BookingWeight?: any;
  TransfersStop?: any;
  JsonTransferVesselsDetails: string;
  ComInvNo: string;
  CusClearNo: string;
  GDeclareNo: string;
  BLNo: string;
  AWBCode?: any;
  ExpFormNo: string;
  BookingContainerDetail: BookingContainerDetail[];
  BookingRouteDetail: BookingRouteDetail[];
  BookingPriceDetail: BookingPriceDetail[];
  BookingEnquiryDetail?: any;
  BookingDocumentDetail?: any;
  BookingRouteMapInfo: BookingRouteMapInfo;
  BookingUserInfo: BookingUserInfo;
  StatusID: number;
  ActivityID?: any;
  LastActivity?: any;
  LastActivityRemarks?: any;
  HMBookingSeqNum: string;
  HMTempBookingSeqNum: string;
  IsManualBookingNumberAllowed: boolean;
  AircraftTypeID?: any;
  AircraftTypeCode?: any;
  AircraftTypeName?: any;
  ProductID?: any;
  ProductName?: any;
  IsVirtualAirLine?: any;
  PriceBasisType?: any;
  MinTransitDays?: any;
  MaxTransitDays?: any;
  JsonDepartureDays?: any;
  PortCutOffDays?: any;
  JsonPortVia?: any;
  BookingSourceVia: string;
  IsBookedByProvider: boolean;
  IsLumpSum: boolean;
  BookingNote: string;
  Commodity: string;
  CommodityVariant: string;
  HSCode: string;
  ContainerWeight: number;
  LinkID?: any;
  CustomerBuyerOrderNo?: any;
  CustomerSellerOrderNo?: any;
  ContainerTrackingURL: string;
  BLTrackingURL: string;
  FlightTrackingURL?: any;
  TotalDocCharges: number;
  CurCodeDocCharges: string;
  BookingActualDays?: any;
  ChargeableDays?: any;
  BookingLeaseSpace?: any;
  ChargeableLeaseSpace?: any;
  ChargeableLeaseSpaceUnit?: any;
  IsAllGoodsReceived?: any;
  HMCommisionRate: number;
  HMCommisionValue: number;
  HMCustomerNature: string;
  InsurancePolicyMode?: any;
  PolicyHolder?: any;
  IsRateNotFoundBooking: boolean;
  IsScheduleUpdateAllowed: boolean;
  PartnerID?: any;
  PartnerName?: any;
  PartnerAddress?: any;
  PartnerContactPerson: string;
  PartnerContactEmail?: any;
  PartnerContactNo?: any;
  SpotRateBookingKey: string;
}
export interface BookingContainerDetail {
  ContainerSpecID: number;
  ContainerSpecCode: string;
  ContainerSpecDesc: string;
  BookingContTypeQty: number;
  BookingPkgTypeCBM?: any;
  BookingPkgTypeWeight?: any;
  ContainerSpecShortName?: any;
  IsTrackingRequired?: boolean;
  IsQualityMonitoringRequired?: boolean;
  JsonContainerInfo?: string
  parsedJsonContainerInfo?: string
}

export interface BookingPriceDetail {
  SurchargeType: string;
  SurchargeID: number;
  SurchargeCode: string;
  SurchargeName: string;
  SurchargeBasis: string;
  ContainerSpecID?: number;
  CurrencyID: number;
  CurrencyCode: string;
  BaseCurrencyID: number;
  BaseCurrencyCode: string;
  TotalAmount: number;
  BaseCurrTotalAmount: number;
  IndividualPrice?: number;
  BaseCurrIndividualPrice?: number;
  SortingOrder: number;
  TransMode: string;
  ExchangeRate: number;
  ActualIndividualPrice: number;
  ActualTotalAmount: number;
}export interface RouteInfo {
  DateUtc: string;
  DateLcl: string;
  ModeOfTrans: string;
  RouteDesc: string;
  IsPassed: boolean;
  PortCode: string;
  PortName: string;
  Latitude?: number;
  Longitude?: number;
  FlightNo?: string;
  AirCraftInfo?: string;
}
export interface IBookingJsonDetail {
  CarrierName: string;
  CarrierImage: string;
  ProviderID: number;
  ProviderName: string;
  ProviderImage: string;
  ProviderEmail: string;
  ProviderPhone: string;
  PolID: number;
  PolCode: string;
  PolName: string;
  PolCountryCode: string;
  PolCountryName: string;
  PolLatitude: string;
  PolLongitude: string;
  PolAddress?: any;
  PolInputAddress: string;
  PodID: number;
  PodCode: string;
  PodName: string;
  PodCountryCode: string;
  PodCountryName: string;
  PodLatitude: string;
  PodLongitude: string;
  PodAddress?: any;
  PodInputAddress: string;
  IsExcludeExp: boolean;
  IsExcludeImp: boolean;
  PickupFrom: string;
  PickupTo: string;
  ChargeableWeight: number;
  PriceBasisType: string;
  AircraftTypeID: number;
  AircraftTypeCode: string;
  AircraftTypeName: string;
  PartnerID?: any;
  PartnerName?: any;
  ProductID?: any;
  ProductName?: any;
  IsVirtualAirLine: boolean;
}

export interface LineMarker {
  LatitudeA: number;
  LongitudeA: number;
  transPortModeA?: string;
  IconA?: string;
  LatitudeB: number;
  LongitudeB: number;
  transPortModeB?: string;
  IconB?: string;
}

export interface UpdateLoadPickupDate {
  loginUserID: string;
  bookingID: number;
  bookingType: string;
  loadPickupDate?: string;
  isCancelLoadPickup: boolean;
}


export interface Polyline {
  lat: number;
  lng: number;
  IsCurrloc: boolean;
  IsHistory: boolean;
  PortCode: string;
  PortName: string;
  PolylineStatus: string;
  IsPort: boolean;
  IsGround?: boolean;
  PortType?: string;
  // PolylineStatus = "Origin" | "Destination" | "Waypoint" | "Connecting"
}

export interface EtaDetails {
  EtaUtc: string;
  EtaLcl: string;
  Status: string;
}

export interface VesselPhoto {
  DocumentFileID: number;
  DocumentFileName: string;
}

export interface VesselInfo {
  VesselName: string;
  VesselType: string;
  VesselFlag: string;
  VesselPhotos: VesselPhoto[];
}

export interface ContainerDetail {
  ContainerSpecID: number;
  ShippingCatID: number;
  ContainerNo: string;
  IsTrackingRequired: boolean;
  IsQualityMonitoringRequired: boolean;
  ContainerSpecDesc: string;
  ContainerImage: string;
  ContainerShortName: string;
  ContainerSpecCode: string;
}

export interface TrackingMonitoring {
  Polylines: Polyline[];
  RouteInformation?: any;
  EtaDetails: EtaDetails;
  VesselInfo: VesselInfo;
  ContainerDetails: ContainerDetail[];
}

export interface QaualitMonitorResp {
  AlertData: QualityMonitoringAlertData[]
  MonitoringData: QualityMonitorGraphData[]
  ProgressIndicators: ProgressIndicator[]
  bookingQualityDetails: bookingQualityDetail[],
  qualityAlertCountDetails: qualityAlertCountDetails[]
}

export interface QualityMonitorGraphData {
  Key: string;
  SensorID: string;
  EventType: string;
  IOTParamValue: number;
  IOTParamName: string;
  SortingOrder: number;
  ContainerNo: string;
  AlertCount: number;
  IOTParamPerformancePercent: number
  TotalCount: number
}


export interface QualityMonitoringAlertData {
  AlertRange?: string
  ContainerNo: string
  ContainerSize: string
  GCoordinates: string
  Humidity?: number
  HumidityDateTimeUTC?: string
  SensorID: string
  Temperature?: number
  TemperatureDateTimeUTC?: string
  ActionEmail: string
  ActionSMS: string;
  TotalTemp?: number;
  TotalTempCount?: number;
  TotalHumid?: number;
  TotalHumidCount?: number;
  AlertMin?: number;
  AlertMax?: number;
  AlertMinHumdaity?: number;
  AlertMaxTemperature?: number;
  AlertMaxHumdaity?: number;
  AletMinTemperature?: number;
}

export interface ProgressIndicator {
  ProgressID: number;
  ProgressIndicatorMin: number;
  ProgressIndicatorMax: number;
  ProgressIndicatorColor: string;
  ProgressIndicatorName: string;
}

export interface bookingQualityDetail {
  ContainerSpecDesc: string;
  containerNo: string;
  iotParameterType: string;
  iotGroupParamName: string;
  actionEmail: string;
  actionSMS: string;
  minAlertValue: number;
  maxAlertValue: number;
  HashMoveBookingNum: string;
}


export interface qualityAlertCountDetails {
  containerNo: string;
  iotParameterType: string;
  iotGroupParamName: string;
  HashMoveBookingNum: string;
  alertCount: number;
}