export interface IAppRoleRight {
  ActionID: number
  ActionName: string
  ActionBL: string
  IsAllowed: boolean
}

export interface IAppObject {
  ObjectID: number
  ObjectCode: string
  ObjectName: string
  ObjectBL: string
  ObjectType: string
  ObjectNature: string
  ParentObjectID?: number
  ObjectURL: string
  ObjectIconClass?: any
  AppRoleRights: IAppRoleRight[]
  ChildMenus: IAppObject[]
  IsSubOpen?: boolean
}

export interface IAccessRights {
  RoleID: number
  RoleName: string
  AppObject: IAppObject[]
}


export interface IInvoiceRoles {
  CUSTOMER_INVOICE_UPLOAD: boolean;
  CUSTOMER_PAYMENT_RECEIPT_ADD: boolean;
  CUSTOMER_PAYMENT_RECEIPT_UPDATE: boolean;
  CUSTOMER_PAYMENT_RECEIPT: boolean;
  PROVIDER_INVOICE_APPROVAL: boolean;
}


export interface IUsersRights {
  USERS_ADMIN_SECTION: number
  USERS_USER_SECTION: number
}


export interface ISettingsRights {
  SETTINGS_CHANGE_PASSWORD: number
  SETTINGS_COMPANY_INFORMATION: number
  SETTINGS_DOCUMENT_MANAGER: number
  SETTINGS_PERSONAL_INFORMATION: number
  SETTINGS_BANK_INFO: number
  SETTINGS_EMAIL_NOTIFICATION: number
}