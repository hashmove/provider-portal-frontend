export interface IPriceLogs {
    LogStatus: string;
    LogStatusTitle: string;
    Price: number;
    BasePrice: number;
    CurrencyID: number;
    CurrencyCode: string;
    CurrencyName: string;
    CreatedDateTime: string;
    UserID: number;
    UserName: string;
    Comments: string;
    CompanyName: string;
    IsRouteDetailUpdate: boolean;
    PickupPortDetail?: any;
    DeliveryPortDetail?: any;
    RouteUpdateInfo?: any;
}