export interface BookingStatus {
    BusinessLogic?: string
    StatusCode?: string
    StatusID?: number
    StatusName?: string
}


export interface ICompanyFilter {
    ID: number;
    Title: string;
}

export interface IProviderFilter {
    ProviderID: number;
    ProviderImage: string;
    ProviderName: string;
}

export interface ICarrierFilter {
    id: number;
    code: string;
    desc: string;
    imageName: string;
    shortName: string;
    title: string;
    type: string;
}


export class IBookingDtl {
    BookingID: number;
    HashMoveBookingNum: string;
    PolType: string;
    PolModeOfTrans: string;
    PolCode: string;
    PolName: string;
    PolInputAddress: string;
    PolAddress?: any;
    PolCountry: string;
    PodType: string;
    PodModeOfTrans: string;
    PodCode: string;
    PodName: string;
    PodInputAddress: string;
    PodAddress?: any;
    PodCountry: string;
    HashMoveBookingDate: string;
    BookingType: string;
    EtaUtc: string;
    EtaLcl: string;
    ShippingMode: string;
    ShippingModeImage: string;
    BookingStatus: string;
    EtdUtc: string;
    EtdLcl: string;
    ContainerLoad: string;
    UserID: number;
    FirstName: string;
    LastName: string;
    UserImage: string;
    BookingTab: string;
    ContainerCount: number;
    BookingTotalAmount: number;
    CurrencyID: number;
    CurrencyCode: string;
    BaseCurrTotalAmount: number;
    BaseCurrencyID: number;
    BaseCurrencyCode: string;
    ContainerLoadType: string;
    BookingRoutePorts: string;
    ShippingModeCode: string;
    WHName: string;
    WHCityCode: string;
    WHCityName: string;
    WHCountryCode: string;
    WHCountryName: string;
    WHGLocCode: string;
    WHGLocName: string;
    WHAddress: string;
    WHLatitude: string;
    WHLongitude: string;
    WHImages: string;
    WHMinimumLeaseTerm: string;
    StoredFromUtc: string;
    StoredUntilUtc: string;
    StoredFromLcl: string;
    StoredUntilLcl: string;
    ProviderName: string;
    ProviderImage: string;
    ProviderEmail: string;
    ProviderPhone: string;
    ProviderID: number;
    CarrierName: string;
    CarrierImage: string;
    DocumentStatus: string;
    ShippingStatus: string;
    CustomerName: string;
    CustomerImage: string;
    CompanyID: number;
    BookingStatusCode: string;
    ShippingStatusCode: string;
    HMCommisionValue: number;
    IsSpecialRequest: boolean;
    SpecialRequestDesc: string;
    SpecialRequestStatusID?: any;
    SpecialRequestStatus: string;
    SpecialRequestStatusBL?: any;
    JSONSpecialRequestLogs?: any;
    BookingSpecialAmount: number;
    BaseCurrSpecialAmount?: any;
    PolCountryCode: string;
    PodCountryCode: string;
    DiscountPrice: number;
    DiscountBasePrice: number;
    BookingDesc: string;
    ShippingCatName: string;
    ShippingCatImage: string;
    JsonSearchCriteria: string;
    PickupFrom?: any;
    PickupTo?: any;
    ChargeableWeight?: any;
    BookingWeight?: any;
    StatusID: number;
    ActivityID?: any;
    LastActivity?: any;
    LastActivityRemarks?: any;
    TotalCountNumber: number;
    AircraftTypeCode?: any;
    PartnerName?: any;
    ProductName?: any;
    IsVirtualAirLine?: any;
    PriceBasisType?: any;
    JsonPortVia?: any;
    TotBooking: number;
    TotCurrBooking: number;
    TotPastBooking: number;
    TotSaveBooking: number;
    TotSpotReq: number;
}

export interface FilterdBookings {
    draw?: any;
    data: IBookingDtl[];
    recordsFiltered: number;
    recordsTotal: number;
    totBooking: number;
    totCurrBooking: number;
    totPastBooking: number;
    totSaveBooking: number;
    totSpotReq: number;
}


export interface IBookingFilterObj {
    fromDate: any
    toDate: any
    isMarketplace: any
    filterDestination: any
    filterOrigin: any

    selectedBookStatusCode: any
    selectedShipStatusCode: any
    selectedShippingModeCode: any
    selectedProvider: any
    selectedCarrier: any
    currPage: any
    currentTab: any
    sortBy: any
}

export interface IAddSchedule {
    carrierCode: string,
    carrierName: string,
    transhipments: string,
    vesselCode: string,
    vesselName: string,
    voyage: string,
    startPortCode: string,
    startPortName: string,
    startDate: string,
    tsVesselCode1?: string,
    tsVesselName1?: string,
    tsVoyage1?: string,
    tsPortCode1?: string,
    tsPortName1?: string,
    tsETD1?: string,
    tsETA1?: string,
    tsVesselCode2?: string,
    tsVesselName2?: string,
    tsVoyage2?: string,
    tsPortCode2?: string,
    tsPortName2?: string,
    tsETD2?: string,
    tsETA2?: string,
    endPortCode: string,
    endPortName: string,
    endDate: string,
    duration: string,
}