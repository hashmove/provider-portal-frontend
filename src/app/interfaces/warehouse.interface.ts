export interface WarehouseAddCharges {
  addChrCode: string;
  addChrID: number;
  addChrName: string;
  addChrType: string;
  currencyID: string;
  price: string;
  priceBasis: string;
  addChrBasis?: any;
  sWHType?: string;
  addOnBasis?: string;
  addOnCount?: number;
  addOnPrice?: number;
}

export interface WHModel {
  WHID: number;
  ProviderID: number;
  WHName: string;
  WHAddress: string;
  WHDesc: string;
  CountryID: number;
  CityID: number;
  CityName?: any;
  WHPOBoxNo: string;
  GLocID?: any;
  Latitude: string;
  Longitude: string;
  TotalCoveredArea: number;
  TotalCoveredAreaUnit: string;
  UsageType: string;
  FacilitiesProviding: string;
  WHGallery: string;
  IsBlocked: boolean;
  OfferedHashMoveArea: number;
  OfferedHashMoveAreaUnit: string;
  CeilingHeight: number;
  CeilingLenght?: any;
  CeilingWidth: number;
  CeilingUnit: string;
  WHMinCBM: number;
  WHMinKG?: any;
  WHMinSQFT: number;
  AvailableSQFT: number;
  ComissionType?: any;
  ComissionCurrencyID?: any;
  ComissionValue?: any;
  Percent?: any;
  CreatedBy?: any;
  CreatedDateTime?: string;
  ModifiedBy?: any;
  ModifiedDateTime?: string;
  IsRateAvailable: boolean;
  WHFacilitiesProviding?: any;
  MinLeaseTermValue: number;
  MinLeaseTermUnit: string;
  CeilingDesc?: any;
  WHMinPallet: number;
  WHMaxPallet?: any;
}

export interface DocumentType {
  DocumentTypeID: number;
  DocumentTypeCode: string;
  DocumentTypeName: string;
  DocumentTypeNameOL?: any;
  DocumentTypeDesc: string;
  SortingOrder: number;
  DocumentNature: string;
  DocumentSubProcess?: any;
  DocumentID?: any;
  UserID?: any;
  BookingID?: any;
  CompanyID?: any;
  ProviderID?: any;
  WHID?: any;
  VesselID?: any;
  DocumentName: string;
  DocumentDesc: string;
  DocumentFileName?: any;
  DocumentFile?: any;
  DocumentFileContent: string;
  DocumentUploadedFileType?: any;
  DocumentLastStatus?: any;
  ExpiryStatusCode: string;
  ExpiryStatusMessage: string;
  DocumentUploadDate?: string;
  IsDownloadable: boolean;
  IsApprovalRequired: boolean;
  BusinessLogic: string;
  CopyOfDocTypeID?: any;
  ReasonID?: any;
  ReasonCode?: any;
  ReasonName?: any;
  DocumentStausRemarks?: any;
  IsUploadable?: any;
  MetaInfoKeysDetail?: any;
  FileContent?: any;
  StatusAction?: any;
  ProviderName?: any;
  EmailTo?: any;
  PhoneTo?: any;
  UserName?: any;
  UserCompanyName?: any;
  UserCountryPhoneCode: number;
  HashMoveBookingNum?: any;
}

export interface WHFacilitiesProviding {
  FacilitiesTypeID: number;
  FacilitiesTypeTitle: string;
  BusinessLogic: string;
  IsAllowed: boolean;
}

export interface WarehouseResponse {
  WHModel: WHModel[];
  documentType: DocumentType;
  IsRealEstate: boolean;
  WHFacilitiesProviding: WHFacilitiesProviding[];
}

export interface IUnitCalculation {
  UnitTypeID: number;
  UnitTypeCode: string;
  UnitTypeCalc: number;
}

export interface ICalUnit {
  UnitTypeID: number;
  UnitTypeCode: string;
  UnitTypeName: string;
  UnitTypeShortName: string;
  UnitTypeNature: string;
  UnitCalculation: IUnitCalculation[];
}