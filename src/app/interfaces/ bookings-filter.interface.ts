

export interface BookingStatus {
    BusinessLogic?: string
    StatusCode?: string
    StatusID?: number
    StatusName?: string
  }
  
  
  export interface ICompanyFilter {
    ID: number;
    Title: string;
  }
  
  export interface IProviderFilter {
    ProviderID: number;
    ProviderImage: string;
    ProviderName: string;
  }
  
  export interface ICarrierFilter {
    id: number;
    code: string;
    desc: string;
    imageName: string;
    shortName: string;
    title: string;
    type: string;
  }
  
  
  export interface IBookingDtl {
    isExpanded?: boolean;
    ChildCount?: number;
    ChildRequests?: IBookingDtl[];
    HMParentBookingNum?: string;
    SpotRateBookingKey?: string;
    BookingID: number;
    HashMoveBookingNum: string;
    PolType: string;
    PolModeOfTrans: string;
    PolCode: string;
    PolName: string;
    PolInputAddress: string;
    PolAddress?: any;
    PolCountry: string;
    PodType: string;
    PodModeOfTrans: string;
    PodCode: string;
    PodName: string;
    PodInputAddress: string;
    PodAddress?: any;
    PodCountry: string;
    HashMoveBookingDate: Date;
    BookingType: string;
    EtaUtc: Date;
    EtaLcl: Date;
    ShippingMode: string;
    ShippingModeImage: string;
    BookingStatus: string;
    EtdUtc: Date;
    EtdLcl: Date;
    ContainerLoad: string;
    UserID: number;
    FirstName: string;
    LastName: string;
    UserImage: string;
    BookingTab: string;
    ContainerCount: number;
    BookingTotalAmount: number;
    CurrencyID: number;
    CurrencyCode: string;
    BaseCurrTotalAmount: number;
    BaseCurrencyID: number;
    BaseCurrencyCode: string;
    ContainerLoadType: string;
    BookingRoutePorts: string;
    ShippingModeCode: string;
    WHName: string;
    WHCityCode: string;
    WHCityName: string;
    WHCountryCode: string;
    WHCountryName: string;
    WHGLocCode: string;
    WHGLocName: string;
    WHAddress: string;
    WHLatitude: string;
    WHLongitude: string;
    WHImages: string;
    WHMinimumLeaseTerm: string;
    StoredFromUtc: Date;
    StoredUntilUtc: Date;
    StoredFromLcl: Date;
    StoredUntilLcl: Date;
    ProviderName: string;
    ProviderImage: string;
    ProviderEmail: string;
    ProviderPhone: string;
    ProviderID: number;
    CarrierName: string;
    CarrierImage: string;
    DocumentStatus: string;
    ShippingStatus: string;
    CustomerName: string;
    CustomerImage: string;
    CompanyID: number;
    BookingStatusCode: string;
    ShippingStatusCode: string;
    HMCommisionValue: number;
    IsSpecialRequest: boolean;
    SpecialRequestDesc: string;
    SpecialRequestStatusID?: any;
    SpecialRequestStatus: string;
    SpecialRequestStatusBL?: any;
    JSONSpecialRequestLogs?: any;
    BookingSpecialAmount: number;
    BaseCurrSpecialAmount?: any;
    PolCountryCode: string;
    PodCountryCode: string;
    DiscountPrice: number;
    DiscountBasePrice: number;
    BookingDesc: string;
    ShippingCatName: string;
    ShippingCatImage: string;
    JsonSearchCriteria: string;
    PickupFrom?: any;
    PickupTo?: any;
    ChargeableWeight?: any;
    BookingWeight?: any;
    StatusID: number;
    ActivityID?: any;
    LastActivity?: any;
    LastActivityRemarks?: any;
    TotalCountNumber: number;
    AircraftTypeCode?: any;
    PartnerName?: any;
    ProductName?: any;
    IsVirtualAirLine?: any;
    PriceBasisType?: any;
    JsonPortVia?: any;
    TotBooking: number;
    TotCurrBooking: number;
    TotPastBooking: number;
    TotSaveBooking: number;
    TotSpotReq: number;
  }
  
  export interface FilterdBookings {
    draw?: any;
    data: IBookingDtl[];
    recordsFiltered: number;
    recordsTotal: number;
    totBooking: number;
    totCurrBooking: number;
    totPastBooking: number;
    totSaveBooking: number;
    totSpotReq: number;
  }
  
  
  export interface IBookingFilterObj {
    fromDate: any
    toDate: any
    isMarketplace: any
    filterDestination: any
    filterOrigin: any
    hmBookingNum: any
    selectedBookStatusCode: any
    selectedShipStatusCode: any
    selectedShippingModeCode: any
    selectedProvider: any
    selectedCarrier: any
    currPage: any
    currentTab: any
    sortBy: any
  }
  