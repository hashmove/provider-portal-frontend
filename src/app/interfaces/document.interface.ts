import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { MetaInfoKeysDetail } from "../components/pages/user-desk/settings/settings.interface";
export interface DocumentUpload {
  DocumentTypeID: number;
  DocumentTypeCode: string;
  DocumentTypeName: string;
  DocumentTypeNameOL: string;
  DocumentTypeDesc: string;
  SortingOrder: number;
  DocumentNature: string;
  DocumentSubProcess: string;
  DocumentID?: any;
  UserID?: any;
  BookingID?: any;
  CompanyID?: any;
  ProviderID?: any;
  DocumentName: string;
  DocumentDesc: string;
  DocumentFileName?: any;
  DocumentFileContent: string;
  DocumentUploadedFileType?: any;
  DocumentLastStatus?: any;
  ExpiryStatusCode: string;
  ExpiryStatusMessage: string;
  DocumentUploadDate?: any;
  IsDownloadable: boolean;
  IsUploadable?: any;
  IsApprovalRequired: boolean;
  BusinessLogic?: any;
  CopyOfDocTypeID?: any;
  MetaInfoKeysDetail?: any;
  FileContent: fileContent[]
}
export interface fileContent {
  documentFileName: string,
  documentFile: string,
  documentUploadedFileType: string
}
export interface DocumentFile {
  fileBaseString: string
  fileName: string
  fileType: string
  fileUrl?: string | ArrayBuffer
}

export interface UserDocument {
  PartnerInvoiceID: any;
  InvoiceID: any;
  IsProvider?: boolean;
  OtherID: any;
  OtherType: string;
  UploadedBy: string;
  HideCancel?: boolean;
  CreatedByUserName?: string;
  CreatedByUserCompanyName?: string;
  DocumentTypeID: number;
  UserCompanyID: any;
  DocumentTypeCode: string;
  DocumentTypeName: string;
  DocumentTypeDesc: string;
  SortingOrder: number;
  DocumentSubProcess: string;
  DocumentID?: number;
  UserID?: number;
  BookingID?: number,
  CompanyID?: number,
  ProviderID?: number,
  DocumentName: string;
  DocumentDesc: string;
  DocumentFileName?: string;
  DocumentFileContent: string;
  DocumentUploadedFileType?: string;
  DocumentLastStatus?: string;
  ExpiryStatusCode: string;
  ExpiryStatusMessage: string;
  DocumentUploadDate?: string;
  IsDownloadable: boolean;
  IsApprovalRequired: boolean;
  MetaInfoKeysDetail: MetaInfoKeysDetail[];
  ShowUpload?: boolean;
  DocumentNature?: string;
  DocumentTypeNameOL?: any;
  WHID?: any;
  VesselID?: any;
  DocumentFile?: any;
  BusinessLogic?: any;
  IsDefaultDocument?: boolean;
  CopyOfDocTypeID?: any;
  ReasonID?: any;
  ReasonCode?: any;
  ReasonName?: any;
  DocumentStausRemarks?: any;
  IsUploadable?: any;
  FileContent?: any;
  StatusAction?: any;
  ProviderName?: any;
  EmailTo?: any;
  PhoneTo?: any;
  UserName?: any;
  UserCompanyName?: any;
  UserCountryPhoneCode?: any;
  HashMoveBookingNum?: any;
  LoginUserID?: number;
  AllowedFileTypes?: string;
  ShowCancel?: false;

  // For Invoice Payment Docs
  PaymentNo?: string
  PaymentTo?: string
  PaymentToDesc?: string
}
