export interface BillingTile {
  currencyID: number;
  currencyCode: string;
  title: string;
  amount: number;
}

export interface PaymentDueTile {
  currencyID: number;
  currencyCode: string;
  title: string;
  amount: number;
  dueDate: string;
}

export interface TotalBillingTile {
  currencyID: number;
  currencyCode: string;
  title: string;
  amount: number;
}

export interface GraphStatistic {
  keyYear: number;
  keyMonth: string;
  keyMode: string;
  amount: number;
  sortingOrder: number;
}

export interface ProviderBillingDashboard {
  billingTile: BillingTile;
  paymentDueTile: PaymentDueTile;
  totalBillingTile: TotalBillingTile;
  graphStatistics: GraphStatistic[];
  invoices: ProviderBillingDashboardInvoice[]
}

export interface ProviderBillingDashboardInvoice {
  invoiceID: number;
  invoiceNo: string;
  description: string;
  issuedDate: string;
  dueDate: string;
  billingCurID: number;
  billingCurCode: string;
  billingAmount: number;
  billingStatus: string;
  paymentCurID: number;
  paymentCurCode: string;
  paymentAmount: number;
  paymentStatus: string;
}

export interface UserInfo {
  // ProviderConfig:IProviderConfig
  ProviderNatureBL?: any;
  DataSourceType: any;
  ServicesOfferedJSON: any;
  POBox: any;
  SocialAccountName: any;
  CityID: any;
  UserID: number;
  CompanyID: number;
  ProviderID: number;
  CountryID: number;
  RegionID: number;
  RegionCode: string;
  FirstNameBL: string;
  LastNameBL: string;
  JobTitleBL: string;
  PrimaryPhoneBL: string;
  CountryPhoneCodeBL?: any;
  FirstNameOL: string;
  LastNameOL: string;
  JobTitleOL: string;
  PrimaryPhoneOL: string;
  CountryPhoneCodeOL: string;
  PrimaryEmail: string;
  PhoneCodeCountryID?: any;
  LanguageID: number;
  IsLogedOut: boolean;
  UserProfileStatus: string;
  PortalName: string;
  LoginID: string;
  CountryPhoneCode: string;
  UserType: string;
  WHID?: any;
  CurrencyID?: number;
  CompanyName?: string;
  IsVerified?: boolean;
  DataSource?: string;
  PhoneNumer?: string;
  AddressLine1?: string;
  SocialMedia: any
  JsonAssociationWith?: any
  ProspectID?: number
  ProviderConfig?: IProviderConfig
}


export interface IProviderConfig {
  IsInvoicePaymentShow: boolean;
  IsSpotRateRquired: boolean;
  IsDisplayCommodityNameOnSearchBox: boolean;
  HMCommissionDisplayFor: string;
  IsAutoRateRequestRequired: boolean;
  BookingExpiryHours: number
  IsBookingExpiryApplied: boolean
  IsPerformanceShow: boolean
  IsSpotRateRestricted: boolean
  IsInsuranceDisplayServiceShow: boolean
  IsPremiumTrackingPlatformControlShow: boolean
  IsManageRateRestricted: boolean
}


export interface CodeValMst {
  codeValID: number;
  codeVal: string;
  codeValShortDesc: string;
  codeValDesc: string;
  codeValPreVal?: any;
  codeValNextVal?: any;
  languageID: number;
  codeType: string;
  isDelete: boolean;
  isActive: boolean;
  createdBy: string;
  createdDateTime: string;
  modifiedBy?: any;
  modifiedDateTime?: any;
  sortingOrder: number;
  codeValLink?: any;
  addChrName?: any;
  modeOfTrans?: any;
  containerLoadType?: string;
}
