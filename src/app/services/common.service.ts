import { Injectable } from '@angular/core';
import { baseApi } from "../constants/base.url";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from 'rxjs';
import { map, publishReplay, refCount } from 'rxjs/operators';
@Injectable()
export class CommonService {
  countries: Observable<any>;

  constructor(private _http: HttpClient) { }

  getCountry() {
    let url: string = "country/GetDropDownDetailOtherLanguage/0";
    return this._http.get(baseApi + url);
  }
  getCountryV2(): Observable<any> {

    // Cache it once if countries value is false
    if (!this.countries) {
      let url: string = "country/GetDropDownDetailOtherLanguage/0";
      this.countries = this._http.get(baseApi + url).pipe(
        map(data => data['countries']),
        publishReplay(1), // this tells Rx to cache the latest emitted
        refCount() // and this tells Rx to keep the Observable alive as long as there are any Subscribers
      );
    }
    return this.countries;
  }

  // Clear configs
  clearCache() {
    this.countries = null;
  }

  getCities() {
    let url: string = "City/GetDropDownDetail/0";
    return this._http.get(baseApi + url);
  }
  getRegions() {
    let url: string = "Region/GetAll";
    return this._http.get(baseApi + url);
  }
  getAllCurrency(languageID) {
    let url: string = `currency/GetDropDownDetail/${languageID}`;
    return this._http.get(baseApi + url);
  }
  translatedLanguage(sourceLanguage, targetLanguage, text) {
    const params = new HttpParams()
      .set('source', sourceLanguage)
      .set('target', targetLanguage)
      .set('q', text)
      .set('key', 'AIzaSyDy8gRbCqDNl7BaN-rqW_r6IfMB45tf1oc');
    let url: string = "https://translation.googleapis.com/language/translate/v2";
    return this._http.get(url, { params });
  }

  detectedLanguage(text) {
    const params = new HttpParams()
      .set('q', text)
      .set('key', 'AIzaSyDy8gRbCqDNl7BaN-rqW_r6IfMB45tf1oc');
    let url: string = "https://translation.googleapis.com/language/translate/v2/detect";
    return this._http.get(url, { params });
  }

  getBrowserlocation() {
    let url = 'https://pro.ip-api.com/json/?key=P7naS9oua6127nD';
    try {
      if (location.protocol.toLowerCase() === 'http:') {
        url = 'http://pro.ip-api.com/json/?key=P7naS9oua6127nD';
      }
    } catch (error) { }
    return this._http.get(url);
  }

  getHelpSupport(providerId: number) {
    // let url: string = `general/GetHMHelpSupportDetail`;
    let url: string = `general/GetProviderHelpSupportDetail/${-1}`;
    return this._http.get((baseApi + url))
  }

  getMstCodeVal(tag: string) {
    let url = `MstCodeVal/GetMstCodeValMultipleList/${tag}`
    return this._http.get(baseApi + url);
  }


  getExchangeRateList(currencyID) {
    const url = 'currency/GetExchangeRateList/' + currencyID
    return this._http.get(baseApi + url)
  }

  getPortsData($portType?: string) {
    let portType = $portType
    if (!portType) {
      portType = 'SEA'
    }
    let url: string = `Ports/GetPortsList/0/${portType}`;
    return this._http.get(baseApi + url);
  }

  getCurrencyNew() {
    const url: string = "currency/GetCurrencyList/0";
    return this._http.get(baseApi + url);
  }

  getCurrency() {
    const url: string = "Currency/GetDropDownDetail/100";
    return this._http.get(baseApi + url);
  }

  getCountryList() {
    let url: string = "Country/GetDropDownDetail/0";
    return this._http.get(baseApi + url);
  }

  getHmLogo() {
    let url: string = "general/GetHMLogo";
    return this._http.get(baseApi + url);
  }

  getCompany() {
    let url: string = "Company/GetDropDownDetail/0";
    return this._http.get(baseApi + url);
  }

  howhear() {
    let url: string = "MstCodeVal/GetMstCodeValMultipleList/HOW_HEAR_US";
    return this._http.get(baseApi + url);
  }

  getFilteredCity($filterVal) {
    let url: string = `city/GetCityDropDownDetail/0/${$filterVal}`
    return this._http.get(baseApi + url);
  }
  getFilteredCityV1($filterVal) {
    let url: string = `city/GetCityDropDownDetailV1/0/${$filterVal}`
    return this._http.get(baseApi + url);
  }
  getCityById($cityId) {
    let url: string = `city/Get/${$cityId}`
    return this._http.get(baseApi + url);
  }

  getFilteredBank($filterVal) {
    let url: string = `Bank/GetBankDropDownDetail/0/${$filterVal}`
    return this._http.get(baseApi + url);
  }


  getExpiringRateCount($providerID) {
    let url: string = `provider/GetExpiringRateCount/${$providerID}`
    return this._http.get(baseApi + url);
  }

  getHMCommissionTermsCondition($bookingKey) {
    let url: string = `booking/GetHMCommissionTermsCondition/${$bookingKey}`
    return this._http.get(baseApi + url);
  }

  getBookingCustomerTermsCondition($bookingKey) {
    let url: string = `booking/GetBookingCustomerTermsCondition/${$bookingKey}`
    return this._http.get(baseApi + url);
  }

  getCarrierDropDownDetail($filterVal) {
    let url: string = `carrier/GetCarrierDropDownDetail/0/${$filterVal}`
    return this._http.get(baseApi + url);
  }

  getCarrierDropDownDetailAir($filterVal) {
    let url: string = `carrier/GetCarrierDropDownDetailWithType/0/${$filterVal}/AIR`
    return this._http.get(baseApi + url);
  }

  getPortsDataV2($portType?: string) {
    let portType = $portType
    if (!portType) {
      portType = 'SEA'
    }
    let url: string = `ports/GetPortsByType/${0}/${portType}`;
    return this._http.get(baseApi + url);
  }

  getFile(_url) {
    return this._http.get(_url)
  }

  getPaymentTermsCondition(companyID, shippingModeID, containerLoadType) {
    const _url: string = `PaymentTermsCondition/GetPaymentTermsCondition/${companyID}/${shippingModeID}/${containerLoadType}`;
    return this._http.get(baseApi + _url);
  }

  getAllPaymentTermsCondition(companyID) {
    const _url: string = `PaymentTermsCondition/GetPaymentTermsCondition/${companyID}`;
    return this._http.get(baseApi + _url);
  }

  getServerDate() {
    const url: string = `general/GetServerUTCDate`;
    return this._http.get(baseApi + url);
  }

  getBankDataByID($bankID) {
    let url: string = `Bank/GetBankDataByID/${$bankID}`
    return this._http.get(baseApi + url);
  }

  getRoleActions(roleID, objBL, objPortal, objType) {
    const url: string = `approlerights/GetObjectAction/${roleID}/${objBL}/${objPortal}/${objType}`
    return this._http.get(baseApi + url)
  }

  getCityDtl(cityId: any) {
    let url: string = `city/GetCityDataByID/${cityId}`;
    return this._http.get(baseApi + url);
  }

  getBookingsCount(data) {
    const url = 'booking/GetDashboardBookingListCount'
    return this._http.post(baseApi + url, data)
  }

  getSupportFAQs() {
    const url = `general/GetHelpSupportTutorials/provider`
    return this._http.get(baseApi + url);
  }
}
