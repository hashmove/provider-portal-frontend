import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { BehaviorSubject } from 'rxjs';
import { ShippingCriteriaCat, CargoTypes } from '../interfaces/business-profile.interface';
import { getProviderShippingCat } from '../constants/globalFunctions';
import { UserInfo } from '../interfaces/billing.interface';
import { IExpiryNotification } from '../components/pages/user-desk/user-desk.component';

@Injectable()
export class SharedService {

  constructor(private _http: HttpClient) { }

  public countryList = new BehaviorSubject<any>(null);
  public regionList = new BehaviorSubject<any>(null);
  public currencyList = new BehaviorSubject<any>(null);
  public cityList = new BehaviorSubject<any>(null);
  public activatedBookingTab = new BehaviorSubject<any>(null);
  public updateAvatar = new BehaviorSubject<any>(null);
  // public countryList = this.countries.asObservable();

  public getUserInfoByOtp = new BehaviorSubject<any>(null);
  public getUserOtpVerified = new BehaviorSubject<any>(null);
  public documentList = new BehaviorSubject<any>(null);
  public dataLogisticServiceBySea = new BehaviorSubject<any>(null);
  public draftRowFCLAdd = new BehaviorSubject<any>(null);
  public draftRowLCLAdd = new BehaviorSubject<any>(null);
  public draftRowAddAir = new BehaviorSubject<any>(null);
  public draftRowAddGround = new BehaviorSubject<any>(null);
  public updatedDraftsAir = new BehaviorSubject<any>(null);
  public termNcondGround = new BehaviorSubject<any>(null);
  public termNcondAir = new BehaviorSubject<any>(null);
  public termNcondFCL = new BehaviorSubject<any>(null);
  public termNcondLCL = new BehaviorSubject<any>(null);
  public businessProfileJsonLabels = new BehaviorSubject<any>(null);
  public jobTitleList = new BehaviorSubject<any>(null);
  public businessDetailObj = new BehaviorSubject<any>(null);
  public dashboardDetail = new BehaviorSubject<any>(null);
  public reloadHeader = new BehaviorSubject<boolean>(null);

  private userLocation = new BehaviorSubject<ILocation>(null);
  public getLocation = this.userLocation.asObservable();


  public IsloggedIn = new BehaviorSubject<boolean>(true);
  public signOutToggler = new BehaviorSubject<boolean>(null);

  private currencyDataSource = new BehaviorSubject<any[]>(null);
  currenciesList = this.currencyDataSource.asObservable();

  //Map

  public resetMapMarkers = new BehaviorSubject<boolean>(false)
  public newMapMarkerList = new BehaviorSubject<Array<any>>(null)
  public appendNewMapMarkers = new BehaviorSubject<Array<any>>(null)
  public updateUserDocsData = new BehaviorSubject<any>(null)
  public obsWarehouseMap = new BehaviorSubject<any>(null)
  public bookingsRefresh = new BehaviorSubject<boolean>(null)
  public providerLogoUpdate = new BehaviorSubject<string>(null)
  public _profile_id = new BehaviorSubject<string>(null)
  public providerLogo = new BehaviorSubject<string>(null)
  public draftSummary = new BehaviorSubject<any>(null)
  public mainScrollListener = new BehaviorSubject<any>(null)
  public currentBookingCount = new BehaviorSubject<any>(null)
  public expNotifications = new BehaviorSubject<IExpiryNotification>(null)
  public isRegistrationRequired = false
  public static SOURCE_VIA = '-1|MP|MP|MP'
  public static VB_SOURCE_VIA = '-1|MP|MP|MP'
  public marketingUser: UserInfo = null
  public rateRequestId: any = null
  static rateRequestIdV2: any = null

  public static SET_MAIN_SOURCE_VIA(via) {
    SharedService.SOURCE_VIA = via
    SharedService.VB_SOURCE_VIA = via
  }
  public static SET_VB_SOURCE_VIA(via) {
    SharedService.VB_SOURCE_VIA = via
  }


  public static GET_SOURCE_VIA(url: string) {
    let via = '-1|MP|MP|MP'
    try {
      if ((location.href.includes('booking-detail') || (location.href.includes('rate-requests') && !url.includes('GetDashboardRequestList'))) && SharedService.VB_SOURCE_VIA) {
        via = SharedService.VB_SOURCE_VIA
      } else {
        via = SharedService.SOURCE_VIA
      }
    } catch (error) {
      via = SharedService.VB_SOURCE_VIA
    }
    if (!via) {
      via = SharedService.SOURCE_VIA
    }
    return via
  }


  static SET_PROFILE_ID($id) {
    localStorage.setItem('_profile_id', $id)
  }

  static GET_PROFILE_ID() {
    try {
      return localStorage.getItem('_profile_id')
    } catch (error) {
      return ''
    }
  }

  public static PROFILE_ID: string = ''

  setMapLocation(data) {
    this.userLocation.next(data);
  }
  getMapLocation(): ILocation {
    return this.userLocation.getValue();
  }
  // setCountries(data) {
  //   this.countries.next(data);
  // }

  setCurrency(data: any[]) {
    this.currencyDataSource.next(data);
  }

  getFilteredCargos(mode: string, cargoArray: Array<any>, comapreKey: string) {
    let _filteredCargoTypes = []

    try {
      const shipCats = getProviderShippingCat()
      if (shipCats && shipCats.length > 0) {
        const providerShipCat: ShippingCriteriaCat[] = shipCats.filter(cat => cat.ShippingModeCode.toLowerCase() === mode.toLowerCase())[0].ShippingCriteriaCat
        providerShipCat.forEach(cat => {
          const { ShippingCatID } = cat
          const _tempCargo = cargoArray.filter((cargo: any) => cargo[comapreKey] === ShippingCatID)[0]
          _filteredCargoTypes.push(_tempCargo)
        })
      } else {
        _filteredCargoTypes = shipCats
      }
    } catch (error) {
      _filteredCargoTypes = cargoArray
    }
    return _filteredCargoTypes
  }
}


export interface UploadRatesDocumnet {
  DocumentTypeID: number;
  DocumentTypeCode: string;
  DocumentTypeName: string;
  DocumentTypeNameOL?: any;
  DocumentTypeDesc: string;
  SortingOrder: number;
  DocumentNature: string;
  DocumentSubProcess?: any;
  DocumentID?: any;
  UserID?: any;
  BookingID?: any;
  CompanyID?: any;
  ProviderID?: any;
  WHID?: any;
  VesselID?: any;
  DocumentName: string;
  DocumentDesc: string;
  DocumentFileName?: any;
  DocumentFile?: any;
  DocumentFileContent: string;
  DocumentUploadedFileType?: any;
  DocumentLastStatus?: any;
  ExpiryStatusCode: string;
  ExpiryStatusMessage: string;
  DocumentUploadDate?: any;
  IsDownloadable: boolean;
  IsApprovalRequired: boolean;
  BusinessLogic: string;
  CopyOfDocTypeID?: any;
  ReasonID?: any;
  ReasonCode?: any;
  ReasonName?: any;
  DocumentStausRemarks?: any;
  IsUploadable?: any;
  MetaInfoKeysDetail?: any;
  FileContent?: any;
  StatusAction?: any;
  ProviderName?: any;
  EmailTo?: any;
  PhoneTo?: any;
  UserName?: any;
  UserCompanyName?: any;
  UserCountryPhoneCode: number;
  HashMoveBookingNum?: any;
  AllowedFileTypes?: any;
}

export interface LogisticServices {
  ProviderID: number;
  CreatedBy?: any;
  ServiceID: number;
  ServiceType?: any;
  LogServID: number;
  LogServCode: string;
  LogServName: string;
  IsRealEstate?: any;
  ActionType?: any;
  UploadRatesDocumnet: UploadRatesDocumnet[];
}


export interface ILocation {
  as: string;
  city: string;
  country: string;
  countryCode: string;
  isp: string;
  lat: number;
  lon: number;
  org: string;
  query: string;
  region: string;
  regionName: string;
  status: string;
  timezone: string;
  zip: string;
}