import { Injectable } from "@angular/core";
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import * as moment from 'moment';
import { setBaseApi, setBaseExternal, setBasePartner, setBiConfig, setHMPaymentPolicy, setHMPaymentTerm, setNewFeature, setPortalOrigin, setRRV1 } from "../constants/base.url";
import { JWTObj, UserCreationService } from "../components/pages/user-creation/user-creation.service";
import { SharedService } from "../services/shared.service";


@Injectable()
export class GuestService {

    private token: string = ''
    private refreshToken: string = ''

    private shareToken: string = ''
    private shareRefreshToken: string = ''

    private rightObj: string = ''
    private origin: string = ''
    private countryCode = 'US'
    // guestObject = {
    //     password: 'h@shMove123',
    //     loginUserID: 'support@hashmove.com',
    //     CountryCode: (this.countryCode) ? this.countryCode : 'DEFAULT',
    //     LoginIpAddress: "0.0.0.0",
    //     LoginDate: moment(Date.now()).format(),
    //     LoginRemarks: ""
    // }

    constructor(
        private _authService: UserCreationService,
        private _http: HttpClient,
        private _sharedService: SharedService,
    ) { }

    public getToken() { return this.token }
    public getLclRefreshToken() { return this.refreshToken }

    public getShareToken() { return this.shareToken }
    public getShareLclRefreshToken() { return this.shareRefreshToken }
    public getRightObj() { return this.rightObj }

    public setToken($token) { this.token = $token }
    public setRefreshToken($refreshToken) { this.refreshToken = $refreshToken }

    public setShareToken($token) { this.shareToken = $token }
    public setShareRefreshToken($refreshToken) { this.shareRefreshToken = $refreshToken }

    public setRightObj($rightObj) { this.rightObj = $rightObj }
    public setOrigin($origin) { this.origin = $origin }


    getJwtToken() {
        let token
        if (localStorage.getItem('token')) {
            token = localStorage.getItem('token')
        } else {
            token = this.getToken()
        }
        return token
    }

    getRefreshToken() {
        let refreshToken
        if (localStorage.getItem('refreshToken')) {
            refreshToken = localStorage.getItem('refreshToken')
        } else {
            refreshToken = this.getLclRefreshToken()
        }
        return refreshToken
    }


    saveJwtToken($token) {
        // console.log($token);
        this.setToken($token)
        localStorage.setItem('token', $token);
    }

    saveRefreshToken($refreshToken) {
        this.setRefreshToken($refreshToken)
        localStorage.setItem('refreshToken', $refreshToken);
    }

    saveJwtRightsObj($obj) {
        this.setRightObj($obj)
        // localStorage.setItem('objectRights', $obj);
    }


    saveShareJwtToken($token) {
        this.setShareToken($token)
        // localStorage.setItem('share-token', $token);
    }

    saveShareRefreshToken($refreshToken) {
        this.setShareRefreshToken($refreshToken)
        // localStorage.setItem('share-refreshToken', $refreshToken);
    }

    getShareJwtToken() {
        // let token
        // if (localStorage.getItem('share-token')) {
        //     token = localStorage.getItem('share-token')
        // } else {
        //     token = this.getShareToken()
        // }
        return this.getShareToken()
    }

    getShareRefreshToken() {
        // let refreshToken
        // if (localStorage.getItem('share-refreshToken')) {
        //     refreshToken = localStorage.getItem('share-refreshToken')
        // } else {
        //     refreshToken = this.getShareLclRefreshToken()
        // }
        return this.getShareLclRefreshToken()
    }


    getJwtTokenForInterceptor() {
        if (isShareLogin()) {
            return this.getShareJwtToken()
        } else {
            return this.getJwtToken()
        }
    }
    getRefreshTokenForInterceptor() {
        if (isShareLogin()) {
            return this.getShareRefreshToken()
        } else {
            return this.getRefreshToken()
        }
    }

    saveJwtTokenForInterceptor($token) {
        // console.log($token);

        if (isShareLogin()) {
            return this.saveShareJwtToken($token)
        } else {
            return this.saveJwtToken($token)
        }
    }
    saveRefershTokenForInterceptor($refreshToken) {
        if (isShareLogin()) {
            return this.saveShareRefreshToken($refreshToken)
        } else {
            return this.saveRefreshToken($refreshToken)
        }
    }

    saveJwtOrigin($obj) {
        this.setOrigin($obj)
        setTimeout(() => {
            localStorage.setItem('jwtOrigin', $obj);
        }, 0);
    }

    removeTokens() {
        localStorage.removeItem('token')
        localStorage.removeItem('refreshToken')
        this.setToken(null)
        this.setRefreshToken(null)
    }

    removeShareTokens() {
        localStorage.removeItem('share-refreshToken')
        localStorage.removeItem('share-token')
        this.setShareToken(null)
        this.setShareRefreshToken(null)
    }



    async load(d3) {
        // AppComponent.clearStorage()
        const _config: AppApiConfig = await this._http.get('assets/app.settings.json').toPromise() as any
        const {
            MAIN_API_BASE_URL,
            MAIN_API_BASE_EXTERNAL_URL,
            PROVIDER_URL,
            ORIGIN_KEY,
            RATE_REQUEST_V1,
            HM_PAYMENT_POLICY,
            HM_PAYMENT_TERMS,
            NEW_FEATURE,
            BI_CONFIG,
        } = _config
        setBaseApi(MAIN_API_BASE_URL);
        setBaseExternal(MAIN_API_BASE_EXTERNAL_URL)
        setBasePartner(PROVIDER_URL)
        setPortalOrigin(ORIGIN_KEY)
        setRRV1(RATE_REQUEST_V1)
        setHMPaymentPolicy(HM_PAYMENT_POLICY)
        setHMPaymentTerm(HM_PAYMENT_TERMS)
        setNewFeature(NEW_FEATURE)
        setBiConfig(BI_CONFIG)

        if (_config.HELP_DESK_DATA) setBiConfig(_config.HELP_DESK_DATA)

        this._authService.getHashmovePartnerSettings().subscribe((resp: JsonResponse) => {
            try {
                if (resp.returnId > 0) {
                    this._sharedService.isRegistrationRequired = resp.returnObject.isRegistrationRequired
                }
            } catch (error) {
                console.log(error);
            }
        })

        if (!getJwtToken() || !isUserLogin()) {
            this.countryCode = (this._sharedService.getMapLocation()) ? this._sharedService.getMapLocation().countryCode : 'US';
            const guestObject = getDefaultHMUser(this.countryCode)
            guestObject.CountryCode = this.countryCode

            const encObjectL: AESModel = encryptStringAES({ d1: moment(Date.now()).format().substring(0, 16), d2: JSON.stringify(guestObject), d3: (d3) ? d3 : '' })

            return new Promise<void>((resolve, reject) => {
                this._authService.guestLoginService(encObjectL).toPromise().then((response: AESModel) => {
                    // console.log('guest-login-success:', response);
                    const decryptedData = decryptStringAES(response)
                    const { token, refreshToken, reqOrigin } = JSON.parse(decryptedData);
                    this.token = token;
                    this.refreshToken = refreshToken;
                    this.origin = reqOrigin;
                    setTimeout(() => {
                        this.saveJwtToken(token);
                        this.saveRefreshToken(refreshToken);
                        this.saveJwtOrigin(reqOrigin)
                    }, 0);
                    resolve();
                }).catch((err) => {
                    resolve();
                })
            })
        } else {
            GuestService.setSourceVia()
        }
    }

    static setSourceVia() {
        try {
            const { ProviderID } = getLoggedUserData()
            const _profile_id = SharedService.GET_PROFILE_ID()
            SharedService.SET_MAIN_SOURCE_VIA(`${ProviderID}|PROVIDER|${_profile_id}|PARTNER`)
        } catch (error) { }
    }

    generateToken() {

    }

    async shareLogin(d3) {
        return new Promise<void>(async (resolve, reject) => {
            this.countryCode = (this._sharedService.getMapLocation()) ? this._sharedService.getMapLocation().countryCode : 'US';
            const guestObject = getDefaultHMUser(this.countryCode);
            guestObject.CountryCode = this.countryCode;
            const encObjectL: AESModel = encryptStringAES({ d1: moment(Date.now()).format().substring(0, 16), d2: JSON.stringify(guestObject), d3: (d3) ? d3 : '' })
            try {
                const response: AESModel = await this._authService.guestLoginService(encObjectL).toPromise() as any
                const decryptedData = decryptStringAES(response)
                const { token, refreshToken, objectRights, reqOrigin } = JSON.parse(decryptedData);
                this.token = token;
                this.refreshToken = refreshToken;
                this.rightObj = objectRights;
                this.origin = reqOrigin;
                setTimeout(() => {
                    this.saveShareJwtToken(token);
                    this.saveShareRefreshToken(refreshToken);
                    this.saveJwtRightsObj(objectRights)
                    this.saveJwtOrigin(reqOrigin)
                    loading(false)
                    resolve()
                }, 0);
            } catch (error) {
                loading(false)
                resolve()
            }
        })

    }
    async sessionRefresh(d3) {
        return new Promise<void>(async (resolve, reject) => {
            await this._authService.logoutAction()
            this.removeTokens();
            this.countryCode = (this._sharedService.getMapLocation()) ? this._sharedService.getMapLocation().countryCode : 'US';
            const guestObject = getDefaultHMUser(this.countryCode);
            guestObject.CountryCode = this.countryCode;
            const encObjectL: AESModel = encryptStringAES({ d1: moment(Date.now()).format().substring(0, 16), d2: JSON.stringify(guestObject), d3: (d3) ? d3 : '' })
            try {
                const response: AESModel = await this._authService.guestLoginService(encObjectL).toPromise() as any
                const decryptedData = decryptStringAES(response)
                const { token, refreshToken, objectRights, reqOrigin } = JSON.parse(decryptedData);
                this.token = token;
                this.refreshToken = refreshToken;
                this.rightObj = objectRights;
                this.origin = reqOrigin;
                setTimeout(() => {
                    this.saveJwtTokenForInterceptor(token);
                    this.saveRefershTokenForInterceptor(refreshToken);
                    this.saveJwtRightsObj(objectRights)
                    this.saveJwtOrigin(reqOrigin)
                    loading(false)
                    resolve()
                }, 0);
            } catch (error) {
                loading(false)
                resolve()
            }
        })

    }



    setJWTByApi(response: JWTObj) {
        const { token, refreshToken } = response;
        this.token = token;
        this.refreshToken = refreshToken;
        this.saveJwtToken(token);
        this.saveRefreshToken(refreshToken);
    }
}



//fetching and updating tokens functions
export function getJwtToken() {
    return localStorage.getItem('token');
}

export function saveJwtToken(token) {
    localStorage.setItem('token', token);
}

export function saveRefreshToken(refreshToken) {
    localStorage.setItem('refreshToken', refreshToken);
}

export function getRefreshToken() {
    return localStorage.getItem('refreshToken');
}


import * as AesCryrpto from 'aes-js'
import { Buffer } from 'buffer'
import { loading, getLoggedUserData } from "../constants/globalFunctions";
import { isShareLogin } from "../http-interceptors/interceptor";
import { JsonResponse } from "../interfaces/JsonResponse";




export function unpad(padded) {
    return padded.subarray(0, padded.byteLength - padded[padded.byteLength - 1]);
}

const keybytes = AesCryrpto.utils.utf8.toBytes('8080808080808080');



export function encryptStringAES({ d1, d2, d3 }: AESModel): AESModel {
    const iv = AesCryrpto.utils.utf8.toBytes(d1); //Dynamic key from object as d1
    const textBytes = AesCryrpto.utils.utf8.toBytes(d2);
    let encrypted
    try {
        const aesCbc = new AesCryrpto.ModeOfOperation.cbc(keybytes, iv);
        const enc = aesCbc.encrypt(pad(textBytes))
        // const enc = await aes.encrypt(textBytes, keybytes, { name: 'AES-CBC', iv })
        encrypted = Buffer.from(enc).toString('base64')
    } catch (err) { }
    return { d1, d2: encrypted, d3 }
}

export function decryptStringAES({ d1, d2 }: AESModel) {
    const iv = AesCryrpto.utils.utf8.toBytes(d1); //Dynamic key from object as d1
    const encrypted = Buffer.from(d2, 'base64');
    // const decrypted = await aes.decrypt(encrypted, keybytes, { name: 'AES-CBC', iv })
    const aesCbc = new AesCryrpto.ModeOfOperation.cbc(keybytes, iv);
    const decrypted = aesCbc.decrypt(encrypted)
    const unpaddedData = unpad(decrypted)
    const decryptedText = AesCryrpto.utils.utf8.fromBytes(unpaddedData);
    return decryptedText
}

export interface AESModel {
    d1: string;
    d2: string;
    d3: string;
}





export function isUserLogin(): boolean {
    const userInfo = JSON.parse(localStorage.getItem('userInfo'))
    if (!userInfo) {
        return false
    }
    const user = JSON.parse(userInfo.returnText);
    if (!user) {
        return false
    }
    if (user && user.IsLogedOut) {
        return false
    }

    if (user && !user.IsLogedOut) {
        return true
    }
}

export function pad(plaintext) {
    const padding = PADDING[(plaintext.byteLength % 16) || 0];
    const result = new Uint8Array(plaintext.byteLength + padding.length);
    result.set(plaintext);
    result.set(padding, plaintext.byteLength);
    return result;
}

// pre-define the padding values
const PADDING = [
    [16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16],
    [15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15],
    [14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14],
    [13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13],
    [12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12],
    [11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11],
    [10, 10, 10, 10, 10, 10, 10, 10, 10, 10],
    [9, 9, 9, 9, 9, 9, 9, 9, 9],
    [8, 8, 8, 8, 8, 8, 8, 8],
    [7, 7, 7, 7, 7, 7, 7],
    [6, 6, 6, 6, 6, 6],
    [5, 5, 5, 5, 5],
    [4, 4, 4, 4],
    [3, 3, 3],
    [2, 2],
    [1]
];

export interface AppApiConfig {
    MAIN_API_BASE_URL: string
    MAIN_API_BASE_EXTERNAL_URL: string
    PROVIDER_URL: string
    ORIGIN_KEY: string
    RATE_REQUEST_V1: boolean
    HM_PAYMENT_POLICY: boolean
    HM_PAYMENT_TERMS: IHMPaymentTerms
    NEW_FEATURE: INewFeatures,
    BI_CONFIG: IBiConfig
    HELP_DESK_DATA: any
}

export interface IBiConfig {
    BI_HEIGHT: string
    BI_WIDTH: string
}

export interface INewFeatures {
    REPORTS_PERFORMANCE: boolean
    SETTINGS_PAYMENT_TERMS: boolean
    SPOT_RATE: boolean
    REPORT_PERFORMANCE_MSG: string
    SETTINGS_PAYMENT_TERMS_MSG: string
    SPOT_RATE_MSG: string
    ACCOUNTS_INVOICE: boolean
    ACCOUNTS_INVOICE_MSG: string
}

export interface IHMPaymentTerms {
    SEA: string;
    AIR: string;
    TRUCK: string;
    WAREHOUSE: string;
}

export function getDefaultHMUser($countryCode: string) {
    return {
        password: 'h@shMove123',
        loginUserID: 'support@hashmove.com',
        CountryCode: $countryCode ? $countryCode : 'DEFAULT',
        LoginIpAddress: "0.0.0.0",
        LoginDate: moment(Date.now()).format(),
        LoginRemarks: "",
        UserID: 100
    }
}