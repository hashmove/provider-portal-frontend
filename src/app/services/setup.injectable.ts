import { Injectable } from '@angular/core';
import { CommonService } from './common.service';
import { CurrencyControl } from './currency.service';
import { CurrencyDetails, Rate } from '../interfaces/currency.interface';
import { removeDuplicateCurrencies, compareValues } from '../components/pages/user-desk/billing/billing.component';
import { SharedService } from './shared.service';

@Injectable()
export class SetupService {

  constructor(
    private _commonService: CommonService,
    private _currencyControl: CurrencyControl,
    private _sharedService: SharedService
  ) { }


  async setBaseCurrencyConfig(countryObject: Array<CountryObject>, countryCode: string) {

    try {
      let res: any = await this._commonService.getCurrency().toPromise()
      let currencyList: CurrencyDetails[] = res
      currencyList = removeDuplicateCurrencies(currencyList)
      currencyList.sort(compareValues('title', "asc"));
      this._sharedService.currencyList.next(currencyList);
      this._currencyControl.setCurrencyList(currencyList)

      let userData = JSON.parse(localStorage.getItem('loginUser'))

      if (userData && !userData.IsLogedOut && ((userData.CurrencyID && userData.CurrencyID > 0) && (userData.CurrencyOwnCountryID && userData.CurrencyOwnCountryID > 0))) {
        // currData = currencyList.filter(curr => curr.id === userData.CurrencyID && JSON.parse(curr.desc).CountryID === userData.CurrencyOwnCountryID)
        // const currData: CurrencyDetails[]  = currencyList.filter(curr => curr.id === userData.CurrencyID)
        const currData: CurrencyDetails = currencyList.filter(curr => curr.id === userData.CurrencyID)[0]
        // console.log(currData);
        this._currencyControl.setCurrencyID(userData.CurrencyID)
        this._currencyControl.setCurrencyCode(currData.code)
        this._currencyControl.setToCountryID(JSON.parse(currData.desc).CountryID)

        let exchangeRes: any = await this._commonService.getExchangeRateList(this._currencyControl.getBaseCurrencyID()).toPromise()
        this._currencyControl.setExchangeRateList(exchangeRes.returnObject)
        let exchnageRate: Rate = exchangeRes.returnObject.rates.filter(rate => rate.currencyID === this._currencyControl.getCurrencyID())[0]
        this._currencyControl.setExchangeRate(exchnageRate.rate)
        if (!localStorage.getItem('CURR_MASTER')) {
          localStorage.setItem('CURR_MASTER', JSON.stringify(this._currencyControl.getMasterCurrency()))
        }
        let masterCurrency = JSON.parse(localStorage.getItem('CURR_MASTER'))
        this._currencyControl.setMasterCurrency(masterCurrency)
      } else {
        const countryData: CountryObject = countryObject.filter(curr => curr.code.toLowerCase() === countryCode.toLowerCase())[0]
        // console.log(countryData);
        const currData: CurrencyDetails = currencyList.filter(curr => curr.id === countryData.desc[0].CurrencyID)[0]
        // console.log(currData);
        this._currencyControl.setCurrencyID(currData.id)
        this._currencyControl.setCurrencyCode(currData.code)
        this._currencyControl.setToCountryID(JSON.parse(currData.desc).CountryID)

        let exchangeRes: any = await this._commonService.getExchangeRateList(this._currencyControl.getBaseCurrencyID()).toPromise()
        this._currencyControl.setExchangeRateList(exchangeRes.returnObject)
        let exchnageRate: Rate = exchangeRes.returnObject.rates.filter(rate => rate.currencyID === this._currencyControl.getCurrencyID())[0]
        this._currencyControl.setExchangeRate(exchnageRate.rate)
        if (!localStorage.getItem('CURR_MASTER')) {
          localStorage.setItem('CURR_MASTER', JSON.stringify(this._currencyControl.getMasterCurrency()))
        }

        let masterCurrency = JSON.parse(localStorage.getItem('CURR_MASTER'))
        this._currencyControl.setMasterCurrency(masterCurrency)
      }
    } catch (error) {

    }
    return 'done'
  }

}

export interface Desc {
  CurrencyID?: number;
  CountryPhoneCode?: string;
  CountryPhoneCode_OtherLang?: string;
  RegionCode?: string;
  LanguageID?: number;
}

export interface CountryObject {
  code: string;
  desc: Desc[];
  id: number;
  imageName: string;
  isBaseCurrency?: any;
  isChinaGateway: boolean;
  jsonServices?: any;
  lastUpdate?: any;
  latitude?: any;
  longitude?: any;
  portCountryCode?: any;
  roundingOff?: any;
  shortName: string;
  shortPortCode?: any;
  sortingOrder: number;
  title: string;
  type?: any;
  webURL: string;
}