import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { getActiveRoutes } from '../constants/globalFunctions';

@Injectable()
export class UserRouteGuard implements CanActivate {

  constructor(public _router: Router) { }
  activeRoutes = getActiveRoutes()

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const currentUrl: string = state.url
    if (this.activeRoutes.includes(currentUrl)) {
      return true
    } else {
      return false
    }
  }
}
