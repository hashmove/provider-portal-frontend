import { Component, OnInit, ViewEncapsulation, Renderer2, OnDestroy } from '@angular/core'
import { IDefAddCharges, IRateData, IRequestSchedule, JsonContainerDet, JsonSurchargeDet } from './request-add-rate.interface'
import { SeaFreightService } from '../../../components/pages/user-desk/manage-rates/sea-freight/sea-freight.service'
import { ViewBookingService } from '../../../components/pages/user-desk/view-booking/view-booking.service'
import { loading, getLoggedUserData, isArrayValid, changeCase, isMobile } from '../../../constants/globalFunctions'
import { NgbDateStruct, NgbDateParserFormatter, NgbTooltipConfig, } from '@ng-bootstrap/ng-bootstrap'
import { ConfirmDialogGenComponent } from '../confirm-dialog-generic/confirm-dialog-generic.component'
import { DashboardService } from '../../../components/pages/user-desk/dashboard/dashboard.service'
import { ConfirmUpdateDialogComponent } from '../confirm-update/confirm-update-dialog.component'
import { NgbActiveModal, NgbDropdownConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { NgbDateFRParserFormatter } from '../../../constants/ngb-date-parser-formatter'
import { ICarrierSchedule } from '../add-schedule/add-schedule.interface'
import { ICarrierFilter, JsonResponse } from '../../../interfaces'
import { SharedService } from '../../../services/shared.service'
import { CommonService } from '../../../services/common.service'
import { RequestRateHelper } from './request-rate-helper'
import { PlatformLocation } from '@angular/common'
import { ToastrService } from 'ngx-toastr'
import * as moment from 'moment'
import { firstBy } from 'thenby'
import { CurrencyControl } from '../../../services/currency.service'
import { RequestPriceDetailsComponent } from '../../../components/pages/user-desk/request-price-details/request-price-details.component'
import { RequestUnitCalc } from '../request-unit-calc/request-unit-calc.component'
import { Observable } from 'rxjs'
import { IRequestContainers } from '../cargo-details-rate-request/cargo-details-rate-request.interface'
import { hmPaymentPolicy, hmPaymentTerms } from '../../../constants/base.url'
@Component({
  selector: 'app-request-add-rate-dialog',
  templateUrl: './request-add-rate-dialog.component.html',
  providers: [{ provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter }, NgbDropdownConfig],
  styleUrls: ['./request-add-rate-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RequestAddRateDialogComponent extends RequestRateHelper implements OnInit, OnDestroy {

  isMobile = isMobile()
  constructor(
    private location: PlatformLocation,
    private _activeModal: NgbActiveModal,
    private _sharedService: SharedService,
    private _parserFormatter: NgbDateParserFormatter,
    private renderer: Renderer2,
    private _seaFreightService: SeaFreightService,
    private _viewBookingService: ViewBookingService,
    private _dashboardService: DashboardService,
    private _commonService: CommonService,
    private _toast: ToastrService,
    private _modalService: NgbModal,
    private config: NgbDropdownConfig,
    private _currencyControl: CurrencyControl,
    private _tooltipConfig: NgbTooltipConfig
  ) {
    super()
    location.onPopState(() => this.closeModal(null))
    config.autoClose = false
  }

  async ngOnInit() {
    if (this.isMobile)
      this._tooltipConfig.disableTooltip = true
    this.userProfile = getLoggedUserData()
    try {
      this.isEnterprise = this.requestData.rateRequest.Customer.IsEnterpriseCompany
    } catch { }
    this.setDateLimit()
    this.getSearchCriteria()
    await this.getDefAddCharges()
    try {
      const _date = new Date()
      this.minDate = { month: _date.getMonth() + 1, day: _date.getDate(), year: _date.getFullYear() }
    } catch (error) { }
    this.containerLoadParam = this.requestData.forType

    this.getExchangeRateList()
    const { ShippingModeCode, ContainerLoad } = this.requestData.rateRequest
    this.searchMode = ShippingModeCode.toLowerCase() + '-' + ContainerLoad.toLowerCase()
    this.rateType = this.getRateType(this.searchMode)
    setTimeout(() => {
      this.setHMTerms()
      this.getTermsOptions()
    }, 0)
    if (this.searchMode === 'air-lcl' || this.searchMode === 'truck-ftl') {
      this.destinationsList = this.requestData.addList.filter(charge => charge.containerLoadType === this.requestData.rateRequest.ShippingModeCode || charge.containerLoadType === 'COMMON')
      this.originsList = this.requestData.addList.filter(charge => charge.containerLoadType === this.requestData.rateRequest.ShippingModeCode || charge.containerLoadType === 'COMMON')
    } else {
      this.destinationsList = this.requestData.addList.filter(charge => charge.containerLoadType === this.requestData.forType || charge.containerLoadType === 'COMMON')
      this.originsList = this.requestData.addList.filter(charge => charge.containerLoadType === this.requestData.forType || charge.containerLoadType === 'COMMON')
    }
    const _contLoad = this.requestData.rateRequest.ShippingModeCode === 'AIR' ? this.requestData.rateRequest.ShippingModeCode : this.requestData.forType
    this.getSurchargeBasis(_contLoad)
    this.getRequestData()
  }

  setHMTerms() {
    this.showHMPolicy = hmPaymentPolicy
    const _hmPaymentTerms = hmPaymentTerms
    switch (this.requestData.rateRequest.ShippingModeCode) {
      case 'SEA':
        this.hmTerms = _hmPaymentTerms.SEA
        break
      case 'AIR':
        this.hmTerms = _hmPaymentTerms.AIR
        break
      case 'TRUCK':
        this.hmTerms = _hmPaymentTerms.TRUCK
        break
      case 'WAREHOUSE':
        this.hmTerms = _hmPaymentTerms.WAREHOUSE
        break
      default:
        this.hmTerms = _hmPaymentTerms.SEA
        break
    }
  }

  getTermsOptions() {
    this._viewBookingService.getRequestPaymentTerms(
      this.requestData.rateRequest.RequestID, 0, 0
    ).subscribe((res: JsonResponse) => {
      loading(false)
      if (res.returnId > 0)
        this.paymentTermsOptions = res.returnObject.filter(_term => _term.IsSelected)
    }, () => loading(false))
  }

  async getDefAddCharges() {
    const { rateRequest } = this.requestData
    const _toSend = {
      ShippingModeID: rateRequest.ShippingModeID,
      ContainerLoadType: rateRequest.ContainerLoad,
      ProviderID: getLoggedUserData().ProviderID,
      CustomerID: rateRequest.Customer.CustomerID,
      CarrierID: this.getCarrierID(),
      OriginID: rateRequest.Origin ? rateRequest.Origin.PolID : null,
      DestinationID: rateRequest.Destination ? rateRequest.Destination.PodID : null,
      OriginType: rateRequest.Origin ? rateRequest.Origin.PolType : null,
      DestinationType: rateRequest.Destination ? rateRequest.Destination.PodType : null
    }
    try {
      const res = await this._seaFreightService.getDefAddCharges(_toSend).toPromise() as JsonResponse
      if (res.returnId > 0 && isArrayValid(res.returnObject, 0))
        this.defAddCharges = res.returnObject
    } catch { }
  }

  getSearchCriteria() {
    this._viewBookingService.getRequestCriteria(this.requestData.rateRequest.RequestID).subscribe((res: JsonResponse) => {
      if (res.returnId > 0)
        this.searchCriteria = JSON.parse(res.returnObject)
    })
  }

  generateLCLInputs(_rateData: IRateData) {
    this.lclContainers = []
    if (_rateData.jsonCargoDetail && JSON.parse(_rateData.jsonCargoDetail) && isArrayValid(JSON.parse(_rateData.jsonCargoDetail), 0)) {
      JSON.parse(_rateData.jsonCargoDetail)
    } else {
      const { LclChips } = this.searchCriteria
      LclChips.forEach(_input => {
        if (!_input.toggle) {
          this.lclContainers.push({
            quantity: _input.quantity,
            lengthUnitID: _input.lengthUnitID,
            weightUnitID: _input.weightUnitID,
            volumeUnitID: _input.volumeUnitID,
            length: _input.length,
            width: _input.width,
            height: _input.height,
            weight: _input.weight as any,
            volume: _input.volume,
            packageType: _input.packageType,
            contSpecID: _input.contSpecID,
            lengthUnit: _input.lengthUnit,
            weightUnit: _input.weightUnit,
            volumeUnit: 'cbm',
            calculationMethod: 'by_unit',
            contSpecDesc: this.containers.filter(_cont => _cont.ContainerSpecID === _input.contSpecID)[0].ContainerSpecDesc,
            contSpecType: 'Package',
            respondedCBM: 0,
            respondedWeight: 0,
            respondedVolumetricWeight: 0
          })
        } else {
          this.lclContainers.push({
            calculationMethod: 'by_volume',
            quantity: _input.quantity,
            weightUnitID: _input.weightUnitID,
            volumeUnitID: _input.volumeUnitID,
            weight: _input.weight as any,
            volume: _input.volume,
            packageType: _input.packageType,
            contSpecID: _input.contSpecID,
            contSpecDesc: this.containers.filter(_cont => _cont.ContainerSpecID === _input.contSpecID)[0].ContainerSpecDesc,
            contSpecType: 'Package',
            weightUnit: _input.weightUnit,
            volumeUnit: 'cbm',
            respondedCBM: 0,
            respondedWeight: 0,
            respondedVolumetricWeight: 0,
          })
        }
      })
    }
  }

  getRateType(_searchMode) {
    switch (_searchMode) {
      case 'sea-fcl':
        return 'Container'
      case 'sea-lcl':
        return 'CBM'
      case 'air-lcl':
        return 'KG'
      case 'truck-ftl':
        return 'Truck'
      default:
        return 'Container'
    }
  }

  getExchangeRateList() {
    this._commonService.getExchangeRateList(JSON.parse(localStorage.getItem('CURR_MASTER')).fromCurrencyID).subscribe((_exRate: JsonResponse) => {
      if (_exRate.returnId > 0)
        this.exchangeRateList = _exRate.returnObject.rates
    })
  }

  async getRequestData() {
    if (this.requestData.rateRequest.Carrier)
      this.originalCarrierID = this.requestData.rateRequest.Carrier.CarrierID
    this.commodityDesc = this.requestData.rateRequest.CommodityDescription ? this.requestData.rateRequest.CommodityDescription : 'N/A'
    this._sharedService.currencyList.subscribe((state: any) => {
      if (state) {
        this.currencyList = state
        this.selectedCurrency = this.currencyList.filter(e => e.id === this.getCurrency())[0]
      }
    })
    this.getAllCarriers()
    if (this.requestData.rateRequest.ShippingModeCode === 'SEA' || this.requestData.rateRequest.ShippingModeCode === 'AIR')
      await this.setPortsData(this.requestData.rateRequest.ShippingModeCode)
    if (this.searchMode !== 'air-lcl' && this.searchMode !== 'warehouse-lcl')
      this.getContainerDetails()
    else {
      const _selectedCurrency = this.currencyList.filter(e => e.id === this.getCurrency())[0]
      this.inputContainers.push({
        container: null,
        price: null,
        doublePrice: null,
        reqQty: null,
        resQty: null,
        bookedQty: 0,
        orginCharges: [{ isValid: true, currency: _selectedCurrency, currId: _selectedCurrency.id }] as any,
        destinCharges: [{ isValid: true, currency: _selectedCurrency, currId: _selectedCurrency.id }] as any,
        currency: _selectedCurrency,
        currencyID: _selectedCurrency.id,
        basePrice: 0,
        contIndexID: 0 + '' + new Date().getTime() + 0,
        isValidPrice: true,
        isValidQty: true,
        isValidCurr: true
      })
    }
    if (this.searchMode === 'air-lcl') {
      this.respondedCBM = this._currencyControl.applyRoundByDecimal(this.requestData.rateRequest.AirDet.ChargeableWeight, 3)
      this.requestedCBM = this._currencyControl.applyRoundByDecimal(this.requestData.rateRequest.AirDet.ChargeableWeight, 3)
      this.setRateData()
    }
  }

  getVesselSchedule() {
    if (this.requestData.rateRequest.EtdUtc) {
      this._viewBookingService.getBookingRouteDetail(this.getBookingKey(this.requestData.rateRequest)).subscribe((res: JsonResponse) => {
        this.vesselSchedule = (res.returnId > 0) ? res.returnObject : null
      })
    }
  }

  async getAllCarriers() {
    try {
      if (localStorage.getItem('carriersList')) {
        const _shippingLines: ICarrierFilter[] = JSON.parse(localStorage.getItem('carriersList')).filter(e => e.type === this.requestData.rateRequest.ShippingModeCode)
        const _allShippingLines = _shippingLines.filter(_ship => _ship.code.toLowerCase() !== 'default')
        const _otherCarrier = _shippingLines.filter(_ship => _ship.code.toLowerCase() === 'default')[0]
        _allShippingLines.push({ ..._otherCarrier })
        this.allShippingLines = _allShippingLines
      } else {
        const _res: any[] = await this._viewBookingService.getAllCarriers().toPromise() as any
        localStorage.setItem('carriersList', JSON.stringify(_res))
        const _allShippingLines = _res.filter(e => e.type === this.requestData.rateRequest.ShippingModeCode && e.code.toLowerCase() !== 'default')
        const _otherCarrier = _res.filter(e => e.type === this.requestData.rateRequest.ShippingModeCode && e.code.toLowerCase() === 'default')[0]
        _allShippingLines.push({ ..._otherCarrier })
        this.allShippingLines = _allShippingLines
      }
      if (this.requestData.forType !== 'FTL') {
        if (this.requestData.rateRequest.Carrier && this.requestData.rateRequest.Carrier.CarrierID)
          this.selectedShipping = this.allShippingLines.filter(obj => obj.id === this.requestData.rateRequest.Carrier.CarrierID)[0]
      }
    } catch (error) { console.log(error) }
  }

  getContainerDetails() {
    this.loading = true
    const { rateRequest } = this.requestData
    const _bookKey = this.getBookingKey(rateRequest)
    if (rateRequest.ResponseID) {
      Observable.forkJoin([
        this._dashboardService.getRequestContainer(_bookKey, 0, 'PROVIDER'),
        this._dashboardService.getRequestContainer(_bookKey, rateRequest.ResponseID, 'PROVIDER')
      ]).subscribe((res: JsonResponse[]) => {
        const [reqResp, resRes] = res
        if (reqResp.returnId > 0) {
          const reqConts: IRequestContainers[] = isArrayValid(reqResp.returnObject, 0) ? reqResp.returnObject : []
          const resConts: IRequestContainers[] = isArrayValid(resRes.returnObject, 0) ? resRes.returnObject : []
          const newFinnContainer = []
          reqConts.forEach(reqCont => {
            try {
              const resCont = resConts.filter(_cont => _cont.ContainerSpecID === reqCont.ContainerSpecID)[0]
              newFinnContainer.push({ ...reqCont, ...resCont })
            } catch {
              newFinnContainer.push({ ...reqCont })
            }
          })
          this.setContainers(newFinnContainer)
        }
      })
    } else {
      this._dashboardService.getRequestContainer(_bookKey, 0, 'PROVIDER').subscribe((res: JsonResponse) => {
        if (res.returnId > 0)
          this.setContainers(res.returnObject)
      })
    }
  }

  setContainers(_containers: IRequestContainers[]) {
    try {
      this.containers = _containers.sort(firstBy(function (v1, v2) { return v1.SortingOrder - v2.SortingOrder }))
    } catch {
      this.containers = _containers
    }
    if (this.containers && this.containers.length > 0) {
      this.containers.forEach((_container, _index) => {
        const _selectedCurrency = this.currencyList.find(e => e.id === this.getCurrency())
        const _reqQty = this.searchMode === 'sea-lcl' ? _container.RequestedCBM : _container.RequestedQty
        const _resQty = this.searchMode === 'sea-lcl' ? _container.RespondedCBM : _container.RespondedQty ? _container.RespondedQty : null
        const _bookedQty = this.searchMode === 'sea-fcl' && _container.BookedQty && _container.BookedQty > 0 ? _container.BookedQty : 0
        this.inputContainers.push({
          container: _container,
          price: null,
          doublePrice: null,
          reqQty: _reqQty, // Requested Line
          resQty: _resQty ? _resQty : _reqQty, // RespondedLine
          bookedQty: _bookedQty,
          orginCharges: [{ currency: _selectedCurrency, isValid: true }] as any,
          destinCharges: [{ currency: _selectedCurrency, isValid: true }] as any,
          currency: _selectedCurrency,
          currencyID: _selectedCurrency.id,
          basePrice: 0,
          contIndexID: _container.ContainerSpecID + '' + new Date().getTime() + _index,
          isValidPrice: true,
          isValidQty: true,
          isValidCurr: true
        })
        if (this.searchMode === 'sea-lcl') {
          this.requestedCBM += _container.RequestedCBM
          this.respondedCBM += _container.RequestedCBM
        }
      })
      try {
        if (this.searchMode === 'sea-lcl') {
          this.requestedCBM = this._currencyControl.applyRoundByDecimal(this.requestedCBM, 3)
          this.respondedCBM = this._currencyControl.applyRoundByDecimal(this.respondedCBM, 3)
        }
      } catch { }
      this.setRateData()
    } else {
      this.setRateData()
    }
    setTimeout(() => { this.loading = false }, 0)
  }

  closeModal = (_state) => this._activeModal.close(_state)

  spaceHandler(event) {
    if (event.charCode === 32) {
      event.preventDefault()
      return false
    }
  }

  async submitPrice(action: string) {
    this.validateInputs()
    try {
      const _pricingJSON = this.getRateObject(this._currencyControl)
      if (_pricingJSON.length === 0)
        return
      const [_totalAmnt, _totalBaseAmount] = this.getTotalPrice(_pricingJSON, this._currencyControl)
      if (!_totalAmnt || _totalAmnt <= 0) {
        this._toast.warning('Total charges cannot be zero')
        return
      }
    } catch { }

    if (this.fromDate && this.fromDate.day && (!this.toDate || !this.toDate.day)) {
      this._toast.warning('Please enter a valid date range for this rate')
      return
    }

    if (this.searchMode === 'sea-fcl' || this.searchMode === 'sea-lcl') {
      if (this.requestData.rateRequest.Origin.PolType === 'GROUND' && (!this.filterOrigin || !this.filterOrigin.id)) {
        this._toast.warning('Please enter origin port.')
        return
      }
      if (this.requestData.rateRequest.Destination.PodType === 'GROUND' && (!this.filterDestination || !this.filterDestination.id)) {
        this._toast.warning('Please enter destination port.')
        return
      }
    }

    if (this.searchMode !== 'truck-ftl') {
      if (!this.filterOrigin || !this.filterOrigin.id) {
        this._toast.warning(`Please enter origin ${this.searchMode.includes('sea') ? 'port' : 'airport'}.`)
        return
      }
      if (!this.filterDestination || !this.filterDestination.id) {
        this._toast.warning(`Please enter destination ${this.searchMode.includes('sea') ? 'port' : 'airport'}.`)
        return
      }
    }

    if (this.fromDate && this.fromDate.day && (!this.toDate || !this.toDate.day)) {
      this._toast.warning('Please enter a valid date range for this rate')
      return
    }

    if (!this.fromDate || !this.fromDate.day) {
      this._toast.warning('Please select a Valid from Date', 'Rate Validity Error')
      return
    }
    if (!this.toDate || !this.toDate.day) {
      this._toast.warning('Please select a Valid to Date', 'Rate Validity Error')
      return
    }

    if (this.hasScheduleChanged && (this.schdeuleHasData() || this.selectedStops !== 'direct')) {
      if (!this.departureDate || !this.departureDate.day) {
        this._toast.warning('Please select a Departure Date for Schedule', 'Vessel Schedule')
        return
      }
      if (!this.cutOffDate || !this.cutOffDate.day) {
        this._toast.warning('Please select a Cut Off Date for Schedule', 'Vessel Schedule')
        return
      }
      if (!this.selectionDuration) {
        this._toast.warning('Please enter valid Transit days', 'Vessel Schedule')
        return
      }
      const _transferStops = this.getTransferStop()
      if (_transferStops > 0) {
        if (!this.t1Route || !this.t1Route.id) {
          this._toast.warning('Please select a Route for the 1st Transit', 'Vessel Schedule')
          return
        }
      }
      if (_transferStops > 1) {
        if (!this.t2Route || !this.t2Route.id) {
          this._toast.warning('Please select a Route for the 2nd Transit', 'Vessel Schedule')
          return
        }
      }
    }

    if (!this.hasRateChange()) {
      this._toast.warning('No Changes has been made to this Quote', 'No Changes')
      return
    }

    const _saveObject = await this.getSaveObject()
    if (!_saveObject)
      return

    this.loading = true
    this.isRateUpdating = true
    const modalRef = this._modalService.open(ConfirmUpdateDialogComponent, this.isMobile ? this.x_smallModal : this.smallModal)
    const { actualIndividualPrice } = this
    modalRef.componentInstance.amount = (actualIndividualPrice <= 0) ? actualIndividualPrice * (-1) : actualIndividualPrice
    modalRef.componentInstance.code = this.getCurrCodeForTotal(this.inputContainers)
    modalRef.componentInstance.isSpotRate = true
    modalRef.componentInstance.data = this.requestData.rateRequest
    modalRef.result.then((result) => {
      setTimeout(() => { document.getElementsByTagName('html')[0].style.overflowY = 'auto !important' }, 0)
      if (result)
        this.updatePriceAction(this.userProfile.UserID, _saveObject, action)
      else
        this.loading = false
    })
    modalRef.result.catch(() => { })
      .then(() => { if (document.querySelector('body > .modal')) { document.body.classList.add('modal-open') } })
  }

  getString = (_str) => _str && typeof _str === 'string' ? (_str.toLowerCase() as any).trimRight().trimLeft() : ''

  hasRateChange() {
    if ((this.searchMode === 'sea-lcl' || this.searchMode === 'sea-fcl') && this.hasScheduleChanged)
      return true
    else if (this.searchMode !== 'truck-ftl' && this.hasCarrierChanged)
      return true
    else if (this.hasPriceChanged || this.isRouteDetailUpdate)
      return true
    else if (this.hasRateTCChanged && this.rateData && this.getString(this.rateData.specialRequestComments) !== this.getString(this.specialRequestComments))
      return true
    else if (this.rateData && this.rateData.effectiveFrom) {
      const _savedDateFrom = moment(this.rateData.effectiveFrom).format('YYYY-MM-DD')
      const _effectFrom = this.getDatefromObj(this.fromDate)
      const _savedDateTo = moment(this.rateData.effectiveTo).format('YYYY-MM-DD')
      const _effectTo = this.getDatefromObj(this.toDate)

      if (_savedDateFrom !== _effectFrom)
        return true
      else if (_savedDateTo !== _effectTo)
        return true
    } else {
      return false
    }
  }

  getRateObject(_currUtil: CurrencyControl): JsonContainerDet[] {
    const objDraft: JsonContainerDet[] = []
    if (!this.validateAdditionalCharges(this._toast))
      return []
    try {
      const { inputContainers, exchangeRateList } = this
      const _perUnitPrice = inputContainers[0].price
      const _isMulti = this.inputContainers.length > 1 ? true : false
      for (let index = 0; index < inputContainers.length; index++) {
        const _inptCont = inputContainers[index]
        const { price, resQty, container, orginCharges, destinCharges } = _inptCont
        if (!_inptCont.currency || !_inptCont.currency.id) {
          if (this.searchMode === 'sea-lcl' || this.searchMode === 'air-lcl') {
            this._toast.error(`Please select currency for ${_inptCont.container.ContainerSpecDesc}`, 'Error')
          } else {
            this._toast.error(`Please select currency in order to proceed`, 'Error')
          }
          this.isRateUpdating = false
          this.loading = false
          _inptCont.isValidCurr = false
          return []
        } else {
          _inptCont.isValidCurr = true
        }
        const _originCharge: JsonSurchargeDet[] = isArrayValid(orginCharges, 0) ? orginCharges.filter(_charge => _charge.price) : []
        const _destCharge: JsonSurchargeDet[] = isArrayValid(destinCharges, 0) ? destinCharges.filter(_charge => _charge.price) : []
        const _jsonSurchargeDet: JsonSurchargeDet[] = _originCharge.concat(_destCharge)

        if (this.searchMode === 'sea-lcl' || this.searchMode === 'air-lcl') {
          if (_perUnitPrice === null || _perUnitPrice === undefined || !(typeof parseFloat(_perUnitPrice as any) === 'number')) {
            this._toast.error('Price cannot be zero', 'Error')
            this.isRateUpdating = false
            _inptCont.isValidPrice = false
            this.loading = false
            return
          } else {
            _inptCont.isValidPrice = true
          }
        } else {
          if (this.getNum(_inptCont.bookedQty) && this.getNum(_inptCont.resQty) >
            (this.getNum(_inptCont.reqQty) - this.getNum(_inptCont.bookedQty))
          ) {
            const _remCont = _inptCont.reqQty - _inptCont.bookedQty
            const _msg = `Your quote is for ${_inptCont.resQty} containers while remaining ${_remCont > 1 ? 'containers' : 'container'} in
            request ${_remCont > 1 ? 'are' : 'is'} ${_remCont} for ${_inptCont.container.ContainerSpecDesc}. Please correct the container quantity in quote`
            this._toast.error(_msg, 'Error')
            this.isRateUpdating = false
            this.loading = false
            _inptCont.isValidQty = false
            return []
          } else if (_inptCont.resQty && this.getNum(_inptCont.resQty) > this.getNum(_inptCont.reqQty)) {
            this._toast.error(`Entered quantity cannot be greater than requested Quantity (${_inptCont.reqQty}) for ${_inptCont.container.ContainerSpecDesc}`, 'Error')
            this.isRateUpdating = false
            this.loading = false
            _inptCont.isValidQty = false
            return []
          } else {
            _inptCont.isValidQty = true
          }
          if (!_isMulti && (!_inptCont.resQty || this.getNum(_inptCont.resQty) === 0)) {
            this._toast.error(`Entered quantity of ${_inptCont.container.ContainerSpecDesc} cannot be 0`, 'Error')
            this.isRateUpdating = false
            this.loading = false
            _inptCont.isValidQty = false
            return []
          } else {
            _inptCont.isValidQty = true
          }
          if (_isMulti) {
            const _qty: number = this.getNum(_inptCont.resQty)
            const _fsurPrice: number = this.getNum(_inptCont.price)
            const _addCharges: number = isArrayValid(_jsonSurchargeDet, 0) ? _jsonSurchargeDet.map(_charge => this.getNum(_charge.price)).reduce((all, item) => (all + item)) : 0
            const _totalRate = _fsurPrice + _addCharges
            if (_qty > 0 && (_totalRate === 0)) {
              this._toast.error(`Total price of ${_inptCont.container.ContainerSpecDesc} cannot be 0, if quantity is selected`, 'Error')
              this.isRateUpdating = false
              this.loading = false
              _inptCont.isValidPrice = false
              return []
            } else {
              _inptCont.isValidPrice = true
            }
            if (_qty === 0 && (_totalRate > 0)) {
              this._toast.error(`Entered quantity of ${_inptCont.container.ContainerSpecDesc} cannot be 0, if price is entered`, 'Error')
              this.isRateUpdating = false
              this.loading = false
              _inptCont.isValidQty = false
              return []
            } else {
              _inptCont.isValidQty = true
            }
          }
        }

        let _surChargeFinn: JsonSurchargeDet[] = []
        if (isArrayValid(_jsonSurchargeDet, 0)) {
          _surChargeFinn = _jsonSurchargeDet.filter(_chg => _chg.price && parseFloat(_chg.price))
            .map(_chg => {
              const __exRate = exchangeRateList.filter(rate => rate.currencyID === _chg.currency.id)[0]
              return {
                ..._chg,
                currId: _chg.currency.id,
                basePrice: _currUtil.getPriceToBase(parseFloat(_chg.price), false, __exRate.rate),
                exchangeRate: __exRate.rate,
              }
            })
        }

        const _exchangeRate = exchangeRateList.filter(rate => rate.currencyID === _inptCont.currency.id)[0]
        const _requestQty = this.searchMode === 'sea-lcl' ? container.RequestedCBM : this.searchMode === 'air-lcl' ? this.requestedCBM : container.RequestedQty
        const _respondedQty = this.searchMode === 'sea-lcl' || this.searchMode === 'air-lcl' ? _requestQty : resQty
        objDraft.push({
          containerData: this.searchMode !== 'air-lcl' ? _inptCont.container : null,
          price: this.searchMode === 'sea-lcl' ? _perUnitPrice : price,
          basePrice: _currUtil.getPriceToBase(parseFloat(price ? price : 0), false, _exchangeRate.rate),
          containerSpecID: this.searchMode !== 'air-lcl' ? container.ContainerSpecID : null,
          currencyID: _inptCont.currency.id,
          exchangeRate: _exchangeRate ? _exchangeRate.rate : null,
          requestedQty: _requestQty,
          respondedQty: _respondedQty ? _respondedQty : _requestQty,
          resQty: _respondedQty,
          jsonSurchargeDet: isArrayValid(_surChargeFinn, 0) ? _surChargeFinn : null,
          contIndexID: _inptCont.contIndexID ? _inptCont.contIndexID : null
        })
      }
      try {
        const _defChIds = this.defAddCharges.filter(_ch => _ch.IsMandatory).map(_ch => _ch.AddChrID)
        if (isArrayValid(_defChIds, 0) && !this.requestData.rateRequest.ResponseID) {
          let _rateCharges: number[] = []
          objDraft.filter(_price => this.getNum(_price.resQty) > 0).forEach(_price => {
            const _filCh = _price.jsonSurchargeDet.map(_ch => _ch.addChrID)
            _rateCharges = [..._rateCharges, ..._filCh]
          })
          const _cmbChgs = Array.from(new Set(_rateCharges.filter(_val => _defChIds.includes(_val))))
          if (!(isArrayValid(_cmbChgs, 0) && _cmbChgs.length === _defChIds.length)) {
            const _reqCharges = this.defAddCharges
              .filter(_ch => !_rateCharges.includes(_ch.AddChrID))
              .filter(_ch => _defChIds.includes(_ch.AddChrID)).map(_ch => _ch.AddChrName).join(', ')
            this._toast.error(`Please add: ${_reqCharges} in order to continue.`, 'Mandatory Charges missing')
            return []
          }
        }
      } catch (error) {
        console.log('manCharges_error:', error)
      }
    } catch {
      return []
    }
    return objDraft
  }

  async getSaveObject() {
    const { rateRequest } = this.requestData
    const _pricingJSON = this.getRateObject(this._currencyControl)
    if (_pricingJSON.length === 0) {
      return
    }
    if ((this.searchMode === 'sea-fcl' || this.searchMode === 'truck-ftl') && _pricingJSON.length < this.containers.length)
      return
    else if (_pricingJSON.length === 0)
      return
    return this.generateSaveObject(rateRequest, _pricingJSON, this._currencyControl, this._toast)
  }

  async updatePriceAction(UserID, $obj, action: string) {
    this._dashboardService.updateSpecialPriceV2(this.requestData.rateRequest.RequestID, UserID, $obj).subscribe((res: JsonResponse) => {
      this.loading = false
      loading(false)
      if (res.returnId > 0) {
        let _response = null
        try { _response = JSON.parse(res.returnText) } catch { }
        let obj: any = {
          price: this.actualIndividualPrice,
          currencyID: this.selectedCurrency.id ? this.selectedCurrency.id : this.requestData.rateRequest.CurrencyID,
          currencyCode: this.selectedCurrency.code ? this.selectedCurrency.code : this.requestData.rateRequest.CurrencyCode,
          isScheduleUpdate: true,
          status: _response && _response.SpecialRequestStatus ? _response.SpecialRequestStatus : 'PRICE SENT',
          statusBl: _response && _response.SpecialRequestStatusBL ? _response.SpecialRequestStatusBL : 'PRICE_SENT',
          logs: _response && _response.JSONSpecialRequestLogs ? JSON.parse(_response.JSONSpecialRequestLogs) : null
        }

        if (this.selectedShipping && this.selectedShipping.id) {
          obj = {
            ...obj, carrierId: this.selectedShipping.id, carrierName: this.getCarrierName(this.selectedShipping), carrierImage: this.selectedShipping.imageName
          }
        }
        this._toast.success(res.returnText, res.returnStatus)
        if (action === 'saveNadd')
          this.responseSent += 1
        else
          this.closeModal(1)
      } else {
        if (res.returnCode === '2') {
          this._toast.info(res.returnText, res.returnStatus)
        } else {
          this._toast.error(res.returnText, res.returnStatus)
        }
      }
    }, () => {
      loading(false)
      this.loading = false
      this.isRouteDetailUpdate = false
    })
  }

  getCarrierName($carrier: ICarrierFilter) {
    const { type, shortName, title } = $carrier
    try {
      const _carrierName = $carrier.title.split(',')[1].replace(' ', '')
      return _carrierName && _carrierName.length > 0 ? _carrierName : type === 'SEA' ? shortName : title
    } catch {
      return type === 'SEA' ? shortName : title
    }
  }

  async setPortsData(_shipMode) {
    const _ports: Array<ICarrierFilter> = (localStorage.getItem('shippingPortDetails')) ? JSON.parse(localStorage.getItem('shippingPortDetails')) : []
    const _portType = (_shipMode.toLowerCase() === 'truck') ? 'sea' : _shipMode.toLowerCase()
    const filteredPorts: Array<ICarrierFilter> = _ports.filter(port => port.type.toLowerCase().includes(_portType))
    if (isArrayValid(filteredPorts, 0)) {
      this.setPorts(this.requestData, filteredPorts)
    } else {
      try {
        const newPorts = _ports.concat(await this._commonService.getPortsDataV2(_portType.toUpperCase()).toPromise() as any)
        const _filteredPorts: Array<ICarrierFilter> = newPorts.filter(port => port.type.toLowerCase().includes(_portType))
        this.setPorts(this.requestData, _filteredPorts)
        localStorage.setItem('shippingPortDetails', JSON.stringify(newPorts))
      } catch { }
    }
  }

  setDefSpecDesc = () =>
    this.specialReqDesc = this.requestData.rateRequest.BookingDesc ? this.requestData.rateRequest.BookingDesc : 'N/A'

  setVesselSchdeule(_schedule: ICarrierSchedule) {
    try {
      this.requestData.rateRequest.Carrier.CarrierID = _schedule.carrierID
      this.requestData.rateRequest.Carrier.CarrierName = _schedule.carrierName
      this.requestData.rateRequest.Carrier.CarrierImage = _schedule.carrierImage
    } catch { }
  }

  removeVesselScheduleData() {
    try {
      this.requestData.rateRequest.EtdUtc = null
      this.requestData.rateRequest.EtdUtc = null
      this.requestData.rateRequest.EtaUtc = null
      this.requestData.rateRequest.EtaUtc = null
    } catch { }
  }

  removeVesselSchedule() {
    try {
      const modalRef = this._modalService.open(ConfirmDialogGenComponent, this.scheduleModal)
      modalRef.componentInstance.modalData = this.deletScheduleMsg
      modalRef.result.then(result => {
        if (result) {
          this.schdeuleStatus = 'REMOVED'
          this.vesselSchedule = null
        }
      })
    } catch { }
  }

  async saveScheduleToBooking() {
    try {
      const _res: JsonResponse = await this._viewBookingService.saveCarrierSchedule(this.vesselSchedule).toPromise() as any
      return (_res.returnId > 0) ? _res.returnObject : null
    } catch { return null }
  }

  async removeSchCall() {
    try {
      const _res: JsonResponse = await this._viewBookingService.removeCarrierScheduleByBooking(this.requestData.rateRequest.RequestID, getLoggedUserData().UserID).toPromise() as any
      return (_res.returnId > 0) ? true : false
    } catch { return false }
  }

  formatter = (_carr) => (_carr.code === 'DEFAULT' ? _carr.shortName : `${_carr.code}, ${_carr.title}`)

  setRateData() {
    if (this.requestData.rateRequest.ResponseID) {
      this._viewBookingService.getResponseData(this.requestData.rateRequest.ResponseID ? this.requestData.rateRequest.ResponseID : 0).subscribe((res: JsonResponse) => {
        if (res.returnId > 0) {
          const _rateData: IRateData = changeCase(res.returnObject, 'camel')
          this.rateData = _rateData

          const _effFrom = new Date(_rateData.effectiveFrom)
          this.fromDate = { year: _effFrom.getFullYear(), month: _effFrom.getMonth() + 1, day: _effFrom.getDate() }
          const _effTo = new Date(_rateData.effectiveTo)
          try { this.specialReqDesc = _rateData.customerSpecialRequestComments ? _rateData.customerSpecialRequestComments : 'N/A' } catch { }
          this.toDate = { year: _effTo.getFullYear(), month: _effTo.getMonth() + 1, day: _effTo.getDate() }

          if (_rateData.specialRequestComments)
            this.specialRequestComments = _rateData.specialRequestComments

          try {
            const _vesselSchedule: IRequestSchedule = JSON.parse(_rateData.jsonTransferVesselsDetails)
            if (_vesselSchedule) {
              this.vesselSchedule = _vesselSchedule
              const _depDate = new Date(_vesselSchedule.DepartureDate)
              this.selectionDuration = _vesselSchedule.TransitDays
              this.departureDate = { year: _depDate.getFullYear(), month: _depDate.getMonth() + 1, day: _depDate.getDate() }
              const _arrivalDate = new Date(this.getArrivalDate())
              this.arrivalDate = { year: _arrivalDate.getFullYear(), month: _arrivalDate.getMonth() + 1, day: _arrivalDate.getDate() }

              this.selectedStops = this.getTransferStopStr(_vesselSchedule.TransferStops)
              const _cutOff = new Date(_vesselSchedule.CutOffDate)
              this.cutOffDate = { year: _cutOff.getFullYear(), month: _cutOff.getMonth() + 1, day: _cutOff.getDate() }
              const _currDate = new Date()
              this.cutOffMinDate = { year: _currDate.getFullYear(), month: _currDate.getMonth() + 1, day: _currDate.getDate() }

              this.t1Route = this.allPorts.filter(_port => _port.id === _vesselSchedule.RouteDetails.R2PolID)[0]
              try {
                const _t1Arr = new Date(_vesselSchedule.RouteDetails.R2EtaDate)
                this.t1Arrival = { year: _t1Arr.getFullYear(), month: _t1Arr.getMonth() + 1, day: _t1Arr.getDate() }
                const _t1Dep = new Date(_vesselSchedule.RouteDetails.R2EtdDate)
                this.t1Departure = { year: _t1Dep.getFullYear(), month: _t1Dep.getMonth() + 1, day: _t1Dep.getDate() }
              } catch { }
              this.t2Route = this.allPorts.filter(_port => _port.id === _vesselSchedule.RouteDetails.R3PolID)[0]
              try {
                const _t2Arr = new Date(_vesselSchedule.RouteDetails.R3EtaDate)
                this.t2Arrival = { year: _t2Arr.getFullYear(), month: _t2Arr.getMonth() + 1, day: _t2Arr.getDate() }
                const _t2Dep = new Date(_vesselSchedule.RouteDetails.R3EtdDate)
                this.t2Departure = { year: _t2Dep.getFullYear(), month: _t2Dep.getMonth() + 1, day: _t2Dep.getDate() }
              } catch { }
            } else {
              this.vesselSchedule = null
            }
          } catch (error) {
            this.vesselSchedule = null
          }
          this.respondedCBM = 0
          this.requestedCBM = 0

          for (let index = 0; index < this.inputContainers.length; index++) {
            try {
              const _container = this.inputContainers[index]
              if (this.searchMode === 'sea-fcl' || this.searchMode === 'truck-ftl') {
                const _jsonContainers: JsonContainerDet[] = JSON.parse(_rateData.jsonContainerDet as any)
                const _jsonContainerDet: JsonContainerDet = _jsonContainers.find(_cont => _cont.containerSpecID === _container.container.ContainerSpecID)
                this.setJsonContainerDet(_container, _jsonContainerDet)
              } else {
                const _jsonContainerDet: JsonContainerDet = JSON.parse(_rateData.jsonContainerDet as any)[index]
                this.setJsonContainerDet(_container, _jsonContainerDet)
              }
            } catch { }
          }
          if (this.searchMode === 'sea-lcl') {
            this.requestedCBM = this._currencyControl.applyRoundByDecimal(this.requestedCBM, 3)
            this.respondedCBM = this._currencyControl.applyRoundByDecimal(this.respondedCBM, 3)
            if (this.respondedCBM < this.requestedCBM)
              this.respondedCBM = this.requestedCBM
          }
          this.setDefaultCharges()
        }
      })
    } else {
      this.setDefaultCharges()
    }
  }

  setDefaultCharges() {
    try {
      const singleCharges = this.defAddCharges.filter(_ch => !this.chargeBasis.includes(_ch.AddChrBasis))
      const multiCharges = this.defAddCharges.filter(_ch => this.chargeBasis.includes(_ch.AddChrBasis))
      if (isArrayValid(multiCharges, 0) && (this.searchMode === 'sea-fcl' || this.searchMode === 'truck-ftl')) {
        this.addDefCharges(singleCharges, 0)
        for (let index = 0; index < this.inputContainers.length; index++) {
          this.addDefCharges(multiCharges, index)
        }
      } else {
        this.addDefCharges(this.defAddCharges, 0)
      }
    } catch (error) {
      console.log(error)
    }
  }

  addDefCharges(_defAddCharges: IDefAddCharges[], _index) {
    let hasAdded = false
    const _originCharges = this.inputContainers[_index].orginCharges
    const _chargesID = _originCharges.filter(_charge => _charge.addChrID).map(_charge => _charge.addChrID)
    const _selectedCurrency = this.currencyList.find(e => e.id === this.getCurrency())
    _defAddCharges.forEach(_charge => {
      if (!_chargesID.includes(_charge.AddChrID) && (!this.rateData || !this.rateData.requestID)) {
        const _orgCharge = this.originsList.filter(_ch => _ch.addChrID === _charge.AddChrID)[0]
        this.inputContainers[_index].orginCharges.push({
          ..._orgCharge,
          imp_Exp: 'EXPORT',
          IsMandatory: _charge.IsMandatory,
          isValid: true,
          currency: _selectedCurrency,
          currId: _selectedCurrency.id
        })
        hasAdded = true
      } else {
        this.inputContainers[_index].orginCharges.forEach(_ch => {
          if (_ch.addChrID === _charge.AddChrID)
            _ch.IsMandatory = _charge.IsMandatory
        })
      }
    })
    if (hasAdded)
      this.inputContainers[_index].orginCharges = this.inputContainers[_index].orginCharges.filter(_ch => _ch.addChrID)
  }

  setJsonContainerDet(_container, _jsonContainerDet) {
    if (_jsonContainerDet) {
      const _contCurr = this.currencyList.find(e => e.id === (_jsonContainerDet.currencyID ? _jsonContainerDet.currencyID : this.getCurrency()))
      _container.price = _jsonContainerDet.price
      _container.basePrice = _jsonContainerDet.basePrice
      _container.resQty = _jsonContainerDet.respondedQty
      _container.reqQty = _jsonContainerDet.requestedQty
      _container.currency = _contCurr

      if (this.searchMode === 'sea-lcl' || this.searchMode === 'air-lcl') {
        try {
          this.requestedCBM += this.getNum(_jsonContainerDet.respondedQty)
          this.respondedCBM += this.getNum(_jsonContainerDet.requestedQty)
        } catch { }
      }

      try {
        if (_jsonContainerDet.jsonSurchargeDet.filter(_charge => _charge.imp_Exp === 'EXPORT').length > 0) {
          _container.orginCharges = _jsonContainerDet.jsonSurchargeDet.filter(_charge => _charge.imp_Exp === 'EXPORT').map(_charge => {
            const _selectedCurrency = this.currencyList.find(e => e.id === (_charge.currId ? _charge.currId : this.getCurrency()))
            return { ..._charge, currency: _selectedCurrency, isValid: true }
          })
        }
      } catch { }
      try {
        if (_jsonContainerDet.jsonSurchargeDet.filter(_charge => _charge.imp_Exp === 'IMPORT').length > 0) {
          _container.destinCharges = _jsonContainerDet.jsonSurchargeDet.filter(_charge => _charge.imp_Exp === 'IMPORT').map(_charge => {
            const _selectedCurrency = this.currencyList.find(e => e.id === (_charge.currId ? _charge.currId : this.getCurrency()))
            return { ..._charge, currency: _selectedCurrency, isValid: true }
          })
        }
      } catch { }
    }
  }

  onDateSelection(date: NgbDateStruct) {
    let parsed = ''
    const _minDate = new Date(this.minDate.year, this.minDate.month - 1, this.minDate.day)
    const selectedDate = new Date(date.year, date.month - 1, date.day)
    if (!this.fromDate && !this.toDate) {
      this.fromDate = moment(selectedDate).isSameOrAfter(_minDate) ? date : null
      return
    } else if (this.fromDate && !this.toDate && moment(selectedDate).isAfter(_minDate)) {
      const _fromDate = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day)
      if (moment(selectedDate).isAfter(_fromDate)) {
        this.toDate = date
        this.input.close()
      }
    } else {
      if (moment(selectedDate).isSameOrAfter(_minDate)) {
        this.toDate = null
        this.fromDate = date
      } else {
        this.fromDate = null
        return
      }
    }
    if (this.fromDate) { parsed += this._parserFormatter.format(this.fromDate) }
    if (this.toDate) { parsed += ' - ' + this._parserFormatter.format(this.toDate) }
    this.renderer.setProperty(this.rangeDp.nativeElement, 'value', parsed)
  }

  addMoreCharges(_charges: JsonSurchargeDet[]) {
    const _chrgs = _charges[_charges.length - 1]
    if (!_chrgs.currency || !_chrgs.currency.id) {
      this._toast.info('Please select currency', 'Info')
      return
    }
    if (!_chrgs.price) {
      this._toast.info('Please add price', 'Info')
      return
    }
    if (!_chrgs.addChrCode) {
      this._toast.info('Please select any additional charge', 'Info')
      return
    }
    if (!(Object.keys(_chrgs).length === 0 && _chrgs.constructor === Object) && parseFloat(_chrgs.price) && _chrgs.currency && _chrgs.currency.id)
      _charges.push({ currId: _chrgs.currency.id, currency: _chrgs.currency, isValid: true } as any)
  }

  removeAdditionalCharge(type, _jsCharge: JsonSurchargeDet, inpIndx: number) {
    this.hasPriceChanged = true
    const _selectedCurrency = this.currencyList.find(e => e.id === this.getCurrency())
    if (type === 'origin') {
      const _charges = this.inputContainers[inpIndx].orginCharges
      if (_charges.length > 0) {
        _charges.forEach(_charge => {
          if (_charge.addChrID === _jsCharge.addChrID) {
            if (_charges.length > 1)
              this.inputContainers[inpIndx].orginCharges.splice(_charges.indexOf(_charge), 1)
            else if (_charges.length === 1)
              this.inputContainers[inpIndx].orginCharges = [{ currency: _selectedCurrency, isValid: true }] as any
            try {
              if (_charge.addChrID)
                this.originsList = this.originsList.sort(firstBy(function (v1, v2) { return v1.sortingorder - v2.sortingorder }))
            } catch (error) { }
          }
        })
      }
    } else if (type === 'destination') {
      const _charges = this.inputContainers[inpIndx].destinCharges
      if (_charges.length > 0) {
        _charges.forEach(element => {
          if (element.addChrID === _jsCharge.addChrID) {
            if (_charges.length > 1)
              this.inputContainers[inpIndx].destinCharges.splice(_charges.indexOf(element), 1)
            else if (_charges.length === 1)
              this.inputContainers[inpIndx].destinCharges = [{ currency: _selectedCurrency, isValid: true }] as any
            try {
              if (element.addChrID)
                this.destinationsList = this.destinationsList.sort(firstBy(function (v1, v2) { return v1.sortingorder - v2.sortingorder }))
            } catch (error) { }
          }
        })
      }
    }
  }

  closeDropdown(event: any) {
    try {
      if (!event.target.className.includes('has-open')) {
        this.originDropdown.close()
        this.destinationDropdown.close()
      }
    } catch { }
  }

  closeFix(event, datePicker) {
    if (event.target.offsetParent == null)
      datePicker.close()
    else if (event.target.offsetParent.nodeName !== 'NGB-DATEPICKER')
      datePicker.close()
  }

  getSurchargeBasis = (contLoad) => this._seaFreightService.getSurchargeBasis(contLoad).subscribe((res: any) => this.surchargesList = res ? res : [])

  showCustomChargesForm(type) {
    if (type === 'origin')
      this.isOriginChargesForm = !this.isOriginChargesForm
    else if (type === 'destination')
      this.isDestinationChargesForm = !this.isDestinationChargesForm
  }

  addCustomLabel(type) {
    const _obj = this.getCustomLabelObject(this.requestData)
    if (!_obj) {
      this._toast.info('Already Added, Please try another name', 'Info')
      return
    }
    this.loading = true
    this._seaFreightService.addCustomCharge(_obj).subscribe((res: JsonResponse) => {
      this.isOriginChargesForm = false
      this.isDestinationChargesForm = false
      this.loading = false
      if (res.returnId !== -1) {
        const _objNew = {
          addChrID: res.returnId,
          addChrCode: 'OTHR',
          addChrName: this.lablelName,
          addChrDesc: this.lablelName,
          modeOfTrans: this.requestData.rateRequest.ShippingModeCode,
          ContainerLoadType: this.requestData.forType === 'FCL',
          addChrBasis: _obj.addChrBasis,
          createdBy: this.userProfile.UserID,
          addChrType: 'ADCH',
          providerID: this.userProfile.ProviderID
        }
        this.getAllAdditionalCharges(this.userProfile.ProviderID)
        if (type === 'origin')
          this.originsList.push(_objNew as any)
        else if (type === 'destination')
          this.destinationsList.push(_objNew)
        this.lablelName = ''
        this.surchargeType = ''
      }
    }, () => this.loading = false)
  }

  getAllAdditionalCharges(providerID) {
    this._seaFreightService.getAllAdditionalCharges(providerID).subscribe((res: any) => {
      localStorage.setItem('additionalCharges', JSON.stringify(res.filter(e => e.addChrType === 'ADCH')))
      loading(false)
    }, () => loading(false))
  }

  viewInvoice() {
    if (this.getRateObject(this._currencyControl).length === 0) {
      return
    }
    const modalRef = this._modalService.open(RequestPriceDetailsComponent, {
      size: 'lg', centered: true, windowClass: 'price-breakdown-popup bar-class', backdrop: 'static', keyboard: false
    })
    modalRef.componentInstance.pricingJSON = this.getRateObject(this._currencyControl)
    modalRef.componentInstance.request = this.requestData.rateRequest
    modalRef.componentInstance.exchangeRateList = this.exchangeRateList
    modalRef.componentInstance.searchCriteria = this.searchCriteria
    modalRef.componentInstance.totalUnit = this.requestedCBM

    modalRef.result.then(() => {
      setTimeout(() => {
        document.getElementsByTagName('html')[0].style.overflowY = 'auto !important'
      }, 0)
    })
    modalRef.result.catch(() => { }).then(() => {
      if (document.querySelector('body > .modal')) document.body.classList.add('modal-open')
    })
  }

  editCBM() {
    const modalRef = this._modalService.open(RequestUnitCalc, {
      size: 'lg', centered: true, windowClass: 'unit-calc-modal', backdrop: 'static', keyboard: false
    })
    modalRef.componentInstance.request = this.requestData.rateRequest
    modalRef.componentInstance.searchCriteria = this.searchCriteria
    modalRef.componentInstance.inputContainers = this.inputContainers
  }

  priceChange = () => this.hasPriceChanged = true

  getRemainingInsCount = () =>
    (this.specialRequestComments) ? 500 - this.specialRequestComments.length : 500


  onTabChange($change) {
    const { nextId } = $change
    this.selectedContainer = nextId
  }

  ngOnDestroy() { }
}
