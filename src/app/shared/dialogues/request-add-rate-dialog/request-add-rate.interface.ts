import { CodeValMst } from '../../../interfaces'
import { IRequestContainers } from '../cargo-details-rate-request/cargo-details-rate-request.interface';

export interface IRequestData {
    forType: string
    addList: CodeValMst[]
    mode: string
    rateData: IRateData
    rateRequest: IRateRequest
}

export class Customer {
    CustomerID: number;
    CustomerName: string;
    CustomerImage: string;
    UserID: number;
    FirstName: string;
    LastName: string;
    UserImage: string;
    HMCustomerNature?: string
    IsEnterpriseCompany?: boolean
}

export interface Origin {
    PolType: string
    PolID: number
    PolCode: string
    PolName: string
    PolInputAddress?: any
    PolAddress: string
    PolCountryCode: string
    PolCountry: string
    PolCityName: string
}

export interface Destination {
    PodType: string
    PodID: number
    PodCode: string
    PodName: string
    PodInputAddress?: any
    PodAddress: string
    PodCountryCode: string
    PodCountry: string
    PodCityName: string
}

export class AirDet {
    ChargeableWeight?: any
    ProductName?: any
    IsVirtualAirLine?: any
    PriceBasisType?: any
    JsonPortVia?: any
    PickupTo: string
    PickupFrom: string
}

export class WarehouseDet {
    WHMinimumLeaseTerm: string
    WHID?: any
    WHName: string
    WHCityCode: string
    WHCityName: string
    WHCountryCode: string
    WHCountryName: string
    WHGLocCode: string
    WHGLocName: string
    WHAddress: string
    WHLongitude: string
    WHLatitude: string
    WHImages: string
    StoredUntilUtc?: any
    StoredFromUtc?: any
    WHDesc: string
    OfferedAreaUnit: string
    UsageType: string
    IsBondedWarehouse: boolean
    IsTransportAvailable: boolean
    StorageType: string
    StorageTypeTitle: string
    TotalSQFT?: any
    TotalSQM?: any
}

export class InsurancePolicy {
    PolicyNumber?: any
    PolicyName?: any
    InsurancePolicyMode?: any
    SumCovered?: any
    SumCoveredCurrID?: any
    SumCoveredCurrCode?: any
}

export class IRateRequest {
    IsDocumentUploaded?: string
    ResponseOn?: string
    ResponseBy?: string
    BookingID?: number
    BookedByUserID?: number
    BookedBy?: string
    BookingDate?: string
    BookingNo?: string
    JsonSearchCriteria: any
    ShippingCatID: any;
    ProviderImage: any;
    BookingSourceVia: string;
    BookingSpecialAmount: any;
    CurrencyCode: any;
    PerUnitRate: any;
    UnitRateType: string;
    PerUnitRateType: any;
    BookingDesc: string;
    EtaUtc: string
    ExpiringInDays: number
    TotalAmount: number

    AirDet: AirDet
    WarehouseDet: WarehouseDet
    InsurancePolicy: InsurancePolicy
    Customer: Customer
    Carrier: {
        CarrierID: number
        CarrierCode: string
        CarrierName: string
        CarrierImage: string
        HMCustomerNature: string
    }
    Provider: {
        ProviderID: number
        ProviderName: string
        ProviderImage: string
    }
    ChildRequests: IRateRequest[]
    ProviderID
    ProviderRequestActive: boolean
    ProviderRequestStatus?: string
    ProviderRequestStatusBL?: string
    ProviderRequestStatusID?: number
    CurrencyID: number
    EtdUtc: any
    Origin: Origin
    Destination: Destination
    RequestOrigin: Origin
    RequestDestination: Destination
    RequestID: number
    RequestSource: string
    RequestNo: string
    RequestDateTime: Date

    RequestStatusID: number
    RequestStatus: string
    RequestStatusBL: string
    CloseStatusName?: string
    IsClosed?: boolean

    CommodityDescription: string
    ShippingModeCode: string
    ShippingMode: string
    ShippingModeImage: string
    ShippingCatName: string
    ShippingCatImage: string
    UserImage: string
    CompanyImage: string
    ContainerLoad: string
    TotalContainerQty: number
    ProviderRespondedQty?: number
    TotalCBM?: any
    TotalPallet?: any

    ProviderUnReadMsgCount: number
    UserUnReadMsgCount: number
    ResponseCount: number

    // Reponses (Child)
    ResponseID: number
    ResponseDateTime: string
    ResponseTagID: number
    ProviderResponseNo: string
    ResponseStatusID: number
    ResponseStatus: string
    ResponseStatusBL: string
    VesselDetail?: IRequestSchedule
    ValidFrom: string
    ValidTo: string
    HMCommissionRate?: number
    HMCommissionValue?: number
    ShippingModeID: any;
}

export interface PickupPort {
    id: number
    code: string
    title: string
    imageName: string
    type?: string
}

export interface DeliveryPort {
    id: number
    code: string
    title: string
    imageName: string
    type?: string
}

export interface Currency {
    id: number
    code: string
    imageName: string
    shortName: string
}

export interface JsonSurchargeDet {
    addChrID: number
    addChrCode: string
    addChrName: string
    addChrDesc: string
    modeOfTrans: string
    addChrType: string
    addChrBasis: string
    isSlabBased: boolean
    jsonSlabData?: any
    sortingorder: number
    containerLoadType: string
    imp_Exp: string
    currId: number
    currency: Currency
    price: string
    basePrice: number
    exchangeRate: number
    currExcgRateListID?: number
    total?: number // only for priceDetails
    totalBase?: number // only saving
    displayPrice?: number // only for priceDetails
    containerName?: string // only for priceDetails
    isValid: boolean // only for priceDetails
    IsMandatory?: boolean
}

export interface JsonContainerDet {
    containerSpecID: number
    containerData?: IRequestContainers
    price: number
    basePrice: number
    currencyID: number
    currCode?: string // only for priceDetails
    total?: number // only for priceDetails
    baseTotal?: number // only saving
    displayPrice?: number
    exchangeRate: number
    currExcgRateListID?: number
    respondedQty: number
    requestedQty: number
    jsonSurchargeDet: JsonSurchargeDet[]
    contIndexID?: string
    resQty?: number
}

export interface IRateData {
    customerSpecialRequestComments?: string;
    jsonSearchCriteria?: string;
    responseID: number
    requestID: number
    customerID: number
    providerID: number
    carrierID: number
    shippingCatID: number
    modeOfTrans: string
    specialRequestComments: string
    responseStatus: string
    IsOnlyComments: boolean
    isRouteDetailUpdate: boolean
    isScheduleUpdate: boolean
    routeAnimation: string
    jsonTransferVesselsDetails?: any
    pickupPort: PickupPort
    deliveryPort: DeliveryPort
    currencyID: number
    exchangeRate: number
    currExcgRateListID: number
    effectiveFrom: string
    effectiveTo: string
    jsonContainerDet: JsonContainerDet[]
    jsonCargoDetail?: string
    partnerID?: any
    totalAmount: number
}

export interface IContainer {
    containerSpecID: number
    containerSpecCode: string
    containerSpecDesc: string
    containerSpecShortName: string
    bookingContTypeQty: number
    bookingPkgTypeCBM: number
    bookingPkgTypeWeight: number
    isTrackingRequired: boolean
    isQualityMonitoringRequired: boolean
    jsonContainerDetail: string
    containerSpecImage: string
    dimensionUnit: string
    weightUnit: string
    containerWeight: number
    containerLength: number
    containerWidth: number
    containerHeight: number
    jsonContainerInfo?: any
}

export interface IContanerInput {
    container: IRequestContainers
    reqQty: any
    resQty?: any
    bookedQty?: any
    price: any
    basePrice?: any
    doublePrice: number
    currency: Currency
    currencyID: number
    contIndexID?: string
    orginCharges: JsonSurchargeDet[]
    destinCharges: JsonSurchargeDet[]
    isValidPrice: boolean
    isValidQty: boolean
    isValidCurr: boolean
}

export interface RouteDetails {
    RouteCode1: string;
    RouteName1?: any;
    R1PolID?: number;
    R1PolCode: string;
    R1PolName?: string;
    R1PodID?: number;
    R1PodCode: string;
    R1PodName?: string;
    R1EtdDate: string;
    R1EtaDate: string;
    R1VesselName: string;
    R1VesselNo: string;
    R1VoyageRefNo: string;

    RouteCode2?: string;
    RouteName2?: any;
    R2PolID?: number;
    R2PolCode?: string;
    R2PolName?: string;
    R2PodID?: number;
    R2PodCode?: string;
    R2PodName?: string;
    R2EtdDate?: string;
    R2EtaDate?: string;
    R2VesselName?: string;
    R2VesselNo?: string;
    R2VoyageRefNo?: string;

    RouteCode3?: string;
    RouteName3?: any;
    R3PolID?: number;
    R3PolCode?: string;
    R3PolName?: string;
    R3PodID?: number;
    R3PodCode?: string;
    R3PodName?: string;
    R3EtdDate?: string;
    R3EtaDate?: string;
    R3VesselName?: string;
    R3VesselNo?: string;
    R3VoyageRefNo?: string;
}

export interface IRequestSchedule {
    DepartureDate?: string;
    TransferStops: number;
    TransitDays: number;
    CarrierID: number;
    RouteAnimation: string;
    CutOffDate: string;
    RouteDetails: RouteDetails;
}

export interface IPricing {
    price: number,
    resQty: any
}


export class SearchCriteria {
    // bookingCategoryID: number = 0;
    BookingDesc?: string = '';
    bookingCategoryID: number = 0;
    pickupPortID: number = 0;
    pickupPortCode: string = "";
    pickupPortName: string = "";
    deliveryPortID: number = 0;
    deliveryPortCode: string = "";
    deliveryPortName: string = "";
    pickupDate: string = "";
    deliveryDate?: string = "";
    pickupFlexibleDays: number = 3;
    shippingModeID: number = 0;
    shippingCatID: number = 0;
    shippingSubCatID: number = 0;
    containerLoad: string = "FCL";
    CurrencyCode: string;
    imp_Exp: string = "EXP";
    carrierID: number = 0;
    routeIDs: string = "";
    etaInDays: number = 0;
    carrierEtdUtcDate: string = "";
    voyageRefNum: string = "";
    recordCounter?: number;
    searchMode?: string; //Refer to DOC-1 at the bottom
    totalChargeableWeight?: number;
    SearchCriteriaTransportationDetail: Array<any>;
    SearchCriteriaContainerDetail: Array<SearchCriteriaContainerDetail>;
    LclChips?: Array<LclChip>;
    TransportMode?: string;
    shippingModeId?: number;
    shippingCatName?: string;
    totalShipmentCMB?: number
    totalVolumetricWeight?: number
    deliveryPortType?: string
    pickupPortType?: string
    pickUpAddress?: string
    deliveryAddress?: string
    userPickup?: any
    userDelivery?: any
    SearchCriteriaPickupGroundDetail?: any
    SearchCriteriaDropGroundDetail?: any
    portJsonData?: string | any = "[]";
    IDlist?: string | any
    criteriaFrom?: string
    selectedModeCaption?: any //only for shipment component use for tempSearchCriteria
    searchTransportDetails?: any //only for shipment component use for tempSearchCriteria
    ProviderID?: any;
    loggedID?: number;
    CustomerID: number;
    CustomerType: string;
    isSearchByCalender: boolean = false;
    pickupDateTo?: string = ''
    chargeableWeight?: number = null
    cargoLoadType?: string = null
    airChipData?: LclChip
    isVirtual?: boolean = false
    lclViewContainers?: Array<any> = []
    fclContainerChips?: Array<string> = []
    lclAirContainerChips?: Array<string> = []
    lclChipConatiners?: Array<any> = []
    currNav?: string = ''
    bookingWeight?: number = 0
    JsonOriginPorts?
    JsonDestinationPorts?
    CONT: number;
    ProposedDepartureDate: any;
}

export interface LclChip {
    cargoLoadType: any;
    quantity: number;
    lengthUnitID: number;
    weightUnitID: number;
    volumeUnitID: number;
    length: number;
    width: any;
    height: number;
    weight: number;
    id: number;
    volume: any;
    packageType: string;
    contSpecID: number;
    lengthUnit: string;
    weightUnit: any;
    contRequestedQty: number;
    toggle: boolean;
    contRequestedCBM: number;
    contRequestedWeight: number;
    volumetricWeight: number;
    volumeUnit?: number;
    inpVolume?: number;
    inpTotalWeight?: number;
    byUnitsChips?: any

    // extra air units
    totalWeight: number
    totalWeightUnit: number
    chargeableWeight: number
    chipWeight: number
    actualWeight: number
}


export class SearchCriteriaContainerDetail {
    contSpecID: number = 0;
    contRequestedQty: number = 0;
    contRequestedCBM: number = 0;
    contRequestedWeight: number = 0;
    volumetricWeight: number = 0;
    containerCode: string = '';
    IsTrackingApplicable?: boolean = false;
    IsQualityApplicable?: boolean = false;
    containerDtl?: any;
    ContainerSpecDesc?: string;
    contRequestedType: string;
}

export interface ILclCargo {
    calculationMethod: string;
    quantity: number;
    weightUnitID: number;
    volumeUnitID: number;

    packageType: string;
    contSpecID: number;
    contSpecDesc: string;
    contSpecType: string;
    lengthUnit?: string;
    weightUnit: string;
    volumeUnit: string;
    respondedCBM: number;
    respondedWeight: number;
    respondedVolumetricWeight: number;
    // by unit
    lengthUnitID?: number;
    length?: number;
    width?: number;
    height?: number;
    weight: string;
    volume: number;
}

export interface IDefAddCharges {
    AddChrName: any;
    AddChrID: number
    ContainerLoadType: string
    IsMandatory: boolean
    AddChrBasis: string
}
