import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestAddRateDialogComponent } from './request-add-rate-dialog.component';

describe('RequestAddRateDialogComponent', () => {
  let component: RequestAddRateDialogComponent;
  let fixture: ComponentFixture<RequestAddRateDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestAddRateDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestAddRateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
