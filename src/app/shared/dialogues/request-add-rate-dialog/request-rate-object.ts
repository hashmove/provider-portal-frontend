import { NgbDateStruct, NgbInputDatepicker, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap'
import { JsonSurchargeDet, IContainer, IContanerInput, Currency, IRequestData, SearchCriteria, ILclCargo, IRateData, IDefAddCharges } from './request-add-rate.interface'
import { UserInfo, CodeValMst, ICarrierFilter, Rate } from '../../../interfaces'
import { IPriceLogs } from '../../../interfaces/pricing.interface'
import { AddRateHelpers } from '../../../helpers/add-rate.helper'
import { IRequestContainers } from '../cargo-details-rate-request/cargo-details-rate-request.interface'
import { Input, ViewChild, ElementRef } from '@angular/core'
import { ITermOption } from '../payment-terms/payment-terms.interface'
import { isMobile } from '../../../constants/globalFunctions'

export class RequestRateObjects extends AddRateHelpers {

    @Input() requestData: IRequestData
    @Input() editorContent
    @Input() warehouseUnit: string = null

    @ViewChild('dp') input: NgbInputDatepicker
    @ViewChild('rangeDp') rangeDp: ElementRef
    @ViewChild('effectiveFrom') effectiveFrom: ElementRef
    @ViewChild('effectiveTo') effectiveTo: ElementRef
    @ViewChild('originPickupBox') originPickupBox: ElementRef
    @ViewChild('destinationPickupBox') destinationPickupBox: ElementRef
    @ViewChild('originDropdown') originDropdown: any
    @ViewChild('destinationDropdown') destinationDropdown

    @ViewChild('carrier') carrier: ElementRef
    @ViewChild('origin') origin: ElementRef
    @ViewChild('destination') destination: ElementRef
    @ViewChild('cargo') cargo: ElementRef
    @ViewChild('container') container: ElementRef
    @ViewChild('depDate') depDate: any
    @ViewChild('cutDate') cutDate: any

    scheduleModal: NgbModalOptions = { size: 'lg', centered: true, windowClass: 'schedule-modal', backdrop: 'static', keyboard: false }
    smallModal: NgbModalOptions = { size: 'lg', centered: true, windowClass: 'small-modal', backdrop: 'static', keyboard: false }
    x_smallModal: NgbModalOptions = { size: 'lg', centered: true, windowClass: 'x-small-modal', backdrop: 'static', keyboard: false }
    historyModal: NgbModalOptions = { size: 'lg', centered: true, windowClass: 'history-modal', backdropClass: '.app-session-modal-backdrop', keyboard: false }

    deletScheduleMsg = {
        messageTitle: 'Delete Schedule',
        messageContent: `Are you sure you want to delete the current Schedule?`,
        buttonTitle: 'Yes',
        data: null
    }

    public currencyList: Currency[] = []
    public userProfile: UserInfo
    public selectedContainer: any = null
    public selectedHandlingUnit: any
    public selectedPrice: any
    public couplePrice: any
    public defaultCurrency: any = { id: 101, shortName: 'USD', imageName: 'US' }
    public selectedCurrency: any = this.defaultCurrency
    public startDate: NgbDateStruct
    public minDate: NgbDateStruct
    public departureMinDate: NgbDateStruct
    public hoveredDate: NgbDateStruct
    public fromDate: any = { day: null, month: undefined, year: undefined }
    public toDate: any = { day: undefined, month: undefined, year: undefined }
    public model: any

    public destinationsList = []
    public originsList: any[] = []

    public selectedOrigins: JsonSurchargeDet[] = [{}] as any
    public selectedDestinations: JsonSurchargeDet[] = [{}] as any
    public containerLoadParam = 'FCL'

    public TotalImportCharges = 0
    public TotalExportCharges = 0

    public loading = false

    originSelectedMode = 'SEA'
    destinSelectedMode = 'SEA'

    public surchargesList: CodeValMst[] = []
    isRateUpdating = false
    public buttonLoading = false
    public isOriginChargesForm = false
    public isDestinationChargesForm = false
    public lablelName = ''
    public surchargeType = ''
    public labelValidate = true
    public surchargeBasisValidate = true
    public canAddLabel = true
    public addDestinationActive = false
    public addOriginActive = false

    // public showDoubleRates = false
    public priceBasis = ''
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

    // Rate Update
    public containers: IRequestContainers[] = []
    public specialRequestComments = ''
    public actualIndividualPrice
    public actualIndividualPriceOrigin

    specialReqDesc = ''
    searchMode = 'sea-fcl'

    originPorts: ICarrierFilter[] = []
    destinPorts: ICarrierFilter[] = []
    allPorts: ICarrierFilter[] = []

    public filterOrigin: ICarrierFilter
    public filterDestination: ICarrierFilter
    public isRouteDetailUpdate = false
    priceUpdateLogs: IPriceLogs[] = []

    public selectedShipping: ICarrierFilter
    allShippingLines: ICarrierFilter[] = []
    commodityDesc: string = null

    originalCarrierID: number
    vesselSchedule = null
    schdeuleStatus = 'NONE'
    inputContainers: IContanerInput[] = []

    selectionDuration = null
    durationDdl: Array<number> = []
    public selectedStops = 'direct'
    public shipStop = ['direct', '1 t/s', '2 t/s']

    departureDate: any = null
    arrivalDate: any = null
    cutOffDate: any = null
    originSearch: string = null
    destinSearch: string = null

    t1Arrival = null
    t1Departure = null
    t1Route: ICarrierFilter = null
    t2Arrival = null
    t2Departure = null
    t2Route: ICarrierFilter = null
    cutOffMinDate = null
    chargeBasis = ['PER_KG', 'PER_CONTAINER', 'PER_CBM', 'PER_TRUCK']
    exchangeRateList: Rate[] = []
    hasScheduleChanged = false
    responseSent = 0
    rateType = 'Container'
    activeAccrodian = ['route_ts2', 'route_ts1']
    respondedCBM = 0
    requestedCBM = 0

    perRateText = 'Rate Per Container'
    searchCriteria: SearchCriteria = null
    lclContainers: ILclCargo[] = []
    schedule: any = null

    // Air Working

    public departureDays: any[] = []
    public cutoffDays: any[] = []
    public jsonSurchargeDetail: any[] = []
    public selectedDepartureDays: any[] = []
    public selectedCutoffDays: any[] = []
    public viaList: ICarrierFilter[] = []
    public productsList: ICarrierFilter[] = []
    public airFreightTypes: ICarrierFilter[] = []


    public selectedProduct: ICarrierFilter = null;
    public selectedAircraft: string = null

    public minTransitDays: number = null
    public maxTransitDays: number = null
    public aircraftTypeID: number = null
    public disableFields = false
    public jsonDepartureDays = null
    public jsonCutOffDays = null
    public isTransitDaysValidated = true
    public hasPriceChanged = false
    public hasCarrierChanged = false
    rateData: IRateData

    // Validation
    isOriginInValid = false
    isDestinInValid = false
    isShippingInValid = false
    isEffectiveInValid = false

    isDepartureInValid = false
    isCutOffInValid = false
    isTransitInValid = false
    IsR1InValid = false
    isR2InValid = false
    hasRateTCChanged = false
    defAddCharges: IDefAddCharges[] = []
    manCharges: number[] = []
    paymentTermsOptions: ITermOption[] = []
    showHMPolicy = false
    hmTerms = ''
    isEnterprise = false
    isMobile = isMobile()
    displayMonths = this.isMobile ? 1 : 2
}
