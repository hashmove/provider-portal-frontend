import { after, before, encryptBookingID, equals, getImagePath, ImageRequiredSize, ImageSource, isArrayValid, isNumber } from '../../../constants/globalFunctions'
import { IContanerInput, IRateRequest, IRequestData, IRequestSchedule, JsonContainerDet, JsonSurchargeDet, LclChip, RouteDetails, SearchCriteria } from './request-add-rate.interface'
import { cloneObject } from '../../../components/pages/user-desk/reports/reports.component'
import { RequestRateObjects } from './request-rate-object'
import { debounceTime, map } from 'rxjs/operators'
import { Observable } from 'rxjs'
import * as moment from 'moment'
import { CurrencyControl } from '../../../services/currency.service'
import { ICarrierFilter } from '../../../interfaces'
export class RequestRateHelper extends RequestRateObjects {

    setDateLimit() {
        const _date = new Date()
        this.minDate = { year: _date.getFullYear(), month: _date.getMonth() + 1, day: _date.getDate() }
        const _depMinDate = moment(_date).add(3, 'days')
        this.departureMinDate = { year: _depMinDate.year(), month: _depMinDate.month() + 1, day: _depMinDate.date() }
    }

    onDepartureRmv() {
        this.departureDate = null
        this.cutOffDate = null
        this.selectionDuration = null
        this.t1Arrival = null
        this.t1Departure = null
        this.t2Arrival = null
        this.t2Departure = null
        this.t1Route = null
        this.t2Route = null
        this.selectedStops = 'direct'
    }

    onKeyDown(_charge: JsonSurchargeDet, event, type) {
        if (!event.target.value) {
            _charge.currency = {} as any
            _charge.currId = null
        }
    }

    selectCharges(_charge: JsonSurchargeDet, _charges: JsonSurchargeDet[], type, model: JsonSurchargeDet, contIndex: number, chIndex: number) {
        if (type === 'EXPORT') {
            model.imp_Exp = type
            if (Object.keys(_charge).length === 0 && _charge.constructor === Object) {
                model.currId = this.selectedCurrency.id
                model.currency = this.selectedCurrency
            } else {
                model.currId = _charge.currency.id
                model.currency = _charge.currency
            }
            const _clCharges = cloneObject(_charges)
            _clCharges.forEach(_ch => {
                if (Object.keys(_ch).length === 0 && _ch.constructor === Object)
                    _clCharges.splice(_clCharges.indexOf(_ch), 1)
            })
            if (_clCharges[chIndex]) {
                if (this.originsList.filter(_ch => _ch.addChrID === _clCharges[chIndex].addChrID).length === 0) {
                    this.originsList.push(_clCharges[chIndex])
                }
                _clCharges[chIndex] = model
            } else {
                _clCharges.push(model)
            }
            this.inputContainers[contIndex].orginCharges = cloneObject(_clCharges)
        } else if (type === 'IMPORT') {
            model.imp_Exp = type
            if (Object.keys(_charge).length === 0 && _charge.constructor === Object) {
                model.currId = this.selectedCurrency.id
                model.currency = this.selectedCurrency
            } else {
                model.currId = _charge.currency.id
                model.currency = _charge.currency
            }
            const _clCharges = cloneObject(_charges)
            _clCharges.forEach(_ch => {
                if (Object.keys(_ch).length === 0 && _ch.constructor === Object)
                    _clCharges.splice(_clCharges.indexOf(_ch), 1)
            })
            if (_clCharges[chIndex]) {
                if (this.destinationsList.filter(_ch => _ch.addChrID === _clCharges[chIndex].addChrID).length === 0) {
                    this.destinationsList.push(_clCharges[chIndex])
                }
                _clCharges[chIndex] = model
            } else {
                _clCharges.push(model)
            }
            this.inputContainers[contIndex].destinCharges = cloneObject(_clCharges)
        }
    }

    isMulti = (basis: any) => this.chargeBasis.includes(basis)

    getNum = (_num: any) => (_num) ? (parseFloat(_num) ? typeof parseFloat(_num) === 'number' ? parseFloat(_num) : 0 : 0) : 0

    getPrice = (price, qty, isMain, basis) =>
        (isMain || this.isMulti(basis)) ? this.getNum(price) * this.getNum(qty) : this.getNum(price)

    getCurrForTotal(inpCont: IContanerInput[]) {
        try {
            return isArrayValid(inpCont, 0) ? (inpCont[0].currency && inpCont[0].currency.id) ? inpCont[0].currency.id : this.getCurrency() : this.getCurrency()
        } catch {
            return this.getCurrency()
        }
    }

    getCurrCodeForTotal(inpCont: IContanerInput[]) {
        try {
            return this.currencyList.filter(_curr => _curr.id === this.getCurrForTotal(inpCont))[0].code
        } catch {
            return this.getCurrency()
        }
    }

    getTotalCharges(inpCont: IContanerInput[], _currUtil: CurrencyControl) {
        try {
            const { rate } = this.exchangeRateList.filter(_rate => rate.currencyID === this.getCurrForTotal(inpCont))[0]
            const _totalFSUR = inpCont.reduce((a, b) =>
                a + this.getPrice(_currUtil.getNewPrice(this.getNum(b.price), rate), b.resQty, true, ''), 0)

            const _totalAddOrigin = inpCont.map(_price => {
                const { rate } = this.exchangeRateList.filter(_rate => rate.currencyID === this.getCurrForTotal(inpCont))[0]
                return _price.orginCharges.reduce((a, b) =>
                    a + this.getPrice(_currUtil.getNewPrice(this.getNum(b.price), rate), _price.resQty, false, b.addChrBasis), 0)
            }).reduce((all, item) => (all + item), 0)

            const _totalAddDestin = inpCont.map(_price =>
                _price.destinCharges.reduce((a, b) =>
                    a + this.getPrice(_currUtil.getNewPrice(this.getNum(b.price), rate), _price.resQty, false, b.addChrBasis), 0)
            ).reduce((all, item) => (all + item), 0)
            return _totalFSUR + _totalAddOrigin + _totalAddDestin
        } catch { return 0 }
    }

    getCustomLabelObject(requestData: IRequestData) {
        this.canAddLabel = true
        if (!this.lablelName) {
            this.labelValidate = false
            return null
        }
        if (!this.surchargeType) {
            this.surchargeBasisValidate = false
            return null
        }
        const selectedSurcharge = this.surchargesList.find(obj => obj.codeValID === parseInt(this.surchargeType))
        const _obj = {
            addChrID: -1,
            addChrCode: 'OTHR',
            addChrName: this.lablelName,
            addChrDesc: this.lablelName,
            modeOfTrans: (requestData.forType === 'FCL' || requestData.forType === 'LCL') ? 'SEA' : 'TRUCK',
            addChrBasis: selectedSurcharge.codeVal,
            createdBy: this.userProfile.UserID,
            addChrType: 'ADCH',
            providerID: this.userProfile.ProviderID,
            ContainerLoadType: requestData.forType,
        }
        requestData.addList.forEach(element => {
            if (element.addChrName === _obj.addChrName)
                this.canAddLabel = false
        })
        return !this.canAddLabel ? null : _obj
    }

    dropdownToggle(event, type) {
        if (event) {
            this.isDestinationChargesForm = false
            this.isOriginChargesForm = false
            this.surchargeBasisValidate = true
            this.labelValidate = true
            if (type === 'destination') {
                this.addDestinationActive = true
                this.addOriginActive = false
            } else if (type === 'origin') {
                this.addOriginActive = true
                this.addDestinationActive = false
            }
        } else {
            this.addOriginActive = false
            this.addDestinationActive = false
        }
    }

    getDateStr($date) {
        const strMonth = isNumber($date.month) ? this.months[$date.month - 1] + '' : ''
        return $date.day + '-' + strMonth + '-' + $date.year
    }

    isHovered = date => this.fromDate && !this.toDate && this.hoveredDate && after(date, this.fromDate) && before(date, this.hoveredDate)
    isInside = date => after(date, this.fromDate) && before(date, this.toDate)
    isFrom = date => equals(date, this.fromDate)
    isTo = date => equals(date, this.toDate)

    onDepartureChange() {
        setTimeout(() => {
            const _arrivalDate = new Date(this.getArrivalDate())
            this.arrivalDate = { year: _arrivalDate.getFullYear(), month: _arrivalDate.getMonth() + 1, day: _arrivalDate.getDate() }
            const _newDate = moment(new Date(this.departureDate.year, this.departureDate.month - 1, this.departureDate.day)).subtract(3, 'day')
            console.log(_newDate.format());

            this.cutOffDate = { year: _newDate.year(), month: _newDate.month() + 1, day: _newDate.date() }
            console.log(this.cutOffDate);

            const _currDate = new Date()
            this.cutOffMinDate = { year: _currDate.getFullYear(), month: _currDate.getMonth() + 1, day: _currDate.getDate() }
            this.t1Arrival = null
            this.t1Departure = null
            this.t2Arrival = null
            this.t2Departure = null
        }, 10)
    }

    t1DateChange() {
        this.t2Arrival = null
        this.t2Departure = null
    }

    setPorts(requestData: IRequestData, _ports) {
        const { PolCountry, PolType, PolCode } = requestData.rateRequest.Origin
        const { PodCountry, PodType, PodCode } = requestData.rateRequest.Destination
        this.allPorts = _ports
        if (PolCountry) {
            this.originPorts = _ports.filter(_port => _port.title.toLowerCase().includes(PolCountry.toLowerCase()))
            if (PolType === 'SEA' || PolType === 'AIR')
                this.filterOrigin = this.originPorts.filter(_port => _port.code === PolCode)[0]
        }
        if (PodCountry) {
            this.destinPorts = _ports.filter(_port => _port.title.toLowerCase().includes(PodCountry.toLowerCase()))
            if (PodType === 'SEA' || PodType === 'AIR')
                this.filterDestination = this.destinPorts.filter(_port => _port.code === PodCode)[0]
        }
    }

    onTs1Change() {
        if (this.t1Route && this.t1Route.id) {
            if (this.t1Route.id === this.filterOrigin.id || this.t1Route.id === this.filterDestination.id) {
                this.t1Route = null
            }
        }
    }

    onTs2Change() {
        if (this.t2Route && this.t2Route.id) {
            if (this.t1Route.id === this.t2Route.id || this.t2Route.id === this.filterOrigin.id || this.t2Route.id === this.filterDestination.id) {
                this.t2Route = null
            }
        }
    }

    filterPortsFormatter = (_port) => _port.title

    filterPortsFormatterTs1 = (_port: ICarrierFilter) => {
        if (_port.id === this.filterDestination.id || _port.id === this.filterOrigin.id) {
            this.t1Route = null
            return null
        } else {
            return _port.title
        }
    }
    filterPortsFormatterTs2 = (_port: ICarrierFilter) => {
        if (_port.id === this.filterDestination.id || _port.id === this.filterOrigin.id || _port.id === this.t1Route.id) {
            this.t2Route = null
            return null
        } else {
            return _port.title
        }
    }

    allPortsFilter = (text$: Observable<string>) => text$.debounceTime(200).map(term => !term || term.length < 3 ? []
        : this.allPorts.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))

    originPortsFilter = (text$: Observable<string>) => text$.debounceTime(200).map(term => !term || term.length < 3 ? []
        : this.originPorts.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))

    destinPortsFilter = (text$: Observable<string>) => text$.debounceTime(200).map(term => !term || term.length < 3 ? []
        : this.destinPorts.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))

    getShippingLineImage = ($image: string) => getImagePath(ImageSource.FROM_SERVER, '/' + $image, ImageRequiredSize.original)

    shippings = (text$: Observable<string>) => text$.pipe(debounceTime(200), map(term => !term || term.length < 3
        ? [] : this.allShippingLines.filter(v => v.title && v.title.toLowerCase().indexOf(term.toLowerCase()) > -1)))

    getBookingKey = (_request: IRateRequest) => encryptBookingID(_request.RequestID, _request.ProviderID, _request.ShippingModeCode)

    getBookingKeyUser = (_request: IRateRequest) => encryptBookingID(_request.RequestID, _request.Customer.UserID, _request.ShippingModeCode)

    currencies = (text$: Observable<string>) => text$.pipe(debounceTime(200), map(term => !term || term.length < 3 ? []
        : this.currencyList.filter(v => v.shortName && v.shortName.toLowerCase().indexOf(term.toLowerCase()) > -1)
    ))
    currencyFormatter = _currency => (_currency.shortName)

    hasCharges(pricingJSON: JsonContainerDet[]) {
        let hasCharges = false
        try {
            const _prices = []
            pricingJSON.forEach(_price => {
                if (isArrayValid(_price.jsonSurchargeDet, 0)) {
                    const { jsonSurchargeDet } = _price
                    const _chrgs = jsonSurchargeDet.filter(_chg => _chg.price)
                        .map(_chg => this.getNum(_chg.price))
                        .reduce((all, item) => (all + item), 0)
                    _prices.push(_chrgs)
                }
            })
            if (isArrayValid(_prices, 0) && _prices.reduce((all, item) => (all + item), 0) > 0)
                hasCharges = true
        } catch {
            hasCharges = false
        }
        return hasCharges
    }

    getCurrentCarrier = (_req: IRateRequest) => _req.Carrier && _req.Carrier.CarrierID ? _req.Carrier.CarrierID : 0

    getTotalPrice(pricingJSON, _currControls) {
        let totalAmount = 0
        let totalBaseAmount = 0
        try {
            if (isArrayValid(pricingJSON, 0)) {
                const _parentExchangeRate = pricingJSON[0].exchangeRate
                // Base Rate
                let freightData = []
                // LCL
                if (this.searchMode === 'sea-lcl' || this.searchMode === 'air-lcl') {
                    const [_cbmCont] = pricingJSON
                    freightData.push({
                        ..._cbmCont,
                        total: this.getPrice(_currControls.getNewPrice(this.getNum(_cbmCont.basePrice), _parentExchangeRate), this.respondedCBM, true, ''),
                        totalBase: this.getPrice(this.getNum(_cbmCont.basePrice), this.respondedCBM, true, ''),
                    })

                } else {
                    // FCL - FTL
                    freightData = pricingJSON.map(_price => {
                        return {
                            ..._price,
                            total: this.getPrice(_currControls.getNewPrice(this.getNum(_price.basePrice), _parentExchangeRate), _price.respondedQty, true, ''),
                            totalBase: this.getPrice(this.getNum(_price.basePrice), _price.respondedQty, true, ''),
                        }
                    })
                }

                // Add Charge
                const additionalData = []
                try {
                    // console.log(pricingJSON)
                    pricingJSON.map(_price => {
                        if (isArrayValid(_price.jsonSurchargeDet, 0)) {
                            _price.jsonSurchargeDet.forEach(_charge => {
                                const _respondedQty = this.searchMode === 'sea-lcl' || this.searchMode === 'air-lcl' ? this.respondedCBM : _price.respondedQty
                                additionalData.push({
                                    ..._charge,
                                    total: this.getPrice(_currControls.getNewPrice(this.getNum(_charge.basePrice), _parentExchangeRate), _respondedQty, false, _charge.addChrBasis),
                                    totalBase: this.getPrice(this.getNum(_charge.basePrice), _respondedQty, false, _charge.addChrBasis),
                                })
                            })
                        }
                    })
                } catch { }
                freightData.forEach(element => {
                    totalAmount += element.total
                    totalBaseAmount += element.totalBase
                })
                additionalData.forEach(element => {
                    totalAmount += element.total
                    totalBaseAmount += element.totalBase
                })
            }
        } catch (error) {
            console.log('Total Calculation Error:', error)
        }
        return [totalAmount, totalBaseAmount]
    }

    generateSaveObject(rateRequest: IRateRequest, pricingJSON: JsonContainerDet[], _currUtil: CurrencyControl, _toast) {
        const { filterDestination, filterOrigin, selectedShipping } = this
        let obj = null
        const _currencyID = this.getCurrForTotal(this.inputContainers)
        const _exchangeRate = this.exchangeRateList.filter(rate => rate.currencyID === _currencyID)[0]
        const [_totalAmnt, _totalBaseAmount] = this.getTotalPrice(pricingJSON, _currUtil)
        const _totalAmount = _totalAmnt
        this.actualIndividualPrice = _totalAmount
        try {
            obj = {
                specialRequestComments: this.specialRequestComments,
                responseStatusBL: 'PRICE_SENT',
                IsOnlyComments: false,
                isRouteDetailUpdate: this.isRouteDetailUpdate,
                carrierID: selectedShipping && selectedShipping.id ? selectedShipping.id : this.getCurrentCarrier(rateRequest),
                pickupPort: {
                    id: filterOrigin ? filterOrigin.id : rateRequest.Origin.PolID,
                    code: filterOrigin ? filterOrigin.code : rateRequest.Origin.PolCode,
                    title: filterOrigin ? filterOrigin.title : rateRequest.Origin.PolName,
                    imageName: filterOrigin ? filterOrigin.imageName : rateRequest.Origin.PolCountryCode,
                    type: filterOrigin ? rateRequest.Origin.PolType === 'AIR' ? 'AIR' : filterOrigin.type : rateRequest.Origin.PolType
                },
                deliveryPort: {
                    id: filterDestination ? filterDestination.id : rateRequest.Destination.PodID,
                    code: filterDestination ? filterDestination.code : rateRequest.Destination.PodCode,
                    title: filterDestination ? filterDestination.title : rateRequest.Destination.PodName,
                    imageName: filterDestination ? filterDestination.imageName : rateRequest.Destination.PodCountryCode,
                    type: filterDestination ? rateRequest.Origin.PolType === 'AIR' ? 'AIR' : filterDestination.type : rateRequest.Destination.PodType
                },
                isScheduleUpdate: false,
                requestID: rateRequest.RequestID,
                responseID: rateRequest.ResponseID ? rateRequest.ResponseID : -1,
                customerID: rateRequest.Customer.CustomerID,
                providerID: rateRequest.ProviderID,
                shippingCatID: rateRequest.ShippingCatID,
                modeOfTrans: rateRequest.ShippingModeCode,

                effectiveFrom: this.fromDate && this.fromDate.month ? this.fromDate.month + '/' + this.fromDate.day + '/' + this.fromDate.year : null,
                effectiveTo: this.toDate && this.toDate.month ? this.toDate.month + '/' + this.toDate.day + '/' + this.toDate.year : null,
                jsonContainerDet: this.getSaveContainer(pricingJSON),
                partnerID: null,

                currencyID: _currencyID,
                totalAmount: _totalAmount,
                totalBaseAmount: _totalBaseAmount,
                exchangeRate: _exchangeRate ? _exchangeRate.rate : null,
                currExcgRateListID: _exchangeRate ? _exchangeRate.currExcgRateListID : null,
                routeAnimation: null,
                createdBy: this.userProfile.UserID,
                perUnitRateType: this.rateType ? this.rateType : null,
                cargoDetail: this.getCargoDetails(this.requestData.forType),
                containerLoad: this.requestData.rateRequest.ContainerLoad,
                jsonCargoDetail: this.getJsonCargoDetails(this.searchCriteria, _currUtil),
            }
            if (this.searchMode === 'sea-fcl' || this.searchMode === 'sea-lcl' || this.searchMode === 'air-lcl') {
                if (!selectedShipping || !selectedShipping.id || typeof selectedShipping === 'string') {
                    if (this.requestData.rateRequest.ShippingModeCode === 'SEA')
                        _toast.warning('Please select shipping line', 'Shipping Line Missing')
                    else
                        _toast.warning('Please select airline', 'Airline Missing')
                    return null
                }
                if (this.searchMode !== 'air-lcl') {
                    if (this.hasScheduleChanged) {
                        const _vesselSchedule = this.generateVesselSchedule()
                        obj = {
                            ...obj,
                            isScheduleUpdate: true,
                            jsonTransferVesselsDetails: _vesselSchedule ? JSON.stringify(_vesselSchedule) : null,
                            routeAnimation: _vesselSchedule ? _vesselSchedule.RouteAnimation : null
                        }
                        if (!_vesselSchedule && obj.responseID <= 0) {
                            const _vesselSchedule = this.generateVesselSchedule()
                            obj = {
                                ...obj,
                                isScheduleUpdate: false,
                                jsonTransferVesselsDetails: _vesselSchedule ? JSON.stringify(_vesselSchedule) : null,
                            }
                        }
                    } else if (this.schdeuleHasData()) {
                        const _vesselSchedule = this.generateVesselSchedule()
                        obj = {
                            ...obj,
                            isScheduleUpdate: false,
                            jsonTransferVesselsDetails: _vesselSchedule ? JSON.stringify(_vesselSchedule) : null,
                        }
                    }
                }
            }
            if (this.searchMode === 'sea-lcl' || this.searchMode === 'air-lcl') {
                obj = {
                    ...obj,
                    perUnitRate: pricingJSON[0].price,
                    perUnitBaseRate: pricingJSON[0].basePrice,
                    perUnitRateCurrencyID: pricingJSON[0].currencyID,
                    perUnitExchangeRate: pricingJSON[0].exchangeRate,
                }
            }
        } catch (error) {
            console.warn('Save Object Generation Error:', error)
        }
        return obj
    }

    getJsonCargoDetails(searchCriteria: SearchCriteria, _currUtil) {
        if (this.requestData.forType === 'FCL' || this.requestData.forType === 'FTL') {
            return this.inputContainers.map(_cont => ({
                calculationMethod: this.requestData.forType === 'FCL' ? 'by_container' : 'by_truck',
                contSpecID: _cont.container.ContainerSpecID,
                contSpecDesc: _cont.container.ContainerSpecDesc,
                packageType: _cont.container.ContainerSpecCode,
                contSpecType: this.requestData.forType === 'FCL' ? 'Container' : 'Truck',
                respondedQty: this.getNum(_cont.resQty),
                contIndexID: _cont && _cont.container && _cont.container.ContIndexID ? _cont.container.ContIndexID : null
            }))
        } else if (this.searchMode === 'sea-lcl') {
            const jsonCargoDetail = []
            const { containers } = this
            try {
                for (let index = 0; index < searchCriteria.LclChips.length; index++) {
                    const _input = searchCriteria.LclChips[index];
                    const _container = containers[index];
                    if (!_input.toggle) {
                        jsonCargoDetail.push({
                            quantity: _input.quantity,
                            lengthUnitID: _input.lengthUnitID,
                            weightUnitID: _input.weightUnitID,
                            volumeUnitID: _input.volumeUnitID,
                            length: _input.length,
                            width: _input.width,
                            height: _input.height,
                            weight: _input.weight as any,
                            volume: _input.volume,
                            packageType: _input.packageType,
                            contSpecID: _input.contSpecID,
                            lengthUnit: _input.lengthUnit,
                            weightUnit: _input.weightUnit,
                            volumeUnit: 'cbm',
                            calculationMethod: 'by_unit',
                            contSpecDesc: this.containers.filter(_cont => _cont.ContainerSpecID === _input.contSpecID)[0].ContainerSpecDesc,
                            contSpecType: 'Package',
                            respondedCBM: this.getNum(_input.contRequestedCBM),
                            respondedWeight: this.getNum(_input.inpTotalWeight),
                            respondedVolumetricWeight: _input.volumetricWeight,
                            contIndexID: _container && _container && _container.ContIndexID ? _container.ContIndexID : null
                        })
                    } else {
                        jsonCargoDetail.push({
                            calculationMethod: 'by_volume',
                            quantity: _input.quantity,
                            weightUnitID: _input.weightUnitID,
                            volumeUnitID: _input.volumeUnitID,
                            weight: _input.weight as any,
                            volume: _input.volume,
                            packageType: _input.packageType,
                            contSpecID: _input.contSpecID,
                            contSpecDesc: this.containers.filter(_cont => _cont.ContainerSpecID === _input.contSpecID)[0].ContainerSpecDesc,
                            contSpecType: 'Package',
                            weightUnit: _input.weightUnit,
                            volumeUnit: 'cbm',
                            respondedCBM: this.getNum(_input.contRequestedCBM),
                            respondedWeight: this.getNum(_input.inpTotalWeight),
                            respondedVolumetricWeight: _input.volumetricWeight,
                            contIndexID: _container && _container && _container.ContIndexID ? _container.ContIndexID : null
                        })
                    }
                }
            } catch (error) {
                // console.log(error)
            }
            return jsonCargoDetail
        } else if (this.searchMode === 'air-lcl') {
            const { airChipData } = searchCriteria
            const jsonCargoDetail = []
            if (airChipData.cargoLoadType === 'by_unit') {
                console.log(airChipData)
                const _airChips: LclChip[] = airChipData.byUnitsChips
                const volWeight = _airChips.map(_chip => _chip.chipWeight).reduce((all, item) => (all + item), 0)
                const actWeight = _airChips.map(_chip => _chip.actualWeight).reduce((all, item) => (all + item), 0)

                const isActualWghtApp = actWeight > volWeight ? true : false

                airChipData.byUnitsChips.forEach(_chip => {
                    const respWeight = isActualWghtApp ? _chip.actualWeight : _chip.chipWeight
                    jsonCargoDetail.push({
                        CalculationMethod: _chip.cargoLoadType,
                        ContSpecID: null,
                        ContSpecDesc: null,
                        ContSpecType: null,
                        ContIndexID: this.inputContainers[0].contIndexID ? this.inputContainers[0].contIndexID : null,
                        LengthUnitID: _chip.lengthUnitID,
                        WeightUnitID: _chip.weightUnitID,
                        VolumeUnitID: _chip.volumeUnitID,
                        Quantity: _chip.quantity,
                        Length: _chip.length,
                        Width: _chip.width,
                        Height: _chip.height,
                        Weight: _chip.actualWeight ? _currUtil.applyRoundByDecimal(_chip.actualWeight, 3) : 0,
                        Volume: _chip.volume,
                        PackageType: _chip.packageType,
                        LengthUnit: _chip.lengthUnit,
                        WeightUnit: _chip.weightUnit,
                        VolumeUnit: _chip.volumeUnit,
                        RespondedQty: null,
                        RespondedCBM: null,
                        RespondedWeight: respWeight ? _currUtil.applyRoundByDecimal(respWeight, 3) : 0,
                        ChipWeight: _chip.chipWeight ? _currUtil.applyRoundByDecimal(_chip.chipWeight, 3) : 0,
                        RespondedVolumetricWeight: null,
                    })
                })
            } else {
                jsonCargoDetail.push({
                    CalculationMethod: airChipData.cargoLoadType,
                    ContSpecID: null,
                    ContSpecDesc: null,
                    ContSpecType: null,
                    ContIndexID: this.inputContainers[0].contIndexID ? this.inputContainers[0].contIndexID : null,
                    LengthUnitID: airChipData.lengthUnitID,
                    WeightUnitID: airChipData.weightUnitID,
                    VolumeUnitID: airChipData.volumeUnitID,
                    Quantity: airChipData.quantity,
                    Length: airChipData.length,
                    Width: airChipData.width,
                    Height: airChipData.height,
                    Weight: airChipData.weight,
                    Volume: airChipData.volume,
                    PackageType: airChipData.packageType,
                    LengthUnit: airChipData.lengthUnit,
                    WeightUnit: airChipData.weightUnit,
                    VolumeUnit: airChipData.volumeUnit,
                    RespondedQty: null,
                    RespondedCBM: null,
                    RespondedWeight: searchCriteria.chargeableWeight ? _currUtil.applyRoundByDecimal(searchCriteria.chargeableWeight, 3) : 0,
                    ChipWeight: airChipData.chipWeight ? _currUtil.applyRoundByDecimal(airChipData.chipWeight, 3) : 0,
                    RespondedVolumetricWeight: null,
                })
            }

            return jsonCargoDetail
        }
    }

    getSaveContainer(pricingJSON: JsonContainerDet[]) {
        if (this.searchMode === 'sea-lcl' || this.searchMode === 'air-lcl') {
            const [_lclPrice] = pricingJSON
            return pricingJSON.map(({ containerData, ...contRate }) => {
                const neoJSON = {
                    ...contRate,
                    price: _lclPrice.price,
                    basePrice: _lclPrice.basePrice,
                    currencyID: _lclPrice.currencyID,
                    exchangeRate: _lclPrice.exchangeRate,
                }
                if (this.searchMode === 'sea-lcl') {
                    return {
                        ...neoJSON,
                        contIndexID: containerData && containerData.ContIndexID ? containerData.ContIndexID : null
                    }
                } else {
                    return {
                        ...neoJSON,
                        contIndexID: contRate && contRate.contIndexID ? contRate.contIndexID : null
                    }
                }
            })
        } else {
            return pricingJSON
                .filter(_cont => this.getNum(_cont.respondedQty) > 0 && (this.getNum(_cont.price) > 0 || isArrayValid(_cont.jsonSurchargeDet, 0)))
                .map(({ containerData, ...contRate }) => ({
                    ...contRate,
                    contIndexID: containerData && containerData.ContIndexID ? containerData.ContIndexID : null
                }))
        }
    }

    getCutOffDate() {
        try {
            const { year, month, day } = this.fromDate
            return moment(new Date(year, month - 1, day)).subtract(3, 'days').format('YYYY-MM-DD')
        } catch (err) {
            console.log(err)
            return null
        }
    }

    getTransferStop() {
        switch (this.selectedStops) {
            case 'direct':
                return 0;
            case '1 t/s':
                return 1;
            case '2 t/s':
                return 2;
            default:
                return 0;
        }
    }

    getTransferStopStr(stops: number) {
        switch (stops) {
            case 0:
                return 'direct';
            case 1:
                return '1 t/s';
            case 2:
                return '2 t/s';
            default:
                return 'direct';
        }
    }

    getDatefromObj(dateObject) {
        try {
            return moment(new Date(dateObject.year, dateObject.month - 1, dateObject.day)).format('YYYY-MM-DD')
        } catch (error) { return null }
    }

    getArrivalDate() {
        return moment(new Date(this.departureDate.year, this.departureDate.month - 1, this.departureDate.day))
            .add(this.getNum(this.selectionDuration), 'days').format('YYYY-MM-DD')
    }

    generateRouteDet(): RouteDetails {
        const _transferStops = this.getTransferStop()

        let _routeDet: RouteDetails = {
            RouteCode1: 'Upon Booking',
            RouteName1: '',
            R1PolID: this.filterOrigin.id,
            R1PolCode: this.filterOrigin.code,
            R1PolName: this.filterOrigin.title.replace(this.filterOrigin.code + ', ', ''),
            R1PodID: this.filterDestination.id,
            R1PodCode: this.filterDestination.code,
            R1PodName: this.filterDestination.title.replace(this.filterOrigin.code + ', ', ''),
            R1EtdDate: this.departureDate && this.departureDate.day ? this.getDatefromObj(this.departureDate) : null,
            R1EtaDate: this.getArrivalDate(),
            R1VesselName: 'Upon Booking',
            R1VesselNo: 'Upon Booking',
            R1VoyageRefNo: 'Upon Booking',
        }
        if (_transferStops > 0) {
            _routeDet = {
                ..._routeDet,
                RouteCode2: '',
                RouteName2: '',

                R1PolID: this.filterOrigin.id,
                R1PolCode: this.filterOrigin.code,
                R1PolName: this.filterOrigin.title.replace(this.filterOrigin.code + ', ', ''),

                R1PodID: this.t1Route && this.t1Route.id ? this.t1Route.id : null,
                R1PodCode: this.t1Route && this.t1Route.id ? this.t1Route.code : null,
                R1PodName: this.t1Route && this.t1Route.id ? this.t1Route.title.replace(this.t1Route.code + ', ', '') : null,

                R2PolID: this.t1Route && this.t1Route.id ? this.t1Route.id : null,
                R2PolCode: this.t1Route && this.t1Route.id ? this.t1Route.code : null,
                R2PolName: this.t1Route && this.t1Route.id ? this.t1Route.title.replace(this.t1Route.code + ', ', '') : null,

                R2PodID: this.filterDestination.id,
                R2PodCode: this.filterDestination.code,
                R2PodName: this.filterDestination.title.replace(this.filterDestination.code + ', ', ''),

                R1EtdDate: this.getDatefromObj(this.departureDate),
                R1EtaDate: this.t1Arrival && this.t1Arrival.day ? this.getDatefromObj(this.t1Arrival) : null,
                R2EtdDate: this.t1Departure && this.t1Departure.day ? this.getDatefromObj(this.t1Departure) : null,
                R2EtaDate: this.getArrivalDate(),

                R2VesselName: 'Upon Booking',
                R2VesselNo: 'Upon Booking',
                R2VoyageRefNo: 'Upon Booking',
            }
        }
        if (_transferStops > 1) {
            _routeDet = {
                ..._routeDet,
                RouteCode2: '',
                RouteName2: '',

                R1PolID: this.filterOrigin.id,
                R1PolCode: this.filterOrigin.code,
                R1PolName: this.filterOrigin.title.replace(this.filterOrigin.code + ', ', ''),

                R1PodID: this.t1Route && this.t1Route.id ? this.t1Route.id : null,
                R1PodCode: this.t1Route && this.t1Route.id ? this.t1Route.code : null,
                R1PodName: this.t1Route && this.t1Route.id ? this.t1Route.title.replace(this.t1Route.code + ', ', '') : null,

                R2PolID: this.t1Route && this.t1Route.id ? this.t1Route.id : null,
                R2PolCode: this.t1Route && this.t1Route.id ? this.t1Route.code : null,
                R2PolName: this.t1Route && this.t1Route.id ? this.t1Route.title.replace(this.t1Route.code + ', ', '') : null,

                R2PodID: this.t2Route && this.t2Route.id ? this.t2Route.id : null,
                R2PodCode: this.t2Route && this.t2Route.id ? this.t2Route.code : null,
                R2PodName: this.t2Route && this.t2Route.id ? this.t2Route.title.replace(this.t2Route.code + ', ', '') : null,

                R3PolID: this.t2Route && this.t2Route.id ? this.t2Route.id : null,
                R3PolCode: this.t2Route && this.t2Route.id ? this.t2Route.code : null,
                R3PolName: this.t2Route && this.t2Route.id ? this.t2Route.title.replace(this.t2Route.code + ', ', '') : null,

                R3PodID: this.filterDestination.id,
                R3PodCode: this.filterDestination.code,
                R3PodName: this.filterDestination.title.replace(this.filterDestination.code + ', ', ''),

                R1EtdDate: this.departureDate && this.departureDate.day ? this.getDatefromObj(this.departureDate) : null,
                R1EtaDate: this.t1Arrival && this.t1Arrival.day ? this.getDatefromObj(this.t1Arrival) : null,
                R2EtdDate: this.t1Departure && this.t1Departure.day ? this.getDatefromObj(this.t1Departure) : null,
                R2EtaDate: this.t2Arrival && this.t2Arrival.day ? this.getDatefromObj(this.t2Arrival) : null,
                R3EtdDate: this.t2Departure && this.t2Departure.day ? this.getDatefromObj(this.t2Departure) : null,
                R3EtaDate: this.departureDate && this.departureDate.day ? this.getArrivalDate() : null,

                R3VesselName: 'Upon Booking',
                R3VesselNo: 'Upon Booking',
                R3VoyageRefNo: 'Upon Booking',
            }
        }

        return _routeDet
    }

    generateAnimation(_route, stops): string {
        try {
            let str = ''
            if (stops === 0) {
                str = `${_route.R1PolCode}|SEA|0^${_route.R1PodCode}|SEA|0`
            } if (stops === 1) {
                str = `${_route.R1PolCode}|SEA|0^${_route.R2PolCode}|SEA|0^${_route.R2PodCode}|SEA|0`
            } if (stops === 2) {
                str = `${_route.R1PolCode}|SEA|0^${_route.R2PolCode}|SEA|0^${_route.R3PolCode}|SEA|0^${_route.R3PodCode}|SEA|0`
            }
            return str
        } catch {
            return ''
        }
    }

    getCarrierID() {
        try {
            return this.requestData.rateRequest.Carrier.CarrierID
        } catch {
            return null
        }
    }

    generateVesselSchedule(): any {
        try {
            return {
                CarrierID: this.selectedShipping ? this.selectedShipping.id : this.getCarrierID(),
                TransferStops: this.getTransferStop(),
                CutOffDate: this.getDatefromObj(this.cutOffDate),
                TransitDays: this.selectionDuration,
                DepartureDate: this.getDatefromObj(this.departureDate),
                RouteDetails: this.generateRouteDet(),
                RouteAnimation: this.generateAnimation(this.generateRouteDet(), this.getTransferStop())
            }
        } catch (error) {
            // console.log('Vessel Sch Gen Error:', error)
            return null
        }
    }

    getCurrency = () =>
        this.requestData.rateRequest.CurrencyID ? this.requestData.rateRequest.CurrencyID : this.userProfile.CurrencyID

    validateAdditionalCharges(_toast) {
        let _orgChargeValid = true, _destChargeValid = true, _areChargesValid = true
        const { inputContainers } = this
        let hasNavigated = false
        for (let index = 0; index < inputContainers.length; index++) {
            const _inptCont = inputContainers[index];
            const { orginCharges, destinCharges } = _inptCont

            if (this.getNum(_inptCont.resQty) > 0) {
                if (isArrayValid(orginCharges, 0))
                    _orgChargeValid = this.validateCharges(orginCharges, _toast, index)
                if (isArrayValid(destinCharges, 0))
                    _destChargeValid = this.validateCharges(destinCharges, _toast, index)
                if (!_orgChargeValid || !_destChargeValid)
                    _areChargesValid = false
                console.log('this.selectedContainer', this.selectedContainer)
                if (!hasNavigated && !_areChargesValid) {
                    this.selectedContainer = 'cont_' + index
                    hasNavigated = true
                }
                console.log('this.selectedContainer', this.selectedContainer)
            }
        }
        return _areChargesValid
    }

    validateCharges(_charges: JsonSurchargeDet[], _toast, _containerInd: number): boolean {
        let _isChargeValid = true
        if (_charges && _charges.length > 0) {
            for (let index = 0; index < _charges.length; index++) {
                const _charge = _charges[index];
                if (_charge && _charge.addChrID) {
                    if (Object.keys(_charge).length && (!_charge.price || !(typeof parseFloat(_charge.price) === 'number') || parseFloat(_charge.price) <= 0)) {
                        _toast.error('Price is missing for Additional Charge', 'Error')
                        this.isRateUpdating = false
                        _isChargeValid = false
                        _charge.isValid = false
                        return _isChargeValid
                    } else if (Object.keys(_charge).length && (!_charge.currency || !_charge.currency.id)) {
                        _toast.error('Currency is missing for Additional Charge', 'Error')
                        this.isRateUpdating = false
                        _isChargeValid = false
                        _charge.isValid = false
                        return _isChargeValid
                    } else if (Object.keys(_charge).length && (!_charge.currency || typeof (_charge.currency) === 'string')) {
                        _toast.error('Currency is missing for Additional Charge', 'Error')
                        this.isRateUpdating = false
                        _isChargeValid = false
                        _charge.isValid = false
                        return _isChargeValid
                    } else if (Object.keys(_charge).length && !_charge.addChrID) {
                        _toast.error('Additional Charge is missing', 'Error')
                        this.isRateUpdating = false
                        _isChargeValid = false
                        _charge.isValid = false
                        return _isChargeValid
                    } else {
                        _charge.isValid = true
                    }
                }
            }
        }
        return _isChargeValid
    }

    getCargoDetails(containerLoad: string) {
        if (containerLoad === 'FCL') {
            const { inputContainers } = this
            const _contStr = []
            inputContainers.forEach(_cont => {
                if (this.getNum(_cont.resQty) > 0)
                    _contStr.push(`${_cont.container.ContainerSpecDesc} x ${_cont.resQty} ${_cont.resQty > 1 ? this.rateType + 's' : this.rateType}`)
            })
            return JSON.stringify(_contStr)
        } else if (containerLoad === 'FTL') {
            const { inputContainers, searchCriteria } = this
            const _contStr = []
            inputContainers.forEach(_cont => {
                let _contType = this.rateType
                try {
                    _contType = searchCriteria.SearchCriteriaContainerDetail.filter(_truck => _truck.contSpecID === _cont.container.ContainerSpecID)[0].containerDtl.ContainerSpecDesc
                } catch { }
                if (this.getNum(_cont.resQty) > 0)
                    _contStr.push(`${_cont.resQty} x ${_cont.container.ContainerSpecDesc} ${_contType} ${_cont.resQty > 1 ? this.rateType + 's' : this.rateType}`)
                // _contStr.push(`${_cont.container.ContainerSpecDesc} x ${_cont.resQty} ${_cont.resQty > 1 ? this.rateType + 's' : this.rateType}`)
            })
            return JSON.stringify(_contStr)
        } else if (this.searchMode === 'sea-lcl') {
            return JSON.stringify(this.searchCriteria.lclChipConatiners)
        } else if (this.searchMode === 'air-lcl') {
            return JSON.stringify(this.searchCriteria.lclAirContainerChips)
        } else {
            return null
        }
    }

    schdeuleHasData() {
        try {
            let _sch = false
            const _schData: IRequestSchedule = this.generateVesselSchedule()
            if (_schData.CutOffDate || _schData.DepartureDate || _schData.TransitDays) {
                _sch = true
            }
            return _sch
        } catch {
            return false
        }
    }

    validateInputs() {

        const { filterOrigin, filterDestination, inputContainers, selectedShipping, fromDate, toDate } = this
        this.isOriginInValid = (!filterOrigin || !filterOrigin.id) ? true : false
        this.isDestinInValid = (!filterDestination || !filterDestination.id) ? true : false
        this.isShippingInValid = (!selectedShipping || !selectedShipping.id) ? true : false
        this.isEffectiveInValid = (!fromDate || !fromDate.day || !toDate || !toDate.day) ? true : false
        // console.log(this.isEffectiveInValid)

        if (this.requestData.rateRequest.ShippingModeCode === 'SEA') {
            if (this.schdeuleHasData()) {
                const _schData: IRequestSchedule = this.generateVesselSchedule()
                this.isDepartureInValid = _schData.DepartureDate ? false : true
                this.isCutOffInValid = _schData.CutOffDate ? false : true
                this.isTransitInValid = _schData.TransitDays ? false : true
                if (_schData.TransferStops > 0) {
                    this.IsR1InValid = (!this.t1Route || !this.t1Route.id) ? true : false
                    if (_schData.TransitDays > 1) {
                        this.isR2InValid = (!this.t2Route || !this.t2Route.id) ? true : false;
                    }
                }
            } else {
                this.isDepartureInValid = false
                this.isCutOffInValid = false
                this.isTransitInValid = false
                this.IsR1InValid = false
                this.isR2InValid = false
            }
        }

        // Price Validation Section
        const _isMulti = inputContainers.length > 1 ? true : false
        const _perUnitPrice = inputContainers[0].price
        inputContainers.forEach(_inptCont => {
            const _originCharge: JsonSurchargeDet[] = isArrayValid(_inptCont.orginCharges, 0) ? _inptCont.orginCharges.filter(_charge => _charge.price) : []
            const _destCharge: JsonSurchargeDet[] = isArrayValid(_inptCont.destinCharges, 0) ? _inptCont.destinCharges.filter(_charge => _charge.price) : []
            const _jsonSurchargeDet: JsonSurchargeDet[] = _originCharge.concat(_destCharge)
            if (this.searchMode === 'sea-lcl' || this.searchMode === 'air-lcl' || this.searchMode === 'truck-ftl') {
                if (_perUnitPrice === null || _perUnitPrice === undefined || !(typeof parseFloat(_perUnitPrice as any) === 'number')) {
                    _inptCont.isValidPrice = false
                } else {
                    _inptCont.isValidPrice = true
                }
            } else {
                if (_inptCont.resQty && this.getNum(_inptCont.resQty) > this.getNum(_inptCont.reqQty)) {
                    _inptCont.isValidQty = false
                } else {
                    _inptCont.isValidQty = true
                }
                if (!_isMulti && (!_inptCont.resQty || this.getNum(_inptCont.resQty) === 0)) {
                    _inptCont.isValidQty = false
                } else {
                    _inptCont.isValidQty = true
                }
                // if (_isMulti) {
                const _qty: number = this.getNum(_inptCont.resQty)
                const _fsurPrice: number = this.getNum(_inptCont.price)
                const _addCharges: number = isArrayValid(_jsonSurchargeDet, 0) ? _jsonSurchargeDet.map(_charge => this.getNum(_charge.price)).reduce((all, item) => (all + item)) : 0
                const _totalRate = _fsurPrice + _addCharges
                if (_qty > 0 && (_totalRate === 0)) {
                    _inptCont.isValidPrice = false
                } else {
                    _inptCont.isValidPrice = true
                }
                if (_qty === 0 && (_totalRate > 0)) {
                    _inptCont.isValidQty = false
                } else {
                    _inptCont.isValidQty = true
                }
                // }
            }
        })
    }

}
