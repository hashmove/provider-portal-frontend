import { Component, OnInit, Input, Renderer2, ElementRef, Provider, ViewChild } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
// import { SearchResultService } from '../../../components/search-results/fcl-search/fcl-search.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';




@Component({
  selector: 'app-share-view-booking',
  templateUrl: './share-view-booking.component.html',
  styleUrls: ['./share-view-booking.component.scss']
})
export class ShareViewBooking implements OnInit {

  @Input() shareObjectInfo: any;
  @ViewChild('shareURL') shareURL: ElementRef
  public fromVendor: boolean = false;
  private PolCode: string = ''
  private PodCode: string = ''
  public info: any;
  shareDetailForm;
  loginUser;
  toemailError;
  fromemailError;
  noteError;
  subjectError;
  loading;
  public selectedProvider: any = {};
  constructor(
    private _activeModal: NgbActiveModal,
    private _renderer: Renderer2,
    private el: ElementRef,
    private _toast: ToastrService,
    // private _searchResult: SearchResultService,
    private location: PlatformLocation,
    private _router: Router
  ) {
    location.onPopState(() => this.closeModal());
  }
  ngOnInit() {
    // this.loginUser = JSON.parse(Tea.getItem('loginUser'));
    console.log('here')
  }

  closeModal() {
    this._activeModal.close();
    document.getElementsByTagName('html')[0].style.overflowY = 'auto';
  }

  public copyBtnText: string = 'Copy URL'
  copyURL(inputElement) {
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
    this.copyBtnText = 'URL Copied!'
    setTimeout(() => {
      this.copyBtnText = 'Copy URL'
    }, 1000);
  }
}
