import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmDialogGenComponent } from './confirm-dialog-generic.component';

describe('ConfirmDialogGenComponent', () => {
  let component: ConfirmDialogGenComponent;
  let fixture: ComponentFixture<ConfirmDialogGenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmDialogGenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmDialogGenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
