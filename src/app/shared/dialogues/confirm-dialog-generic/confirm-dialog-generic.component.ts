import { Component, OnInit, Input } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";



@Component({
  selector: 'app-confirm-dialog-generic',
  templateUrl: './confirm-dialog-generic.component.html',
  styleUrls: ['./confirm-dialog-generic.component.scss']
})
export class ConfirmDialogGenComponent implements OnInit {

  @Input() modalData: ConfirmDialogContent = {
    messageTitle: '',
    messageContent: '',
    buttonTitle: '',
    data: null
  }


  constructor(
    private _activeModal: NgbActiveModal,
    private location: PlatformLocation
  ) {
    location.onPopState(() => this.closeModal(true));
  }

  ngOnInit() {

  }
  closeModal(event) {
    this._activeModal.close(event);
    document.getElementsByTagName('html')[0].style.overflowY = 'auto';
  }

  onConfirmClick(event) {
    event.stopPropagation();
    this.closeModal(true)
  }
}



export interface ConfirmDialogContent {
  messageTitle: string,
  messageContent: string,
  buttonTitle: string,
  data: any
}
