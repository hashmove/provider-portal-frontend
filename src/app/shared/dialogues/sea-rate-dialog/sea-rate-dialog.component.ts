import { Component, OnInit, ViewEncapsulation, EventEmitter, ViewChild, Renderer2, ElementRef, Input, Output, OnDestroy } from "@angular/core";
import { PlatformLocation } from "@angular/common";
import { NgbActiveModal, NgbDropdownConfig } from "@ng-bootstrap/ng-bootstrap";
import { SharedService } from "../../../services/shared.service";
import { Observable, Subject } from "rxjs";
import { ToastrService } from "ngx-toastr";
import { debounceTime, distinctUntilChanged, map } from "rxjs/operators";
import { NgbInputDatepicker, NgbDateStruct, NgbCalendar, NgbDateParserFormatter, } from "@ng-bootstrap/ng-bootstrap";
import { NgbDateFRParserFormatter } from "../../../constants/ngb-date-parser-formatter";
import { SeaFreightService } from "../../../components/pages/user-desk/manage-rates/sea-freight/sea-freight.service";
import { cloneObject } from "../../../components/pages/user-desk/reports/reports.component";
import { changeCase, loading, getImagePath, ImageSource, ImageRequiredSize, getLoggedUserData, isMobile } from "../../../constants/globalFunctions";
import * as moment from "moment";
import { ManageRatesService } from "../../../components/pages/user-desk/manage-rates/manage-rates.service";
import { firstBy } from 'thenby'
import { CodeValMst, UserInfo } from "../../../interfaces/billing.interface";
import { AddRateHelpers } from "../../../helpers/add-rate.helper";

const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
  one &&
  two &&
  two.year === one.year &&
  two.month === one.month &&
  two.day === one.day;

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two
    ? false
    : one.year === two.year
      ? one.month === two.month
        ? one.day === two.day
          ? false
          : one.day < two.day
        : one.month < two.month
      : one.year < two.year;

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two
    ? false
    : one.year === two.year
      ? one.month === two.month
        ? one.day === two.day
          ? false
          : one.day > two.day
        : one.month > two.month
      : one.year > two.year;
@Component({
  selector: "app-sea-rate-dialog",
  templateUrl: "./sea-rate-dialog.component.html",
  encapsulation: ViewEncapsulation.None,
  providers: [
    { provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter },
    NgbDropdownConfig
  ],
  styleUrls: ["./sea-rate-dialog.component.scss"],
  host: {
    "(document:click)": "closeDropdown($event)"
  }
})
export class SeaRateDialogComponent extends AddRateHelpers implements OnInit, OnDestroy {
  @ViewChild("dp") input: NgbInputDatepicker;
  @ViewChild("dp2") inputDed: NgbInputDatepicker;
  @ViewChild("rangeDp") rangeDp: ElementRef;
  @ViewChild("rangeDp2") rangeDp2: ElementRef;
  @ViewChild("rangeDpDed") rangeDpDed: ElementRef;
  @ViewChild("originPickupBox") originPickupBox: ElementRef;
  @ViewChild("destinationPickupBox") destinationPickupBox: ElementRef;
  @ViewChild("originDropdown") originDropdown: any;
  @ViewChild("originDropdownDed") originDropdownDed: any;
  @ViewChild("destinationDropdown") destinationDropdown;
  @Input() selectedData: any;
  @Output() savedRow = new EventEmitter<any>();
  @Input() warehouseUnit: string = null;

  @ViewChild("carrier") carrier: ElementRef;
  @ViewChild("origin") origin: ElementRef;
  @ViewChild("destination") destination: ElementRef;
  @ViewChild("cargo") cargo: ElementRef;
  @ViewChild("container") container: ElementRef;

  citiesResults: Object;
  searchTerm$ = new Subject<string>();

  isMobile = isMobile()

  public allShippingLines: any[] = [];
  public allCargoType: any[] = [];
  public allContainersType: any[] = [];
  public allContainers: any[] = [];
  public allHandlingType: any[] = [];
  public allCustomers: any[] = [];
  public allPorts: any[] = [];
  public seaPorts: any[] = [];
  public allCurrencies: any[] = [];
  private allRatesFilledData: any[] = [];
  public filterOrigin: any = {};
  public filterDestination: any = {};
  public userProfile: UserInfo;
  public selectedCategory: any = null;
  public selectedContSize: any = null;
  public selectedHandlingUnit: any;
  public selectedCustomer: any[] = [];
  public selectedShipping: any;
  public selectedPrice: any;
  public selectedPriceDed: any;
  public couplePrice: any;
  public thirdPrice: any;
  public couplePriceDed: any;
  public thirdPriceDed: any;
  public defaultCurrency: any = {
    id: 101,
    shortName: "USD",
    imageName: 'US'
  };
  public selectedCurrency: any = this.defaultCurrency;
  public selectedCurrencyDed: any = this.defaultCurrency;
  public startDate: NgbDateStruct;
  public maxDate: NgbDateStruct;
  public minDate: NgbDateStruct;
  public hoveredDate: NgbDateStruct;
  public fromDate: any = {
    day: null,
    month: undefined,
    year: undefined
  };
  public toDate: any = {
    day: undefined,
    month: undefined,
    year: undefined
  };
  public startDateDed: NgbDateStruct;
  public maxDateDed: NgbDateStruct;
  public minDateDed: NgbDateStruct;
  public hoveredDateDed: NgbDateStruct;
  public fromDateDed: any = {
    day: null,
    month: undefined,
    year: undefined
  };
  public toDateDed: any = {
    day: undefined,
    month: undefined,
    year: undefined
  };
  public model: any;
  private newProviderPricingDraftID = undefined;
  public disableWarehouse: boolean = false;
  isHovered = date =>
    this.fromDate &&
    !this.toDate &&
    this.hoveredDate &&
    after(date, this.fromDate) &&
    before(date, this.hoveredDate);
  isInside = date => after(date, this.fromDate) && before(date, this.toDate);
  isFrom = date => equals(date, this.fromDate);
  isTo = date => equals(date, this.toDate);

  isHoveredDed = date =>
    this.fromDateDed &&
    !this.toDateDed &&
    this.hoveredDateDed &&
    after(date, this.fromDateDed) &&
    before(date, this.hoveredDateDed);
  isInsideDed = date => after(date, this.fromDateDed) && before(date, this.toDateDed);
  isFromDed = date => equals(date, this.fromDateDed);
  isToDed = date => equals(date, this.toDateDed);

  public destinationsList = [];
  public originsList = [];
  public originsListDed = [];
  // public userInfo: any;
  public selectedOrigins: any = [{}];
  public selectedDestinations: any = [{}];
  public selectedOriginsDed: any = [{}];
  public selectedDestinationsDed: any = [{}];
  public disabledCustomers: boolean = false;
  public containerLoadParam: string = "FCL";
  public userCurrency: number;
  public TotalImportCharges: number = 0;
  public TotalExportCharges: number = 0;
  public warehouseTypes: any[] = [];
  public storageType: string = "";
  whPricingID: any;
  public pricingJSON: any[] = [];

  singlePriceTag: string = ''
  singlePriceTagDed: string = ''
  doublePriceTag: string = ''
  thirdPriceTag: string = ''
  thirdPriceTagDed: string = ''
  doublePriceTagDed: string = ''
  public loading: boolean = false

  @ViewChild("elChargeSQM") elChargeSQM: any;
  @ViewChild("elChargePLT") elChargePLT: any;
  @ViewChild("elChargeSQFT") elChargeSQFT: any;

  @ViewChild("elChargeSQMDed") elChargeSQMDed: any;
  @ViewChild("elChargePLTDed") elChargePLTDed: any;
  @ViewChild("elChargeSQFTDed") elChargeSQFTDed: any;

  @ViewChild("elAddOnSQM") elAddOnSQM: any;
  disableElAddOnSQM = false
  @ViewChild("elAddOnPLT") elAddOnPLT: any;
  disableElAddOnPLT = false
  @ViewChild("elAddOnSQFT") elAddOnSQFT: any;
  disableElAddOnSQFT = false

  @ViewChild("elAddOnSQMDed") elAddOnSQMDed: any;
  disableElAddOnSQMDed = false
  @ViewChild("elAddOnPLTDed") elAddOnPLTDed: any;
  disableElAddOnPLTDed = false
  @ViewChild("elAddOnSQFTDed") elAddOnSQFTDed: any;
  disableElAddOnSQFTDed = false

  tempBasis
  tempBasisDed

  tempAddOnBasis
  tempAddOnBasisPrice
  tempAddOnBasisDed
  tempAddOnBasisDedPrice

  originSelectedMode = 'SEA'
  destinSelectedMode = 'SEA'
  public surchargesList: CodeValMst[] = [];
  disableTerms: boolean = false
  private toolbarOptions = [
    ['bold', 'italic', 'underline'],        // toggled buttons
    [{ 'list': 'ordered' }, { 'list': 'bullet' }],
    [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
    [{ 'direction': 'rtl' }],                         // text direction
    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
    [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
    [{ 'align': [] }],
    ['clean']                                         // remove formatting button
  ];
  public editorOptions = {
    placeholder: "insert content...",
    modules: {
      toolbar: this.toolbarOptions
    }
  };
  @Input() editorContent
  displayMonths = this.isMobile ? 1 : 2


  constructor(
    private location: PlatformLocation,
    private _activeModal: NgbActiveModal,
    private _sharedService: SharedService,
    private _parserFormatter: NgbDateParserFormatter,
    private _manageRateService: ManageRatesService,
    private renderer: Renderer2,
    private _seaFreightService: SeaFreightService,
    private _toast: ToastrService,
    private config: NgbDropdownConfig,
    calendar: NgbCalendar
  ) {
    super()
    location.onPopState(() => this.closeModal(null));
    config.autoClose = false;
  }

  ngOnInit() {
    try {
      const date = new Date();
      this.minDate = { month: date.getMonth() + 1, day: date.getDate(), year: date.getFullYear() };
    } catch (error) { }
    this._sharedService.currencyList.subscribe(res => {
      if (res) {
        this.allCurrencies = res;
      }
    });
    this.userProfile = getLoggedUserData()
    let data = null

    if (this.selectedData.mode === "publish") {
      data = changeCase(this.selectedData.data[0], "pascal");
    } else {
      data = changeCase(this.selectedData.data, "pascal");
    }
    let partnerId = null
    try {
      if (this.selectedData.mode === "publish") {
        partnerId = data.PartnerID
      } else if (this.selectedData.mode === "draft" && data) {
        partnerId = data.PartnerID
      }
    } catch (error) {
    }

    this.getAirPartners(this.userProfile.ProviderID, partnerId, this._manageRateService)
    this.setCurrency();
    this.setDateLimit();
    this.containerLoadParam =
      this.selectedData.forType === "FCL-Ground"
        ? "FCL"
        : this.selectedData.forType;
    if (this.selectedData.forType === "WAREHOUSE") {
      this.warehouseTypes = this.selectedData.drafts;
      this.getWarehousePricing();
    }
    this.allservicesBySea();
    if (this.selectedData.mode === "draft") {
      if (this.selectedData.data && this.selectedData.data.JsonSurchargeDet) {
        this.setEditData(this.selectedData.mode);
      } else {
        this.destinationsList = this.selectedData.addList;
        this.originsList = this.selectedData.addList;
        this.originsListDed = this.selectedData.addList
      }
    } else if (this.selectedData.mode === "publish") {
      if (this.selectedData.forType !== "WAREHOUSE") {
        if (
          this.selectedData.data &&
          this.selectedData.data[0].jsonSurchargeDet
        ) {
          this.setEditData(this.selectedData.mode);
        } else {
          this.destinationsList = this.selectedData.addList;
          this.originsList = this.selectedData.addList;
        }
      } else {
        if (this.selectedData.data && (this.selectedData.data.JsonSurchargeDet || this.selectedData.data.JsonDedicatedSurchargeDet)) {
          this.setEditData(this.selectedData.mode);
        } else {
          this.destinationsList = this.selectedData.addList;
          this.originsList = this.selectedData.addList;
          this.originsListDed = this.selectedData.addList;
        }
      }
    }
    this.allCustomers = this.selectedData.customers;
    this.getSurchargeBasis(this.containerLoadParam);
  }

  allservicesBySea() {
    this.getDropdownsList();
    if (
      this.selectedData &&
      this.selectedData.data &&
      this.containerLoadParam === "FCL"
    ) {
      if (this.selectedData.mode === "publish") {
        this.disabledCustomers = true;
        let data = changeCase(this.selectedData.data[0], "pascal");
        this.setData(data);
      } else {
        this.setData(this.selectedData.data);
      }
    } else if (
      this.selectedData &&
      this.selectedData.data &&
      (this.containerLoadParam === "LCL" || this.containerLoadParam === "FTL")
    ) {
      if (this.selectedData.mode === "publish") {
        this.disabledCustomers = true;
        let data = changeCase(this.selectedData.data[0], "pascal");
        this.setData(data);
      } else {
        this.setData(this.selectedData.data);
      }
    } else if (
      this.selectedData &&
      this.selectedData.data &&
      this.containerLoadParam === "WAREHOUSE"
    ) {
      if (this.selectedData.mode === "publish") {
        this.selectedData.data = changeCase(this.selectedData.data, "pascal");
        // let data = changeCase(this.selectedData.data, 'pascal')
        this.disabledCustomers = true;
        this.setData(this.selectedData.data);
      } else {
        this.setData(this.selectedData.data);
      }
    }
  }

  setDateLimit() {
    const date = new Date();
    this.minDate = {
      year: date.getFullYear(),
      month: date.getMonth() + 1,
      day: date.getDate()
    };
    // this.maxDate = {
    //   year: ((this.minDate.month === 12 && this.minDate.day >= 17) ? date.getFullYear() + 1 : date.getFullYear()),
    //   month:
    //     moment(date)
    //       .add(15, "days")
    //       .month() + 1,
    //   day: moment(date)
    //     .add(15, "days")
    //     .date()
    // };
  }

  setData(data) {
    let parsed = "";

    let partnerId = null
    try {
      if (this.selectedData.mode === "publish") {
        partnerId = data.PartnerID
      } else if (this.selectedData.mode === "draft" && data) {
        partnerId = data.PartnerID
      }
    } catch (error) {
    }

    this.getAirPartners(this.userProfile.ProviderID, partnerId, this._manageRateService)
    this.selectedCategory = data.ShippingCatID;
    this.cargoTypeChange(this.selectedCategory);
    this.selectedContSize = data.ContainerSpecID;
    if (data.PolType === "Ground") {
      this.filterOrigin = this.groundPorts.find(
        obj => obj.PortID == data.PolID
      );
    } else if (data.PolType === "CITY") {
      this.originSelectedMode = 'CITY'
      this._manageRateService
        .getAllCities(data.PolName)
        .pipe(
          debounceTime(400),
          distinctUntilChanged()
        )
        .subscribe(
          (res: any) => {
            // this.showDoubleRates = false;
            if (res.length > 1) {
              this.filterOrigin = res.filter(_city => _city.id === data.PolID)[0]
            } else {
              this.filterOrigin = res[0]
            }
          },
          (err: any) => {
          }
        );
    } else {
      this.filterOrigin = this.seaPorts.find(obj => obj.PortID == data.PolID);
    }
    if (data.PodType === "Ground") {
      this.filterDestination = this.groundPorts.find(
        obj => obj.PortID == data.PodID
      );
    } else if (data.PodType === "CITY") {
      this.destinSelectedMode = 'CITY'
      this._manageRateService
        .getAllCities(data.PodName)
        .pipe(
          debounceTime(400),
          distinctUntilChanged()
        )
        .subscribe(
          (res: any) => {
            // this.showDoubleRates = false;
            if (res.length > 1) {
              this.filterDestination = res.filter(_city => _city.id === data.PodID)[0]
            } else {
              this.filterDestination = res[0]
            }
          },
          (err: any) => {
          }
        );
    } else {
      this.filterDestination = this.seaPorts.find(
        obj => obj.PortID == data.PodID
      );
    }

    if (this.selectedData.forType === 'WAREHOUSE' && this.selectedData.mode === "publish") {
      this.storageType = data.StorageType;
      const parsedPricingJson = JSON.parse(data.PricingJson);
      this.sharedWarehousePricing = parsedPricingJson;
      this.disableWarehouse = true;
      const { sharedWarehousePricing } = this;
      try {
        sharedWarehousePricing.forEach(adCharge => {
          const { priceBasis, price } = adCharge;
          //Shared Warehouse
          if (priceBasis.includes("PER_SQM") && !priceBasis.includes('DED')) {
            this.selectedPrice = price;
            this.tempBasis = priceBasis
            this.tempAddOnBasis = adCharge.addOnBasis
            this.addOnSQMPrice = adCharge.addOnPrice
          } else if (priceBasis.includes("PER_SQFT") && !priceBasis.includes('DED')) {
            this.couplePrice = price;
            this.tempBasis = priceBasis
            this.tempAddOnBasis = adCharge.addOnBasis
            this.addOnSQFTPrice = adCharge.addOnPrice
          } else if (priceBasis.includes("PER_PLT") && !priceBasis.includes('DED')) {
            this.thirdPrice = price;
            this.tempBasis = priceBasis
            this.tempAddOnBasis = adCharge.addOnBasis
            this.addOnPLTPrice = adCharge.addOnPrice
          } else if (priceBasis.includes("PER_SQM") && priceBasis.includes('DED')) {
            this.selectedPriceDed = price;
            this.tempBasisDed = priceBasis
            this.tempAddOnBasisDed = adCharge.addOnBasis
            this.addOnSQMPriceDed = adCharge.addOnPrice
          } else if (priceBasis.includes("PER_SQFT") && priceBasis.includes('DED')) {
            this.couplePriceDed = price;
            this.tempBasisDed = priceBasis
            this.tempAddOnBasisDed = adCharge.addOnBasis
            this.addOnSQFTPriceDed = adCharge.addOnPrice
          } else if (priceBasis.includes("PER_PLT") && priceBasis.includes('DED')) {
            this.thirdPriceDed = price;
            this.tempBasisDed = priceBasis
            this.tempAddOnBasisDed = adCharge.addOnBasis
            this.addOnPLTPriceDed = adCharge.addOnPrice
          }
          //Full Warehouse
          else if (priceBasis === "PER_MONTH") {
            this.selectedPrice = price;
          } else if (priceBasis === "PER_YEAR") {
            this.couplePrice = price;
          }
        });
      } catch (error) {
      }
      this.selectedCurrency = this.allCurrencies.find(
        obj => obj.id === this.selectedData.data.CurrencyID
      );
      this.selectedCurrencyDed = this.allCurrencies.find(
        obj => obj.id === this.selectedData.data.DedicatedCurrencyID
      );
    } else if (this.selectedData.forType === 'WAREHOUSE' && this.selectedData.mode === "draft") {
      this.disabledCustomers = false;
    } else {
      this.disabledCustomers = true;
      this.selectedPrice = data.Price;
      this.selectedCurrency = this.allCurrencies.find(
        obj => obj.id === data.CurrencyID
      );
      this.selectedCurrencyDed = this.allCurrencies.find(
        obj => obj.id === data.DedicatedCurrencyID
      );
    }

    this.containerChange(data.ContainerSpecID);

    if (data.CouplePrice) {
      this.couplePrice = data.CouplePrice;
      this.showDoubleRates = true;
    }

    this.selectedShipping = this.allShippingLines.find(
      obj => obj.id == data.CarrierID
    );

    if (data.JsonCustomerDetail && data.CustomerType !== "null") {
      this.selectedCustomer = JSON.parse(data.JsonCustomerDetail);

    }

    if (data.EffectiveFrom) {
      this.fromDate.day = new Date(data.EffectiveFrom).getDate();
      this.fromDate.year = new Date(data.EffectiveFrom).getFullYear();
      this.fromDate.month = new Date(data.EffectiveFrom).getMonth() + 1;
    }
    if (data.EffectiveTo) {
      this.toDate.day = new Date(data.EffectiveTo).getDate();
      this.toDate.year = new Date(data.EffectiveTo).getFullYear();
      this.toDate.month = new Date(data.EffectiveTo).getMonth() + 1;
    }
    if (!this.selectedCurrency) {
      this.selectedCurrency = this.defaultCurrency;
    }

    if (this.fromDate && this.fromDate.day) {
      this.model = this.fromDate;
      parsed += this._parserFormatter.format(this.fromDate);
    }
    if (this.toDate && this.toDate.day) {
      parsed += " - " + this._parserFormatter.format(this.toDate);
    }
    this.rangeDp.nativeElement.value = parsed;
    this.rangeDpDed.nativeElement.value = parsed;

  }

  cargoTypeChange(type) {
    if (this.transPortMode === "SEA") {
      let data = this.fclContainers.filter(obj => obj.ShippingCatID == parseInt(type));
      this.allContainers = data;
    } else if (this.transPortMode === "GROUND") {
      let data = this.combinedContainers.filter(obj => obj.ShippingCatID == parseInt(type) && obj.ContainerFor === 'FTL');
      const containers = data.filter(
        e => e.ContainerSpecGroupName === "Container"
      );
      const trucks = data.filter(
        e => e.ContainerSpecGroupName != "Container"
      );
      this.allContainers = containers.concat(trucks);
    }
  }

  isRateUpdating = false;
  /**
   * [On Save Button Click Action]
   * @param  type [string] fcl/lcl/ftl/fcl-ground
   * @return      [description]
   */
  savedraftrow(type) {
    this.loading = true
    if (this.isRateUpdating) {
      return;
    }
    this.isRateUpdating = true;
    if (type !== "update") {
      this.saveDraft(type);
    } else if (type === "update") {
      this.updatePublishedRate(this.containerLoadParam.toLowerCase());
    }
  }

  /**
   * [Udpdate Published Record Button Action]
   * @param  type [string]
   * @return [description]
   */
  updatePublishedRate(type) {
    let rateData = [];


    let JsonSurchargeDet: any

    try {
      let _originCharge = null
      let _destCharge = null
      try {
        _originCharge = (JSON.stringify(this.selectedOrigins) === '[{}]') ? null : this.selectedOrigins
        _destCharge = (JSON.stringify(this.selectedDestinations) === '[{}]') ? null : this.selectedDestinations
      } catch (error) {
        console.log(error);
      }
      if (!_originCharge && !_destCharge) {
        JsonSurchargeDet = null
      } else if (_originCharge && !_destCharge) {
        JsonSurchargeDet = JSON.stringify(_originCharge)
      } else if (!_originCharge && _destCharge) {
        JsonSurchargeDet = JSON.stringify(_destCharge)
      } else if (_originCharge && _destCharge) {
        JsonSurchargeDet = JSON.stringify(_originCharge.concat(_destCharge))
      }
    } catch (error) { }
    try {
      if (!this.fromDate || this.fromDate === null) {
        this._toast.warning("Please enter a valid date range for this rate");
        this.isRateUpdating = false;
        this.loading = false
        return;
      }

      if (!this.toDate || this.toDate === null) {
        this._toast.warning("Please enter a valid date range for this rate");
        this.isRateUpdating = false;
        this.loading = false
        return;
      }
      if (this.selectedData.forType !== "WAREHOUSE") {
        if (!this.validateAdditionalCharges()) {
          this.loading = false
          return;
        }
        if (
          this.selectedData.data &&
          this.selectedData.data &&
          this.selectedData.data.length
        ) {
          this.selectedData.data.forEach(element => {
            let LCLObj = {
              consolidatorPricingID: element.consolidatorPricingID,
              rate: this.selectedPrice,
              effectiveFrom:
                this.fromDate && this.fromDate.month
                  ? this.fromDate.month +
                  "/" +
                  this.fromDate.day +
                  "/" +
                  this.fromDate.year
                  : null,
              effectiveTo:
                this.toDate && this.toDate.month
                  ? this.toDate.month +
                  "/" +
                  this.toDate.day +
                  "/" +
                  this.toDate.year
                  : null,
              modifiedBy: this.userProfile.UserID,
              JsonSurchargeDet: JsonSurchargeDet,
              customerID: element.customerID,
              jsonCustomerDetail:
                JSON.stringify(element.jsonCustomerDetail) === "[{},{}]"
                  ? null
                  : element.jsonCustomerDetail,
              customerType: element.customerType,
              partnerID: (this.selectedPartner && this.selectedPartner.id) ? this.selectedPartner.id : null,
            };
            let FCLObj = {
              carrierPricingID: element.carrierPricingID,
              rate: this.selectedPrice,
              effectiveFrom:
                this.fromDate && this.fromDate.month
                  ? this.fromDate.month +
                  "/" +
                  this.fromDate.day +
                  "/" +
                  this.fromDate.year
                  : null,
              effectiveTo:
                this.toDate && this.toDate.month
                  ? this.toDate.month +
                  "/" +
                  this.toDate.day +
                  "/" +
                  this.toDate.year
                  : null,
              modifiedBy: this.userProfile.UserID,
              JsonSurchargeDet: JsonSurchargeDet,
              customerID: element.customerID,
              jsonCustomerDetail: element.jsonCustomerDetail,
              customerType: element.customerType,
              partnerID: (this.selectedPartner && this.selectedPartner.id) ? this.selectedPartner.id : null,
            };
            let FTLObj = {
              pricingID: element.id,
              couplePrice: parseFloat(this.couplePrice),
              rate: this.selectedPrice,
              effectiveFrom:
                this.fromDate && this.fromDate.month
                  ? this.fromDate.month +
                  "/" +
                  this.fromDate.day +
                  "/" +
                  this.fromDate.year
                  : null,
              effectiveTo:
                this.toDate && this.toDate.month
                  ? this.toDate.month +
                  "/" +
                  this.toDate.day +
                  "/" +
                  this.toDate.year
                  : null,
              modifiedBy: this.userProfile.UserID,
              transportType: "TRUCK",
              JsonSurchargeDet: JsonSurchargeDet,
              customerID: element.customerID,
              jsonCustomerDetail: element.jsonCustomerDetail,
              customerType: element.customerType,
              partnerID: (this.selectedPartner && this.selectedPartner.id) ? this.selectedPartner.id : null,
            };
            if (type === "fcl" && this.transPortMode === "SEA") {
              rateData.push(FCLObj);
            } else if (type === "lcl" && this.transPortMode === "SEA") {
              rateData.push(LCLObj);
            } else if (
              (type === "ftl" && this.transPortMode === "GROUND") ||
              (type === "fcl" && this.transPortMode === "GROUND")
            ) {
              type = "ground";
              rateData.push(FTLObj);
            }
          });
        }
      } else if (this.selectedData.forType === "WAREHOUSE") {
        if (this.selectedData.data.UsageType === "SHARED") {
          if (!this.validateAdditionalCharges()) {
            this.loading = false
            return;
          }
        }
        this.setWarehouseDate()
        //khatam
        let singlePrice: number = 0;
        let doublePRice: number = 0;
        let thirdPrice: number = 0;
        let singlePriceDed: number = 0;
        let doublePRiceDed: number = 0;
        let thirdPriceDed: number = 0;

        try {
          singlePrice =
            this.selectedPrice ||
              parseFloat(this.selectedPrice) === NaN ||
              parseFloat(this.selectedPrice) <= 0
              ? parseFloat(this.selectedPrice)
              : 0;
          doublePRice =
            this.couplePrice ||
              parseFloat(this.couplePrice) === NaN ||
              parseFloat(this.couplePrice) <= 0
              ? parseFloat(this.couplePrice)
              : 0;
          singlePriceDed =
            this.selectedPriceDed ||
              parseFloat(this.selectedPriceDed) === NaN ||
              parseFloat(this.selectedPriceDed) <= 0
              ? parseFloat(this.selectedPriceDed)
              : 0;
          doublePRiceDed =
            this.couplePriceDed ||
              parseFloat(this.couplePriceDed) === NaN ||
              parseFloat(this.couplePriceDed) <= 0
              ? parseFloat(this.couplePriceDed)
              : 0;
          thirdPrice =
            this.thirdPrice ||
              parseFloat(this.couplePriceDed) === NaN ||
              parseFloat(this.couplePriceDed) <= 0
              ? parseFloat(this.couplePriceDed)
              : 0;
          thirdPriceDed =
            this.thirdPriceDed ||
              parseFloat(this.thirdPriceDed) === NaN ||
              parseFloat(this.thirdPriceDed) <= 0
              ? parseFloat(this.thirdPriceDed)
              : 0;
        } catch (error) {
          this.loading = false
        }


        if (this.selectedData.data.UsageType === "FULL" &&
          ((singlePrice === 0 || singlePrice === NaN) &&
            (doublePRice === 0 || doublePRice === NaN))) {
          this._toast.error(
            "Both Price fields cannot be zero or empty",
            "Error"
          );
          this.isRateUpdating = false;
          this.loading = false
          return;
        }

        if (this.selectedData.data.UsageType === "SHARED" &&
          (singlePrice === 0) &&
          (doublePRice === 0) &&
          (thirdPrice === 0) &&
          (singlePriceDed === 0) &&
          (doublePRiceDed === 0) &&
          (thirdPriceDed === 0)
        ) {
          this._toast.error(
            "Price fields cannot be zero or empty",
            "Error"
          );
          this.isRateUpdating = false;
          this.loading = false
          return;
        }



        let addOnPrice = 0
        let addOnPriceDed = 0
        if (this.transPortMode === "WAREHOUSE" && this.selectedData.data.UsageType === "SHARED") {

          if (this.warehouseUnit === 'pallet') {
            addOnPrice = this.addOnPLTPrice
            addOnPriceDed = this.addOnPLTPriceDed
          }

          if (this.warehouseUnit === 'sqft') {
            addOnPrice = this.addOnSQFTPrice
            addOnPriceDed = this.addOnSQFTPriceDed
          }

          if (this.warehouseUnit === 'sqm') {
            addOnPrice = this.addOnSQMPrice
            addOnPriceDed = this.addOnSQMPriceDed
          }

        }

        if (this.transPortMode === "WAREHOUSE" && this.selectedData.data.UsageType === "SHARED" &&
          ((singlePrice || doublePRice || thirdPrice) && (!addOnPrice || addOnPrice === 0))) {
          this._toast.error("Addon price cannot be zero", "Error");
          this.isRateUpdating = false;
          this.loading = false
          return;
        }

        if (this.transPortMode === "WAREHOUSE" && this.selectedData.data.UsageType === "SHARED" &&
          ((singlePriceDed || doublePRiceDed || thirdPriceDed) && (!addOnPriceDed || addOnPriceDed === 0))) {
          this._toast.error("Addon price cannot be zero", "Error");
          this.isRateUpdating = false;
          this.loading = false
          return;
        }

        // if ((singlePrice === 0 || doublePRice === 0)) {
        //   this._toast.error("Price fields cannot be zero", "Error");
        //   this.isRateUpdating = false;
        //   return;
        // }

        if (!this.fromDate || this.fromDate === null) {
          this._toast.warning("Effective from Cannot be empty");
          this.isRateUpdating = false;
          this.loading = false
          return;
        }


        if (!this.toDate || this.toDate === null) {
          this._toast.warning("Effective to Cannot be empty");
          this.isRateUpdating = false;
          this.loading = false
          return;
        }

        this.calculatePricingJSON();
        let WHObj = {
          whPricingID: this.selectedData.data.WhPricingID,
          pricingJson: JSON.stringify(this.pricingJSON),
          effectiveFrom:
            this.fromDate && this.fromDate.month
              ? this.fromDate.month +
              "/" +
              this.fromDate.day +
              "/" +
              this.fromDate.year
              : null,
          effectiveTo:
            this.toDate && this.toDate.month
              ? this.toDate.month +
              "/" +
              this.toDate.day +
              "/" +
              this.toDate.year
              : null,
          modifiedBy: this.userProfile.UserID,
          jsonSurchargeDet: JsonSurchargeDet,
          customerID: this.selectedData.data.CustomerID,
          jsonCustomerDetail: this.selectedData.data.JsonCustomerDetail,
          customerType: this.selectedData.data.CustomerType
        };
        let toSend
        if (this.selectedData.data.UsageType === "SHARED") {
          WHObj.effectiveFrom = this.fromDate && this.fromDate.month
            ? this.fromDate.month +
            "/" +
            this.fromDate.day +
            "/" +
            this.fromDate.year
            : null
          WHObj.effectiveTo = this.toDate && this.toDate.month
            ? this.toDate.month +
            "/" +
            this.toDate.day +
            "/" +
            this.toDate.year
            : null,
            toSend = {
              ...WHObj,
              jsonDedicatedSurchargeDet:
                JSON.stringify(
                  this.selectedOriginsDed
                ) === "[{},{}]"
                  ? null
                  : JSON.stringify(
                    this.selectedOriginsDed
                  ) === "[{},{}]"
                    ? null
                    : JSON.stringify(
                      this.selectedOriginsDed
                    ),
            }
        } else {
          toSend = WHObj
        }
        rateData.push(toSend);
      }
    } catch (error) {
      this.isRateUpdating = false;
      this.loading = false
    }

    this._seaFreightService.rateValidityFCL(type, rateData).subscribe(
      (res: any) => {
        loading(false);
        this.loading = false
        this.isRateUpdating = false;
        this.loading = false
        if (res.returnId > 0) {
          if (res.returnText && typeof res.returnText === 'string') {
            this._toast.success(res.returnText, "Success");
          } else {
            this._toast.success("Rates added successfully", "Success");
          }
          this.closeModal(true);
        } else {
          this._toast.warning(res.returnText);
        }
      },
      error => {
        this.isRateUpdating = false;
        this.loading = false
        loading(false);
        this.loading = false
        this._toast.error("Error while saving rates, please try later");
      }
    );
  }

  addRowLCL() {
    this._seaFreightService
      .addDraftRatesLCL({
        createdBy: this.userProfile.UserID,
        providerID: this.userProfile.ProviderID
      })
      .subscribe((res: any) => {
        if (res.returnStatus == "Success") {
          this._sharedService.draftRowLCLAdd.next(res.returnObject);
          this.newProviderPricingDraftID = undefined;
          this.selectedPrice = undefined;
          this.selectedHandlingUnit = null;
          this.newProviderPricingDraftID =
            res.returnObject.ConsolidatorPricingDraftID;
        }
        this.loading = false
      });
  }

  /**
   * Save Draft Row in Drafts Table
   * [Saving the draft record ]
   * @param {string}  type [description]
   * @return      [description]
   */
  public buttonLoading: boolean = false;
  saveDraft(type) {
    this.buttonLoading = true;
    let objDraft: any;
    try {
      const { filterOrigin, filterDestination, transPortMode } = this;
      if (
        transPortMode === "SEA" &&
        filterOrigin &&
        filterOrigin.CountryCode &&
        filterDestination &&
        filterDestination.CountryCode &&
        filterOrigin.CountryCode.toLowerCase() ===
        filterDestination.CountryCode.toLowerCase()
      ) {
        this._toast.warning(
          "Please select different pickup and drop Country",
          "Warning"
        );
        this.isRateUpdating = false;
        this.loading = false
        return;
      }
      let customers = [];
      if (this.selectedCustomer.length) {
        this.selectedCustomer.forEach(element => {
          let obj = {
            CustomerID: element.CustomerID,
            CustomerType: element.CustomerType,
            CustomerName: element.CustomerName,
            CustomerImage: element.CustomerImage
          };
          customers.push(obj);
        });
      }

      let totalImp = [];
      let totalExp = [];
      // this.selectedOrigins = this.selectedOrigins.filter(e => e.addChrID)
      // this.selectedDestinations = this.selectedDestinations.filter(e => e.addChrID)
      const expCharges = this.selectedOrigins.filter(
        e => e.Imp_Exp === "EXPORT"
      );
      const impCharges = this.selectedDestinations.filter(
        e => e.Imp_Exp === "IMPORT"
      );

      if (impCharges && impCharges.length) {
        impCharges.forEach(element => {
          totalImp.push(parseFloat(element.Price));
        });
        this.TotalImportCharges = totalImp.reduce((all, item) => {
          return all + item;
        });
      }

      if (expCharges && expCharges.length) {
        expCharges.forEach(element => {
          totalExp.push(parseFloat(element.Price));
        });
        this.TotalExportCharges = totalExp.reduce((all, item) => {
          return all + item;
        });
      }
      if (this.selectedData.forType === "WAREHOUSE") {
        this.transPortMode = "WAREHOUSE";
        this.calculatePricingJSON();
      }

      let JsonSurchargeDet: any

      // let JsonSurchargeDet = JSON.stringify(
      //   this.selectedOrigins.concat(this.selectedDestinations)
      // );
      let _originCharge = null
      let _destCharge = null

      try {

        _originCharge = (JSON.stringify(this.selectedOrigins) === '[{}]') ? null : this.selectedOrigins
        _destCharge = (JSON.stringify(this.selectedDestinations) === '[{}]') ? null : this.selectedDestinations

      } catch (error) {
        console.log(error);

      }
      if (!_originCharge && !_destCharge) {
        JsonSurchargeDet = null
      } else if (_originCharge && !_destCharge) {
        JsonSurchargeDet = JSON.stringify(_originCharge)
      } else if (!_originCharge && _destCharge) {
        JsonSurchargeDet = JSON.stringify(_destCharge)
      } else if (_originCharge && _destCharge) {
        JsonSurchargeDet = JSON.stringify(_originCharge.concat(_destCharge))
      }
      let JsonDedicatedSurchargeDet = JSON.stringify(this.selectedOriginsDed);
      let singlePrice: number = 0;
      let singlePriceDed: number = 0;
      let doublePRice: number = 0;
      let doublePRiceDed: number = 0;
      let thirdPrice: number = 0;
      let thirdPriceDed: number = 0;

      try {
        singlePrice =
          this.selectedPrice ||
            parseFloat(this.selectedPrice) === NaN ||
            parseFloat(this.selectedPrice) <= 0
            ? parseFloat(this.selectedPrice)
            : 0;
        doublePRice =
          this.couplePrice ||
            parseFloat(this.couplePrice) === NaN ||
            parseFloat(this.couplePrice) <= 0
            ? parseFloat(this.couplePrice)
            : 0;
        thirdPrice =
          this.thirdPrice ||
            parseFloat(this.thirdPrice) === NaN ||
            parseFloat(this.thirdPrice) <= 0
            ? parseFloat(this.thirdPrice)
            : 0;
        singlePriceDed =
          this.selectedPriceDed ||
            parseFloat(this.selectedPriceDed) === NaN ||
            parseFloat(this.selectedPriceDed) <= 0
            ? parseFloat(this.selectedPriceDed)
            : 0;
        doublePRiceDed =
          this.couplePriceDed ||
            parseFloat(this.couplePriceDed) === NaN ||
            parseFloat(this.couplePriceDed) <= 0
            ? parseFloat(this.couplePriceDed)
            : 0;
        thirdPriceDed =
          this.thirdPriceDed ||
            parseFloat(this.thirdPriceDed) === NaN ||
            parseFloat(this.thirdPriceDed) <= 0
            ? parseFloat(this.thirdPriceDed)
            : 0;
      } catch (error) {
        this.loading = false
      }

      //yolo
      this.setWarehouseDate()

      objDraft = {
        // GROUND ID
        ID: this.selectedData.ID ? this.selectedData.ID : 0,

        // FCL ID
        providerPricingDraftID: this.selectedData.data
          ? this.selectedData.data.ProviderPricingDraftID
          : 0,

        // LCL ID
        consolidatorPricingDraftID: this.selectedData.data
          ? this.selectedData.data.ConsolidatorPricingDraftID
          : 0,

        customerID: this.selectedCustomer.length
          ? this.selectedCustomer[0].CustomerID
          : null,
        customersList: customers.length ? customers : null,
        carrierID: this.selectedShipping
          ? this.selectedShipping.id
          : undefined,
        carrierName: this.selectedShipping
          ? this.selectedShipping.title
          : undefined,
        carrierImage: this.selectedShipping
          ? this.selectedShipping.imageName
          : undefined,
        providerID: this.userProfile.ProviderID,
        containerSpecID:
          this.selectedContSize == null || this.selectedContSize == "null"
            ? null
            : parseInt(this.selectedContSize),
        containerSpecName:
          this.selectedContSize == null || this.selectedContSize == "null"
            ? undefined
            : this.getContSpecName(this.selectedContSize),
        shippingCatID:
          this.selectedCategory == null || this.selectedCategory == "null"
            ? null
            : parseInt(this.selectedCategory),
        shippingCatName:
          this.selectedCategory == null || this.selectedCategory == "null"
            ? undefined
            : this.getShippingName(this.selectedCategory),
        containerLoadType: this.containerLoadParam,
        transportType: this.transPortMode,
        modeOfTrans: this.transPortMode,
        priceBasis: this.priceBasis,
        providerLocationD: "",
        providerLocationL: "",
        polID:
          this.filterOrigin &&
            (this.filterOrigin.PortID || this.filterOrigin.id)
            ? this.filterOrigin.PortID || this.filterOrigin.id
            : null,
        polName:
          this.filterOrigin &&
            (this.filterOrigin.PortID || this.filterOrigin.id)
            ? this.filterOrigin.PortName || this.filterOrigin.title
            : null,
        polCode:
          this.filterOrigin &&
            (this.filterOrigin.PortID || this.filterOrigin.id)
            ? this.filterOrigin.PortCode || this.filterOrigin.code
            : null,
        podID:
          this.filterDestination &&
            (this.filterDestination.PortID || this.filterDestination.id)
            ? this.filterDestination.PortID || this.filterDestination.id
            : null,
        polType:
          this.filterOrigin &&
            (this.filterOrigin.PortID || this.filterOrigin.id)
            ? this.filterOrigin.PortType || this.filterOrigin.type
            : null,
        podName:
          this.filterDestination &&
            (this.filterDestination.PortID || this.filterDestination.id)
            ? this.filterDestination.PortName || this.filterDestination.title
            : null,
        podCode:
          this.filterDestination &&
            (this.filterDestination.PortID || this.filterDestination.id)
            ? this.filterDestination.PortID || this.filterDestination.code
            : null,
        podType:
          this.filterDestination &&
            (this.filterDestination.PortID || this.filterDestination.id)
            ? this.filterDestination.PortType || this.filterDestination.type
            : null,
        price: this.selectedPrice,
        couplePrice: this.couplePrice,
        currencyID:
          this.selectedCurrency && this.selectedCurrency.id
            ? this.selectedCurrency.id
            : 101,
        currencyCode:
          this.selectedCurrency && this.selectedCurrency.shortName
            ? this.selectedCurrency.shortName
            : "USD",
        effectiveFrom:
          this.fromDate && this.fromDate.month
            ? this.fromDate.month +
            "/" +
            this.fromDate.day +
            "/" +
            this.fromDate.year
            : null,
        effectiveTo:
          this.toDate && this.toDate.month
            ? this.toDate.month + "/" + this.toDate.day + "/" + this.toDate.year
            : null,
        JsonSurchargeDet: JsonSurchargeDet === "[{},{}]" ? null : JsonSurchargeDet,
        TotalImportCharges: this.TotalImportCharges,
        TotalExportCharges: this.TotalExportCharges,
        createdBy: this.userProfile.UserID,

        //WAREHOUSE FIELDS
        storageType: this.storageType,
        whPricingID: 0,
        whid:
          this.selectedData.data && this.selectedData.data.WHID
            ? this.selectedData.data.WHID
            : null,
        pricingJson: JSON.stringify(this.pricingJSON),
        parentID: 0,
        dedicatedEffectiveFrom:
          this.fromDateDed && this.fromDateDed.month
            ? this.fromDateDed.month +
            "/" +
            this.fromDateDed.day +
            "/" +
            this.fromDateDed.year
            : null,
        dedicatedEffectiveTo:
          this.toDateDed && this.toDateDed.month
            ? this.toDateDed.month + "/" + this.toDateDed.day + "/" + this.toDateDed.year
            : null,
        whCurrencyID:
          this.selectedCurrencyDed && this.selectedCurrencyDed.id
            ? this.selectedCurrencyDed.id
            : 101,
        whCurrencyCode:
          this.selectedCurrencyDed && this.selectedCurrencyDed.shortName
            ? this.selectedCurrencyDed.shortName
            : "USD",
        DedicatedCurrencyID:
          this.selectedCurrencyDed && this.selectedCurrencyDed.id
            ? this.selectedCurrencyDed.id
            : 101,
        JsonDedicatedSurchargeDet:
          JsonDedicatedSurchargeDet === "[{},{}]" ? null : JsonDedicatedSurchargeDet,
        partnerID: (this.selectedPartner && this.selectedPartner.id) ? this.selectedPartner.id : null,
      };

      // VALIDATIONS STARTS HERE
      // this.selectedData.data.UsageType === "SHARED"
      if (objDraft.transportType === "WAREHOUSE" && this.selectedData.data.UsageType === "SHARED") {
        // objDraft.effectiveFrom = objDraft.dedicatedEffectiveFrom
        // objDraft.effectiveTo = objDraft.dedicatedEffectiveTo
      }
      if (objDraft.transportType !== "WAREHOUSE") {
        if (
          !objDraft.carrierID &&
          !objDraft.containerSpecID &&
          !objDraft.effectiveFrom &&
          !objDraft.effectiveTo &&
          !objDraft.podID &&
          !objDraft.polID &&
          !objDraft.price &&
          !objDraft.shippingCatID
        ) {
          this._toast.info("Please fill atleast one field to save", "Info");
          this.isRateUpdating = false;
          this.loading = false
          return;
        }
      } else if (objDraft.transportType === "WAREHOUSE") {
        if (
          this.transPortMode === "WAREHOUSE" && this.selectedData.data.UsageType === "FULL" &&
          (singlePrice === 0 || singlePrice === NaN) &&
          (doublePRice === 0 || doublePRice === NaN)) {
          this._toast.error("Price fields cannot be zero or empty", "Error");
          this.isRateUpdating = false;
          this.loading = false
          return;
        }
        if (
          (this.transPortMode === "WAREHOUSE" && this.selectedData.data.UsageType === "SHARED") &&
          ((singlePrice === 0) &&
            (doublePRice === 0) &&
            (thirdPrice === 0)) &&
          (singlePriceDed === 0) &&
          (doublePRiceDed === 0) &&
          (thirdPriceDed === 0)
        ) {
          this._toast.error("Price fields cannot be zero or empty", "Error");
          this.isRateUpdating = false;
          this.loading = false
          return;
        }


        if (
          !objDraft.effectiveFrom &&
          !objDraft.effectiveTo &&
          !objDraft.podID &&
          !objDraft.polID &&
          !objDraft.storageType
        ) {
          this._toast.info("Please fill atleast one field to save", "Info");
          this.isRateUpdating = false;
          this.loading = false
          return;
        }
      }
      if ((objDraft.podID && objDraft.polID && objDraft.podID === objDraft.polID) &&
        !(this.selectedData.forType === "FCL-Ground" || this.selectedData.forType === "FTL")
      ) {
        this._toast.error(
          "Source and Destination ports cannot be same",
          "Error"
        );
        this.isRateUpdating = false;
        this.loading = false
        return;
      }

      if (this.transPortMode !== "WAREHOUSE") {
        if (
          objDraft.price === null || objDraft.price === undefined ||
          !(typeof parseFloat(objDraft.price) == "number")
        ) {
          this._toast.error("Price cannot be zero", "Error");
          this.isRateUpdating = false;
          this.loading = false
          return;
        }
      }

      if (this.transPortMode === "WAREHOUSE" && this.selectedData.data.UsageType === "FULL" && (singlePrice === 0 || doublePRice === 0)) {
        this._toast.error("Price fields cannot be zero", "Error");
        this.isRateUpdating = false;
        this.loading = false
        return;
      }


      let addOnPrice = 0
      let addOnPriceDed = 0
      if (this.transPortMode === "WAREHOUSE" && this.selectedData.data.UsageType === "SHARED") {

        if (this.warehouseUnit === 'pallet') {
          addOnPrice = this.addOnPLTPrice
          addOnPriceDed = this.addOnPLTPriceDed
        }

        if (this.warehouseUnit === 'sqft') {
          addOnPrice = this.addOnSQFTPrice
          addOnPriceDed = this.addOnSQFTPriceDed
        }

        if (this.warehouseUnit === 'sqm') {
          addOnPrice = this.addOnSQMPrice
          addOnPriceDed = this.addOnSQMPriceDed
        }

      }


      if (this.transPortMode === "WAREHOUSE" && this.selectedData.data.UsageType === "SHARED" &&
        ((singlePrice === 0 && doublePRice === 0 && thirdPrice === 0) &&
          (singlePriceDed === 0 && doublePRiceDed === 0 && thirdPriceDed === 0))) {
        this._toast.error("Price fields cannot be zero", "Error");
        this.isRateUpdating = false;
        this.loading = false
        return;
      }

      if (this.transPortMode === "WAREHOUSE" && this.selectedData.data.UsageType === "SHARED" &&
        ((singlePrice || doublePRice || thirdPrice) && (!addOnPrice || addOnPrice === 0))) {
        this._toast.error("Addon price cannot be zero", "Error");
        this.isRateUpdating = false;
        this.loading = false
        return;
      }

      if (this.transPortMode === "WAREHOUSE" && this.selectedData.data.UsageType === "SHARED" &&
        ((singlePriceDed || doublePRiceDed || thirdPriceDed) && (!addOnPriceDed || addOnPriceDed === 0))) {
        this._toast.error("Addon price cannot be zero", "Error");
        this.isRateUpdating = false;
        this.loading = false
        return;
      }

      if (
        objDraft.transportType === "SEA" ||
        objDraft.transportType === "GROUND" ||
        (this.transPortMode === "WAREHOUSE" &&
          this.selectedData.data.UsageType === "SHARED")
      ) {
        //Content of validateAdditionalCharges() function was here
        if (!this.validateAdditionalCharges()) {
          this.loading = false
          return;
        }
      }

      let duplicateRecord: boolean = false;
      if (this.selectedData.drafts && this.selectedData.forType === "FCL") {
        this.selectedData.drafts.forEach(element => {
          if (
            element.CarrierID === objDraft.carrierID &&
            element.ContainerSpecID === objDraft.containerSpecID &&
            moment(element.EffectiveFrom).format("D MMM, Y") ===
            moment(objDraft.effectiveFrom).format("D MMM, Y") &&
            moment(element.EffectiveTo).format("D MMM, Y") ===
            moment(objDraft.effectiveTo).format("D MMM, Y") &&
            element.PodID === objDraft.podID &&
            element.PolID === objDraft.polID &&
            element.Price === parseFloat(objDraft.price) &&
            element.ShippingCatID === objDraft.shippingCatID &&
            element.JsonSurchargeDet === objDraft.JsonSurchargeDet
          ) {
            duplicateRecord = true;
          }
        });
      } else if (
        this.selectedData.drafts &&
        this.selectedData.forType === "LCL"
      ) {
        this.selectedData.drafts.forEach(element => {
          if (
            moment(element.EffectiveFrom).format("D MMM, Y") ===
            moment(objDraft.effectiveFrom).format("D MMM, Y") &&
            moment(element.EffectiveTo).format("D MMM, Y") ===
            moment(objDraft.effectiveTo).format("D MMM, Y") &&
            element.PodID === objDraft.podID &&
            element.PolID === objDraft.polID &&
            element.Price === parseFloat(objDraft.price) &&
            element.ShippingCatID === objDraft.shippingCatID &&
            element.JsonSurchargeDet === objDraft.JsonSurchargeDet
          ) {
            duplicateRecord = true;
          }
        });
      } else if (
        this.selectedData.drafts &&
        (this.selectedData.forType === "FTL" ||
          this.selectedData.forType === "FCL-Ground")
      ) {
        this.selectedData.drafts.forEach(element => {

          const elementJsonSDSerialized = (element.JsonSurchargeDet === null || element.JsonSurchargeDet === [{}, {}] || element.JsonSurchargeDet === '[{},{}]') ? true : element.JsonSurchargeDet;
          const currObjJsonSDSerialized = (objDraft.JsonSurchargeDet === null || objDraft.JsonSurchargeDet === [{}, {}] || objDraft.JsonSurchargeDet === '[{},{}]') ? true : objDraft.JsonSurchargeDet;

          if (
            moment(element.EffectiveFrom).format("D MMM, Y") ===
            moment(objDraft.effectiveFrom).format("D MMM, Y") &&
            moment(element.EffectiveTo).format("D MMM, Y") ===
            moment(objDraft.effectiveTo).format("D MMM, Y") &&
            element.PodID === objDraft.podID &&
            element.PolID === objDraft.polID &&
            element.Price === parseFloat(objDraft.price) &&
            elementJsonSDSerialized === currObjJsonSDSerialized
          ) {
            duplicateRecord = true;
          }
        });
      }

      // if (
      //   objDraft.podType &&
      //   objDraft.podType === "Ground" &&
      //   (objDraft.polType && objDraft.polType === "Ground")
      // ) {
      //   this.isRateUpdating = false;
      //   this._toast.info("Please change origin or destination type", "Info");
      //   this.loading = false
      //   return;
      // }

      // if (duplicateRecord) {
      //   this.isRateUpdating = false;
      //   this._toast.warning("This record has already been added", "Warning");
      //   this.loading = false
      //   return;
      // }
    } catch (error) {
      this.isRateUpdating = false;
      this.loading = false
      return;
    }

    try {
      if (this.fromDate && this.fromDate.day && (!this.toDate || !this.toDate.day)) {
        this._toast.warning("Please enter a valid date range for this rate");
        this.isRateUpdating = false;
        this.loading = false
        return;
      }
    } catch (error) {
      console.log(error);


    }

    if (this.selectedData.forType === "FCL") {
      this._seaFreightService
        .saveDraftRate(this.selectedData.forType.toLowerCase(), objDraft)
        .subscribe(
          (res: any) => {
            this.loading = false
            this.buttonLoading = false;
            this.isRateUpdating = false;
            if (res.returnId > 0) {
              if (res.returnText && typeof res.returnText === 'string') {
                this._toast.success(res.returnText, "Success");
              } else {
                this._toast.success("Rates added successfully", "Success");
              }
              if (type === "onlySave") {
                this.closeModal(res.returnObject);
              } else {
                if (this.selectedData.data) {
                  this.selectedData.data.ProviderPricingDraftID = 0;
                }
                this.selectedPrice = undefined;
                this.selectedContSize = null;
                this.savedRow.emit(res.returnObject);
              }
            } else {
              this._toast.warning(res.returnText);
            }
          },
          error => {
            this.isRateUpdating = false;
            this.loading = false
            this._toast.error("Error While saving, please try late");
          }
        );
    } else if (this.selectedData.forType == "LCL") {
      this._seaFreightService.saveDraftRate("lcl", objDraft).subscribe(
        (res: any) => {
          this.buttonLoading = false;
          this.isRateUpdating = false;
          this.loading = false
          if (res.returnId > 0) {
            if (res.returnText && typeof res.returnText === 'string') {
              this._toast.success(res.returnText, "Success");
            } else {
              this._toast.success("Rates added successfully", "Success");
            }
            if (type === "onlySave") {
              this.closeModal(res.returnObject);
            } else {
              this.selectedPrice = undefined;
              if (this.selectedData.data) {
                this.selectedData.data.ConsolidatorPricingDraftID = 0;
              }
              this.selectedContSize = null;
              this.savedRow.emit(res.returnObject);
            }
          } else {
            this._toast.warning(res.returnText);
          }
        },
        error => {
          this.isRateUpdating = false;
          this.loading = false
          this._toast.error("Error While saving, please try late");
        }
      );
    } else if (this.selectedData.forType == "WAREHOUSE") {
      if (!objDraft.price && !objDraft.couplePrice || (!objDraft.thirdPrice && !objDraft.price && !objDraft.couplePrice)) {
        // this.isRateUpdating = false;
        // this._toast.error("Please provide atleast one price", "Error");
        // return;
      } else if (!objDraft.effectiveFrom && !objDraft.effectiveTo) {
        this.isRateUpdating = false;
        this.loading = false
        this._toast.error("Please provide date range", "Error");
        this.loading = false
        return;
      } else if (!objDraft.storageType) {
        this.isRateUpdating = false;
        this.loading = false
        this._toast.error("Please provide warehouse storage type", "Error");
        return;
      }
      // return true
      this._seaFreightService.saveWarehouseRate(objDraft).subscribe(
        (res: any) => {
          this.buttonLoading = false;
          this.isRateUpdating = false;
          this.loading = false
          if (res.returnId > 0) {
            this._toast.success(res.returnText, "Success");
            if (type === "onlySave") {
              this.closeModal(res.returnObject);
            } else {
              this.pricingJSON = [];
              this.savedRow.emit(res.returnObject);
            }
          } else {
            this.pricingJSON = [];
            this._toast.warning(res.returnText);
          }
        },
        error => {
          this.isRateUpdating = false;
          this.loading = false
          this._toast.error("Error While saving, please try late");
        }
      );
    } else if (
      this.selectedData.forType == "FCL-Ground" ||
      this.selectedData.forType == "FTL"
    ) {
      this.buttonLoading = false;
      this.loading = false
      this._seaFreightService.saveDraftRate("ground", objDraft).subscribe(
        (res: any) => {
          this.isRateUpdating = false;
          this.loading = false
          if (res.returnId > 0) {
            if (res.returnText && typeof res.returnText === 'string') {
              this._toast.success(res.returnText, "Success");
            } else {
              this._toast.success("Rates added successfully", "Success");
            }
            if (type === "onlySave") {
              this.closeModal(res.returnObject);
            } else {
              this.selectedPrice = undefined;
              this.couplePrice = null;
              this.savedRow.emit(res.returnObject);
            }
          } else {
            this._toast.warning(res.returnText);
          }
        },
        error => {
          this.isRateUpdating = false;
          this.loading = false
          this._toast.error("Error While saving, please try late");
        }
      );
    }
  }

  setWarehouseDate() {
    if (this.selectedData.forType === 'WAREHOUSE') {
      if (this.fromDateDed && this.fromDateDed.day) {
        this.fromDate = this.fromDateDed
        this.toDate = this.toDateDed
      }
    }
  }

  validateAdditionalCharges(): boolean {
    let ADCHValidated: boolean = true;
    // let exportCharges
    // let importCharges
    // if (objDraft.JsonSurchargeDet) {
    //   const parsedJsonSurchargeDet = JSON.parse(obj.JsonSurchargeDet)
    //   exportCharges = parsedJsonSurchargeDet.filter(e => e.Imp_Exp === 'EXPORT')
    //   importCharges = parsedJsonSurchargeDet.filter(e => e.Imp_Exp === 'IMPORT')
    // }
    if (this.selectedOrigins && this.selectedOrigins.length > 0) {
      this.selectedOrigins.forEach(element => {
        if (
          Object.keys(element).length &&
          (!element.Price ||
            !(typeof parseFloat(element.Price) == "number") ||
            parseFloat(element.Price) <= 0)
        ) {
          this._toast.error("Price is missing for Additional Charge", "Error");
          this.isRateUpdating = false;
          ADCHValidated = false;
          return ADCHValidated;
        }
        if (Object.keys(element).length && !element.CurrId) {
          this._toast.error(
            "Currency is missing for Additional Charge",
            "Error"
          );
          this.isRateUpdating = false;
          ADCHValidated = false;
          return ADCHValidated;
        }
        if (Object.keys(element).length && (!element.currency || typeof (element.currency) === 'string')) {
          this._toast.error(
            "Currency is missing for Additional Charge",
            "Error"
          );
          this.isRateUpdating = false;
          ADCHValidated = false;
          return ADCHValidated;
        }
        if (Object.keys(element).length && !element.addChrID) {
          this._toast.error("Additional Charge is missing", "Error");
          this.isRateUpdating = false;
          ADCHValidated = false;
          return ADCHValidated;
        }
      });
    }
    if (this.selectedDestinations && this.selectedDestinations.length > 0) {
      this.selectedDestinations.forEach(element => {
        if (
          Object.keys(element).length &&
          (!element.Price ||
            !(typeof parseFloat(element.Price) == "number") ||
            parseFloat(element.Price) <= 0)
        ) {
          this._toast.error("Price is missing for Additional Charge", "Error");
          this.isRateUpdating = false;
          ADCHValidated = false;
          return ADCHValidated;
        }
        if (Object.keys(element).length && !element.CurrId) {
          this._toast.error(
            "Currency is missing for Additional Charge",
            "Error"
          );
          this.isRateUpdating = false;
          ADCHValidated = false;
          return ADCHValidated;
        }

        if (Object.keys(element).length && (!element.currency || typeof (element.currency) === 'string')) {
          this._toast.error(
            "Currency is missing for Additional Charge",
            "Error"
          );
          this.isRateUpdating = false;
          ADCHValidated = false;
          return ADCHValidated;
        }

        if (Object.keys(element).length && !element.addChrID) {
          this._toast.error("Additional Charge is missing", "Error");
          this.isRateUpdating = false;
          ADCHValidated = false;
          return ADCHValidated;
        }
      });
    }
    if (this.selectedOriginsDed && this.selectedOriginsDed.length > 0) {
      this.selectedOriginsDed.forEach(element => {
        if (
          Object.keys(element).length &&
          (!element.Price ||
            !(typeof parseFloat(element.Price) == "number") ||
            parseFloat(element.Price) <= 0)
        ) {
          this._toast.error("Price is missing for Additional Charge", "Error");
          this.isRateUpdating = false;
          ADCHValidated = false;
          return ADCHValidated;
        }
        if (Object.keys(element).length && !element.CurrId) {
          this._toast.error(
            "Currency is missing for Additional Charge",
            "Error"
          );
          this.isRateUpdating = false;
          ADCHValidated = false;
          return ADCHValidated;
        }
        if (Object.keys(element).length && (!element.currency || typeof (element.currency) === 'string')) {
          this._toast.error(
            "Currency is missing for Additional Charge",
            "Error"
          );
          this.isRateUpdating = false;
          ADCHValidated = false;
          return ADCHValidated;
        }
        if (Object.keys(element).length && !element.addChrID) {
          this._toast.error("Additional Charge is missing", "Error");
          this.isRateUpdating = false;
          ADCHValidated = false;
          return ADCHValidated;
        }
      });
    }
    if (this.selectedDestinationsDed && this.selectedDestinationsDed.length > 0) {
      this.selectedDestinationsDed.forEach(element => {
        if (
          Object.keys(element).length &&
          (!element.Price ||
            !(typeof parseFloat(element.Price) == "number") ||
            parseFloat(element.Price) <= 0)
        ) {
          this._toast.error("Price is missing for Additional Charge", "Error");
          this.isRateUpdating = false;
          ADCHValidated = false;
          return ADCHValidated;
        }
        if (Object.keys(element).length && !element.CurrId) {
          this._toast.error(
            "Currency is missing for Additional Charge",
            "Error"
          );
          this.isRateUpdating = false;
          ADCHValidated = false;
          return ADCHValidated;
        }

        if (Object.keys(element).length && (!element.currency || typeof (element.currency) === 'string')) {
          this._toast.error(
            "Currency is missing for Additional Charge",
            "Error"
          );
          this.isRateUpdating = false;
          ADCHValidated = false;
          return ADCHValidated;
        }

        if (Object.keys(element).length && !element.addChrID) {
          this._toast.error("Additional Charge is missing", "Error");
          this.isRateUpdating = false;
          ADCHValidated = false;
          return ADCHValidated;
        }
      });
    }

    return ADCHValidated;
  }

  addRow() {
    this._seaFreightService
      .addDraftRates({
        createdBy: this.userProfile.UserID,
        providerID: this.userProfile.ProviderID
      })
      .subscribe((res: any) => {
        if (res.returnStatus == "Success") {
          this._sharedService.draftRowFCLAdd.next(res.returnObject);
          this.newProviderPricingDraftID = undefined;
          this.selectedPrice = undefined;
          this.selectedContSize = null;
          this.newProviderPricingDraftID =
            res.returnObject.ProviderPricingDraftID;
        }
      });
  }

  numberValidwithDecimal(evt) {
    let charCode = (evt.which) ? evt.which : evt.keyCode;
    if (evt.target.value && evt.target.value[evt.target.value.length - 1] == '.') {
      if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

      return true;
    }
    if (charCode != 46 && charCode > 31
      && (charCode < 48 || charCode > 57))
      return false;

    return true;
  }

  getContSpecName(id) {
    return this.allContainers.find(obj => obj.ContainerSpecID == id)
      .ContainerSpecShortName;
  }
  getShippingName(id) {
    return this.allCargoType.find(obj => obj.ShippingCatID == id)
      .ShippingCatName;
  }

  getHandlingSpecName(id) {
    return this.allHandlingType.find(obj => obj.ContainerSpecID == id)
      .ContainerSpecShortName;
  }
  onDateSelection(date: NgbDateStruct) {
    if (this.selectedData.mode === "publish" && this.selectedData.data) {
      this.onDateSelectionEdit(date)
    } else {
      this.onDateSelectionNew(date)
    }
  }

  onDateSelectionEdit(date: NgbDateStruct) {
    let parsed = '';
    const _minDate = new Date(this.minDate.year, this.minDate.month - 1, this.minDate.day);
    const selectedDate = new Date(date.year, date.month - 1, date.day);
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && moment(selectedDate).isSameOrAfter(_minDate)) {
      const _fromDate = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
      if (moment(selectedDate).isAfter(_fromDate)) {
        this.toDate = date;
        this.input.close();
      }
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
    if (this.fromDate) {
      parsed += this._parserFormatter.format(this.fromDate);
    }
    if (this.toDate) {
      parsed += ' - ' + this._parserFormatter.format(this.toDate);
    }
    this.renderer.setProperty(this.rangeDp.nativeElement, 'value', parsed);
  }

  onDateSelectionNew(date: NgbDateStruct) {
    let parsed = "";
    const _minDate = new Date(this.minDate.year, this.minDate.month - 1, this.minDate.day);
    const selectedDate = new Date(date.year, date.month - 1, date.day);
    if (!this.fromDate && !this.toDate) {
      if (moment(selectedDate).isSameOrAfter(_minDate)) {
        this.fromDate = date;
      } else {
        this.fromDate = null
        return
      }
    } else if (this.fromDate && !this.toDate && moment(selectedDate).isAfter(_minDate)) {
      const _fromDate = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
      if (moment(selectedDate).isAfter(_fromDate)) {
        this.toDate = date;
        this.input.close();
      }
    } else {
      const selectedDate = new Date(date.year, date.month - 1, date.day);
      if (moment(selectedDate).isSameOrAfter(_minDate)) {
        this.toDate = null;
        this.fromDate = date;
      } else {
        this.fromDate = null
        return
      }
    }
    if (this.fromDate) { parsed += this._parserFormatter.format(this.fromDate); }
    if (this.toDate) { parsed += " - " + this._parserFormatter.format(this.toDate); }
    this.renderer.setProperty(this.rangeDp.nativeElement, 'value', parsed);
  }


  onDateSelectionDed(date: NgbDateStruct) {
    if (this.selectedData.mode === "publish" && this.selectedData.data) {
      this.onDateSelectionEditDed(date)
    } else {
      this.onDateSelectionNewDed(date)
    }
  }

  onDateSelectionEditDed(date: NgbDateStruct) {
    let parsed = '';
    const _minDate = new Date(this.minDate.year, this.minDate.month - 1, this.minDate.day);

    const selectedDate = new Date(date.year, date.month - 1, date.day);
    if (!this.fromDateDed && !this.toDateDed) {
      this.fromDateDed = date;
    } else if (this.fromDateDed && !this.toDateDed && moment(selectedDate).isSameOrAfter(_minDate)) {
      const _fromDateDed = new Date(this.fromDateDed.year, this.fromDateDed.month - 1, this.fromDateDed.day);
      if (moment(selectedDate).isAfter(_fromDateDed)) {
        this.toDateDed = date;
        this.inputDed.close();
      }
    } else {
      this.toDateDed = null;
      this.fromDateDed = date;
    }
    if (this.fromDateDed) {
      parsed += this._parserFormatter.format(this.fromDateDed);
    }
    if (this.toDateDed) {
      parsed += ' - ' + this._parserFormatter.format(this.toDateDed);
    }
    this.renderer.setProperty(this.rangeDpDed.nativeElement, 'value', parsed);
  }

  onDateSelectionNewDed(date: NgbDateStruct) {
    let parsed = "";
    const _minDate = new Date(this.minDate.year, this.minDate.month - 1, this.minDate.day);
    const selectedDate = new Date(date.year, date.month - 1, date.day);
    if (!this.fromDateDed && !this.toDateDed) {
      if (moment(selectedDate).isSameOrAfter(_minDate)) {
        this.fromDateDed = date;
      } else {
        this.fromDateDed = null
        return
      }
    } else if (this.fromDateDed && !this.toDateDed && moment(selectedDate).isAfter(_minDate)) {
      const _fromDateDed = new Date(this.fromDateDed.year, this.fromDateDed.month - 1, this.fromDateDed.day);
      if (moment(selectedDate).isAfter(_fromDateDed)) {
        this.toDateDed = date;
        this.inputDed.close();
      }
    } else {
      const selectedDate = new Date(date.year, date.month - 1, date.day);
      if (moment(selectedDate).isSameOrAfter(_minDate)) {
        this.toDateDed = null;
        this.fromDateDed = date;
      } else {
        this.fromDateDed = null
        return
      }
    }
    if (this.fromDateDed) { parsed += this._parserFormatter.format(this.fromDateDed); }
    if (this.toDateDed) { parsed += " - " + this._parserFormatter.format(this.toDateDed); }
    this.renderer.setProperty(this.rangeDpDed.nativeElement, 'value', parsed);
  }

  closeModal(status) {
    this._activeModal.close(status);
    if (this.containerLoadParam == "FCL") {
      this._sharedService.draftRowFCLAdd.next(null);
    } else if (this.containerLoadParam == "LCL") {
      this._sharedService.draftRowLCLAdd.next(null);
    }
    document.getElementsByTagName("html")[0].style.overflowY = "auto";
  }
  closePopup() {
    // let object = {
    //   data: this.allRatesFilledData
    // };
    this.closeModal(false);
  }
  shippings = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term =>
        !term || term.length < 3
          ? []
          : this.allShippingLines.filter(
            v =>
              v.title &&
              v.title.toLowerCase().indexOf(term.toLowerCase()) > -1
          )
      )
    );
  formatter = (x: { code: string; title: string; imageName: string }) =>
    x.code + ', ' + x.title;

  ports = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term =>
        !term || term.length < 3
          ? []
          : this.seaPorts.filter(
            v =>
              v.PortName &&
              v.PortName.toLowerCase().indexOf(term.toLowerCase()) > -1
          )
      )
    );
  portsFormatter = (x: { PortName: string }) => x.PortName;

  currencies = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term =>
        !term || term.length < 3
          ? []
          : this.allCurrencies.filter(
            v =>
              v.shortName &&
              v.shortName.toLowerCase().indexOf(term.toLowerCase()) > -1
          )
      )
    );
  currencyFormatter = x => x.shortName;

  selectCharges(type, model, index) {
    model.Imp_Exp = type;
    if (type === "EXPORT") {
      if (
        (Object.keys(this.selectedOrigins[index]).length === 0 &&
          this.selectedOrigins[index].constructor === Object) ||
        !this.selectedOrigins[index].hasOwnProperty("currency")
      ) {
        model.CurrId = this.selectedCurrency.id;
        model.currency = this.selectedCurrency;
      } else {
        model.CurrId = this.selectedOrigins[index].currency.id;
        model.currency = this.selectedOrigins[index].currency;
      }
      const { selectedOrigins } = this;
      selectedOrigins.forEach(element => {
        if (
          Object.keys(element).length === 0 &&
          element.constructor === Object
        ) {
          let idx = selectedOrigins.indexOf(element);
          selectedOrigins.splice(idx, 1);
        }
      });
      if (selectedOrigins[index]) {
        this.originsList.push(selectedOrigins[index]);
        selectedOrigins[index] = model;
      } else {
        selectedOrigins.push(model);
      }
      this.selectedOrigins = cloneObject(selectedOrigins);
      this.originsList = this.originsList.filter(
        e => e.addChrID && e.addChrID !== model.addChrID
      );
    } else if (type === "IMPORT") {
      if (
        (Object.keys(this.selectedDestinations[index]).length === 0 &&
          this.selectedDestinations[index].constructor === Object) ||
        !this.selectedDestinations[index].hasOwnProperty("currency")
      ) {
        model.CurrId = this.selectedCurrency.id;
        model.currency = this.selectedCurrency;
      } else {
        model.CurrId = this.selectedDestinations[index].currency.id;
        model.currency = this.selectedDestinations[index].currency;
      }
      const { selectedDestinations } = this;
      selectedDestinations.forEach(element => {
        if (
          Object.keys(element).length === 0 &&
          element.constructor === Object
        ) {
          let idx = selectedDestinations.indexOf(element);
          selectedDestinations.splice(idx, 1);
        }
      });
      if (selectedDestinations[index]) {
        this.destinationsList.push(selectedDestinations[index]);
        selectedDestinations[index] = model;
      } else {
        selectedDestinations.push(model);
      }
      this.selectedDestinations = cloneObject(selectedDestinations);
      this.destinationsList = this.destinationsList.filter(
        e => e.addChrID && e.addChrID !== model.addChrID
      );
    }
  }


  /**
   *
   * Dedicated Warehouse Additional Charges Calculation
   * @param {string} type
   * @param {object} model
   * @param {number} index
   * @memberof SeaRateDialogComponent
   */
  selectChargesDed(type, model, index) {
    model.Imp_Exp = type;
    if (type === "EXPORT") {
      if (
        (Object.keys(this.selectedOriginsDed[index]).length === 0 &&
          this.selectedOriginsDed[index].constructor === Object) ||
        !this.selectedOriginsDed[index].hasOwnProperty("currency")
      ) {
        model.CurrId = this.selectedCurrencyDed.id;
        model.currency = this.selectedCurrencyDed;
      } else {
        model.CurrId = this.selectedOriginsDed[index].currency.id;
        model.currency = this.selectedOriginsDed[index].currency;
      }
      const { selectedOriginsDed } = this;
      selectedOriginsDed.forEach(element => {
        if (
          Object.keys(element).length === 0 &&
          element.constructor === Object
        ) {
          let idx = selectedOriginsDed.indexOf(element);
          selectedOriginsDed.splice(idx, 1);
        }
      });
      if (selectedOriginsDed[index]) {
        this.originsListDed.push(selectedOriginsDed[index]);
        selectedOriginsDed[index] = model;
      } else {
        selectedOriginsDed.push(model);
      }
      this.selectedOriginsDed = cloneObject(selectedOriginsDed);
      this.originsListDed = this.originsListDed.filter(
        e => e.addChrID && e.addChrID !== model.addChrID
      );
    }
  }

  getVal(idx, event, type) {
    if (typeof event === "object") {
      if (type === "origin") {
        this.selectedOrigins[idx].CurrId = event.id;
      } else if (type === "destination") {
        this.selectedDestinations[idx].CurrId = event.id;
      } else if (type === "originDed") {
        this.selectedOriginsDed[idx].CurrId = event.id;
      }
    }
  }

  addMoreCharges(type) {
    if (type === "origin") {
      if (!this.selectedOrigins[this.selectedOrigins.length - 1].CurrId) {
        this._toast.info("Please select currency", "Info");
        return;
      }
      if (!this.selectedOrigins[this.selectedOrigins.length - 1].Price) {
        this._toast.info("Please add price", "Info");
        return;
      }
      if (!this.selectedOrigins[this.selectedOrigins.length - 1].addChrCode) {
        this._toast.info("Please select any additional charge", "Info");
        return;
      }
      if (
        !(
          Object.keys(this.selectedOrigins[this.selectedOrigins.length - 1])
            .length === 0 &&
          this.selectedOrigins[this.selectedOrigins.length - 1].constructor ===
          Object
        ) &&
        parseFloat(this.selectedOrigins[this.selectedOrigins.length - 1].Price) &&
        this.selectedOrigins[this.selectedOrigins.length - 1].CurrId
      ) {
        this.selectedOrigins.push({
          CurrId: this.selectedOrigins[this.selectedOrigins.length - 1].currency
            .id,
          currency: this.selectedOrigins[this.selectedOrigins.length - 1]
            .currency
        });
      }
    } else if (type === "originDed") {
      if (!this.selectedOriginsDed[this.selectedOriginsDed.length - 1].CurrId) {
        this._toast.info("Please select currency", "Info");
        return;
      }
      if (!this.selectedOriginsDed[this.selectedOriginsDed.length - 1].Price) {
        this._toast.info("Please add price", "Info");
        return;
      }
      if (!this.selectedOriginsDed[this.selectedOriginsDed.length - 1].addChrCode) {
        this._toast.info("Please select any additional charge", "Info");
        return;
      }
      if (
        !(
          Object.keys(this.selectedOriginsDed[this.selectedOriginsDed.length - 1])
            .length === 0 &&
          this.selectedOriginsDed[this.selectedOriginsDed.length - 1].constructor ===
          Object
        ) &&
        parseFloat(this.selectedOriginsDed[this.selectedOriginsDed.length - 1].Price) &&
        this.selectedOriginsDed[this.selectedOriginsDed.length - 1].CurrId
      ) {
        this.selectedOriginsDed.push({
          CurrId: this.selectedOriginsDed[this.selectedOriginsDed.length - 1].currency
            .id,
          currency: this.selectedOriginsDed[this.selectedOriginsDed.length - 1]
            .currency
        });
      }
    } else if (type === "destination") {
      if (
        !this.selectedDestinations[this.selectedDestinations.length - 1].CurrId
      ) {
        this._toast.info("Please select currency", "Info");
        return;
      }
      if (
        !this.selectedDestinations[this.selectedDestinations.length - 1].Price
      ) {
        this._toast.info("Please add price", "Info");
        return;
      }
      if (
        !this.selectedDestinations[this.selectedDestinations.length - 1]
          .addChrCode
      ) {
        this._toast.info("Please select any additional charge", "Info");
        return;
      }
      if (
        !(
          Object.keys(
            this.selectedDestinations[this.selectedDestinations.length - 1]
          ).length === 0 &&
          this.selectedDestinations[this.selectedDestinations.length - 1]
            .constructor === Object
        ) &&
        parseFloat(
          this.selectedDestinations[this.selectedDestinations.length - 1].Price
        ) &&
        this.selectedDestinations[this.selectedDestinations.length - 1].CurrId
      ) {
        this.selectedDestinations.push({
          CurrId: this.selectedDestinations[
            this.selectedDestinations.length - 1
          ].currency.id,
          currency: this.selectedDestinations[
            this.selectedDestinations.length - 1
          ].currency
        });
      }
    }
  }

  setEditData(type) {

    let parsedJsonSurchargeDet;
    let parsedJsonDedicatedSurchargeDet;
    if (type === "publish") {
      parsedJsonSurchargeDet =
        this.selectedData.forType === "WAREHOUSE"
          ? JSON.parse(this.selectedData.data.JsonSurchargeDet)
          : JSON.parse(this.selectedData.data[0].jsonSurchargeDet);
      parsedJsonDedicatedSurchargeDet =
        (this.selectedData.forType === "WAREHOUSE" && this.selectedData.data.UsageType === 'SHARED')
          ? JSON.parse(this.selectedData.data.JsonDedicatedSurchargeDet)
          : null;
    } else if (type === "draft") {
      parsedJsonSurchargeDet = JSON.parse(
        this.selectedData.data.JsonSurchargeDet
      );
    }
    this.destinationsList = cloneObject(this.selectedData.addList);
    this.originsList = cloneObject(this.selectedData.addList);
    this.originsListDed = cloneObject(this.selectedData.addList);
    if (parsedJsonSurchargeDet) {
      this.selectedOrigins = parsedJsonSurchargeDet.filter(
        e => e.Imp_Exp === "EXPORT"
      );
      this.selectedDestinations = parsedJsonSurchargeDet.filter(
        e => e.Imp_Exp === "IMPORT"
      );
    }

    if (parsedJsonDedicatedSurchargeDet) {
      this.selectedOriginsDed = parsedJsonDedicatedSurchargeDet.filter(
        e => e.Imp_Exp === "EXPORT"
      );
    }

    if (!this.selectedOrigins.length) {
      this.selectedOrigins = [{}];
    }
    if (!this.selectedOriginsDed.length) {
      this.selectedOriginsDed = [{}];
    }

    if (!this.selectedDestinations.length) {
      this.selectedDestinations = [{}];
    }
    if (this.selectedDestinations.length) {
      this.selectedDestinations.forEach(element => {
        this.destinationsList.forEach(e => {
          if (e.addChrID === element.addChrID) {
            let idx = this.destinationsList.indexOf(e);
            this.destinationsList.splice(idx, 1);
          }
        });
      });
    }

    if (this.selectedOrigins.length) {
      this.selectedOrigins.forEach(element => {
        this.originsList.forEach(e => {
          if (e.addChrID === element.addChrID) {
            let idx = this.originsList.indexOf(e);
            this.originsList.splice(idx, 1);
          }
        });
      });
    }

    if (this.selectedOriginsDed.length) {
      this.selectedOriginsDed.forEach(element => {
        this.originsListDed.forEach(e => {
          if (e.addChrID === element.addChrID) {
            let idx = this.originsListDed.indexOf(e);
            this.originsListDed.splice(idx, 1);
          }
        });
      });
    }
  }

  closeDropdown(event) {
    let x: any = document.getElementsByClassName("dropdown-menu");
    if (!event.target.className.includes("has-open")) {
      this.originDropdown.close();
      // this.originDropdownDed.close();
      this.destinationDropdown.close();
    }
    // if (!this._eref.nativeElement.contains(event.target)) // or some similar check
  }

  getSurchargeBasis(containerLoad) {
    this._seaFreightService.getSurchargeBasis(containerLoad).subscribe((res: any) => {
      try {
        const _surchargesList: CodeValMst[] = res;
        if (this.selectedData.forType === 'WAREHOUSE') {
          const { warehouseUnit } = this
          this.surchargesList = _surchargesList
            .filter(_charge => _charge.codeVal.includes(warehouseUnit.toUpperCase()) || _charge.codeVal === 'PER_BOOKNG')
        } else {
          this.surchargesList = _surchargesList
        }
        // let filteredData = []
        // this.selectedData.addList.forEach(e => {
        //   this.surchargesList.forEach(element => {
        //     if (e.addChrBasis === element.codeVal) {
        //       filteredData.push(e)
        //     }
        //   });
        // });
      } catch { }
    },
      err => { }
    );
  }
  public isOriginChargesForm = false;
  public isDestinationChargesForm = false;
  public lablelName: string = "";
  public lablelNameDed: string = "";
  public surchargeType = "";
  public surchargeTypeDed = "";
  public labelValidate: boolean = true;
  public surchargeBasisValidate: boolean = true;
  public surchargeBasisValidateDed: boolean = true;
  showCustomChargesForm(type) {
    if (type === "origin") {
      this.isOriginChargesForm = !this.isOriginChargesForm;
    } else if (type === "destination") {
      this.isDestinationChargesForm = !this.isDestinationChargesForm;
    }
  }

  onKeyDown(idx, event, type) {
    if (!event.target.value) {
      if (type === "origin") {
        this.selectedOrigins[idx].currency = {};
        this.selectedOrigins[idx].CurrId = null;
      } else if (type === "destination") {
        this.selectedDestinations[idx].currency = {};
        this.selectedDestinations[idx].CurrId = null;
      }
    }

  }

  public canAddLabel: boolean = true;
  addCustomLabel(type) {
    this.canAddLabel = true;
    if (!this.lablelName) {
      this.labelValidate = false;
      return;
    }
    if (!this.surchargeType) {
      this.surchargeBasisValidate = false;
      return;
    }
    const selectedSurcharge = this.surchargesList.find(
      obj => obj.codeValID === parseInt(this.surchargeType)
    );
    let obj = {
      addChrID: -1,
      addChrCode: "OTHR",
      addChrName: this.lablelName,
      addChrDesc: this.lablelName,
      modeOfTrans: (this.selectedData.forType === 'FCL' || this.selectedData.forType === 'LCL') ? 'SEA' : ((this.selectedData.forType === 'WAREHOUSE') ? 'WAREHOUSE' : 'TRUCK'),
      addChrBasis: selectedSurcharge.codeVal,
      createdBy: this.userProfile.UserID,
      addChrType: "ADCH",
      providerID: this.userProfile.ProviderID,
      ContainerLoadType: this.selectedData.forType,
    };
    this.selectedData.addList.forEach(element => {
      if (element.addChrName === obj.addChrName) {
        this.canAddLabel = false;
      }
    });

    if (!this.canAddLabel) {
      this._toast.info("Already Added, Please try another name", "Info");
      return false;
    }

    this._seaFreightService.addCustomCharge(obj).subscribe(
      (res: any) => {
        this.isOriginChargesForm = false;
        this.isDestinationChargesForm = false;
        if (res.returnId !== -1) {
          let obj = {
            addChrID: res.returnId,
            addChrCode: "OTHR",
            addChrName: this.lablelName,
            addChrDesc: this.lablelName,
            modeOfTrans: (this.selectedData.forType === 'FCL' || this.selectedData.forType === 'LCL') ? 'SEA' : ((this.selectedData.forType === 'WAREHOUSE') ? 'WAREHOUSE' : 'TRUCK'),
            ContainerLoadType: this.selectedData.forType === 'FCL',
            addChrBasis: selectedSurcharge.codeVal,
            createdBy: this.userProfile.UserID,
            addChrType: "ADCH",
            providerID: this.userProfile.ProviderID
          };
          this.getAllAdditionalCharges(this.userProfile.ProviderID)
          if (type === "origin") {
            this.originsList.push(obj);
          } else if (type === "destination") {
            this.destinationsList.push(obj);
          }
          this.lablelName = "";
          this.surchargeType = "";
        }
      },
      err => {
      }
    );
  }


  getAllAdditionalCharges(providerID) {
    this._seaFreightService.getAllAdditionalCharges(providerID).subscribe((res: any) => {
      localStorage.setItem('additionalCharges', JSON.stringify(res.filter(e => e.addChrType === 'ADCH')))
      loading(false)
    }, (err) => {
      loading(false)
    })
  }

  public addDestinationActive: boolean = false;
  public addOriginActive: boolean = false;
  public addDestinationDedActive: boolean = false;
  public addOriginDedActive: boolean = false;
  dropdownToggle(event, type) {
    if (event) {
      this.isDestinationChargesForm = false;
      this.isOriginChargesForm = false;
      this.surchargeBasisValidate = true;
      this.labelValidate = true;
      if (type === "destination") {
        this.addDestinationActive = true;
      } else if (type === "origin") {
        this.addOriginActive = true;
      } else if (type === "originDed") {
        this.addOriginDedActive = true;
      }
    } else {
      this.addOriginActive = false;
      this.addDestinationActive = false;
    }
  }

  public combinedContainers = [];
  public fclContainers = [];
  public selectedFCLContainers = [];
  public shippingCategories = [];
  public cities: any[] = [];
  /**
   * Getting all dropdown values to fill
   *
   * @memberof SeaFreightComponent
   */
  public lclContainers = []
  getDropdownsList() {
    this.allShippingLines = JSON.parse(localStorage.getItem('carriersList')).filter(e => e.type === 'SEA');
    this.transPortMode = "SEA";
    this.allPorts = JSON.parse(localStorage.getItem("PortDetails"));
    this.seaPorts = this.allPorts.filter(e => e.PortType === "SEA");
    this.combinedContainers = JSON.parse(localStorage.getItem("containers"));
    if (this.selectedData.forType === 'LCL') {
      const lclContiners = this.combinedContainers.filter(e => e.ContainerFor === 'LCL')
      let uniq = {};
      this.allCargoType = lclContiners.filter(
        obj => !uniq[obj.ShippingCatID] && (uniq[obj.ShippingCatID] = true)
      );
      this.fclContainers = this.allCargoType.filter(
        e => e.ContainerFor === "LCL"
      );
    } else if (
      this.selectedData.forType === "FCL-Ground" ||
      this.selectedData.forType === "FTL"
    ) {
      this.transPortMode = "GROUND";
      const groundConts = this.combinedContainers.filter(e => e.ContainerFor === 'FTL')
      let uniq = {};
      this.allCargoType = groundConts.filter(
        obj => !uniq[obj.ShippingCatID] && (uniq[obj.ShippingCatID] = true)
      );
      let selectedCategory = this.allCargoType.find(
        obj => obj.ShippingCatName.toLowerCase() == "goods"
      );
      this.selectedCategory = selectedCategory.ShippingCatID;
      const groundContainers = groundConts.filter(
        e =>
          e.ShippingCatID === this.selectedCategory
      );
      const containers = groundContainers.filter(
        e => e.ContainerSpecGroupName === "Container"
      );
      const trucks = groundContainers.filter(
        e => e.ContainerSpecGroupName != "Container"
      );
      this.allContainers = containers.concat(trucks);
      this.fclContainers = this.allContainers
      this.groundPorts = this.allPorts.filter(e => e.PortType === "Ground");
    } else {
      const fclContainers = this.combinedContainers.filter(e => e.ContainerFor === 'FCL' && e.ShippingModeCode === 'SEA')
      let uniq = {};
      this.allCargoType = fclContainers.filter(
        obj => !uniq[obj.ShippingCatID] && (uniq[obj.ShippingCatID] = true)
      );
      this.fclContainers = fclContainers.filter(
        e => e.ContainerFor === "FCL" && e.ShippingModeCode === 'SEA'
      );
    }
  }

  // GROUND WORKING
  public showPickupDropdown: boolean = false;
  public showDestinationDropdown: boolean = false;

  public showPickupPorts: boolean = false;
  public showPickupDoors: boolean = false;
  public showPickupCity: boolean = false;

  public showDestPorts: boolean = false;
  public showDestDoors: boolean = false;
  public showDestCity: boolean = false;

  toggleDropdown(type) {
    if (this.selectedData.mode === "publish") {
      return;
    }
    if (!this.selectedContSize) return;
    if (type === "pickup") {
      this.showPickupDropdown = !this.showPickupDropdown;
      this.closeDropDown("delivery");
      this.showPickupPorts = true;
      this.showPickupDoors = true;
      this.showPickupCity = true;
    }
    if (type === "delivery") {
      this.showDestinationDropdown = !this.showDestinationDropdown;
      this.closeDropDown("pickup");
      this.showDestPorts = true;
      this.showDestDoors = true;
      this.showDestCity = true;
    }
  }

  togglePorts(type) {
    if (type === "pickup-sea") {
      this.originSelectedMode = 'SEA'
      this.showPickupPorts = true;
      this.showPickupDoors = false;
      this.showPickupCity = false;
      setTimeout(() => {
        this.originPickupBox.nativeElement.focus();
        this.originPickupBox.nativeElement.select();
      }, 10);
    } else if (type === "pickup-door") {
      this.originSelectedMode = 'DOOR'
      this.showPickupPorts = false;
      this.showPickupDoors = true;
      this.showPickupCity = false;
      setTimeout(() => {
        this.originPickupBox.nativeElement.focus();
        this.originPickupBox.nativeElement.select();
      }, 10);
    } else if (type === "pickup-city") {
      this.originSelectedMode = 'CITY'
      this.showPickupPorts = false;
      this.showPickupDoors = false;
      this.showPickupCity = true;
      setTimeout(() => {
        this.originPickupBox.nativeElement.focus();
        this.originPickupBox.nativeElement.select();
      }, 10);
    }
    else if (type === "delivery-sea") {
      this.destinSelectedMode = 'SEA'
      this.showDestPorts = true;
      this.showDestDoors = false;
      this.showDestCity = false;
      setTimeout(() => {
        this.destinationPickupBox.nativeElement.focus();
        this.destinationPickupBox.nativeElement.select();
      }, 10);
    } else if (type === "delivery-door") {
      this.destinSelectedMode = 'DOOR'
      this.showDestPorts = false;
      this.showDestDoors = true;
      this.showDestCity = false;
      setTimeout(() => {
        this.destinationPickupBox.nativeElement.focus();
        this.destinationPickupBox.nativeElement.select();
      }, 10);
    } else if (type === "delivery-city") {
      this.destinSelectedMode = 'CITY'
      this.showDestPorts = false;
      this.showDestDoors = false;
      this.showDestCity = true;
      setTimeout(() => {
        this.destinationPickupBox.nativeElement.focus();
        this.destinationPickupBox.nativeElement.select();
      }, 10);
    }
  }

  closeDropDown($action: string) {
    switch ($action) {
      case "pickup":
        if (this.showPickupDropdown) {
          this.showPickupDropdown = false;
        }
        break;
      case "delivery":
        if (this.showDestinationDropdown) {
          this.showDestinationDropdown = false;
        }
        break;
      default:
        if (this.showPickupDropdown || this.showDestinationDropdown) {
          this.showPickupDropdown = false;
          this.showDestinationDropdown = false;
        }
        break;
    }
  }

  public groundAddresses: any = [];
  public groundsPorts: any = [];
  public transPortMode = "SEA";
  portsFilterartion(obj) {
    if (typeof obj === "object") {
      this.showPickupDropdown = false;
      this.showDestinationDropdown = false;
    }
  }

  //Ground areas formatter and observer
  public groundPorts = [];
  addresses = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term =>
        !term || term.length < 3
          ? []
          : this.groundPorts.filter(
            v =>
              v.PortName &&
              v.PortName.toLowerCase().indexOf(term.toLowerCase()) > -1
          )
      )
    );
  addressFormatter = (x: { PortName: string }) => {
    return x.PortName;
  };

  //Ground areas formatter and observer
  // citiesList = (text$: Observable<string>) =>
  //   text$.pipe(
  //     debounceTime(500),
  //     map(term => (!term || term.length < 3) ? [] : this.cities.filter(
  //       v => (v.title.toLowerCase().indexOf(term.toLowerCase()) > -1 || v.shortName.toLowerCase().indexOf(term.toLowerCase()) > -1 || v.code.toLowerCase().indexOf(term.toLowerCase()) > -1))))

  citiesFormatter = (x: { title: string; imageName: string }) => {
    return x.title;
  };

  citiesList = (text$: Observable<string>) =>
    text$
      .debounceTime(300) //debounce time
      .distinctUntilChanged()
      .mergeMap(term => {
        let some: any = []; //  Initialize the object to return
        if (term && term.length >= 3) {
          //search only if item are more than three
          some = this._manageRateService
            .getAllCities(term)
            .do(res => res)
            .catch(() => []);
        } else {
          some = [];
        }
        return some;
      })
      .do(res => res); // final server list

  public showDoubleRates: boolean = false;
  public priceBasis: string = "";
  containerChange(containerID) {
    if (this.transPortMode === "GROUND") {
      let fclContainers = this.allContainers.filter(
        e =>
          e.ContainerSpecID === parseInt(containerID) &&
          e.ContainerSpecGroupName === "Container"
      );
      let ftlContainers = this.allContainers.filter(
        e => e.ContainerSpecID === parseInt(containerID)
      );
      if (fclContainers.length) {
        this.priceBasis = fclContainers[0].PriceBasis;
        this.showDoubleRates = true;
        this.containerLoadParam = "FCL";
      } else {
        this.couplePrice = null;
        this.showDoubleRates = false;
        this.containerLoadParam = "FTL";
      }
      if (ftlContainers.length) {
        this.priceBasis = ftlContainers[0].PriceBasis;
      }
    } else if (this.transPortMode === "SEA") {
      const fclContainers = this.fclContainers.filter(
        e => e.ContainerSpecID === containerID
      );
      if (fclContainers.length) {
        this.priceBasis = fclContainers[0].PriceBasis;
      }
    }
  }

  /**
   * GET BASE URL FOR UI IMAGES
   *
   * @param {string} $image
   * @returns
   * @memberof SeaRateDialogComponent
   */
  getShippingLineImage($image: string) {
    return getImagePath(
      ImageSource.FROM_SERVER,
      "/" + $image,
      ImageRequiredSize.original
    );
  }

  setCurrency() {
    this.selectedCurrency = JSON.parse(localStorage.getItem("userCurrency"));
    this.selectedCurrencyDed = JSON.parse(localStorage.getItem("userCurrency"));
  }

  /**
   *
   * Removed added additional charges
   * @param {string} type origin/destination
   * @param {object} obj
   * @memberof SeaRateDialogComponent
   */
  removeAdditionalCharge(type, obj) {
    if (type === "origin") {
      if (this.selectedOrigins.length > 0) {
        this.selectedOrigins.forEach(element => {
          if (element.addChrID === obj.addChrID) {
            if (this.selectedOrigins.length > 1) {
              let idx = this.selectedOrigins.indexOf(element);
              this.selectedOrigins.splice(idx, 1);
            } else if (this.selectedOrigins.length === 1) {
              this.selectedOrigins = [{}]
            }
            try {
              if (element.addChrID) {
                this.originsList.push(element);
                const { originsList } = this
                const _originsList = originsList.sort(firstBy(function (v1, v2) { return v1.sortingorder - v2.sortingorder; }));
                this.originsList = _originsList
              }
            } catch (error) { }
          }
        });
      }
    } else if (type === "originDed") {
      if (this.selectedOriginsDed.length > 0) {
        this.selectedOriginsDed.forEach(element => {
          if (element.addChrID === obj.addChrID) {
            if (this.selectedOriginsDed.length > 1) {
              let idx = this.selectedOriginsDed.indexOf(element);
              this.selectedOriginsDed.splice(idx, 1);
            } else if (this.selectedOriginsDed.length === 1) {
              this.selectedOriginsDed = [{}]
            }
            try {
              if (element.addChrID) {
                this.originsListDed.push(element);
                const { originsListDed } = this
                const _originsListDed = originsListDed.sort(firstBy(function (v1, v2) { return v1.sortingorder - v2.sortingorder; }));
                this.originsListDed = _originsListDed
              }
            } catch (error) { }
          }
        });
      }
    } else if (type === "destination") {
      if (this.selectedDestinations.length > 0) {
        this.selectedDestinations.forEach(element => {
          if (element.addChrID === obj.addChrID) {
            if (this.selectedDestinations.length > 1) {
              let idx = this.selectedDestinations.indexOf(element);
              this.selectedDestinations.splice(idx, 1);
            } else if (this.selectedDestinations.length === 1) {
              this.selectedDestinations = [{}]
            }
            try {
              if (element.addChrID) {
                this.originsList.push(element);
                const { destinationsList } = this
                const _destinationsList = destinationsList.sort(firstBy(function (v1, v2) { return v1.sortingorder - v2.sortingorder; }));
                this.destinationsList = _destinationsList
              }
            } catch (error) { }
          }
        });
      }
    }
  }

  chargePLT: Array<WarehousePricing> = []
  selectedchargePLT: WarehousePricing = null
  addOnPLT: CodeValMst = null
  addOnPLTPrice: any = null

  chargeSQM: Array<WarehousePricing> = []
  selectedchargeSQM: WarehousePricing = null
  addOnSQM: CodeValMst = null
  addOnSQMPrice: any = null

  chargeSQFT: Array<WarehousePricing> = []
  selectedchargeSQFT: WarehousePricing = null
  addOnSQFT: CodeValMst = null
  addOnSQFTPrice: any = null

  chargePLTDed: Array<WarehousePricing> = []
  selectedchargePLTDed: WarehousePricing = null
  addOnPLTDed: CodeValMst = null
  addOnPLTPriceDed: any = null

  chargeSQMDed: Array<WarehousePricing> = []
  selectedchargeSQMDed: WarehousePricing = null
  addOnSQMDed: CodeValMst = null
  addOnSQMPriceDed: any = null

  chargeSQFTDed: Array<WarehousePricing> = []
  selectedchargeSQFTDed: WarehousePricing = null
  addOnSQFTDed: CodeValMst = null
  addOnSQFTPriceDed: any = null

  whAddsOn: CodeValMst[] = []
  masterWhAddsOn: CodeValMst[] = []

  onWHPriceTypeSelect($charge: WarehousePricing, $from: string) {
    let addOn = null
    const { masterWhAddsOn } = this
    if ($charge.addChrBasis.includes('DAY')) {
      this.whAddsOn = masterWhAddsOn.filter(_charge => _charge.codeVal.includes('DAY'))
      addOn = this.whAddsOn[0]
    } else if ($charge.addChrBasis.includes('MONTH')) {
      this.whAddsOn = masterWhAddsOn.filter(_charge => !_charge.codeVal.includes('YEAR'))
      addOn = this.whAddsOn[0]
    } else if ($charge.addChrBasis.includes('YEAR')) {
      this.whAddsOn = masterWhAddsOn
      addOn = this.whAddsOn[0]
    }
    if ($from === 'PLT') {
      this.selectedchargePLT = $charge
      this.elChargePLT.close();
      this.addOnPLT = addOn
    }
    if ($from === 'SQM') {
      this.selectedchargeSQM = $charge
      this.elChargeSQM.close();
      this.addOnSQM = addOn
    }
    if ($from === 'SQFT') {
      this.selectedchargeSQFT = $charge
      this.elChargeSQFT.close();
      this.addOnSQFT = addOn
    }
    if ($from === 'SQM_DED') {
      this.selectedchargeSQMDed = $charge
      this.elChargeSQMDed.close();
      this.addOnSQMDed = addOn
    }
    if ($from === 'PLT_DED') {
      this.selectedchargePLTDed = $charge
      this.elChargePLTDed.close();
      this.addOnPLTDed = addOn
    }
    if ($from === 'SQFT_DED') {
      this.selectedchargeSQFTDed = $charge
      this.elChargeSQFTDed.close();
      this.addOnSQFTDed = addOn
    }
    this.addOnValidation()
  }

  getaddOnPeriods() {
    this._manageRateService.getaddOnPeriods().subscribe((res: any) => {
      this.whAddsOn = res
      this.masterWhAddsOn = res
      const { masterWhAddsOn } = this

      if (this.warehouseUnit === 'pallet' && this.tempAddOnBasis) {
        this.addOnPLT = masterWhAddsOn.filter(_unit => _unit.codeVal === this.tempAddOnBasis)[0]
      } else {
        this.whAddsOn = masterWhAddsOn.filter(_charge => _charge.codeVal.includes('DAY'))
        this.addOnPLT = this.whAddsOn[0]
      }
      if (this.warehouseUnit === 'pallet' && this.tempAddOnBasisDed) {
        this.addOnPLTDed = masterWhAddsOn.filter(_unit => _unit.codeVal === this.tempAddOnBasisDed)[0]
      } else {
        this.whAddsOn = masterWhAddsOn.filter(_charge => _charge.codeVal.includes('DAY'))
        this.addOnPLTDed = this.whAddsOn[0]
      }

      if (this.warehouseUnit === 'sqm' && this.tempAddOnBasis) {
        this.addOnSQM = masterWhAddsOn.filter(_unit => _unit.codeVal === this.tempAddOnBasis)[0]
      } else {
        this.whAddsOn = masterWhAddsOn.filter(_charge => _charge.codeVal.includes('DAY'))
        this.addOnSQM = this.whAddsOn[0]
      }
      if (this.warehouseUnit === 'sqm' && this.tempAddOnBasisDed) {
        this.addOnSQMDed = masterWhAddsOn.filter(_unit => _unit.codeVal === this.tempAddOnBasisDed)[0]
      } else {
        this.whAddsOn = masterWhAddsOn.filter(_charge => _charge.codeVal.includes('DAY'))
        this.addOnSQMDed = this.whAddsOn[0]
      }


      if (this.warehouseUnit === 'sqft' && this.tempAddOnBasis) {
        this.addOnSQFT = masterWhAddsOn.filter(_unit => _unit.codeVal === this.tempAddOnBasis)[0]
      } else {
        this.whAddsOn = masterWhAddsOn.filter(_charge => _charge.codeVal.includes('DAY'))
        this.addOnSQFT = this.whAddsOn[0]
      }
      if (this.warehouseUnit === 'sqft' && this.tempAddOnBasisDed) {
        this.addOnSQFTDed = masterWhAddsOn.filter(_unit => _unit.codeVal === this.tempAddOnBasisDed)[0]
      } else {
        this.whAddsOn = masterWhAddsOn.filter(_charge => _charge.codeVal.includes('DAY'))
        this.addOnSQFTDed = this.whAddsOn[0]
      }

      this.addOnValidation()
    })
  }

  onAddsOnSelect($addOn: any, $from) {
    if ($from === 'PLT') {
      this.addOnPLT = $addOn
      this.elAddOnPLT.close();
      if (this.addOnPLT.codeVal === 'DAYS' && this.selectedchargePLT.addChrBasis.includes('DAY')) {
        this.addOnPLTPrice = this.thirdPrice
        this.disableElAddOnPLT = true
      } else if (this.addOnPLT.codeVal === 'MONTHS' && this.selectedchargePLT.addChrBasis.includes('MONTH')) {
        this.addOnPLTPrice = this.thirdPrice
        this.disableElAddOnPLT = true
      } else if (this.addOnPLT.codeVal === 'YEARS' && this.selectedchargePLT.addChrBasis.includes('YEAR')) {
        this.addOnPLTPrice = this.thirdPrice
        this.disableElAddOnPLT = true
      } else {
        this.disableElAddOnPLT = false
      }
    }
    if ($from === 'SQM') {
      this.addOnSQM = $addOn
      this.elAddOnSQM.close();
      if (this.addOnSQM.codeVal === 'DAYS' && this.selectedchargeSQM.addChrBasis.includes('DAY')) {
        this.addOnSQMPrice = this.selectedPrice
        this.disableElAddOnSQM = true
      } else if (this.addOnSQM.codeVal === 'MONTHS' && this.selectedchargeSQM.addChrBasis.includes('MONTH')) {
        this.addOnSQMPrice = this.selectedPrice
        this.disableElAddOnSQM = true
      } else if (this.addOnSQM.codeVal === 'YEARS' && this.selectedchargeSQM.addChrBasis.includes('YEAR')) {
        this.addOnSQMPrice = this.selectedPrice
        this.disableElAddOnSQM = true
      } else {
        this.disableElAddOnSQM = false
      }
    }
    if ($from === 'SQFT') {
      this.addOnSQFT = $addOn
      this.elAddOnSQFT.close();
      if (this.addOnSQFT.codeVal === 'DAYS' && this.selectedchargeSQFT.addChrBasis.includes('DAY')) {
        this.addOnSQFTPrice = this.couplePrice
        this.disableElAddOnSQFT = true
      } else if (this.addOnSQFT.codeVal === 'MONTHS' && this.selectedchargeSQFT.addChrBasis.includes('MONTH')) {
        this.addOnSQFTPrice = this.couplePrice
        this.disableElAddOnSQFT = true
      } else if (this.addOnSQFT.codeVal === 'YEARS' && this.selectedchargeSQFT.addChrBasis.includes('YEAR')) {
        this.addOnSQFTPrice = this.couplePrice
        this.disableElAddOnSQFT = true
      } else {
        this.disableElAddOnSQFT = false
      }
    }
    if ($from === 'SQM_DED') {
      this.addOnSQMDed = $addOn
      this.elAddOnSQMDed.close();
      if (this.addOnSQMDed.codeVal === 'DAYS' && this.selectedchargeSQMDed.addChrBasis.includes('DAY')) {
        this.addOnSQMPriceDed = this.thirdPriceDed
        this.disableElAddOnSQMDed = true
      } else if (this.addOnSQMDed.codeVal === 'MONTHS' && this.selectedchargeSQMDed.addChrBasis.includes('MONTH')) {
        this.addOnSQMPriceDed = this.thirdPriceDed
        this.disableElAddOnSQMDed = true
      } else if (this.addOnSQMDed.codeVal === 'YEARS' && this.selectedchargeSQMDed.addChrBasis.includes('YEAR')) {
        this.addOnSQMPriceDed = this.thirdPriceDed
        this.disableElAddOnSQMDed = true
      } else {
        this.disableElAddOnSQMDed = false
      }
    }
    if ($from === 'PLT_DED') {
      this.addOnPLTDed = $addOn
      this.elAddOnPLTDed.close();
      if (this.addOnPLTDed.codeVal === 'DAYS' && this.selectedchargePLTDed.addChrBasis.includes('DAY')) {
        this.addOnPLTPriceDed = this.selectedPriceDed
        this.disableElAddOnPLTDed = true
      } else if (this.addOnPLTDed.codeVal === 'MONTHS' && this.selectedchargePLTDed.addChrBasis.includes('MONTH')) {
        this.addOnPLTPriceDed = this.selectedPriceDed
        this.disableElAddOnPLTDed = true
      } else if (this.addOnPLTDed.codeVal === 'YEARS' && this.selectedchargePLTDed.addChrBasis.includes('YEAR')) {
        this.addOnPLTPriceDed = this.selectedPriceDed
        this.disableElAddOnPLTDed = true
      } else {
        this.disableElAddOnPLTDed = false
      }
    }
    if ($from === 'SQFT_DED') {
      this.addOnSQFTDed = $addOn
      this.elAddOnSQFTDed.close();
      if (this.addOnSQFTDed.codeVal === 'DAYS' && this.selectedchargeSQFTDed.addChrBasis.includes('DAY')) {
        this.addOnSQFTPriceDed = this.couplePriceDed
        this.disableElAddOnSQFTDed = true
      } else if (this.addOnSQFTDed.codeVal === 'MONTHS' && this.selectedchargeSQFTDed.addChrBasis.includes('MONTH')) {
        this.addOnSQFTPriceDed = this.couplePriceDed
        this.disableElAddOnSQFTDed = true
      } else if (this.addOnSQFTDed.codeVal === 'YEARS' && this.selectedchargeSQFTDed.addChrBasis.includes('YEAR')) {
        this.addOnSQFTPriceDed = this.couplePriceDed
        this.disableElAddOnSQFTDed = true
      } else {
        this.disableElAddOnSQFTDed = false
      }
    }
  }

  addOnValidation() {
    if (this.warehouseUnit === 'pallet') {
      if (this.addOnPLT.codeVal === 'DAYS' && this.selectedchargePLT.addChrBasis.includes('DAY')) {
        this.addOnPLTPrice = this.thirdPrice
        this.disableElAddOnPLT = true
      } else if (this.addOnPLT.codeVal === 'MONTHS' && this.selectedchargePLT.addChrBasis.includes('MONTH')) {
        this.addOnPLTPrice = this.thirdPrice
        this.disableElAddOnPLT = false
      } else if (this.addOnPLT.codeVal === 'YEARS' && this.selectedchargePLT.addChrBasis.includes('YEAR')) {
        this.addOnPLTPrice = this.thirdPrice
        this.disableElAddOnPLT = false
      } else {
        this.disableElAddOnPLT = false
      }
      if (this.addOnPLTDed.codeVal === 'DAYS' && this.selectedchargePLTDed.addChrBasis.includes('DAY')) {
        this.addOnPLTPriceDed = this.selectedPriceDed
        this.disableElAddOnPLTDed = true
      } else if (this.addOnPLTDed.codeVal === 'MONTHS' && this.selectedchargePLTDed.addChrBasis.includes('MONTH')) {
        this.addOnPLTPriceDed = this.selectedPriceDed
        this.disableElAddOnPLTDed = false
      } else if (this.addOnPLTDed.codeVal === 'YEARS' && this.selectedchargePLTDed.addChrBasis.includes('YEAR')) {
        this.addOnPLTPriceDed = this.selectedPriceDed
        this.disableElAddOnPLTDed = false
      } else {
        this.disableElAddOnPLTDed = false
      }
    }

    if (this.warehouseUnit === 'sqm') {
      if (this.addOnSQM.codeVal === 'DAYS' && this.selectedchargeSQM.addChrBasis.includes('DAY')) {
        this.addOnSQMPrice = this.selectedPrice
        this.disableElAddOnSQM = true
      } else if (this.addOnSQM.codeVal === 'MONTHS' && this.selectedchargeSQM.addChrBasis.includes('MONTH')) {
        this.addOnSQMPrice = this.selectedPrice
        this.disableElAddOnSQM = false
      } else if (this.addOnSQM.codeVal === 'YEARS' && this.selectedchargeSQM.addChrBasis.includes('YEAR')) {
        this.addOnSQMPrice = this.selectedPrice
        this.disableElAddOnSQM = false
      } else {
        this.disableElAddOnSQM = false
      }

      if (this.addOnSQMDed.codeVal === 'DAYS' && this.selectedchargeSQMDed.addChrBasis.includes('DAY')) {
        this.addOnSQMPriceDed = this.selectedPriceDed
        this.disableElAddOnSQMDed = true
      } else if (this.addOnSQMDed.codeVal === 'MONTHS' && this.selectedchargeSQMDed.addChrBasis.includes('MONTH')) {
        this.addOnSQMPriceDed = this.selectedPriceDed
        this.disableElAddOnSQMDed = false
      } else if (this.addOnSQMDed.codeVal === 'YEARS' && this.selectedchargeSQMDed.addChrBasis.includes('YEAR')) {
        this.addOnSQMPriceDed = this.selectedPriceDed
        this.disableElAddOnSQMDed = false
      } else {
        this.disableElAddOnSQMDed = false
      }
    }

    if (this.warehouseUnit === 'sqft') {
      if (this.addOnSQFT.codeVal === 'DAYS' && this.selectedchargeSQFT.addChrBasis.includes('DAY')) {
        this.addOnSQFTPrice = this.couplePrice
        this.disableElAddOnSQFT = true
      } else if (this.addOnSQFT.codeVal === 'MONTHS' && this.selectedchargeSQFT.addChrBasis.includes('MONTH')) {
        this.addOnSQFTPrice = this.couplePrice
        this.disableElAddOnSQFT = false
      } else if (this.addOnSQFT.codeVal === 'YEARS' && this.selectedchargeSQFT.addChrBasis.includes('YEAR')) {
        this.addOnSQFTPrice = this.couplePrice
        this.disableElAddOnSQFT = false
      } else {
        this.disableElAddOnSQFT = false
      }

      if (this.addOnSQFTDed.codeVal === 'DAYS' && this.selectedchargeSQFTDed.addChrBasis.includes('DAY')) {
        this.addOnSQFTPriceDed = this.couplePriceDed
        this.disableElAddOnSQFTDed = true
      } else if (this.addOnSQFTDed.codeVal === 'MONTHS' && this.selectedchargeSQFTDed.addChrBasis.includes('MONTH')) {
        this.addOnSQFTPriceDed = this.couplePriceDed
        this.disableElAddOnSQFTDed = false
      } else if (this.addOnSQFTDed.codeVal === 'YEARS' && this.selectedchargeSQFTDed.addChrBasis.includes('YEAR')) {
        this.addOnSQFTPriceDed = this.couplePriceDed
        this.disableElAddOnSQFTDed = false
      } else {
        this.disableElAddOnSQFTDed = false
      }
    }
  }

  onPriceChange($from: string) {

    if (this.warehouseUnit === 'pallet') {
      if ($from === 'ded' && this.disableElAddOnPLTDed) {
        this.addOnPLTPriceDed = this.thirdPriceDed
      } else if (this.disableElAddOnPLT) {
        this.addOnPLTPrice = this.thirdPrice
      }
    }
    if (this.warehouseUnit === 'sqm') {
      if ($from === 'ded' && this.disableElAddOnSQMDed) {
        this.addOnSQMPriceDed = this.selectedPriceDed
      } else if (this.disableElAddOnSQM) {
        this.addOnSQMPrice = this.selectedPrice

      }
    }
    if (this.warehouseUnit === 'sqft') {
      if ($from === 'ded' && this.disableElAddOnSQFTDed) {
        this.addOnSQFTPriceDed = this.couplePriceDed
      } else if (this.disableElAddOnSQFT) {
        this.addOnSQFTPrice = this.couplePrice
      }
    }
  }

  // WAREHOUSE WORKING
  /**
   * [GET WAREHOUSE PRICING]
   * @return [description]
   */
  public warehousePricing: any[] = [];
  public sharedWarehousePricing: any[] = [];
  public fullWarehousePricing: any = [];
  getWarehousePricing() {
    loading(true);
    this._manageRateService.getWarehousePricing("WAREHOUSE").subscribe(
      (res: any) => {
        loading(false);
        this.warehousePricing = res;
        const pricingCpy: Array<WarehousePricing> = res
        this.getaddOnPeriods()
        // tempBasis
        // tempBasisDed

        this.chargePLT = pricingCpy.filter(_unit => _unit.addChrBasis.includes('PLT') && !_unit.addChrBasis.includes('DED'))
        if (this.warehouseUnit === 'pallet' && this.tempBasis) {
          this.selectedchargePLT = pricingCpy.filter(_unit => _unit.addChrBasis === this.tempBasis)[0]
        } else {
          this.selectedchargePLT = this.chargePLT.filter(_charge => _charge.addChrBasis.includes('DAY'))[0]
        }

        this.chargePLTDed = pricingCpy.filter(_unit => _unit.addChrBasis.includes('PLT') && _unit.addChrBasis.includes('DED'))
        if (this.warehouseUnit === 'pallet' && this.tempBasisDed) {
          this.selectedchargePLTDed = pricingCpy.filter(_unit => _unit.addChrBasis === this.tempBasisDed)[0]
        } else {
          this.selectedchargePLTDed = this.chargePLTDed.filter(_charge => _charge.addChrBasis.includes('DAY'))[0]
        }

        this.chargeSQM = pricingCpy.filter(_unit => _unit.addChrBasis.includes('SQM') && !_unit.addChrBasis.includes('DED'))
        if (this.warehouseUnit === 'sqm' && this.tempBasis) {
          this.selectedchargeSQM = pricingCpy.filter(_unit => _unit.addChrBasis === this.tempBasis)[0]
        } else {
          this.selectedchargeSQM = this.chargeSQM.filter(_charge => _charge.addChrBasis.includes('DAY'))[0]
        }

        this.chargeSQMDed = pricingCpy.filter(_unit => _unit.addChrBasis.includes('SQM') && _unit.addChrBasis.includes('DED'))
        if (this.warehouseUnit === 'sqm' && this.tempBasisDed) {
          this.selectedchargeSQMDed = pricingCpy.filter(_unit => _unit.addChrBasis === this.tempBasisDed)[0]
        } else {
          this.selectedchargeSQMDed = this.chargeSQMDed.filter(_charge => _charge.addChrBasis.includes('DAY'))[0]
        }

        this.chargeSQFT = pricingCpy.filter(_unit => _unit.addChrBasis.includes('SQFT') && !_unit.addChrBasis.includes('DED'))
        if (this.warehouseUnit === 'sqft' && this.tempBasis) {
          this.selectedchargeSQFT = pricingCpy.filter(_unit => _unit.addChrBasis === this.tempBasis)[0]
        } else {
          this.selectedchargeSQFT = this.chargeSQFT.filter(_charge => _charge.addChrBasis.includes('DAY'))[0]
        }

        this.chargeSQFTDed = pricingCpy.filter(_unit => _unit.addChrBasis.includes('SQFT') && _unit.addChrBasis.includes('DED'))
        if (this.warehouseUnit === 'sqft' && this.tempBasisDed) {
          this.selectedchargeSQFTDed = pricingCpy.filter(_unit => _unit.addChrBasis === this.tempBasisDed)[0]
        } else {
          this.selectedchargeSQFTDed = this.chargeSQFTDed.filter(_charge => _charge.addChrBasis.includes('DAY'))[0]
        }

        pricingCpy.forEach(wPrice => {
          const { addChrBasis, addChrName } = wPrice;
          if (this.selectedData.data.UsageType === 'SHARED') {
            if (addChrBasis === "PER_SQM_PER_DAY") {
              this.singlePriceTag = addChrName;
            } else if (addChrBasis === "PER_SQFT_PER_DAY") {
              this.doublePriceTag = addChrName;
            } else if (addChrBasis === "PER_PLT_PER_DAY") {
              this.thirdPriceTag = addChrName;
            } else if (addChrBasis === "PER_PLT_PER_DAY_DED") {
              this.thirdPriceTagDed = addChrName;
            } else if (addChrBasis === "PER_SQFT_PER_DAY_DED") {
              this.doublePriceTagDed = addChrName;
            } else if (addChrBasis === "PER_SQM_PER_DAY_DED") {
              this.singlePriceTagDed = addChrName;
            }
          } else if (this.selectedData.data.UsageType === 'FULL') {
            if (addChrBasis === "PER_MONTH") {
              this.singlePriceTag = addChrName;
            } else if (addChrBasis === "PER_YEAR") {
              this.doublePriceTag = addChrName;
            }
          }
          // if (addChrBasis === "PER_SQM_PER_DAY" && this.selectedData.data.UsageType === 'SHARED') {
          //   this.singlePriceTag = addChrName;
          // } else if (addChrBasis === "PER_MONTH" && this.selectedData.data.UsageType === 'FULL') {
          //   this.singlePriceTag = addChrName;
          // } else if (addChrBasis === "PER_SQFT_PER_DAY" && this.selectedData.data.UsageType === 'SHARED') {
          //   this.doublePriceTag = addChrName;
          // } else if (addChrBasis === "PER_YEAR" && this.selectedData.data.UsageType === 'FULL') {
          //   this.doublePriceTag = addChrName;
          // }
        })
        this.sharedWarehousePricing = this.warehousePricing.filter(
          e =>
            e.addChrBasis === "PER_SQM_PER_DAY" ||
            e.addChrBasis === "PER_SQFT_PER_DAY" ||
            e.addChrBasis === "PER_PLT_PER_DAY" ||
            e.addChrBasis === "PER_PLT_PER_DAY_DED" ||
            e.addChrBasis === "PER_SQFT_PER_DAY_DED" ||
            e.addChrBasis === "PER_SQM_PER_DAY_DED"
        );
        this.fullWarehousePricing = this.warehousePricing.filter(
          e => e.addChrBasis === "PER_MONTH" || e.addChrBasis === "PER_YEAR"
        );
      },
      err => {
        loading(false);
      }
    );
  }


  /**
   *
   *  CALCULATE THE PRICING JSON
   * @memberof SeaRateDialogComponent
   */
  calculatePricingJSON() {
    this.pricingJSON = []
    if (this.selectedPrice) {
      if (this.selectedData.data.UsageType === "SHARED") {
        // const cbmArr = this.sharedWarehousePricing.filter(e => e.addChrBasis === 'PER_SQM_PER_DAY')
        let json = {
          addChrID: this.selectedchargeSQM.addChrID,
          addChrCode: this.selectedchargeSQM.addChrCode,
          addChrName: this.selectedchargeSQM.addChrName,
          addChrType: this.selectedchargeSQM.addChrType,
          priceBasis: this.selectedchargeSQM.addChrBasis,
          price: parseFloat(this.selectedPrice),
          currencyID: this.selectedCurrency.id,
          sWHType: "SHARED",
          addOnBasis: this.addOnSQM.codeVal,
          addOnCount: 1,
          addOnPrice: parseFloat(this.addOnSQMPrice)
        };
        this.pricingJSON.push(json);
      } else if (this.selectedData.data.UsageType === "FULL") {
        let json = {
          addChrID: this.fullWarehousePricing[0].addChrID,
          addChrCode: this.fullWarehousePricing[0].addChrCode,
          addChrName: this.fullWarehousePricing[0].addChrName,
          addChrType: this.fullWarehousePricing[0].addChrType,
          priceBasis: this.fullWarehousePricing[0].addChrBasis,
          price: parseFloat(this.selectedPrice),
          currencyID: this.selectedCurrency.id,
          sWHType: "FULL"
        };
        if (this.pricingJSON.length < 2) {
          this.pricingJSON.push(json);
        }
      }
    }

    if (this.couplePrice) {
      if (this.selectedData.data.UsageType === "SHARED") {
        // const sqftArr = this.sharedWarehousePricing.filter(e => e.addChrBasis === 'PER_SQFT_PER_DAY')
        let json = {
          addChrID: this.selectedchargeSQFT.addChrID,
          addChrCode: this.selectedchargeSQFT.addChrCode,
          addChrName: this.selectedchargeSQFT.addChrName,
          addChrType: this.selectedchargeSQFT.addChrType,
          priceBasis: this.selectedchargeSQFT.addChrBasis,
          price: parseFloat(this.couplePrice),
          currencyID: this.selectedCurrency.id,
          sWHType: "SHARED",
          addOnBasis: this.addOnSQFT.codeVal,
          addOnCount: 1,
          addOnPrice: parseFloat(this.addOnSQFTPrice)
        };
        this.pricingJSON.push(json);
      } else if (this.selectedData.data.UsageType === "FULL") {
        let json = {
          addChrID: this.fullWarehousePricing[1].addChrID,
          addChrCode: this.fullWarehousePricing[1].addChrCode,
          addChrName: this.fullWarehousePricing[1].addChrName,
          addChrType: this.fullWarehousePricing[1].addChrType,
          priceBasis: this.fullWarehousePricing[1].addChrBasis,
          price: parseFloat(this.couplePrice),
          currencyID: this.selectedCurrency.id,
          sWHType: "FULL"
        };
        if (this.pricingJSON.length < 2) {
          this.pricingJSON.push(json);
        }
      }
    }

    if (this.thirdPrice) {
      if (this.selectedData.data.UsageType === "SHARED") {
        // const pltArr = this.sharedWarehousePricing.filter(e => e.addChrBasis === 'PER_PLT_PER_DAY')
        let json = {
          addChrID: this.selectedchargePLT.addChrID,
          addChrCode: this.selectedchargePLT.addChrCode,
          addChrName: this.selectedchargePLT.addChrName,
          addChrType: this.selectedchargePLT.addChrType,
          priceBasis: this.selectedchargePLT.addChrBasis,
          price: parseFloat(this.thirdPrice),
          currencyID: this.selectedCurrency.id,
          sWHType: "SHARED",
          addOnBasis: this.addOnPLT.codeVal,
          addOnCount: 1,
          addOnPrice: parseFloat(this.addOnPLTPrice)
        };
        this.pricingJSON.push(json);
      }
    }

    if (this.selectedPriceDed) {
      if (this.selectedData.data.UsageType === "SHARED") {
        // const cbmArr = this.sharedWarehousePricing.filter(e => e.addChrBasis === 'PER_SQM_PER_DAY_DED')
        let json = {
          addChrID: this.selectedchargeSQMDed.addChrID,
          addChrCode: this.selectedchargeSQMDed.addChrCode,
          addChrName: this.selectedchargeSQMDed.addChrName,
          addChrType: this.selectedchargeSQMDed.addChrType,
          priceBasis: this.selectedchargeSQMDed.addChrBasis,
          price: parseFloat(this.selectedPriceDed),
          currencyID: this.selectedCurrencyDed.id,
          sWHType: "DEDICATED",
          addOnBasis: this.addOnSQMDed.codeVal,
          addOnCount: 1,
          addOnPrice: parseFloat(this.addOnSQMPriceDed)
        };
        this.pricingJSON.push(json);
      }
    }

    if (this.couplePriceDed) {
      if (this.selectedData.data.UsageType === "SHARED") {
        // const sqftArr = this.sharedWarehousePricing.filter(e => e.addChrBasis === 'PER_SQFT_PER_DAY_DED')
        let json = {
          addChrID: this.selectedchargeSQFTDed.addChrID,
          addChrCode: this.selectedchargeSQFTDed.addChrCode,
          addChrName: this.selectedchargeSQFTDed.addChrName,
          addChrType: this.selectedchargeSQFTDed.addChrType,
          priceBasis: this.selectedchargeSQFTDed.addChrBasis,
          price: parseFloat(this.couplePriceDed),
          currencyID: this.selectedCurrencyDed.id,
          sWHType: "DEDICATED",
          addOnBasis: this.addOnSQFTDed.codeVal,
          addOnCount: 1,
          addOnPrice: parseFloat(this.addOnSQFTPriceDed)
        };
        this.pricingJSON.push(json);
      }
    }

    if (this.thirdPriceDed) {
      if (this.selectedData.data.UsageType === "SHARED") {
        // const pltArr = this.sharedWarehousePricing.filter(e => e.addChrBasis === 'PER_PLT_PER_DAY_DED')
        let json = {
          addChrID: this.selectedchargePLTDed.addChrID,
          addChrCode: this.selectedchargePLTDed.addChrCode,
          addChrName: this.selectedchargePLTDed.addChrName,
          addChrType: this.selectedchargePLTDed.addChrType,
          priceBasis: this.selectedchargePLTDed.addChrBasis,
          price: parseFloat(this.thirdPriceDed),
          currencyID: this.selectedCurrencyDed.id,
          sWHType: "DEDICATED",
          addOnBasis: this.addOnPLTDed.codeVal,
          addOnCount: 1,
          addOnPrice: parseFloat(this.addOnPLTPriceDed)
        };
        this.pricingJSON.push(json);
      }
    }
  }


  months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  getDateStr($date) {
    const strMonth = isNumber($date.month) ? this.months[$date.month - 1] + "" : "";
    return $date.day + '-' + strMonth + '-' + $date.year
  }

  toggleGlow() {
    this.carrier.nativeElement.classList.add()
    this.origin.nativeElement.classList.add()
    this.destination.nativeElement.classList.add()
    this.cargo.nativeElement.classList.add()
    this.container.nativeElement.classList.add()
  }

  onEditorBlured(quill, type) {
  }

  onEditorFocused(quill, type) {
  }

  onEditorCreated(quill, type) {
  }
  onContentChanged($event, type) {
    this.editorContent = $event.html
  }

  isEmpty(htmlString) {
    if (htmlString) {
      const parser = new DOMParser();
      const { textContent } = parser.parseFromString(htmlString, "text/html").documentElement;
      return !textContent.trim();
    } else {
      return true;
    }
  };

  ngOnDestroy() {

  }

}

export function isNumber(value: any): boolean {
  return !isNaN(toInteger(value));
}

export function toInteger(value: any): number {
  return parseInt(`${value}`, 10);
}


export interface WarehousePricing {
  addChrBasis?: string;
  addChrCode?: string;
  addChrDesc?: string;
  addChrID?: number;
  addChrName?: string;
  addChrType?: string;
  modeOfTrans?: string;
}