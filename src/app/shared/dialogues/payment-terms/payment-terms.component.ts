import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { ToastrService } from 'ngx-toastr';
import { cloneObject } from '../../../components/pages/user-desk/reports/reports.component';
import { ViewBookingService } from '../../../components/pages/user-desk/view-booking/view-booking.service';
import { hmPaymentPolicy, hmPaymentTerms } from '../../../constants/base.url';
import { getLoggedUserData, isArrayValid, loading, isMobile } from '../../../constants/globalFunctions';
import { JsonResponse, UserInfo } from '../../../interfaces';
import { CommonService } from '../../../services/common.service';
import { IRateRequest } from '../request-add-rate-dialog/request-add-rate.interface';
import { IPaymentTerms, ITermOption } from './payment-terms.interface';

@Component({
  selector: 'app-payment-terms-dialog',
  templateUrl: './payment-terms.component.html',
  styleUrls: ['./payment-terms.component.scss']
})
export class PaymentTermsDialogComponent implements OnInit {
  @Input() paymentTerms: IPaymentTerms = null
  @Input() selectedTermsOpt: ITermOption = null
  @Input() paymentTermsOptions: ITermOption[] = []
  @Input() action = 'view'
  @Input() from = 'SHIP'
  @Input() request: any = null
  @Input() isEnterprise = false
  editorContent = null
  isEdit = false
  hasChanges = false

  isRouting: boolean = false
  activeTabID
  remarksContent = ''
  loginUser: UserInfo
  showHMPolicy = false
  hmTerms = ''
  
  isMobile = isMobile()

  constructor(
    private _activeModal: NgbActiveModal,
    private _bookService: ViewBookingService,
    private _dropdownservice: CommonService,
    private _toastr: ToastrService
  ) { }

  ngOnInit() {
    this.showHMPolicy = hmPaymentPolicy
    try {
      if (this.request.BookingSourceVia && this.request.BookingSourceVia.includes('ENTERPRISE'))
        this.isEnterprise = true
    } catch { }
    this.loginUser = getLoggedUserData()
    if (this.from === 'SHIP') {
      this.getTermsOptions()
    } else {
      this.setTermOptions()
    }
    try { this.setHMTerms() } catch (error) { }
    this.isRouting = false
    if (this.action === 'edit')
      this.isEdit = true
  }

  setHMTerms() {
    const _hmPaymentTerms = hmPaymentTerms
    console.log(this.request.ShippingModeCode);
    
    switch (this.request.ShippingModeCode) {
      case 'SEA':
        this.hmTerms = _hmPaymentTerms.SEA
        break
      case 'AIR':
        this.hmTerms = _hmPaymentTerms.AIR
        break
      case 'TRUCK':
        this.hmTerms = _hmPaymentTerms.TRUCK
        break
      case 'WAREHOUSE':
        this.hmTerms = _hmPaymentTerms.WAREHOUSE
        break
      default:
        this.hmTerms = _hmPaymentTerms.SEA
        break
    }
  }

  closeModal($action: any) {
    if (this.isRouting)
      return
    this._activeModal.close($action);
  }

  onConfirmClick = ($action: string) => this.closeModal($action)


  isEmpty(htmlString) {
    if (htmlString) {
      new DOMParser().parseFromString(htmlString, 'text/html').documentElement.textContent.trim();
    } else {
      return true;
    }
  }

  savePayTerms() {
    if (!this.hasChanges) {
      this._toastr.error('No changes have been made', 'No Changes')
      return
    }
    const _selectedTerm = this.paymentTermsOptions.filter(_term => _term.IsSelected)
    if (!isArrayValid(_selectedTerm, 0)) {
      this._toastr.error('Please select atleast one payment terms option', 'Payment Term not selected')
      return
    }
    console.log('asd');

    if (isArrayValid(_selectedTerm, 0) && typeof _selectedTerm[0].PTermDays && !parseInt(_selectedTerm[0].PTermDays as any)) {
      this._toastr.error('Please select days for payment terms', 'Payment Term days missing')
      return
    }
    if (this.from === 'RR') {
      let toSend: any = {
        ShippingMode: this.request.ShippingModeCode,
        ParameterType: "CUSTOMER_PAYMENT_TERMS",
        ParameterValue:
          JSON.stringify({ ..._selectedTerm[0], PTermRemarks: this.remarksContent }),
        ResponseID: null,
        ResponseTagID: null,
        ProviderID: this.request.ProviderID ? this.request.ProviderID : 0,
        UserID: this.request.Customer ? this.request.Customer.UserID : this.request.UserID,
        IsUpdateByProvider: false
      }

      const onSuccess = (res: JsonResponse) => {
        this.isRouting = false
        if (res.returnId > 0) {
          this._toastr.success('Request Payment Terms Save Successfully', 'Success')
          this._activeModal.close({ data: this.editorContent })
        } else {
          if (res.returnCode === '2')
            this._toastr.info(res.returnText, res.returnStatus)
          else
            this._toastr.error(res.returnText, res.returnStatus)
        }
      }
      const onError = () => (this.isRouting = false)
      this.isRouting = true

      if (this.request.RequestID) {
        this._bookService.updateRequestTerms(this.loginUser.UserID, { ...toSend, RequestID: this.request.RequestID })
          .subscribe(onSuccess, onError)
      } else {
        this._bookService.updateBookingTerms(this.loginUser.UserID, { ...toSend, BookingID: this.request.BookingID })
          .subscribe(onSuccess, onError)
      }
    } else {
      this._activeModal.close({ ..._selectedTerm[0], PTermRemarks: this.remarksContent } as ITermOption)
    }
  }

  getTermsOptions() {
    this._dropdownservice.getPaymentTermsCondition(
      this.request.Customer ? this.request.Customer.CustomerID : this.request.CustomerID,
      this.paymentTerms.ShippingModeID,
      this.paymentTerms.ContainerLoadType
    ).subscribe((res: JsonResponse) => {
      console.log(res)
      loading(false)
      if (res.returnId > 0) {
        this.paymentTermsOptions = res.returnObject.filter(_term => _term.IsSelected)
        this.setTermOptions()
      }
    }, () => loading(false))
  }

  setTermOptions() {
    if (this.selectedTermsOpt) {
      try {
        this.remarksContent = this.selectedTermsOpt.PTermRemarks
        this.paymentTermsOptions.forEach(_terms => {
          if (typeof _terms.PTermID === 'number' && _terms.PTermID === this.selectedTermsOpt.PTermID)
            _terms.PTermDays = this.selectedTermsOpt.PTermDays
        })
      } catch { this.remarksContent = null }
    } else {
      try {
        this.remarksContent = cloneObject(this.paymentTermsOptions.filter(_term => _term.PTermRemarks)[0]).PTermRemarks
      } catch { this.remarksContent = null }
    }
  }

  onTermOption(termOption: ITermOption, termInd: number) {
    termOption.IsSelected = !termOption.IsSelected
    if (typeof termOption.PTermDays === 'number' && !termOption.IsSelected)
      termOption.PTermDays = 0
    for (let index = 0; index < this.paymentTermsOptions.length; index++) {
      if (index !== termInd) {
        this.paymentTermsOptions[index].IsSelected = false
        if (typeof this.paymentTermsOptions[index].PTermDays === 'number')
          this.paymentTermsOptions[index].PTermDays = 0
      }
    }
  }

  notifCounter(termOption: ITermOption, action: string) {
    if (!termOption.IsSelected)
      return
    if (action === 'add' && termOption.PTermDays < 100)
      termOption.PTermDays = parseInt(termOption.PTermDays as any) + 1
    else if (action === 'sub' && termOption.PTermDays > 0)
      termOption.PTermDays = parseInt(termOption.PTermDays as any) - 1
  }
}