export interface IPaymentTerms {
    TermsConditionID: number;
    PaymentTermsID: number;
    TermsConditionName: string;
    TermsConditionText: string;
    CustomerID: number;
    ProviderID?: any;
    IsByCustomer: boolean;
    IsByProvider: boolean;
    LoginUserID: number;
    ShippingModeID: number;
    ContainerLoadType: string;
    HasChanged?: boolean
    ShippingModeCode?: string
}

export interface IPaymentTermsSettings {
    tabID?: string // only for settings
    termID: number
    modeCode: string
    modeIcon: string
    modeID: number
    editorContent: string
    isEdit: boolean
    isChanged: boolean
    containerLoad: string
    tabDisplay: string
    termName: string
    hasChanged: boolean
}

export interface ITermOption {
    PTermID: number;
    PTermCode: string;
    PTermText: string;
    PTermDesc: string;
    PTermDays?: number;
    PTermRemarks: string;
    PTermBL: string;
    ShippingModeID: number;
    ShippingModeCode: string;
    ContainerLoadType: string;
    IsSelected: boolean;
}