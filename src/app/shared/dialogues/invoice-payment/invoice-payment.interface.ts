import { IInvoiceDetail } from "../invoice-upload/invoice.interface";

export interface IPaymentDetail {
    PaymentDocument: any
    PaymentDtlID: number
    PaymentID: number
    PaymentModeID: number
    PaymentMode: string
    DocNo: string
    DocDate: string
    CurrencyID: number
    CurrencyCode: string
    ExchangeRate?: any
    ExRateListID?: any
    BankID: number
    OtherBank: string
    Amount: number
}

export interface ISalesData {
    HMCommissionRate?: number
    HMCommissionAmount?: number
    HMCommSTaxRate?: number
    HMCommSTaxAmount?: number
    RoundByDiffAmount?: number
    NetPaymentAmount?: number
}
export interface IInvoicePayment extends ISalesData {
    DocNo: any;
    PaymentMode: string;
    OtherBank: any;
    BankID: any;
    DocDate: any;
    DocAmount: any;
    PaymentRefDate: any
    RemainingAmount: any
    PaidAmount: any
    InvoiceAmount: any
    PaymentID: number
    PaymentDate: string
    PaymentNo?: any
    InvoiceID: number
    ReferenceNo?: any
    ReferenceDate?: any
    CurrencyID: number
    CurrencyCode: string
    ExchangeRate?: any
    ExRateListID?: any
    PaymentAmount: number
    PaymentTo?: string
    PaymentToDesc?: string
    PaymentDetail: IPaymentDetail[]
    PaymentDocumentDetail?: any
    Remarks?: string
}


export interface IPaymentDoc {
    DocumentID: number
    DocumentName: string
    DocumentTypeID: number
    DocumentTypeName: string
    DocumentFile: string
}

export interface IInvoicePaymentItem {
    PaymentID: number
    PaymentDate: string
    PaymentNo: string
    ReferenceNo?: any
    ReferenceDate?: any
    CurrencyID: number
    CurrencyCode: string
    PaymentAmount: number
    Remarks?: any
    Doucuments: IPaymentDoc[]
    PaymentDetail: IInvoiceDetail[]
    PaymentModeID: number
    PaymentMode: string
    DocNo: string
    DocDate: string
    BankID: number
    OtherBank: string
    DocAmount: number
    PaymentTo?: string
    PaymentToDesc?: string
    PaymentModeTitle?: string
}
