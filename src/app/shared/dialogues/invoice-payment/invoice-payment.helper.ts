import { ElementRef, Injectable, Input, ViewChild } from "@angular/core"
import { FormGroup } from "@angular/forms"
import { BookingStatus } from "../booking-status-updation/booking-status-updation.component"
import { Observable } from 'rxjs'
import { debounceTime, map } from 'rxjs/operators'
import * as moment from 'moment'
import { CodeValMst, CurrencyDetails, UserDocument, UserInfo } from "../../../interfaces"
import { getScreenHeight, getScreenHeightPayment, isArrayValid } from "../../../constants/globalFunctions"
import { NgFilesConfig } from "../../../directives/ng-files"
import { IInvoicePaymentItem, ISalesData } from "./invoice-payment.interface"
import { IInvoiceCard } from "../../../components/pages/user-desk/invoice-card/invoice-card.interface"
import { applyRoundByDecimal } from "../../../components/pages/user-desk/reports/reports.component"
import { IInvoiceDetail } from "../invoice-upload/invoice.interface"


@Injectable()
export class InvoicePaymentHelper {
    loading: boolean = false
    invoiceStatusList: BookingStatus[] = []


    public currentDocObject: UserDocument
    public commissionDocument: UserDocument
    allowedFiles = 'pdf, jpg, jpeg, xlsx, xls, doc, docx, png, txt'
    docText = '(supported types: pdf, jpg, jpeg, xlsx, doc, docx, png and txt. Maximum size per document: 10MB)'
    public config: NgFilesConfig = {
        acceptExtensions: ['pdf', 'jpg', 'jpeg', 'xlsx', 'xls', 'doc', 'docx', 'png'],
        maxFilesCount: 1,
        maxFileSize: 12 * 1024 * 1000,
        totalFilesSize: 12 * 12 * 1024 * 1000
    }
    public additionalDocuments: UserDocument[] = []
    docTypeList = []
    isDocConfigSet = false

    heightLimit = getScreenHeightPayment()
    public allCurrencies: CurrencyDetails[] = []
    minDate = null

    transNumber = null
    transDate = null
    selectedCurrency: CurrencyDetails
    docAmount = null
    invoiceAmount = null
    paidAmount = null
    remainingAmount = null

    transNumberInvalid = false
    transDateInvalid = false
    selectedCurrencyInvalid = false
    receiptAmountInvalid = false


    commissionRateInvalid = false
    salesTaxInvalid = false

    rounding = null
    roundingInvalid = false
    loginUser: UserInfo
    hasDocUploaded = false
    currentDocID = -1
    salesData: ISalesData = null
    savedInstrucmentContent: any = null
    savedCommissionContent: any = null
    selectedDocument
    hasUploadedDocs = false
    invAmountCaption = 'Payment Amount'
    isAdjustment = false
    viewMode = "FORM"
    paidItems: IInvoicePaymentItem[] = []
    @Input() invoice: IInvoiceCard

    constructor() { }


    closeFix(event, datePicker) {
        if (event.target.offsetParent == null || event.target.offsetParent.nodeName != 'NGB-DATEPICKER')
            datePicker.close()
    }

    getNum = (_num: any) =>
        (_num) ? (parseFloat(_num) ? typeof parseFloat(_num) === 'number' ? parseFloat(_num) : 0 : 0) : 0

    formatter = currency => currency.title
    currencyFormatter = currency => currency.shortName
    formatterBank = (_bank) => _bank.title

    currencies = (text$: Observable<string>) => text$.pipe(debounceTime(200), map(term => !term || term.length < 3
        ? [] : this.allCurrencies.filter(
            v => v.shortName && v.shortName.toLowerCase().indexOf(term.toLowerCase()) > -1)
    ))


}


