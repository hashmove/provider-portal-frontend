import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core'
import { PlatformLocation } from '@angular/common'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { InvoicePaymentHelper } from './invoice-payment.helper'
import * as moment from 'moment'
import { IInvoiceTrail } from '../../../components/pages/user-desk/invoice-card/invoice-card.interface'
import { ViewBookingService } from '../../../components/pages/user-desk/view-booking/view-booking.service'
import { baseExternalAssets } from '../../../constants/base.url'
import { getLoggedUserData, isMobile } from '../../../constants/globalFunctions'
import { JsonResponse } from '../../../interfaces'
@Component({
  selector: 'app-invoice-payment',
  templateUrl: './invoice-payment.component.html',
  styleUrls: ['./invoice-payment.component.scss']
})
export class InvoicePaymentComponent extends InvoicePaymentHelper implements OnInit {

  @Input() selectedInvoice: IInvoiceTrail
  @Input() from = 'book'
  @Input() groupLogic = ''
  @Input() action = 'add'
  @ViewChild('bankNameCmp') bankNameCmp: ElementRef;
  @ViewChild('scrollContainer') scrollContainer: ElementRef;

  isMobile = isMobile()


  constructor(
    private location: PlatformLocation,
    private _activeModal: NgbActiveModal,
    private bookingService: ViewBookingService
  ) {
    super()
    location.onPopState(() => this.closeModal(null))
  }

  ngOnInit() {
    this.loginUser = getLoggedUserData()
    const date = moment(new Date()).subtract('months', 3)
    this.minDate = { month: date.month() + 1, day: date.date(), year: date.year() }
    this.viewMode = 'LIST'
    this.getPaymentList()
  }

  getPaymentList() {
    this.bookingService.getBookingInvoicePaymentsDetail(this.selectedInvoice.invoiceID, -1).subscribe((res: JsonResponse) => {
      if (res.returnId > 0) {
        this.paidItems = res.returnObject
        this.paidAmount = this.paidItems.map(_payment => this.getNum(_payment.PaymentAmount)).reduce((all, item) => {
          return all + item;
        })
        this.invoiceAmount = this.getNum(this.selectedInvoice.invoiceAmount)
        this.remainingAmount = this.getNum(this.invoiceAmount - this.paidAmount)
        console.log(this.paidAmount)
        console.log(this.invoiceAmount)
        console.log(this.remainingAmount)
      }
    })
  }

  acDownloadAction($url: string, $event) {
    // $event.preventDefault()
    // $event.stopPropagation()
    if ($url && $url.length > 0) {
      const _urlStr = $url.startsWith('[{') ? JSON.parse($url)[0].DocumentFile : $url
      window.open(baseExternalAssets + _urlStr, '_blank')
      if (this.invoice.BookingStatus.toLowerCase() === 'cancelled') return
    }
  }

  scrollToBottom(): void {
    try {
      this.scrollContainer.nativeElement.scrollTop = this.scrollContainer.nativeElement.scrollHeight;
    } catch (err) {
      console.log(err)
    }
  }

  closeModal(status) {
    this._activeModal.close(status)
    document.getElementsByTagName('html')[0].style.overflowY = 'auto'
  }
}
