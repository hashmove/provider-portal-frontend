import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AirAfricaCargoDetailsComponent } from './air-africa-cargo-dtl.component';

describe('AirAfricaCargoDetailsComponent', () => {
  let component: AirAfricaCargoDetailsComponent;
  let fixture: ComponentFixture<AirAfricaCargoDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AirAfricaCargoDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AirAfricaCargoDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
