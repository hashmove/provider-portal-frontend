import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-air-africa-cargo-dtl',
  templateUrl: './air-africa-cargo-dtl.component.html',
  styleUrls: ['./air-africa-cargo-dtl.component.scss']
})
export class AirAfricaCargoDetailsComponent implements OnInit {
  @Input() data: any

  selectedTypeBasis: string = 'N/A'
  desc: string = 'N/A'
  selectedFrequency: string = 'N/A'
  chargeableWeight: number = 0
  lclChips: Array<string> = []
  cbm = 0
  grossweight = 0
  cargoLoadType = 'by_total_ship'
  searchCriteria
  constructor(
    public _activeModal: NgbActiveModal,
  ) { }

  ngOnInit() {
    const { data } = this
    const searcCrit = JSON.parse(data.JsonSearchCriteria)
    this.searchCriteria = searcCrit
    this.lclChips = searcCrit.lclAirContainerChips
    this.cargoLoadType = searcCrit.cargoLoadType
    try {
      this.cbm = searcCrit.airChipData.volume
      this.grossweight = searcCrit.airChipData.weight
    } catch (error) {
    }
    this.chargeableWeight = searcCrit.chargeableWeight
    this.chargeableWeight = searcCrit.chargeableWeight
    this.selectedTypeBasis = this.data.PriceBasisType
    try {
      this.selectedFrequency = JSON.parse(data.JSONSpecialRequestLogs)[0].SpecialRequestFreqShipment
    } catch (error) { }
    try {
      this.desc = JSON.parse(data.JSONSpecialRequestLogs)[0].Comments
    } catch (error) {

    }
  }


  close() {
    this._activeModal.close()
  }

}
