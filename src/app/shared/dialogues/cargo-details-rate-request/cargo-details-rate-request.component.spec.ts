import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CargoDetailsRateRequestComponent } from './cargo-details-rate-request.component';

describe('CargoDetailsRateRequestComponent', () => {
  let component: CargoDetailsRateRequestComponent;
  let fixture: ComponentFixture<CargoDetailsRateRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CargoDetailsRateRequestComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CargoDetailsRateRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
