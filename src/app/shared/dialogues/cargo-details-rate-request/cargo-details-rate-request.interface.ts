export interface IRequestContainers {
  RequestID: number;
  ResponseID?: any;
  ContIndexID: string;
  ContainerSpecID: number;

  RequestedQty: number;
  RequestedCBM: number;
  RequestedWeight: number;

  RespondedQty: number;
  RespondedCBM: number;
  RespondedWeight: number;

  BookedQty: number;
  BookedCBM: number;
  BookedWeight: number;

  VolumetricWeight: number;
  ContainerSpecCode: string;
  ContainerSpecDesc: string;
  ContainerSpecImage: string;
  ContainerSpecShortName: string;
  ContainerHeight: number;
  ContainerWidth: number;
  ContainerLength: number;
  DimensionUnit: string;
  ContainerWeight: number;
  WeightUnit: string;
  IsQualityMonitoringRequired: string;
  IsTrackingRequired: string;
  PickupDate?: string
  PickupDateTo?: string
  CargoReadinessDate?: string
  quantity?: any
  contSpecID?: any
  SortingOrder: number
}
