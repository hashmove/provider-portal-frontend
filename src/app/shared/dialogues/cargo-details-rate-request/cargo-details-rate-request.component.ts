import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { feet2String, loading } from '../../../constants/globalFunctions';
import { DashboardService } from '../../../components/pages/user-desk/dashboard/dashboard.service';
import { CurrencyControl } from '../../../services/currency.service';
import { JsonResponse } from '../../../interfaces';
import { IRateRequest, SearchCriteria } from '../request-add-rate-dialog/request-add-rate.interface';
import { LclChip } from '../cargo-details/cargo-details.interface';
import { IRequestContainers } from './cargo-details-rate-request.interface';

@Component({
  selector: 'app-cargo-details-rate-request',
  templateUrl: './cargo-details-rate-request.component.html',
  styleUrls: ['./cargo-details-rate-request.component.scss']
})
export class CargoDetailsRateRequestComponent implements OnInit {
  @Input() request: IRateRequest
  @Input() searchCriteria: SearchCriteria = null
  @Input() contResp: JsonResponse = null
  public containers: any[] = []
  totalCBM = 0
  totalWeight = 0
  searchMode = 'sea-fcl'
  commodity = ''
  lclContainers: LclChip[] = []
  lclAirContainerChips: any[] = []
  IsAdmin = false
  editPaymentTerms = false
  paymentTerms = null
  public editorContentAgent: any;
  public editorContentConsignee: any;
  private toolbarOptions = [
    ['bold', 'italic', 'underline'],
    [{ 'list': 'ordered' }, { 'list': 'bullet' }],
    [{ 'script': 'sub' }, { 'script': 'super' }],
    [{ 'direction': 'rtl' }],
    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
    [{ 'color': [] }, { 'background': [] }],
    [{ 'align': [] }],
    ['clean']
  ];
  public editorOptions = {
    placeholder: 'insert content...',
    modules: { toolbar: this.toolbarOptions }
  }

  pickupCaption = ''
  pickupDate: string = null
  pickupDateTo: string = null
  reqContTotal = 0
  resContTotal = 0
  bokContTotal = 0
  loading = false


  constructor(
    public _activeModal: NgbActiveModal,
    private _dashboardService: DashboardService,
    private _currencyControl: CurrencyControl
  ) { }

  ngOnInit() {
    // this.getPaymentTerms()
    loading(false)
    // try {
    //   this.commodity = this.request.CommodityDescription
    // } catch { }
    try {
      this.searchMode = this.request.ShippingModeCode.toLowerCase() + '-' + this.request.ContainerLoad.toLowerCase()
    } catch (ex) {
    }
    this.getContainerDetails()
    try {
      if (this.searchMode === 'air-lcl') {
        this.lclAirContainerChips = this.searchCriteria.lclAirContainerChips
      }
    } catch { }
  }

  close = () => this._activeModal.close()

  getPaymentTerms() {
    // this._dashboardService.getPaymentTerms(
    //   encryptBookingID(this.request.RequestID, this.request.Customer.UserID, this.request.ShippingModeCode)
    // ).subscribe(res => {
    // })
  }

  async getContainerDetails() {
    // const _id = encryptBookingID(this.request.RequestID, this.request.ProviderID, this.request.ShippingModeCode);
    // let res: JsonResponse = null
    try {
      // res = await this._dashboardService.getRequestContainer(_id, (this.request.ResponseID ? this.request.ResponseID : 0), 'PROVIDER').toPromise() as JsonResponse
      if (this.searchMode === 'air-lcl') {
        this.pickupCaption = 'Cargo Readiness Date'
        const _res = this.contResp.returnObject[0]
        this.pickupDate = _res.CargoReadinessDate
        this.commodity = _res.CommodityDescription
        // const _pickupDateTo = _res.PickupDateTo
        // this.pickupDateTo = this.pickupDate !== _pickupDateTo ? _pickupDateTo : null
      } else {
        const _res = this.contResp.returnObject[0]
        this.pickupDate = _res.CargoReadinessDate
        this.commodity = _res.CommodityDescription
        this.pickupCaption = 'Cargo Readiness Date'
        this.setContainers(this.contResp)
      }
    } catch (error) {
      console.log(error)
      setTimeout(() => {
        this.loading = false
      }, 0);
    }
    setTimeout(() => {
      this.loading = false
    }, 0);
  }

  getNum = (_num: any) => (_num) ? (parseFloat(_num) ? typeof parseFloat(_num) === 'number' ? parseFloat(_num) : 0 : 0) : 0


  setContainers(res: JsonResponse) {
    if (this.searchMode === 'sea-lcl') {
      this.containers = this.setContainersDataforView(res.returnObject)
    } else {
      this.containers = res.returnObject
    }
    const { containers } = this
    if (this.searchMode === 'sea-fcl' || this.searchMode === 'truck-ftl') {
      try {
        this.reqContTotal = containers.map(_cont => this.getNum(_cont.RequestedQty))
          .reduce((all, item) => (all + item))
        this.bokContTotal = containers.map(_cont => this.getNum(_cont.BookedQty))
          .reduce((all, item) => (all + item))
        this.resContTotal = containers.map(_cont => this.getNum(_cont.RespondedQty))
          .reduce((all, item) => (all + item))
      } catch { }
    }
    // try { this.pickupDate = containers[0].CargoReadinessDate } catch { }
    if (this.request.ContainerLoad === 'LCL' && (!this.request.AirDet || !this.request.AirDet.IsVirtualAirLine)) {
      let _cbm = 0, _weight = 0
      try {
        containers.forEach(obj => {
          if (obj.RequestedCBM) {
            _cbm += Number(obj.RequestedCBM)
            try { _weight += Number(obj.VolumetricWeight) } catch { }
          } else {
            _cbm += Number(obj.RequestedQty)
          }
        })
      } catch { }
      try {
        if (_cbm < 1) {
          _cbm = Math.ceil(_cbm)
        } else {
          _cbm = this._currencyControl.applyRoundByDecimal(_cbm, 3)
        }
        if (_weight < 1) {
          _weight = Math.ceil(_weight)
        } else {
          _weight = this._currencyControl.applyRoundByDecimal(_cbm, 3)
        }
      } catch {
      }
      this.totalCBM = _cbm
      // if (this.searchMode === 'sea-lcl') {
      //   const _searchCriteria = JSON.parse(this.request.JsonSearchCriteria)
      //   try {
      //     this.totalWeight = _searchCriteria.totalVolumetricWeight
      //   } catch {
      //     this.totalWeight = 0
      //   }
      // } else {
      this.totalWeight = _weight
      // }
    }
  }

  setContainersDataforView(_containers: IRequestContainers[]) {
    const { searchCriteria } = this
    let finnContainers = []
    try {
      for (let index = 0; index < _containers.length; index++) {
        const _container = _containers[index]
        const _contInput = searchCriteria.LclChips[index]
        finnContainers.push({
          ..._container,
          ..._contInput
        })
      }
    } catch (error) {
      console.log(error)
      finnContainers = _containers
    }
    console.log(finnContainers)
    return finnContainers
  }

  getContainerInfo(container) {
    const containerInfo = {
      containerSize: undefined,
      containerWeight: undefined
    };
    containerInfo.containerWeight = container.MaxGrossWeight;
    containerInfo.containerSize =
      feet2String(container.containerLength) +
      ` x ` +
      feet2String(container.containerWidth) +
      ` x ` +
      feet2String(container.containerHeight);
    return containerInfo;
  }

  getImageName($code: string) {
    switch ($code) {
      case 'PLTS':
        return 'Pallets.svg'
      case 'BOXS':
        return 'Boxes.svg'
      case 'DRUM':
        return 'Drums.svg'
      case 'ASMT':
        return 'AssortedShipment.svg'
      default:
        return 'Pallets.svg'
    }
  }


  onEditorBlured(quill) {
  }

  onEditorFocused(quill) {
  }

  onEditorCreated(quill) {
  }

  onContentChanged($event) {
    this.paymentTerms = $event.html
  }

  changePaymentTerm() {
    this.editPaymentTerms = true
  }

  saveTerms() {
    this.editPaymentTerms = false
  }
}