import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SharedService } from '../../../services/shared.service';
import { ScrollbarComponent } from 'ngx-scrollbar';
import { DashboardService } from '../../../components/pages/user-desk/dashboard/dashboard.service';
import { encryptBookingID } from '../../../constants/globalFunctions';

@Component({
  selector: 'app-adjustment-logs',
  templateUrl: './adjustment-logs.component.html',
  styleUrls: ['./adjustment-logs.component.scss']
})
export class AdjustmentLogsComponent implements OnInit {
  closeResult: string;
  @Input() logs: IAdjustmentLogs[] = []
  constructor(
    public _activeModal: NgbActiveModal,
  ) { }


  ngOnInit() {
  }
}


export interface IAdjustmentLogs {
  LogDate: string;
  UpdateBy: string;
  PreviousAmount: number;
  UpdatedAmount: number;
  AdjustmentAmount: number;
  ProviderRemarks: string;
  CurrencyCode: string
}