import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdjustmentLogsComponent } from './adjustment-logs.component';

describe('AdjustmentLogsComponent', () => {
  let component: AdjustmentLogsComponent;
  let fixture: ComponentFixture<AdjustmentLogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdjustmentLogsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdjustmentLogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
