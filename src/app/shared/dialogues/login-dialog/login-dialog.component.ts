import { Component, OnInit, Output, EventEmitter } from '@angular/core'
import { PlatformLocation } from '@angular/common'
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { ToastrService } from 'ngx-toastr'
import { JsonResponse } from '../../../interfaces/JsonResponse'
import { EMAIL_REGEX, ValidateEmail, isJSON, getDefaultIp, loading, base64Encode, getLoggedUserData, setAccessRights } from '../../../constants/globalFunctions'
import { HttpErrorResponse } from '@angular/common/http'
import { Router } from '@angular/router'
import * as moment from 'moment'
import { UserCreationService } from '../../../components/pages/user-creation/user-creation.service'
import { SharedService } from '../../../services/shared.service'
import { baseExternalAssets } from '../../../constants/base.url'
import { AESModel, encryptStringAES, GuestService } from '../../../services/jwt.injectable'
import { IProviderConfig, UserInfo } from '../../../interfaces/billing.interface'
import { BasicInfoService } from '../../../components/pages/user-creation/basic-info/basic-info.service'
import { Base64 } from 'js-base64'
@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.scss'],
  providers: [
    NgbActiveModal,
  ]
})
export class LoginDialogComponent implements OnInit {
  public savedUser: boolean
  public saveUserData: any
  public userAvatar: string
  public userName: string = undefined
  public countryCode: string
  public loading: boolean
  public colorEye
  public placeholder: string = "Your unique password"
  public emailError
  public passError
  public loginForm: FormGroup
  @Output() onForgetPassClick = new EventEmitter<string>()
  showResend = false
  userID = -1

  constructor(
    private activeModal: NgbActiveModal,
    private modalService: NgbModal,
    private _userCreationService: UserCreationService,
    private toastr: ToastrService,
    private _router: Router,
    private location: PlatformLocation,
    private _sharedService: SharedService,
    private _basicInfoService: BasicInfoService
  ) {
    location.onPopState(() => this.closeModal())
  }

  ngOnInit() {
    if (localStorage) {
      if (localStorage.getItem('userInfo') && Object.keys('userInfo').length) {
        this.saveUserData = getLoggedUserData()
        this.savedUser = true
        if (this.saveUserData.ProviderImage && this.saveUserData.ProviderImage != "[]" && isJSON(this.saveUserData.ProviderImage))
          this.userAvatar = baseExternalAssets + JSON.parse(this.saveUserData.ProviderImage)[0].DocumentFile
        this.placeholder = "Enter your unique password"
        this.userName = this.saveUserData.FirstNameBL + ' ' + this.saveUserData.LastNameBL
      }
    }
    this.createForm()
  }

  private createForm() {
    this.loginForm = new FormGroup({
      loginUserID: new FormControl(null, [Validators.required, Validators.pattern(EMAIL_REGEX), Validators.maxLength(320)]),
      password: new FormControl(null, [Validators.required]),
    })

    if (this.savedUser)
      this.loginForm.controls['loginUserID'].setValue(this.saveUserData.LoginID)
  }



  forgetPassword = () => this.onForgetPassClick.emit('f_pass')

  closeModal() {
    this.activeModal.close()
    document.getElementsByTagName('html')[0].style.overflowY = 'auto'
  }


  loginUser(data) {
    let valid: boolean = ValidateEmail(data.loginUserID)
    if (!valid) {
      this.toastr.error('Invalid email entered.', 'Error')
      return
    }
    const loc: any = this._sharedService.getMapLocation()
    this.loading = true
    const _loginObj = {
      password: data.password,
      loginUserID: data.loginUserID,
      CountryCode: (loc) ? loc.countryCode : 'DEFAULT',
      LoginIpAddress: getDefaultIp(),
      LoginDate: moment(Date.now()).format(),
      LoginRemarks: ""
    }
    // const encObjectL: AESModel = encryptStringAES({
    //   d1: moment(Date.now()).format().substring(0, 16),
    //   d2: JSON.stringify(_loginObj),
    //   d3: null
    // })
    // this._userCreationService.userLoginV2(encObjectL).subscribe(async (response: JsonResponse) => {
    this._userCreationService.userLogin(_loginObj).subscribe(async (response: JsonResponse) => {
      let resp = response
      if (resp.returnId > 0) {
        if (resp.returnText) {
          localStorage.removeItem('isGuestLogin')
          this.loading = false
          const parseObject: UserInfo = JSON.parse(resp.returnText)
          if (!parseObject.IsVerified) {
            this.showResend = true
            this.userID = parseObject.UserID
            this.toastr.warning('Please verify your email to access the portal')
            return
          }
          if (localStorage) {
            try { GuestService.setSourceVia() } catch (error) { }
            if (resp.returnObject) {
              this._userCreationService.saveJwtToken(resp.returnObject.token)
              this._userCreationService.saveRefreshToken(resp.returnObject.refreshToken)
              try { setAccessRights(resp.returnObject.objectRights) } catch { }
            }
            let loginData = JSON.parse(resp.returnText)
            loginData.IsLogedOut = false
            if (loginData.WHID)
              localStorage.setItem('warehouseId', loginData.WHID)
            if (parseObject.DataSource && parseObject.DataSource.toUpperCase() === 'PRE-SALES_REGISTRATION'.toUpperCase()) {
              try {
                localStorage.setItem('ServicesOfferedJSON', parseObject.ServicesOfferedJSON)
              } catch (error) {
                console.log(error)
              }
              this.registration(parseObject)
            } else if (parseObject.DataSource && parseObject.DataSource.toUpperCase() === 'EmailMarketingCompaign'.toUpperCase()) {
              try { localStorage.removeItem('userInfo') } catch { }
              this._sharedService.marketingUser = parseObject
              this.toastr.success('Login Successful!', 'Success')
              this.getProviderSettings(getLoggedUserData().ProviderID, resp)
              try {
                localStorage.setItem('sourceType', parseObject.DataSourceType)
              } catch (error) { }
              this.activeModal.close()
              document.getElementsByTagName('html')[0].style.overflowY = 'auto'
              this._router.navigate(['registration'])
            } else {
              localStorage.setItem('userInfo', JSON.stringify(resp))
              this.toastr.success('Login Successful!', 'Success')
              await this.getProviderSettings(getLoggedUserData().ProviderID, resp)
              this.activeModal.close()
              document.getElementsByTagName('html')[0].style.overflowY = 'auto'
              this._sharedService.IsloggedIn.next(loginData.IsLogedOut)
              if (loginData.UserProfileStatus.toLowerCase() === "dashboard") {
                this._router.navigate(['provider/dashboard'])
              } else {
                const otpKey = base64Encode(loginData.UserID)
                this._basicInfoService.getUserOtpVerified(otpKey).subscribe((res: JsonResponse) => {
                  if (res.returnId == 1) {
                    this._router.navigate(['password', otpKey])
                  } else {
                    this._router.navigate(['business-info'])
                  }
                })
              }
            }
          } else {
            this.toastr.warning("Please Enable Cookies to use this app", "Cookies Disabled")
            this.activeModal.close()
            document.getElementsByTagName('html')[0].style.overflowY = 'auto'
            return
          }
        } else {
          this.toastr.warning('Please verify your email to access the portal')
        }
      } else if (response.returnCode === '2') {
        this.toastr.info(response.returnText, response.returnStatus)
        this.loading = false
      } else {
        this.loading = false
        this.toastr.error('Invalid email or password.', 'Failed')
      }
    }, (err: HttpErrorResponse) => {
      console.log(err)
      this.loading = false
      this.toastr.error('Invalid email or password.', 'Failed')
    })
  }

  async getProviderSettings(providerId, resp: JsonResponse) {
    try {
      const res = await this._userCreationService.getProviderDetails(providerId).toPromise() as JsonResponse
      if (res.returnId > 0) {
        try {
          const _providerConfig: IProviderConfig = JSON.parse(res.returnObject.FeatureToggleJson)
          const _newLoginObj = { ...getLoggedUserData(), ProviderConfig: _providerConfig }
          resp.returnText = JSON.stringify(_newLoginObj);
          localStorage.setItem("userInfo", JSON.stringify(resp));
        } catch (error) { console.log(error) }
        if (res.returnText) {
          try {
            const providerSettings = JSON.parse(res.returnText)
            if (providerSettings) {
              localStorage.setItem('customerSettings', res.returnText)
            } else {
              localStorage.removeItem('customerSettings')
            }
          } catch {
            localStorage.removeItem('customerSettings')
          }
        } else {
          localStorage.removeItem('customerSettings')
        }
      } else {
        localStorage.removeItem('customerSettings')
      }
    } catch (error) {
      localStorage.removeItem('customerSettings')
    }
  }

  async registration(preSaleData: UserInfo) {
    loading(true)
    const UserObjectBL = {
      primaryEmail: preSaleData.PrimaryEmail,
      firstName: preSaleData.FirstNameBL,
      lastName: preSaleData.LastNameBL,
      primaryPhone: preSaleData.PrimaryPhoneBL,
      countryPhoneCode: preSaleData.CountryPhoneCodeBL,
      phoneCodeCountryID: preSaleData.PhoneCodeCountryID,
      jobTitle: preSaleData.JobTitleBL,
      sourceID: -1,
      sourceType: 'PROVIDER_SIGNUP'
    }
    const CompanyObjectBL = {
      companyName: preSaleData.CompanyName,
      companyAddress: preSaleData.AddressLine1,
      companyPhone: preSaleData.PhoneNumer,
      POBox: (preSaleData.POBox) ? preSaleData.POBox : null,
      City: preSaleData.CityID,
      countryPhoneCode: preSaleData.CountryPhoneCodeBL,
      phoneCodeCountryID: preSaleData.PhoneCodeCountryID
    }
    // console.log(preSaleData)

    const parsedSerives: IServicesOfferedJSON[] = JSON.parse(preSaleData.ServicesOfferedJSON)
    let _logSerIds = null
    try {
      const res = await this._basicInfoService.getServiceOffered().toPromise() as JsonResponse
      if (res.returnId > 0) {
        const filteredId = parsedSerives.map(serive => serive.ServiceNameBaseLang.toLowerCase())
        const _logSerIds1 = res.returnObject.filter(_id => filteredId.includes(_id.logServName.toLowerCase()))
        _logSerIds = _logSerIds1.map(_service => _service.logServID)
      }
    } catch (error) { }
    let _socialLinks = []
    try {
      _socialLinks = JSON.parse(preSaleData.SocialMedia)
    } catch { }

    let _jsonAffiliations: any = null
    try {
      _jsonAffiliations = preSaleData.JsonAssociationWith ? JSON.parse(preSaleData.JsonAssociationWith) : null
    } catch { }
    const obj = {
      logisticServiceID: _logSerIds,
      countryID: preSaleData.CountryID,
      redirectUrl: window.location.protocol + "//" + window.location.host + "/password",
      socialMedias: (_socialLinks) ? _socialLinks : null,
      linkURL: null,
      companyOL: null,
      companyBL: CompanyObjectBL,
      userBL: UserObjectBL,
      userOL: null,
      jsonAssociationWith: _jsonAffiliations,
      prospectID: preSaleData.ProspectID ? preSaleData.ProspectID : null
    }

    this._basicInfoService.preSaleSignup(obj).subscribe((res: any) => {
      if (res.returnStatus == "Success") {
        this.toastr.success(res.returnText, '', { timeOut: 15 * 60 * 1000 })
        this.toastr.success('Login Successful!', 'Success')
        this.activeModal.close()
        this._router.navigate(['password', Base64.encode(JSON.parse(res.returnObject).UserID)])
        loading(false)
      }
      else {
        loading(false)
        this.toastr.error(res.returnText, '', { timeOut: 15 * 60 * 1000 })
      }
    }, (err: HttpErrorResponse) => {
      loading(false)
      console.log(err)
    })
  }

  resendEmailRequest() {
    this._userCreationService.resendEmail(this.userID).subscribe((res: JsonResponse) => {
      loading(false)
      if (res.returnId > 0) {
        this.toastr.success(res.returnText, res.returnStatus)
      } else {
        this.toastr.warning(res.returnText, res.returnStatus)
      }
    })
  }

  confirmPassword(event) {
    let element = event.currentTarget.nextSibling.nextSibling
    if (element.type === "password" && element.value) {
      element.type = "text"
      this.colorEye = "grey"
    }
    else {
      element.type = "password"
      this.colorEye = "black"
    }
  }

  navigateReg = () => this._router.navigate(['registration'])

  notMyAccount() {
    this.loginForm.reset()
    this.savedUser = !this.savedUser
    this.placeholder = "Your unique password"

  }
}

export interface IServicesOfferedJSON {
  ServiceID: number
  ServiceNameBaseLang: string
  ServiceNameOtherLang: string
}