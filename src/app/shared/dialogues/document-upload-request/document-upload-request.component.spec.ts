import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentUploadRequestComponent } from './document-upload-request.component';

describe('DocumentUploadRequestComponent', () => {
  let component: DocumentUploadRequestComponent;
  let fixture: ComponentFixture<DocumentUploadRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentUploadRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentUploadRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
