import { HttpErrorResponse } from '@angular/common/http'
import { Component, OnInit, Input } from '@angular/core'
import { baseExternalAssets } from '../../../constants/base.url'
import { JsonResponse } from '../../../interfaces/JsonResponse'
import { ToastrService } from 'ngx-toastr'
import * as moment from 'moment'
import { NgbActiveModal, NgbDateStruct, NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { cloneObject } from '../../../components/pages/user-desk/reports/reports.component'
import { getLoggedUserData, base64Encode, isMobile } from '../../../constants/globalFunctions'
import { UserDocument, MetaInfoKeysDetail, DocumentFile, IBookingDtl } from '../../../interfaces'
import { SharedService } from '../../../services/shared.service'
import { ViewBookingService } from '../../../components/pages/user-desk/view-booking/view-booking.service'
import { DashboardService } from '../../../components/pages/user-desk/dashboard/dashboard.service'
import { ReUploadDocComponent } from '../re-upload-doc/re-upload-doc.component'
import { IRateRequest } from '../request-add-rate-dialog/request-add-rate.interface'
@Component({
  selector: 'app-document-upload-request',
  templateUrl: './document-upload-request.component.html',
  styleUrls: ['./document-upload-request.component.scss']
})
export class DocumentUploadRequestComponent implements OnInit {

  @Input() request: IRateRequest
  additionalDocuments: UserDocument[] = []
  public currentDocObject: UserDocument
  selectedDocument: number
  docTypeList = []

  public displayMonths = 1
  public navigation = 'select'
  public showWeekNumbers = false
  public outsideDays = 'visible'
  public minDate = null
  public add_docs_cust: UserDocument
  loading = false
  private approvedStatus: any[] = [];
  //Remove this in html to allow provider to upload docs
  isProvider: boolean = false
  isMobile = isMobile()

  constructor(
    public _activeModal: NgbActiveModal,
    public _toastr: ToastrService,
    private _dataService: SharedService,
    private bookingService: ViewBookingService,
    private _userService: DashboardService,
    private _modalService: NgbModal
  ) { }

  ngOnInit() {
    this.getUploadedDocuments()
  }

  getDefaultDocs() {
    setTimeout(async () => {
      if (
        !this.additionalDocuments || this.additionalDocuments.length === 0
        || (this.additionalDocuments.length > 0 && this.additionalDocuments.filter(_doc => _doc.DocumentNature === 'MSDS_DOC').length === 0)
      ) {
        await this.getDocumentList()
        const { docTypeList } = this
        try {
          const { additionalDocuments } = this
          if (additionalDocuments.filter(_doc => _doc.BusinessLogic === 'MSDS_DOC').length === 0) {
            const _packingList = docTypeList.filter(_doc => _doc.businessLogic === 'MSDS_DOC')[0]
            await this.getDocumentDtl(_packingList.documentTypeID)
          }
        } catch { }
      }
    }, 0);
  }

  getUploadedDocuments() {
    this._userService.getOtherDocument('RATE_REQUEST', this.request.RequestID).subscribe((res: JsonResponse) => {
      console.log(res)
      this.loading = false
      if (res.returnId > 0 && res.returnObject && typeof res.returnObject === 'object') {
        this.additionalDocuments = res.returnObject
        // this.getDefaultDocs()
      } else {
        // this.getDefaultDocs()
      }
    }, error => {
      this.loading = false
      // this.getDefaultDocs()
    })
  }

  refreshDoc() {
    this.loading = true
    this.getUploadedDocuments()
  }

  onDocumentClick($newDocument: UserDocument, index: number, $event) {
    let newDoc: UserDocument = $newDocument
    this.currentDocObject = $newDocument
    if ((this.request.RequestStatusBL.toLowerCase() === 'cancelled' || this.request.RequestStatusBL.toLowerCase() === 'declined') ||
      ($newDocument.DocumentID && $newDocument.DocumentID > 0 && !$newDocument.IsProvider && !$newDocument.MetaInfoKeysDetail)) {
      return
    }
    try {
      if ($newDocument.DocumentTypeID === -1) {
        newDoc.MetaInfoKeysDetail.forEach((element: MetaInfoKeysDetail) => {
          if (element.DataType.toLowerCase() === 'datetime') {
            if (element.KeyValue) {
              element.DateModel = this.generateDateStructure(element.KeyValue)
            }
          }
        })
      }
    } catch (error) { }
    this.changeVisibility($newDocument)
    // this.resetAccordian(index)
  }

  async resetAccordian(index?: number) {
    if (index)
      this.additionalDocuments[index].ShowUpload = !this.additionalDocuments[index].ShowUpload

    for (let i = 0; i < this.additionalDocuments.length; i++) {
      if (i !== index)
        this.additionalDocuments[i].ShowUpload = false
    }
  }

  changeVisibility($document: UserDocument) {
    $document.ShowUpload = !$document.ShowUpload
  }

  generateDateStructure(strDate: string): NgbDateStruct {
    const arr: Array<string> = strDate.split('/')
    const dateModel: NgbDateStruct = { day: parseInt(arr[1]), month: parseInt(arr[0]), year: parseInt(arr[2]) }
    return dateModel
  }

  acDownloadAction($url: string, $event) {
    $event.preventDefault()
    $event.stopPropagation()
    if ($url && $url.length > 0) {
      const _urlStr = $url.startsWith('[{') ? JSON.parse($url)[0].DocumentFile : $url
      window.open(baseExternalAssets + _urlStr, '_blank')
    }
  }

  removeDocObj($event, docId) {
    setTimeout(() => { this.loading = true }, 10)
    const { additionalDocuments } = this
    this.additionalDocuments = additionalDocuments.filter(_doc => _doc.DocumentTypeID !== docId)
    setTimeout(() => { this.loading = false }, 20)
  }

  fetchDocDtl() {
    const { additionalDocuments, selectedDocument } = this
    if (!selectedDocument) {
      this._toastr.warning('Please select a document first')
      return
    }
    if (additionalDocuments.filter(_doc => _doc.DocumentNature !== 'ADD_DOC' && _doc.DocumentTypeID === parseInt(selectedDocument as any)).length > 0) {
      this._toastr.warning('This document already exists.')
      return
    }

    this.loading = true
    this.bookingService.getDocObject(selectedDocument).subscribe((res: JsonResponse) => {
      this.loading = false
      if (res.returnId > 0) {
        const _newDoc: UserDocument = { ...res.returnObject, ShowUpload: true }
        this.currentDocObject = _newDoc
        const _arr = [...additionalDocuments, _newDoc]
        this.additionalDocuments = _arr.filter(_doc => _doc.DocumentTypeID > -1)
        this.selectedDocument = undefined
      } else {
        this._toastr.error(res.returnText, res.returnStatus)
      }
    }, (error: any) => {
      this.loading = false
    })

  }

  onKeyPress($event, index: number, length: number) { return true }

  onDocInputChange($event: any) { }

  customDragCheck($fileEvent: DocumentFile) {
    const selectedFile: DocumentFile = $fileEvent
    this.currentDocObject.DocumentName = selectedFile.fileName
    this.currentDocObject.DocumentFileContent = selectedFile.fileBaseString
    this.currentDocObject.DocumentUploadedFileType = selectedFile.fileType
  }

  fileSelectFailedEvent($message: string) {
    this._toastr.error($message, 'Error')
  }

  dateChangeEvent($event: NgbDateStruct, index: number) {
    let selectedDate = new Date($event.year, $event.month - 1, $event.day)
    let formattedDate = moment(selectedDate).format('L')
    this.currentDocObject.MetaInfoKeysDetail[index].KeyValue = formattedDate
  }

  uploadDocument(index: number, document: UserDocument) {
    const _loginUser = getLoggedUserData()
    this.loading = true
    let toSend: UserDocument = cloneObject(document)
    const { currentDocObject } = this
    try { toSend.DocumentName = currentDocObject.DocumentName } catch { }
    try { toSend.DocumentFileContent = currentDocObject.DocumentFileContent } catch { }
    try { toSend.DocumentUploadedFileType = currentDocObject.DocumentUploadedFileType } catch { }
    let docName: string = ''
    try {
      if (toSend.DocumentNature === 'ADD_DOC') {
        docName = toSend.MetaInfoKeysDetail.filter(meta => meta.KeyName === 'DOCNAME')[0].KeyValue
        toSend.DocumentName = docName
        try {
          const _docDesc = toSend.MetaInfoKeysDetail.filter(meta => meta.KeyName === 'DOCDESC')[0].KeyValue
          if (_docDesc) {
            toSend.DocumentDesc = _docDesc
          }
        } catch { }
      }
    } catch (error) { }

    if (!toSend.DocumentFileContent && !toSend.DocumentFileName) {
      this._toastr.error('Please select a file to upload', 'Invalid Operation')
      this.loading = false
      return
    }

    let emptyFieldFlag: boolean = false
    let hasInvalidLength: boolean = false
    let emptyFieldName: string = ''

    try {
      if (toSend.MetaInfoKeysDetail) {
        toSend.MetaInfoKeysDetail.forEach((element: MetaInfoKeysDetail) => {
          if (element.IsMandatory && !element.KeyValue) {
            emptyFieldFlag = true
            emptyFieldName = element.KeyNameDesc
            return
          }

          if (element.IsMandatory && element.KeyValue && element.KeyValue.length > element.FieldLength) {
            hasInvalidLength = true
            emptyFieldName = element.KeyNameDesc
            return
          }
        })
      }
    } catch (error) { }

    if (emptyFieldFlag) {
      this._toastr.error(`${emptyFieldName} field is empty`, 'Invalid Operation')
      this.loading = false
      return
    }

    if (hasInvalidLength) {
      this._toastr.error(`${emptyFieldName} field length should be less or equal to 50 charaters`, 'Invalid Operation')
      this.loading = false
      return
    }

    try {
      if (this.additionalDocuments.filter(_doc => _doc.DocumentID > 0
        && _doc.DocumentName.toLowerCase() === toSend.DocumentName.toLowerCase()
        && _doc.BusinessLogic === 'RATE_REQUEST_ADD_DOC').length > 0) {
        this._toastr.error(`A document already exist with the same name.`, 'Duplicated Document')
        this.loading = false
        return
      }
    } catch { }

    toSend.DocumentID = (toSend.DocumentID) ? toSend.DocumentID : -1
    // toSend.BookingID = this.request.BookingID
    toSend.OtherID = this.request.RequestID ? this.request.RequestID : 0
    toSend.OtherType = 'RATE_REQUEST'
    toSend.LoginUserID = (_loginUser && _loginUser.UserID) ? _loginUser.UserID : -1

    for (let ind = 0; ind < this.additionalDocuments.length; ind++) {
      if (ind === index) {
        this.additionalDocuments[index].ShowUpload = false
      }
    }

    if (toSend.DocumentID > 0 && toSend.DocumentFileName) {
      if (toSend.DocumentLastStatus.toLowerCase() === 'approved') {
        toSend.DocumentLastStatus = 'RESET'
      } else {
        toSend.DocumentLastStatus = 'RE-UPLOAD'
      }
    } else {
      toSend.DocumentLastStatus = toSend.IsApprovalRequired ? 'DRAFT' : null
    }


    try {

      toSend.ProviderName = ""
      toSend.HashMoveBookingNum = this.request.RequestNo
      toSend.UserName = this.request.Customer.FirstName + ' ' + this.request.Customer.LastName
      toSend.UserCompanyName = this.request.Customer.CustomerName
      toSend.UserCompanyID = this.request.Customer.CustomerID
      toSend.UserCountryPhoneCode = "000" //
      toSend.EmailTo = ""
      toSend.PhoneTo = ""
    } catch (error) {
    }
    this._userService.saveUserDocument(toSend).subscribe((res: JsonResponse) => {
      this.loading = false
      if (res.returnId > 0) {
        this._toastr.success(`Document ${toSend.DocumentID > 0 ? 'Updated' : 'Saved'} Successfully`, res.returnStatus)
        this.getUploadedDocuments()
      } else {
        this._toastr.error(res.returnText, res.returnStatus)
      }
    }, (err: HttpErrorResponse) => {
      this.loading = false
    })
  }

  async addDocument($event: any) {
    if ((this.request.RequestStatusBL.toLowerCase() === 'cancelled' || this.request.RequestStatusBL.toLowerCase() === 'declined')) {
      this._toastr.warning('Uploading documents are permitted only to specific users.', 'Restricted')
      return
    }
    this.selectedDocument = undefined
    setTimeout(() => { this.loading = true }, 10)
    await this.getDocumentList()

    const { additionalDocuments } = this
    const _existDoc = additionalDocuments.filter(doc => doc.DocumentID === -1)
    if (_existDoc && _existDoc.length > 0) {
      return
    }
    let _cpy_add_docs_cust: any = {
      DocumentID: -1,
      DocumentTypeID: -1,
      DocumentName: 'ADDITIONAL DOCUMENT',
      DocumentTypeDesc: 'ADDITIONAL DOCUMENT',
      DocumentTypeName: null,
      DocumentStausRemarks: '',
      DocumentUploadDate: '',
      DocumentFileName: null,
      IsDownloadable: false,
      DocumentUploadedFileType: null,
      DocumentLastStatus: '',
      MetaInfoKeysDetail: [] as any,
      RequestID: this.request.RequestID
    }

    try {
      _cpy_add_docs_cust.MetaInfoKeysDetail.forEach(inp => { inp.KeyValue = '' })
    } catch (error) { }

    setTimeout(() => { this.loading = false }, 10)
    this.additionalDocuments.push(_cpy_add_docs_cust)
  }

  async getDocumentList() {
    try {
      this.loading = true
      if (!this.docTypeList || this.docTypeList.length === 0) {
        try {
          await this.getDocTypeList('COMMON', 'SPOT_RATE')
        } catch (error) { }
      }
      this.loading = false
    } catch { }
    setTimeout(() => { this.loading = false }, 10)
  }

  async getDocTypeList(subProcess, key) {
    const _key = encodeURIComponent(base64Encode(JSON.stringify(['INVOICE'])))
    const res = await this.bookingService.getDocListV1(subProcess, _key).toPromise() as any
    if (res.returnId > 0) {
      this.docTypeList = res.returnObject
    }
  }

  async getDocumentDtl(selectedDocument) {
    const { additionalDocuments } = this
    try {
      const res = await this.bookingService.getDocObject(selectedDocument).toPromise() as any
      if (res.returnId > 0) {
        const _newDoc: UserDocument = {
          ...res.returnObject,
          ShowUpload: false,
          HideCancel: true
        }

        this.currentDocObject = _newDoc
        const _arr = [
          ...additionalDocuments,
          _newDoc
        ]
        this.additionalDocuments = _arr.filter(_doc => _doc.DocumentTypeID > -1)
      } else {
        this._toastr.error(res.returnText, res.returnStatus)
      }
    } catch {

    }

  }

  close() {
    this._activeModal.close()
  }

  approvedDoc(data) {
    if (data.DocumentLastStatus.toLowerCase() == 'approved' || data.DocumentLastStatus.toLowerCase() == 're-upload')
      return;
    this.loading = true
    const _toSend = {
      documentStatusID: 0,
      documentStatusCode: '',
      documentStatus: this.approvedStatus[0].StatusName,
      documentStatusRemarks: "",
      documentLastApproverID: this.request.ProviderID,
      documentID: data.DocumentID,
      createdBy: getLoggedUserData().UserID,
      modifiedBy: "",
      approverIDType: "PROVIDER",

      DocumentStatusID: 0,
      DocumentStatusCode: '',
      DocumentStatus: this.approvedStatus[0].StatusName,
      DocumentStatusRemarks: '',
      // DocumentLastApproverID: this.userProfile.ProviderID,
      // DocumentID: data.DocumentID,
      // CreatedBy: (this.isGuestLogin) ? getDefaultHMUser('').loginUserID : this.userProfile.LoginID,,
      ModifiedBy: '',
      ApproverIDType: 'PROVIDER',
      ProviderName: this.request.Provider.ProviderName,
      EmailTo: "", // None
      PhoneTo: "", // None
      UserName: "", // None
      HashMoveBookingNum: this.request.RequestNo,
      UserCountryPhoneCode: "000" // None
    }
    this.bookingService.approvedDocx(_toSend).subscribe((res: any) => {
      this.loading = false
      if (res.returnStatus == "Success") {
        data.DocumentLastStatus = this.approvedStatus[0].StatusName;
        this._toastr.success('Document has been approved', '')
      } else {
        this._toastr.error('There was an error while processing your request. Please try later', 'Failed')
      }
    }, error => {
      this.loading = false
      this._toastr.error('There was an error while processing your request. Please try later', 'Failed')
    })
  }

  reuploadDoc(data) {
    const modalRef = this._modalService.open(ReUploadDocComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'medium-modal',
      backdrop: 'static',
      keyboard: false
    });

    let obj = {
      docTypeID: data.DocumentTypeID,
      docID: data.DocumentID,
      userID: getLoggedUserData().UserID,
      createdBy: getLoggedUserData().UserID,
      bookingData: this.bookingService
    }

    modalRef.componentInstance.documentObj = obj;
    modalRef.result.then((result) => {
      if (result.resType == "Success") {
        data.DocumentLastStatus = result.status;
      }
    });

    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);
  }
}