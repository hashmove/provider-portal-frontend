import { Component, OnInit, Input, ElementRef, ViewChild, Renderer2 } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { NgbActiveModal, NgbDateParserFormatter, NgbDateStruct, NgbInputDatepicker } from "@ng-bootstrap/ng-bootstrap";
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment'
import { SettingService } from '../../../components/pages/user-desk/settings/setting.service';
import { GEN_URL, getLoggedUserData, loading } from '../../../constants/globalFunctions';
import { JsonResponse } from '../../../interfaces/JsonResponse';
import { INews } from '../../../interfaces';

const now = new Date();
const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
  one && two && two.year === one.year && two.month === one.month && two.day === one.day;

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day < two.day : one.month < two.month : one.year < two.year;

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day > two.day : one.month > two.month : one.year > two.year;


@Component({
  selector: 'app-news-dialog',
  templateUrl: './news-dialog.component.html',
  styleUrls: ['./news-dialog.component.scss']
})
export class NewsDialogComponent implements OnInit {

  @Input() newsData: INews;
  newTitle
  newsLink
  public startDate: NgbDateStruct;
  public maxDate: NgbDateStruct;
  public minDate: NgbDateStruct;
  public hoveredDate: NgbDateStruct;
  public fromDate = {
    day: null,
    month: undefined,
    year: undefined
  };
  public toDate = {
    day: undefined,
    month: undefined,
    year: undefined
  };
  @ViewChild("dp") input: NgbInputDatepicker;
  @ViewChild('rangeDp') rangeDp: ElementRef;

  isHovered = date =>
    this.fromDate && !this.toDate && this.hoveredDate && after(date, this.fromDate) && before(date, this.hoveredDate)
  isInside = date => after(date, this.fromDate) && before(date, this.toDate);
  isFrom = date => equals(date, this.fromDate);
  isTo = date => equals(date, this.toDate);

  constructor(
    private location: PlatformLocation,
    private _activeModal: NgbActiveModal,
    private _toast: ToastrService,
    private renderer: Renderer2,
    private _parserFormatter: NgbDateParserFormatter,
    private _settingService: SettingService,
  ) { location.onPopState(() => this.closeModal(null)); }

  ngOnInit() {
    const date = new Date();
    this.minDate = { month: date.getMonth() + 1, day: date.getDate(), year: date.getFullYear() };
    if (this.newsData) {
      const { newsData } = this
      console.log(newsData)
      if (newsData.NewsTitle) {
        this.newTitle = newsData.NewsTitle
      }
      if (newsData.NewsUrl) {
        this.newsLink = newsData.NewsUrl
      }
      if (newsData.StartDateTime) {
        this.fromDate.day = new Date(newsData.StartDateTime).getDate();
        this.fromDate.year = new Date(newsData.StartDateTime).getFullYear();
        this.fromDate.month = new Date(newsData.StartDateTime).getMonth() + 1;
      }
      if (newsData.EndDateTime) {
        this.toDate.day = new Date(newsData.EndDateTime).getDate();
        this.toDate.year = new Date(newsData.EndDateTime).getFullYear();
        this.toDate.month = new Date(newsData.EndDateTime).getMonth() + 1;
      }
      let parsed = "";
      if (this.fromDate && this.fromDate.day) {
        parsed += this._parserFormatter.format(this.fromDate);
      }
      if (this.toDate && this.toDate.day) {
        parsed += " - " + this._parserFormatter.format(this.toDate);
      }

      setTimeout(() => {
        this.rangeDp.nativeElement.value = parsed;
      }, 100);
    }
  }

  onDateSelection(date: NgbDateStruct) {
    let parsed = "";
    const _minDate = new Date(this.minDate.year, this.minDate.month - 1, this.minDate.day);
    const selectedDate = new Date(date.year, date.month - 1, date.day);
    if (!this.fromDate && !this.toDate) {
      if (moment(selectedDate).isSameOrAfter(_minDate)) {
        this.fromDate = date;
      } else {
        this.fromDate = null
        return
      }
    } else if (this.fromDate && !this.toDate && moment(selectedDate).isSameOrAfter(_minDate)) {
      const _fromDate = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
      if (moment(selectedDate).isSameOrAfter(_fromDate)) {
        this.toDate = date;
        this.input.close();
      }
    } else {
      const selectedDate = new Date(date.year, date.month - 1, date.day);
      if (moment(selectedDate).isSameOrAfter(_minDate)) {
        this.toDate = null;
        this.fromDate = date;
      } else {
        this.fromDate = null
        return
      }
    }
    if (this.fromDate) { parsed += this._parserFormatter.format(this.fromDate); }
    if (this.toDate) { parsed += " - " + this._parserFormatter.format(this.toDate); }
    this.renderer.setProperty(this.rangeDp.nativeElement, 'value', parsed);
  }
  closeModal(status) {
    this._activeModal.close(status);
    document.getElementsByTagName('html')[0].style.overflowY = 'auto';

  }

  addNews() {
    if (!this.newTitle) {
      this._toast.warning('Please enter news title')
      return
    }
    if (!this.newsLink) {
      this._toast.warning('Please enter news link')
      return
    }
    if (!GEN_URL.test(this.newsLink)) {
      this._toast.warning('Your new link url is not valid')
      return
    }
    if (!this.fromDate || !this.fromDate.month || !this.toDate || !this.toDate.month) {
      this._toast.warning('Please enter news validity')
      return
    }

    loading(true)
    const toSend = {
      newsID: this.newsData && this.newsData.NewsID ? this.newsData.NewsID : 0,
      providerID: getLoggedUserData().ProviderID,
      newsTitle: this.newTitle,
      newsUrl: this.newsLink,
      startDateTime: (this.fromDate && this.fromDate.month) ? this.fromDate.month + '/' + this.fromDate.day + '/' + this.fromDate.year : null,
      endDateTime: (this.toDate && this.toDate.month) ? this.toDate.month + '/' + this.toDate.day + '/' + this.toDate.year : null,
      // createdBy: getLoggedUserData().UserID,
    }
    this._settingService.addProviderNews(toSend).subscribe((res: JsonResponse) => {
      loading(false)
      if (res.returnId > 0) {
        this._toast.success('Success')
        this.closeModal(true)
      } else {
        this._toast.error(res.returnText, "Failed")
      }
    }, error => {
      loading(false)
      this._toast.error('There was an error while processing your request, please try later', "Failed")
    })
  }
}
