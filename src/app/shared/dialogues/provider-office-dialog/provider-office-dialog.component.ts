import { Component, OnInit, Input } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { ToastrService } from 'ngx-toastr';
import { SettingService } from '../../../components/pages/user-desk/settings/setting.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { getPhoneNumber, loading } from '../../../constants/globalFunctions';
import { JsonResponse } from '../../../interfaces/JsonResponse';
import { CommonService } from '../../../services/common.service';
import { Observable } from 'rxjs';
import { of } from 'rxjs/observable/of';
import { IAddressInfo } from '../../../interfaces';
@Component({
  selector: 'app-provider-office-dialog',
  providers: [],
  templateUrl: './provider-office-dialog.component.html',
  styleUrls: ['./provider-office-dialog.component.scss']
})
export class ProviderOfficeDialogComponent implements OnInit {

  @Input() countryList: any[] = [];
  @Input() userProfile: any;
  @Input() action: any;
  @Input() editOfficeData: any;
  public officeAddressForm: any;
  officeName_error: boolean = false
  officeAddress_error: boolean = false
  officeCity_error: boolean = false
  officePhone_error: boolean = false

  constructor(
    private location: PlatformLocation,
    private _activeModal: NgbActiveModal,
    private _settingService: SettingService,
    private _commonService: CommonService,
    private _toastr: ToastrService
  ) { location.onPopState(() => this.closeModal(null)); }

  ngOnInit() {

    // console.log(this.countryList)
    this.officeAddressForm = new FormGroup({
      officeName: new FormControl(null, [Validators.required, Validators.maxLength(150), Validators.minLength(10)]),
      officeAddress: new FormControl(null, [Validators.required, Validators.maxLength(500), Validators.minLength(10)]),
      officeCity: new FormControl(null, [Validators.required, Validators.maxLength(100), Validators.minLength(1)]),
      officePhone: new FormControl(null, [Validators.required, Validators.pattern(/^(?!(\d)\1+(?:\1+){0}$)\d+(\d+){0}$/), Validators.minLength(7), Validators.maxLength(13)]),
    });
    setTimeout(() => {
      if (this.action && this.action === 'edit') {
        this.onOfficeEdit(this.editOfficeData)
      }
    }, 0);
  }

  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

  addOffice() {
    console.log('...')
    // this.officeAddressForm.markAsDirty()
    // this.officeAddressForm.markAsTouched()
    this.validateAllFormFields(this.officeAddressForm); //{7}
    this.officeErrorMessage(true)
    let _officePhone = ''
    try {
      _officePhone = this.officeAddressForm.value.officePhone
      _officePhone = getPhoneNumber(_officePhone)
      this.officeAddressForm.controls['officePhone'].setValue(_officePhone);
    } catch { }

    if (!this.officeAddressForm.invalid) {
      if (!_officePhone) {
        this._toastr.warning('Phone Number cannot be empty', 'Phone Required')
        return
      }
      if (!this.officePhoneCountryId) {
        this._toastr.warning('Please select phone country code before saving', 'Select Country')
        return
      }
      const { officeName, officeAddress, officeCity, officePhone } = this.officeAddressForm.value

      loading(true)

      const toSend = {
        ProviderOfficeID: this.selectedAddressId,
        ProviderID: this.userProfile.ProviderID,
        OfficeName: officeName,
        AddressLine1: officeAddress,
        AddressLine2: "",
        City: officeCity.title,
        CityID: officeCity.id,
        CountryPhoneCode: this.officePhoneCode,
        PhoneCodeCountryID: this.officePhoneCountryId,
        OfficePhone: officePhone,
        CreatedBy: this.userProfile.UserID
      }
      this._settingService.addEditBusinessAddress(toSend).subscribe((res: JsonResponse) => {
        loading(false)
        if (res.returnId > 0) {
          this._toastr.success('Office address added successfully.')
          this.officeAddressForm.reset();
          this.selectedAddressId = 0
          this.closeModal(true)
        } else {

        }
      }, error => {
        loading(false)
      })
    } else {
    }
  }

  officeCountryFlagImage
  officePhoneCode
  officePhoneCountryId
  officeSelCountryTel

  selectOfficePhone(list) {
    let description = list.desc;
    this.officeCountryFlagImage = list.code;
    this.officePhoneCode = description[0].CountryPhoneCode;
    this.officePhoneCountryId = list.id
    this.officeSelCountryTel = list
  }

  officeErrorMessage($validCheck?: boolean) {
    if (this.officeAddressForm.controls.officeName.status === "INVALID" && ($validCheck || this.officeAddressForm.controls.officeName.touched)) {
      this.officeName_error = true;
    }
    if (this.officeAddressForm.controls.officeAddress.status === "INVALID" && ($validCheck || this.officeAddressForm.controls.officeAddress.touched)) {
      this.officeAddress_error = true;
    }
    if (($validCheck || this.officeAddressForm.controls.officeCity.touched) && (!this.officeAddressForm.controls.officeCity || !this.officeAddressForm.controls.officeCity.id)) {
      this.officeCity_error = true;
    }
    if (this.officeAddressForm.controls.officePhone.status === "INVALID" && ($validCheck || this.officeAddressForm.controls.officePhone.touched)) {
      this.officePhone_error = true;
    }
  }


  resetOfficeInfo() {
    this.officeAddressForm.reset();
    this.selectedAddressId = 0
  }

  selectedAddressId = 0

  onOfficeEdit($office: IAddressInfo) {
    this.officeAddressForm.controls['officeName'].setValue($office.OfficeName);
    this.officeAddressForm.controls['officeAddress'].setValue($office.AddressLine1);
    this.officeAddressForm.controls['officePhone'].setValue($office.OfficePhone);
    let selectedCountry = this.countryList.find(obj => obj.id == $office.PhoneCodeCountryID);
    if (selectedCountry && Object.keys(selectedCountry).length) {
      this.selectOfficePhone(selectedCountry);
    }
    this.selectedAddressId = $office.ProviderOfficeID
    this._commonService.getFilteredCity($office.CityName).subscribe((res: any[]) => {
      let _city = null
      if (res.length > 1) {
        _city = res.filter(_city => _city.id === $office.CityID)[0]
      } else {
        _city = res[0]
      }
      this.officeAddressForm.controls['officeCity'].setValue(_city);
    })
  }

  oneSpaceHandler(event) {
    if (event.target.value) {
      var end = event.target.selectionEnd;
      if (event.keyCode == 32 && (event.target.value[end - 1] == " " || event.target.value[end] == " ")) {
        event.preventDefault();
        return false;
      }
    }
    else if (event.target.selectionEnd == 0 && event.keyCode == 32) {
      return false;
    }
  }

  formatterCty = (x) => {
    this.isCitySearching = false
    try {
      this.selectOfficePhone(this.countryList.filter(_country => _country.code.toLowerCase() === x.imageName.toLowerCase())[0])

    } catch { }

    return x.title;
  };

  isCitySearching: boolean = false
  hasCitySearchFailed: boolean = false
  hasCitySearchSuccess: boolean = false

  search2 = (text$: Observable<string>) =>
    text$
      .debounceTime(300) //debounce time
      .distinctUntilChanged()
      .do(() => { this.isCitySearching = true; this.hasCitySearchFailed = false; this.hasCitySearchSuccess = false; }) // do any action while the user is typing
      .mergeMap(term => {
        let some = of([]); //  Initialize the object to return
        try {
          if (term && term.length >= 3) { //search only if item are more than three
            some = this._commonService.getFilteredCity(term)
              .do((res) => { this.isCitySearching = false; this.hasCitySearchSuccess = true; return res; })
              .catch(() => { this.isCitySearching = false; this.hasCitySearchFailed = true; return []; })
          } else { this.isCitySearching = false; some = of([]); }
        } catch (error) {
          this.isCitySearching = false
        }
        return some;
      })
      .do((res) => { this.isCitySearching = false; return res; })
      .catch(() => { this.isCitySearching = false; return of([]); }); // final server list

  closeModal(status) {
    this._activeModal.close(status);
    document.getElementsByTagName('html')[0].style.overflowY = 'auto';
  }

  numberValid(evt) {
    let charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }

  selectCity(_city) {
    this.officeAddressForm.controls['officeCity'].setValue(_city)
  }
}
