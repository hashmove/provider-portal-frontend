import { Component, OnInit, Input } from '@angular/core'
import { NgbActiveModal, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap'
import { ViewBookingService } from '../../../components/pages/user-desk/view-booking/view-booking.service'
import { ToastrService } from 'ngx-toastr'
import { JsonResponse } from '../../../interfaces/JsonResponse'
import { getLoggedUserData } from '../../../constants/globalFunctions'
import { FormGroup, FormControl, Validators } from '@angular/forms'
import * as moment from 'moment'
import { NgbDateFRParserFormatter } from '../../../constants/ngb-date-parser-formatter'

@Component({
  selector: 'app-add-bl',
  templateUrl: './add-bl.component.html',
  styleUrls: ['./add-bl.component.scss'],
  providers: [{ provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter }]
})
export class AddBlComponent implements OnInit {
  @Input() data: any
  @Input() type: string
  @Input() lclContainerNo: any
  blText: string = 'B/L'
  // BLNo
  loading = false
  minDate = null

  blForm: FormGroup
  constructor(
    public _activeModal: NgbActiveModal,
    private _viewBookingService: ViewBookingService,
    private _toastr: ToastrService) { }

  ngOnInit() {
    this.blText = this.data.ContainerLoad === 'LCL' ? 'House B/L' : 'B/L'
    const _date = new Date(this.data.PortCutOffLcl)
    this.minDate = { month: _date.getMonth() + 1, day: _date.getDate(), year: _date.getFullYear() }
    this.blForm = new FormGroup({
      blNo: new FormControl(null, [Validators.minLength(9), Validators.maxLength(16)]),
      blDate: new FormControl(null, []),
    })

    const _blValue = (this.type === 'BL_NO') ? this.data.BLNo : this.lclContainerNo
    setTimeout(() => {
      this.blForm.controls['blNo'].setValue(_blValue)
    }, 0)
    try {
      if (this.data.BLDate) {
        const _blDate = new Date(this.data.BLDate)
        const _blFDate = { month: _blDate.getMonth() + 1, day: _blDate.getDate(), year: _blDate.getFullYear() }
        this.blForm.controls['blDate'].setValue(_blFDate)
      }
    } catch { }
  }

  close(data) {
    this._activeModal.close(data)
    document.getElementsByTagName('html')[0].style.overflowY = 'auto'
  }

  saveBL() {
    if (this.blForm.invalid) {
      this._toastr.warning('Input is invalid, please try again', 'Warning')
      return
    }
    const obj = {
      bookingID: this.data.BookingID,
      blNo: this.blForm.value.blNo,
      blDate: this.getDatefromObj(this.blForm.value.blDate),
      shippingModeCode: this.data.ShippingModeCode,
      comInvNo: this.data.ComInvNo,
      cusClearNo: this.data.CusClearNo,
      gDeclareNo: this.data.GDeclareNo,
      expFormNo: this.data.ExpFormNoe
    }
    if (!obj.blDate && obj.blNo) {
      this._toastr.warning(`Please enter ${this.blText} date`, `${this.blText} Date Missing`)
      return
    }
    if (obj.blDate && !obj.blNo) {
      this._toastr.warning(`Please enter ${this.blText} number`, `${this.blText} Number Missing`)
      return
    }

    this.loading = true
    this._viewBookingService.saveBLNumber(obj, getLoggedUserData().UserID).subscribe((res: JsonResponse) => {
      if (res.returnId > 0) {
        this._toastr.success('B/L number added successfully', 'Success')
        this.loading = false
        this.close(obj)
      } else {
        if (res.returnObject && res.returnObject.length > 0) {
          this.loading = false
          let newString = ''
          try {
            newString += res.returnText + ' <br>'
            const { returnObject } = res
            returnObject.forEach(str => {
              newString += str + ' <br>'
            })
          } catch (error) {
            newString = res.returnText
          }
          console.log(newString)

          this._toastr.warning(newString, '', { enableHtml: true })
        } else {
          this._toastr.warning(res.returnText)
        }
        this.loading = false
      }
    }, (err: any) => {
      this.loading = false
      this._toastr.error('There was a problem while updating, please try again later')
      console.log(err)
    })
  }

  saveAction() {
    if (this.type === 'BL_NO') {
      this.saveBL()
    } else {
      this.saveLclContainer()
    }
  }

  saveLclContainer() {
    const _toSend = {
      bookingID: this.data.BookingID,
      shippingMode: this.data.ShippingModeCode,
      parameterType: 'LCL_CONTAINER_NO',
      parameterValue: JSON.stringify([{ ContainerNo: this.blForm.value.blNo }]),
      providerID: this.data.ProviderID,
      userID: this.data.UserID,
      isUpdateByProvider: true,
    }
    this._viewBookingService.updateBookingAdditionalParameters(getLoggedUserData().UserID, _toSend).subscribe((res: JsonResponse) => {
      this.loading = false
      if (res.returnId > 0) {
        this._toastr.success('Container number added successfully', 'Success')
        this.close(this.blForm.value.blNo)
      } else {
        this._toastr.error(res.returnText, res.returnStatus)
      }
    }, (error: any) => {
      this.loading = false
      this._toastr.error(error.error.ParameterValue[0], 'Failed')
    })
  }

  closeFix(event, datePicker) {
    if (event.target.offsetParent == null)
      datePicker.close()
    else if (event.target.offsetParent.nodeName !== 'NGB-DATEPICKER')
      datePicker.close()
  }

  removeFormData = (_ctrl) => this.blForm.controls[_ctrl].setValue(null)

  getDatefromObj(dateObject) {
    let toSend = null
    try {
      toSend = moment(new Date(dateObject.year, dateObject.month - 1, dateObject.day)).format('YYYY-MM-DD')
    } catch (error) { }
    return toSend
  }
}
