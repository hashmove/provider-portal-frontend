import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViaPortSeaComponent } from './via-port.component';

describe('ViaPortSeaComponent', () => {
  let component: ViaPortSeaComponent;
  let fixture: ComponentFixture<ViaPortSeaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ViaPortSeaComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViaPortSeaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
