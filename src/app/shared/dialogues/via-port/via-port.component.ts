import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-via-port-sea',
  templateUrl: './via-port.component.html',
  styleUrls: ['./via-port.component.scss']
})
export class ViaPortSeaComponent implements OnInit {
  @Input() portData: any
  @Input() portConnectData: any

  constructor(
    public _activeModal: NgbActiveModal,
  ) { }

  ngOnInit(): void {

  }
  getPortData = (_code: string) => this.portConnectData.filter(_port => _port.PortCode === _code)[0]



  close() {
    this._activeModal.close()
  }

}