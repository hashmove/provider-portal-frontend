import { Component, OnInit, Input, AfterViewInit, ViewChild, ElementRef } from '@angular/core'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { DashboardService } from '../../../components/pages/user-desk/dashboard/dashboard.service'
import { encryptBookingID, getImagePath, getLoggedUserData, ImageRequiredSize, ImageSource, isArrayValid, isJSON, isMobile } from '../../../constants/globalFunctions'
import { baseExternalAssets } from '../../../constants/base.url'
import { JsonResponse } from '../../../interfaces'
import { ToastrService } from 'ngx-toastr'
import { ScrollbarComponent } from 'ngx-scrollbar'
import { IRateRequest } from '../request-add-rate-dialog/request-add-rate.interface'
@Component({
  selector: 'app-chat-request',
  templateUrl: './chat-request.component.html',
  styleUrls: ['./chat-request.component.scss']
})
export class ChatRequestComponent implements OnInit, AfterViewInit {
  @Input() request: IRateRequest
  @Input() isEnabled = true
  closeResult: string
  @ViewChild(ScrollbarComponent) scrollRef: ScrollbarComponent
  @ViewChild('scrollMe') scrollMe: ElementRef
  @ViewChild('messageBox') messageBox: ElementRef
  public currencyList: any[] = []
  public selectedCurrency: any = {}
  comments: string
  loading: boolean = false
  chatList: IChat[] = []


  disableScrollDown = false
  userID: number = null
  userInfoList: IUserChatInfo[] = []
  defImage = baseExternalAssets + 'images/hashmove/original/icons_user.svg'
  autComp = 'xas' + new Date().getTime()

  hasChatInitiated = false
  placeholderText: string = 'Type a message'
  disableChat: boolean = false
  frameLoad = true
  modalHeight = '500px'
  isMobile = isMobile()
  providerName = ''

  constructor(
    public _activeModal: NgbActiveModal,
    private _dashboardService: DashboardService,
    private _toastr: ToastrService
  ) { }


  ngOnInit() {
    this.userID = getLoggedUserData().UserID
    this.providerName = getLoggedUserData().CompanyName
    this.getChatList(true)
    this.modalHeight = isMobile ? '350px' : '500px'
    // if (this.request.SpecialRequestStatusBL.toLowerCase() === 'pending' && !this.hasChatInitiated) {
    //   this.initiateChat()
    // }
    try {
      const { RequestStatusBL } = this.request
      if (RequestStatusBL === 'CANCELLED' || RequestStatusBL === 'COMPLETED') {
        this.placeholderText = 'Request has been ' + this.request.RequestStatus
        this.disableChat = true
      }
    } catch { }
    try {
      if (this.isEnabled) {
        this.disableChat = false
      } else {
        this.placeholderText = 'Chat has been disabled'
        this.disableChat = true
      }
    } catch {

    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.messageBox.nativeElement.select()
      this.messageBox.nativeElement.select()
    }, 0)
  }

  initiateChat() {
    const { request } = this
    const _toSend = {
      requestID: request.RequestID,
      currencyID: this.selectedCurrency.id ? this.selectedCurrency.id : request.CurrencyID,
      actualIndividualPrice: 0,
      baseIndividualPrice: 0,
      specialRequestComments: 'Messaging Started',
      userID: getLoggedUserData().UserID,
      requestStatus: 'PRICE_SENT',
      IsOnlyComments: true,
      exchangeRate: 0,
      currExcgRateListID: null,
      isRouteDetailUpdate: false,
      carrierID: null,
      pickupPort: {
        id: null,
        code: request.Origin.PolCode,
        title: request.Origin.PolName,
        imageName: request.Origin.PolCountryCode
      },
      deliveryPort: {
        id: null,
        code: request.Destination.PodCode,
        title: request.Destination.PodName,
        imageName: request.Destination.PodCountryCode
      },
      perUnitCurrencyID: request.CurrencyID,
      perUnitRate: null,
      perUnitBaseRate: 0,
      perUnitExchangeRate: null,
      perUnitRateType: null
    }
    this._dashboardService.updateSpecialPrice(request.RequestID, getLoggedUserData().UserID, _toSend).subscribe((res: JsonResponse) => {
      if (res.returnId > 0) {
        this.hasChatInitiated = true
      }
    })
  }

  closeChat(_state) {
    this._activeModal.close(this.hasChatInitiated)
  }

  ngAfterViewChecked() {
    // this.scrollToBottom(false)
  }

  onScroll() {
    let element = this.scrollMe.nativeElement
    let atBottom = element.scrollHeight - element.scrollTop === element.clientHeight
    if (this.disableScrollDown && atBottom) {
      this.disableScrollDown = false
    } else {
      this.disableScrollDown = true
    }
  }


  private scrollToBottom(_disableScrollDown): void {
    if (_disableScrollDown) {
      return
    }
    try {
      this.scrollMe.nativeElement.scrollTop = this.scrollMe.nativeElement.scrollHeight
    } catch (err) { }
  }

  getChatList(getUserData?: boolean) {
    const _reqKey = encryptBookingID(this.request.RequestID, this.getCurrenProviderID(), this.request.ShippingModeCode)
    this._dashboardService.getChatListV2(_reqKey, 'PROVIDER', 'OFFLINE_CHAT', (this.request.ResponseTagID) ? this.request.ResponseTagID : 0).subscribe((res: JsonResponse) => {
      this.autComp = 'xas' + new Date().getTime()
      if (res.returnId > 0) {
        const { parameterValue }: IChatResponse = res.returnObject
        const _chatData: IChatBody = JSON.parse(parameterValue)
        const _chatList = _chatData.messages
        this.readMessage()
        if (getUserData) {
          const _usersList: number[] = Array.from(new Set(_chatList.map(_chat => _chat.userID)))
          if (!isArrayValid(_usersList, 0) || !_usersList.includes(getLoggedUserData().UserID)) {
            _usersList.push(getLoggedUserData().UserID)
          }
          this._dashboardService.getUserBasicDetail(_usersList).subscribe((res: JsonResponse) => {
            if (res.returnId > 0) {
              this.userInfoList = res.returnObject
              setTimeout(() => {
                this.setChatList(_chatList)
              }, 10)
            } else {
              this.setChatList(_chatList)
            }
          }, error => {
            this.setChatList(_chatList)
          })
        } else {
          this.setChatList(_chatList)
        }
        // this.chatContainer.nativeElement.scrollTop = this.chatContainer.nativeElement.scrollHeight
      } else {
        this._dashboardService.getUserBasicDetail([getLoggedUserData().UserID]).subscribe((res: JsonResponse) => {
          if (res.returnId > 0) {
            this.userInfoList = res.returnObject
          }
        })
        this.frameLoad = false
      }
    }, error => {
      this.frameLoad = false
    })
  }

  readMessage() {
    try {
      const _toSend = {
        requestID: this.request.RequestID,
        messageBy: 'PROVIDER',
        msgStatus: 'READ',
        providerID: this.getCurrenProviderID(),
        responseTagID: (this.request.ResponseTagID) ? this.request.ResponseTagID : 0,
        msgID: null
      }
      this._dashboardService.updateRequestChatStatusV2(getLoggedUserData().UserID, _toSend).subscribe(res => {
        // console.log(res)
      })
    } catch { }
  }

  setChatList(_chatList) {
    this.chatList = _chatList
    this.frameLoad = false
    setTimeout(() => {
      this.scrollToBottom(false)
    }, 10)
  }

  getImage($userID: number) {
    try {
      const { userInfoList } = this
      const _image = userInfoList.filter(_user => _user.userID === $userID)[0].userImage
      return getImagePath(ImageSource.FROM_SERVER, _image, ImageRequiredSize.original)
    } catch (error) {
      console.log(error)
      return this.defImage
    }
  }

  getUserName($userID: number) {
    try {
      const { userInfoList } = this
      return userInfoList.filter(_user => _user.userID === $userID)[0].userName
    } catch (error) {
      console.log(error)
      return 'Customer'
    }
  }

  sendChat() {
    if (!this.messageHasText(this.comments)) {
      return
    }
    const _commentsJSON: IChat = {
      msgDateTime: new Date().toISOString(),
      userID: getLoggedUserData().UserID,
      msgText: this.comments,
      uploadedDocID: null,
      messageBy: 'PROVIDER',
      msgID: null,
      msgStatus: null,
    }

    const _toSend = {
      requestID: this.request.RequestID,
      shippingMode: this.request.ShippingModeCode,
      parameterType: 'OFFLINE_CHAT',
      parameterValue: JSON.stringify(_commentsJSON),
      providerID: this.getCurrenProviderID(),
      userID: this.request.Customer.UserID,
      responseTagID: (this.request.ResponseTagID) ? this.request.ResponseTagID : 0,
      isUpdateByProvider: true,
    }
    this.loading = true
    this._dashboardService.sendChatV2(getLoggedUserData().UserID, _toSend).subscribe((res: JsonResponse) => {
      this.loading = false
      if (res.returnId > 0) {
        // if (this.request.RequestStatusBL.toLowerCase() === 'pending' && !this.hasChatInitiated && this.request.RequestSource === 'RATE_NOT_FOUND') {
        //   // this.initiateChat()
        // }
        if (res.returnCode === '2') {
          this._toastr.info(res.returnText, res.returnStatus)
        } else {
        }
        this.comments = null
        this.getChatList(true)
      } else {
        this._toastr.error(res.returnText, res.returnStatus)
      }
    }, error => {
      this.loading = false
      this._toastr.error('There was an error while processing your request, please try again later', 'Failed')
    })
  }

  handleInput(e) {
    if (e.key == 'Enter') {
      if (e.shiftKey) e.target.style.height = e.target.offsetHeight + 20 + 'px'
      else e.target.form.submit()
    }
  }

  onKeyPress(e) {
    if (e.keyCode === 13 && !e.altKey && this.messageHasText(this.comments)) {
      e.preventDefault()
      e.stopPropagation()
      this.sendChat()
      return
    }
    if (e.keyCode === 13 && e.altKey) {
      console.log('yolo')
      this.comments = (this.comments ? this.comments : '') + '\n'
      const _chatBox = document.getElementById('chatBox') as any
      _chatBox.focus()
      _chatBox.setSelectionRange(_chatBox.value.length, _chatBox.value.length)
      _chatBox.scrollTop = _chatBox.scrollHeight
      try {
        this.messageBox.nativeElement.scrollTop = this.messageBox.nativeElement.scrollHeight
      } catch (err) { }
    }
  }

  messageHasText(_message: string) {
    if (_message) {
      const _str = _message.replace(/\n/g, '').replace(/ /g, '')
      return _str && _str.length && _str.length > 0 ? true : false
    } else {
      return false
    }
  }

  renderMessage(_message: string) {
    try {
      return _message.replace(/\n/g, '<br>')
    } catch {
      return _message
    }
  }

  getCurrenProviderID(): number {
    try {
      return this.request.ProviderID ? this.request.ProviderID : this.request.Provider.ProviderID ? this.request.Provider.ProviderID : 0
    } catch {
      return 0
    }
  }
}
export interface IChatResponse {
  requestID: number
  parameterType: string
  parameterValue: string
}


interface IChatBody {
  providerCount: number
  customerCount: number
  messages: IChat[]
}
export interface IChat {
  msgDateTime: string
  userID: number
  msgText: string
  uploadedDocID?: any
  messageBy: string
  msgStatus: string
  msgID: number
}
interface IUserChatInfo {
  userID: number
  userName: string
  contactNumber: string
  email: string
  phoneCodeCountryID: number
  countryCode: string
  countryPhoneCode: string
  companyID: number
  sourceID: number
  sourceType: string
  userImage: string
}