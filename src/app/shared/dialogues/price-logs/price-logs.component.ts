import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DashboardService } from '../../../components/pages/user-desk/dashboard/dashboard.service';
import { encryptBookingID, getImagePath, ImageRequiredSize, ImageSource, isJSON } from '../../../constants/globalFunctions';
import { baseExternalAssets } from '../../../constants/base.url';

@Component({
  selector: 'app-price-logs',
  templateUrl: './price-logs.component.html',
  styleUrls: ['./price-logs.component.scss']
})
export class PriceLogsComponent implements OnInit {
  @Input() data: any;
  @Input() isRV2 = false
  closeResult: string;
  @Input() logs: any[] = []
  public currencyList: any[] = []
  public selectedCurrency: any = {}
  constructor(public _activeModal: NgbActiveModal, private _dashboardService: DashboardService) { }


  ngOnInit() {
    if (this.isRV2)
      this.getRequestLogsV2()
    else
      this.getRequestLogsV1()
  }

  getRequestLogsV1() {
    const id = encryptBookingID(this.data.booking.BookingID, this.data.booking.ProviderID, this.data.booking.ShippingModeCode)
    this._dashboardService.getBookingSpecialLogs(id, 'PROVIDER').subscribe((res: any) => {
      this.logs = res.returnObject
    }, (err: any) => {
      console.log(err)
    })
  }

  getRequestLogsV2() {
    const { request } = this.data
    const _reqKey = encryptBookingID(request.RequestID, request.ProviderID, request.ShippingModeCode)
    this._dashboardService.getRequestPriceLog(_reqKey, request.ResponseTagID ? request.ResponseTagID : 0, 'PROVIDER').subscribe((res: any) => {
      this.logs = res.returnObject
    }, (err: any) => {
      console.log(err)
    })
  }

  getPortData = (log, type) => {
    let code = '', flag = '', title = ''
    try {
      const _port = type === 'origin' ? JSON.parse(log.PickupPortDetail) : JSON.parse(log.DeliveryPortDetail)
      code = _port.Code
      flag = _port.ImageName.toLowerCase()
      title = _port.Title
    } catch { }
    return { code, flag, title }
  }

  getCustomerImage($image: string) {
    if (isJSON($image)) {
      const providerImage = JSON.parse($image)
      return baseExternalAssets + '/' + providerImage[0].DocumentFile
    } else {
      return getImagePath(ImageSource.FROM_SERVER, $image, ImageRequiredSize.original)
    }
  }
}
