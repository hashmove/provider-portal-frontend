export interface IAppRoles {
  roleID: number
  roleCode: string
  roleName: string
  sourceID?: any
  sourceType?: any
  sortingOrder: number
  isDelete: boolean
  isActive: boolean
  createdBy: string
  createdDateTime: Date
  modifiedBy?: any
  modifiedDateTime?: any
}



export interface IRegUserData {
  userID: number;
  userCode: string;
  loginID: string;
  primaryEmail: string;
  secondaryEmail: string;
  firstName: string;
  middleName: string;
  lastName: string;
  primaryPhone: string;
  secondaryPhone: string;
  countryID: number;
  cityID: number;
  companyID: number;
  roleID: number;
  modifiedBy: string;
  countryPhoneCode: string;
  phoneCodeCountryID: number;
  companyName?: any;
  sourceID: number;
  sourceType: string;
}

export interface ISelectedCity {
  title?: String,
  imageName?: String,
  desc?: any;
  code?: string
}