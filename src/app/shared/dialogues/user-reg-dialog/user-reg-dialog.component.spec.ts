import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserRegDialogComponent } from './user-reg-dialog.component';

describe('UserRegDialogComponent', () => {
  let component: UserRegDialogComponent;
  let fixture: ComponentFixture<UserRegDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserRegDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserRegDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
