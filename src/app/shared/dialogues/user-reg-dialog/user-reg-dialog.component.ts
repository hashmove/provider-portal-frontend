import { Component, OnInit, ViewEncapsulation, Input, Renderer2, ElementRef } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { LoginDialogComponent } from '../login-dialog/login-dialog.component';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { EMAIL_REGEX, CustomValidator, ValidateEmail, getLoggedUserData, validateName, getAccessRights } from '../../../constants/globalFunctions';
import { Observable } from 'rxjs';
import { CountryDropdown } from '../../country-select/country-select.component';
import { JsonResponse } from '../../../interfaces/JsonResponse';
import { isUserLogin } from '../../../services/jwt.injectable';
import { of } from 'rxjs/observable/of';
import { CommonService } from '../../../services/common.service';
import { SharedService } from '../../../services/shared.service';
import { UserCreationService } from '../../../components/pages/user-creation/user-creation.service';
import { IAppRoles, IRegUserData, ISelectedCity } from './user-reg-dialog.interface';
import { ICarrierFilter } from '../../../interfaces';


@Component({
  selector: 'app-user-reg-dialog',
  templateUrl: './user-reg-dialog.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./user-reg-dialog.component.scss']
})



export class UserRegDialogComponent implements OnInit {

  @Input() shareUserObject: any;
  @Input() mode = 'add'
  @Input() isPartner: boolean = false;
  closeResult: string;
  currentJustify = 'justified';
  public creationCorporateType;
  public countryList;
  public toggle: boolean = false;
  public country;
  public cityList;
  public cityValidation;
  public cityValid;
  public companyList;
  public hearList;
  public requiredFields = "This field is required!";
  public selectedImage;
  public select;
  public countryPCode;
  public phoneCountryId;
  public colorEye;
  public term: boolean = true;
  public firstNameError;
  public lastNameError;
  public contactError;
  public cityError;
  public emailError;
  public companyError;
  public passwordError;
  public corporateUser: boolean = false;
  public company: any;

  public city: any = {
    title: '',
    // imageName: this.getUserLocation(),
    imageName: 'US',
    desc: '',
    code: ''
  }
  isValidFormSubmitted = null;
  userForm;
  public hideLogin: boolean = false;
  public activeIdString: string = "tab-corporate"

  public isSubmitting: boolean = false
  // userRoles: IAppRoles[] = []
  showClose = true
  userRoles: any[] = []
  regUserData: IRegUserData = null
  placeholderActive = false
  isCompanyDisabled = true

  constructor(
    private modalService: NgbModal,
    private _renderer: Renderer2,
    private el: ElementRef,
    private dropdownservice: CommonService,
    private authService: UserCreationService,
    private _toast: ToastrService,
    private activeModal: NgbActiveModal,
    private _sharedService: SharedService,
    private _location: PlatformLocation
  ) {
    _location.onPopState(() => this.closeModal(false));
  }
  async ngOnInit() {

    this.isSubmitting = false
    this.activeIdString = 'tab-corporate'
    this.Account('corporate')
    this.userForm = new FormGroup({
      firstName: new FormControl(null, [Validators.required, Validators.pattern(/^[a-zA-Z\s]*$/), Validators.minLength(2), Validators.maxLength(50)]),
      lastName: new FormControl(null, [Validators.required, Validators.pattern(/^[a-zA-Z\s]*$/), Validators.minLength(2), Validators.maxLength(50)]),
      city: new FormControl(null, [Validators.required, Validators.minLength(3), Validators.maxLength(25)]),
      email: new FormControl(null, [
        Validators.required,
        Validators.pattern(EMAIL_REGEX),
        Validators.maxLength(320)
      ]),
      phone: new FormControl(null, [Validators.required, Validators.pattern(/^(?!(\d)\1+(?:\1+){0}$)\d+(\d+){0}$/), Validators.minLength(7), Validators.maxLength(12)]),
      company: new FormControl('', [CustomValidator.bind(this)]),
      howHearAboutUs: new FormControl(''),
      // termCondition: new FormControl('', [Validators.required]),
      password: new FormControl('ff@123456', [Validators.required, Validators.minLength(6), Validators.maxLength(15)]),
      userRole: new FormControl('', [Validators.required])
    });

    if (this.mode === 'edit') {
      this.placeholderActive = true
      try {
        const _userData = await this.authService.getPortalUserData(this.shareUserObject.userID).toPromise() as any
        if (_userData && typeof _userData === 'object') {
          this.regUserData = _userData as any
          console.log(this.regUserData)
          this.userForm.controls['firstName'].setValue(this.regUserData.firstName)
          this.userForm.controls['lastName'].setValue(this.regUserData.lastName)
          this.userForm.controls['email'].setValue(this.regUserData.primaryEmail)
          this.userForm.controls['phone'].setValue(this.regUserData.primaryPhone)
          const [_city] = await this.dropdownservice.getCityDtl(this.regUserData.cityID).toPromise() as ICarrierFilter[]
          this.userForm.controls['city'].setValue(_city)
          this.city = _city
          this.phoneCountryId = this.regUserData.phoneCodeCountryID
          this.countryPCode = this.regUserData.countryPhoneCode
          this.userForm.controls['userRole'].setValue(this.regUserData.roleID)
        }
      } catch (error) {
        console.log(error)
      }
      this.placeholderActive = false
    }

    try {
      this.authService.getUserRoles(getAccessRights().RoleID).subscribe((res: any) => {
        this.userRoles = res
      })
    } catch { }



    this.dropdownservice.getCountry().subscribe((res: Observable<CountryDropdown>) => {
      let List = res;
      List.map((obj) => {
        obj.desc = JSON.parse(obj.desc);
      })
      this.countryList = List;
      if (this.mode === 'edit') {
        this.selCountry = this.countryList.find(_country => _country.id === this.regUserData.phoneCodeCountryID)
      }
    })

    this.dropdownservice.getCompany().subscribe((res: Observable<CountryDropdown>) => {
      this.companyList = res;
      if (this.shareUserObject && this.shareUserObject.companyID && !this.isPartner) {
        this.company = this.companyList.find(list => list.id === this.shareUserObject.companyID);
        let companyInput = this.el.nativeElement.querySelector('input.company');
        this._renderer.setAttribute(companyInput, 'readonly', 'true');
      }

    }, (err: HttpErrorResponse) => {
    })

    this.dropdownservice.howhear().subscribe((res: any) => {
      this.hearList = res;
    }, (err: HttpErrorResponse) => {
    })

    // if (this.shareUserObject && this.shareUserObject.CompanyName) {
    this.Account("corporate");
    this.creationCorporateType = true;
    let tab = this.el.nativeElement.querySelector('ul[role="tablist"]');
    this._renderer.setStyle(tab, 'display', 'none');
    // }

  }


  getUserLocation() {
    try {
      let countryCode = this._sharedService.getMapLocation().countryCode;
      this.city = this.findCityByCountryCode(countryCode);
      this.flag(this.city, 'city');
    } catch (error) {

    }
  }

  findCityByCountryCode(countryCode: string): ISelectedCity {

    let searchedCity: ISelectedCity;
    let cityObj = this.cityList.find((obj) => {

      if (JSON.parse(obj.desc)[0].CountryCode.toLowerCase() == countryCode.toLowerCase()) {
        searchedCity = {
          desc: obj.desc,
          title: obj.title,
          code: obj.imageName,
          imageName: obj.imageName,
        }
        return obj;
      }
    });
    return searchedCity;
  }

  errorMessages() {
    if (this.userForm.controls.firstName.status == "INVALID" && this.userForm.controls.firstName.touched) {
      this.firstNameError = true;
    }
    if (this.userForm.controls.lastName.status == "INVALID" && this.userForm.controls.lastName.touched) {
      this.lastNameError = true;
    }
    if (this.userForm.controls.phone.status == "INVALID" && this.userForm.controls.phone.touched) {
      this.contactError = true;
    }
    if (this.userForm.controls.email.status == "INVALID" && this.userForm.controls.email.touched) {
      this.emailError = true;
    }
    if (this.userForm.controls.password.status == "INVALID" && this.userForm.controls.password.touched) {
      this.passwordError = true;
    }
    if (this.userForm.controls.company.status == "INVALID" && this.userForm.controls.company.touched) {
      this.companyError = true;
    }


  }


  login() {
    this.activeModal.close();

    this.modalService.open(LoginDialogComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'login-modal',
      backdrop: 'static',
      keyboard: false
    });
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);

  }
  closeModal(status) {
    this.activeModal.close(status);
    document.getElementsByTagName('html')[0].style.overflowY = 'auto';

  }

  selCountry

  flag(list, type) {
    this.cityValidation = false;
    this.cityValid = false;
    if (typeof (list) == 'object' && list.title && type == 'city') {
      this.selectedImage = list.imageName;
      let description = JSON.parse(list.desc);
      if (this.countryList && this.countryList.length) {
        const objCountry = this.countryList.find(item => item.id === description[0].CountryID);
        this.selCountry = objCountry
        this.select = objCountry
        this.countryPCode = this.select.desc[0].CountryPhoneCode;
        this.phoneCountryId = this.select.id;
      }
    }
    else if (typeof (list) == 'object' && list.title && type == 'country') {
      this.selectedImage = list.code;
      let description = list.desc;
      this.countryPCode = description[0].CountryPhoneCode;
      this.phoneCountryId = list.id
      this.selCountry = list
    }
  }

  spaceHandler(event) {
    if (event.target.value) {
      const end = event.target.selectionEnd;
      if (event.keyCode == 32 && (event.target.value[end - 1] == " " || event.target.value[end] == " ")) {
        event.preventDefault();
        return false;
      }
    }
  }
  emailPassSpaceHandler(event) {
    if (event.keyCode == 32) {
      event.preventDefault();
      return false;
    }
  }
  checkCity(type) {

    let cityName = this.userForm.value.city;
    if (type == 'focusOut') {
      if (typeof (cityName) == "string" && cityName.length > 2 && cityName.length < 26) {
        this.cityValidation = "City should be selected from the dropdown";
        this.cityValid = false;
      }
      if (this.userForm.controls.city.status == "INVALID") {
        this.cityError = true;
      }
    }
    else if (type == 'focus' && this.cityValidation && typeof (cityName) == "string") {

      this.cityValidation = false;
      this.cityValid = true;
    }
    setTimeout(() => {
      this.isCitySearching = false
    }, 0);
  }
  Account(accountType) {
    if (accountType == "corporate") {
      this.corporateUser = true;
    }
    else if (accountType == "individual") {
      this.corporateUser = false;
      this.userForm.controls.company.reset();
    }
  }

  async userRegistration(userForm) {
    let userItem = getLoggedUserData();
    this.isSubmitting = true
    let valid: boolean = ValidateEmail(userForm.email);

    if (this.userForm.invalid) {
      this.isSubmitting = false
      return;
    }
    else if (!valid) {
      this.isSubmitting = false
      this._toast.warning('Invalid email entered.', 'Failed')
      return
    }

    if ((userForm.company && typeof userForm.company === 'object' && !userItem) || (userForm.company && typeof userForm.company === 'object' && userItem && userItem.IsLogedOut)) {
      try {
        const res: JsonResponse = await this.authService.getCompanyAdmin(userForm.company.id).toPromise() as any
        if (res.returnId > 0) {
          console.log(res);
          // res.returnObject
          const { adminName, adminEmail, adminContact } = res.returnObject
          this._toast.warning(`Please contact ${userForm.company.title}’s admin - ${adminName} at ${adminEmail} to create your account within ${userForm.company.title}`)
        } else {
          this._toast.warning('Please contact ' + userForm.company.title + ' admin to create your account', 'Info')
        }
      } catch (error) {
        this._toast.warning('Please contact ' + userForm.company.title + ' admin to create your account', 'Info')
      }
      this.isSubmitting = false
      return false;
    }

    const { firstName, lastName } = userForm
    const fName = validateName(firstName)
    const lName = validateName(lastName)
    let country_id = JSON.parse(userForm.city.desc);
    let _sourceId = userItem.ProviderID
    let _sourceType = 'PROVIDER_SIGNUP'
    let obj
    if (this.mode === 'edit') {
      obj = {
        userID: this.mode === 'edit' ? this.regUserData.userID : -1,
        secondaryEmail: '',
        firstName: fName,
        middleName: '',
        lastName: lName,
        primaryPhone: userForm.phone,
        secondaryPhone: '',
        countryID: Number(country_id[0].CountryID),
        cityID: userForm.city.id,
        roleID: this.getNum(userForm.userRole),
        modifiedBy: (isUserLogin()) ? getLoggedUserData().UserID : '',
        countryPhoneCode: this.countryPCode,
        phoneCodeCountryID: this.phoneCountryId,
        secondaryCountryPhoneCode: '',
        secondaryPhoneCodeCountryID: 0
      }
    } else {
      obj = {
        userID: -1,
        userCode: "",
        loginID: userForm.email,
        password: userForm.password,
        primaryEmail: userForm.email,
        secondaryEmail: "",
        firstName: fName,
        middleName: "",
        lastName: lName,
        primaryPhone: userForm.phone,
        secondaryPhone: "",
        CountryPhoneCode: this.countryPCode,
        PhoneCodeCountryID: this.phoneCountryId,
        companyID: (this.corporateUser && typeof (userForm.company) == "object") ? userForm.company.id : null,
        countryID: Number(country_id[0].CountryID),
        cityID: userForm.city.id,
        howHearAboutUs: "OTHR",
        howHearOthers: "",
        roleID: this.getNum(userForm.userRole),
        isDelete: false,
        isActive: true,
        createdBy: (isUserLogin()) ? getLoggedUserData().UserID : '',
        modifiedBy: "",
        userImage: "",
        companyTradeLicenseNum: "",
        isCorporateUser: this.corporateUser,
        isAgreeTermsCond: this.term,
        customerID: null,
        portalName: 'PROVIDER',
        sourceID: _sourceId,
        sourceType: _sourceType
      };
    }

    if (obj && obj.isCorporateUser) {
      obj.companyName = (typeof (userForm.company) == "object") ? userForm.company.title : userForm.company;
    }
    else if (obj && !obj.isCorporateUser) {
      obj.companyName = "";
    }
    const _regAPI = this.mode === 'edit' ? this.authService.updateRegUser(getLoggedUserData().UserID, obj) : this.authService.userRegistration(obj)
    _regAPI.subscribe((res: JsonResponse) => {
      if (res.returnId > 0) {
        if (this.mode === 'edit') {
          this._toast.success('Account updated successfully')
        } else {
          this._toast.success(res.returnText)
        }
        this.userForm.reset()
        this.closeModal('success')
      } else {
        if (res.returnCode === '2') {
          this._toast.info(res.returnText, res.returnStatus)
        } else {
          this._toast.error(res.returnText, res.returnStatus)
        }
      }
      this.isSubmitting = false
    }, (err: HttpErrorResponse) => {
      this.isSubmitting = false
    })
  }
  NumberValid(evt) {
    let charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 37 && charCode != 39 && charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }

  confirmPassword(event) {
    let element = event.target.nextSibling;
    if (element.type === "password" && element.value) {
      element.type = "text";
      this.colorEye = "black";
    }
    else {
      element.type = "password";
      this.colorEye = "grey";

    };
  }


  textValidation(event) {
    try {
      const pattern = /[a-zA-Z-][a-zA-Z -]*$/;
      const inputChar = String.fromCharCode(event.charCode);
      if (!pattern.test(inputChar)) {

        if (event.charCode == 0) {
          return true;
        }

        if (event.target.value) {
          const end = event.target.selectionEnd;
          if ((event.which == 32 || event.keyCode == 32) && (event.target.value[end - 1] == " " || event.target.value[end] == " ")) {
            event.preventDefault();
            return false;
          }
        }
        else {
          event.preventDefault();
          return false;
        }
      } else {
        return true;
      }
    } catch (error) {
      return false
    }

  }



  search = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .map(term => (!term || term.length < 3) ? []
        : this.cityList.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1));
  formatter = (x: { title: string, desc: string, imageName: string }) => {
    this.city.imageName = x.imageName;
    this.city.title = x.title;
    this.city.desc = x.desc;
    this.isCitySearching = false
    return x.title;

  };

  isCitySearching: boolean = false
  hasCitySearchFailed: boolean = false
  hasCitySearchSuccess: boolean = false

  search2 = (text$: Observable<string>) =>
    text$
      .debounceTime(300) //debounce time
      .distinctUntilChanged()
      .do(() => { this.isCitySearching = true; this.hasCitySearchFailed = false; this.hasCitySearchSuccess = false; }) // do any action while the user is typing
      .mergeMap(term => {
        let some = of([]); //  Initialize the object to return
        try {
          if (term && term.length >= 3) { //search only if item are more than three
            some = this.dropdownservice.getFilteredCity(term)
              .do((res) => { this.isCitySearching = false; this.hasCitySearchSuccess = true; return res; })
              .catch(() => { this.isCitySearching = false; this.hasCitySearchFailed = true; return []; })
          } else { this.isCitySearching = false; some = of([]); }
        } catch (error) {
          this.isCitySearching = false
        }
        return some;
      })
      .do((res) => { this.isCitySearching = false; return res; })
      .catch(() => { this.isCitySearching = false; return of([]); }); // final server list

  onCitySelect({ target }, city) {
    if (target.value === "") {

      // this.city = {};
    }
  }

  companyPredic = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .map(term => (!term || term.length < 3) ? []
        : this.companyList.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1));
  companyFormater = (x: { title: string, desc: string, imageName: string }) => {
    return x.title;

  };

  getNum(_num) {
    return (_num) ? (parseFloat(_num) ? typeof parseFloat(_num) === 'number' ? parseFloat(_num) : 0 : 0) : 0
  }

}

