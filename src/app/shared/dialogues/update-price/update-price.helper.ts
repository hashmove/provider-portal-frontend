import { DashboardService } from "../../../components/pages/user-desk/dashboard/dashboard.service";
import { ICarrierSchedule } from "../add-schedule/add-schedule.interface";
import { IBookingDtl } from "../../../interfaces/user-bookings.interface"
import { ICarrierFilter } from "../../../interfaces/schedule.interface";
import { getLoggedUserData } from "../../../constants/globalFunctions"
import { JsonResponse } from "../../../interfaces";


export class UpdatePriceHelper {

    defaultFeilds = {
        actualIndividualPrice: 0,
        baseIndividualPrice: 0,
        exchangeRate: 0,
        currExcgRateListID: null,
        perUnitRate: null,
        perUnitBaseRate: 0,
        perUnitExchangeRate: null,
        perUnitRateType: null,
        requestStatus: 'PRICE_SENT',
    }

    constructor() {
    }

    sendPriceLogMessage(
        _dashboardService: DashboardService,
        _booking: IBookingDtl,
        _message: string,
        _bookingCarrierSchedule: ICarrierSchedule,
        _origin: ICarrierFilter,
        _destination: ICarrierFilter,
        _selectedShipping: any,
        isRouteDetailUpdate: boolean,
        _pricingDetails: IPricingDetails,
        _originalCarrierID: number
    ): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            setTimeout(() => {
                const _toSend = {
                    ..._pricingDetails,
                    ...this.defaultFeilds,

                    bookingID: _booking.BookingID,
                    currencyID: _booking.CurrencyID,
                    specialRequestComments: _message,
                    carrierID: _selectedShipping && _selectedShipping.id && _selectedShipping.id !== _originalCarrierID ? _selectedShipping.id : null,
                    userID: getLoggedUserData().UserID,
                    isRouteDetailUpdate: isRouteDetailUpdate,
                    
                    IsOnlyComments: true,

                    pickupPort: {
                        id: _origin && _origin.id ? _origin.id : null,
                        code: _origin && _origin.code ? _origin.code : _booking.PolCode,
                        title: _origin && _origin.title ? _origin.title : _booking.PolName,
                        imageName: _origin && _origin.imageName ? _origin.imageName : _booking.PolCountryCode
                    },
                    deliveryPort: {
                        id: _destination && _destination.id ? _destination.id : null,
                        code: _destination && _destination.code ? _destination.code : _booking.PodCode,
                        title: _destination && _destination.title ? _destination.title : _booking.PodName,
                        imageName: _destination && _destination.imageName ? _destination.imageName : _booking.PodCountryCode
                    },
                    isScheduleUpdate: true,
                    bookingCarrierSchedule: _bookingCarrierSchedule
                }

                _dashboardService.updateSpecialPrice(_booking.BookingID, getLoggedUserData().UserID, _toSend).subscribe((res: JsonResponse) => {
                    if (res.returnId > 0) {
                        resolve(true)
                    } else {
                        resolve(false)
                    }
                }, error => {
                    reject(false)
                })
            }, 0);
        });
    }

    getRateType(searchMode) {
        switch (searchMode) {
            case 'sea-fcl':
                return 'Rate Per Container';
            case 'sea-lcl':
                return 'Rate Per CBM';
            case 'air-lcl':
                return 'Rate Per KG';
            case 'truck-ftl':
                return 'Rate Per Truck';
            default:
                return 'Container';
        }
    }
}

export interface IPricingDetails {
    currencyID: number
    actualIndividualPrice: number
    baseIndividualPrice: number
    exchangeRate: number
    currExcgRateListID: number
    perUnitCurrencyID: number
    perUnitRate: number
    perUnitBaseRate: number
    perUnitExchangeRate: number
    perUnitRateType: string
    requestStatus: string
}
