import { Component, OnInit, Input } from '@angular/core'
import { encryptBookingID, feet2String, getLoggedUserData, getImagePath, ImageRequiredSize, ImageSource } from '../../../constants/globalFunctions'
import { ConfirmDialogContent, ConfirmDialogGenComponent } from '../confirm-dialog-generic/confirm-dialog-generic.component'
import { ViewBookingService } from '../../../components/pages/user-desk/view-booking/view-booking.service'
import { DashboardService } from '../../../components/pages/user-desk/dashboard/dashboard.service'
import { ConfirmUpdateDialogComponent } from '../confirm-update/confirm-update-dialog.component'
import { AddScheduleComponent } from '../add-schedule/add-schedule.component'
import { UpdatePriceHelper } from './update-price.helper'
import { isNumber } from '../sea-rate-dialog/sea-rate-dialog.component'
import { PriceLogsComponent } from '../price-logs/price-logs.component'
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { CurrencyControl } from '../../../services/currency.service'
import { IPriceLogs } from '../../../interfaces/pricing.interface'
import { CommonService } from '../../../services/common.service'
import { SharedService } from '../../../services/shared.service'
import { JsonResponse } from '../../../interfaces/JsonResponse'
import { ICarrierFilter } from '../../../interfaces'
import { debounceTime, map } from 'rxjs/operators'
import { ToastrService } from 'ngx-toastr'
import { Observable } from 'rxjs'
import { ICarrierSchedule } from '../add-schedule/add-schedule.interface'
import { SearchCriteriaContainerDetail } from '../cargo-details/cargo-details.interface'
@Component({
  selector: 'app-update-price',
  templateUrl: './update-price.component.html',
  styleUrls: ['./update-price.component.scss']
})
export class UpdatePriceComponent extends UpdatePriceHelper implements OnInit {
  @Input() data: any
  @Input() searchCriteria: any
  public containers: any[] = []
  public specialRequestComments = ''
  public selectedCurrency: any = {}
  public actualIndividualPrice
  public actualIndividualPriceOrigin
  public userInfo: any = {}
  public loading = false
  allowCurrenySelect = false
  specialReqDesc = ''
  searchMode = 'sea-fcl'

  originPorts: ICarrierFilter[] = []
  destinPorts: ICarrierFilter[] = []

  public filterOrigin: ICarrierFilter
  public filterDestination: ICarrierFilter
  public isRouteDetailUpdate = false
  priceUpdateLogs: IPriceLogs[] = []

  public selectedShipping: ICarrierFilter
  allShippingLines: ICarrierFilter[] = []
  commodityDesc: string
  showPerRateInp = false
  rateType = 'Container'
  perRatePrice: any = null
  perRatePriceOrigin: any = null

  perPriceError = false
  totalPriceError = false
  originalCarrierID: number
  vesselSchedule = null
  schdeuleStatus = 'NONE'
  public currencyList: any[] = []
  totalCBM = 0

  constructor(
    private _modalService: NgbModal,
    private _dashboardService: DashboardService,
    public _activeModal: NgbActiveModal,
    private _sharedService: SharedService,
    private _toast: ToastrService,
    private _currencyService: CurrencyControl,
    private _commonService: CommonService,
    private _viewBookingService: ViewBookingService
  ) {
    super()
  }

  ngOnInit() {
    const { data } = this
    this.originalCarrierID = data.CarrierID
    if (this.data.ShippingModeCode === 'SEA') {
      this.getVesselSchedule()
    }
    try {
      this.commodityDesc = data.BookingDesc ? data.BookingDesc : 'N/A'
    } catch {
      this.commodityDesc = 'N/A'
    }

    this.userInfo = getLoggedUserData()
    this.getBookingLogs()
    this.getAllCarriers()
    try {
      if (this.data.BookingSpecialAmount && this.data.IsRateNotFoundBooking) {
        this.actualIndividualPrice = this.data.BookingSpecialAmount ? this.data.BookingSpecialAmount : 0
        this.actualIndividualPriceOrigin = this.data.BookingSpecialAmount ? this.data.BookingSpecialAmount : 0
        this.perRatePrice = this.data.PerUnitRate ? this.data.PerUnitRate : null
        this.perRatePriceOrigin = this.data.PerUnitRate ? this.data.PerUnitRate : null
      }
    } catch { }
    try {
      this.searchMode = this.data.ShippingModeCode.toLowerCase() + '-' + this.data.ContainerLoad.toLowerCase()
    } catch (ex) { }

    if (this.searchMode === 'sea-lcl')
      this.setCBM(JSON.parse(this.data.JsonSearchCriteria).SearchCriteriaContainerDetail)

    this.rateType = this.getRateType(this.searchMode)
    if (this.data.IsRateNotFoundBooking) {
      this.showPerRateInp = true
      if (this.searchMode === 'warehouse-lcl')
        this.showPerRateInp = false
    }

    if (data.IsRateNotFoundBooking && (!data.BookingSpecialAmount || data.BookingSpecialAmount === 0))
      this.allowCurrenySelect = true

    this.getContainerDetails()
    this._sharedService.currencyList.subscribe((state: any) => {
      if (state) {
        this.currencyList = state
        if (!this.allowCurrenySelect)
          this.selectedCurrency = this.currencyList.find(e => e.id === this.data.CurrencyID)
      }
    })
    if (this.data.ShippingModeCode === 'SEA' || this.data.ShippingModeCode === 'AIR')
      this.setPortsData(this.data.ShippingModeCode)
  }

  setCBM($containers: SearchCriteriaContainerDetail[]) {
    const _totalCBM = $containers.reduce((a, b) => +a + +b.contRequestedCBM, 0);
    this.totalCBM = _totalCBM ? Math.ceil(_totalCBM) : 1
  }

  getVesselSchedule() {
    if (this.data.EtdUtc) {
      this._viewBookingService.getBookingRouteDetail(encryptBookingID(this.data.BookingID, this.data.ProviderID, this.data.ShippingModeCode)).subscribe((res: JsonResponse) => {
        if (res.returnId > 0) {
          this.vesselSchedule = res.returnObject
          console.log('after fetched:', JSON.stringify(this.vesselSchedule))
        }
      }, error => { })
    }
  }

  setDefSpecDesc() {
    try {
      this.specialReqDesc = this.data.SpecialRequestDesc ? this.data.SpecialRequestDesc : 'N/A'
    } catch (error) {
      this.specialReqDesc = 'N/A'
    }
  }

  async getAllCarriers() {
    try {
      if (localStorage.getItem('carriersList')) {
        this.allShippingLines = JSON.parse(localStorage.getItem('carriersList')).filter(e => e.type === this.data.ShippingModeCode)
      } else {
        const _res: any[] = await this._viewBookingService.getAllCarriers().toPromise() as any
        localStorage.setItem('carriersList', JSON.stringify(_res))
        this.allShippingLines = _res.filter(e => e.type === this.data.ShippingModeCode)
      }
      if (this.data.CarrierID)
        this.selectedShipping = this.allShippingLines.find(obj => obj.id === this.data.CarrierID)
    } catch (error) { }
  }

  closeModal = () => this._activeModal.close(false)

  getContainerDetails() {
    if (this.data.ShippingModeCode !== 'AIR') {
      this._dashboardService.getContainerDetails(encryptBookingID(this.data.BookingID, this.data.ProviderID, this.data.ShippingModeCode), 'PROVIDER')
        .subscribe((res: JsonResponse) => {
          this.containers = res.returnObject
          if (this.containers && this.containers.length > 1 && this.data.IsRateNotFoundBooking && this.searchMode !== 'sea-lcl')
            this.showPerRateInp = false
        }, (err) => { console.log(err) })
    }
  }

  getContainerInfo(container) {
    const containerInfo = { containerSize: undefined, containerWeight: undefined }
    containerInfo.containerWeight = container.MaxGrossWeight
    const { containerLength, containerWidth, containerHeight } = container
    containerInfo.containerSize = feet2String(containerLength) + ` x ` + feet2String(containerWidth) + ` x ` + feet2String(containerHeight)
    return containerInfo
  }

  currency = (text$: Observable<string>) =>
    text$.debounceTime(200).map(term => (!term || term.length < 2) ? []
      : this.currencyList.filter(v => v.shortName.toLowerCase().indexOf(term.toLowerCase()) > -1))

  formatterCurrency = (_curr) => {
    this.selectedCurrency = _curr
    return _curr.shortName
  }

  spaceHandler(event) {
    if (event.charCode === 32) {
      event.preventDefault()
      return false
    }
  }

  async submitPrice() {

    let _checkCondit = true
    if ((!this.actualIndividualPrice || parseFloat(this.actualIndividualPrice) === 0) && (!this.data.BookingSpecialAmount || parseFloat(this.data.BookingSpecialAmount) === 0))
      _checkCondit = false
    else if (parseFloat(this.actualIndividualPrice) === this.data.BookingSpecialAmount)
      _checkCondit = false

    if (this.showPerRateInp && !_checkCondit) {
      _checkCondit = true
      if ((!this.perRatePrice || parseFloat(this.perRatePrice) === 0) && (!this.data.PerUnitRate || parseFloat(this.data.PerUnitRate) === 0))
        _checkCondit = false
      else if (parseFloat(this.perRatePrice) === this.data.PerUnitRate)
        _checkCondit = false
    }

    if (this.isPriceInvalid(_checkCondit))
      return

    // if (this.data.IsRateNotFoundBooking && !_checkCondit && !this.specialRequestComments) {
    //   this._toast.warning('No changes has been made to this Request', 'Cannot Update')
    //   return
    // }
    if ((this.selectedCurrency && this.selectedCurrency.id) && (!this.actualIndividualPrice || this.actualIndividualPrice <= 0)) {
      this._toast.warning('Price cannot be zero if currency is selected', 'Warning')
      return
    }
    if ((this.isRouteDetailUpdate || (this.actualIndividualPrice)) && this.searchMode === 'sea-fcl') {
      if (this.data.PolType === 'GROUND' && (!this.filterOrigin || !this.filterOrigin.id)) {
        this._toast.warning('Please enter origin port.')
        return
      }
      if (this.data.PodType === 'GROUND' && (!this.filterDestination || !this.filterDestination.id)) {
        this._toast.warning('Please enter destination port.')
        return
      }
    }
    try {
      if (this.data.BookingSpecialAmount && (!this.actualIndividualPrice || parseFloat(this.actualIndividualPrice) === 0)) {
        this._toast.warning('Price cannot be zero after quoting the price.')
        return
      }
    } catch { }
    try {
      if (this.actualIndividualPrice && !isNumber(this.actualIndividualPrice)) {
        this._toast.warning('Invalid Price entered, price should be a number.', 'Invalid Operation')
        return
      }
    } catch { }
    try {
      if (this.data.BookingSpecialAmount && !this.selectedCurrency) {
        this._toast.warning('Please select currency.')
        return
      }
    } catch { }


    const baseCurr = JSON.parse(localStorage.getItem('CURR_MASTER'))
    let exchangeRate = null
    const _exchangeRes: JsonResponse = await this._commonService.getExchangeRateList(baseCurr.fromCurrencyID).toPromise()
    try {
      exchangeRate = _exchangeRes.returnObject.rates.filter(rate => rate.currencyID === (this.selectedCurrency.id ? this.selectedCurrency.id : this.data.CurrencyID))[0]
    } catch (error) { }

    let obj: any
    const discountedPrice = this.data.BookingTotalAmount - this.actualIndividualPrice
    const { UserID } = getLoggedUserData()
    try {
      obj = {
        bookingID: this.data.BookingID,
        currencyID: this.selectedCurrency.id ? this.selectedCurrency.id : this.data.CurrencyID,
        actualIndividualPrice: discountedPrice ? discountedPrice : 0,
        baseIndividualPrice: discountedPrice ? this._currencyService.getPriceToBase((discountedPrice), true, exchangeRate.rate) : 0,
        specialRequestComments: this.specialRequestComments,
        userID: UserID,
        requestStatus: 'PRICE_SENT',
        // IsOnlyComments: (!this.selectedShipping || this.selectedShipping.id === this.data.CarrierID) ? !_checkCondit : false,
        IsOnlyComments: false,
        exchangeRate: exchangeRate ? exchangeRate.rate : null,
        currExcgRateListID: exchangeRate ? exchangeRate.currExcgRateListID : null,
        isRouteDetailUpdate: this.isRouteDetailUpdate,
        carrierID: this.selectedShipping && this.selectedShipping.id && this.selectedShipping.id !== this.data.CarrierID ? this.selectedShipping.id : null,
        pickupPort: {
          id: this.searchMode === 'sea-fcl' && this.filterOrigin ? this.filterOrigin.id : null,
          code: this.searchMode === 'sea-fcl' && this.filterOrigin ? this.filterOrigin.code : this.data.PolCode,
          title: this.searchMode === 'sea-fcl' && this.filterOrigin ? this.filterOrigin.title : this.data.PolName,
          imageName: this.searchMode === 'sea-fcl' && this.filterOrigin ? this.filterOrigin.imageName : this.data.PolCountryCode
        },
        deliveryPort: {
          id: this.searchMode === 'sea-fcl' && this.filterDestination ? this.filterDestination.id : null,
          code: this.searchMode === 'sea-fcl' && this.filterDestination ? this.filterDestination.code : this.data.PodCode,
          title: this.searchMode === 'sea-fcl' && this.filterDestination ? this.filterDestination.title : this.data.PodName,
          imageName: this.searchMode === 'sea-fcl' && this.filterDestination ? this.filterDestination.imageName : this.data.PodCountryCode
        },
        perUnitCurrencyID: this.selectedCurrency.id ? this.selectedCurrency.id : this.data.CurrencyID,
        perUnitRate: this.perRatePrice ? parseFloat(this.perRatePrice) : null,
        perUnitBaseRate: this.perRatePrice ? this._currencyService.getPriceToBase((this.perRatePrice), true, exchangeRate.rate) : 0,
        perUnitExchangeRate: exchangeRate ? exchangeRate.rate : null,
        PerUnitRateType: this.rateType ? this.rateType : null,
      }
      if (this.schdeuleStatus !== 'NONE') {
        obj = {
          ...obj,
          isScheduleUpdate: true,
          bookingCarrierSchedule: this.vesselSchedule
        }
      } else {
        obj = {
          ...obj,
          isScheduleUpdate: false,
          bookingCarrierSchedule: null
        }
      }

      if (this.isPriceInvalid(true)) {
        return
      }

      let _hasPriceChanged = false

      if (this.showPerRateInp) {
        if (parseFloat(this.actualIndividualPriceOrigin) !== parseFloat(this.actualIndividualPrice) ||
          parseFloat(this.perRatePriceOrigin) !== parseFloat(this.perRatePrice))
          _hasPriceChanged = true
      } else {
        if (parseFloat(this.actualIndividualPriceOrigin) !== parseFloat(this.actualIndividualPrice))
          _hasPriceChanged = true
      }

      // check if there are any changes of the below mentioned citeria
      console.log(_hasPriceChanged, obj.isRouteDetailUpdate, obj.carrierID, obj.isScheduleUpdate)
      const _hasChanges = _hasPriceChanged || obj.isRouteDetailUpdate || obj.carrierID || obj.isScheduleUpdate
      if (!_hasChanges) {
        this._toast.warning('No changes has been made to this Request', 'Cannot Update')
        return
      }

    } catch (error) {
      console.log(error)
    }
    this.loading = true


    if (_checkCondit) {
      const modalRef = this._modalService.open(ConfirmUpdateDialogComponent, {
        size: 'lg',
        centered: true,
        windowClass: 'small-modal',
        backdrop: 'static',
        keyboard: false
      })
      const { actualIndividualPrice } = this
      modalRef.componentInstance.amount = (actualIndividualPrice <= 0) ? actualIndividualPrice * (-1) : actualIndividualPrice
      modalRef.componentInstance.code = this.selectedCurrency.code
      modalRef.componentInstance.isSpotRate = true
      modalRef.componentInstance.data = this.data

      modalRef.result.then((result) => {
        if (result) {
          this.updatePriceAction(UserID, obj)
        } else {
          this.loading = false
        }
      })
    } else {
      this.updatePriceAction(UserID, obj)
    }
  }

  isPriceInvalid(_checkCondit: boolean) {
    let _isInvalid = false
    if (_checkCondit) {
      if (!this.selectedCurrency || !this.selectedCurrency.id) {
        this._toast.warning('Please Select Currency', 'Warning')
        _isInvalid = true
      } else if (this.showPerRateInp && (!this.perRatePrice)) {
        this._toast.warning(`${this.rateType} cannot be Empty.`, 'Warning')
        _isInvalid = true
      } else if (this.actualIndividualPrice >= this.data.BookingTotalAmount && !this.data.IsRateNotFoundBooking) {
        this._toast.warning('Total Charges should be less than original price.', 'Warning')
        _isInvalid = true
      } else if (this.actualIndividualPrice <= 0) {
        this._toast.warning('Total Charges cannot be zero', 'Warning')
        _isInvalid = true
      } else if (this.showPerRateInp && this.actualIndividualPrice <= 0) {
        this._toast.warning(`${this.rateType} cannot be zero`, 'Warning')
        _isInvalid = true
      } else if (this.showPerRateInp) {
        const _perPrice = parseFloat(this.perRatePrice)
        if (_perPrice > this.actualIndividualPrice) {
          this._toast.warning(`${this.rateType} cannot be greater than Total Charges`, 'Warning')
          this.perPriceError = true
          this.totalPriceError = true
          _isInvalid = true
        } else {
          this.perPriceError = false
          this.totalPriceError = false
        }
      }
    }
    return _isInvalid
  }

  validatePerPrice() {
    if (this.showPerRateInp && this.perRatePrice && this.actualIndividualPrice) {
      const _perPrice = parseFloat(this.perRatePrice)
      if (_perPrice > this.actualIndividualPrice) {
        this._toast.warning(`${this.rateType} cannot be greater than Total Charges`, 'Warning')
        this.perPriceError = true
        this.totalPriceError = true
        return
      } else {
        this.perPriceError = false
        this.totalPriceError = false
      }
    }
  }

  async updatePriceAction(UserID, $obj) {
    if (this.schdeuleStatus !== 'NONE') {
      if (this.schdeuleStatus === 'ADDED') {
        const _schedule = await this.saveScheduleToBooking()
        console.log(_schedule)
        if (_schedule) {
          this._toast.success('Vessel schedule has been added')
          this.setVesselSchdeule(_schedule)
        }
      } else {
        const _state = await this.removeSchCall()
        if (_state) {
          this._toast.success('Vessel schedule has been removed')
          this.removeVesselScheduleData()
        }
      }
    }
    this._dashboardService.updateSpecialPrice(this.data.BookingID, UserID, $obj).subscribe((res: JsonResponse) => {
      this.loading = false
      if (res.returnId > 0) {
        const response = JSON.parse(res.returnText)
        let obj: any = {
          price: this.actualIndividualPrice,
          currencyID: this.selectedCurrency.id ? this.selectedCurrency.id : this.data.CurrencyID,
          currencyCode: this.selectedCurrency.code ? this.selectedCurrency.code : this.data.CurrencyCode,
          status: response.SpecialRequestStatus,
          perRatePrice: this.perRatePrice,
          rateType: this.rateType,
          statusBl: response.SpecialRequestStatusBL,
          logs: JSON.parse(response.JSONSpecialRequestLogs),
          isScheduleUpdate: $obj.isScheduleUpdate ? $obj.isScheduleUpdate : false
        }

        if (this.selectedShipping && this.selectedShipping.id) {
          const { selectedShipping } = this
          obj = {
            ...obj,
            carrierId: selectedShipping.id,
            carrierName: this.getCarrierName(selectedShipping),
            carrierImage: selectedShipping.imageName
          }
        }
        this._toast.success('Price updated successfully', 'Success')
        this._activeModal.close(obj)
      } else {
        this._toast.error(res.returnText, 'Failed')
      }
    }, (err: any) => {
      this.loading = false
      console.log(err)
    })
  }

  getCarrierName($selectedShipping) {
    let _finnCarrier = ''
    try {
      const _carrierName = $selectedShipping.title.split(',')[1].replace(' ', '')
      if (_carrierName && _carrierName.length > 0) {
        _finnCarrier = _carrierName
      } else {
        _finnCarrier = $selectedShipping.type === 'SEA' ? $selectedShipping.shortName : $selectedShipping.title
      }
    } catch {
      _finnCarrier = $selectedShipping.type === 'SEA' ? $selectedShipping.shortName : $selectedShipping.title
    }
    return _finnCarrier
  }

  async setPortsData(selectedShippingModeCode) {
    const _ports: Array<ICarrierFilter> = (localStorage.getItem('shippingPortDetails')) ? JSON.parse(localStorage.getItem('shippingPortDetails')) : []
    const ports_to_get = (selectedShippingModeCode.toLowerCase() === 'truck') ? 'sea' : selectedShippingModeCode.toLowerCase()
    const filteredPorts: Array<ICarrierFilter> = _ports.filter(port => port.type.toLowerCase().includes(ports_to_get))
    const hasMode = filteredPorts.length > 0 ? true : false
    if (hasMode) {
      this.setPorts(filteredPorts)
    } else {
      try {
        const res = await this._commonService.getPortsDataV2(ports_to_get.toUpperCase()).toPromise() as any
        const newPorts = _ports.concat(res)
        const _filteredPorts: Array<ICarrierFilter> = newPorts.filter(port => port.type.toLowerCase().includes(ports_to_get))
        this.setPorts(_filteredPorts)
        localStorage.setItem('shippingPortDetails', JSON.stringify(newPorts))
      } catch {
      }
    }
  }

  setPorts(_ports) {
    const { PolCountry, PolType, PolCode, PodCountry, PodType, PodCode } = this.data
    if (PolCountry) {
      this.originPorts = _ports.filter(_port => _port.title.toLowerCase().includes(PolCountry.toLowerCase()))
      if (PolType === 'SEA' || PolType === 'AIR') {
        this.filterOrigin = this.originPorts.filter(_port => _port.code === PolCode)[0]
      }
    }
    if (PodCountry) {
      this.destinPorts = _ports.filter(_port => _port.title.toLowerCase().includes(PodCountry.toLowerCase()))
      if (PodType === 'SEA' || PodType === 'AIR') {
        this.filterDestination = this.destinPorts.filter(_port => _port.code === PodCode)[0]
      }
    }
  }

  filterPortsFormatter = (x) => x.title
  originPortsFilter = (text$: Observable<string>) =>
    text$.debounceTime(200).map(term => !term || term.length < 3 ? []
      : this.originPorts.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))

  destinPortsFilter = (text$: Observable<string>) =>
    text$.debounceTime(200).map(term => !term || term.length < 3 ? []
      : this.destinPorts.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))

  getBookingLogs() {
    const _id = encryptBookingID(this.data.BookingID, this.data.ProviderID, this.data.ShippingModeCode)
    this._dashboardService.getBookingSpecialLogs(_id, 'PROVIDER').subscribe((res: JsonResponse) => {
      if (res.returnId > 0) {
        this.priceUpdateLogs = res.returnObject
        const { priceUpdateLogs } = this
        try {
          const _xas = priceUpdateLogs.filter(_log => _log.LogStatus && _log.LogStatus !== 'PRICE_SENT')
          if (_xas && _xas.length > 0) {
            if (this.data.IsRateNotFoundBooking && _xas[0].LogStatus === 'PENDING') {
              this.specialReqDesc = 'N/A'
            } else {
              this.specialReqDesc = _xas[0].Comments
            }
          } else {
            this.setDefSpecDesc()
          }
        } catch {
          this.setDefSpecDesc()
        }
        try {
          const originLogs = priceUpdateLogs.filter(_log => _log.PickupPortDetail)[0]
          const destLogs = priceUpdateLogs.filter(_log => _log.DeliveryPortDetail)[0]

          if (originLogs) {
            const _portsData = JSON.parse(originLogs.PickupPortDetail)
            this.filterOrigin = this.originPorts.filter(_port => _port.code === _portsData.Code)[0]
          }
          if (destLogs) {
            const _portsData = JSON.parse(destLogs.DeliveryPortDetail)
            this.filterDestination = this.destinPorts.filter(_port => _port.code === _portsData.Code)[0]
          }
        } catch (error) {
          console.log(error)
        }
      }
    }, (err: any) => {
      console.log(err)
    })
  }

  getShippingLineImage = ($image: string) => getImagePath(ImageSource.FROM_SERVER, '/' + $image, ImageRequiredSize.original)

  viewLogs() {
    const { priceUpdateLogs } = this
    const modalRef = this._modalService.open(PriceLogsComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'history-modal',
      backdropClass: '.app-session-modal-backdrop',
      keyboard: false
    })
    modalRef.result.then((result) => { })
    modalRef.componentInstance.data = { booking: this.data, logs: priceUpdateLogs }
  }

  shippings = (text$: Observable<string>) =>
    text$.pipe(debounceTime(200), map(term => !term || term.length < 3
      ? [] : this.allShippingLines.filter(v => v.title && v.title.toLowerCase().indexOf(term.toLowerCase()) > -1)))

  openAddSchedule() {
    if (this.data.PolType === 'GROUND' && (!this.filterOrigin || !this.filterOrigin.id)) {
      this._toast.warning('Please enter origin port.')
      return
    }

    if (this.data.PodType === 'GROUND' && (!this.filterDestination || !this.filterDestination.id)) {
      this._toast.warning('Please enter destination port.')
      return
    }

    if ((!this.selectedShipping || !this.selectedShipping.id) || (this.selectedShipping && this.selectedShipping.code === 'DEFAULT')) {
      this._toast.warning('Please select a valid Shipping Line')
      return
    }

    try {
      const modalRef = this._modalService.open(AddScheduleComponent, {
        size: 'lg',
        centered: true,
        windowClass: 'schedule-modal',
        backdrop: 'static',
        keyboard: false
      })

      modalRef.componentInstance.carrierList = this.allShippingLines
      modalRef.componentInstance.PolID = this.filterOrigin.id
      modalRef.componentInstance.PodID = this.filterDestination.id
      modalRef.componentInstance.bookingDetails = this.data
      modalRef.componentInstance.bookCarrier = this.selectedShipping && this.selectedShipping.id ? this.selectedShipping : null
      modalRef.componentInstance.from = 'BOOKING_LIST'
      modalRef.componentInstance.schedule = this.schdeuleStatus !== 'REMOVED' ? this.vesselSchedule : null


      modalRef.result.then(_schedule => {
        if (_schedule) {
          this.vesselSchedule = _schedule
          this.schdeuleStatus = 'ADDED'
        }
      })
    } catch (error) {
      console.log(error)
    }
  }

  setVesselSchdeule(_schedule: ICarrierSchedule) {
    try {
      this.data.CarrierID = _schedule.carrierID
      this.data.CarrierName = _schedule.carrierName
      this.data.CarrierImage = _schedule.carrierImage
      this.data.VoyageRefNum = _schedule.voyageRefNo
      this.data.VesselName = _schedule.vesselName
      this.data.VesselCode = _schedule.vesselCode
      this.data.PortCutOffLcl = _schedule.portCutOffDate
      this.data.EtdLcl = _schedule.etdDate
      this.data.EtdUtc = _schedule.etdDate
      this.data.EtaLcl = _schedule.etaDate
      this.data.EtaUtc = _schedule.etaDate
      try { this.data.TransitTime = _schedule.transitDays } catch (err) { console.log(err) }
    } catch { }
  }

  removeVesselScheduleData() {
    try {
      this.data.VoyageRefNum = null
      this.data.VesselName = null
      this.data.VesselCode = null
      this.data.PortCutOffLcl = null
      this.data.EtdLcl = null
      this.data.EtdUtc = null
      this.data.EtaLcl = null
      this.data.EtaUtc = null
      try { this.data.TransitTime = null } catch (err) { console.log(err) }
    } catch { }
  }

  removeVesselSchedule() {
    try {
      const modalRef = this._modalService.open(ConfirmDialogGenComponent, {
        size: 'lg',
        centered: true,
        windowClass: 'schedule-modal',
        backdrop: 'static',
        keyboard: false
      })

      const _modalData: ConfirmDialogContent = {
        messageTitle: 'Delete Schedule',
        messageContent: `Are you sure you want to delete the current Schedule?`,
        buttonTitle: 'Yes',
        data: null
      }
      modalRef.componentInstance.modalData = _modalData

      modalRef.result.then(result => {
        if (result) {
          this.schdeuleStatus = 'REMOVED'
          this.vesselSchedule = null
        }
      })
    } catch (error) {
      console.log(error)
    }
  }

  getRateType(_searchMode) {
    switch (_searchMode) {
      case 'sea-fcl':
        return 'Rate Per Container'
      case 'sea-lcl':
        return 'Rate Per CBM'
      case 'air-lcl':
        return 'Rate Per KG'
      case 'truck-ftl':
        return 'Rate Per Truck'
      default:
        return 'Rate Per Container'
    }
  }

  async saveScheduleToBooking() {
    try {
      const _res: JsonResponse = await this._viewBookingService.saveCarrierSchedule(this.vesselSchedule).toPromise() as any
      return (_res.returnId > 0) ? _res.returnObject : null
    } catch { return null }
  }

  async removeSchCall() {
    try {
      const _res: JsonResponse = await this._viewBookingService.removeCarrierScheduleByBooking(this.data.BookingID, getLoggedUserData().UserID).toPromise() as any
      return (_res.returnId > 0) ? true : false
    } catch { return false }
  }

  formatter = (_carr) => (_carr.code === 'DEFAULT' ? _carr.shortName : `${_carr.code}, ${_carr.title}`)
}
