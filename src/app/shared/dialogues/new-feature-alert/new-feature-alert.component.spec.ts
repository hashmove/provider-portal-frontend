import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewFeatureAlertComponent } from './new-feature-alert.component';

describe('NewFeatureAlertComponent', () => {
  let component: NewFeatureAlertComponent;
  let fixture: ComponentFixture<NewFeatureAlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewFeatureAlertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewFeatureAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
