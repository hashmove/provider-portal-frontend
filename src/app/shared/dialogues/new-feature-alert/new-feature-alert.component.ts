import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-new-feature-alert',
  templateUrl: './new-feature-alert.component.html',
  styleUrls: ['./new-feature-alert.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NewFeatureAlertComponent {

  @Input() tipMsg:string = null
  @Input() placement = 'top'
  @Input() hasContainer = false
}