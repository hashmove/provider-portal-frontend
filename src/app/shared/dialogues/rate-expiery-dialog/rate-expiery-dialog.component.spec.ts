import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RateExpieryDialogComponent } from './rate-expiery-dialog.component';

describe('RateExpieryDialogComponent', () => {
  let component: RateExpieryDialogComponent;
  let fixture: ComponentFixture<RateExpieryDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RateExpieryDialogComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RateExpieryDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
