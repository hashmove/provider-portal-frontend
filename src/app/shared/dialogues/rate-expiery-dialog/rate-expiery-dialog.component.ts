import { Component, OnInit, Input, ElementRef, ViewChild, Renderer2 } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { NgbActiveModal, NgbDateParserFormatter, NgbDateStruct, NgbInputDatepicker } from "@ng-bootstrap/ng-bootstrap";
import * as moment from 'moment'
import { isNumber } from '../sea-rate-dialog/sea-rate-dialog.component';
import { ToastrService } from 'ngx-toastr';
import { ManageRatesService } from '../../../components/pages/user-desk/manage-rates/manage-rates.service';
import { JsonResponse } from '../../../interfaces/JsonResponse';
import { getLoggedUserData, loading } from '../../../constants/globalFunctions';

const now = new Date();
const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
  one && two && two.year === one.year && two.month === one.month && two.day === one.day;

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day < two.day : one.month < two.month : one.year < two.year;

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day > two.day : one.month > two.month : one.year > two.year;


@Component({
  selector: 'app-rate-expiery-dialog',
  templateUrl: './rate-expiery-dialog.component.html',
  styleUrls: ['./rate-expiery-dialog.component.scss']
})
export class RateExpieryDialogComponent implements OnInit {

  public startDate: NgbDateStruct;
  public maxDate: NgbDateStruct;
  public minDate: NgbDateStruct;
  public hoveredDate: NgbDateStruct;
  public fromDate = {
    day: null,
    month: undefined,
    year: undefined
  };
  public toDate = {
    day: undefined,
    month: undefined,
    year: undefined
  };
  @ViewChild("dp") input: NgbInputDatepicker;
  @ViewChild('rangeDp') rangeDp: ElementRef;

  isHovered = date =>
    this.fromDate && !this.toDate && this.hoveredDate && after(date, this.fromDate) && before(date, this.hoveredDate)
  isInside = date => after(date, this.fromDate) && before(date, this.toDate);
  isFrom = date => equals(date, this.fromDate);
  isTo = date => equals(date, this.toDate);


  val1 = 0
  val2 = 0
  departureDate
  @Input() mode
  @Input() rateToDate: string

  constructor(
    private _activeModal: NgbActiveModal,
    private location: PlatformLocation,
    private _parserFormatter: NgbDateParserFormatter,
    private renderer: Renderer2,
    private _toastr: ToastrService,
    private _manageRateService: ManageRatesService
  ) {
    location.onPopState(() => this.closeModal(true));
  }

  ngOnInit() {

  }

  numberValidStatistic(event, data, maxValue) {

    try {
      const charCode = (event.which) ? event.which : event.keyCode
      const key = event.key
      if (key === '.') {
        event.preventDefault()
        return false
      }
      let isTrue = true
      const currentValue: string = event.target.value
      const newValue = currentValue + key
      // console.log(parseInt(newValue))
      if (isNumber(newValue) && parseInt(newValue) > maxValue) {
        event.preventDefault()
        return false
      }
      // console.log(event);

      // console.log('key: *' + key + '*', 'value:*' + currentValue + '*');
      // console.log(key === '.', currentValue.includes('.'))
      if (
        (charCode != 45 || currentValue.toString().includes('-')) && // “-” CHECK MINUS, AND ONLY ONE.
        (charCode != 46 || currentValue.toString().includes('.')) && // “.” CHECK DOT, AND ONLY ONE.
        (charCode < 48 || charCode > 57) ||
        key === '-' ||
        (key === '.' && currentValue.toString().includes('.')) ||
        ((key === '.' || key === '-') && !currentValue)
      ) {
        event.preventDefault()
        isTrue = false;
      }
      return isTrue
    } catch (error) {
      event.preventDefault()
      return false
    }
  }

  onDateSelection(date: NgbDateStruct) {
    let parsed = "";
    const _minDate = new Date(this.minDate.year, this.minDate.month - 1, this.minDate.day);
    const selectedDate = new Date(date.year, date.month - 1, date.day);
    if (!this.fromDate && !this.toDate) {
      if (moment(selectedDate).isSameOrAfter(_minDate)) {
        this.fromDate = date;
      } else {
        this.fromDate = null
        return
      }
    } else if (this.fromDate && !this.toDate && moment(selectedDate).isSameOrAfter(_minDate)) {
      const _fromDate = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
      if (moment(selectedDate).isSameOrAfter(_fromDate)) {
        this.toDate = date;
        this.input.close();
      }
    } else {
      const selectedDate = new Date(date.year, date.month - 1, date.day);
      if (moment(selectedDate).isSameOrAfter(_minDate)) {
        this.toDate = null;
        this.fromDate = date;
      } else {
        this.fromDate = null
        return
      }
    }
    if (this.fromDate) { parsed += this._parserFormatter.format(this.fromDate); }
    if (this.toDate) { parsed += " - " + this._parserFormatter.format(this.toDate); }
    this.renderer.setProperty(this.rangeDp.nativeElement, 'value', parsed);
  }

  closeModal(event) {
    this._activeModal.close(event);
    document.getElementsByTagName('html')[0].style.overflowY = 'auto';
  }

  onConfirmClick(event) {
    if (!this.departureDate) {
      this._toastr.warning('Please select Validity date.', 'Invalid Input.')
      return
    }
    const { departureDate } = this
    const selectedDate = new Date(departureDate.year, departureDate.month - 1, departureDate.day);
    const _toDate = moment(selectedDate);
    const _currentDate = moment(new Date());


    event.stopPropagation();

    const _toSend = {
      providerID: getLoggedUserData().ProviderID,
      // extendedDays: _toDate.diff(_currentDate, 'days') + 1,
      extendedDays: 0,
      extendedDate: selectedDate,
      increaseBaseRatePercentage: this.val1,
      increaseAdditionalRatePercentage: this.val2,
      beforeExpiryDay: 7,
      modifiedBy: getLoggedUserData().UserID
    }
    loading(true)
    if (this.mode === 'fcl') {
      this._manageRateService.updateAllFCLPublishExpiringRates(_toSend).subscribe((res: JsonResponse) => {
        loading(true)
        if (res.returnId > 0) {
          this._toastr.success(res.returnText, 'Success')
          this.closeModal(true)
        } else {
          this._toastr.error(res.returnText)
        }
      }, error => {
        loading(false)
      })
    } else {
      this._manageRateService.updateAllLCLPublishExpiringRates(_toSend).subscribe((res: JsonResponse) => {
        loading(true)
        if (res.returnId > 0) {
          this._toastr.success(res.returnText, 'Success')
          this.closeModal(true)
        } else {
          this._toastr.error(res.returnText)
        }
      }, error => {
        loading(false)
      })
    }
  }

  counter($action: string, $stat, $type: string) {
    try {
      let _statValue = 0
      if ($type === 'ad') {
        _statValue = this.val2
      } else {
        _statValue = this.val1
      }
      if ($action === 'add') {
        _statValue++
      } else if (_statValue > 1) {
        _statValue--
      }
      console.log(_statValue)
      if ($type === 'ad') {
        this.val2 = _statValue
      } else {
        this.val1 = _statValue
      }
    } catch (error) {
      console.log(error)
    }
  }
}