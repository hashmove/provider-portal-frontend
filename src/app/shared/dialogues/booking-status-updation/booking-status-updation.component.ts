import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ViewBookingService } from '../../../components/pages/user-desk/view-booking/view-booking.service';
import { HttpErrorResponse } from '@angular/common/http';
import { loading, getLoggedUserData, encryptBookingID } from '../../../constants/globalFunctions';
import { ToastrService } from "ngx-toastr";
import { PlatformLocation } from '@angular/common';
import { CommonService } from '../../../services/common.service';
import { JsonResponse } from '../../../interfaces/JsonResponse';
import * as moment from 'moment'
import { VesselScheduleDialogComponent } from '../vessel-schedule-dialog/vessel-schedule-dialog.component';
import { ConfirmDialogContent } from '../confirm-dialog-generic/confirm-dialog-generic.component';
import { TermsConditDialogComponent } from '../terms-condition/terms-condition.component';
import { isShareLogin } from '../../../http-interceptors/interceptor';
import { getDefaultHMUser } from '../../../services/jwt.injectable';
@Component({
  selector: 'app-booking-status-updation',
  templateUrl: './booking-status-updation.component.html',
  styleUrls: ['./booking-status-updation.component.scss']
})
export class BookingStatusUpdationComponent implements OnInit {
  @Input() modalData: any;
  public label: string;
  public description: string;
  public bookingReasons: BookingReason[] = [];
  public notifyReasons: any[] = [];
  public bookingStatuses: BookingStatus[] = [];
  public activityStatus: ActivityStatus[] = [];
  public actionObj: any;
  public notifyActionObj: any;
  public cancelledStatus: any;
  public selectPlaceholder: string;
  public selectedReason: any = {
    remarks: '',
    status: '',
    id: 0,
    code: ''
  };
  public userInfo

  public newHMRefNumber: string = ''
  carriersList = []
  btnLoader = false

  freightModes = ['SEA', 'AIR', 'TRUCK']
  specialStatus = ['ASSAYER', 'INSURANCE', 'WAREHOUSE']

  constructor(
    private _modalService: NgbModal,
    private _viewBookingService: ViewBookingService,
    private _toast: ToastrService,
    private location: PlatformLocation,
    private _commonService: CommonService,
    public _activeModal: NgbActiveModal) { location.onPopState(() => this.closeModal(null)); }

  ngOnInit() {
    console.log('');
    this.userInfo = !isShareLogin() ? getLoggedUserData() : getDefaultHMUser('')

    if (this.modalData.type === 'cancel') {
      if (this.modalData.booking.RateRequestID && this.modalData.booking.RateRequestID > 0 && this.modalData.booking.BookingTab && this.modalData.booking.BookingTab.toLowerCase() === 'specialrequest') {
        this.label = 'Discard Request'
        this.description = 'Are you sure you want to discard your request?'
        this.selectPlaceholder = 'Select Reason';
        this.getBookingReasons();
      } else {
        this.label = 'Cancel Booking'
        this.description = 'Please provide the reason of cancellation.'
        this.selectPlaceholder = 'Select Reason';
        this.getBookingReasons();
      }
    } else if (this.modalData.type === 'notify') {
      this.label = 'Notify Customer'
      this.description = 'Please provide the reason of notification.'
      this.selectPlaceholder = 'Select Reason';
      this.getNotifyReasons();
    } else if (this.modalData.type === 'updated') {
      this.label = 'Update Status'
      this.description = "Where's the Shipment?"
      this.selectPlaceholder = 'Select Status';
      this.getBookingStatuses('status');
    } else if (this.modalData.type === 'sub-status') {
      this.label = 'Update Status'
      this.description = "Where's the Shipment?"
      this.selectPlaceholder = 'Select Status';
      this.getBookingStatuses('sub-status');
    }
  }



  // getBookingReasons() {
  //   this._viewBookingService.getBookingReasons().subscribe((res: any) => {
  //     if (res.returnStatus == "Success") {
  //       this.bookingReasons = res.returnObject
  //     }
  //   }, (err: HttpErrorResponse) => {
  //     console.log(err);
  //   })
  // }

  async getBookingReasons() {
    try {
      let res: JsonResponse = null
      if (this.modalData.booking.RateRequestID && this.modalData.booking.RateRequestID > 0 && this.modalData.booking.BookingTab && this.modalData.booking.BookingTab.toLowerCase() === 'specialrequest') {
        res = await this._viewBookingService.getRateRequestReason().toPromise()
      } else {
        res = await this._viewBookingService.getBookingReasons().toPromise()
      }
      if (res.returnId === 1) {
        this.bookingReasons = res.returnObject.filter(e => e.BusinessLogic !== 'CANCELLED' && e.BusinessLogic !== 'EXPIRED')
      } else {
        this._toast.error('There was an error while updating your booking, please try later')
      }
    } catch (err) {
      this._toast.error('There was an error while updating your booking, please try later')
      console.warn(err.message)
    }
  }

  getNotifyReasons() {
    this._commonService.getMstCodeVal('NOTIFY_REASON').subscribe((res: any) => {
      console.log(res)
      this.notifyReasons = res
    }, (err: any) => {
      console.log(err)
    })
  }

  getBookingStatuses(status) {
    if (status === 'status') {
      const _shipMode = this.modalData.booking.ShippingModeCode
      const toSend: string = (this.specialStatus.includes(_shipMode)) ? _shipMode : 'BOOKING'
      this._viewBookingService.getBookingStatuses(toSend).subscribe((res: JsonResponse) => {
        if (res.returnId > 0) {
          const data = res.returnObject.filter(e => e.BusinessLogic.toLowerCase() !== 'cancelled' && e.BusinessLogic.toLowerCase() !== 'draft' && e.BusinessLogic !== 'EXPIRED');
          this.bookingStatuses = data.filter(e => e.BusinessLogic.toLowerCase() !== this.modalData.bookingStatus.toLowerCase())
          if (this.modalData.booking.ShippingModeCode === 'ASSAYER') {
            this.bookingStatuses = this.bookingStatuses.filter(e => e.BusinessLogic.toLowerCase() !== 'in-transit')
          }
        }
      }, () => {
        loading(false)
        this.btnLoader = false;
      })
    } else if ('sub-status') {
      this._viewBookingService.getBookingSubStatuses(this.modalData.booking.ShippingModeCode, this.modalData.booking.StatusID).subscribe((res: JsonResponse) => {
        if (res.returnId > 0)
          this.activityStatus = res.returnObject
      }, () => {
        loading(false)
        this.btnLoader = false;
      })
    }
  }

  onModelChange(model: any) {
    // console.log(model.target.value);
    if (model) {
      this.selectedReason.id = parseInt(model.target.value)
      if (this.modalData.type === 'updated') {
        const status = this.bookingStatuses.filter(s => s.StatusID == this.selectedReason.id)[0]
        const { StatusName, StatusID, BusinessLogic } = status
        this.selectedReason = {
          id: StatusID,
          remarks: '',
          status: StatusName,
          code: BusinessLogic
        }
        // console.log(this.selectedReason);
      } else if (this.modalData.type === 'sub-status') {
        const status = this.activityStatus.filter(s => s.ActivityID == this.selectedReason.id)[0]
        const { ActivityName, ActivityID, BusinessLogic } = status
        this.selectedReason = {
          id: ActivityID,
          remarks: '',
          status: ActivityName,
          code: BusinessLogic
        }
        // console.log(this.selectedReason);
      } else if (this.modalData.type === 'notify') {
        const status = this.notifyReasons.filter(s => s.codeValID == this.selectedReason.id)[0]
        const { codeValID, codeValDesc } = status
        this.selectedReason = {
          id: codeValID,
          remarks: '',
          status: codeValDesc
        }
        // console.log(this.selectedReason);
      } else {
        const status = this.bookingReasons.filter(s => s.ReasonID == this.selectedReason.id)[0]
        const { ReasonID, ReasonName } = status
        this.selectedReason = {
          id: ReasonID,
          remarks: '',
          status: ReasonName
        }
        // console.log(this.selectedReason);
      }
    }
  }

  submit() {

    if (this.modalData.type === 'cancel') {
      if (this.modalData.booking.BookingTab && this.modalData.booking.BookingTab.toLowerCase() === 'specialrequest') {
        this.cancelRequest()
      } else {
        const { id, remarks, status } = this.selectedReason;
        if (id && remarks) {
          this.actionObj = {
            bookingID: this.modalData.bookingID,
            bookingStatus: "CANCELLED",
            bookingStatusRemarks: remarks,
            createdBy: this.userInfo.UserID,
            modifiedBy: this.userInfo.UserID,
            approverID: this.modalData.providerID,
            approverType: 'PROVIDER',
            reasonID: id,
            reasonText: status,
            providerName: this.modalData.booking.ProviderName,
            emailTo: (this.modalData.booking && this.modalData.booking.BookingUserInfo && this.modalData.booking.BookingUserInfo.PrimaryEmail) ? this.modalData.booking.BookingUserInfo.PrimaryEmail : '',
            phoneTo: (this.modalData.booking && this.modalData.booking.BookingUserInfo && this.modalData.booking.BookingUserInfo.PrimaryPhone) ? this.modalData.booking.BookingUserInfo.PrimaryPhone : '',
            userName: this.modalData.booking.UserName,
            hashMoveBookingNum: this.modalData.booking.HashMoveBookingNum,
            userCountryPhoneCode: this.modalData.booking.UserCountryPhoneCodeID,
            bookingType: this.modalData.booking.ShippingModeCode,
            currentBookingStatus: this.modalData.booking.BookingStatusBL,
            companyID: this.modalData.booking.BookingUserInfo ? this.modalData.booking.BookingUserInfo.CompanyID : 0
          }
          this._viewBookingService.cancelBooking(this.actionObj).subscribe((res: any) => {
            if (res.returnId > 0) {
              this._toast.success(res.returnText, 'Success', { enableHtml: true });
              let obj = {
                bookingStatus: res.returnObject.bookingStatus,
                bookingStatusCode: res.returnObject.bookingStatusCode,
                shippingStatus: res.returnObject.shippingStatus,
                shippingStatusCode: res.returnObject.shippingStatus,
                resType: res.returnStatus
              }
              this.closeModal(obj);
            } else {
              // if (res.returnCode === '2') {
              //   this._toast.info(res.returnText, res.returnStatus, { enableHtml: true });
              //   this.closeModal(false);
              // } else {
              this._toast.error(res.returnText, 'Failed');
              // this.closeModal(false);
              // }
            }
          }, (err: HttpErrorResponse) => {
            console.log(err);
          })
        }
      }
    } else if (this.modalData.type === 'updated') {
      loading(true)
      this.btnLoader = true
      const { booking } = this.modalData
      if (this.selectedReason.status.toLowerCase() === 'confirmed' && booking.BookingSourceVia && booking.BookingSourceVia.includes('ENTERPRISE')) {
        const _id = encryptBookingID(booking.BookingID, this.userInfo.UserID, booking.ShippingModeCode, 'GUEST')
        this._commonService.getHMCommissionTermsCondition((_id)).subscribe((res: JsonResponse) => {
          loading(false)
          this.btnLoader = false
          if (res.returnId > 0) {
            const _modalData: ConfirmDialogContent = {
              messageTitle: 'Confirm',
              messageContent: `Are you sure you want to confirm?`,
              buttonTitle: 'Yes',
              data: { termsCondition: res.returnText, action: 'action' }
            }
            const modalRef = this._modalService.open(TermsConditDialogComponent, {
              size: 'lg',
              centered: true,
              windowClass: 'large-modal',
              backdrop: 'static',
              keyboard: false
            });
            modalRef.result.then((result: string) => {
              if (result) { this.updateStatusAction() }
            })
            modalRef.componentInstance.modalData = _modalData;
            setTimeout(() => {
              if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
                document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
              }
            }, 0);
          } else {
            this.updateStatusAction()
          }
        }, error => {
          loading(false)
          this.btnLoader = false
        })
      } else {
        this.updateStatusAction()
      }


    } else if (this.modalData.type === 'notify') {
      const { id, remarks, status } = this.selectedReason;
      if (id && remarks) {
        this.notifyActionObj = {
          bookingID: this.modalData.bookingID,
          userID: this.modalData.booking.UserID,
          providerID: this.userInfo.ProviderID ? this.userInfo.ProviderID : null,
          userName: this.modalData.booking.UserName,
          userCompanyName: this.modalData.booking.BookingUserInfo.CompanyName,
          providerName: this.userInfo.FirstNameBL ? this.userInfo.FirstNameBL + ' ' + this.userInfo.LastNameBL : null,
          providerCompanyName: this.modalData.booking.ProviderName,
          hashMoveBookingNum: this.modalData.booking.HashMoveBookingNum,
          reasonText: status,
          reasonRemarks: remarks,
          userEmail: (this.modalData.booking && this.modalData.booking.BookingUserInfo && this.modalData.booking.BookingUserInfo.PrimaryEmail) ? this.modalData.booking.BookingUserInfo.PrimaryEmail : '',
          providerEmail: this.modalData.booking.ProviderEmail
        }
        this._viewBookingService.notifyCustomer(this.notifyActionObj).subscribe((res: any) => {
          if (res.returnId > 0) {
            this.closeModal('notify');
          } else {
            this.btnLoader = false
            this._toast.error(res.returnText, 'Failed');
          }
        }, (err: HttpErrorResponse) => {
          this.btnLoader = false
          console.log(err);
        })
      }
    } else if (this.modalData.type === 'sub-status') {
      const { status, id, remarks } = this.selectedReason;
      if (remarks && remarks.length > 100) {
        this._toast.warning('The field Booking Activity Remarks must be a string or array type with a maximum length of 100.', 'Warning')
        this.btnLoader = false
        return
      }
      if (status && id) {
        const toSend = {
          bookingID: this.modalData.bookingID,

          activityBy: "PROVIDER",
          activityByID: this.modalData.providerID,
          statusID: this.modalData.booking.StatusID, //booking status
          activityID: id,
          bookingActivity: status,
          bookingActivityDate: "2019-08-07T10:46:37.616Z",
          bookingActivityRemarks: remarks,
          createdBy: this.userInfo.UserID,
          modifiedBy: this.userInfo.UserID,
          approverID: this.modalData.providerID,
          approverType: 'PROVIDER',
          providerName: this.modalData.booking.ProviderName,
          emailTo: (this.modalData.booking && this.modalData.booking.BookingUserInfo && this.modalData.booking.BookingUserInfo.PrimaryEmail) ? this.modalData.booking.BookingUserInfo.PrimaryEmail : '',
          phoneTo: (this.modalData.booking && this.modalData.booking.BookingUserInfo && this.modalData.booking.BookingUserInfo.PrimaryPhone) ? this.modalData.booking.BookingUserInfo.PrimaryPhone : '',
          userName: this.modalData.booking.UserName,
          hashMoveBookingNum: this.modalData.booking.HashMoveBookingNum,
          userCountryPhoneCode: this.modalData.booking.UserCountryPhoneCodeID,

        }
        this._viewBookingService.updateBookingActivity(toSend).subscribe((res: any) => {
          if (res.returnId > 0) {
            this._toast.success(res.returnText, 'Success', { enableHtml: true });
            const prasedObject = JSON.parse(res.returnObject)
            let obj = {
              activityStatus: prasedObject.BookingActivity,
              resType: res.returnStatus,
              modalType: this.modalData.type
            }
            this.closeModal(obj);
          } else {
            this.btnLoader = false
            this._toast.error(res.returnText, 'Failed');
          }
        }, (err: HttpErrorResponse) => {
          this.btnLoader = false
          console.log(err);
        })
      }
    }
  }

  cancelRequest() {
    const _isParent = this.modalData.booking.HMParentBookingNum ? true : false
    let _customerID = null
    try {
      _customerID = this.modalData.booking.CustomerID
    } catch { }

    const { id, remarks } = this.selectedReason;
    const _toSend = {
      RateRequestID: null,
      RequestStatusBL: 'DECLINED',
      RequestStatusRemarks: remarks,
      ReasonID: parseInt(id),
      RequestNo: _isParent ? this.modalData.booking.HMParentBookingNum : null,
      RequestType: this.modalData.booking.ShippingModeCode,
      RequestCompanyID: _customerID,
      RequestUserID: this.modalData.booking.UserID,
      isCancelByProvider: true,
      BookingID: _isParent ? null : this.modalData.booking.BookingID
    }
    this.btnLoader = true
    this._viewBookingService.discardRequest(getLoggedUserData().UserID, _toSend).subscribe((res: any) => {
      this.btnLoader = false
      if (res.returnId > 0) {
        this._activeModal.close(true)
        this._toast.success(res.returnText, 'Success')
      } else {
        this._toast.error('There was an error while updating your booking, please try later')
      }
    }, (err) => {
      this.btnLoader = false
      console.warn(err.message)
      this._toast.error('There was an error while updating your booking, please try later')
    })

  }

  updateStatusAction() {
    const { booking } = this.modalData
    if (booking.ShippingModeCode === 'AIR' && booking.CarrierCode === 'DEFAULT') {
      this._toast.warning('Please select a valid Airline', 'Invalid Airline')
      loading(false)
      this.btnLoader = false
      return
    }
    if (
      (booking.ContainerLoad === 'FCL' && booking.ShippingModeCode === 'SEA' && (!booking.EtdUtc || booking.VesselName.toLowerCase() === 'upon booking' || booking.CarrierCode === 'DEFAULT') && this.selectedReason.code === 'CONFIRMED') ||
      (booking.ContainerLoad === 'LCL' && booking.ShippingModeCode === 'SEA' && (!booking.CarrierName || !booking.VesselName || booking.VesselName.length === 0) && this.selectedReason.code === 'IN-TRANSIT')
    ) {
      this.getCarrierSchedule('details')
    } else {
      this.updateBookingAction()
    }
  }

  updateBookingAction() {
    const { status, id, code, remarks } = this.selectedReason;
    if (status && id) {
      let hmBookingNumber = this.modalData.booking.HashMoveBookingNum
      if (this.modalData.booking.IsManualBookingNumberAllowed && status.toLowerCase() === 'confirmed') {
        if ((this.newHMRefNumber && this.newHMRefNumber.length > 0 && this.newHMRefNumber.length <= 25)) {
          try {
            hmBookingNumber = this.newHMRefNumber
          } catch (error) {
            hmBookingNumber = this.modalData.booking.BookingStatus
          }
        } else {
          this._toast.warning('Your Booking Number is mandatory & should be less than 25 character')
          this.btnLoader = false
          return
        }

      }

      let _custId = -1
      try {
        const _searchCriteria = JSON.parse(this.modalData.booking.JsonSearchCriteria)
        _custId = _searchCriteria.CustomerID
      } catch (error) {
      }

      let _curBookStatus = this.modalData.booking.BookingStatusBL
      // let _curBookStatus = this.modalData.booking.BookingStatus
      // try {
      //   const { BookingStatus, ShippingStatus, ShippingModeCode } = this.modalData.booking
      //   if (this.freightModes.includes(ShippingModeCode) && BookingStatus.toLowerCase() === 'confirmed' && ShippingStatus.toLowerCase() === 'in-transit') {
      //     _curBookStatus = ShippingStatus
      //   }
      // } catch { }
      this.actionObj = {
        bookingID: this.modalData.bookingID,
        bookingStatus: code,
        bookingStatusRemarks: remarks,
        createdBy: this.userInfo.UserID,
        modifiedBy: this.userInfo.UserID,
        approverID: this.modalData.providerID,
        approverType: 'PROVIDER',
        // reasonID: id,
        // reasonText: status,
        providerName: this.modalData.booking.ProviderName,
        emailTo: (this.modalData.booking && this.modalData.booking.BookingUserInfo && this.modalData.booking.BookingUserInfo.PrimaryEmail) ? this.modalData.booking.BookingUserInfo.PrimaryEmail : '',
        phoneTo: (this.modalData.booking && this.modalData.booking.BookingUserInfo && this.modalData.booking.BookingUserInfo.PrimaryPhone) ? this.modalData.booking.BookingUserInfo.PrimaryPhone : '',
        userName: this.modalData.booking.UserName,
        hashMoveBookingNum: hmBookingNumber,
        userCountryPhoneCode: this.modalData.booking.UserCountryPhoneCodeID,
        bookingType: this.modalData.booking.ShippingModeCode,
        currentBookingStatus: _curBookStatus,
        providerID: this.modalData.booking.ProviderID,
        companyID: _custId
      }
      loading(true)
      this.btnLoader = true
      this._viewBookingService.updateBookingStatus(this.actionObj).subscribe((res: JsonResponse) => {
        loading(false)
        this.btnLoader = false
        if (res.returnId > 0) {
          if (res.returnCode === "2") {
            this._toast.info(res.returnText, 'Info', { enableHtml: true });
          } else {
            this._toast.success(res.returnText, 'Success', { enableHtml: true });
          }
          let obj = {
            bookingStatus: res.returnObject.bookingStatus,
            bookingStatusCode: res.returnObject.bookingStatusCode,
            shippingStatus: res.returnObject.shippingStatus,
            shippingStatusCode: res.returnObject.shippingStatus,
            statusID: res.returnObject.statusID,
            hashMoveBookingNum: res.returnObject.hashMoveBookingNum,
            resType: res.returnStatus,
          }
          this.closeModal(obj);
        } else {
          this.btnLoader = false
          this._toast.error(res.returnText, 'Failed');
        }
      }, (err: HttpErrorResponse) => {
        loading(false)
        this.btnLoader = false
        console.log(err);
      })
    }
  }
  closeModal(resType) {
    this._activeModal.close(resType);
    document.getElementsByTagName('html')[0].style.overflowY = 'auto';

  }

  /**
 *
 * Getting Shipping Lines
 * @param {object} reason
 * @memberof ViewBookingComponent
 */
  getCarrierSchedule(reason) {
    let data = null
    const { booking } = this.modalData
    try {
      data = {
        BookingID: booking.BookingID,
        PolID: booking.PolID,
        PodID: booking.PodID,
        ContainerLoadType: booking.ContainerLoad,
        ResponseType: reason,
        CarrierID: 0,
        fromEtdDate: booking.EtdUtc,
        toEtdDate: booking.EtdUtc,
      }
      if (!data.fromEtdDate) {
        const _date = moment.utc(new Date()).format()
        data.fromEtdDate = _date
      }
      if (!data.toEtdDate) {
        const _date = moment.utc(data.fromEtdDate).add(1, 'month').format()
        data.toEtdDate = _date
      }
    } catch (error) {
      loading(false)
      this.btnLoader = false
    }
    this._viewBookingService.getCarrierSchedule(data).subscribe((res: any) => {
      // console.log(res)
      loading(false)
      this.btnLoader = false
      if (reason === 'details') {
        this.openVesselSchedule(res.returnObject)
      } else {
        this.carriersList = res.returnObject;
      }
    }, (err: any) => {
      loading(false)
      this.btnLoader = false
      console.log(err)
    })
  }

  openVesselSchedule(data) {
    const { booking, searchCarrierList } = this.modalData
    const modalRef = this._modalService.open(VesselScheduleDialogComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'large-modal',
      backdrop: 'static',
      keyboard: false
    })
    modalRef.componentInstance.data = {
      bookingID: booking.BookingID,
      date: new Date(),
      schedules: data,
      bookingDetails: booking,
      carriersList: searchCarrierList,
      from: 'VIEW_BOOKING'
    }
    modalRef.result.then(result => {
      if (result) {
        try {
          this.updateBookingAction()
          booking.VoyageRefNum = result.voyageRefNo
          booking.VesselName = result.vesselName
          booking.VesselCode = result.vesselCode
          booking.CarrierName = result.carrierName
          booking.CarrierCode = result.carrierCode ? result.carrierCode : result.carrierName
          booking.CarrierImage = result.carrierImage
          booking.PortCutOffLcl = result.portCutOffDate
          booking.EtdLcl = result.etdDate
          try { booking.TransitTime = result.transitDays } catch (err) { console.log(err) }
          this._toast.success('Update Successful');
        } catch { }
      }
    })
  }


}

export interface BookingStatus {
  BusinessLogicGroup?: string;
  BusinessLogic?: string;
  StatusCode?: string;
  StatusID?: number;
  StatusName?: string;
}

export interface ActivityStatus {
  ActivityID: 106;
  ActivityCode: string;
  ActivityName: string;
  BusinessLogic: string;
}

export interface BookingReason {
  BusinessLogic?: string;
  ReasonCode?: string;
  ReasonID?: number;
  ReasonName?: string;
  SortingOrder?: string;
}
