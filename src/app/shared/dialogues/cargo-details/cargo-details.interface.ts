export interface SearchCriteriaContainerDetail {
    contSpecID: number;
    contRequestedQty: number;
    contRequestedCBM: number;
    volumetricWeight: number;
    contRequestedWeight: number;
    containerCode: string;
    IsTrackingApplicable: boolean;
    IsQualityApplicable: boolean;
    ContainerSpecDesc: string;
  }
  
  export interface LclChip {
    bookingContTypeQty?: number;
    containerSpecDesc?: string;
    containerSpecImage?: string;
    quantity: number;
    lengthUnitID: number;
    weightUnitID: number;
    volumeUnitID: number;
    length: any;
    width: any;
    height: any;
    weight: string;
    id: number;
    volume: number;
    packageType: string;
    contSpecID: number;
    lengthUnit: string;
    weightUnit: string;
    contRequestedQty: number;
    toggle: boolean;
    contRequestedCBM: number;
    contRequestedWeight: number;
    volumetricWeight: number;
    inpVolume?: number;
    inpTotalWeight?: number;
  }
  
  export interface ISearchContainers {
    SearchCriteriaContainerDetail: SearchCriteriaContainerDetail[];
    LclChips: LclChip[];
  }