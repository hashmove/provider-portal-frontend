import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { encryptBookingID, feet2String } from '../../../constants/globalFunctions';
import { DashboardService } from '../../../components/pages/user-desk/dashboard/dashboard.service';
import { CurrencyControl } from '../../../services/currency.service';
import { ISearchContainers, LclChip } from './cargo-details.interface';

@Component({
  selector: 'app-cargo-details',
  templateUrl: './cargo-details.component.html',
  styleUrls: ['./cargo-details.component.scss']
})
export class CargoDetailsComponent implements OnInit {
  @Input() data: any
  @Input() containerCount: any
  public containers: any[] = []
  totalCBM = 0
  totalWeight = 0
  searchMode = 'sea-fcl'
  commodity = ''
  lclContainers: LclChip[] = []
  lclAirContainerChips: any[] = []
  IsAdmin: boolean = false
  editPaymentTerms: boolean = false
  paymentTerms = null
  public editorContentAgent: any;
  public editorContentConsignee: any;
  private toolbarOptions = [
    ['bold', 'italic', 'underline'],        // toggled buttons
    [{ 'list': 'ordered' }, { 'list': 'bullet' }],
    [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
    [{ 'direction': 'rtl' }],                         // text direction
    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
    [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
    [{ 'align': [] }],
    ['clean']                                         // remove formatting button
  ];
  public editorOptions = {
    placeholder: "insert content...",
    modules: {
      toolbar: this.toolbarOptions
    }
  };
  pickupDate = ''

  constructor(
    public _activeModal: NgbActiveModal,
    private _dashboardService: DashboardService,
    private _currencyControl: CurrencyControl
  ) { }



  close() {
    this._activeModal.close()
  }


  ngOnInit() {
    this.getPaymentTerms()
    try {
      const { LclChips, SearchCriteriaContainerDetail, lclAirContainerChips, pickupDate } = JSON.parse(this.data.JsonSearchCriteria)
      this.lclAirContainerChips = lclAirContainerChips
      this.pickupDate = pickupDate
      if (LclChips && LclChips.length > 0) {
        this.lclContainers = LclChips.map((_lclChip, _index) => {
          const _containers = SearchCriteriaContainerDetail
            .filter(_container => _container.contSpecID === _lclChip.contSpecID)
          return {
            ..._lclChip,
            bookingContTypeQty: _lclChip.quantity,
            containerSpecDesc: _containers.length > 0 ? _containers[0].ContainerSpecDesc : '',
            containerSpecImage: this.getImageName(_lclChip.packageType)
          }
        })
      }
    } catch (error) {
      console.log(error)
    }
    try {
      this.commodity = this.data.BookingDesc
    } catch { }
    try {
      this.searchMode = this.data.ShippingModeCode.toLowerCase() + '-' + this.data.ContainerLoad.toLowerCase()
    } catch (ex) {
      console.log(ex)
    }
    this.getContainerDetails()
  }

  getPaymentTerms() {
    this._dashboardService.getPaymentTerms(encryptBookingID(this.data.BookingID, this.data.UserID, this.data.ShippingModeCode)).subscribe(res => {
      // this.paymentTerms
      console.log(res)
    })
  }

  getContainerDetails() {
    let id = encryptBookingID(this.data.BookingID, this.data.UserID, this.data.ShippingModeCode);
    this._dashboardService.getContainerDetails(id, 'CUSTOMER').subscribe((res: any) => {
      this.containers = res.returnObject
      const { containers } = this
      if (this.data.ContainerLoad === 'LCL' && !this.data.IsVirtualAirLine) {
        let _cbm = 0, _weight = 0
        try {
          containers.forEach(obj => {
            if (obj.bookingPkgTypeCBM) {
              _cbm += Number(obj.bookingPkgTypeCBM)
              try { _weight += Number(obj.bookingPkgTypeWeight) } catch { }
            } else {
              _cbm += Number(obj.BookingContTypeQty)
            }
          })
        } catch { }
        try {
          if (_cbm < 1) {
            _cbm = Math.ceil(_cbm)
          } else {
            _cbm = this._currencyControl.applyRoundByDecimal(_cbm, 3)
          }
          if (_weight < 1) {
            _weight = Math.ceil(_weight)
          } else {
            _weight = this._currencyControl.applyRoundByDecimal(_cbm, 3)
          }
        } catch {
        }
        this.totalCBM = _cbm
        if (this.searchMode === 'sea-lcl') {
          const _searchCriteria = JSON.parse(this.data.JsonSearchCriteria)
          try {
            this.totalWeight = _searchCriteria.totalVolumetricWeight
          } catch {
            this.totalWeight = 0
          }
        } else {
          this.totalWeight = _weight
        }
      }
    }, (err) => {
      console.log(err)
    })
  }

  getContainerInfo(container) {
    let containerInfo = {
      containerSize: undefined,
      containerWeight: undefined
    };
    containerInfo.containerWeight = container.MaxGrossWeight;
    containerInfo.containerSize =
      feet2String(container.containerLength) +
      ` x ` +
      feet2String(container.containerWidth) +
      ` x ` +
      feet2String(container.containerHeight);
    return containerInfo;
  }

  getImageName($code: string) {
    switch ($code) {
      case 'PLTS':
        return 'Pallets.svg'
      case 'BOXS':
        return 'Boxes.svg'
      case 'DRUM':
        return 'Drums.svg'
      case 'ASMT':
        return 'AssortedShipment.svg'
      default:
        return 'Pallets.svg'
    }
  }


  onEditorBlured(quill) {
  }

  onEditorFocused(quill) {
  }

  onEditorCreated(quill) {
  }

  onContentChanged($event) {
    this.paymentTerms = $event.html
  }

  changePaymentTerm() {
    this.editPaymentTerms = true
  }

  saveTerms() {
    this.editPaymentTerms = false
  }
}