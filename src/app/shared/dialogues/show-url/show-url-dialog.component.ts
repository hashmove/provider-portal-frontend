import { Component, OnInit, Input } from "@angular/core";
import { Router } from "@angular/router";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { PlatformLocation } from "@angular/common";
import { UserCreationService } from "../../../components/pages/user-creation/user-creation.service";
import { SharedService } from "../../../services/shared.service";
import { HttpErrorResponse } from "@angular/common/http";

// import { HashStorage, Tea } from '../../../constants/globalfunctions';
// import { DataService } from '../../../services/commonservice/data.service';
// import { AuthService } from '../../../services/authservice/auth.service';
@Component({
  selector: "app-show-url",
  templateUrl: "./show-url-dialog.component.html",
  styleUrls: ["./show-url-dialog.component.scss"]
})
export class ShowUrlDialogComponent implements OnInit {
  public loading: boolean = false;

  @Input() portalURL = ''
  @Input() userName = ''

  constructor(
    private _router: Router,
    private _activeModal: NgbActiveModal,
    private _sharedService: SharedService,
    private location: PlatformLocation,
  ) {
    location.onPopState(() => this.closeModal());
  }

  ngOnInit() { }

  closeModal() {
    this._activeModal.close();
    this.loading = true
    document.getElementsByTagName("html")[0].style.overflowY = "auto";
  }
  onConfirmClick() {
    this.closeModal()
  }
}
