import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SeaFreightService } from '../../../components/pages/user-desk/manage-rates/sea-freight/sea-freight.service';
import { baseExternalAssets } from '../../../constants/base.url';
import { GroundTransportService } from '../../../components/pages/user-desk/manage-rates/ground-transport/ground-transport.service';
import { AirFreightService } from '../../../components/pages/user-desk/manage-rates/air-freight/air-freight.service';
import { loading, getImagePath, ImageSource, ImageRequiredSize, getProviderImage, getLoggedUserData } from '../../../constants/globalFunctions';
import { ManageRatesService } from '../../../components/pages/user-desk/manage-rates/manage-rates.service';
import { HttpErrorResponse } from '@angular/common/http';
import { WarehouseService } from '../../../components/pages/user-desk/manage-rates/warehouse-list/warehouse.service';
import { UserInfo } from '../../../interfaces/billing.interface';
@Component({
  selector: 'app-rate-history',
  templateUrl: './rate-history.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./rate-history.component.scss']
})
export class RateHistoryComponent implements OnInit {
  @Input() getRecord: any;
  public loading: boolean = false
  public baseExternalAssets: string = baseExternalAssets;
  public userProfile: UserInfo;
  public history: any;
  public destinationDet: any;
  public originDet: any;
  public shippingInfo: any = [];
  public containerInfo: any;
  public cargoInfo: any;
  public customerInfo: any;
  combinedContainers: any;
  fclContainers: any;
  allCargoType: any;
  historyHead: any;
  groundPorts: any;
  historyImage: string = null;
  constructor(
    private _activeModal: NgbActiveModal,
    private _seaFreightService: SeaFreightService,
    private _airFreightService: AirFreightService,
    private _groundTransportService: GroundTransportService,
    private _manageRateService: ManageRatesService,
    private _warehouseService: WarehouseService
  ) { }

  ngOnInit() {
    this.userProfile = getLoggedUserData();
    this.getLists()
    if (this.getRecord.type && this.getRecord.type == 'Rate_FCL') {
      this.getHistoryFCL();
    }
    else if (this.getRecord.type && this.getRecord.type == 'Rate_LCL') {
      this.getHistoryLCL();
    } else if (this.getRecord.type && this.getRecord.type == 'Rate_WH') {
      this.getHistoryWarehouse();
    }
    else if (this.getRecord.type && this.getRecord.type == 'Rate_AIR') {
      this.getHistoryAir();
    }
    else if (this.getRecord.type && this.getRecord.type != 'Rate_LCL' && this.getRecord.type != 'Rate_FCL' && this.getRecord.type != 'Rate_AIR') {
      this.getHistoryGround();
    }
  }

  getLists() {
    this.combinedContainers = JSON.parse(localStorage.getItem('containers'))
  }

  getHistoryFCL() {
    this.loading = true
    this._seaFreightService.getRecHistoryFCL(this.getRecord.id, this.getRecord.type, this.userProfile.UserID).subscribe((res: any) => {
      if (res.returnStatus == "Success") {
        this.loading = false
        let records = res.returnObject;
        if (records.history && records.history.length) {
          records.history.map(obj => {
            if (typeof (obj.AuditDesc) == "string") {
              obj.AuditDesc = JSON.parse(obj.AuditDesc);
            }
          })
          this.history = records.history;
        }
        this.historyHead = records.historyHead;
        this.shippingInfo = this.getRecord.shippingLines.filter(e => e.id === this.historyHead[0].carrierID)
        const containers = JSON.parse(localStorage.getItem('containers'))
        this.containerInfo = containers.find(e => e.ContainerSpecID === this.historyHead[0].containerSpecID && e.ContainerFor === 'FCL')
        console.log(this.containerInfo)
        this.cargoInfo = this.historyHead[0].shippingCatName
        this.destinationDet = this.getRecord.ports.find(obj => obj.PortID === this.historyHead[0].podID);
        this.originDet = this.getRecord.ports.find(obj => obj.PortID === this.historyHead[0].polID);
      }
    })
  }

  getHistoryLCL() {
    this.loading = true
    this._seaFreightService.getRecHistoryLCL(this.getRecord.id, this.getRecord.type, this.userProfile.UserID).subscribe((res: any) => {
      this.loading = false
      if (res.returnStatus == "Success") {
        let records = res.returnObject;
        if (records.history && records.history.length) {
          records.history.map(obj => {
            if (typeof (obj.AuditDesc) == "string") {
              obj.AuditDesc = JSON.parse(obj.AuditDesc);
            }
          })
          this.history = records.history;
        }
        this.historyHead = records.historyHead;
        this.shippingInfo = this.getRecord.shippingLines.filter(e => e.id === this.historyHead[0].carrierID)
        this.cargoInfo = this.historyHead[0].shippingCatName
        this.destinationDet = this.getRecord.ports.find(obj => obj.PortID === this.historyHead[0].podID);
        this.originDet = this.getRecord.ports.find(obj => obj.PortID === this.historyHead[0].polID);
      }
    })
  }

  getHistoryAir() {
    this.loading = true
    this._airFreightService.getRecHistory(this.getRecord.id, this.getRecord.type, this.userProfile.UserID).subscribe((res: any) => {
      this.loading = false
      if (res.returnStatus == "Success") {
        let records = res.returnObject;
        if (records.history && records.history.length) {
          records.history.map(obj => {
            if (typeof (obj.AuditDesc) == "string") {
              obj.AuditDesc = JSON.parse(obj.AuditDesc);
              obj.AuditDescDetailed = obj.AuditDesc.Desc.split(";")
              if ((this.getRecord.type === 'Rate_AIR' || this.getRecord.type === 'Rate_WH') && !obj.AuditDesc.Desc.toLowerCase().includes('by')) {
                obj.AuditDescDetailed.push(`Rate updated by ${obj.AuditDesc.ModifiedBy}`)
              }
              obj.Activity = obj.AuditDesc.Activity.split(";")
            }
          })
          this.history = records.history;
        }
        this.historyHead = records.historyHead;
        this.shippingInfo = this.getRecord.shippingLines.filter(e => e.id === this.historyHead[0].carrierID)
        this.cargoInfo = this.historyHead[0].shippingCatName
        this.destinationDet = this.getRecord.ports.find(obj => obj.PortID === this.historyHead[0].podID);
        this.originDet = this.getRecord.ports.find(obj => obj.PortID === this.historyHead[0].polID);
      }
    })
  }

  whName: string = ''
  whAddress: string = ''
  whImage: string = ''

  getHistoryWarehouse() {
    this.loading = true
    this._warehouseService.getWarehouseRecHistory(this.getRecord.id, this.getRecord.type, this.userProfile.UserID).subscribe((res: any) => {
      this.loading = false
      if (res.returnStatus == "Success") {
        const records: any = res.returnObject;
        if (records.history && records.history.length) {
          records.history.map(obj => {
            if (typeof (obj.AuditDesc) == "string") {
              obj.AuditDesc = JSON.parse(obj.AuditDesc);
              obj.AuditDescDetailed = obj.AuditDesc.Desc.split(";")
              obj.Activity = obj.AuditDesc.Activity.split(";")
            }
          })
          this.history = records.history;
          try {
            this.whName = records.historyHead[0].whName
            this.whAddress = records.historyHead[0].whAddress
            const _whImage = JSON.parse(records.historyHead[0].whImage)
            const whDeep = JSON.parse(_whImage[0].Value)
            this.whImage = baseExternalAssets + '/' + whDeep.DocumentFile
          } catch (error) {
            console.log(error)
          }
        }
        this.historyHead = records.historyHead;
        // this.shippingInfo = this.getRecord.shippingLines.filter(e => e.id === this.historyHead[0].carrierID)
        // this.cargoInfo = this.historyHead[0].shippingCatName
        // this.destinationDet = this.getRecord.ports.find(obj => obj.PortID === this.historyHead[0].podID);
        // this.originDet = this.getRecord.ports.find(obj => obj.PortID === this.historyHead[0].polID);
        console.log(this.history)
      }
    })
  }

  getHistoryGround() {
    this.loading = true
    this.getGroundPorts()
    this._groundTransportService.getRecHistoryGround(this.getRecord.id, 'RATE_TRUCK', this.userProfile.UserID, this.getRecord.transportType).subscribe((res: any) => {
      this.loading = false
      if (res.returnStatus == "Success") {
        let records = res.returnObject;
        if (records.history && records.history.length) {
          records.history.map(obj => {
            if (typeof (obj.AuditDesc) == "string") {
              obj.AuditDesc = JSON.parse(obj.AuditDesc);
            }
          })
          this.history = records.history;
        }
        this.historyHead = records.historyHead;
        const containers = JSON.parse(localStorage.getItem('containers'))
        this.containerInfo = containers.find(e => e.ContainerSpecID === this.historyHead[0].containerSpecID && e.ContainerFor === 'FTL')
        console.log(this.containerInfo)
        this.cargoInfo = this.historyHead[0].shippingCatName
        const ports = JSON.parse(localStorage.getItem('PortDetails'))

        if (this.historyHead[0].polType === 'CITY') {
          this.originDet = {
            polType: this.historyHead[0].polType,
            polName: this.historyHead[0].polName,
            polImageName: this.historyHead[0].polImageName
          }
        }

        if (this.historyHead[0].podType === 'CITY') {
          this.destinationDet = {
            podType: this.historyHead[0].podType,
            podName: this.historyHead[0].podName,
            podImageName: this.historyHead[0].podImageName,
          }
        }

        if (this.historyHead[0].podType === 'Ground') {
          this.destinationDet = this.groundPorts.find(obj => obj.PortID === this.historyHead[0].podID);
        } else if (this.historyHead[0].podType === 'SEA') {
          this.destinationDet = ports.find(obj => obj.PortID === this.historyHead[0].podID);
        }
        if (this.historyHead[0].polType === 'Ground') {
          this.originDet = this.groundPorts.find(obj => obj.PortID === this.historyHead[0].polID);
        } else if (this.historyHead[0].polType === 'SEA') {
          this.originDet = ports.find(obj => obj.PortID === this.historyHead[0].polID);
        }
      }
    })
  }
  closeModal() {
    this._activeModal.close(status);
    document.getElementsByTagName('html')[0].style.overflowY = 'auto';
  }

  getGroundPorts() {
    this._manageRateService.getPortsData('ground').subscribe((res: any) => {
      loading(false)
      this.groundPorts = res;
    })
  }

  getProviderImage($image: string) {
    const providerImage = getProviderImage($image)
    return getImagePath(ImageSource.FROM_SERVER, providerImage, ImageRequiredSize.original)
  }

}

export interface History {
  AuditDesc: string;
}

export interface HistoryHead {
  carrierID?: any;
  polID?: any;
  podID?: any;
  containerSpecID?: any;
  shippingCatID?: any;
  polType?: any;
  podType?: any;
  shippingCatName?: any;
  polName?: any;
  podName?: any;
  polCode?: any;
  podCode?: any;
  polImageName?: any;
  podImageName?: any;
  whImage: string;
  whName: string;
  whAddress: string;
  whDesc: string;
}

export interface WarehouseResponse {
  history: History[];
  historyHead: HistoryHead[];
}

