import { Component, OnInit, ViewEncapsulation, ViewChild, Renderer2, ElementRef, Input, OnDestroy, ɵConsole, Output, EventEmitter } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { NgbActiveModal, NgbDropdownConfig } from "@ng-bootstrap/ng-bootstrap";
import { SharedService } from '../../../services/shared.service';
import { Observable, Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import {
  NgbDatepicker,
  NgbInputDatepicker,
  NgbDateStruct,
  NgbCalendar,
  NgbDateAdapter,
  NgbDateParserFormatter,
  NgbModal
} from '@ng-bootstrap/ng-bootstrap';
import { AirFreightService } from '../../../components/pages/user-desk/manage-rates/air-freight/air-freight.service';
import { ToastrService } from 'ngx-toastr';
import { NgbDateFRParserFormatter } from '../../../constants/ngb-date-parser-formatter';
import { JsonResponse } from '../../../interfaces/JsonResponse';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { cloneObject, extractColumn } from '../../../components/pages/user-desk/reports/reports.component';
import { SeaFreightService } from '../../../components/pages/user-desk/manage-rates/sea-freight/sea-freight.service';
import { CommonService } from '../../../services/common.service';
import { getImagePath, ImageSource, ImageRequiredSize, loading, removeDuplicates } from '../../../constants/globalFunctions';
import * as moment from "moment";
import { ManageRatesService } from '../../../components/pages/user-desk/manage-rates/manage-rates.service';
import { firstBy } from 'thenby';
import { IUploadRateList } from '../../../components/pages/user-desk/manage-rates/air-freight/air-freight.component';
import { baseExternalAssets } from '../../../constants/base.url';

const now = new Date();
const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
  one && two && two.year === one.year && two.month === one.month && two.day === one.day;

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day < two.day : one.month < two.month : one.year < two.year;

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day > two.day : one.month > two.month : one.year > two.year;

@Component({
  selector: 'app-upload-history',
  templateUrl: './upload-history.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [
    { provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter }, NgbDropdownConfig
  ],
  styleUrls: ['./upload-history.component.scss'],
})
export class UploadHistoryDialogComponent implements OnInit, OnDestroy {

  @Input() _sheetList: IUploadRateList[] = []

  constructor(
    private location: PlatformLocation,
    private _activeModal: NgbActiveModal,
  ) {

  }

  ngOnInit() {

  }


  closeModal() {
    this._activeModal.close(status);
    document.getElementsByTagName('html')[0].style.overflowY = 'auto';
  }

  downloadDoc(_filename) {
    window.open(baseExternalAssets + _filename, '_blank');
  }

  getIcon(_fileType) {
    if (_fileType === 'xlsx' || _fileType === 'xls')
      return 'excel'
    if (_fileType === 'docx' || _fileType === 'doc')
      return 'word'
    if (_fileType === 'pdf')
      return 'pdf'
  }

  ngOnDestroy(): void {
  }



}