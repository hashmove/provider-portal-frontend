import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadHistoryDialogComponent } from './upload-history.component';

describe('UploadHistoryDialogComponent', () => {
  let component: UploadHistoryDialogComponent;
  let fixture: ComponentFixture<UploadHistoryDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadHistoryDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadHistoryDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
