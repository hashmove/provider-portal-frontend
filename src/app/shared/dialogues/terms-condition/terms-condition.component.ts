import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { ToastrService } from 'ngx-toastr';
import { PlatformLocation } from '@angular/common';
import { Router, NavigationEnd } from '@angular/router';
import { ConfirmDialogContent } from '../confirm-dialog-generic/confirm-dialog-generic.component';

@Component({
  selector: 'app-terms-condition',
  templateUrl: './terms-condition.component.html',
  styleUrls: ['./terms-condition.component.scss']
})
export class TermsConditDialogComponent implements OnInit {
  @Input() modalData: ConfirmDialogContent = {
    messageTitle: 'Confirm',
    messageContent: 'Are you sure you want to cofirm?',
    data: {},
    buttonTitle: 'Yes'
  }

  isRouting: boolean = false

  constructor(
    private _activeModal: NgbActiveModal,
    private toastr: ToastrService,
    private location: PlatformLocation,
    private _router: Router
  ) {
    // location.onPopState(() => this.closeModal('close'));
  }

  ngOnInit() {
    console.log(this.modalData)
    // console.log(false)
    this.isRouting = false
  }

  closeModal($action: string) {
    if (this.isRouting) {
      return
    }
    this._activeModal.close($action);
    document.getElementsByTagName('html')[0].style.overflowY = 'auto';
  }

  onConfirmClick($action: string) {
    this.closeModal($action)
  }

}


