import { Component, OnInit, Input, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DashboardService } from '../../../components/pages/user-desk/dashboard/dashboard.service';
import { encryptBookingID, getImagePath, getLoggedUserData, ImageRequiredSize, ImageSource, isArrayValid, isJSON, isMobile } from '../../../constants/globalFunctions';
import { baseExternalAssets } from '../../../constants/base.url';
import { JsonResponse } from '../../../interfaces';
import { ToastrService } from 'ngx-toastr';
import { ScrollbarComponent } from 'ngx-scrollbar';
import * as JQ from 'jquery'
import { isShareLogin } from '../../../http-interceptors/interceptor';
@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, AfterViewInit {
  @Input() data: any;
  @Input() isEnabled: any;
  @Input() from = 'card';
  closeResult: string;
  @Input() logs: any[] = []
  public currencyList: any[] = []
  public selectedCurrency: any = {}
  comments: string
  loading: boolean = false
  chatList: IChat[] = []
  @ViewChild(ScrollbarComponent) scrollRef: ScrollbarComponent;
  @ViewChild('scrollMe') scrollMe: ElementRef;
  @ViewChild('messageBox') messageBox: ElementRef;

  disableScrollDown = false
  // userID: number = null
  userInfoList: IUserChatInfo[] = []
  defImage = baseExternalAssets + 'images/hashmove/original/icons_user.svg'
  autComp = 'xas' + new Date().getTime()

  hasChatInitiated = false
  placeholderText: string = 'Type a message'
  disableChat: boolean = false
  modalHeight = '500px'
  isMobile = isMobile()

  constructor(
    public _activeModal: NgbActiveModal,
    private _dashboardService: DashboardService,
    private _toastr: ToastrService
  ) { }


  ngOnInit() {
    this.getChatList(true)
    this.modalHeight = isMobile ? '350px' : '500px'
    // if (this.data.booking.SpecialRequestStatusBL.toLowerCase() === 'pending' && !this.hasChatInitiated) {
    //   this.initiateChat()
    // }
    try {
      const { booking } = this.data
      if (this.from === 'card' && (booking.BookingStatusCode === 'CANCELLED' || booking.ShippingStatusCode === 'COMPLETED' || isShareLogin())) {
        this.placeholderText = isShareLogin() ? 'Chat Disabled' : 'Booking has been ' + this.data.booking.ShippingStatus
        this.disableChat = true
      } else if (this.from === 'details' && (booking.BookingStatus.toUpperCase() === 'CANCELLED' || booking.ShippingStatus.toUpperCase() === 'COMPLETED' || isShareLogin())) {
        this.placeholderText = isShareLogin() ? 'Chat Disabled' : 'Booking has been ' + this.data.booking.ShippingStatus
        this.disableChat = true
      }
    } catch { }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.messageBox.nativeElement.select();
      this.messageBox.nativeElement.select();
    }, 0);
  }

  initiateChat() {
    const { booking } = this.data
    const _toSend = {
      bookingID: booking.BookingID,
      currencyID: this.selectedCurrency.id ? this.selectedCurrency.id : booking.CurrencyID,
      actualIndividualPrice: 0,
      baseIndividualPrice: 0,
      specialRequestComments: 'Messaging Started',
      userID: getLoggedUserData().UserID,
      requestStatus: 'PRICE_SENT',
      IsOnlyComments: true,
      exchangeRate: 0,
      currExcgRateListID: null,
      isRouteDetailUpdate: false,
      carrierID: null,
      pickupPort: {
        id: null,
        code: booking.PolCode,
        title: booking.PolName,
        imageName: booking.PolCountryCode
      },
      deliveryPort: {
        id: null,
        code: booking.PodCode,
        title: booking.PodName,
        imageName: booking.PodCountryCode
      },
      perUnitCurrencyID: booking.CurrencyID,
      perUnitRate: null,
      perUnitBaseRate: 0,
      perUnitExchangeRate: null,
      perUnitRateType: null
    }
    this._dashboardService.updateSpecialPrice(booking.BookingID, getLoggedUserData().UserID, _toSend).subscribe((res: JsonResponse) => {
      if (res.returnId > 0) {
        this.hasChatInitiated = true
      }
    })
  }

  closeChat(_state) {
    this._activeModal.close(this.hasChatInitiated)
  }

  ngAfterViewChecked() {
    // this.scrollToBottom(false);
  }

  onScroll() {
    let element = this.scrollMe.nativeElement
    let atBottom = element.scrollHeight - element.scrollTop === element.clientHeight
    if (this.disableScrollDown && atBottom) {
      this.disableScrollDown = false
    } else {
      this.disableScrollDown = true
    }
  }


  private scrollToBottom(_disableScrollDown): void {
    if (_disableScrollDown) {
      return
    }
    try {
      this.scrollMe.nativeElement.scrollTop = this.scrollMe.nativeElement.scrollHeight;
    } catch (err) { }
  }

  getChatList(getUserData?: boolean) {
    const _bookKey = encryptBookingID(this.data.booking.BookingID, this.data.booking.ProviderID, this.data.booking.ShippingModeCode)
    this._dashboardService.getChatList(_bookKey, 'OFFLINE_CHAT').subscribe((res: JsonResponse) => {
      this.autComp = 'xas' + new Date().getTime()
      if (res.returnId > 0) {
        const { parameterValue }: IChatResponse = res.returnObject
        const _chatData: IChatBody = JSON.parse(parameterValue)
        const _chatList = _chatData.messages
        try {
          const _toSend = {
            bookingID: this.data.booking.BookingID,
            messageBy: "PROVIDER",
            msgStatus: "READ",
            msgID: null
          }
          if (!isShareLogin()) {
            this._dashboardService.updateBookingChatStatus(getLoggedUserData().UserID, _toSend).subscribe(res => {
              // console.log(res)
            })
          }
        } catch { }
        if (getUserData) {
          const _usersList: number[] = Array.from(new Set(_chatList.map(_chat => _chat.userID)))
          try {
            if (!isArrayValid(_usersList, 0) || !_usersList.includes(getLoggedUserData().UserID)) {
              _usersList.push(getLoggedUserData().UserID)
            }
          } catch { }
          this._dashboardService.getUserBasicDetail(_usersList).subscribe((res: JsonResponse) => {
            if (res.returnId > 0) {
              this.userInfoList = res.returnObject
              setTimeout(() => {
                this.setChatList(_chatList)
              }, 10);
            } else {
              this.setChatList(_chatList)
            }
          }, error => {
            this.setChatList(_chatList)
          })
        } else {
          this.setChatList(_chatList)
        }
        // this.chatContainer.nativeElement.scrollTop = this.chatContainer.nativeElement.scrollHeight;
      } else {
        this._dashboardService.getUserBasicDetail([getLoggedUserData().UserID]).subscribe((res: JsonResponse) => {
          if (res.returnId > 0) {
            this.userInfoList = res.returnObject
          }
        })
      }
    })
  }

  readMessage() {
    try {
      const _toSend = {
        bookingID: this.data.booking.BookingID,
        messageBy: "PROVIDER",
        msgStatus: "READ",
        msgID: null
      }
      this._dashboardService.updateBookingChatStatus(getLoggedUserData().UserID, _toSend).subscribe(res => {
        // console.log(res)
      })
    } catch { }
  }

  setChatList(_chatList) {
    this.chatList = _chatList
    setTimeout(() => {
      this.scrollToBottom(false)
    }, 10);
  }

  getImage($userID: number) {
    try {
      const { userInfoList } = this
      const _image = userInfoList.filter(_user => _user.userID === $userID)[0].userImage
      return getImagePath(ImageSource.FROM_SERVER, _image, ImageRequiredSize.original)
    } catch (error) {
      console.log(error)
      return this.defImage
    }
  }

  getUserName($userID: number) {
    try {
      const { userInfoList } = this
      return userInfoList.filter(_user => _user.userID === $userID)[0].userName
    } catch (error) {
      console.log(error)
      return 'Customer'
    }
  }

  sendChat() {
    if (!this.messageHasText(this.comments)) {
      return
    }
    const _commentsJSON: IChat = {
      msgDateTime: new Date().toISOString(),
      userID: getLoggedUserData().UserID,
      msgText: this.comments,
      uploadedDocID: null,
      messageBy: 'PROVIDER',
      msgID: null,
      msgStatus: null,
    }

    const _toSend = {
      bookingID: this.data.booking.BookingID,
      shippingMode: this.data.booking.ShippingModeCode,
      parameterType: 'OFFLINE_CHAT',
      parameterValue: JSON.stringify(_commentsJSON),
      providerID: this.data.booking.ProviderID,
      userID: this.data.booking.UserID,
      isUpdateByProvider: true,
    }
    this.loading = true
    this._dashboardService.sendChat(getLoggedUserData().UserID, _toSend).subscribe((res: JsonResponse) => {
      this.loading = false
      if (res.returnId > 0) {
        try {
          if (this.data.booking.SpecialRequestStatusBL.toLowerCase() === 'pending' && !this.hasChatInitiated && this.data.booking.IsRateNotFoundBooking) {
            this.initiateChat()
          }
        } catch { }
        if (res.returnCode === '2') {
          this._toastr.info(res.returnText, res.returnStatus)
        }
        this.comments = null
        this.getChatList(true)
      } else {
        this._toastr.error(res.returnText, res.returnStatus)
      }
    }, error => {
      this.loading = false
      this._toastr.error('There was an error while processing your request, please try again later', 'Failed')
    })
  }

  handleInput(e) {
    if (e.key == "Enter") {
      if (e.shiftKey) e.target.style.height = e.target.offsetHeight + 20 + "px";
      else e.target.form.submit();
    }
  }

  onKeyPress(e) {
    if (e.keyCode === 13 && !e.altKey && this.messageHasText(this.comments)) {
      e.preventDefault();
      e.stopPropagation();
      this.sendChat()
      return
    }
    if (e.keyCode === 13 && e.altKey) {
      console.log('yolo')
      this.comments = (this.comments ? this.comments : '') + '\n'
      const _chatBox = document.getElementById('chatBox') as any
      _chatBox.focus();
      _chatBox.setSelectionRange(_chatBox.value.length, _chatBox.value.length);
      _chatBox.scrollTop = _chatBox.scrollHeight;
      try {
        this.messageBox.nativeElement.scrollTop = this.messageBox.nativeElement.scrollHeight;
      } catch (err) { }
    }
  }

  messageHasText(_message: string) {
    if (_message) {
      const _str = _message.replace(/\n/g, '').replace(/ /g, '')
      return _str && _str.length && _str.length > 0 ? true : false
    } else {
      return false
    }
  }

  renderMessage(_message: string) {
    try {
      return _message.replace(/\n/g, '<br>');
    } catch {
      return _message
    }
  }
}
export interface IChatResponse {
  bookingID: number;
  parameterType: string;
  parameterValue: string;
}


export interface IChatBody {
  providerCount: number;
  customerCount: number;
  messages: IChat[];
}
export interface IChat {
  msgDateTime: string;
  userID: number;
  msgText: string;
  uploadedDocID?: any;
  messageBy: string;
  msgStatus: string;
  msgID: number;
}
interface IUserChatInfo {
  userID: number;
  userName: string;
  contactNumber: string;
  email: string;
  phoneCodeCountryID: number;
  countryCode: string;
  countryPhoneCode: string;
  companyID: number;
  sourceID: number;
  sourceType: string;
  userImage: string;
}