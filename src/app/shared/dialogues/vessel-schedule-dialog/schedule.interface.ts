export interface TransferVesselsDetails {
    RouteCode1: string;
    RouteName1?: any;
    R1PolCode: string;
    R1PodCode: string;
    R1EtdDate: string;
    R1EtaDate: string;
    R1VesselName: string;
    R1VesselNo: string;
    R1VoyageRefNo: string;
    RouteCode2?: any;
    RouteName2?: any;
    R2PolCode: string;
    R2PodCode: string;
    R2EtdDate: string;
    R2EtaDate: string;
    R2VesselName: string;
    R2VesselNo: string;
    R2VoyageRefNo: string;
    RouteCode3?: any;
    RouteName3?: any;
    R3PolCode: string;
    R3PodCode?: any;
    R3EtdDate?: any;
    R3EtaDate?: any;
    R3VesselName: string;
    R3VesselNo: string;
    R3VoyageRefNo: string;
    RouteCode4?: any;
    RouteName4?: any;
    R4PolCode?: any;
    R4PodCode?: any;
    R4EtdDate?: any;
    R4EtaDate?: any;
    R4VesselName?: any;
    R4VesselNo?: any;
    R4VoyageRefNo?: any;
    RouteCode5?: any;
    RouteName5?: any;
    R5PolCode?: any;
    R5PodCode?: any;
    R5EtdDate?: any;
    R5EtaDate?: any;
    R5VesselName?: any;
    R5VesselNo?: any;
    R5VoyageRefNo?: any;
}

export interface RouteInfo {
    dateUtc: Date;
    dateLcl: Date;
    modeOfTrans: string;
    routeDesc: string;
    isPassed: boolean;
    portCode: string;
    portName: string;
    latitude: string;
    longitude: string;
    vesselCode: string;
    vesselName: string;
    voyageRefNum: string;
    flightNo: string;
    airCraftInfo: string;
}

export interface BookingRouteMapInfo {
    route: string;
    transitTime: number;
    carrierName: string;
    carrierImage: string;
    freeTimeAtPort: number;
    routeInfo: RouteInfo[];
    jsonTransferVesselsDetails: string;
}

export interface IBookingSchedule {
    bookingID: number;
    portCutOffUtc: Date;
    portCutOffLcl: Date;
    etdUtc: Date;
    etaUtc: Date;
    etdLcl: Date;
    etaLcl: Date;
    transitTime: number;
    etaInDays: number;
    transfersStop?: any;
    carrierID: number;
    carrierCode: string;
    carrierName: string;
    carrierImage: string;
    vesselCode: string;
    vesselName: string;
    voyageRefNum: string;
    bookingRouteMapInfo: BookingRouteMapInfo;
}