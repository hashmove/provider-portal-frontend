import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { getImagePath, ImageSource, ImageRequiredSize, encryptBookingID } from '../../../constants/globalFunctions';
import { ViewBookingService } from '../../../components/pages/user-desk/view-booking/view-booking.service';
import * as moment from 'moment';
import { JsonResponse } from '../../../interfaces/JsonResponse';
import { ToastrService } from 'ngx-toastr';
import { AddScheduleComponent } from '../add-schedule/add-schedule.component';
import { TransferVesselsDetails } from './schedule.interface';



@Component({
  selector: 'app-vessel-schedule-dialog',
  templateUrl: './vessel-schedule-dialog.component.html',
  styleUrls: ['./vessel-schedule-dialog.component.scss'],
})
export class VesselScheduleDialogComponent implements OnInit {
  @Input() data: any;
  public selectedSchedule: any
  constructor(
    private activeModal: NgbActiveModal,
    private modalService: NgbModal,
    private _viewBookingService: ViewBookingService,
    private _toastr: ToastrService
  ) {
  }

  ngOnInit() {
    console.log(this.data)
    if (this.data.bookingID > -1) {
      this.data.date.month = this.data.date.month - 1
      this.data.date = moment(this.data.date).format('D MMM, Y')
    }
  }

  closeModal() {
    this.activeModal.close();
    document.getElementsByTagName('html')[0].style.overflowY = 'auto';
  }


  /**
   *
   * Get Imgages for Shipping Lines
   * @param {string} $image
   * @returns
   * @memberof VesselScheduleDialogComponent
   */
  getShippingLineImage($image: string) {
    return getImagePath(
      ImageSource.FROM_SERVER,
      "/" + $image,
      ImageRequiredSize.original
    );
  }

  selectSchedule(item) {
    this.selectedSchedule = item;
  }

  confirmSchedule() {
    this.selectedSchedule.bookingID = this.data.bookingID
    this._viewBookingService.saveCarrierSchedule(this.selectedSchedule).subscribe((res: JsonResponse) => {
      console.log(res)
      const { returnId, returnObject, returnText } = res
      if (returnId > 0) {
        this.activeModal.close(returnObject);
      } else {
        this._toastr.error(returnText)
      }
    }, (err: any) => {
      this._toastr.error('There was an error while processing your request, Please try again later.', 'Error')
      console.log(err)
    })
  }

  addVesselSchedule(data) {
    try {
      const { BookingID, ProviderID, ShippingModeCode } = this.data.bookingDetails
      this._viewBookingService
        .getBookingRouteDetail(encryptBookingID(BookingID, ProviderID, ShippingModeCode)).subscribe((res: JsonResponse) => {
          if (res.returnId > 0) {
            const modalRef = this.modalService.open(AddScheduleComponent, {
              size: 'lg', centered: true, windowClass: 'schedule-modal', backdrop: 'static', keyboard: false
            })
            modalRef.componentInstance.carrierList = this.data.carriersList.filter(_ship => _ship.code.toLowerCase() !== 'default')
            modalRef.componentInstance.PolID = this.data.bookingDetails.PolID
            modalRef.componentInstance.PodID = this.data.bookingDetails.PodID
            modalRef.componentInstance.bookingDetails = this.data.bookingDetails
            modalRef.componentInstance.bookCarrier = this.data.carriersList.filter(_carrier => _carrier.id === this.data.bookingDetails.CarrierID)[0]
            modalRef.componentInstance.from = 'VIEW_BOOKING'
            modalRef.componentInstance.schedule = res.returnObject
            modalRef.componentInstance.isEdit = true
            modalRef.result.then(_schedule => {
              if (_schedule) {
                try {
                  const _data = JSON.parse(_schedule.returnObject)
                  const toSend = { ..._data, bookingID: this.data.bookingID }
                  this._viewBookingService.saveCarrierSchedule(toSend).subscribe((res: JsonResponse) => {
                    const { returnId, returnObject, returnText } = res
                    if (returnId > 0)
                      this.activeModal.close(returnObject);
                    else
                      this._toastr.error(returnText)
                  }, (err: any) => {
                    this._toastr.error('There was an error while processing your request, Please try again later.', 'Error')
                    console.log(err)
                  })
                } catch { }
              }
            })
          }
        })
    } catch (error) {
      console.log(error)
    }
  }

  getVesselDirectino(vesselData: string) {
    let transit = 'direct'
    try {
      if (vesselData) {
        let numOfStops = 0
        let index = 1
        const parsedVeselData: TransferVesselsDetails = JSON.parse(vesselData)
        Object.entries(parsedVeselData).forEach(([key, value]) => {
          if (key.includes('PodCode')) {
            if (parsedVeselData[`R${index}PodCode`] && parsedVeselData[`R1PodCode`]) {
              numOfStops++
            }
            index++
          }
        });
        const finnStop = numOfStops - 1
        if (finnStop > 0) {
          transit = (finnStop) > 1 ? `${finnStop} Stops` : `${finnStop} Stop`
        }
      }
    } catch {
    }
    return transit
  }
}
