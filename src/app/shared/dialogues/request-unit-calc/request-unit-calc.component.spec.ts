import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestUnitCalc } from './request-unit-calc.component';

describe('RequestUnitCalc', () => {
  let component: RequestUnitCalc;
  let fixture: ComponentFixture<RequestUnitCalc>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestUnitCalc ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestUnitCalc);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
