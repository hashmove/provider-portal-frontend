import { Component, OnInit, ViewEncapsulation, Input, OnDestroy } from '@angular/core'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { CurrencyControl } from '../../../services/currency.service'
import { IRequestContainers } from '../cargo-details-rate-request/cargo-details-rate-request.interface'
import { IContanerInput, IRequestData, SearchCriteria } from '../request-add-rate-dialog/request-add-rate.interface'
@Component({
  selector: 'app-request-unit-calc',
  templateUrl: './request-unit-calc.component.html',
  styleUrls: ['./request-unit-calc.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RequestUnitCalc implements OnInit, OnDestroy {

  @Input() requestData: IRequestData
  @Input() inputContainers: IContanerInput[] = []
  @Input() searchCriteria: SearchCriteria = null
  cbmContainerData: any = {
    length: 0,
    width: 0,
    height: 0,
    actualWeight: 0,
    quantity: 0,
  }
  unitTabId = 'by_unit'
  public lengthUnits: Array<any> = [];
  public weightUnits: Array<UnitType> = [];
  public volumeUnits: Array<any> = [];
  public selectedLengthUnit: number;
  public unitsResponse: any;
  public selectedLengthUnitID: number = 2;
  public selectedWeightUnitID: number = 6;
  public selectedVolumeUnitID: number = 9;
  public selectedAreaUnitID: number = 13;

  public totalCBM: number;
  public totalWeight: number;
  public totalArea: number;


  maxHeightLimitInMeter: number = 2.95 // max height limit in meter
  maxHeightLimitInMeterUnitId: number = 1 // max height limit in meter
  maxHeightLimit: number = 0


  public formValidation = {
    invalid: false,
    message: ""
  };


  constructor(
    private _currencyControl: CurrencyControl,
    private _activeModal: NgbActiveModal,
  ) {

  }

  ngOnInit() { }

  closeModal = (_state) => this._activeModal.close(_state)

  calculateTotalCBM(_container: any) {
    const { unitTabId } = this;
    this.totalCBM = 0;
    this.totalWeight = 0;
    this.totalArea = 0;
    if (unitTabId === "by_unit") {
      if (
        _container.height &&
        _container.length &&
        _container.width
      ) {
        if (_container.lengthUnit === 2) {
          this.totalCBM = _container.length * 0.01 * (_container.width * 0.01) * (_container.height * 0.01) * _container.quantity;
          this.totalArea = _container.length * 0.0328084 * (_container.width * 0.0328084);
        } else if (_container.lengthUnit === 3) {
          this.totalCBM = _container.length * 0.001 * (_container.width * 0.001) * (_container.height * 0.001) * _container.quantity;
          this.totalArea = _container.length * 0.001 * (_container.width * 0.001);
        } else if (_container.lengthUnit === 4) {
          this.totalCBM = _container.length * 0.3048 * (_container.width * 0.3048) * (_container.height * 0.3048) * _container.quantity;
          this.totalArea = _container.length * _container.width;
        } else if (_container.lengthUnit === 5) {
          this.totalCBM = _container.length * 0.0254 * (_container.width * 0.0254) * (_container.height * 0.0254) * _container.quantity;
          this.totalArea = _container.length * 0.0833333 * (_container.width * 0.0833333);
        } else if (_container.lengthUnit === 1) {
          this.totalCBM = _container.length * _container.width * _container.height * _container.quantity;
          this.totalArea = _container.length * 3.28084 * (_container.width * 3.28084);
        }
      }
      if (_container.weight) {
        if (_container.weightUnit === 7) {
          this.totalWeight = _container.weight / 2.20462;
        } else if (_container.weightUnit === 8) {
          this.totalWeight = _container.weight * 2204.62;
        } else {
          this.totalWeight = _container.weight;
        }
      }
      if (_container.totalWeight) {
        if (_container.totalWeightUnit === 7) {
          this.totalWeight = _container.totalWeight / 2.20462;
        } else if (_container.totalWeightUnit === 8) {
          this.totalWeight = _container.totalWeight / 2204.62;
        } else {
          this.totalWeight = _container.totalWeight;
        }
      }
    } else {
      if (_container.weight) {
        if (_container.totalWeightUnit === 7) {
          this.totalWeight = _container.weight / 2.20462;
        } else if (_container.totalWeightUnit === 8) {
          this.totalWeight = _container.weight * 1000;
        } else {
          this.totalWeight = _container.weight;
        }
      } else {
      }
      if (_container.volume) {
        if (_container.volumeUnit === 10) {
          this.totalCBM = _container.volume * 0.0283168;
        } else {
          this.totalCBM = _container.volume;
        }
      }
      if (_container.area) {
        if (_container.areaUnit === 11) {
          this.totalArea = _container.area * 0.00107639;
        } else if (_container.areaUnit === 12) {
          this.totalArea = _container.area * 10.7639;
        } else if (_container.areaUnit === 13) {
          this.totalArea = _container.area;
        } else if (_container.areaUnit === 14) {
          this.totalArea = _container.area * 9;
        }
      }
    }
    this.totalArea = this.totalArea
    this.totalCBM = this.totalCBM
    this.totalWeight = this.totalWeight

    let weightOne: number = 0
    let weightTwo: number = 0
    let finWeight: number = 0

    try {
      weightOne = this.totalWeight
      // weightTwo = this.totalCBM * 166.67 //before
      weightTwo = (this.totalCBM / 0.006); //after remarks from BetaShip as per meeting with AeroAfrica at 10/09/2019
      finWeight = (weightOne > weightTwo) ? weightOne : weightTwo
      finWeight = this._currencyControl.applyRoundByDecimal(finWeight, 2)
    } catch (error) {
      console.warn('error:', error)
    }
    _container.chipWeight = finWeight
    _container.cargoLoadType = this.unitTabId
    if (this.unitTabId !== 'by_unit') {
      try {
        const rightSide = Math.floor(finWeight)
        let leftSide = finWeight - rightSide
        if (leftSide > 0) {
          if (leftSide >= 0.01 && leftSide <= 0.50) {
            leftSide = 0.50
          } else if (leftSide > 0.5 && leftSide <= 0.99) {
            leftSide = 1
          }
          finWeight = rightSide + leftSide
        }
        _container.chargeableWeight = finWeight
      } catch (error) {
        _container.chargeableWeight = finWeight
      }
    }
  }

  onUnitChange(event, type, _container) {
    event = parseInt(event);
    if (type === "length") {
      _container.lengthUnit = event;
      this.selectedLengthUnitID = event;
      this.maxHeightLimit = this.unitConversion(this.lengthUnits, this.maxHeightLimitInMeterUnitId, this.maxHeightLimitInMeter, event) // setting init maxLength value
      this.validateHight(_container.height, _container)
      this.calculateTotalCBM(_container);
    } else if (type === "weight") {
      _container.totalWeightUnit = event;
      _container.weightUnit = event;
      this.selectedWeightUnitID = event;
      this.calculateTotalCBM(_container);
    } else if (type === "actualWeight") {
      _container.totalWeightUnit = event;
      _container.weightUnit = event;
      this.selectedWeightUnitID = event;
      this.calculateTotalCBM(_container);
    } else if (type === "volume") {
      _container.volumeUnit = event;
      this.selectedVolumeUnitID = event;
      _container.volumeUnit = event;
      this.calculateTotalCBM(_container);
    } else if (type === "area") {
      _container.areaUnit = event;
      this.selectedAreaUnitID = event;
      this.calculateTotalCBM(_container);
    } else if (type === "totalWeight") {
      _container.totalWeightUnit = event;
      this.selectedWeightUnitID = event;
      this.calculateTotalCBM(_container);
    }
  }

  unitConversion(unitArray: Array<UnitType>, srcUnit: number, srcValue: number, destUnit: number): number {
    let finValue = 0
    try {
      if (srcValue > 0 && srcUnit !== destUnit) {
        const sourceUnitObj: UnitType = unitArray.filter(e => e.UnitTypeID === srcUnit)[0];
        const destUnitObj: UnitCalculation = sourceUnitObj.UnitCalculation.filter(unit => unit.UnitTypeID === destUnit)[0]
        const { UnitTypeCalc } = destUnitObj
        finValue = UnitTypeCalc * srcValue
      } else if (srcUnit === destUnit) {
        finValue = srcValue
      }
    } catch {
      finValue = 0
    }
    return finValue
  }

  validateHight(numb, _container) {
    if (numb > this.maxHeightLimit) {
      // console.log(this.maxHeightLimit);
      this.formValidation = {
        invalid: true,
        message: 'height should not exceed ' + this._currencyControl.applyRoundByDecimal(this.maxHeightLimit, 2) + ' ' + this.getLenghtUnit(_container.lengthUnit)
      };
    } else {
      this.formValidation = {
        invalid: false,
        message: ""
      };
    }
  }


  getLenghtUnit(id) {
    let _return = 'cm'
    try {
      const { lengthUnits } = this
      const data = lengthUnits.filter(unit => unit.UnitTypeID === id)[0]
      _return = data.UnitTypeShortName
    } catch (error) { }
    return _return
  }


  counter(pos, event, _container) {
    if (
      _container.quantity === undefined ||
      _container.quantity === null ||
      !_container.quantity
    ) {
      _container.quantity = 0;
    }
    _container.quantity = parseInt(_container.quantity);
    if (pos === "plus") {
      if (_container.quantity <= 999) {
        _container.quantity++;
      } else {
        event.preventDefault();
      }
      this.calculateTotalCBM(_container);
    } else if (pos === "minus" && _container.quantity > 0) {
      _container.quantity--;
      this.calculateTotalCBM(_container);
    }
  }

  ngOnDestroy() {

  }

}



export interface UnitCalculation {
  UnitTypeID: number;
  UnitTypeCode: string;
  UnitTypeCalc: number;
}

export interface UnitType {
  UnitTypeID: number;
  UnitTypeCode: string;
  UnitTypeName: string;
  UnitTypeShortName: string;
  UnitTypeNature: string;
  UnitCalculation: UnitCalculation[];
}