import { Component, OnInit, Input } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { PlatformLocation } from "@angular/common";

@Component({
  selector: "app-confirm-update-dialog",
  templateUrl: "./confirm-update-dialog.component.html",
  styleUrls: ["./confirm-update-dialog.component.scss"]
})
export class ConfirmUpdateDialogComponent implements OnInit {
  public loading: boolean = false;

  @Input() amount: number = 0
  @Input() code: string = ''
  @Input() isSpotRate: boolean = false
  @Input() data: any

  constructor(
    private _activeModal: NgbActiveModal,
    private location: PlatformLocation,
  ) {
    location.onPopState(() => this.closeModal());
  }

  ngOnInit() { }

  closeModal() {
    this._activeModal.close(false);
  }
  onConfirmClick() {
    this._activeModal.close(true)
  }
}
