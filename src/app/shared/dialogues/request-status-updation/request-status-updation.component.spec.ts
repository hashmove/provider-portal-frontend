import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestStatusUpdationComponent } from './request-status-updation.component';

describe('RequestStatusUpdationComponent', () => {
  let component: RequestStatusUpdationComponent;
  let fixture: ComponentFixture<RequestStatusUpdationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RequestStatusUpdationComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestStatusUpdationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
