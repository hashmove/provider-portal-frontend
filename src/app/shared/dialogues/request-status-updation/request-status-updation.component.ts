import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ViewBookingService } from '../../../components/pages/user-desk/view-booking/view-booking.service';
import { getLoggedUserData } from '../../../constants/globalFunctions';
import { ToastrService } from "ngx-toastr";
import { PlatformLocation } from '@angular/common';
import { CommonService } from '../../../services/common.service';
import { JsonResponse } from '../../../interfaces/JsonResponse';
import { IRateRequest } from '../request-add-rate-dialog/request-add-rate.interface';
import { BookingReason } from '../booking-status-updation/booking-status-updation.component';

@Component({
  selector: 'app-request-status-updation',
  templateUrl: './request-status-updation.component.html',
  styleUrls: ['./request-status-updation.component.scss']
})
export class RequestStatusUpdationComponent implements OnInit {
  @Input() request: IRateRequest;
  public label: string;
  public description: string;
  public requestReasons: BookingReason[] = [];
  public actionObj: any;
  public notifyActionObj: any;
  public cancelledStatus: any;
  public selectPlaceholder: string;
  public selectedReason: any = { remarks: '', status: '', id: 0, code: '' }
  public userInfo

  public newHMRefNumber: string = ''
  carriersList = []
  btnLoader = false

  constructor(
    private _viewBookingService: ViewBookingService,
    private _toast: ToastrService,
    private location: PlatformLocation,
    private _commonService: CommonService,
    public _activeModal: NgbActiveModal) { location.onPopState(() => this.closeModal(null)); }

  ngOnInit() {
    this.userInfo = getLoggedUserData()
    this.label = this.request.ResponseID && this.request.ResponseID > 0 ? 'Discard Quote' : 'Discard Rate Request'
    this.description = 'Please provide the reason for discard/decline.'
    this.selectPlaceholder = 'Select Reason';
    this.getBookingReasons();
  }


  async getBookingReasons() {
    try {
      const res: JsonResponse = await this._viewBookingService.getRateRequestReason().toPromise() as any
      if (res.returnId === 1) {
        this.requestReasons = res.returnObject.filter(e => e.BusinessLogic !== 'CANCELLED')
      } else {
        this._toast.error('There was an error while updating your booking, please try later')
      }
    } catch (err) {
      this._toast.error('There was an error while updating your booking, please try later')
      console.warn(err.message)
    }
  }


  onModelChange(model: any) {
    if (model) {
      this.selectedReason.id = parseInt(model.target.value)
      const status = this.requestReasons.filter(s => s.ReasonID == this.selectedReason.id)[0]
      const { ReasonID, ReasonName } = status
      this.selectedReason = { id: ReasonID, remarks: '', status: ReasonName }
    }
  }

  submit() {


    const { id, remarks } = this.selectedReason;
    const _toSend = {
      RequestID: this.request.RequestID,
      ProviderID: this.getCurrenProviderID(),
      ResponseID: this.request.ResponseID ? this.request.ResponseID : 0,
      RequestStatusBL: 'DISCARD',
      RequestStatusRemarks: remarks,
      ReasonID: parseInt(id),
      RequestNo: this.request.RequestNo,
      RequestType: this.request.ShippingModeCode,
      RequestCompanyID: this.request.Customer && this.request.Customer.CustomerID ? this.request.Customer.CustomerID : 0,
      RequestUserID: this.request.Customer && this.request.Customer.UserID ? this.request.Customer.UserID : 0,
      isCancelByProvider: true,
    }
    this.btnLoader = true
    this._viewBookingService.discardRequestV2(getLoggedUserData().UserID, _toSend).subscribe((res: any) => {
      this.btnLoader = false
      if (res.returnId > 0) {
        this._activeModal.close(true)
        this._toast.success(res.returnText, 'Success')
      } else {
        this._toast.error('There was an error while updating your booking, please try later')
      }
    }, (err) => {
      this.btnLoader = false
      console.warn(err.message)
      this._toast.error('There was an error while updating your booking, please try later')
    })

  }


  closeModal(resType) {
    this._activeModal.close(resType);
    document.getElementsByTagName('html')[0].style.overflowY = 'auto';

  }

  getCurrenProviderID(): number {
    try {
      return this.request.ProviderID ? this.request.ProviderID : this.request.Provider.ProviderID ? this.request.Provider.ProviderID : 0
    } catch {
      return 0
    }
  }

}
