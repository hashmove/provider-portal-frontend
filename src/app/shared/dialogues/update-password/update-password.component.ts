import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Location, PlatformLocation } from '@angular/common';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoginDialogComponent } from '../login-dialog/login-dialog.component';
import { ToastrService } from 'ngx-toastr';
// import { AuthService } from './../../../services/authservice/auth.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { UserCreationService } from '../../../components/pages/user-creation/user-creation.service';
import { JsonResponse } from '../../../interfaces';
import { PASSWORD_REGEX, validatePassword } from '../../../constants/globalFunctions';
@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class UpdatePasswordComponent implements OnInit, AfterViewInit {

  public colorEye;
  public passwordError;
  closeResult: string;
  currentJustify = 'justified';
  updatePassForm;

  newPasswordStatus = {
    lengthValid: false, upperCaseValid: false, lowerCaseValid: false,
    digitValid: false, specialCharValid: false
  }
  showPassValidator = false
  newPass

  constructor(
    private _userCreationService: UserCreationService,
    private activeModal: NgbActiveModal,
    private modalService: NgbModal,
    private _toast: ToastrService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private _location: Location,
    private _browseNavigate: PlatformLocation
  ) {
    _browseNavigate.onPopState(() => this.closeModal());
  }

  ngOnInit() {
    this.updatePassForm = new FormGroup({
      updatePassword: new FormControl('', {
        validators: [Validators.required, Validators.minLength(8),
        Validators.pattern(PASSWORD_REGEX), Validators.maxLength(40)]
      })
    });
  }

  errorMessages() {
    if (this.updatePassForm.controls.updatePassword.status == "INVALID" && this.updatePassForm.controls.updatePassword.touched) {
      this.passwordError = true;
    }
  }
  confirmPassword(event) {
    let element = event.currentTarget.nextSibling.nextElementSibling;
    if (element.type === "password" && element.value) {
      element.type = "text";
      this.colorEye = "grey";
    }
    else {
      element.type = "password";
      this.colorEye = "black";

    };
  }

  closeModal() {
    this._location.replaceState('login');
    this.activeModal.close();
    document.getElementsByTagName('html')[0].style.overflowY = 'auto';
  }

  loginModal() {
    this.activeModal.close();
    this.modalService.open(LoginDialogComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'login-modal',
      backdrop: 'static',
      keyboard: false
    });
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);
  }



  updatePassword(obj) {
    if (this.updatePassForm.invalid) {
      this.errorMessages();
      return;
    }
    let object = {
      Code: this.activatedRoute.snapshot.queryParams.code,
      Password: obj.updatePassword
    };
    this._userCreationService.userupdatepassword(object).subscribe((res: JsonResponse) => {
      if (res.returnId > 0) {
        this._toast.success("Password updated successfully.");
        this._location.replaceState('login');
        this.updatePassForm.reset();
        setTimeout(() => {
          this.activeModal.close()
        }, 10);
      } else if (res.returnCode === '2') {
        this._toast.info(res.returnText, res.returnStatus)
      } else {
        this._toast.error(res.returnText);
      }
    }, (err: HttpErrorResponse) => {
      console.log(err)
    })
  }

  ngAfterViewInit() {
    setTimeout(() => this.updatePassForm.reset(), 100)
  }

  onNewPassChange(_value) {
    console.log('new pass:', _value)
    this.updatePassForm.controls['updatePassword'].setValue(_value)
    this.newPasswordStatus = validatePassword(_value)
    console.log(this.newPasswordStatus)
    this.showPassValidator = true
  }

}
