import { ElementRef, Injectable, ViewChild } from '@angular/core';
import { NgFilesConfig } from '../../../directives/ng-files';
import { CurrencyDetails, UserDocument, UserInfo } from '../../../interfaces';
import { IInvoiceDetail, ISaveInvoice, IUploadedInvoiceDet } from './invoice.interface';
import { Observable } from 'rxjs'
import { debounceTime, map } from 'rxjs/operators'
import * as moment from 'moment'
import { getRoundedNum, getScreenHeight, isArrayValid } from '../../../constants/globalFunctions';
import { IInvoiceTrail } from '../../../components/pages/user-desk/invoice-card/invoice-card.interface';

export class IInvoiceHelper {
  // Document Config
  public config: NgFilesConfig = {
    acceptExtensions: ['pdf', 'jpg', 'jpeg', 'xlsx', 'xls', 'doc', 'docx', 'png', 'txt'],
    maxFilesCount: 1,
    maxFileSize: 12 * 1024 * 1000,
    totalFilesSize: 12 * 12 * 1024 * 1000
  }

  isDocConfigSet = false
  allowedFiles = 'pdf, jpg, jpeg, xlsx, xls, doc, docx, png, txt'
  docText = '(supported types: pdf, jpg, jpeg, xlsx, doc, docx, png and txt. Maximum size per document: 10MB)'
  droppedDoc

  public additionalDocuments: UserDocument[] = []
  public currentDocObject: UserDocument
  // docTypeList = []
  loading = false

  // Invoice Details
  public invoiceNumber = null
  public invoiceAmount = null
  public invoiceDate = null
  public prevCurrency: CurrencyDetails
  public selectedCurrency: CurrencyDetails
  public isAddItemAllowed = false
  public minDate = null

  // Invoice Items
  public invAddTxt = ''
  public invAddTxtType = null
  public invoiceCurr: CurrencyDetails
  public invAddAmount: number = null
  public invAddExRate: number = null

  public invAddTxtInvalid = false
  public invAddTxtTypeInvalid = false
  public invoiceCurrInvalid = false
  public invAddAmountInvalid = false
  public invAddExRateInvalid = false

  public invoiceItems: IInvoiceDetail[] = []
  public allCurrencies: CurrencyDetails[] = []

  // Misc
  disableScrollDown = false
  loginUser: UserInfo
  heightLimit = getScreenHeight()
  uploadedInvoiceDet: IUploadedInvoiceDet = null
  invDtlID: number = null
  isInvoiceEditable = true
  surchageType = [{ code: 'ADCH', label: 'Other Charges' },
  { code: 'FSUR', label: 'Base Freight' }]
  @ViewChild('lineItem') lineItem: ElementRef;

  constructor() { }

  getDatefromObj(_date) {
    let toSend = null
    try {
      toSend = moment(new Date(_date.year, _date.month - 1, _date.day)).format('YYYY-MM-DD')
    } catch (error) { }
    return toSend
  }

  getNum = (_num: any) =>
    (_num) ? (parseFloat(_num) ? typeof parseFloat(_num) === 'number' ? parseFloat(_num) : 0 : 0) : 0


  formatter = currency => currency.title
  currencyFormatter = currency => currency.shortName

  currencies = (text$: Observable<string>) => text$.pipe(debounceTime(200), map(term => !term || term.length < 3
    ? [] : this.allCurrencies.filter(
      v => v.shortName && v.shortName.toLowerCase().indexOf(term.toLowerCase()) > -1)
  ))

  addItem() {
    this.invAddTxtInvalid = (!this.invAddTxt) ? true : false
    this.invAddTxtTypeInvalid = (!this.invAddTxtType) ? true : false
    this.invAddAmountInvalid = (!this.invAddAmount) ? true : false
    this.invAddExRateInvalid = (!this.invAddExRate) ? true : false
    this.invoiceCurrInvalid = (!this.invoiceCurr || !this.invoiceCurr.code) ? true : false

    if (this.invAddTxtInvalid || this.invAddAmountInvalid || this.invAddExRateInvalid || this.invoiceCurrInvalid || this.invAddTxtTypeInvalid)
      return

    const _exRate = this.getNum(this.invAddExRate)
    const _invAmount = this.getNum(this.invAddAmount)
    if (this.invDtlID) {
      this.invoiceItems.forEach(_inv => {
        if (_inv.InvDtlID === this.invDtlID) {
          _inv.AddChrID = null
          _inv.AddChrType = this.invAddTxtType
          _inv.OtherAddChrText = this.invAddTxt
          _inv.CurrencyID = this.invoiceCurr.id
          _inv.CurrencyCode = this.invoiceCurr.code
          _inv.Amount = _invAmount
          _inv.ExchangeRate = _exRate
        }
      })
    } else {
      this.invoiceItems.push({
        AddChrID: null,
        AddChrType: this.invAddTxtType,
        InvDtlID: new Date().getTime(),
        OtherAddChrText: this.invAddTxt,
        CurrencyID: this.invoiceCurr.id,
        CurrencyCode: this.invoiceCurr.code,
        Amount: _invAmount,
        ExchangeRate: _exRate,
      })
    }
    this.invAddTxt = null
    this.invAddAmount = null
    this.invAddTxtType = null
    this.invDtlID = null
    this.autosize()
    console.log('invoiceItems:', this.invoiceItems);
  }

  editInvItem(_invoice: IInvoiceDetail) {
    this.invDtlID = _invoice.InvDtlID
    this.invAddTxt = _invoice.OtherAddChrText
    this.invAddTxtType = _invoice.AddChrType
    this.invoiceCurr = this.allCurrencies.find(_curr => _curr.id === _invoice.CurrencyID)
    this.invAddAmount = _invoice.Amount
    this.invAddExRate = _invoice.ExchangeRate
  }
  removeInvItem = (index: number) => this.invoiceItems.splice(index, 1)

  getInvSaveObject = (_uploadDoc: any, selectedInvoice: IInvoiceTrail): ISaveInvoice => ({
    InvoiceID: selectedInvoice.invoiceID > 0 ? selectedInvoice.invoiceID : null,
    InvoiceRefNo: this.invoiceNumber,
    InvoiceRefDate: this.getDatefromObj(this.invoiceDate),
    CurrencyID: this.selectedCurrency && this.selectedCurrency.id ? this.selectedCurrency.id : null,
    ExchangeRate: null,
    ExRateListID: null,
    InvoiceAmount: this.applyRoundByDecimal(this.getNum(this.invoiceAmount), 2),
    InvoiceDetail: isArrayValid(this.invoiceItems, 0) ? this.invoiceItems : null,
    InvoiceDocument: _uploadDoc as any
  })

  onItemCurrChange() {
    if (this.invoiceCurr && this.selectedCurrency &&
      this.invoiceCurr.id && this.selectedCurrency.id &&
      this.invoiceCurr.id === this.selectedCurrency.id
    ) {
      this.invAddExRate = 1
    } else {
      this.invAddExRate = null
    }
  }

  applyRoundByDecimal(amount: number, decimalPlaces: number): number {
    let newAmount = Number(parseFloat(amount + '').toFixed(decimalPlaces));
    return newAmount
  }

  getItemsTotal(): number {
    if (!isArrayValid(this.invoiceItems, 0))
      return 0
    const _total = this.invoiceItems.map(_it => this.applyRoundByDecimal(_it.Amount * _it.ExchangeRate, 2))
      .reduce((all, _it) => (all + _it), 0)
    return _total
  }

  getChargeType = (_code) => {
    try {
      return this.surchageType.find(_chg => _chg.code === _code).label
    } catch {
      return _code
    }
  }

  autosize() {
    const el = this.lineItem.nativeElement;
    setTimeout(() => {
      el.style.cssText = 'height:auto; padding:0';
      el.style.cssText = 'height:' + el.scrollHeight + 'px';
    }, 0)
  }


}
