export interface InvoiceDocument {
    DocumentTypeID: number
    DocumentTypeCode: string
    DocumentTypeName: string
    DocumentTypeNameOL?: any
    DocumentTypeDesc: string
    SortingOrder: number
    DocumentNature: string
    DocumentSubProcess: string
    DocumentID: number
    UserID?: any
    BookingID: number
    CompanyID?: any
    ProviderID?: any
    WHID?: any
    VesselID?: any
    DocumentName: string
    DocumentDesc: string
    DocumentFileName: string
    DocumentFile?: any
    DocumentFileContent: string
    DocumentUploadedFileType: string
    DocumentLastStatus: string
    ExpiryStatusCode: string
    ExpiryStatusMessage: string
    DocumentUploadDate?: any
    IsDownloadable: boolean
    IsApprovalRequired: boolean
    BusinessLogic: string
    CopyOfDocTypeID?: any
    ReasonID?: any
    ReasonCode: string
    ReasonName: string
    DocumentStausRemarks: string
    IsUploadable: boolean
    MetaInfoKeysDetail?: any
    FileContent?: any
    StatusAction: string
    ProviderName: string
    EmailTo: string
    PhoneTo: string
    UserName: string
    UserCompanyID: number
    UserCompanyName: string
    UserCountryPhoneCode: number
    HashMoveBookingNum: string
    ContainerLoadType?: any
    LoginUserID: number
    IsProvider: boolean
    JsonUploadSource: string
    AllowedFileTypes: string
    OtherID?: any
    OtherType?: any
    CreatedByUserName?: any
    CreatedByUserCompanyName?: any
    UploadedBy: string
    IsDefaultDocument: boolean
    ShowUpload: boolean
    HideCancel: boolean
}

export interface ISaveInvoice {
    InvoiceID: number
    InvoiceRefNo: string
    InvoiceRefDate: string
    CurrencyID: number
    ExchangeRate?: any
    ExRateListID?: any
    InvoiceAmount: number
    InvoiceDetail?: IInvoiceDetail[]
    InvoiceDocument: InvoiceDocument
}


export interface IInvoiceDetail {
    AddChrID?: number
    AddChrType?: number
    InvDtlID: number
    OtherAddChrText: string
    CurrencyID: number
    CurrencyCode: string
    // InvInputAmount: number
    Amount: number
    ExchangeRate: number
    ExRateListID?: number
}


export interface IBookingInvoiceDetail {
    InvDtlID: number;
    InvDtlDate: Date;
    InvoiceID: number;
    AddChrID?: any;
    OtherAddChrText: string;
    CurrencyID: number;
    Amount: number;
    ExchangeRate: number;
    ExRateListID?: any;
    IsDelete: boolean;
    IsActive: boolean;
    CreatedBy: string;
    CreatedDateTime: Date;
    ModifiedBy?: any;
    ModifiedDateTime?: any;
}

export interface IBookingInvoiceDoucument {
    DocumentFileName: string;
}

export interface IUploadedInvoiceDet {
    InvoiceID: number;
    InvoiceDate: Date;
    InvoiceNo: string;
    BookingID: number;
    InvoiceStatusID: number;
    InvoiceStatusRemarks: string;
    InvoiceStatusBL: string;
    InvoiceSource: string;
    CurrencyID: number;
    ExchangeRate?: any;
    ExRateListID?: any;
    InvoiceAmount: number;
    IsDelete: boolean;
    IsActive: boolean;
    CreatedBy: string;
    CreatedDateTime: Date;
    ModifiedBy: string;
    ModifiedDateTime: Date;
    InvoiceRefNo: string;
    InvoiceRefDate: Date;
    BookingInvoiceDetail: IBookingInvoiceDetail[];
    BookingInvoiceDoucument: IBookingInvoiceDoucument[];
}