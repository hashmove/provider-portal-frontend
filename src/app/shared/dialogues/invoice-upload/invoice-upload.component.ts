import { Component, OnInit, Input, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core'
import { DocumentFile, UserDocument } from '../../../interfaces/document.interface'
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { baseExternalAssets } from '../../../constants/base.url'
import { JsonResponse } from '../../../interfaces/JsonResponse'
import { ToastrService } from 'ngx-toastr'
import { ViewBookingService } from '../../../components/pages/user-desk/view-booking/view-booking.service'
import { DashboardService } from '../../../components/pages/user-desk/dashboard/dashboard.service'
import { cloneObject } from '../../../components/pages/user-desk/reports/reports.component'
import { getLoggedUserData, base64Encode, isArrayValid, getRoundedNum, isMobile } from '../../../constants/globalFunctions'
import { IInvoiceCard, IInvoiceTrail } from '../../../components/pages/user-desk/invoice-card/invoice-card.interface'
import { SharedService } from '../../../services/shared.service'
import { NgFilesSelected, NgFilesService, NgFilesStatus } from '../../../directives/ng-files'
import { ISaveInvoice } from './invoice.interface'
import { IInvoiceHelper } from './invoice-upload.helper'
import { ConfirmDialogContent, ConfirmDialogGenComponent } from '../confirm-dialog-generic/confirm-dialog-generic.component'
import * as moment from 'moment'

@Component({
  selector: 'app-invoice-upload',
  templateUrl: './invoice-upload.component.html',
  styleUrls: ['./invoice-upload.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class InvoiceUploadComponent extends IInvoiceHelper implements OnInit {

  @Input() invoice: IInvoiceCard
  @Input() selectedInvoice: IInvoiceTrail
  remarks

  isMobile = isMobile()

  constructor(
    public _activeModal: NgbActiveModal,
    public _toastr: ToastrService,
    private bookingService: ViewBookingService,
    private _userService: DashboardService,
    private _modalService: NgbModal,
    private _dataService: SharedService,
    private ngFilesService: NgFilesService,
  ) { super() }

  async ngOnInit() {
    this.loginUser = getLoggedUserData()
    const date = moment(new Date()).subtract('months', 3)
    this.minDate = { month: date.month() + 1, day: date.date(), year: date.year() }
    this._dataService.currencyList.subscribe(state => {
      this.allCurrencies = isArrayValid(state, 0) ? state : []
    })
    this.setData()
  }

  async setData() {
    this.loading = true
    if (this.selectedInvoice.invoiceID > 0)
      await this.getUploadedDaInvData()
    else
      this.isInvoiceEditable = true
    this.getUploadedDocuments()
  }

  async getUploadedDaInvData() {
    try {
      const res = await this.bookingService.getAddedInvoice(this.selectedInvoice.invoiceID).toPromise() as any
      if (res.returnId > 0) {
        this.uploadedInvoiceDet = res.returnObject
        const { uploadedInvoiceDet } = this
        this.invoiceNumber = uploadedInvoiceDet.InvoiceRefNo ? uploadedInvoiceDet.InvoiceRefNo : null
        this.invoiceAmount = uploadedInvoiceDet.InvoiceAmount ? uploadedInvoiceDet.InvoiceAmount : null
        if (uploadedInvoiceDet.InvoiceRefDate) {
          const _date = new Date(uploadedInvoiceDet.InvoiceRefDate);
          this.invoiceDate = { year: _date.getFullYear(), month: _date.getMonth() + 1, day: _date.getDate() };
        }
        if (isArrayValid(uploadedInvoiceDet.BookingInvoiceDetail, 0)) {
          this.invoiceItems = uploadedInvoiceDet.BookingInvoiceDetail as any
        }
        if (uploadedInvoiceDet.CurrencyID) {
          try {
            this.selectedCurrency = this.allCurrencies.find(_cur => _cur.id === uploadedInvoiceDet.CurrencyID)
          } catch (error) { }
        }
        if (uploadedInvoiceDet.InvoiceStatusRemarks)
          this.remarks = uploadedInvoiceDet.InvoiceStatusRemarks
        this.isInvoiceEditable = uploadedInvoiceDet.InvoiceStatusBL === 'INVOICE_REJECTED' ? true : false
        this.setInvAmountState()
      }
    } catch {

    }
  }

  getDefaultDocs(docTypeID) {
    setTimeout(async () => {
      try {
        await this.getDocumentDtl(docTypeID)
      } catch (error) {
        console.log(error)
      }
    }, 0)
  }

  getUploadedDocuments() {
    this._userService.getInvoiceDocs(this.invoice.BookingID, this.selectedInvoice.invoiceID ? this.selectedInvoice.invoiceID : 0).subscribe((res: JsonResponse) => {
      this.loading = false
      const _additionalDocuments = res.returnId > 0 && isArrayValid(res.returnObject, 0) ?
        res.returnObject.filter(_doc => _doc.BusinessLogic === 'FF_HM_BOOKING_INVOICE') : []
      if (this.isInvoiceEditable) {
        this.additionalDocuments = _additionalDocuments
        this.getDefaultDocs(this.additionalDocuments[0].DocumentTypeID)
      } else {
        this.additionalDocuments = _additionalDocuments.filter(_doc => _doc.InvoiceID === this.selectedInvoice.invoiceID)
      }
    }, () => {
      this.loading = false
    })
  }

  refreshDoc() {
    this.loading = true
    this.setData()
  }

  async getDocumentDtl(selectedDocument) {
    const { additionalDocuments } = this
    try {
      const res = await this.bookingService.getDocObject(selectedDocument).toPromise() as any
      if (res.returnId > 0) {
        const _newDoc: UserDocument = { ...res.returnObject, ShowUpload: false, HideCancel: true }
        this.currentDocObject = _newDoc
        const _arr = [...additionalDocuments, _newDoc]
        this.additionalDocuments = _arr
        try {
          const _allFiles = this.additionalDocuments[0].AllowedFileTypes.toLowerCase()
          const _allowedFiles = _allFiles.split(',')
          if (isArrayValid(_allowedFiles, 0)) {
            const _allowedFilesStr = _allowedFiles.join(', ')
            this.docText = `(supported types: ${_allowedFilesStr}. Maximum size per document: 10MB)`
            this.config.acceptExtensions = _allowedFiles
            this.ngFilesService.addConfig(this.config, 'config')
          } else {
            this.ngFilesService.addConfig(this.config, 'config')
          }
        } catch {
          this.ngFilesService.addConfig(this.config, 'config')
        }
        this.isDocConfigSet = true
      } else {
        this.ngFilesService.addConfig(this.config, 'config')
        this.isDocConfigSet = true
        this._toastr.error(res.returnText, res.returnStatus)
      }
    } catch (err) {
      console.log('doc_set_err:', err)
    }
  }

  acDownloadAction($url: string, $event) {
    $event.preventDefault()
    $event.stopPropagation()
    if ($url && $url.length > 0) {
      if ($url.startsWith('[{'))
        window.open(baseExternalAssets + JSON.parse($url)[0].DocumentFile, '_blank')
      else
        window.open(baseExternalAssets + $url, '_blank')
      if (this.invoice.BookingStatus.toLowerCase() === 'cancelled')
        return
    }
  }

  selectDocx(selectedFiles: NgFilesSelected): void {
    if (selectedFiles.status !== NgFilesStatus.STATUS_SUCCESS) {
      if (selectedFiles.status == 1) this._toastr.error('Please select 12 or less file(s) to upload.', '')
      else if (selectedFiles.status == 2) this._toastr.error('File size should not exceed 10 MB. Please upload smaller file.', '')
      else if (selectedFiles.status == 4) this._toastr.error('File format is not supported. Please upload supported format file.', '')
      return
    } else {
      this.onFileChange(selectedFiles)
    }
  }

  onFileChange(event) {
    if (event) {
      try {
        const reader = new FileReader()
        const file = event.files[0]
        reader.readAsDataURL(file)
        reader.onload = () => {
          const selectedFile: DocumentFile = {
            fileName: file.name,
            fileType: file.name.substring(file.name.lastIndexOf('.') + 1, file.name.length),
            fileUrl: reader.result,
            fileBaseString: (reader as any).result.split(',').pop()
          }
          if (event.files.length <= this.config.maxFilesCount) {
            this.currentDocObject.DocumentName = selectedFile.fileName
            this.currentDocObject.DocumentFileContent = selectedFile.fileBaseString
            this.currentDocObject.DocumentUploadedFileType = selectedFile.fileType
          } else
            this._toastr.error('Please select only ' + this.config.maxFilesCount + 'file to upload', '')
        }
      } catch (err) {
        console.log(err)
      }
    }
  }

  uploadDocument(document: UserDocument) {
    let toSend: UserDocument = cloneObject(document)
    try { toSend.DocumentName = this.currentDocObject.DocumentName } catch { }
    try { toSend.DocumentFileContent = this.currentDocObject.DocumentFileContent } catch { }
    try { toSend.DocumentUploadedFileType = this.currentDocObject.DocumentUploadedFileType } catch { }

    if (!toSend.DocumentFileContent && !toSend.DocumentFileName) {
      this._toastr.error('Please select a file to upload', 'Invalid Operation')
      this.loading = false
      return
    }
    const _docId = this.additionalDocuments[0].DocumentID ? this.additionalDocuments[0].DocumentID : -1
    toSend.DocumentID = this.getNum(this.selectedInvoice.invoiceID) > 0 ? _docId : -1
    toSend.InvoiceID = this.selectedInvoice.invoiceID
    toSend.OtherID = null
    toSend.OtherType = null
    toSend.LoginUserID = this.loginUser.UserID
    toSend.DocumentLastStatus = (toSend.DocumentID > 0 && toSend.DocumentFileName) ?
      toSend.DocumentLastStatus.toLowerCase() === 'approved' ? 'RESET' : 'RE-UPLOAD' :
      toSend.DocumentLastStatus = toSend.IsApprovalRequired ? 'DRAFT' : null
    toSend.BookingID = this.invoice.BookingID
    toSend.IsProvider = true
    toSend.UploadedBy = 'PROVIDER'
    toSend.InvoiceID = this.invoice.InvoiceInfo && isArrayValid(this.invoice.InvoiceInfo, 0) ? this.invoice.InvoiceInfo[0].InvoiceID : 0
    try {
      toSend.ProviderName = ''
      toSend.HashMoveBookingNum = this.invoice.HashMoveBookingNum
      toSend.UserName = ''
      toSend.UserCompanyName = ''
      toSend.UserCompanyID = this.invoice.Customer.CustomerID
      toSend.UserCountryPhoneCode = '000'
      toSend.EmailTo = ''
      toSend.PhoneTo = ''
    } catch (error) { }
    return toSend
  }

  close = () => this._activeModal.close()

  closeFix(event, datePicker) {
    if (event.target.offsetParent == null || event.target.offsetParent.nodeName != 'NGB-DATEPICKER')
      datePicker.close()
  }

  onInvAmountChange() {
    this.setInvAmountState()
    if (!this.prevCurrency || !this.prevCurrency.id) {
      this.prevCurrency = this.selectedCurrency
    } else if (this.prevCurrency && this.selectedCurrency &&
      this.prevCurrency.id && this.selectedCurrency.id &&
      this.prevCurrency.id !== this.selectedCurrency.id &&
      isArrayValid(this.invoiceItems, 0)
    ) {
      const _modalData: ConfirmDialogContent = {
        messageTitle: 'Change Currency',
        messageContent: `By Changing the currency, you will loose all invoice items added, do you want to continue?`,
        buttonTitle: 'Yes, I would like to change',
        data: null
      }
      this.confirmCurrChange(_modalData).then(state => {
        if (state) {
          this.prevCurrency = this.selectedCurrency
          this.invoiceItems = []
        } else {
          this.selectedCurrency = this.prevCurrency
        }
        this.onItemCurrChange()
      })
    } else {
      this.onItemCurrChange()
    }
  }

  setInvAmountState() {
    this.isAddItemAllowed =
      (this.getNum(this.invoiceAmount) && this.selectedCurrency && this.selectedCurrency.id) ? true : false
  }

  confirmCurrChange = (_modalData) => new Promise((resolve, reject) => {
    const modalRef = this._modalService.open(ConfirmDialogGenComponent, {
      size: 'lg', centered: true, windowClass: 'medium-modal', backdrop: 'static', keyboard: false
    })
    modalRef.componentInstance.modalData = _modalData
    modalRef.result.then((state: boolean) => {
      resolve(state)
    })
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open'))
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden'
    }, 0)
  })

  uploadInvoiceData() {
    const _uploadDoc = this.uploadDocument(this.additionalDocuments[0])
    if (!_uploadDoc)
      return
    if (!this.currentDocObject.DocumentFileContent) {
      this._toastr.error('Please select a file to upload', 'Invalid Operation')
      return
    }
    const toSend: ISaveInvoice = this.getInvSaveObject(_uploadDoc, this.selectedInvoice)
    if (!this.validateInvoice(toSend)) return
    if (toSend.InvoiceAmount > 0 || this.getItemsTotal() > 0) {
      const _modalData: ConfirmDialogContent = {
        messageTitle: 'Partner Invoice',
        messageContent: 'Are you sure the invoice amount and line items amount matches the original booking amount',
        buttonTitle: 'Yes',
        data: null
      }
      this.confirmCurrChange(_modalData).then(state => {
        if (state)
          this.saveAction(toSend)
      })
    } else {
      this.saveAction(toSend)
    }
  }

  saveAction(toSend) {
    this.loading = true
    this.bookingService.uploadBookingInvoice(toSend).subscribe((res: JsonResponse) => {
      this.loading = false
      if (res.returnId > 0) {
        this._toastr.success(res.returnText, res.returnStatus)
        this.close()
      } else {
        this._toastr.error(res.returnText, res.returnStatus)
      }

    }, () => {
      this.loading = false
      this._toastr.error('There was an error while processing your request, please try again later', 'Request Failed')
    })
  }

  validateInvoice(invData: ISaveInvoice) {
    let isInvoiceValid = true
    if (this.invoice.IsInvoiceDetRequiredOnUpload) {
      if (invData.InvoiceAmount > 0 || this.getItemsTotal() > 0) {
        const _invItemsTotal = this.applyRoundByDecimal(this.getItemsTotal(), 2)
        const _invTotal = this.applyRoundByDecimal(invData.InvoiceAmount, 2)
        if (_invItemsTotal !== _invTotal) {
          this._toastr.warning(`Line Item Total Amount not matching with Invoice Amount (Difference: ${_invTotal - _invItemsTotal})`, 'Invoice Amount')
          isInvoiceValid = false
        }
      }
    }
    return isInvoiceValid
  }


  getHtmlTxt = (_txt) => _txt.replaceAll('\n', '<br>')
}
