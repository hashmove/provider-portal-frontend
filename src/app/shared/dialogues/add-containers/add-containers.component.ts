import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ViewBookingService } from '../../../components/pages/user-desk/view-booking/view-booking.service';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { ToastrService } from 'ngx-toastr';
import { JsonResponse } from '../../../interfaces/JsonResponse';

@Component({
  selector: 'app-add-containers',
  templateUrl: './add-containers.component.html',
  styleUrls: ['./add-containers.component.scss']
})
export class AddContainersComponent implements OnInit {
  @Input() containerDetails: any
  public totalCount: number = 0
  constructor(
    public _activeModal: NgbActiveModal,
    private _viewBookingService: ViewBookingService,
    private _toastr: ToastrService
  ) { }

  ngOnInit() {
    console.log(this.containerDetails)
    if (this.containerDetails.BookingContainerDetail.length > 1) {
      // this.totalCount = this.containerDetails.BookingContainerDetail.reduce((a, b) => ({ BookingContTypeQty: a.BookingContTypeQty + b.BookingContTypeQty }));
      this.totalCount = this.containerDetails.BookingContainerDetail.reduce((acc, obj) => { return acc + obj.BookingContTypeQty; }, 0); // number
    } else {
      this.totalCount = this.containerDetails.BookingContainerDetail[0].BookingContTypeQty
    }
  }

  close() {
    this._activeModal.close(false)
  }

  // public isValidated: boolean = true
  saveContainer = () => {
    let bookingContainers = []
    let isValidated = true
    this.containerDetails.BookingContainerDetail.forEach(element => {
      element.JsonContainerInfo = JSON.stringify(element.parsedJsonContainerInfo)
      element.parsedJsonContainerInfo.forEach(container => {
        console.log(container.ContainerNo.length)
        if (container.ContainerNo && (container.ContainerNo.length < 11)) {
            isValidated = false
        }
      });
      let obj = {
        containerSpecID: element.ContainerSpecID,
        jsonContainerInfo: element.JsonContainerInfo
      }
      bookingContainers.push(obj)
    });
    if (!isValidated) {
      this._toastr.warning('The container number must correspond to the ISO-Container Standard of 11 characters', 'Warning')
      return false;
    }
    let obj = {
      bookingID: this.containerDetails.BookingID,
      bookingContainers: [...bookingContainers]
    }
    this._viewBookingService.saveContainerDetails(obj).subscribe((res: JsonResponse) => {
      const { returnId, returnText, returnStatus } = res
      if (returnId > 0) {
        this._toastr.success('Container number updated successfully', returnStatus)
        this._activeModal.close(true)
      } else {
        this._toastr.error(returnText, returnStatus)
      }
    }, (err: any) => {
      this._toastr.error("Error wile updating please try later", 'Error')
      this._activeModal.close(false)
    })
  }

}
