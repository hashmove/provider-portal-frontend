import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarehouseRateDialogComponent } from './warehouse-rate-dialog.component';

describe('WarehouseRateDialogComponent', () => {
  let component: WarehouseRateDialogComponent;
  let fixture: ComponentFixture<WarehouseRateDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarehouseRateDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarehouseRateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
