import { Component, OnInit, ViewEncapsulation, EventEmitter, ViewChild, Renderer2, ElementRef, Input, Output } from "@angular/core";
import { PlatformLocation } from "@angular/common";
import { NgbActiveModal, NgbDropdownConfig } from "@ng-bootstrap/ng-bootstrap";
import { SharedService } from "../../../services/shared.service";
import { Observable, Subject, Subscription } from "rxjs";
import { ToastrService } from "ngx-toastr";
import { debounceTime, distinctUntilChanged, map } from "rxjs/operators";
import { NgbDatepicker, NgbInputDatepicker, NgbDateStruct, NgbCalendar, NgbDateAdapter, NgbDateParserFormatter, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { NgbDateFRParserFormatter } from "../../../constants/ngb-date-parser-formatter";
import { SeaFreightService } from "../../../components/pages/user-desk/manage-rates/sea-freight/sea-freight.service";
import { cloneObject } from "../../../components/pages/user-desk/reports/reports.component";
import { changeCase, loading, getImagePath, ImageSource, ImageRequiredSize, removeDuplicates, getLoggedUserData, isMobile } from "../../../constants/globalFunctions";
import * as moment from "moment";
import { ManageRatesService } from "../../../components/pages/user-desk/manage-rates/manage-rates.service";
import { WHModel } from "../../../interfaces/warehouse.interface";
import { firstBy } from 'thenby'
import { WarehousePricing, isNumber } from "../sea-rate-dialog/sea-rate-dialog.component";
import { Container } from "@angular/compiler/src/i18n/i18n_ast";
import { ContainerDetail } from "../../../interfaces/view-booking.interface";
import { CodeValMst } from "../../../interfaces/billing.interface";

const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
  one &&
  two &&
  two.year === one.year &&
  two.month === one.month &&
  two.day === one.day;

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two
    ? false
    : one.year === two.year
      ? one.month === two.month
        ? one.day === two.day
          ? false
          : one.day < two.day
        : one.month < two.month
      : one.year < two.year;

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two
    ? false
    : one.year === two.year
      ? one.month === two.month
        ? one.day === two.day
          ? false
          : one.day > two.day
        : one.month > two.month
      : one.year > two.year;
@Component({
  selector: "app-warehouse-rate-dialog",
  templateUrl: "./warehouse-rate-dialog.component.html",
  encapsulation: ViewEncapsulation.None,
  providers: [
    { provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter },
    NgbDropdownConfig
  ],
  styleUrls: ["./warehouse-rate-dialog.component.scss"],
})
export class WarehouseRateDialogComponent implements OnInit {
  @ViewChild("dp") input: NgbInputDatepicker;
  @ViewChild("rangeDp") rangeDp: ElementRef;

  @Input() selectedData: any;
  @Output() savedRow = new EventEmitter<any>();
  @Input() warehouseUnit: string = null;

  @ViewChild("container") container: ElementRef;

  citiesResults: Object;
  searchTerm$ = new Subject<string>();


  public allContainersType: any[] = [];
  public allContainers: any[] = [];
  public allHandlingType: any[] = [];
  public allCustomers: any[] = [];
  public allCurrencies: any[] = [];
  private allRatesFilledData: any[] = [];
  public filterOrigin: any = {};
  public filterDestination: any = {};
  public userProfile: any;
  public selectedCategory: any = null;
  public selectedContSize: any = null;
  public containerType: string = 'empty'
  public selectedHandlingUnit: any;
  public selectedCustomer: any[] = [];
  public selectedShipping: any;

  public defaultCurrency: any = {
    id: 101,
    shortName: "USD",
    imageName: 'US'
  };
  public selectedCurrency: any = this.defaultCurrency;
  public whCurrency: any = this.defaultCurrency;
  public startDate: NgbDateStruct;
  public maxDate: NgbDateStruct;
  public minDate: NgbDateStruct;
  public hoveredDate: NgbDateStruct;
  public fromDate: any = {
    day: null,
    month: undefined,
    year: undefined
  };
  public toDate: any = {
    day: undefined,
    month: undefined,
    year: undefined
  };

  public model: any;
  private newProviderPricingDraftID = undefined;
  public disableWarehouse: boolean = false;
  isHovered = date =>
    this.fromDate &&
    !this.toDate &&
    this.hoveredDate &&
    after(date, this.fromDate) &&
    before(date, this.hoveredDate);
  isInside = date => after(date, this.fromDate) && before(date, this.toDate);
  isFrom = date => equals(date, this.fromDate);
  isTo = date => equals(date, this.toDate);



  public originsList = [];

  public selectedOrigins: any = [{}];
  public disabledCustomers: boolean = false;
  public containerLoadParam: string = "FCL";
  public userCurrency: number;
  public TotalImportCharges: number = 0;
  public TotalExportCharges: number = 0;
  public warehouseTypes: any[] = [];
  public storageType: string = "";
  whPricingID: any;
  public pricingJSON: any[] = [];

  singlePriceTag: string = ''
  singlePriceTagDed: string = ''
  doublePriceTag: string = ''
  thirdPriceTag: string = ''
  thirdPriceTagDed: string = ''
  doublePriceTagDed: string = ''
  public loading: boolean = false

  selectedWHPriceType: { name: string, code: string } = { name: 'Per Day', code: 'PER_CONT_PER_DAY' }
  whPriceTypeList = [
    { name: 'Per Day', code: 'PER_CONT_PER_DAY' },
    { name: 'Per Month', code: 'PER_CONT_PER_MONTH' },
    { name: 'Per Year', code: 'PER_CONT_PER_YEAR' }
  ]
  whPrice = null
  // allCargoType: any[];
  currentContainerName: string = ''
  @ViewChild("priceBasis") priceBasis: any;
  @ViewChild("eladdOnChargePrice") eladdOnChargePrice: any;
  addOnCharge: CodeValMst
  addOnChargePrice: any
  whAddsOn: CodeValMst[] = []
  masterWhAddsOn: CodeValMst[] = []
  tempAddOnBasis
  disableElAddOn = false

  isMobile = isMobile()
  displayMonth = this.isMobile ? 1 : 2

  constructor(
    private location: PlatformLocation,
    private _activeModal: NgbActiveModal,
    private _sharedService: SharedService,
    private _parserFormatter: NgbDateParserFormatter,
    private _manageRateService: ManageRatesService,
    private renderer: Renderer2,
    private _seaFreightService: SeaFreightService,
    private _toast: ToastrService,
    private config: NgbDropdownConfig,
    calendar: NgbCalendar
  ) {
    location.onPopState(() => this.closeModal(null));
    config.autoClose = false;
    // this.fromDate = calendar.getToday();
  }

  ngOnInit() {
    try {
      const date = new Date();
      this.minDate = { month: date.getMonth() + 1, day: date.getDate(), year: date.getFullYear() };
    } catch (error) { }
    // console.log(this.selectedData)
    this._sharedService.currencyList.subscribe(res => {
      if (res) {
        this.allCurrencies = res;
      }
    });
    this.userProfile = getLoggedUserData()
    this.setCurrency();
    this.setDateLimit();
    this.containerLoadParam = this.selectedData.forType
    if (this.selectedData.forType === "WAREHOUSE") {
      this.warehouseTypes = this.selectedData.drafts;
      this.getWarehousePricing();
    }
    try {
      const parsedPricingJson = JSON.parse(this.selectedData.data.pricingJson)[0];
      // console.log(parsedPricingJson)
      if (parsedPricingJson.containerSpecID) {
        const _containerId = parseInt(parsedPricingJson.containerSpecID)
        // console.log(_containerId)
        this.selectedContSize = _containerId
        // console.log(this.selectedContSize)
      }
      if (parsedPricingJson.containerType) {
        this.containerType = parsedPricingJson.containerType
      }

      if (parsedPricingJson.priceBasis) {
        const _selectedBasis = this.whPriceTypeList.filter(_basis => _basis.code === parsedPricingJson.priceBasis)[0]
        this.selectedWHPriceType = _selectedBasis
        this.onWHPriceType(this.selectedWHPriceType.code)
      } else {
        this.onWHPriceType(this.selectedWHPriceType.code)
      }
    } catch (error) {
      // console.log(error)
    }
    this.allservicesBySea();
    if (this.selectedData.mode === "draft") {
      const _originsList = this.selectedData.addList;
      this.originsList = _originsList.filter(_charge => _charge.addChrBasis == 'PER_BOOKING' || _charge.addChrBasis === 'PER_CONTAINER')
    } else if (this.selectedData.mode === "publish") {
      if (this.selectedData.data && (this.selectedData.data.JsonSurchargeDet || this.selectedData.data.JsonDedicatedSurchargeDet)) {
        this.setEditData(this.selectedData.mode);
      } else {
        const _originsList = this.selectedData.addList;
        this.originsList = _originsList.filter(_charge => _charge.addChrBasis == 'PER_BOOKING' || _charge.addChrBasis === 'PER_CONTAINER')
      }
    }
    this.allCustomers = this.selectedData.customers;
    this.getSurchargeBasis(this.containerLoadParam);
  }

  allservicesBySea() {
    this.getDropdownsList();
    if (
      this.selectedData &&
      this.selectedData.data &&
      this.containerLoadParam === "WAREHOUSE"
    ) {
      if (this.selectedData.mode === "publish") {
        this.selectedData.data = changeCase(this.selectedData.data, "pascal");
        this.disabledCustomers = true;
        this.setData(this.selectedData.data);
      } else {
        this.setData(this.selectedData.data);
      }
    }
  }

  setDateLimit() {
    const date = new Date();
    this.minDate = {
      year: date.getFullYear(),
      month: date.getMonth() + 1,
      day: date.getDate()
    };
    // this.maxDate = {
    //   year: ((this.minDate.month === 12 && this.minDate.day >= 17) ? date.getFullYear() + 1 : date.getFullYear()),
    //   month:
    //     moment(date)
    //       .add(15, "days")
    //       .month() + 1,
    //   day: moment(date)
    //     .add(15, "days")
    //     .date()
    // };
  }

  setData(data) {
    // console.log(data)
    let parsed = "";
    this.selectedCategory = data.ShippingCatID;
    // this.selectedContSize = data.ContainerSpecID;
    // console.log('warehouseData:', data)

    if (this.selectedData.forType === 'WAREHOUSE' && this.selectedData.mode === "publish") {
      this.storageType = data.StorageType;
      const parsedPricingJson = JSON.parse(data.PricingJson);
      this.sharedWarehousePricing = parsedPricingJson;
      this.disableWarehouse = true;
      const { sharedWarehousePricing } = this;
      // console.log(sharedWarehousePricing)
      try {
        sharedWarehousePricing.forEach(adCharge => {
          const { priceBasis, price } = adCharge;
          if (priceBasis.includes('PER_CONT')) {
            this.whPrice = price;
            this.tempAddOnBasis = adCharge.addOnBasis
            this.addOnChargePrice = adCharge.addOnPrice
          }
        });
      } catch (error) {
      }

      this.selectedCurrency = this.allCurrencies.find(
        obj => obj.id === this.selectedData.data.CurrencyID
      );
      this.whCurrency = this.allCurrencies.find(
        obj => obj.id === this.selectedData.data.DedicatedCurrencyID
      );
    } else if (this.selectedData.forType === 'WAREHOUSE' && this.selectedData.mode === "draft") {
      this.disabledCustomers = false;
    } else {
      this.disabledCustomers = true;
      this.whPrice = data.Price;
      this.selectedCurrency = this.allCurrencies.find(
        obj => obj.id === data.CurrencyID
      );
    }

    this.containerChange(data.ContainerSpecID);

    if (data.JsonCustomerDetail && data.CustomerType !== "null") {
      this.selectedCustomer = JSON.parse(data.JsonCustomerDetail);
    }

    // console.log(data.JsonContainerDetail)

    if (data.JsonContainerDetail) {
      try {
        const _selectdContainers = JSON.parse(data.JsonContainerDetail)
        this.selectedContainers = this.ftlContainers.filter(_cont => _selectdContainers.ContainerSpecID === _cont.ContainerSpecID);
      } catch (error) {
        console.log(error)
      }
    }

    if (data.EffectiveFrom) {
      this.fromDate.day = new Date(data.EffectiveFrom).getDate();
      this.fromDate.year = new Date(data.EffectiveFrom).getFullYear();
      this.fromDate.month = new Date(data.EffectiveFrom).getMonth() + 1;
    }
    if (data.EffectiveTo) {
      this.toDate.day = new Date(data.EffectiveTo).getDate();
      this.toDate.year = new Date(data.EffectiveTo).getFullYear();
      this.toDate.month = new Date(data.EffectiveTo).getMonth() + 1;
    }
    if (!this.selectedCurrency) {
      this.selectedCurrency = this.defaultCurrency;
    }

    if (this.fromDate && this.fromDate.day) {
      this.model = this.fromDate;
      parsed += this._parserFormatter.format(this.fromDate);
    }
    if (this.toDate && this.toDate.day) {
      parsed += " - " + this._parserFormatter.format(this.toDate);
    }
    this.rangeDp.nativeElement.value = parsed;

  }


  isRateUpdating = false;
  /**
   * [On Save Button Click Action]
   * @param  type [string] fcl/lcl/ftl/fcl-ground
   * @return      [description]
   */
  savedraftrow(type) {
    this.loading = true
    if (this.isRateUpdating) {
      return;
    }
    this.isRateUpdating = true;
    if (type !== "update") {
      this.saveDraft(type);
    } else if (type === "update") {
      this.updatePublishedRate(this.containerLoadParam.toLowerCase());
    }
  }

  /**
   * [Udpdate Published Record Button Action]
   * @param  type [string]
   * @return [description]
   */
  updatePublishedRate(type) {
    let rateData = [];
    // console.log(this.selectedData.data)


    let JsonSurchargeDet: any

    try {
      let _originCharge = null
      let _destCharge = null
      try {
        _originCharge = (JSON.stringify(this.selectedOrigins) === '[{}]') ? null : this.selectedOrigins

      } catch (error) {
        console.log(error);
      }
      if (!_originCharge && !_destCharge) {
        JsonSurchargeDet = null
      } else if (_originCharge && !_destCharge) {
        JsonSurchargeDet = JSON.stringify(_originCharge)
      } else if (!_originCharge && _destCharge) {
        JsonSurchargeDet = JSON.stringify(_destCharge)
      } else if (_originCharge && _destCharge) {
        JsonSurchargeDet = JSON.stringify(_originCharge.concat(_destCharge))
      }
    } catch (error) { }
    try {
      if (!this.fromDate || this.fromDate === null) {
        this._toast.warning("Please enter a valid date range for this rate");
        this.isRateUpdating = false;
        this.loading = false
        return;
      }

      if (!this.toDate || this.toDate === null) {
        this._toast.warning("Please enter a valid date range for this rate");
        this.isRateUpdating = false;
        this.loading = false
        return;
      }
      if (this.selectedData.forType === "WAREHOUSE") {
        if (this.selectedData.data.UsageType === "SHARED") {
          if (!this.validateAdditionalCharges()) {
            this.loading = false
            return;
          }
        }
        //khatam

        if (!this.whPrice || this.whPrice === 0) {
          this._toast.error("Price fields cannot be zero", "Error");
          this.isRateUpdating = false;
          this.loading = false
          return;
        }
        if (!this.addOnChargePrice || this.addOnChargePrice === 0) {
          this._toast.error("Addon price cannot be zero", "Error");
          this.isRateUpdating = false;
          this.loading = false
          return;
        }

        if (!this.fromDate || this.fromDate === null) {
          this._toast.warning("Effective from Cannot be empty");
          this.isRateUpdating = false;
          this.loading = false
          return;
        }


        if (!this.toDate || this.toDate === null) {
          this._toast.warning("Effective to Cannot be empty");
          this.isRateUpdating = false;
          this.loading = false
          return;
        }

        if (!this.selectedContSize) {
          this._toast.warning("Please select a container");
          this.isRateUpdating = false;
          this.loading = false
          return;
        }
        if (!this.containerType) {
          this._toast.warning("Please select container Type");
          this.isRateUpdating = false;
          this.loading = false
          return;
        }

        this.calculatePricingJSON();
        const toSend = {
          containerSpecID: null,
          containerLoadType: null,
          storageType: 'Dry',
          whPricingID: this.selectedData.data.WhPricingID,
          pricingJson: JSON.stringify(this.pricingJSON),
          effectiveFrom:
            this.fromDate && this.fromDate.month ? this.fromDate.month + "/" + this.fromDate.day + "/" + this.fromDate.year : null,
          effectiveTo:
            this.toDate && this.toDate.month ? this.toDate.month + "/" + this.toDate.day + "/" + this.toDate.year : null,
          modifiedBy: this.userProfile.UserID,
          jsonSurchargeDet: JsonSurchargeDet,
          customerID: this.selectedData.data.CustomerID,
          jsonCustomerDetail: this.selectedData.data.JsonCustomerDetail,
          jsonContainerDetail: this.selectedData.data.JsonContainerDetail,
          customerType: this.selectedData.data.CustomerType
        };

        rateData.push(toSend);
      }
    } catch (error) {
      console.log(error)
      this.isRateUpdating = false;
      this.loading = false
    }

    this._seaFreightService.rateValidityFCL(type, rateData).subscribe(
      (res: any) => {
        loading(false);
        this.loading = false
        this.isRateUpdating = false;
        this.loading = false
        if (res.returnId > 0) {
          if (res.returnText && typeof res.returnText === 'string') {
            this._toast.success(res.returnText, "Success");
          } else {
            this._toast.success("Rates added successfully", "Success");
          }
          this.closeModal(true);
        } else {
          this._toast.warning(res.returnText);
        }
      },
      error => {
        this.isRateUpdating = false;
        this.loading = false
        loading(false);
        this.loading = false
        this._toast.error("Error while saving rates, please try later");
      }
    );
  }

  addRowLCL() {
    this._seaFreightService
      .addDraftRatesLCL({
        createdBy: this.userProfile.UserID,
        providerID: this.userProfile.ProviderID
      })
      .subscribe((res: any) => {
        if (res.returnStatus == "Success") {
          this._sharedService.draftRowLCLAdd.next(res.returnObject);
          this.newProviderPricingDraftID = undefined;
          this.whPrice = undefined;
          this.selectedHandlingUnit = null;
          this.newProviderPricingDraftID =
            res.returnObject.ConsolidatorPricingDraftID;
        }
        this.loading = false
      });
  }

  /**
   * Save Draft Row in Drafts Table
   * [Saving the draft record ]
   * @param {string}  type [description]
   * @return      [description]
   */
  public buttonLoading: boolean = false;
  saveDraft(type) {
    this.buttonLoading = true;
    let objDraft: any;
    try {

      let customers = [];
      if (this.selectedCustomer.length) {
        this.selectedCustomer.forEach(element => {
          let obj = {
            CustomerID: element.CustomerID,
            CustomerType: element.CustomerType,
            CustomerName: element.CustomerName,
            CustomerImage: element.CustomerImage
          };
          customers.push(obj);
        });
      }
      let totalExp = [];
      const expCharges = this.selectedOrigins.filter(
        e => e.Imp_Exp === "EXPORT"
      );


      if (expCharges && expCharges.length) {
        expCharges.forEach(element => {
          totalExp.push(parseFloat(element.Price));
        });
        this.TotalExportCharges = totalExp.reduce((all, item) => {
          return all + item;
        });
      }
      if (this.selectedData.forType === "WAREHOUSE") {
        this.transPortMode = "WAREHOUSE";
        this.calculatePricingJSON();
      }

      let JsonSurchargeDet: any
      let _originCharge = null
      let _destCharge = null

      try {
        _originCharge = (JSON.stringify(this.selectedOrigins) === '[{}]') ? null : this.selectedOrigins
      } catch (error) {
        console.log(error);

      }
      if (!_originCharge && !_destCharge) {
        JsonSurchargeDet = null
      } else if (_originCharge && !_destCharge) {
        JsonSurchargeDet = JSON.stringify(_originCharge)
      } else if (!_originCharge && _destCharge) {
        JsonSurchargeDet = JSON.stringify(_destCharge)
      } else if (_originCharge && _destCharge) {
        JsonSurchargeDet = JSON.stringify(_originCharge.concat(_destCharge))
      }
      this.calculatePricingJSON();

      // containerSpecID: this.selectedContSize,
      // containerName: this.currentContainerName,
      // containerType: this.containerType


      if (!this.selectedContainers || this.selectedContainers.length === 0) {
        this._toast.warning("Please select a container");
        this.isRateUpdating = false;
        this.loading = false
        return;
      }

      const { selectedContainers } = this
      const _containerList = selectedContainers.map(_container => {
        return {
          ContainerSpecID: _container.ContainerSpecID,
          ContainerType: "EMPTY",
        }
      })

      objDraft = {
        // GROUND ID
        ID: this.selectedData.ID ? this.selectedData.ID : 0,

        // FCL ID
        providerPricingDraftID: this.selectedData.data
          ? this.selectedData.data.ProviderPricingDraftID
          : 0,

        // LCL ID
        consolidatorPricingDraftID: this.selectedData.data
          ? this.selectedData.data.ConsolidatorPricingDraftID
          : 0,

        customerID: this.selectedCustomer.length
          ? this.selectedCustomer[0].CustomerID
          : null,
        customersList: customers.length ? customers : null,
        providerID: this.userProfile.ProviderID,
        containerLoadType: null,
        transportType: this.transPortMode,
        modeOfTrans: this.transPortMode,
        priceBasis: null,
        providerLocationD: "",
        providerLocationL: "",
        price: this.whPrice,
        currencyID:
          this.selectedCurrency && this.selectedCurrency.id
            ? this.selectedCurrency.id
            : 101,
        currencyCode:
          this.selectedCurrency && this.selectedCurrency.shortName
            ? this.selectedCurrency.shortName
            : "USD",
        effectiveFrom:
          this.fromDate && this.fromDate.month
            ? this.fromDate.month +
            "/" +
            this.fromDate.day +
            "/" +
            this.fromDate.year
            : null,
        effectiveTo:
          this.toDate && this.toDate.month
            ? this.toDate.month + "/" + this.toDate.day + "/" + this.toDate.year
            : null,
        JsonSurchargeDet: JsonSurchargeDet === "[{},{}]" ? null : JsonSurchargeDet,
        TotalImportCharges: this.TotalImportCharges,
        TotalExportCharges: this.TotalExportCharges,
        createdBy: this.userProfile.UserID,

        //WAREHOUSE FIELDS
        storageType: 'Dry',
        whPricingID: 0,
        whid:
          this.selectedData.data && this.selectedData.data.WHID
            ? this.selectedData.data.WHID
            : null,
        pricingJson: JSON.stringify(this.pricingJSON),
        parentID: 0,
        whCurrencyID:
          this.whCurrency && this.whCurrency.id
            ? this.whCurrency.id
            : 101,
        whCurrencyCode:
          this.whCurrency && this.whCurrency.shortName
            ? this.whCurrency.shortName
            : "USD",
        DedicatedCurrencyID:
          this.whCurrency && this.whCurrency.id
            ? this.whCurrency.id
            : 101,
        containerList: _containerList,
        JsonDedicatedSurchargeDet: null,
      };

      // VALIDATIONS STARTS HERE


      if (!this.whPrice || this.whPrice === 0) {
        this._toast.error("Price fields cannot be zero", "Error");
        this.isRateUpdating = false;
        this.loading = false
        return;
      }

      if (!this.addOnChargePrice || this.addOnChargePrice === 0) {
        this._toast.error("Addon price cannot be zero", "Error");
        this.isRateUpdating = false;
        this.loading = false
        return;
      }

      if (
        objDraft.transportType === "SEA" ||
        objDraft.transportType === "GROUND" ||
        (this.transPortMode === "WAREHOUSE" &&
          this.selectedData.data.UsageType === "SHARED")
      ) {
        //Content of validateAdditionalCharges() function was here
        if (!this.validateAdditionalCharges()) {
          this.loading = false
          return;
        }
      }
    } catch (error) {
      console.log(error)
      this.isRateUpdating = false;
      this.loading = false
      return;
    }

    try {
      if (this.fromDate && this.fromDate.day && (!this.toDate || !this.toDate.day)) {
        this._toast.warning("Please enter a valid date range for this rate");
        this.isRateUpdating = false;
        this.loading = false
        return;
      }
    } catch (error) {
      console.log(error);
    }


    if (!this.containerType) {
      this._toast.warning("Please select container Type");
      this.isRateUpdating = false;
      this.loading = false
      return;
    }

    // console.log(JSON.stringify(objDraft))
    // setTimeout(() => {
    //   this.isRateUpdating = false;
    //   this.loading = false
    // }, 100);

    this._seaFreightService.saveWarehouseRate(objDraft).subscribe(
      (res: any) => {
        this.buttonLoading = false;
        this.isRateUpdating = false;
        this.loading = false
        if (res.returnId > 0) {
          this._toast.success(res.returnText, "Success");
          if (type === "onlySave") {
            this.closeModal(res.returnObject);
          } else {
            this.pricingJSON = [];
            if (this.selectedData.data) {
              this.selectedData.data.ProviderPricingDraftID = 0;
            }
            this.selectedContSize = null;
            this.whPrice = null
            this.containerType = 'empty';
            this.savedRow.emit(res.returnObject);
          }
        } else {
          this.pricingJSON = [];
          this._toast.warning(res.returnText);
        }
      },
      error => {
        this.isRateUpdating = false;
        this.loading = false
        this._toast.error("Error While saving, please try late");
      }
    );

  }

  validateAdditionalCharges(): boolean {
    let ADCHValidated: boolean = true;
    // let exportCharges
    // let importCharges
    // if (objDraft.JsonSurchargeDet) {
    //   const parsedJsonSurchargeDet = JSON.parse(obj.JsonSurchargeDet)
    //   exportCharges = parsedJsonSurchargeDet.filter(e => e.Imp_Exp === 'EXPORT')
    //   importCharges = parsedJsonSurchargeDet.filter(e => e.Imp_Exp === 'IMPORT')
    // }
    if (this.selectedOrigins && this.selectedOrigins.length > 0) {
      this.selectedOrigins.forEach(element => {
        if (
          Object.keys(element).length &&
          (!element.Price ||
            !(typeof parseFloat(element.Price) == "number") ||
            parseFloat(element.Price) <= 0)
        ) {
          this._toast.error("Price is missing for Additional Charge", "Error");
          this.isRateUpdating = false;
          ADCHValidated = false;
          return ADCHValidated;
        }
        if (Object.keys(element).length && !element.CurrId) {
          this._toast.error(
            "Currency is missing for Additional Charge",
            "Error"
          );
          this.isRateUpdating = false;
          ADCHValidated = false;
          return ADCHValidated;
        }
        if (Object.keys(element).length && (!element.currency || typeof (element.currency) === 'string')) {
          this._toast.error(
            "Currency is missing for Additional Charge",
            "Error"
          );
          this.isRateUpdating = false;
          ADCHValidated = false;
          return ADCHValidated;
        }
        if (Object.keys(element).length && !element.addChrID) {
          this._toast.error("Additional Charge is missing", "Error");
          this.isRateUpdating = false;
          ADCHValidated = false;
          return ADCHValidated;
        }
      });
    }

    return ADCHValidated;
  }

  addRow() {
    this._seaFreightService
      .addDraftRates({
        createdBy: this.userProfile.UserID,
        providerID: this.userProfile.ProviderID
      })
      .subscribe((res: any) => {
        if (res.returnStatus == "Success") {
          this._sharedService.draftRowFCLAdd.next(res.returnObject);
          this.newProviderPricingDraftID = undefined;
          this.whPrice = undefined;
          this.selectedContSize = null;
          this.newProviderPricingDraftID =
            res.returnObject.ProviderPricingDraftID;
        }
      });
  }

  numberValidwithDecimal(evt) {
    let charCode = (evt.which) ? evt.which : evt.keyCode;
    if (evt.target.value && evt.target.value[evt.target.value.length - 1] == '.') {
      if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

      return true;
    }
    if (charCode != 46 && charCode > 31
      && (charCode < 48 || charCode > 57))
      return false;

    return true;
  }

  getContSpecName(id) {
    return this.allContainers.find(obj => obj.ContainerSpecID == id)
      .ContainerSpecShortName;
  }


  getHandlingSpecName(id) {
    return this.allHandlingType.find(obj => obj.ContainerSpecID == id)
      .ContainerSpecShortName;
  }
  onDateSelection(date: NgbDateStruct) {
    if (this.selectedData.mode === "publish" && this.selectedData.data) {
      this.onDateSelectionEdit(date)
    } else {
      this.onDateSelectionNew(date)
    }
  }

  onDateSelectionEdit(date: NgbDateStruct) {
    let parsed = '';
    const _minDate = new Date(this.minDate.year, this.minDate.month - 1, this.minDate.day);
    const selectedDate = new Date(date.year, date.month - 1, date.day);
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && moment(selectedDate).isSameOrAfter(_minDate)) {
      const _fromDate = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
      if (moment(selectedDate).isAfter(_fromDate)) {
        this.toDate = date;
        this.input.close();
      }
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
    if (this.fromDate) {
      parsed += this._parserFormatter.format(this.fromDate);
    }
    if (this.toDate) {
      parsed += ' - ' + this._parserFormatter.format(this.toDate);
    }
    this.renderer.setProperty(this.rangeDp.nativeElement, 'value', parsed);
  }

  onDateSelectionNew(date: NgbDateStruct) {
    let parsed = "";
    const _minDate = new Date(this.minDate.year, this.minDate.month - 1, this.minDate.day);
    const selectedDate = new Date(date.year, date.month - 1, date.day);
    if (!this.fromDate && !this.toDate) {
      if (moment(selectedDate).isSameOrAfter(_minDate)) {
        this.fromDate = date;
      } else {
        this.fromDate = null
        return
      }
    } else if (this.fromDate && !this.toDate && moment(selectedDate).isAfter(_minDate)) {
      const _fromDate = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
      if (moment(selectedDate).isAfter(_fromDate)) {
        this.toDate = date;
        this.input.close();
      }
    } else {
      const selectedDate = new Date(date.year, date.month - 1, date.day);
      if (moment(selectedDate).isSameOrAfter(_minDate)) {
        this.toDate = null;
        this.fromDate = date;
      } else {
        this.fromDate = null
        return
      }
    }
    if (this.fromDate) { parsed += this._parserFormatter.format(this.fromDate); }
    if (this.toDate) { parsed += " - " + this._parserFormatter.format(this.toDate); }
    this.renderer.setProperty(this.rangeDp.nativeElement, 'value', parsed);
  }




  closeModal(status) {
    this._activeModal.close(status);
    if (this.containerLoadParam == "FCL") {
      this._sharedService.draftRowFCLAdd.next(null);
    } else if (this.containerLoadParam == "LCL") {
      this._sharedService.draftRowLCLAdd.next(null);
    }
    document.getElementsByTagName("html")[0].style.overflowY = "auto";
  }
  closePopup() {
    // let object = {
    //   data: this.allRatesFilledData
    // };
    this.closeModal(false);
  }

  formatter = (x: { title: string; imageName: string }) =>
    x.title;


  currencies = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term =>
        !term || term.length < 3
          ? []
          : this.allCurrencies.filter(
            v =>
              v.shortName &&
              v.shortName.toLowerCase().indexOf(term.toLowerCase()) > -1
          )
      )
    );
  currencyFormatter = x => x.shortName;

  selectCharges(type, model, index) {
    model.Imp_Exp = type;
    if (type === "EXPORT") {
      if (
        (Object.keys(this.selectedOrigins[index]).length === 0 &&
          this.selectedOrigins[index].constructor === Object) ||
        !this.selectedOrigins[index].hasOwnProperty("currency")
      ) {
        model.CurrId = this.selectedCurrency.id;
        model.currency = this.selectedCurrency;
      } else {
        model.CurrId = this.selectedOrigins[index].currency.id;
        model.currency = this.selectedOrigins[index].currency;
      }
      const { selectedOrigins } = this;
      selectedOrigins.forEach(element => {
        if (
          Object.keys(element).length === 0 &&
          element.constructor === Object
        ) {
          let idx = selectedOrigins.indexOf(element);
          selectedOrigins.splice(idx, 1);
        }
      });
      if (selectedOrigins[index]) {
        this.originsList.push(selectedOrigins[index]);
        selectedOrigins[index] = model;
      } else {
        selectedOrigins.push(model);
      }
      this.selectedOrigins = cloneObject(selectedOrigins);
      this.originsList = this.originsList.filter(
        e => e.addChrID && e.addChrID !== model.addChrID
      );
      // console.log(this.originsList)
    }
  }


  getVal(idx, event, type) {
    if (typeof event === "object") {
      if (type === "origin") {
        this.selectedOrigins[idx].CurrId = event.id;
      }
    }
  }

  addMoreCharges(type) {
    if (type === "origin") {
      if (!this.selectedOrigins[this.selectedOrigins.length - 1].CurrId) {
        this._toast.info("Please select currency", "Info");
        return;
      }
      if (!this.selectedOrigins[this.selectedOrigins.length - 1].Price) {
        this._toast.info("Please add price", "Info");
        return;
      }
      if (!this.selectedOrigins[this.selectedOrigins.length - 1].addChrCode) {
        this._toast.info("Please select any additional charge", "Info");
        return;
      }
      if (
        !(
          Object.keys(this.selectedOrigins[this.selectedOrigins.length - 1])
            .length === 0 &&
          this.selectedOrigins[this.selectedOrigins.length - 1].constructor ===
          Object
        ) &&
        parseFloat(this.selectedOrigins[this.selectedOrigins.length - 1].Price) &&
        this.selectedOrigins[this.selectedOrigins.length - 1].CurrId
      ) {
        this.selectedOrigins.push({
          CurrId: this.selectedOrigins[this.selectedOrigins.length - 1].currency
            .id,
          currency: this.selectedOrigins[this.selectedOrigins.length - 1]
            .currency
        });
      }
    }
  }

  setEditData(type) {

    let parsedJsonSurchargeDet;
    if (type === "publish") {
      parsedJsonSurchargeDet =
        this.selectedData.forType === "WAREHOUSE"
          ? JSON.parse(this.selectedData.data.JsonSurchargeDet)
          : JSON.parse(this.selectedData.data[0].jsonSurchargeDet);
    }
    const _originsList = cloneObject(this.selectedData.addList);
    this.originsList = _originsList.filter(_charge => _charge.addChrBasis == 'PER_BOOKING' || _charge.addChrBasis === 'PER_CONTAINER')
    // console.log(this.originsList);

    if (parsedJsonSurchargeDet) {
      this.selectedOrigins = parsedJsonSurchargeDet.filter(
        e => e.Imp_Exp === "EXPORT"
      );
    }


    if (!this.selectedOrigins.length) {
      this.selectedOrigins = [{}];
    }


    if (this.selectedOrigins.length) {
      this.selectedOrigins.forEach(element => {
        this.originsList.forEach(e => {
          if (e.addChrID === element.addChrID) {
            let idx = this.originsList.indexOf(e);
            this.originsList.splice(idx, 1);
          }
        });
      });
    }

  }



  public surchargesList: any = [];
  getSurchargeBasis(containerLoad) {
    // console.log(containerLoad)
    // console.log(this.selectedData.addList)
    this._seaFreightService.getSurchargeBasis(containerLoad).subscribe(
      res => {
        this.surchargesList = res;
        let filteredData = []
        this.selectedData.addList.forEach(e => {
          this.surchargesList.forEach(element => {
            if (e.addChrBasis === element.codeVal || e.addChrBasis === "PER_BOOKING") {
              filteredData.push(e)
            }
          });
        });
        // console.log(this.originsList)
        // this.originsList = filteredData
      },
      err => { }
    );
  }
  public isOriginChargesForm = false;
  public isDestinationChargesForm = false;
  public lablelName: string = "";
  public lablelNameDed: string = "";
  public surchargeType = "";
  public surchargeTypeDed = "";
  public labelValidate: boolean = true;
  public surchargeBasisValidate: boolean = true;
  public surchargeBasisValidateDed: boolean = true;
  showCustomChargesForm(type) {
    if (type === "origin") {
      this.isOriginChargesForm = !this.isOriginChargesForm;
    } else if (type === "destination") {
      this.isDestinationChargesForm = !this.isDestinationChargesForm;
    }
  }

  onKeyDown(idx, event, type) {
    if (!event.target.value) {
      if (type === "origin") {
        this.selectedOrigins[idx].currency = {};
        this.selectedOrigins[idx].CurrId = null;
      }
    }

  }

  public canAddLabel: boolean = true;
  addCustomLabel(type) {
    this.canAddLabel = true;
    if (!this.lablelName) {
      this.labelValidate = false;
      return;
    }
    if (!this.surchargeType) {
      this.surchargeBasisValidate = false;
      return;
    }
    const selectedSurcharge = this.surchargesList.find(
      obj => obj.codeValID === parseInt(this.surchargeType)
    );
    let obj = {
      addChrID: -1,
      addChrCode: "OTHR",
      addChrName: this.lablelName,
      addChrDesc: this.lablelName,
      modeOfTrans: (this.selectedData.forType === 'FCL' || this.selectedData.forType === 'LCL') ? 'SEA' : ((this.selectedData.forType === 'WAREHOUSE') ? 'WAREHOUSE' : 'TRUCK'),
      addChrBasis: selectedSurcharge.codeVal,
      createdBy: this.userProfile.UserID,
      addChrType: "ADCH",
      providerID: this.userProfile.ProviderID,
      ContainerLoadType: this.selectedData.forType,
    };
    this.selectedData.addList.forEach(element => {
      if (element.addChrName === obj.addChrName) {
        this.canAddLabel = false;
      }
    });

    if (!this.canAddLabel) {
      this._toast.info("Already Added, Please try another name", "Info");
      return false;
    }

    this._seaFreightService.addCustomCharge(obj).subscribe(
      (res: any) => {
        this.isOriginChargesForm = false;
        this.isDestinationChargesForm = false;
        if (res.returnId !== -1) {
          let obj = {
            addChrID: res.returnId,
            addChrCode: "OTHR",
            addChrName: this.lablelName,
            addChrDesc: this.lablelName,
            modeOfTrans: (this.selectedData.forType === 'FCL' || this.selectedData.forType === 'LCL') ? 'SEA' : ((this.selectedData.forType === 'WAREHOUSE') ? 'WAREHOUSE' : 'TRUCK'),
            ContainerLoadType: this.selectedData.forType === 'FCL',
            addChrBasis: selectedSurcharge.codeVal,
            createdBy: this.userProfile.UserID,
            addChrType: "ADCH",
            providerID: this.userProfile.ProviderID
          };
          this.getAllAdditionalCharges(this.userProfile.ProviderID)
          if (type === "origin") {
            this.originsList.push(obj);
          }
          this.lablelName = "";
          this.surchargeType = "";
        }
      },
      err => {
      }
    );
  }

  getAllAdditionalCharges(providerID) {
    this._seaFreightService.getAllAdditionalCharges(providerID).subscribe((res: any) => {
      localStorage.setItem('additionalCharges', JSON.stringify(res.filter(e => e.addChrType === 'ADCH')))
      loading(false)
    }, (err) => {
      loading(false)
    })
  }

  public addDestinationActive: boolean = false;
  public addOriginActive: boolean = false;
  public addDestinationDedActive: boolean = false;
  public addOriginDedActive: boolean = false;
  dropdownToggle(event, type) {
    if (event) {
      this.isDestinationChargesForm = false;
      this.isOriginChargesForm = false;
      this.surchargeBasisValidate = true;
      this.labelValidate = true;
      if (type === "destination") {
        this.addDestinationActive = true;
      } else if (type === "origin") {
        this.addOriginActive = true;
      } else if (type === "originDed") {
        this.addOriginDedActive = true;
      }
    } else {
      this.addOriginActive = false;
      this.addDestinationActive = false;
    }
  }

  public combinedContainers = [];
  public ftlContainers: any[] = [];
  public selectedContainers = [];
  public shippingCategories = [];
  public cities: any[] = [];
  /**
   * Getting all dropdown values to fill
   *
   * @memberof SeaFreightComponent
   */

  getDropdownsList() {
    this.combinedContainers = JSON.parse(localStorage.getItem("containers"));
    this.allContainers = this.combinedContainers
    this.ftlContainers = this.combinedContainers.filter(_container => _container.ShippingModeCode.toUpperCase() === 'WAREHOUSE')
    // console.log(this.ftlContainers)
  }

  // GROUND WORKING
  public showPickupDropdown: boolean = false;
  public showDestinationDropdown: boolean = false;
  public showPickupPorts: boolean = false;
  public showPickupDoors: boolean = false;
  public showDestPorts: boolean = false;
  public showDestDoors: boolean = false;

  toggleDropdown(type) {
    if (this.selectedData.mode === "publish") {
      return;
    }
    if (!this.selectedContSize) return;
    if (type === "pickup") {
      this.showPickupDropdown = !this.showPickupDropdown;
      this.closeDropDown("delivery");
      this.showPickupPorts = false;
      this.showPickupDoors = false;
    }
    if (type === "delivery") {
      this.showDestinationDropdown = !this.showDestinationDropdown;
      this.closeDropDown("pickup");
      this.showDestPorts = false;
      this.showDestDoors = false;
    }
  }

  closeDropDown($action: string) {
    switch ($action) {
      case "pickup":
        if (this.showPickupDropdown) {
          this.showPickupDropdown = false;
        }
        break;
      case "delivery":
        if (this.showDestinationDropdown) {
          this.showDestinationDropdown = false;
        }
        break;
      default:
        if (this.showPickupDropdown || this.showDestinationDropdown) {
          this.showPickupDropdown = false;
          this.showDestinationDropdown = false;
        }
        break;
    }
  }

  public groundAddresses: any = [];
  public groundsPorts: any = [];
  public transPortMode = "SEA";
  portsFilterartion(obj) {
    if (typeof obj === "object") {
      this.showPickupDropdown = false;
      this.showDestinationDropdown = false;
    }
  }

  //Ground areas formatter and observer
  public groundPorts = [];
  addresses = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term =>
        !term || term.length < 3
          ? []
          : this.groundPorts.filter(
            v =>
              v.PortName &&
              v.PortName.toLowerCase().indexOf(term.toLowerCase()) > -1
          )
      )
    );
  addressFormatter = (x: { PortName: string }) => {
    return x.PortName;
  };

  //Ground areas formatter and observer
  // citiesList = (text$: Observable<string>) =>
  //   text$.pipe(
  //     debounceTime(500),
  //     map(term => (!term || term.length < 3) ? [] : this.cities.filter(
  //       v => (v.title.toLowerCase().indexOf(term.toLowerCase()) > -1 || v.shortName.toLowerCase().indexOf(term.toLowerCase()) > -1 || v.code.toLowerCase().indexOf(term.toLowerCase()) > -1))))

  citiesFormatter = (x: { title: string; imageName: string }) => {
    return x.title;
  };

  citiesList = (text$: Observable<string>) =>
    text$
      .debounceTime(300) //debounce time
      .distinctUntilChanged()
      .mergeMap(term => {
        let some: any = []; //  Initialize the object to return
        if (term && term.length >= 3) {
          //search only if item are more than three
          some = this._manageRateService
            .getAllCities(term)
            .do(res => res)
            .catch(() => []);
        } else {
          some = [];
        }
        return some;
      })
      .do(res => res); // final server list

  public showDoubleRates: boolean = false;
  containerChange(containerId) {
    // console.log(containerId)
    try {
      const x = this.ftlContainers.filter(_container => _container.ContainerSpecID === parseInt(containerId))[0]
      // console.log(x)
    } catch {

    }
  }

  /**
   * GET BASE URL FOR UI IMAGES
   *
   * @param {string} $image
   * @returns
   * @memberof SeaRateDialogComponent
   */
  getShippingLineImage($image: string) {
    return getImagePath(
      ImageSource.FROM_SERVER,
      "/" + $image,
      ImageRequiredSize.original
    );
  }

  setCurrency() {
    this.selectedCurrency = JSON.parse(localStorage.getItem("userCurrency"));
    this.whCurrency = JSON.parse(localStorage.getItem("userCurrency"));
  }

  /**
   *
   * Removed added additional charges
   * @param {string} type origin/destination
   * @param {object} obj
   * @memberof SeaRateDialogComponent
   */
  removeAdditionalCharge(type, obj) {
    // console.log(type, obj);
    // console.log(this.selectedOrigins);
    if (type === "origin") {
      if (this.selectedOrigins.length > 0) {
        this.selectedOrigins.forEach(element => {
          if (element.addChrID === obj.addChrID) {
            if (this.selectedOrigins.length > 1) {
              let idx = this.selectedOrigins.indexOf(element);
              this.selectedOrigins.splice(idx, 1);
            } else if (this.selectedOrigins.length === 1) {
              this.selectedOrigins = [{}]
            }
            try {
              if (element.addChrID) {
                this.originsList.push(element);
                const { originsList } = this
                const _originsList = originsList.sort(firstBy(function (v1, v2) { return v1.sortingorder - v2.sortingorder; }));
                this.originsList = _originsList
                // console.log(this.originsList);

              }
            } catch (error) { }
          }
        });
      }
    }
  }

  // WAREHOUSE WORKING
  /**
   * [GET WAREHOUSE PRICING]
   * @return [description]
   */
  public warehousePricing: any[] = [];
  public sharedWarehousePricing: any[] = [];

  getWarehousePricing() {
    loading(true);
    this._manageRateService.getWarehousePricing("WAREHOUSE").subscribe(
      (res: any) => {
        loading(false);
        this.getaddOnPeriods()
        // console.log(this.warehousePricing)
        this.warehousePricing = res;
        const pricingCpy: Array<WarehousePricing> = res
        pricingCpy.forEach(wPrice => {
          const { addChrBasis, addChrName } = wPrice;
          if (this.selectedData.data.UsageType === 'SHARED') {
            if (addChrBasis === "PER_CONTAINER") {
              this.singlePriceTagDed = addChrName;
            }
          }
        })
        // console.log(this.warehousePricing)
        this.sharedWarehousePricing = this.warehousePricing.filter(
          e => e.addChrBasis.includes('PER_CONT')
        );
      },
      err => {
        loading(false);
      }
    );
  }

  getaddOnPeriods() {
    this._manageRateService.getaddOnPeriods().subscribe((res: any) => {
      this.whAddsOn = res
      this.masterWhAddsOn = res
      if (this.tempAddOnBasis) {
        this.addOnCharge = this.masterWhAddsOn.filter(_unit => _unit.codeVal === this.tempAddOnBasis)[0]
      } else {
        this.whAddsOn = this.masterWhAddsOn.filter(_charge => _charge.codeVal.includes('DAY'))
        this.addOnCharge = this.whAddsOn[0]
      }
      this.addOnValidation()
    })
  }



  addOnValidation() {
    if (this.addOnCharge.codeVal === 'DAYS' && this.selectedWHPriceType.code.includes('DAY')) {
      this.addOnChargePrice = this.whPrice
      this.disableElAddOn = true
    } else if (this.addOnCharge.codeVal === 'MONTHS' && this.selectedWHPriceType.code.includes('MONTH')) {
      this.addOnChargePrice = this.whPrice
      this.disableElAddOn = false
    } else if (this.addOnCharge.codeVal === 'YEARS' && this.selectedWHPriceType.code.includes('YEAR')) {
      this.addOnChargePrice = this.whPrice
      this.disableElAddOn = false
    } else {
      this.disableElAddOn = false
    }
  }



  /**
   *
   *  CALCULATE THE PRICING JSON
   * @memberof SeaRateDialogComponent
   */
  calculatePricingJSON() {
    this.pricingJSON = []
    if (this.whPrice) {
      if (this.selectedData.data.UsageType === "SHARED") {
        const containerArr = this.sharedWarehousePricing.filter(e => e.addChrBasis === this.selectedWHPriceType.code)
        let json = {
          addChrID: containerArr[0].addChrID,
          addChrCode: containerArr[0].addChrCode,
          addChrName: containerArr[0].addChrName,
          addChrType: containerArr[0].addChrType,
          priceBasis: containerArr[0].addChrBasis,
          price: parseFloat(this.whPrice),
          currencyID: this.whCurrency.id,
          sWHType: "SHARED",
          addOnBasis: this.addOnCharge.codeVal,
          addOnCount: 1,
          addOnPrice: parseFloat(this.addOnChargePrice)
        };
        this.pricingJSON.push(json);
      }
    }

  }


  months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  getDateStr($date) {
    const strMonth = isNumber($date.month) ? this.months[$date.month - 1] + "" : "";
    return $date.day + '-' + strMonth + '-' + $date.year
  }




  onWHPriceType($type: string) {
    this.selectedWHPriceType = this.whPriceTypeList.filter(_type => _type.code === $type)[0]
    try {
      this.priceBasis.close();
    } catch (error) {

    }
    const { masterWhAddsOn } = this
    if ($type.includes('DAY')) {
      this.whAddsOn = masterWhAddsOn.filter(_charge => _charge.codeVal.includes('DAY'))
      this.addOnCharge = this.whAddsOn[0]
    } else if ($type.includes('MONTH')) {
      this.whAddsOn = masterWhAddsOn.filter(_charge => !_charge.codeVal.includes('YEAR'))
      this.addOnCharge = this.whAddsOn[0]
    } else if ($type.includes('YEAR')) {
      this.whAddsOn = masterWhAddsOn
      this.addOnCharge = this.whAddsOn[0]
    }

    this.addOnValidation()
  }

  onAddsOnSelect($addOn: any) {
    this.addOnCharge = $addOn
    this.eladdOnChargePrice.close();
    this.addOnValidation()
    if (this.addOnCharge.codeVal === 'DAYS' && this.selectedWHPriceType.code.includes('DAY')) {
      this.addOnChargePrice = this.whPrice
      this.disableElAddOn = true
    } else if (this.addOnCharge.codeVal === 'MONTHS' && this.selectedWHPriceType.code.includes('MONTH')) {
      this.addOnChargePrice = this.whPrice
      this.disableElAddOn = true
    } else if (this.addOnCharge.codeVal === 'YEARS' && this.selectedWHPriceType.code.includes('YEAR')) {
      this.addOnChargePrice = this.whPrice
      this.disableElAddOn = true
    } else {
      this.disableElAddOn = false
    }

  }


  onPriceChange() {
    if (this.disableElAddOn) {
      this.addOnChargePrice = this.whPrice
    }
  }
}