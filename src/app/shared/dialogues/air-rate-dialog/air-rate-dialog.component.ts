import { Component, OnInit, ViewEncapsulation, ViewChild, Renderer2, ElementRef, Input, OnDestroy, ɵConsole, Output, EventEmitter } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { NgbActiveModal, NgbDropdownConfig } from "@ng-bootstrap/ng-bootstrap";
import { SharedService } from '../../../services/shared.service';
import { Observable } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { NgbInputDatepicker, NgbDateStruct, NgbDateParserFormatter, } from '@ng-bootstrap/ng-bootstrap';
import { AirFreightService } from '../../../components/pages/user-desk/manage-rates/air-freight/air-freight.service';
import { ToastrService } from 'ngx-toastr';
import { NgbDateFRParserFormatter } from '../../../constants/ngb-date-parser-formatter';
import { JsonResponse } from '../../../interfaces/JsonResponse';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { cloneObject, extractColumn } from '../../../components/pages/user-desk/reports/reports.component';
import { SeaFreightService } from '../../../components/pages/user-desk/manage-rates/sea-freight/sea-freight.service';
import { CommonService } from '../../../services/common.service';
import { getImagePath, ImageSource, ImageRequiredSize, loading, removeDuplicates, getLoggedUserData, isMobile } from '../../../constants/globalFunctions';
import * as moment from "moment";
import { firstBy } from 'thenby';
import { AddRateHelpers } from '../../../helpers/add-rate.helper';
import { ManageRatesService } from '../../../components/pages/user-desk/manage-rates/manage-rates.service';

const now = new Date();
const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
  one && two && two.year === one.year && two.month === one.month && two.day === one.day;

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day < two.day : one.month < two.month : one.year < two.year;

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day > two.day : one.month > two.month : one.year > two.year;

@Component({
  selector: 'app-air-rate-dialog',
  templateUrl: './air-rate-dialog.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [
    { provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter }, NgbDropdownConfig
  ],
  styleUrls: ['./air-rate-dialog.component.scss'],
  host: {
    "(document:click)": "closeDropdown($event)"
  }
})
export class AirRateDialogComponent extends AddRateHelpers implements OnInit, OnDestroy {

  @ViewChild("dp") input: NgbInputDatepicker;
  @ViewChild('rangeDp') rangeDp: ElementRef;
  @Input() selectedData: any;
  @ViewChild("originDropdown") originDropdown: any;
  @ViewChild("destinationDropdown") destinationDropdown: any;
  @Output() savedRow = new EventEmitter<any>();

  public allAirLines: any[] = [];
  public allCargoType: any[] = [];
  public allPorts: any[] = [];
  public allCurrencies: any[] = [];
  public allCustomers: any[] = [];
  public filterOrigin: any = {};
  public filterDestination: any = {};
  public userProfile: any;
  public selectedCategory: any = null;
  public selectedProduct: any = null;
  // public selectedPartner: any = null;
  public selectedAirline: any = null;
  public minPrice: any;
  public normalPrice: any;
  public plusfortyFivePrice: any;
  public plushundredPrice: any;
  public plusTwoFiftyPrice: any;
  public plusFiveHundPrice: any;
  public plusThousandPrice: any;
  public loading: boolean = true

  isMobile = isMobile()

  public defaultCurrency: any = {
    CurrencyID: 101,
    CurrencyCode: 'USD',
    CountryCode: 'US',
  }
  public selectedCurrency: any = this.defaultCurrency;


  public startDate: NgbDateStruct;
  public maxDate: NgbDateStruct;
  public minDate: NgbDateStruct;
  public hoveredDate: NgbDateStruct;
  public fromDate: any = {
    day: null,
    month: undefined,
    year: undefined
  };
  public toDate: any = {
    day: undefined,
    month: undefined,
    year: undefined
  };
  public model: any;
  private allRatesFilledData: any[] = [];
  private newProviderPricingDraftID = undefined;

  private newDraftOne = undefined;
  private newDraftTwo = undefined;
  private newDraftThree = undefined;
  private newDraftFour = undefined;
  private newDraftFive = undefined;
  private newDraftSix = undefined;

  public airSlabs: Array<any> = []
  public slabPrice: any[] = []
  public originSlabPrice: any[] = []
  public destinationSlabPrice: any[] = []
  public selectedOrigins: any = [{}];
  public selectedDestinations: any = [{}];
  public destinationsList = [];
  public originsListSimple: any[] = []
  public destinationsListSimple: any[] = []
  public originsList = [];
  public isDestinationChargesForm: boolean = false;
  public isOriginChargesForm: boolean = false;
  public surchargeBasisValidate: boolean;
  public labelValidate: boolean;
  public lablelName: string = "";
  public lablelNameDed: string = "";
  public surchargeType = "";
  public surchargeTypeDed = "";
  public surchargeBasisValidateDed: boolean = true;
  public isOriginSimplerMethod: boolean = false
  public isDestinationSimplerMethod: boolean = false
  public minTransitDays: number;
  public maxTransitDays: number;
  public aircraftTypeID: number = 0
  public jsonSurchargeDetail: any[] = []
  public disableFields: boolean = false
  public jsonDepartureDays: string
  public jsonCutOffDays: string
  public selectedCustomer: any[] = [];
  public disabledCustomers: boolean = false

  isHovered = date =>
    this.fromDate && !this.toDate && this.hoveredDate && after(date, this.fromDate) && before(date, this.hoveredDate)
  isInside = date => after(date, this.fromDate) && before(date, this.toDate);
  isFrom = date => equals(date, this.fromDate);
  isTo = date => equals(date, this.toDate);

  public isVirtualAirline: boolean = false;
  displayMonths = this.isMobile ? 1 : 2

  constructor(
    private location: PlatformLocation,
    private _activeModal: NgbActiveModal,
    private _sharedService: SharedService,
    private _parserFormatter: NgbDateParserFormatter,
    private renderer: Renderer2,
    private _airFreightService: AirFreightService,
    private _toast: ToastrService,
    private _seaFreightService: SeaFreightService,
    private _manageRateService: ManageRatesService,
    private config: NgbDropdownConfig,
    private _eref: ElementRef,
    private _commonService: CommonService,
  ) {
    super()
    location.onPopState(() => this.closeModal(null));
    config.autoClose = false;
  }

  ngOnInit() {
    try {
      const date = new Date();
      this.minDate = { month: date.getMonth() + 1, day: date.getDate(), year: date.getFullYear() };
    } catch (error) { }
    this.selectedData.addList = this.selectedData.addList.filter((e => e.modeOfTrans === 'AIR'))
    this.userProfile = getLoggedUserData()
    this._sharedService.currencyList.subscribe(res => {
      if (res) {
        this.allCurrencies = res;
      }
    });
    this.originsList = this.selectedData.addList.filter(e => !e.isSlabBased);
    this.destinationsList = this.selectedData.addList.filter(e => !e.isSlabBased)
    this.originsListSimple = this.selectedData.addList.filter(e => e.isSlabBased && e.addChrCode === 'ADCH_O');
    this.destinationsListSimple = this.selectedData.addList.filter(e => e.isSlabBased && e.addChrCode === 'ADCH_D')
    if (this.originsListSimple && this.originsListSimple.length) {
      this.originsListSimple[0].jsonSlabDataParsed = JSON.parse(this.originsListSimple[0].jsonSlabData.replace(/\\/g, ""))
    }
    if (this.destinationsListSimple && this.destinationsListSimple.length) {
      this.destinationsListSimple[0].jsonSlabDataParsed = JSON.parse(this.destinationsListSimple[0].jsonSlabData.replace(/\\/g, ""))
    }
    let partnerid = null
    try {
      if (this.selectedData.mode === "publish") {
        partnerid = this.selectedData.data[0].partnerID
        this.disableFields = true
      } else if (this.selectedData.mode === "draft" && this.selectedData.data) {
        partnerid = this.selectedData.data.partnerID
      }
    } catch (error) {
      console.log(partnerid);

    }
    console.log(partnerid);
    this.getAirPartners(this.userProfile.ProviderID, partnerid, this._manageRateService)
    this.getProductAir()

    // VIRTUAL AIRLINE CHECKS
    if (localStorage.hasOwnProperty('isVirtualAirline') && JSON.parse(localStorage.getItem('isVirtualAirline'))) {
      this.isVirtualAirline = JSON.parse(localStorage.getItem('isVirtualAirline'))
      this.isOriginSimplerMethod = true
      this.isDestinationSimplerMethod = true



      this.originsListSimple = this.selectedData.addList.filter(e => e.isSlabBased && e.addChrCode == 'FCA');
      this.destinationsListSimple = this.selectedData.addList.filter(e => e.isSlabBased && e.addChrCode === 'EX-WORKS')
      if (this.originsListSimple && this.originsListSimple.length) {
        this.originsListSimple[0].jsonSlabDataParsed = JSON.parse(this.originsListSimple[0].jsonSlabData.replace(/\\/g, ""))
        this.destinationsListSimple[0].jsonSlabDataParsed = JSON.parse(this.destinationsListSimple[0].jsonSlabData.replace(/\\/g, ""))
      }
    }
    this.setCurrency();
    this.allservicesByAir();
    this.getDropdownLists()
    this.getSurchargeBasis('AIR');
    // console.log(this.selectedData.customers);

    this.allCustomers = this.selectedData.customers;
    // console.log(this.allCustomers);
    // console.log(this.selectedData);


    if (this.selectedData.mode === "publish") {
      this.setData(this.selectedData.data[0]);
      this.disableFields = true
    } else if (this.selectedData.mode === "draft" && this.selectedData.data) {
      this.setData(this.selectedData.data);
    }
  }


  allservicesByAir() {
    const cargoList = JSON.parse(localStorage.getItem('cargoTypes')) //Old
    this.allCargoType = this._sharedService.getFilteredCargos('AIR', cargoList, 'id')
    if (this.allCargoType.length === 1) {
      this.selectedCategory = this.allCargoType[0].id
    }
    this.allAirLines = JSON.parse(localStorage.getItem('carriersList')).filter(e => e.type === 'AIR');
  }

  setData(data) {
    // Via Working
    setTimeout(() => {
      try {
        if (data.jsonPortVia) {
          const { jsonPortVia } = data
          for (let index = 0; index < 8; index++) {
            const _jsonPortVia = JSON.parse(jsonPortVia)
            // console.log(_jsonPortVia);
            const _port = _jsonPortVia[`Stop${index + 1}`]
            if (_port && _port.Code && _port.Code.length > 0) {
              this.viaList.push(_port)
            }
          }
          // console.log(this.viaList);

        }
      } catch (error) { }
    }, 0);
    let parsed = "";
    this.disabledCustomers = true
    this.selectedCategory = data.shippingCatID;
    this.selectedProduct = data.productID
    // this.selectedPartner = data.partnerID
    this.setCurrentPartner(data.partnerID)

    this.filterOrigin = this.allPorts.find(obj => obj.PortID == data.polID);
    this.filterDestination = this.allPorts.find(obj => obj.PortID == data.podID);
    this.selectedAirline = this.allAirLines.find(obj => obj.id == data.carrierID);
    const { id, imageName, shortName } = this.allCurrencies.find(
      obj => obj.id === data.currencyID
    );
    this.selectedCurrency = { id, imageName, shortName }
    this.aircraftTypeID = (data.aircraftTypeID) ? data.aircraftTypeID : 0
    this.minPrice = data.minPrice
    this.minTransitDays = (data.minTransitDays) ? data.minTransitDays : 0
    this.maxTransitDays = (data.maxTransitDays) ? data.maxTransitDays : 0
    this.jsonDepartureDays = data.jsonDepartureDays
    this.jsonCutOffDays = data.jsonCutOffDays
    let _custDtl = null
    try {
      _custDtl = data.jsonCustomerDetail
    } catch (error) {
      console.log(error);
    }

    if (_custDtl && (data.customerType && data.customerType !== "null")) {
      this.selectedCustomer = JSON.parse(_custDtl);
    }
    if (data.effectiveFrom) {
      this.fromDate.day = new Date(data.effectiveFrom).getDate();
      this.fromDate.year = new Date(data.effectiveFrom).getFullYear();
      this.fromDate.month = new Date(data.effectiveFrom).getMonth() + 1;
    }
    if (data.effectiveTo) {
      this.toDate.day = new Date(data.effectiveTo).getDate();
      this.toDate.year = new Date(data.effectiveTo).getFullYear();
      this.toDate.month = new Date(data.effectiveTo).getMonth() + 1;
    }
    if (this.fromDate && this.fromDate.day) {
      this.model = this.fromDate;
      parsed += this._parserFormatter.format(this.fromDate);
    }
    if (this.toDate && this.toDate.day) {
      parsed += " - " + this._parserFormatter.format(this.toDate);
    }

    if (!this.selectedCurrency) {
      this.selectedCurrency = this.defaultCurrency;
    }

    if (data.pricingJson) {
      const parsedPricingJSON = JSON.parse(data.pricingJson)
      parsedPricingJSON.forEach(element => {
        for (let index = 0; index < parsedPricingJSON.length; index++) {
          this.slabPrice[element.SlabID - 1] = element.ViewPrice
        }
      });
    }
    this.isChinaPort = data.isChinaGateway

    if (data.jsonSurchargeDetail && data.jsonSurchargeDetail !== '[{},{}]') {
      const parsedjsonSurchargeDetail = JSON.parse(data.jsonSurchargeDetail)
      const originChargesSimple = parsedjsonSurchargeDetail.filter(e => e.Imp_Exp === 'EXPORT' && e.isSlabBased)
      const originChargesAdvanced = parsedjsonSurchargeDetail.filter(e => e.Imp_Exp === 'EXPORT' && !e.isSlabBased)
      const destinationChargesSimple = parsedjsonSurchargeDetail.filter(e => e.Imp_Exp === 'IMPORT' && e.isSlabBased)
      const destinationChargesAdvanced = parsedjsonSurchargeDetail.filter(e => e.Imp_Exp === 'IMPORT' && !e.isSlabBased)
      if (originChargesSimple.length) {
        this.isOriginSimplerMethod = true
        this.baseRateOrigin = originChargesSimple[0].Price
        const parsedjsonSlabData = JSON.parse(originChargesSimple[0].jsonSlabData)
        parsedjsonSlabData.forEach(element => {
          for (let index = 0; index < parsedjsonSlabData.length; index++) {
            this.originSlabPrice[element.SlabID - 1] = element.ViewPrice
          }
        });
      } else if (originChargesAdvanced) {
        this.isOriginSimplerMethod = false
        this.setAdvancedChargesData(this.selectedData.mode);
      }

      if (this.isVirtualAirline) {
        this.setAdvancedChargesData(this.selectedData.mode);
      }

      if (destinationChargesSimple.length) {
        this.isDestinationSimplerMethod = true
        this.baseRateDestination = destinationChargesSimple[0].Price
        const parsedjsonSlabData = JSON.parse(destinationChargesSimple[0].jsonSlabData)
        parsedjsonSlabData.forEach(element => {
          for (let index = 0; index < parsedjsonSlabData.length; index++) {
            this.destinationSlabPrice[element.SlabID - 1] = element.ViewPrice
          }
        });
      } else if (destinationChargesAdvanced) {
        this.isDestinationSimplerMethod = false
        this.setAdvancedChargesData(this.selectedData.mode);
      }
    }

    setTimeout(() => {
      this.rangeDp.nativeElement.value = parsed;
    }, 1000);
  }

  savedraftrow(type) {
    this.loading = true
    if (type === 'saveNadd' || type === 'onlySave') {
      this.saveDraftRate(type)
    } else if (type === 'update') {
      this.updatePublishedRate()
    }
  }


  /**
   *
   * ADD DRAFT ROW
   * @memberof AirRateDialogComponent
   */
  // public buttonLoading: boolean = false
  public TotalImportCharges: any
  public TotalExportCharges: any
  public isRateUpdating = false;
  saveDraftRate(type) {
    // this.buttonLoading = true;

    try {
      if (this.fromDate && this.fromDate.day && (!this.toDate || !this.toDate.day)) {
        this._toast.warning("Please enter a valid date range for this rate");
        this.isRateUpdating = false;
        this.loading = false
        return;
      }
      if (!this.selectedAirline || !this.selectedAirline.id) {
        this._toast.warning("Please select an Airline.");
        this.isRateUpdating = false;
        this.loading = false
        return;
      }
    } catch (error) {
      console.log(error);


    }

    let JsonSurchargeDet: any
    let obj
    try {
      const { filterOrigin, filterDestination } = this;
      if (
        filterOrigin &&
        filterDestination && (filterOrigin === filterDestination)
      ) {
        this._toast.warning(
          "Please select different pickup and drop ariport",
          "Warning"
        );
        this.isRateUpdating = false;
        this.loading = false
        return;
      }

      let totalImp = [];
      let totalExp = [];
      // this.selectedOrigins = this.selectedOrigins.filter(e => e.addChrID)
      // this.selectedDestinations = this.selectedDestinations.filter(e => e.addChrID)
      const expCharges = this.selectedOrigins.filter(
        e => e.Imp_Exp === "EXPORT"
      );
      const impCharges = this.selectedDestinations.filter(
        e => e.Imp_Exp === "IMPORT"
      );

      if (impCharges && impCharges.length) {
        impCharges.forEach(element => {
          totalImp.push(parseFloat(element.Price));
        });
        this.TotalImportCharges = totalImp.reduce((all, item) => {
          return all + item;
        });
      }

      if (expCharges && expCharges.length) {
        expCharges.forEach(element => {
          totalExp.push(parseFloat(element.Price));
        });
        this.TotalExportCharges = totalExp.reduce((all, item) => {
          return all + item;
        });
      }


      this.calculateSlabsPrice()
      this.calculateAdditionalSimpleData()
      // console.log(this.selectedOrigins);

      if (true) {
        let _originCharge = null
        let _destCharge = null
        console.log(!this.isOriginSimplerMethod && !this.isDestinationSimplerMethod && !this.isVirtualAirline);

        try {
          if (!this.isOriginSimplerMethod && !this.isDestinationSimplerMethod && !this.isVirtualAirline) {
            _originCharge = (JSON.stringify(this.selectedOrigins) === '[{}]') ? null : this.selectedOrigins
            _destCharge = (JSON.stringify(this.selectedDestinations) === '[{}]') ? null : this.selectedDestinations
          } else if (this.isOriginSimplerMethod && !this.isDestinationSimplerMethod) {
            _originCharge = (JSON.stringify(this.originSimpleCharges) === '[{}]') ? null : (JSON.parse(this.originSimpleCharges[0].jsonSlabData).reduce((a, b) => +a + +b.Price, 0) > 0) ? this.originSimpleCharges : null
            _destCharge = (JSON.stringify(this.selectedDestinations) === '[{}]') ? null : this.selectedDestinations
          } else if (!this.isOriginSimplerMethod && this.isDestinationSimplerMethod) {
            _originCharge = (JSON.stringify(this.selectedOrigins) === '[{}]') ? null : this.selectedOrigins
            _destCharge = (JSON.stringify(this.destinationSimpleCharges) === '[{}]') ? null : (JSON.parse(this.destinationSimpleCharges[0].jsonSlabData).reduce((a, b) => +a + +b.Price, 0) > 0) ? this.destinationSimpleCharges : null
          } else if (this.isOriginSimplerMethod && this.isDestinationSimplerMethod) {
            _originCharge = (JSON.stringify(this.originSimpleCharges) === '[{}]') ? null : (JSON.parse(this.originSimpleCharges[0].jsonSlabData).reduce((a, b) => +a + +b.Price, 0) > 0) ? this.originSimpleCharges : null
            _destCharge = (JSON.stringify(this.destinationSimpleCharges) === '[{}]') ? null : (JSON.parse(this.destinationSimpleCharges[0].jsonSlabData).reduce((a, b) => +a + +b.Price, 0) > 0) ? this.destinationSimpleCharges : null
            // if (!_originCharge) {
            //   this._toast.warning('Please provide FCA Per KG rate', 'Warning')
            //   loading(false)
            // }
            // if (!_destCharge) {
            //   this._toast.warning('Please provide Ex Works Per KG rate', 'Warning')
            //   loading(false)
            // }
          } else if (this.isVirtualAirline && this.isChinaPort) {
            _originCharge = (JSON.stringify(this.originSimpleCharges) === '[{}]') ? null : (JSON.parse(this.originSimpleCharges[0].jsonSlabData).reduce((a, b) => +a + +b.Price, 0) > 0) ? this.originSimpleCharges : null
            _destCharge = (JSON.stringify(this.destinationSimpleCharges) === '[{}]') ? null : (JSON.parse(this.destinationSimpleCharges[0].jsonSlabData).reduce((a, b) => +a + +b.Price, 0) > 0) ? this.destinationSimpleCharges : null
          }
          console.log(_originCharge, _destCharge);


        } catch (error) {
          console.log(error);

        }
        if (!_originCharge && !_destCharge) {
          JsonSurchargeDet = null
        } else if (_originCharge && !_destCharge) {
          JsonSurchargeDet = JSON.stringify(_originCharge)
        } else if (!_originCharge && _destCharge) {
          JsonSurchargeDet = JSON.stringify(_destCharge)
        } else if (_originCharge && _destCharge) {
          JsonSurchargeDet = JSON.stringify(_originCharge.concat(_destCharge))
        }
        try {
          if (!this.isChinaPort && this.isVirtualAirline && JsonSurchargeDet) {
            const _cloned = JSON.parse(JsonSurchargeDet)
            const newCharges = _cloned.filter(charge => charge.addChrCode !== 'FCA' && charge.addChrCode !== 'EX-WORKS')
            JsonSurchargeDet = JSON.stringify(newCharges)
          }
        } catch (error) { }
      }

      let customers = [];
      if (this.selectedCustomer.length) {
        this.selectedCustomer.forEach(element => {
          let obj = {
            CustomerID: element.CustomerID,
            CustomerType: element.CustomerType,
            CustomerName: element.CustomerName,
            CustomerImage: element.CustomerImage
          };
          customers.push(obj);
        });
      }

      // AERO AFRICA CHINA PORT CHECK
      if (this.isVirtualAirline) {
        if (this.isChinaPort) {
          let parsedJsonSurchargeDet = JSON.parse(JsonSurchargeDet)
          if (JSON.stringify(this.selectedOrigins) !== '[{}]') {
            JsonSurchargeDet = JSON.stringify(parsedJsonSurchargeDet.concat((this.selectedOrigins)))
          } else {
            JsonSurchargeDet = JSON.stringify(parsedJsonSurchargeDet)
          }
        } else {
          if (JSON.stringify(this.selectedOrigins) !== '[{}]') {
            JsonSurchargeDet = JSON.stringify((this.selectedOrigins))
          }
        }

        if (!this.selectedAirline) {
          this._toast.error('Please provide a carrier', 'Error')
          this.loading = false
          return
        }
        if (!this.selectedPartner) {
          this._toast.error('Please provide a partner', 'Error')
          this.loading = false
          return
        }
        // if (!this.selectedProduct) {
        //   this._toast.error('Please provide a product', 'Error')
        //   this.loading = false
        //   return
        // }
      }
      if (this.isVirtualAirline) {
        const surchargeDet = JSON.parse(JsonSurchargeDet)
        if (!this.minPrice) {
          this.minPrice = 0
          // this._toast.warning('Please provider minimum price for FOB rate', 'Warning')
          // this.loading = false
          // return
        }
        const { airSlabs } = this
        const hasFobRates = airSlabs.filter(slab => slab.ViewPrice > 0)
        if (!(hasFobRates && hasFobRates.length > 0)) {
          this._toast.warning('Please provide alteast one slab for FOB rate', 'Warning')
          this.loading = false
          return
        }
        // console.log(this.baseRateOrigin)
        if (this.isChinaPort) {
          if (this.baseRateOrigin === 0 || !this.baseRateOrigin) {
            this._toast.warning('Please provide base rate for FCA rate', 'Warning')
            this.loading = false
            return
          }
          // console.log(JSON.parse(surchargeDet[0].jsonSlabData)[0].ViewPrice)
          if (JSON.parse(surchargeDet[0].jsonSlabData)[0].ViewPrice === 0 || !(JSON.parse(surchargeDet[0].jsonSlabData)[0].ViewPrice)) {
            this._toast.warning('Please provide FCA Per KG rate', 'Warning')
            this.loading = false
            return
          }

          if (this.baseRateDestination === 0 || !this.baseRateDestination) {
            this._toast.warning('Please provide base rate for Ex Works rate', 'Warning')
            this.loading = false
            return
          }

          if (JSON.parse(surchargeDet[1].jsonSlabData)[0].ViewPrice === 0 || !(JSON.parse(surchargeDet[1].jsonSlabData)[0].ViewPrice)) {
            this._toast.warning('Please provide Ex Works Per KG rate', 'Warning')
            this.loading = false
            return
          }
        }

      }

      if (!this.isVirtualAirline) {
        const surchargeDet: any[] = JSON.parse(JsonSurchargeDet)

        // if (!this.airSlabs[0].ViewPrice || this.airSlabs[0].ViewPrice === 0) {
        //   this._toast.warning('Please provide Normal rate for Freight', 'Warning')
        //   this.loading = false
        //   return
        // }

        const { airSlabs } = this
        const hasFobRates = airSlabs.filter(slab => slab.ViewPrice > 0)
        console.log(this.isDestinationSimplerMethod)
        console.log(surchargeDet)
        if (!(hasFobRates && hasFobRates.length > 0)) {
          this._toast.warning('Please provide alteast one slab for Freight rate', 'Warning')
          this.loading = false
          return
        }
        if (this.isOriginSimplerMethod) {
          try {
            const originSlab = JSON.parse(surchargeDet[0].jsonSlabData)
            const _slabWithRate = originSlab.filter(slab => slab.ViewPrice > 0)
            if (this.baseRateOrigin && this.baseRateOrigin > 0) {
              if (!_slabWithRate || _slabWithRate.length === 0) {
                this._toast.warning('Please provide Per KG rate at the Port of Origin', 'Invalid Additional Charge')
                this.loading = false
                return
              }
            }
            if ((_slabWithRate && _slabWithRate.length > 0) && (!this.baseRateOrigin || this.baseRateOrigin === 0)) {
              this._toast.warning('Please provide base rate at the Port of Origin', 'Invalid Additional Charge')
              this.loading = false
              return
            }
          } catch (error) { }
        }
        if (this.isDestinationSimplerMethod) {
          try {
            let index = 0
            console.log('asd')
            if (this.isOriginSimplerMethod || (surchargeDet && surchargeDet.filter(_charge => _charge.Imp_Exp === 'IMPORT'))) {
              index = 1
            }
            const destinSlab = JSON.parse(surchargeDet[index].jsonSlabData)
            const _slabWithRate = destinSlab.filter(slab => slab.ViewPrice > 0)
            if (this.baseRateDestination && this.baseRateDestination > 0 && (!_slabWithRate || _slabWithRate.length === 0)) {
              this._toast.warning('Please provide Per KG rate at the Port of Destination', 'Invalid Additional Charge')
              this.loading = false
              return
            }
            if ((_slabWithRate && _slabWithRate.length > 0) && (!this.baseRateDestination || this.baseRateDestination === 0)) {
              this._toast.warning('Please provide base rate at the Port of Destination', 'Invalid Additional Charge')
              this.loading = false
              return
            }
          } catch (error) {
            console.log(error)
          }
        }
      }

      if (!this.fromDate || this.fromDate === null) {
        this._toast.warning("Effective from Cannot be empty");
        this.isRateUpdating = false;
        this.loading = false
        return;
      }

      if (!this.toDate || this.toDate === null) {
        this._toast.warning("Effective to Cannot be empty");
        this.isRateUpdating = false;
        this.loading = false
        return;
      }

      // if (!this.selectedPartner) {
      //   this._toast.error('Please provide a partner', 'Error')
      //   this.isRateUpdating = false;
      //   this.loading = false
      //   return
      // }


      if (!this.validateAdditionalCharges()) {
        this.loading = false
        return;
      }

      try {
        const { viaList } = this
        viaList.map((port, index) => {
          const { Code, Name, SCode } = port
          this.viaPorts[`Stop${index + 1}`].Code = Code
          this.viaPorts[`Stop${index + 1}`].Name = Name
          this.viaPorts[`Stop${index + 1}`].SCode = SCode
        });
        // console.log(this.viaPorts);
      } catch (error) { }

      console.log(this.selectedPartner);

      obj =
      {
        carrierPricingDraftID: this.selectedData.data ? this.selectedData.data.carrierPricingDraftID : 0,
        carrierID: (this.selectedAirline) ? this.selectedAirline.id : undefined,
        carrierName: (this.selectedAirline) ? this.selectedAirline.title : undefined,
        carrierImage: (this.selectedAirline) ? this.selectedAirline.imageName : undefined,
        containerSpecID: null,
        customerID: (customers && customers.length && customers[0] && customers[0].CustomerID) ? customers[0].CustomerID : null,
        customersList: customers.length ? customers : null,
        price: 0,
        fromKg: 0,
        toKg: 0,
        modeOfTrans: "AIR",
        currencyID: (this.selectedCurrency.id) ? this.selectedCurrency.id : 101,
        currencyCode: (this.selectedCurrency.shortName) ? this.selectedCurrency.shortName : 'USD',
        effectiveFrom: (this.fromDate && this.fromDate.month) ? this.fromDate.month + '/' + this.fromDate.day + '/' + this.fromDate.year : null,
        effectiveTo: (this.toDate && this.toDate.month) ? this.toDate.month + '/' + this.toDate.day + '/' + this.toDate.year : null,
        minPrice: this.minPrice,
        providerID: this.userProfile.ProviderID,
        shippingCatID: (this.selectedCategory == 'null') ? null : this.selectedCategory,
        shippingCatName: (this.selectedCategory) ? this.getShippingName(this.selectedCategory) : undefined,
        polID: (this.filterOrigin && this.filterOrigin.PortID) ? this.filterOrigin.PortID : null,
        polName: (this.filterOrigin && this.filterOrigin.PortID) ? this.filterOrigin.PortName : null,
        polCode: (this.filterOrigin && this.filterOrigin.PortID) ? this.filterOrigin.PortCode : null,
        podID: (this.filterDestination && this.filterDestination.PortID) ? this.filterDestination.PortID : null,
        podName: (this.filterDestination && this.filterDestination.PortID) ? this.filterDestination.PortName : null,
        podCode: (this.filterDestination && this.filterDestination.PortID) ? this.filterDestination.PortCode : null,
        containerSpecShortName: null,
        jsonCustomerDetail: null,
        jsonSurchargeDetail: (JsonSurchargeDet === "[{},{}]" || JsonSurchargeDet === "null") ? null : JsonSurchargeDet,
        customerType: (customers && customers.length && customers[0] && customers[0].CustomerType) ? customers[0].CustomerType : null,
        minTransitDays: this.minTransitDays ? this.minTransitDays : 0,
        maxTransitDays: this.maxTransitDays ? this.maxTransitDays : 0,
        jsonDepartureDays: this.jsonDepartureDays,
        aircraftTypeID: this.aircraftTypeID,
        pricingJson: JSON.stringify(this.airSlabs),
        createdBy: this.userProfile.UserID,
        partnerID: (this.selectedPartner && this.selectedPartner.id) ? this.selectedPartner.id : null,
        productID: (this.selectedProduct == 'null') ? null : this.selectedProduct,
        jsonCutOffDays: this.jsonCutOffDays,
        JsonPortVia: JSON.stringify(this.viaPorts)
      }

      // console.log(this.airSlabs);
      // VALIDATIONS STARTS HERE
      // this.selectedData.data.UsageType === "SHARED"
      if (
        !obj.carrierID &&
        !obj.effectiveFrom &&
        !obj.effectiveTo &&
        !obj.podID &&
        !obj.polID &&
        !obj.minPrice &&
        !obj.shippingCatID
      ) {
        this._toast.info("Please fill atleast one field to save", "Info");
        this.isRateUpdating = false;
        this.loading = false
        return;
      }

      if (obj.podID && obj.polID && obj.podID === obj.polID) {
        this._toast.error(
          "Source and Destination cannot be same",
          "Error"
        );
        this.isRateUpdating = false;
        loading(false)
        return;
      }


      if (
        !obj.minPrice ||
        !(typeof parseFloat(obj.minPrice) == "number") ||
        parseFloat(obj.minPrice) < -1
      ) {
        // console.log(typeof parseFloat(obj.minPrice))
        // console.log(obj.minPrice)
        // console.log(parseFloat(obj.minPrice) < -1)
        // this._toast.error("Minimum Price cannot be zero", "Error");
        // this.isRateUpdating = false;
        // this.loading = false
        // return;
        obj.minPrice = 0
      }


      let duplicateRecord: boolean = false;
      if (this.selectedData.drafts) {
        this.selectedData.drafts.forEach(element => {
          if (
            moment(element.effectiveFrom).format("D MMM, Y") ===
            moment(obj.effectiveFrom).format("D MMM, Y") &&
            moment(element.effectiveTo).format("D MMM, Y") ===
            moment(obj.effectiveTo).format("D MMM, Y") &&
            element.polID === obj.polID &&
            element.polID === obj.polID &&
            element.minPrice === parseFloat(obj.minPrice) &&
            element.shippingCatID === obj.shippingCatID &&
            element.jsonSurchargeDetail === obj.jsonSurchargeDetail
          ) {
            duplicateRecord = true;
          }
        });
      }

      if (
        obj.podType &&
        obj.podType === "Ground" &&
        (obj.polType && obj.polType === "Ground")
      ) {
        this.isRateUpdating = false;
        this._toast.info("Please change origin or destination type", "Info");
        this.loading = false
        return;
      }

      // if (duplicateRecord) {
      //   this.isRateUpdating = false;
      //   this._toast.warning("This record has already been added", "Warning");
      //   this.loading = false
      //   return;
      // }

    } catch (error) {
      console.log(error);

      this.isRateUpdating = false;
      this.loading = false
      return;
    }

    this._airFreightService.saveDraftRate(obj).subscribe((res: JsonResponse) => {
      this.loading = false
      if (res.returnId > 0) {
        // this.buttonLoading = false;
        if (res.returnText && typeof res.returnText === 'string') {
          this._toast.success(res.returnText, "Success");
        } else {
          this._toast.success("Rates added successfully", "Success");
        }
        if (type === "onlySave") {
          this.closePopup(true)
        } else {
          if (this.selectedData.data) {
            this.selectedData.data.CarrierPricingDraftID = 0;
          }
          this.savedRow.emit(res.returnObject);
        }
      } else {
        this._toast.error(res.returnText, "Error")
      }
    })
  }

  numberValidwithDecimal(evt) {
    let charCode = (evt.which) ? evt.which : evt.keyCode;
    if (evt.target.value && evt.target.value[evt.target.value.length - 1] == '.') {
      if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

      return true;
    }
    if (charCode != 46 && charCode > 31
      && (charCode < 48 || charCode > 57))
      return false;

    return true;
  }


  getShippingName(id) {
    return this.allCargoType.find(obj => obj.id == id).title;
  }

  onDateSelection(date: NgbDateStruct) {
    if (this.selectedData.mode === "publish" && this.selectedData.data) {
      this.onDateSelectionEdit(date)
    } else {
      this.onDateSelectionNew(date)
    }
  }

  onDateSelectionEdit(date: NgbDateStruct) {
    let parsed = '';
    const _minDate = new Date(this.minDate.year, this.minDate.month - 1, this.minDate.day);
    const selectedDate = new Date(date.year, date.month - 1, date.day);
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && moment(selectedDate).isSameOrAfter(_minDate)) {
      const _fromDate = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
      if (moment(selectedDate).isSameOrAfter(_fromDate)) {
        this.toDate = date;
        this.input.close();
      }
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
    if (this.fromDate) {
      parsed += this._parserFormatter.format(this.fromDate);
    }
    if (this.toDate) {
      parsed += ' - ' + this._parserFormatter.format(this.toDate);
    }
    this.renderer.setProperty(this.rangeDp.nativeElement, 'value', parsed);
  }

  onDateSelectionNew(date: NgbDateStruct) {
    let parsed = "";
    const _minDate = new Date(this.minDate.year, this.minDate.month - 1, this.minDate.day);
    const selectedDate = new Date(date.year, date.month - 1, date.day);
    if (!this.fromDate && !this.toDate) {
      if (moment(selectedDate).isSameOrAfter(_minDate)) {
        this.fromDate = date;
      } else {
        this.fromDate = null
        return
      }
    } else if (this.fromDate && !this.toDate && moment(selectedDate).isSameOrAfter(_minDate)) {
      const _fromDate = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
      if (moment(selectedDate).isSameOrAfter(_fromDate)) {
        this.toDate = date;
        this.input.close();
      }
    } else {
      const selectedDate = new Date(date.year, date.month - 1, date.day);
      if (moment(selectedDate).isSameOrAfter(_minDate)) {
        this.toDate = null;
        this.fromDate = date;
      } else {
        this.fromDate = null
        return
      }
    }
    if (this.fromDate) { parsed += this._parserFormatter.format(this.fromDate); }
    if (this.toDate) { parsed += " - " + this._parserFormatter.format(this.toDate); }
    this.renderer.setProperty(this.rangeDp.nativeElement, 'value', parsed);
  }

  closePopup(val?) {
    // let object = {
    //   data: this.allRatesFilledData
    // };
    this.closeModal((val) ? val : null);
  }
  closeModal(status) {
    this._sharedService.draftRowAddAir.next(null);
    this._activeModal.close(status);
    document.getElementsByTagName('html')[0].style.overflowY = 'auto';
  }

  airlines = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => (!term || term.length < 2) ? []
        : this.allAirLines.filter(v => v.title && v.title.toLowerCase().indexOf(term.toLowerCase()) > -1))
    )

  formatter = (x: { code: string; title: string }) => x.code + ', ' + x.title;

  ports = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => (!term || term.length < 3)
        ? []
        : this.getFilterdPorts(term))
    )
  portsFormatter = (x: { PortName: string }) => x.PortName;

  getFilterdPorts(term): any[] {
    const { allPorts } = this
    let toSend = []
    try {
      const _firstIteration: Array<any> = allPorts.filter(v => v.PortCode.toLowerCase().indexOf(term.toLowerCase()) > -1)
      const _secondIteration: Array<any> = allPorts.filter(v => v.PortName.toLowerCase().indexOf(term.toLowerCase()) > -1)
      const _combinedArr = _firstIteration.concat(_secondIteration)
      toSend = removeDuplicates(_combinedArr, "PortCode");
      // console.log(toSend);

    } catch (error) {
      // console.log(error);
    }
    return toSend
  }


  currencies = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term =>
        !term || term.length < 3
          ? []
          : this.allCurrencies.filter(
            v =>
              v.shortName &&
              v.shortName.toLowerCase().indexOf(term.toLowerCase()) > -1
          )
      )
    );
  currencyFormatter = x => x.shortName;

  getDropdownLists() {
    // this.loading = true
    this.getDepartureDays()
    this.getAirSlabs()
    this.getAirFreightTypes()
    this.allPorts = JSON.parse(localStorage.getItem('AirPortDetails'))
    setTimeout(() => {
      const carriers = JSON.parse(localStorage.getItem('carriersList'))
      this.allAirLines = carriers.filter(e => e.type === 'AIR');
    }, 500);
  }

  public departureDays: any[] = []
  public cutoffDays: any[] = []
  /**
   * GET DEPARTURE DAYS DROPDOWN
   *
   * @memberof AirRateDialogComponent
   */
  public selectedDepartureDays: any[] = []
  public selectedCutoffDays: any[] = []
  getDepartureDays() {
    this.departureDays = JSON.parse(localStorage.getItem('departureDays'))
    this.cutoffDays = JSON.parse(localStorage.getItem('departureDays'))
    if (this.departureDays) {
      this.createDepartureDaysJSON()
    } else {
      this._commonService.getMstCodeVal('DEPARTURE_DAYS').pipe(untilDestroyed(this)).subscribe((res: any) => {
        this.departureDays = res
        this.cutoffDays = res
        this.departureDays.forEach(e => {
          e.isChecked = false
        })
        this.cutoffDays.forEach(e => {
          e.isChecked = false
        })
        localStorage.setItem('departureDays', JSON.stringify(this.departureDays))
        localStorage.setItem('cutoffDays', JSON.stringify(this.cutoffDays))
        // EDIT DRAFT CASE
        this.createDepartureDaysJSON()
      }, (err: any) => {
        this._toast.error('Error fetching air freight departure days')
      })
    }
  }


  /**
   * SETTING JSON FOR EDIT DEPARTURE DAYS
   *
   * @memberof AirRateDialogComponent
   */
  createDepartureDaysJSON() {
    // EDIT DRAFT CASE (DEPARTURE DAYS)
    if (this.selectedData.data && this.selectedData.data.jsonDepartureDays && this.selectedData.mode === 'draft') {
      const parsedDepartureDays = JSON.parse(this.selectedData.data.jsonDepartureDays)
      const _days: Array<string> = []
      try {
        Object.entries(parsedDepartureDays).forEach(([key, value]) => {
          if (value === 1) {
            _days.push(key)
          }
        })
      } catch (error) {
        console.log(error);
      }
      const shouldDisableAll = (_days.includes('DAILY') || _days.includes('UPON_BOOKING')) ? true : false
      this.departureDays.forEach(e => {
        if (e.codeVal.includes('D1')) {
          if (parsedDepartureDays.D1 === 1) {
            e.isChecked = true
            this.selectedDepartureDays.push(e.codeVal)
          }
          e.isDisabled = shouldDisableAll
        }
        if (e.codeVal.includes('D2')) {
          if (parsedDepartureDays.D2 === 1) {
            e.isChecked = true
            this.selectedDepartureDays.push(e.codeVal)
          }
          e.isDisabled = shouldDisableAll
        }
        if (e.codeVal.includes('D3')) {
          if (parsedDepartureDays.D3 === 1) {
            e.isChecked = true
            this.selectedDepartureDays.push(e.codeVal)
          }
          e.isDisabled = shouldDisableAll
        }
        if (e.codeVal.includes('D4')) {
          if (parsedDepartureDays.D4 === 1) {
            e.isChecked = true
            this.selectedDepartureDays.push(e.codeVal)
          }
          e.isDisabled = shouldDisableAll
        }
        if (e.codeVal.includes('D5')) {
          if (parsedDepartureDays.D5 === 1) {
            e.isChecked = true
            this.selectedDepartureDays.push(e.codeVal)
          }
          e.isDisabled = shouldDisableAll
        }
        if (e.codeVal.includes('D6')) {
          if (parsedDepartureDays.D6 === 1) {
            e.isChecked = true
            this.selectedDepartureDays.push(e.codeVal)
          }
          e.isDisabled = shouldDisableAll
        }
        if (e.codeVal.includes('D7')) {
          if (parsedDepartureDays.D7 === 1) {
            e.isChecked = true
            this.selectedDepartureDays.push(e.codeVal)
          }
          e.isDisabled = shouldDisableAll
        }
        if (e.codeVal.includes('DAILY')) {
          if (parsedDepartureDays.DAILY === 1) {
            e.isChecked = true
            e.isDisabled = false
            this.selectedDepartureDays.push(e.codeVal)
          }
        }
        if (e.codeVal.includes('UPON_BOOKING')) {
          if (parsedDepartureDays.UPON_BOOKING === 1) {
            e.isChecked = true
            e.isDisabled = false
            this.selectedDepartureDays.push(e.codeVal)
          }
        }
      })
    } else if (this.selectedData.data && this.selectedData.data.length && this.selectedData.mode === 'publish') {
      const parsedDepartureDays = JSON.parse(this.selectedData.data[0].jsonDepartureDays)
      if (parsedDepartureDays) {
        this.departureDays.forEach(e => {
          // parsedDepartureDays.forEach(element => {
          //   if (e.codeValID === element.codeValID) {
          //     e.isChecked = true
          //     e.isDisabled = false
          //     this.selectedDepartureDays.push(element.codeVal)
          //   }
          // });
          if (e.codeVal.includes('D1')) {
            if (parsedDepartureDays.D1 === 1) {
              e.isChecked = true
              e.isDisabled = false
              this.selectedDepartureDays.push(e.codeVal)
            }
          }
          if (e.codeVal.includes('D2')) {
            if (parsedDepartureDays.D2 === 1) {
              e.isChecked = true
              e.isDisabled = false
              this.selectedDepartureDays.push(e.codeVal)
            }
          }
          if (e.codeVal.includes('D3')) {
            if (parsedDepartureDays.D3 === 1) {
              e.isChecked = true
              e.isDisabled = false
              this.selectedDepartureDays.push(e.codeVal)
            }
          }
          if (e.codeVal.includes('D4')) {
            if (parsedDepartureDays.D4 === 1) {
              e.isChecked = true
              e.isDisabled = false
              this.selectedDepartureDays.push(e.codeVal)
            }
          }
          if (e.codeVal.includes('D5')) {
            if (parsedDepartureDays.D5 === 1) {
              e.isChecked = true
              e.isDisabled = false
              this.selectedDepartureDays.push(e.codeVal)
            }
          }
          if (e.codeVal.includes('D6')) {
            if (parsedDepartureDays.D6 === 1) {
              e.isChecked = true
              e.isDisabled = false
              this.selectedDepartureDays.push(e.codeVal)
            }
          }
          if (e.codeVal.includes('D7')) {
            if (parsedDepartureDays.D7 === 1) {
              e.isChecked = true
              e.isDisabled = false
              this.selectedDepartureDays.push(e.codeVal)
            }
          }
          if (e.codeVal.includes('DAILY')) {
            if (parsedDepartureDays.DAILY === 1) {
              e.isChecked = true
              e.isDisabled = false
              this.selectedDepartureDays.push(e.codeVal)
            }
          }
          if (e.codeVal.includes('UPON_BOOKING')) {
            if (parsedDepartureDays.UPON_BOOKING === 1) {
              e.isChecked = true
              e.isDisabled = false
              this.selectedDepartureDays.push(e.codeVal)
            }
          }
        })
      }
    }

    // EDIT DRAFT CASE (CUTT OFF DAYS)
    if (this.selectedData.data && this.selectedData.data.jsonCutOffDays && this.selectedData.mode === 'draft') {
      const parsedCutoffDays = JSON.parse(this.selectedData.data.jsonCutOffDays)
      const _days: Array<string> = []
      try {
        Object.entries(parsedCutoffDays).forEach(([key, value]) => {
          if (value === 1) {
            _days.push(key)
          }
        })
      } catch (error) {
        console.log(error);
      }
      const shouldDisableAll = (_days.includes('DAILY') || _days.includes('UPON_BOOKING')) ? true : false
      this.cutoffDays.forEach(e => {
        if (e.codeVal.includes('D1')) {
          if (parsedCutoffDays.D1 === 1) {
            e.isChecked = true
            this.selectedCutoffDays.push(e.codeVal)
          }
          e.isDisabled = shouldDisableAll
        }
        if (e.codeVal.includes('D2')) {
          if (parsedCutoffDays.D2 === 1) {
            e.isChecked = true
            this.selectedCutoffDays.push(e.codeVal)
          }
          e.isDisabled = shouldDisableAll
        }
        if (e.codeVal.includes('D3')) {
          if (parsedCutoffDays.D3 === 1) {
            e.isChecked = true
            this.selectedCutoffDays.push(e.codeVal)
          }
          e.isDisabled = shouldDisableAll
        }
        if (e.codeVal.includes('D4')) {
          if (parsedCutoffDays.D4 === 1) {
            e.isChecked = true
            this.selectedCutoffDays.push(e.codeVal)
          }
          e.isDisabled = shouldDisableAll
        }
        if (e.codeVal.includes('D5')) {
          if (parsedCutoffDays.D5 === 1) {
            e.isChecked = true
            this.selectedCutoffDays.push(e.codeVal)
          }
          e.isDisabled = shouldDisableAll
        }
        if (e.codeVal.includes('D6')) {
          if (parsedCutoffDays.D6 === 1) {
            e.isChecked = true
            this.selectedCutoffDays.push(e.codeVal)
          }
          e.isDisabled = shouldDisableAll
        }
        if (e.codeVal.includes('D7')) {
          if (parsedCutoffDays.D7 === 1) {
            e.isChecked = true
            this.selectedCutoffDays.push(e.codeVal)
          }
          e.isDisabled = shouldDisableAll
        }
        if (e.codeVal.includes('DAILY')) {
          if (parsedCutoffDays.DAILY === 1) {
            e.isChecked = true
            e.isDisabled = false
            this.selectedCutoffDays.push(e.codeVal)
          }
        }
        if (e.codeVal.includes('UPON_BOOKING')) {
          if (parsedCutoffDays.UPON_BOOKING === 1) {
            e.isChecked = true
            e.isDisabled = false
            this.selectedCutoffDays.push(e.codeVal)
          }
        }
      })
    } else if (this.selectedData.data && this.selectedData.data.length && this.selectedData.mode === 'publish') {
      if (this.selectedData.data[0].jsonCutOffDays) {
        const parsedCutoffDays = JSON.parse(this.selectedData.data[0].jsonCutOffDays)
        if (parsedCutoffDays) {
          this.cutoffDays.forEach(e => {
            // parsedCutoffDays.forEach(element => {
            //   if (e.codeValID === element.codeValID) {
            //     e.isChecked = true
            //     e.isDisabled = false
            //     this.selectedCutoffDays.push(element.codeVal)
            //   }
            // });
            if (e.codeVal.includes('D1')) {
              if (parsedCutoffDays.D1 === 1) {
                e.isChecked = true
                e.isDisabled = false
                this.selectedCutoffDays.push(e.codeVal)
              }
            }
            if (e.codeVal.includes('D2')) {
              if (parsedCutoffDays.D2 === 1) {
                e.isChecked = true
                e.isDisabled = false
                this.selectedCutoffDays.push(e.codeVal)
              }
            }
            if (e.codeVal.includes('D3')) {
              if (parsedCutoffDays.D3 === 1) {
                e.isChecked = true
                e.isDisabled = false
                this.selectedCutoffDays.push(e.codeVal)
              }
            }
            if (e.codeVal.includes('D4')) {
              if (parsedCutoffDays.D4 === 1) {
                e.isChecked = true
                e.isDisabled = false
                this.selectedCutoffDays.push(e.codeVal)
              }
            }
            if (e.codeVal.includes('D5')) {
              if (parsedCutoffDays.D5 === 1) {
                e.isChecked = true
                e.isDisabled = false
                this.selectedCutoffDays.push(e.codeVal)
              }
            }
            if (e.codeVal.includes('D6')) {
              if (parsedCutoffDays.D6 === 1) {
                e.isChecked = true
                e.isDisabled = false
                this.selectedCutoffDays.push(e.codeVal)
              }
            }
            if (e.codeVal.includes('D7')) {
              if (parsedCutoffDays.D7 === 1) {
                e.isChecked = true
                e.isDisabled = false
                this.selectedCutoffDays.push(e.codeVal)
              }
            }
            if (e.codeVal.includes('DAILY')) {
              if (parsedCutoffDays.DAILY === 1) {
                e.isChecked = true
                e.isDisabled = false
                this.selectedCutoffDays.push(e.codeVal)
              }
            }
            if (e.codeVal.includes('UPON_BOOKING')) {
              if (parsedCutoffDays.UPON_BOOKING === 1) {
                e.isChecked = true
                e.isDisabled = false
                this.selectedCutoffDays.push(e.codeVal)
              }
            }
          })
        }
      }
    }
  }


  /**
   *
   * ON DAYS CHECK
   * @param {string} item
   * @param {number} index
   * @memberof AirRateDialogComponent
   */
  public selectedAircraft: string;
  onDaysCheck(type, item, index) {
    item.isChecked = !item.isChecked
    if (type === 'departureDays') {
      this.selectedDepartureDays = []
      const selectedDepartureDaysArr = []
      const deptDays = { DAILY: 0, UPON_BOOKING: 0, D1: 0, D2: 0, D3: 0, D4: 0, D5: 0, D6: 0, D7: 0 }
      this.departureDays.forEach(e => {
        if (item.codeVal === 'DAILY' && item.isChecked) {
          if (e.codeVal !== 'DAILY') {
            e.isChecked = false
            e.isDisabled = true
            deptDays.DAILY = 0
          }
        } else if (item.codeVal === 'DAILY' && !item.isChecked) {
          if (e.codeVal !== 'DAILY') {
            e.isChecked = false
            e.isDisabled = false
            deptDays.DAILY = 0
          }
        }
        if (item.codeVal === 'UPON_BOOKING' && item.isChecked) {
          if (e.codeVal !== 'UPON_BOOKING') {
            e.isChecked = false
            e.isDisabled = true
            deptDays.UPON_BOOKING = 0
          }
        } else if (item.codeVal === 'UPON_BOOKING' && !item.isChecked) {
          if (e.codeVal !== 'UPON_BOOKING') {
            e.isChecked = false
            e.isDisabled = false
            deptDays.UPON_BOOKING = 0
          }
        }
        if (e.isChecked) {
          this.selectedDepartureDays.push(e.codeVal)
        }
      })

      this.departureDays.forEach(element => {
        if (element.isChecked) {
          if (element.codeVal === "DAILY") { // Daily
            deptDays.DAILY = 1
          }
          if (element.codeVal === "UPON_BOOKING") { // UPON_BOOKING
            deptDays.UPON_BOOKING = 1
          }
          if (element.codeVal === "D1") { // Monday - to onwards
            deptDays.D1 = 1
          }
          if (element.codeVal === "D2") {
            deptDays.D2 = 1
          }
          if (element.codeVal === "D3") {
            deptDays.D3 = 1
          }
          if (element.codeVal === "D4") {
            deptDays.D4 = 1
          }
          if (element.codeVal === "D5") {
            deptDays.D5 = 1
          }
          if (element.codeVal === "D6") {
            deptDays.D6 = 1
          }
          if (element.codeVal === "D7") {
            deptDays.D7 = 1
          }
          selectedDepartureDaysArr.push(element.codeVal)
        }
      });
      // this.jsonDepartureDays = JSON.stringify(this.departureDays.filter(e => e.isChecked))
      this.jsonDepartureDays = JSON.stringify(deptDays)

    } else if (type === 'cutoffDays') {
      this.selectedCutoffDays = []
      const selectedCutoffDaysArr = []
      const deptDays = { DAILY: 0, UPON_BOOKING: 0, D1: 0, D2: 0, D3: 0, D4: 0, D5: 0, D6: 0, D7: 0 }
      this.cutoffDays.forEach(e => {
        if (item.codeVal === 'DAILY' && item.isChecked) {
          if (e.codeVal !== 'DAILY') {
            e.isChecked = false
            e.isDisabled = true
            deptDays.DAILY = 0
          }
        } else if (item.codeVal === 'DAILY' && !item.isChecked) {
          if (e.codeVal !== 'DAILY') {
            e.isChecked = false
            e.isDisabled = false
            deptDays.DAILY = 0
          }
        }
        if (item.codeVal === 'UPON_BOOKING' && item.isChecked) {
          if (e.codeVal !== 'UPON_BOOKING') {
            e.isChecked = false
            e.isDisabled = true
            deptDays.UPON_BOOKING = 0
          }
        } else if (item.codeVal === 'UPON_BOOKING' && !item.isChecked) {
          if (e.codeVal !== 'UPON_BOOKING') {
            e.isChecked = false
            e.isDisabled = false
            deptDays.UPON_BOOKING = 0
          }
        }
        if (e.isChecked) {
          this.selectedCutoffDays.push(e.codeVal)
        }
      })

      this.cutoffDays.forEach(element => {
        if (element.isChecked) {
          if (element.codeVal === "DAILY") { // Daily
            deptDays.DAILY = 1
          }
          if (element.codeVal === "UPON_BOOKING") { // UPON_BOOKING
            deptDays.UPON_BOOKING = 1
          }
          if (element.codeVal === "D1") { // Monday - to onwards
            deptDays.D1 = 1
          }
          if (element.codeVal === "D2") {
            deptDays.D2 = 1
          }
          if (element.codeVal === "D3") {
            deptDays.D3 = 1
          }
          if (element.codeVal === "D4") {
            deptDays.D4 = 1
          }
          if (element.codeVal === "D5") {
            deptDays.D5 = 1
          }
          if (element.codeVal === "D6") {
            deptDays.D6 = 1
          }
          if (element.codeVal === "D7") {
            deptDays.D7 = 1
          }
          selectedCutoffDaysArr.push(element.codeVal)
        }
      });
      // this.jsonDepartureDays = JSON.stringify(this.departureDays.filter(e => e.isChecked))
      this.jsonCutOffDays = JSON.stringify(deptDays)

    } else if (type === 'aircraft') {
      this.aircraftTypeID = item.id
      this.selectedAircraft = item.title
    }
  }

  /**
   * GET AIR FREIGHT TYPES DROPDOWN
   *
   * @memberof AirRateDialogComponent
   */
  public airFreightTypes: any[] = []
  getAirFreightTypes() {
    this._airFreightService.getAirFreightTypes().pipe(untilDestroyed(this)).subscribe((res: any) => {
      this.airFreightTypes = res;
      localStorage.setItem('airCrafts', JSON.stringify(this.airFreightTypes))
      this.airFreightTypes.forEach(e => {
        e.isChecked = false
      })
      // EDIT DRAFT CASE
      if (this.selectedData.data && this.selectedData.data.aircraftTypeID) {
        this.airFreightTypes.forEach(e => {
          if (e.id === this.aircraftTypeID) {
            e.isChecked = true
            this.selectedAircraft = e.title
          }
        })
      } else if (this.selectedData.data && this.selectedData.data.length && this.selectedData.mode === 'publish') {
        if (this.selectedData.data[0].aircraftTypeID) {
          this.airFreightTypes.forEach(e => {
            if (e.id === this.aircraftTypeID) {
              e.isChecked = true
              this.selectedAircraft = e.title
            }
          })
        }
      }
    }, (err: any) => {
      this._toast.error('Error fetching air freight types')
    })
  }


  /**
   *
   * GET AIR SLABS
   * @memberof AirRateDialogComponent
   */
  getAirSlabs() {
    let param: boolean = this.isVirtualAirline ? true : false
    this._airFreightService.getGetAirFreightSlab(param).pipe(untilDestroyed(this)).subscribe((res: any) => {
      this.airSlabs = res.returnObject
      this.loading = false
    }, (err: any) => {
    })
  }

  public addDestinationActive: boolean = false;
  public addOriginActive: boolean = false;
  public addDestinationDedActive: boolean = false;
  public addOriginDedActive: boolean = false;

  /**
   *
   *
   * @param {*} event
   * @param {*} type
   * @memberof AirRateDialogComponent
   */
  dropdownToggle(event, type) {
    if (event) {
      this.isDestinationChargesForm = false;
      this.isOriginChargesForm = false;
      this.surchargeBasisValidate = true;
      this.labelValidate = true;
      if (type === "destination") {
        this.addDestinationActive = true;
      } else if (type === "origin") {
        this.addOriginActive = true;
      }
    } else {
      this.addOriginActive = false;
      this.addDestinationActive = false;
    }
  }



  /**
   *
   * ON SELECT CHARGES
   * @param {*} type
   * @param {*} model
   * @param {*} index
   * @memberof AirRateDialogComponent
   */
  selectCharges(type, model, index) {
    model.Imp_Exp = type;
    if (type === "EXPORT") {
      if (
        (Object.keys(this.selectedOrigins[index]).length === 0 &&
          this.selectedOrigins[index].constructor === Object) ||
        !this.selectedOrigins[index].hasOwnProperty("currency")
      ) {
        model.CurrId = this.selectedCurrency.id;
        const { id, imageName, shortName } = this.selectedCurrency
        model.currency = { id, imageName, shortName };
      } else {
        model.CurrId = this.selectedOrigins[index].currency.id;
        const { id, imageName, shortName } = this.selectedOrigins[index].currency;
        model.currency = { id, imageName, shortName };
      }
      const { selectedOrigins } = this;
      selectedOrigins.forEach(element => {
        if (
          Object.keys(element).length === 0 &&
          element.constructor === Object
        ) {
          let idx = selectedOrigins.indexOf(element);
          selectedOrigins.splice(idx, 1);
        }
      });
      if (selectedOrigins[index]) {
        this.originsList.push(selectedOrigins[index]);
        selectedOrigins[index] = model;
      } else {
        selectedOrigins.push(model);
      }
      this.selectedOrigins = cloneObject(selectedOrigins);
      this.originsList = this.originsList.filter(
        e => e.addChrID && e.addChrID !== model.addChrID
      );
    } else if (type === "IMPORT") {
      if (
        (Object.keys(this.selectedDestinations[index]).length === 0 &&
          this.selectedDestinations[index].constructor === Object) ||
        !this.selectedDestinations[index].hasOwnProperty("currency")
      ) {
        model.CurrId = this.selectedCurrency.id;
        const { id, imageName, shortName } = this.selectedCurrency
        model.currency = { id, imageName, shortName };
      } else {
        model.CurrId = this.selectedDestinations[index].currency.id;
        const { id, imageName, shortName } = this.selectedDestinations[index].currency
        model.currency = { id, imageName, shortName };
      }
      const { selectedDestinations } = this;
      selectedDestinations.forEach(element => {
        if (
          Object.keys(element).length === 0 &&
          element.constructor === Object
        ) {
          let idx = selectedDestinations.indexOf(element);
          selectedDestinations.splice(idx, 1);
        }
      });
      if (selectedDestinations[index]) {
        this.destinationsList.push(selectedDestinations[index]);
        selectedDestinations[index] = model;
      } else {
        selectedDestinations.push(model);
      }
      this.selectedDestinations = cloneObject(selectedDestinations);
      this.destinationsList = this.destinationsList.filter(
        e => e.addChrID && e.addChrID !== model.addChrID
      );
    }
  }


  /**
   *
   * SET THE DEFAULT CURRENCY
   * @memberof AirRateDialogComponent
   */
  setCurrency() {
    this.selectedCurrency = JSON.parse(localStorage.getItem("userCurrency"));
  }

  public surchargesList: any = [];

  /**
   *
   * GET SURCHARGE BASIS DROPDOWN LIST
   * @param {string} containerLoad
   * @memberof AirRateDialogComponent
   */
  getSurchargeBasis(containerLoad) {
    this._seaFreightService.getSurchargeBasis(containerLoad).subscribe(
      res => {
        this.surchargesList = res;
      },
      err => { }
    );
  }


  /**
   *
   * TOGGLE CUSTOM CHARGES FORM
   * @param {string} type
   * @memberof AirRateDialogComponent
   */
  showCustomChargesForm(type) {
    if (type === "origin") {
      this.isOriginChargesForm = !this.isOriginChargesForm;
    } else if (type === "destination") {
      this.isDestinationChargesForm = !this.isDestinationChargesForm;
    }
  }

  closeDropdown(event) {
    if (this.originSimpleCharges || this.destinationSimpleCharges) {
      return;
    }
    let x: any = document.getElementsByClassName("dropdown-menu");
    if (!event.target.className.includes("has-open")) {
      this.originDropdown.close();
      this.destinationDropdown.close();
      // this.transitDropdown.close()
    }
    // if (!this._eref.nativeElement.contains(event.target)) // or some similar check
  }

  public canAddLabel: boolean = true;

  /**
   *
   * SUBMIT CALL FOR CUSTOM LABEL
   * @param {string} type
   * @returns
   * @memberof AirRateDialogComponent
   */
  addCustomLabel(type) {
    this.canAddLabel = true;
    if (!this.lablelName) {
      this.labelValidate = false;
      return;
    }
    if (!this.surchargeType) {
      this.surchargeBasisValidate = false;
      return;
    }
    const selectedSurcharge = this.surchargesList.find(
      obj => obj.codeValID === parseInt(this.surchargeType)
    );
    let obj = {
      addChrID: -1,
      addChrCode: "OTHR",
      addChrName: this.lablelName,
      addChrDesc: this.lablelName,
      modeOfTrans: "air",
      addChrBasis: selectedSurcharge.codeVal,
      createdBy: this.userProfile.UserID,
      addChrType: "ADCH",
      providerID: this.userProfile.ProviderID,
      ContainerLoadType: this.selectedData.forType,
    };
    this.selectedData.addList.forEach(element => {
      if (element.addChrName === obj.addChrName) {
        this.canAddLabel = false;
      }
    });

    if (!this.canAddLabel) {
      this._toast.info("Already Added, Please try another name", "Info");
      return false;
    }

    this._seaFreightService.addCustomCharge(obj).subscribe(
      (res: any) => {
        this.isOriginChargesForm = false;
        this.isDestinationChargesForm = false;
        if (res.returnId !== -1) {
          let obj = {
            addChrID: res.returnId,
            addChrCode: "OTHR",
            addChrName: this.lablelName,
            addChrDesc: this.lablelName,
            modeOfTrans: "AIR",
            addChrBasis: selectedSurcharge.codeVal,
            createdBy: this.userProfile.UserID,
            ContainerLoadType: this.selectedData.forType,
            addChrType: "ADCH",
            providerID: this.userProfile.ProviderID
          };
          this.originsList.push(obj);
          this.destinationsList.push(obj);
          localStorage.removeItem('additionalCharges')
          this.getAllAdditionalCharges(this.userProfile.ProviderID)
          // if (type === "origin") {
          //   this.originsList.push(obj);
          // } else if (type === "destination") {
          //   this.destinationsList.push(obj);
          // }
          this.lablelName = "";
          this.surchargeType = "";
        }
      },
      err => {
      }
    );
  }

  getAllAdditionalCharges(providerID) {
    this._seaFreightService.getAllAdditionalCharges(providerID).subscribe((res: any) => {
      localStorage.setItem('additionalCharges', JSON.stringify(res.filter(e => e.addChrType === 'ADCH')))
      loading(false)
    }, (err) => {
      loading(false)
    })
  }

  getVal(idx, event, type) {
    if (typeof event === "object") {
      if (type === "origin") {
        this.selectedOrigins[idx].CurrId = event.id;
      } else if (type === "destination") {
        this.selectedDestinations[idx].CurrId = event.id;
      }
    }
  }


  /**
   *
   * ON ADD MORE CHARGES BUTTON CLICK
   * @param {string} type
   * @returns
   * @memberof AirRateDialogComponent
   */
  addMoreCharges(type) {
    if (type === "origin") {
      if (!this.selectedOrigins[this.selectedOrigins.length - 1].CurrId) {
        this._toast.info("Please select currency", "Info");
        return;
      }
      if (!this.selectedOrigins[this.selectedOrigins.length - 1].Price) {
        this._toast.info("Please add price", "Info");
        return;
      }
      if (!this.selectedOrigins[this.selectedOrigins.length - 1].addChrCode) {
        this._toast.info("Please select any additional charge", "Info");
        return;
      }
      if (
        !(
          Object.keys(this.selectedOrigins[this.selectedOrigins.length - 1])
            .length === 0 &&
          this.selectedOrigins[this.selectedOrigins.length - 1].constructor ===
          Object
        ) &&
        parseFloat(this.selectedOrigins[this.selectedOrigins.length - 1].Price) &&
        this.selectedOrigins[this.selectedOrigins.length - 1].CurrId
      ) {
        const { id, imageName, shortName } = this.selectedOrigins[this.selectedOrigins.length - 1].currency;
        this.selectedOrigins.push({
          CurrId: id,
          currency: { id, imageName, shortName }
        });
      }
    } else if (type === "destination") {
      if (
        !this.selectedDestinations[this.selectedDestinations.length - 1].CurrId
      ) {
        this._toast.info("Please select currency", "Info");
        return;
      }
      if (
        !this.selectedDestinations[this.selectedDestinations.length - 1].Price
      ) {
        this._toast.info("Please add price", "Info");
        return;
      }
      if (
        !this.selectedDestinations[this.selectedDestinations.length - 1]
          .addChrCode
      ) {
        this._toast.info("Please select any additional charge", "Info");
        return;
      }
      if (
        !(
          Object.keys(
            this.selectedDestinations[this.selectedDestinations.length - 1]
          ).length === 0 &&
          this.selectedDestinations[this.selectedDestinations.length - 1]
            .constructor === Object
        ) &&
        parseFloat(
          this.selectedDestinations[this.selectedDestinations.length - 1].Price
        ) &&
        this.selectedDestinations[this.selectedDestinations.length - 1].CurrId
      ) {
        const { id, imageName, shortName } = this.selectedDestinations[this.selectedDestinations.length - 1].currency;
        this.selectedDestinations.push({
          CurrId: id,
          currency: { id, imageName, shortName }
        });
      }
    }
  }


  calculateSlabsPrice() {
    let tempPrice: number = 0
    for (let index = 0; index < this.airSlabs.length; index++) {
      if (this.slabPrice[index]) {
        this.airSlabs[index].ViewPrice = this.slabPrice[index]
      }
      if (this.airSlabs[index].ViewPrice > 0) {
        tempPrice = this.airSlabs[index].ViewPrice
      }
      this.airSlabs[index].Price = tempPrice
    }
  }


  /**
   *
   * VALIDATION FOR NUMBERIC FIELD
   * @param {object} e
   * @returns // true or false
   * @memberof AirRateDialogComponent
   */
  validateNumber(e) {
    let keycode = (e.which) ? e.which : e.keyCode;
    //comparing pressed keycodes
    if ((keycode < 48 || keycode > 57) && keycode !== 13) {
      e.preventDefault();
      return false;
    }
  }

  /**
   * GET BASE URL FOR UI IMAGES
   *
   * @param {string} $image
   * @returns
   * @memberof AIRRateDialogComponent
   */
  getShippingLineImage($image: string) {
    return getImagePath(
      ImageSource.FROM_SERVER,
      "/" + $image,
      ImageRequiredSize.original
    );
  }


  /**
   * TRANSIT DAYS VALIDATION
   *
   * @param {string} type //minimum or maximum
   * @returns boolean
   * @memberof AirRateDialogComponent
   */
  public isTransitDaysValidated: boolean = true
  validateTransitDays(type) {
    if (type === 'minimum') {
      if ((this.maxTransitDays && this.maxTransitDays) && (this.minTransitDays >= this.maxTransitDays)) {
        this._toast.error('Minimum transit days should be less than maximum transit days', 'Error')
        this.isTransitDaysValidated = false
        return;
      }
      if (!this.minTransitDays) {
        this._toast.error('Please provide minimum transit days', 'Error')
        this.isTransitDaysValidated = false
        return;
      }
    }
    // else if (type === 'maximum') {
    //   if ((this.minTransitDays && this.maxTransitDays) && (this.minTransitDays >= this.maxTransitDays)) {
    //     this._toast.error('Maximum transit days should be less than minimum transit days', 'Error')
    //     this.isTransitDaysValidated = false
    //     return;
    //   }
    //   if (!this.maxTransitDays) {
    //     this._toast.error('Please provide maximum transit days', 'Error')
    //     this.isTransitDaysValidated = false
    //     return;
    //   }
    // }
  }


  /**
   *
   * CALCULATING SIMPLE METHOD ADDTIONAL CHARGES
   * @memberof AirRateDialogComponent
   */
  public baseRateOrigin: any
  public baseRateDestination: any
  public originSimpleCharges: any = []
  public destinationSimpleCharges: any = []
  calculateAdditionalSimpleData() {
    this.destinationSimpleCharges = []
    this.originSimpleCharges = []
    if (this.isOriginSimplerMethod) {
      let tempPrice: number = 0
      for (let index = 0; index < this.originsListSimple[0].jsonSlabDataParsed.length; index++) {
        if (this.originSlabPrice[index]) {
          this.originsListSimple[0].jsonSlabDataParsed[index].ViewPrice = this.originSlabPrice[index]
        }
        this.originsListSimple[0].jsonSlabDataParsed[index].FixedCharges = this.baseRateOrigin
        if (this.originsListSimple[0].jsonSlabDataParsed[index].ViewPrice > 0) {
          tempPrice = this.originsListSimple[0].jsonSlabDataParsed[index].ViewPrice
        }
        this.originsListSimple[0].jsonSlabDataParsed[index].Price = tempPrice
      }
      const { addChrID, addChrCode, addChrName, addChrDesc, addChrType, addChrBasis, isSlabBased } = this.originsListSimple[0]
      const originChargeObj = {
        addChrID: addChrID,
        addChrCode: addChrCode,
        addChrName: addChrName,
        addChrDesc: addChrDesc,
        modeOfTrans: 'AIR',
        addChrType: addChrType,
        addChrBasis: addChrBasis,
        isSlabBased: isSlabBased,
        jsonSlabData: JSON.stringify(this.originsListSimple[0].jsonSlabDataParsed),
        Imp_Exp: 'EXPORT',
        CurrId: this.selectedCurrency.id,
        currency: this.selectedCurrency,
        Price: this.baseRateOrigin
      }
      this.originSimpleCharges.push(originChargeObj)
    }

    if (this.isDestinationSimplerMethod) {
      let tempPrice: number = 0
      for (let index = 0; index < this.destinationsListSimple[0].jsonSlabDataParsed.length; index++) {
        if (this.destinationSlabPrice[index]) {
          this.destinationsListSimple[0].jsonSlabDataParsed[index].ViewPrice = this.destinationSlabPrice[index]
        }
        this.destinationsListSimple[0].jsonSlabDataParsed[index].FixedCharges = this.baseRateDestination
        if (this.destinationsListSimple[0].jsonSlabDataParsed[index].ViewPrice > 0) {
          tempPrice = this.destinationsListSimple[0].jsonSlabDataParsed[index].ViewPrice
        }
        this.destinationsListSimple[0].jsonSlabDataParsed[index].Price = tempPrice
      }

      const { addChrID, addChrCode, addChrName, addChrDesc, addChrType, addChrBasis, isSlabBased } = this.destinationsListSimple[0]
      const destinationChargeObj = {
        addChrID: addChrID,
        addChrCode: addChrCode,
        addChrName: addChrName,
        addChrDesc: addChrDesc,
        modeOfTrans: 'AIR',
        addChrType: addChrType,
        addChrBasis: addChrBasis,
        isSlabBased: isSlabBased,
        jsonSlabData: JSON.stringify(this.destinationsListSimple[0].jsonSlabDataParsed),
        Imp_Exp: 'IMPORT',
        CurrId: this.selectedCurrency.id,
        currency: this.selectedCurrency,
        Price: this.baseRateDestination
      }
      this.destinationSimpleCharges.push(destinationChargeObj)
    }
  }

  /**
  * [Udpdate Published Record Button Action]
  * @param  type [string]
  * @return [description]
  */
  updatePublishedRate() {
    let rateData = [];
    try {
      if (this.fromDate && this.fromDate.day && (!this.toDate || !this.toDate.day)) {
        this._toast.warning("Please enter a valid date range for this rate");
        this.isRateUpdating = false;
        this.loading = false
        return;
      }
    } catch (error) {
      console.log(error);
    }
    try {
      if (!this.validateAdditionalCharges()) {
        this.loading = false
        return;
      }
      if (
        this.selectedData.data &&
        this.selectedData.data &&
        this.selectedData.data.length
      ) {
        this.calculateSlabsPrice()
        this.calculateAdditionalSimpleData()
        let JsonSurchargeDet

        if (true) {

          let _originCharge = null
          let _destCharge = null

          try {
            if (!this.isOriginSimplerMethod && !this.isDestinationSimplerMethod) {
              _originCharge = (JSON.stringify(this.selectedOrigins) === '[{}]') ? null : this.selectedOrigins
              _destCharge = (JSON.stringify(this.selectedDestinations) === '[{}]') ? null : this.selectedDestinations
            } else if (this.isOriginSimplerMethod && !this.isDestinationSimplerMethod) {
              _originCharge = (JSON.stringify(this.originSimpleCharges) === '[{}]') ? null : (JSON.parse(this.originSimpleCharges[0].jsonSlabData).reduce((a, b) => +a + +b.Price, 0) > 0) ? this.originSimpleCharges : null
              _destCharge = (JSON.stringify(this.selectedDestinations) === '[{}]') ? null : this.selectedDestinations
            } else if (!this.isOriginSimplerMethod && this.isDestinationSimplerMethod) {
              _originCharge = (JSON.stringify(this.selectedOrigins) === '[{}]') ? null : this.selectedOrigins
              _destCharge = (JSON.stringify(this.destinationSimpleCharges) === '[{}]') ? null : (JSON.parse(this.destinationSimpleCharges[0].jsonSlabData).reduce((a, b) => +a + +b.Price, 0) > 0) ? this.destinationSimpleCharges : null
            } else if (this.isOriginSimplerMethod && this.isDestinationSimplerMethod) {
              _originCharge = (JSON.stringify(this.originSimpleCharges) === '[{}]') ? null : (JSON.parse(this.originSimpleCharges[0].jsonSlabData).reduce((a, b) => +a + +b.Price, 0) > 0) ? this.originSimpleCharges : null
              _destCharge = (JSON.stringify(this.destinationSimpleCharges) === '[{}]') ? null : (JSON.parse(this.destinationSimpleCharges[0].jsonSlabData).reduce((a, b) => +a + +b.Price, 0) > 0) ? this.destinationSimpleCharges : null
            }

          } catch (error) {
            console.log(error);

          }
          // console.log(_originCharge, _destCharge);

          if (!_originCharge && !_destCharge) {
            JsonSurchargeDet = null
          } else if (_originCharge && !_destCharge) {
            JsonSurchargeDet = JSON.stringify(_originCharge)
          } else if (!_originCharge && _destCharge) {
            JsonSurchargeDet = JSON.stringify(_destCharge)
          } else if (_originCharge && _destCharge) {
            JsonSurchargeDet = JSON.stringify(_originCharge.concat(_destCharge))
          }
        }

        if (this.isVirtualAirline && this.isChinaPort) {
          const parsedJsonSurchargeDet = JSON.parse(JsonSurchargeDet)
          if (JSON.stringify(this.selectedOrigins) !== '[{}]') {
            JsonSurchargeDet = JSON.stringify(parsedJsonSurchargeDet.concat((this.selectedOrigins)))
          } else {
            JsonSurchargeDet = JSON.stringify(parsedJsonSurchargeDet)
          }
        } else if (this.isVirtualAirline && !this.isChinaPort) {
          if (JSON.stringify(this.selectedOrigins) !== '[{}]') {
            JsonSurchargeDet = JSON.stringify((this.selectedOrigins))
          }
        }

        if (this.isVirtualAirline) {
          // console.log(this.airSlabs[0].ViewPrice);

          const surchargeDet = JSON.parse(JsonSurchargeDet)
          const { airSlabs } = this
          const hasFobRates = airSlabs.filter(slab => slab.ViewPrice > 0)
          if (!(hasFobRates && hasFobRates.length > 0)) {
            this._toast.warning('Please provide alteast one slab for FOB rate', 'Warning')
            this.loading = false
            return
          }
          // console.log(this.baseRateOrigin)

          if (this.isChinaPort) {
            if (this.baseRateOrigin === 0 || !this.baseRateOrigin) {
              this._toast.warning('Please provide base rate for FCA rate', 'Warning')
              this.loading = false
              return
            }
            if (JSON.parse(surchargeDet[0].jsonSlabData)[0].ViewPrice === 0 || !(JSON.parse(surchargeDet[0].jsonSlabData)[0].ViewPrice)) {
              this._toast.warning('Please provide FCA Per KG rate', 'Warning')
              this.loading = false
              return
            }
            if (this.baseRateDestination === 0 || !this.baseRateDestination) {
              this._toast.warning('Please provide base rate for Ex Works rate', 'Warning')
              this.loading = false
              return
            }
            if (JSON.parse(surchargeDet[1].jsonSlabData)[0].ViewPrice === 0 || !(JSON.parse(surchargeDet[1].jsonSlabData)[0].ViewPrice)) {
              this._toast.warning('Please provide Ex Works Per KG rate', 'Warning')
              this.loading = false
              return
            }
          }
        }

        if (!this.isVirtualAirline) {
          const surchargeDet = JSON.parse(JsonSurchargeDet)

          const { airSlabs } = this
          const hasFobRates = airSlabs.filter(slab => slab.ViewPrice > 0)
          if (!(hasFobRates && hasFobRates.length > 0)) {
            this._toast.warning('Please provide alteast one slab for Freight rate', 'Warning')
            this.loading = false
            return
          }

          // if (!this.airSlabs[0].ViewPrice || this.airSlabs[0].ViewPrice === 0) {
          //   this._toast.warning('Please provide Normal rate for Freight', 'Warning')
          //   this.loading = false
          //   return
          // }
          if (this.isOriginSimplerMethod) {
            try {
              const originSlab = JSON.parse(surchargeDet[0].jsonSlabData)
              const _slabWithRate = originSlab.filter(slab => slab.ViewPrice > 0)
              if (this.baseRateOrigin && this.baseRateOrigin > 0) {
                if (!_slabWithRate || _slabWithRate.length === 0) {
                  this._toast.warning('Please provide Per KG rate at the Port of Origin', 'Invalid Additional Charge')
                  this.loading = false
                  return
                }
              }
              if ((_slabWithRate && _slabWithRate.length > 0) && (!this.baseRateOrigin || this.baseRateOrigin === 0)) {
                this._toast.warning('Please provide base rate at the Port of Origin', 'Invalid Additional Charge')
                this.loading = false
                return
              }
            } catch (error) { }
          }
          if (this.isDestinationSimplerMethod) {
            try {
              let index = 0

              if (this.isOriginSimplerMethod || (surchargeDet && surchargeDet.filter(_charge => _charge.Imp_Exp === 'IMPORT'))) {
                index = 1
              }
              const destinSlab = JSON.parse(surchargeDet[index].jsonSlabData)
              const _slabWithRate = destinSlab.filter(slab => slab.ViewPrice > 0)
              if (this.baseRateDestination && this.baseRateDestination > 0 && (!_slabWithRate || _slabWithRate.length === 0)) {
                this._toast.warning('Please provide Per KG rate at the Port of Destination', 'Invalid Additional Charge')
                this.loading = false
                return
              }
              console.log((_slabWithRate && _slabWithRate.length > 0) && (!this.baseRateDestination || this.baseRateDestination === 0))
              if ((_slabWithRate && _slabWithRate.length > 0) && (!this.baseRateDestination || this.baseRateDestination === 0)) {
                this._toast.warning('Please provide base rate at the Port of Destination', 'Invalid Additional Charge')
                this.loading = false
                return
              }
            } catch (error) { }
          }
        }

        if (!this.fromDate || this.fromDate === null) {
          this._toast.warning("Effective from Cannot be empty");
          this.isRateUpdating = false;
          this.loading = false
          return;
        }

        if (!this.toDate || this.toDate === null) {
          this._toast.warning("Effective to Cannot be empty");
          this.isRateUpdating = false;
          this.loading = false
          return;
        }

        this.selectedData.data.forEach(element => {
          let AIRObj = {
            carrierPricingID: element.carrierPricingID,
            minPrice: this.minPrice,
            effectiveFrom:
              this.fromDate && this.fromDate.month
                ? this.fromDate.month +
                "/" +
                this.fromDate.day +
                "/" +
                this.fromDate.year
                : null,
            effectiveTo:
              this.toDate && this.toDate.month
                ? this.toDate.month +
                "/" +
                this.toDate.day +
                "/" +
                this.toDate.year
                : null,
            modifiedBy: this.userProfile.UserID,
            jsonSurchargeDetail: (JsonSurchargeDet === 'null') ? null : JsonSurchargeDet,
            customerID: (this.selectedCustomer && this.selectedCustomer.length) ? this.selectedCustomer[0].CustomerID : null,
            jsonCustomerDetail:
              JSON.stringify(element.jsonCustomerDetail) === "[{},{}]"
                ? null
                : element.jsonCustomerDetail,
            customerType: element.customerType,
            minTransitDays: this.minTransitDays ? this.minTransitDays : 0,
            maxTransitDays: this.maxTransitDays ? this.maxTransitDays : 0,
            jsonDepartureDays: this.jsonDepartureDays,
            aircraftTypeID: this.aircraftTypeID,
            pricingJson: JSON.stringify(this.airSlabs)
          };
          rateData.push(AIRObj);
        });
      }
    } catch (error) {
      console.log(error);

      this.isRateUpdating = false;
      this.loading = false
    }
    this._airFreightService.rateValidity(rateData).subscribe(
      (res: any) => {
        loading(false);
        this.isRateUpdating = false;
        this.loading = false
        if (res.returnId > 0) {
          if (res.returnText && typeof res.returnText === 'string') {
            this._toast.success(res.returnText, "Success");
          } else {
            this._toast.success("Rates added successfully", "Success");
          }
          this.closeModal(true);
        } else {
          this._toast.warning(res.returnText);
        }
      },
      error => {
        this.isRateUpdating = false;
        loading(false);
        this.loading = false
        this._toast.error("Error while saving rates, please try later");
      }
    );
  }


  /**
   * VALIDATE ADDITIONAL CHARGES
   *
   * @returns {boolean}
   * @memberof AirRateDialogComponent
   */
  validateAdditionalCharges(): boolean {
    let ADCHValidated: boolean = true;
    // let exportCharges
    // let importCharges
    // if (obj.JsonSurchargeDet) {
    //   const parsedJsonSurchargeDet = JSON.parse(obj.JsonSurchargeDet)
    //   exportCharges = parsedJsonSurchargeDet.filter(e => e.Imp_Exp === 'EXPORT')
    //   importCharges = parsedJsonSurchargeDet.filter(e => e.Imp_Exp === 'IMPORT')
    // }
    if (!this.isOriginSimplerMethod && this.selectedOrigins && this.selectedOrigins.length > 0) {
      this.selectedOrigins.forEach(element => {
        if (
          Object.keys(element).length &&
          (!element.Price ||
            !(typeof parseFloat(element.Price) == "number") ||
            parseFloat(element.Price) <= 0)
        ) {
          this._toast.error("Price is missing for Additional Charge", "Error");
          this.isRateUpdating = false;
          this.loading = false
          ADCHValidated = false;
          return ADCHValidated;
        }

        if (element.addChrBasis !== 'PER_SHIPMENT' && Object.keys(element).length) {
          if (
            element.MaxPrice && (typeof parseFloat(element.MaxPrice) == "number") && parseFloat(element.MaxPrice) > 0 &&
            (!element.MinPrice || !(typeof parseFloat(element.MinPrice) == "number") || parseFloat(element.MinPrice) <= 0)
          ) {
            this._toast.error("Please enter minimum price along with maximum price", "Error");
            this.isRateUpdating = false;
            this.loading = false
            ADCHValidated = false;
            return ADCHValidated;
          }

          if (
            element.MinPrice && (typeof parseFloat(element.MinPrice) == "number") && parseFloat(element.MinPrice) > 0 &&
            (!element.MaxPrice || !(typeof parseFloat(element.MaxPrice) == "number") || parseFloat(element.MaxPrice) <= 0)
          ) {
            this._toast.error("Please enter maximum price along with minimum price", "Error");
            this.isRateUpdating = false;
            this.loading = false
            ADCHValidated = false;
            return ADCHValidated;
          }

          if (element.addChrBasis !== 'PER_SHIPMENT' && parseFloat(element.MaxPrice) < parseFloat(element.MinPrice)) {
            this._toast.error("Minimum Price cannot be greater than Maximum Price for Additional Charge", "Error");
            this.isRateUpdating = false;
            this.loading = false
            ADCHValidated = false;
            return ADCHValidated;
          }
        }


        if (Object.keys(element).length && !element.CurrId) {
          this._toast.error(
            "Currency is missing for Additional Charge",
            "Error"
          );
          this.isRateUpdating = false;
          this.loading = false
          ADCHValidated = false;
          return ADCHValidated;
        }
        if (Object.keys(element).length && (!element.currency || typeof (element.currency) === 'string')) {
          this._toast.error(
            "Currency is missing for Additional Charge",
            "Error"
          );
          this.isRateUpdating = false;
          this.loading = false
          ADCHValidated = false;
          return ADCHValidated;
        }
        if (Object.keys(element).length && !element.addChrID) {
          this._toast.error("Additional Charge is missing", "Error");
          this.isRateUpdating = false;
          this.loading = false
          ADCHValidated = false;
          return ADCHValidated;
        }
      });
    }
    if (!this.isDestinationSimplerMethod && this.selectedDestinations && this.selectedDestinations.length > 0) {
      this.selectedDestinations.forEach(element => {
        if (
          Object.keys(element).length &&
          (!element.Price ||
            !(typeof parseFloat(element.Price) == "number") ||
            parseFloat(element.Price) <= 0)
        ) {
          this._toast.error("Price is missing for Additional Charge", "Error");
          this.isRateUpdating = false;
          ADCHValidated = false;
          this.loading = false
          return ADCHValidated;
        }

        if (element.addChrBasis !== 'PER_SHIPMENT' && Object.keys(element).length) {
          if (
            element.MaxPrice && (typeof parseFloat(element.MaxPrice) == "number") && parseFloat(element.MaxPrice) > 0 &&
            (!element.MinPrice || !(typeof parseFloat(element.MinPrice) == "number") || parseFloat(element.MinPrice) <= 0)
          ) {
            this._toast.error("Please enter minimum price along with maximum price", "Error");
            this.isRateUpdating = false;
            this.loading = false
            ADCHValidated = false;
            return ADCHValidated;
          }

          if (
            element.MinPrice && (typeof parseFloat(element.MinPrice) == "number") && parseFloat(element.MinPrice) > 0 &&
            (!element.MaxPrice || !(typeof parseFloat(element.MaxPrice) == "number") || parseFloat(element.MaxPrice) <= 0)
          ) {
            this._toast.error("Please enter maximum price along with minimum price", "Error");
            this.isRateUpdating = false;
            this.loading = false
            ADCHValidated = false;
            return ADCHValidated;
          }

          if (element.addChrBasis !== 'PER_SHIPMENT' && parseFloat(element.MaxPrice) < parseFloat(element.MinPrice)) {
            this._toast.error("Minimum Price cannot be greater than Maximum Price for Additional Charge", "Error");
            this.isRateUpdating = false;
            this.loading = false
            ADCHValidated = false;
            return ADCHValidated;
          }
        }


        if (Object.keys(element).length && !element.CurrId) {
          this._toast.error(
            "Currency is missing for Additional Charge",
            "Error"
          );
          this.isRateUpdating = false;
          ADCHValidated = false;
          this.loading = false
          return ADCHValidated;
        }

        if (Object.keys(element).length && (!element.currency || typeof (element.currency) === 'string')) {
          this._toast.error(
            "Currency is missing for Additional Charge",
            "Error"
          );
          this.isRateUpdating = false;
          ADCHValidated = false;
          this.loading = false
          return ADCHValidated;
        }

        if (Object.keys(element).length && !element.addChrID) {
          this._toast.error("Additional Charge is missing", "Error");
          this.isRateUpdating = false;
          ADCHValidated = false;
          this.loading = false
          return ADCHValidated;
        }
      });
    }
    return ADCHValidated;
  }



  /**
   *
   * SET ADVANCED CHARGES IN EDIT
   * @param {*} type
   * @memberof AirRateDialogComponent
   */
  setAdvancedChargesData(type) {
    let parsedJsonSurchargeDet;
    if (type === "publish") {
      parsedJsonSurchargeDet = JSON.parse(this.selectedData.data[0].jsonSurchargeDetail);
    } else if (type === "draft") {
      parsedJsonSurchargeDet = JSON.parse(
        this.selectedData.data.jsonSurchargeDetail
      );
    }
    // this.destinationsList = cloneObject(this.selectedData.addList);
    // this.originsList = cloneObject(this.selectedData.addList);
    this.originsList = cloneObject(this.selectedData.addList.filter(e => !e.isSlabBased));
    this.destinationsList = cloneObject(this.selectedData.addList.filter(e => !e.isSlabBased))
    if (parsedJsonSurchargeDet) {
      this.selectedOrigins = parsedJsonSurchargeDet.filter(
        e => e.Imp_Exp === "EXPORT" && !e.isSlabBased
      );
      this.selectedDestinations = parsedJsonSurchargeDet.filter(
        e => e.Imp_Exp === "IMPORT" && !e.isSlabBased
      );
    }

    if (!this.selectedOrigins.length) {
      this.selectedOrigins = [{}];
    }
    if (!this.selectedDestinations.length) {
      this.selectedDestinations = [{}];
    }
    if (this.selectedDestinations.length) {
      this.selectedDestinations.forEach(element => {
        this.destinationsList.forEach(e => {
          if (e.addChrID === element.addChrID) {
            let idx = this.destinationsList.indexOf(e);
            this.destinationsList.splice(idx, 1);
          }
        });
      });
    }

    if (this.selectedOrigins.length) {
      this.selectedOrigins.forEach(element => {
        this.originsList.forEach(e => {
          if (e.addChrID === element.addChrID) {
            let idx = this.originsList.indexOf(e);
            this.originsList.splice(idx, 1);
          }
        });
      });
    }
  }

  /**
   *
   * Removed added additional charges
   * @param {string} type origin/destination
   * @param {object} obj
   * @memberof AirRateDialogComponent
   */
  removeAdditionalCharge(type, obj) {
    if (type === "origin") {
      if (this.selectedOrigins.length > 0) {
        this.selectedOrigins.forEach(element => {
          if (element.addChrID === obj.addChrID) {
            if (this.selectedOrigins.length > 1) {
              let idx = this.selectedOrigins.indexOf(element);
              this.selectedOrigins.splice(idx, 1);
              // console.log(this.selectedOrigins);

            } else if (this.selectedOrigins.length === 1) {
              this.selectedOrigins = [{}]
            }
            try {
              if (element.addChrID) {
                this.originsList.push(element);
                const { originsList } = this
                const _originsList = originsList.sort(firstBy(function (v1, v2) { return v1.sortingorder - v2.sortingorder; }));
                this.originsList = _originsList
              }
            } catch (error) {
              console.log(error);

            }
          }
        });
      }
    } else if (type === "destination") {
      if (this.selectedDestinations.length > 0) {
        this.selectedDestinations.forEach(element => {
          if (element.addChrID === obj.addChrID) {
            if (this.selectedDestinations.length > 1) {
              let idx = this.selectedDestinations.indexOf(element);
              this.selectedDestinations.splice(idx, 1);
            } else if (this.selectedDestinations.length === 1) {
              this.selectedDestinations = [{}]
            }
            try {
              if (element.addChrID) {
                this.originsList.push(element);
                const { destinationsList } = this
                const _destinationsList = destinationsList.sort(firstBy(function (v1, v2) { return v1.sortingorder - v2.sortingorder; }));
                this.destinationsList = _destinationsList
              }
            } catch (error) { }
          }
        });
      }
    }
  }

  switchMethod(type) {
    if (type === 'origin') {
      this.baseRateOrigin = 0
      this.originSlabPrice = []
      this.selectedOrigins = [{}];
      this.originsList = this.selectedData.addList.filter(e => !e.isSlabBased);
    } else if (type === 'destination') {
      this.selectedDestinations = [{}];
      this.baseRateDestination = 0
      this.destinationSlabPrice = []
      this.destinationsList = this.selectedData.addList.filter(e => !e.isSlabBased)
    }
  }


  /**
   *
   * GET PRODUCT DROPDOWNS FOR AIR
   * @memberof AirRateDialogComponent
   */
  public productsList: any = []
  getProductAir() {
    this._airFreightService.getProductAir().pipe(untilDestroyed(this)).subscribe((res: any) => {
      this.productsList = res
      // console.log(res)
    }, (err: any) => {
      // console.log(err)
    })
  }




  /**
   *
   * ON ORIGIN SELECT
   * @param {object} event
   * @memberof AirRateDialogComponent
   */
  public isChinaPort: boolean = false
  onOriginSelect(event) {
    this.isChinaPort = false
    if (event.item.IsChinaGateway) {
      this.isChinaPort = true
    }
    this.viaList = []
  }

  onDestinationSelect(event) {
    this.viaList = []
  }


  // Via Port Working
  public viaPorts: ViaRoutes = {
    Stop1: { Code: '', Name: '', SCode: '' },
    Stop2: { Code: '', Name: '', SCode: '' },
    Stop3: { Code: '', Name: '', SCode: '' },
    Stop4: { Code: '', Name: '', SCode: '' },
    Stop5: { Code: '', Name: '', SCode: '' },
    Stop6: { Code: '', Name: '', SCode: '' },
    Stop7: { Code: '', Name: '', SCode: '' },
    Stop8: { Code: '', Name: '', SCode: '' },
  }

  public viaList: Array<ViraPortDtl> = []
  viaPortsFormatter = ({ PortCode, PortShortName, PortName }) => {
    // console.log(PortCode, PortShortName);
    const { filterOrigin, filterDestination } = this
    if (filterOrigin.PortCode && filterOrigin.PortCode === PortCode) {
      this._toast.warning('A Transit Airport cannot be the origin or destination airport')
      return
    }

    if (filterDestination.PortCode && filterDestination.PortCode === PortCode) {
      this._toast.warning('A Transit Airport cannot be the origin or destination airport')
      return
    }

    const { viaList } = this
    const _list = extractColumn(viaList, 'Code')
    if (_list.includes(PortCode)) {
      this._toast.warning('Airport already added')
      return
    }

    let _sCode: string = ''
    try {
      _sCode = PortName.split(',')[0]
    } catch (error) {

    }

    if (viaList.length < 8) {
      this.viaList.push({ Code: PortCode, Name: PortShortName, SCode: _sCode })
    } else {
      this._toast.warning('A Maximum of 8 Transit Airports can be entered')
      return
    }
  };

  removeViaPort(index) {
    this.viaList.splice(index, 1);
  }



  ngOnDestroy() {

  }


}

export interface ViaRoutes {
  Stop1?: ViraPortDtl;
  Stop2?: ViraPortDtl;
  Stop3?: ViraPortDtl;
  Stop4?: ViraPortDtl;
  Stop5?: ViraPortDtl;
  Stop6?: ViraPortDtl;
  Stop7?: ViraPortDtl;
  Stop8?: ViraPortDtl;
}

export interface ViraPortDtl {
  Code?: string;
  Name?: string;
  SCode?: string;
}

export interface IAirDates {
  DAILY: number;
  UPON_BOOKING: number
  D1: number;
  D2: number;
  D3: number;
  D4: number;
  D5: number;
  D6: number;
  D7: number;
}