export interface IScheduleResponse {
    CarrierScheduleID: number;
    CarrierID: number;
    CarrierName: string;
    CarrierImage: string;
    TransitDays: number;
    EtdDate: Date;
    EtaDate: Date;
    VesselCode: string;
    VesselName: string;
    VoyageRefNo: string;
    PolID: number;
    PodID: number;
    JsonTransferVesselsDetails: string;
}
export interface ICarrierSchedule {
    carrierScheduleID: number;
    carrierName: string;
    carrierImage: string;
    bookingID: number;
    carrierID: number;
    transitDays: number;
    transfersStop?: any;
    etaDate: Date;
    etdDate: Date;
    portCutOffDate: Date;
    vesselCode: string;
    vesselName: string;
    voyageRefNo: string;
    polID: number;
    polName?: any;
    polCode?: any;
    podID: number;
    podName?: any;
    podCode?: any;
    jsonTransferVesselsDetails: string;
    flightNo?: any;
}
