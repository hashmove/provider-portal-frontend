import { Component, OnInit, ViewEncapsulation, ViewChild, Output, EventEmitter, Input } from '@angular/core'
import { removeDuplicates, loading, getImagePath, ImageSource, ImageRequiredSize, getProviderImage, isMobile } from '../../../constants/globalFunctions'
import { NgbDateParserFormatter, NgbDropdownConfig, NgbDateStruct, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { ViewBookingService } from '../../../components/pages/user-desk/view-booking/view-booking.service'
import { ICarrierFilter, BookingDetails, ICompanyFilter, JsonResponse } from '../../../interfaces'
import { BookingStatus } from '../booking-status-updation/booking-status-updation.component'
import { NgbDateFRParserFormatter } from '../../../constants/ngb-date-parser-formatter'
import { NgbDate } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date'
import { debounceTime, map } from 'rxjs/operators'
import { ToastrService } from 'ngx-toastr'
import { Observable } from 'rxjs'
import * as moment from 'moment'
import { IScheduleResponse } from './add-schedule.interface'
@Component({
  selector: 'app-add-schedule',
  templateUrl: './add-schedule.component.html',
  styleUrls: ['./add-schedule.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    { provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter },
    NgbDropdownConfig
  ],
})
export class AddScheduleComponent implements OnInit {
  public maxSize: number = 7
  public directionLinks: boolean = true
  public autoHide: boolean = false
  @Input() carrierList: Array<ICarrierFilter> = []
  @Input() bookCarrier: ICarrierFilter = null
  @Input() PolID: number
  @Input() PodID: number
  @Input() bookingDetails: BookingDetails
  @Input() from: string = 'VIEW_BOOKING'
  @Input() isEdit = false

  public vesselCode: string = ''
  public vesselName: string = ''
  public voyageCode: string = ''

  public vesselCode1: string = ''
  public vesselName1: string = ''
  public voyageCode1: string = ''

  public vesselCode2: string = ''
  public vesselName2: string = ''
  public voyageCode2: string = ''

  public selectedBookStatusCode: string = null
  public selectedShipStatusCode: string = null
  public selectedStops = 'direct'

  public shipStop = ['direct', '1 t/s', '2 t/s']

  public selectedShippingModeCode = null
  public isMarketplace: boolean = false
  public filterbyContainerType
  public fromDate
  public toDate
  public fromDate1
  public toDate1
  public fromDate2
  public toDate2
  public filterOrigin: any = {}
  public filterDestination: any = {}
  public transPort1: any = {}
  public transPort1Disabled: boolean = false
  public transPort2: any = {}
  public transPort2Disabled: boolean = false
  public bookingStatuses: Array<BookingStatus> = []
  public shippingStatuses: Array<BookingStatus> = []

  public selectedCarrier: ICarrierFilter
  public selectedCompany: ICompanyFilter
  public allShippingLines = []
  public shippingModes: Array<ICarrierFilter> = []
  public sortBy: string = 'date'

  @ViewChild("d") eventDate
  @ViewChild("d1") eventDate1
  @ViewChild("d2") eventDate2
  @ViewChild("calenderField") calenderField: any
  @ViewChild("calenderField1") calenderField1: any
  @ViewChild("calenderField2") calenderField2: any
  hoveredDate: NgbDate
  hoveredDate1: NgbDate
  hoveredDate2: NgbDate
  public minDate: any
  public maxDate: any
  ports: ICarrierFilter[] = []
  selectionDuration: number = 0
  durationDdl: Array<number> = []

  @Output() onListChange = new EventEmitter<string>()
  @Input() currentTab: string = 'tab-current'
  @Input() currPage: number = 1
  @Input() schedule: any = null
  isProcessing = false

  isHovered = date => this.fromDate && !this.toDate && this.hoveredDate && after(date, this.fromDate) && before(date, this.hoveredDate)
  isInside = date => after(date, this.fromDate) && before(date, this.toDate)
  isFrom = date => equals(date, this.fromDate)
  isTo = date => equals(date, this.toDate)

  isHovered1 = date => this.fromDate1 && !this.toDate1 && this.hoveredDate1 && after(date, this.fromDate1) && before(date, this.hoveredDate1)
  isInside1 = date => after(date, this.fromDate1) && before(date, this.toDate1)
  isFrom1 = date => equals(date, this.fromDate1)
  isTo1 = date => equals(date, this.toDate1)

  isHovered2 = date => this.fromDate2 && !this.toDate2 && this.hoveredDate2 && after(date, this.fromDate2) && before(date, this.hoveredDate2)
  isInside2 = date => after(date, this.fromDate2) && before(date, this.toDate2)
  isFrom2 = date => equals(date, this.fromDate2)
  isTo2 = date => equals(date, this.toDate2)

  public trans1MinDate: any
  public trans1MaxDate: any

  public trans2MinDate: any
  public trans2MaxDate: any
  displayMonth = isMobile() ? 1 : 2

  constructor(
    private _parserFormatter: NgbDateParserFormatter,
    private _userService: ViewBookingService,
    private _toastr: ToastrService,
    private _activeModal: NgbActiveModal,
  ) { }

  async ngOnInit() {
    console.log(this.schedule)
    try { await this.getBookingStatuses((this.selectedShippingModeCode) ? this.selectedShippingModeCode : 'SEA') } catch (error) { }
    try { await this.getShippingStatus() } catch (error) { }
    try { await this.setPortsData((this.selectedShippingModeCode) ? this.selectedShippingModeCode : 'SEA') } catch (error) { }

    this.setInitData()
    try {
      const _modes = await this._userService.getShipModes().toPromise() as Array<any>
      const _filtered = _modes.filter(mode => mode.code !== 'RAIL' && mode.code !== 'HYPERLOOP')
      this.shippingModes = _filtered
    } catch (error) { }
  }

  setInitData() {
    const date = new Date()
    this.minDate = { month: date.getMonth() + 1, day: date.getDate(), year: date.getFullYear() }
    if (this.bookCarrier)
      this.selectedCarrier = this.bookCarrier
    if (this.schedule)
      this.setAddedData()
  }

  setAddedData() {
    if (this.schedule.bookingRouteMapInfo) {
      this.vesselCode = this.schedule.vesselCode
      this.vesselName = this.schedule.vesselName
      this.voyageCode = this.schedule.voyageRefNum
      this.selectionDuration = this.schedule.transitTime
      const _fromDate = new Date(this.schedule.etdLcl)
      const _toDate = new Date(this.schedule.etaLcl)
      this.fromDate = { year: _fromDate.getFullYear(), month: _fromDate.getMonth() + 1, day: _fromDate.getDate() } as any
      this.toDate = { year: _toDate.getFullYear(), month: _toDate.getMonth() + 1, day: _toDate.getDate() } as any
      this.setTransferStop(false)
      const parsed = this._parserFormatter.format(this.fromDate) + " - " + this._parserFormatter.format(this.toDate)
      this.calenderField.nativeElement.value = parsed
      try {
        if (this.schedule.bookingRouteMapInfo.jsonTransferVesselsDetails)
          this.setR1andR2(JSON.parse(this.schedule.bookingRouteMapInfo.jsonTransferVesselsDetails))
      } catch (error) {
        console.log(error)
      }
    } else if (this.schedule.JsonTransferVesselsDetails) {
      this.vesselCode = this.schedule.VesselCode
      this.vesselName = this.schedule.VesselName
      this.voyageCode = this.schedule.VoyageRefNo
      this.selectionDuration = this.schedule.TransitDays
      const _fromDate = new Date(this.schedule.EtdDate)
      const _toDate = new Date(this.schedule.EtaDate)
      this.fromDate = { year: _fromDate.getFullYear(), month: _fromDate.getMonth() + 1, day: _fromDate.getDate() } as any
      this.toDate = { year: _toDate.getFullYear(), month: _toDate.getMonth() + 1, day: _toDate.getDate() } as any
      this.setTransferStop(false)
      const parsed = this._parserFormatter.format(this.fromDate) + " - " + this._parserFormatter.format(this.toDate)
      this.calenderField.nativeElement.value = parsed
      if (this.schedule.JsonTransferVesselsDetails)
        this.setR1andR2(JSON.parse(this.schedule.JsonTransferVesselsDetails))
    }
  }

  setR1andR2(_transferDtl: IJsonTransferVesselsDetails) {
    console.log(_transferDtl)
    if (_transferDtl.R3PolCode) {
      this.vesselCode2 = _transferDtl.R3VesselNo ? _transferDtl.R3VesselNo : null
      this.vesselName2 = _transferDtl.R3VesselName ? _transferDtl.R3VesselName : null
      this.voyageCode2 = _transferDtl.R3VoyageRefNo ? _transferDtl.R3VoyageRefNo : null

      const _R3Etd = _transferDtl.R3EtdDate ? new Date(_transferDtl.R3EtdDate) : null
      const _R2Eta = _transferDtl.R2EtaDate ? new Date(_transferDtl.R2EtaDate) : null

      this.toDate2 = _R3Etd ? { year: _R3Etd.getFullYear(), month: _R3Etd.getMonth() + 1, day: _R3Etd.getDate() } : null
      this.fromDate2 = _R2Eta ? { year: _R2Eta.getFullYear(), month: _R2Eta.getMonth() + 1, day: _R2Eta.getDate() } : null
      if (this.toDate2 && this.fromDate2) {
        const parsed2 = this._parserFormatter.format(this.fromDate2) + " - " + this._parserFormatter.format(this.toDate2)
        setTimeout(() => { this.calenderField2.nativeElement.value = parsed2 }, 100);
      }
      try {
        const _port2 = this.getFilteredPorts(_transferDtl.R3PolCode, this.selectedShippingModeCode)
        this.transPort2 = _port2[0]
        if (this.transPort2 && this.transPort2.id)
          this.transPort2Disabled = true
        else
          this.transPort2Disabled = false
      } catch { }
      this.selectedStops = '2 t/s'

    }
    if (_transferDtl.R2PolCode) {
      this.vesselCode1 = _transferDtl.R2VesselNo ? _transferDtl.R2VesselNo : null
      this.vesselName1 = _transferDtl.R2VesselName ? _transferDtl.R2VesselName : null
      this.voyageCode1 = _transferDtl.R2VoyageRefNo ? _transferDtl.R2VoyageRefNo : null

      const _R2Etd = _transferDtl.R2EtdDate ? new Date(_transferDtl.R2EtdDate) : null
      const _R1Eta = _transferDtl.R1EtaDate ? new Date(_transferDtl.R1EtaDate) : null

      this.toDate1 = _R2Etd ? { year: _R2Etd.getFullYear(), month: _R2Etd.getMonth() + 1, day: _R2Etd.getDate() } : null
      this.fromDate1 = _R1Eta ? { year: _R1Eta.getFullYear(), month: _R1Eta.getMonth() + 1, day: _R1Eta.getDate() } : null

      if (this.toDate1 && this.fromDate1) {
        const parsed1 = this._parserFormatter.format(this.fromDate1) + " - " + this._parserFormatter.format(this.toDate1)
        setTimeout(() => { this.calenderField1.nativeElement.value = parsed1 }, 100);
      }

      try {
        const _port1 = this.getFilteredPorts(_transferDtl.R2PolCode, this.selectedShippingModeCode)
        this.transPort1 = _port1[0]
        if (this.transPort1 && this.transPort1.id)
          this.transPort1Disabled = true
        else
          this.transPort1Disabled = false
      } catch { }
      if (!_transferDtl.R3VesselNo)
        this.selectedStops = '1 t/s'
    }
  }


  portsFilter = (text$: Observable<string>) => (text$.debounceTime(200).map(term => !term || term.length < 3 ? []
    : this.getFilteredPorts(term, this.selectedShippingModeCode)
  ))

  getFilteredPorts(term, transportMode) {
    const { ports } = this
    let toSend = []
    if (transportMode === 'AIR') {
      const _firstIteration: Array<any> = ports.filter(v => v.code.toLowerCase().indexOf(term.toLowerCase()) > -1)
      const _secondIteration: Array<any> = ports.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1)
      const _combinedArr = _firstIteration.concat(_secondIteration)
      toSend = removeDuplicates(_combinedArr, "code")
    } else {
      toSend = ports.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1)
    }
    return toSend
  }

  filterPortsFormatter = (x: { title: string }) => x.title
  carrierFilter = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => (!term || term.length < 2) ? []
        : this.carrierList.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1))
    )
  filterCarrierFormatter = (x: { title: string }) => x.title

  showbutton(type: number) {
    if (type === 0) { try { this.eventDate.toggle() } catch (error) { } }
    if (type === 1) { try { this.eventDate1.toggle() } catch (error) { } }
    if (type === 2) { try { this.eventDate2.toggle() } catch (error) { } }
  }



  setTransferStop(_toggle) {
    setTimeout(() => {
      if (this.toDate) {
        if (_toggle) {
          this.eventDate.toggle()
        }
        let _duration = 0
        try {
          const { fromDate, toDate } = this
          _duration = moment(this.getDatefromObjItr(toDate)).diff(moment(this.getDatefromObjItr(fromDate)), 'days')
          if (_duration > 0) {
            const _incDay = _duration + 1
            this.durationDdl = [_duration, _incDay]
          }
          console.log(_duration)
          this.selectionDuration = _duration
          // if (_duration <= 3) {
          //   this.selectedStops = 'direct'
          // }
          this.trans1MinDate = { day: fromDate.day, month: fromDate.month, year: fromDate.year, }
          this.trans1MaxDate = { day: toDate.day, month: toDate.month, year: toDate.year, }

          this.trans2MinDate = { day: fromDate.day, month: fromDate.month, year: fromDate.year, }
          this.trans2MaxDate = { day: toDate.day, month: toDate.month, year: toDate.year, }
        } catch (error) { console.log(error) }
      }
    }, 0)
  }
  onDateSelection(date: NgbDateStruct) {
    let parsed = ""
    const _minDate = new Date(this.minDate.year, this.minDate.month - 1, this.minDate.day)
    const selectedDate = new Date(date.year, date.month - 1, date.day)
    if (!this.fromDate && !this.toDate) {
      if (moment(selectedDate).isSameOrAfter(_minDate)) {
        this.fromDate = date
        this.toDate = null
      } else {
        this.fromDate = null
        return
      }
    } else if (this.fromDate && !this.toDate && moment(selectedDate).isSameOrAfter(_minDate)) {
      const _fromDate = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day)
      if (moment(selectedDate).isSameOrAfter(_fromDate)) {
        this.toDate = date
      }
    } else {
      const selectedDate = new Date(date.year, date.month - 1, date.day)
      if (moment(selectedDate).isSameOrAfter(_minDate)) {
        this.toDate = null
        this.fromDate = date
      } else {
        this.fromDate = null
        return
      }
    }
    if (this.fromDate) { parsed += this._parserFormatter.format(this.fromDate) }
    if (this.toDate) { parsed += " - " + this._parserFormatter.format(this.toDate) }
    setTimeout(() => { this.calenderField.nativeElement.value = parsed }, 10)
    this.setTransferStop(true)
  }
  onDateSelection1(date: NgbDateStruct) {
    let parsed = ""
    const _minDate = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day)
    const _maxDate = new Date(this.toDate.year, this.toDate.month - 1, this.toDate.day)
    const selectedDate = new Date(date.year, date.month - 1, date.day)
    if (!this.fromDate1 && !this.toDate1) {
      if (moment(selectedDate).isSameOrAfter(_minDate) && moment(selectedDate).isSameOrBefore(_maxDate)) {
        this.fromDate1 = date
        this.toDate1 = null
      } else {
        this.fromDate1 = null
        return
      }
    } else if (this.fromDate1 && !this.toDate1 && moment(selectedDate).isSameOrAfter(_minDate)) {
      const _fromDate1 = new Date(this.fromDate1.year, this.fromDate1.month - 1, this.fromDate1.day)
      if (moment(selectedDate).isSameOrAfter(_fromDate1) && moment(selectedDate).isSameOrBefore(_maxDate)) {
        this.toDate1 = date
      }
    } else {
      const selectedDate = new Date(date.year, date.month - 1, date.day)
      if (moment(selectedDate).isSameOrAfter(_minDate) && moment(selectedDate).isSameOrBefore(_maxDate)) {
        this.toDate1 = null
        this.fromDate1 = date
      } else {
        this.fromDate1 = null
        return
      }
    }
    if (this.fromDate1) { parsed += this._parserFormatter.format(this.fromDate1) }
    if (this.toDate1) { parsed += " - " + this._parserFormatter.format(this.toDate1) }
    setTimeout(() => {
      this.calenderField1.nativeElement.value = parsed
      if (this.toDate1)
        this.eventDate1.toggle()
    }, 0)
  }

  onDateSelection2(date: NgbDateStruct) {
    let parsed = ""
    const _minDate = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day)
    const _maxDate = new Date(this.toDate.year, this.toDate.month - 1, this.toDate.day)
    const selectedDate = new Date(date.year, date.month - 1, date.day)
    if (!this.fromDate2 && !this.toDate2) {
      if (moment(selectedDate).isSameOrAfter(_minDate) && moment(selectedDate).isSameOrBefore(_maxDate)) {
        this.fromDate2 = date
        this.toDate2 = null
      } else {
        this.fromDate2 = null
        return
      }
    } else if (this.fromDate2 && !this.toDate2 && moment(selectedDate).isSameOrAfter(_minDate)) {
      const _fromDate2 = new Date(this.fromDate2.year, this.fromDate2.month - 1, this.fromDate2.day)
      if (moment(selectedDate).isSameOrAfter(_fromDate2) && moment(selectedDate).isSameOrBefore(_maxDate)) {
        this.toDate2 = date
      }
    } else {
      const selectedDate = new Date(date.year, date.month - 1, date.day)
      if (moment(selectedDate).isSameOrAfter(_minDate) && moment(selectedDate).isSameOrBefore(_maxDate)) {
        this.toDate2 = null
        this.fromDate2 = date
      } else {
        this.fromDate2 = null
        return
      }
    }
    if (this.fromDate2) { parsed += this._parserFormatter.format(this.fromDate2) }
    if (this.toDate2) { parsed += " - " + this._parserFormatter.format(this.toDate2) }
    setTimeout(() => {
      this.calenderField2.nativeElement.value = parsed
      if (this.toDate2)
        this.eventDate2.toggle()
    }, 0)
  }

  shippings = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => (!term || term.length < 3) ? []
        : this.ports.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1 || v.shortName.toLowerCase().indexOf(term.toLowerCase()) > -1 || v.code.toLowerCase().indexOf(term.toLowerCase()) > -1))
    )
  shippingFormatter = (x: { title: string }) => x.title

  async getBookingStatuses(selectedShippingModeCode) {
    const toSend: string = (selectedShippingModeCode === 'WAREHOUSE') ? 'WAREHOUSE' : 'BOOKING'
    try {
      const res = await this._userService.getBookingStatuses(toSend).toPromise() as JsonResponse
      if (res.returnId > 0) {
        const { returnObject } = res
        this.bookingStatuses = returnObject.filter(status => status.BusinessLogic.toUpperCase() !== 'IN-TRANSIT' && status.BusinessLogic.toUpperCase() !== 'COMPLETED')
      }
    } catch (error) { }
  }

  async getShippingStatus() {
    try {
      const res = await this._userService.getBookingStatuses('SHIPMENT').toPromise() as JsonResponse
      if (res.returnId > 0) {
        this.shippingStatuses = res.returnObject
        const _pending = { BusinessLogic: "PENDING", StatusCode: "PENDING", StatusID: -1, StatusName: "Pending", }
        this.shippingStatuses.push(_pending)
      }
    } catch (error) { }
  }

  isOriginDisabled = false
  isDestinationDisabled = false

  async setPortsData(selectedShippingModeCode) {
    const _ports: Array<any> = (localStorage.getItem("shippingPortDetails")) ? JSON.parse(localStorage.getItem("shippingPortDetails")) : []
    const ports_to_get = (selectedShippingModeCode.toLowerCase() === 'truck') ? 'sea' : selectedShippingModeCode.toLowerCase()
    const filteredPorts: Array<any> = _ports.filter(port => port.type.toLowerCase().includes(ports_to_get))
    const hasMode: boolean = filteredPorts.length > 0 ? true : false
    if (hasMode) {
      this.ports = filteredPorts
    } else {
      try {
        loading(true)
        const res = await this._userService.getPortsData(ports_to_get.toUpperCase()).toPromise() as any
        loading(false)
        const _newPorts = _ports.concat(res)
        const _filteredPorts: Array<any> = _newPorts.filter(port => port.type.toLowerCase().includes(ports_to_get))
        this.ports = _filteredPorts
        localStorage.setItem("shippingPortDetails", JSON.stringify(_newPorts))
      } catch (error) {
        loading(false)
      }
    }
    const { ports } = this
    console.log(this.ports)
    const _filterOrigin = ports.filter(_port => _port.id === this.PolID)
    console.log(_filterOrigin)
    if (_filterOrigin && _filterOrigin.length > 0) {
      this.filterOrigin = _filterOrigin[0]
      this.isOriginDisabled = true
    } else if (this.bookingDetails.BookingRouteDetail) {
      const { BookingRouteDetail } = this.bookingDetails
      const filteredPort = BookingRouteDetail.filter(_port => _port.PolType === 'SEA' && _port.PodType === 'SEA')
      if (filteredPort && filteredPort.length > 0) {
        const portObject = {
          code: filteredPort[0].PolCode,
          desc: filteredPort[0].PolCode.split(' ')[0],
          id: filteredPort[0].PolID,
          imageName: filteredPort[0].PolCode.split(' ')[0],
          shortName: filteredPort[0].PolName,
          title: `${filteredPort[0].PolCode}, ${filteredPort[0].PolName}`,
          type: "SEA"
        }
        this.filterOrigin = portObject
        this.isOriginDisabled = true
        console.log(this.filterOrigin)
      }
    }
    const _filterDestination = ports.filter(_port => _port.id === this.PodID)
    if (_filterDestination && _filterDestination.length > 0) {
      this.filterDestination = _filterDestination[0]
      this.isDestinationDisabled = true
    } else if (this.bookingDetails.BookingRouteDetail) {
      const { BookingRouteDetail } = this.bookingDetails
      const filteredPort = BookingRouteDetail.filter(_port => _port.PolType === 'SEA' && _port.PodType === 'SEA')
      if (filteredPort && filteredPort.length > 0) {
        const portObject = {
          code: filteredPort[0].PodCode,
          desc: filteredPort[0].PodCode.split(' ')[0],
          id: filteredPort[0].PodID,
          imageName: filteredPort[0].PodCode.split(' ')[0],
          shortName: filteredPort[0].PodName,
          title: `${filteredPort[0].PodCode}, ${filteredPort[0].PodName}`,
          type: "SEA"
        }
        this.filterDestination = portObject
        this.isDestinationDisabled = true
      }
    }
  }

  getDatefromObjItr(dateObject) {
    let toSend = null
    try {
      toSend = new Date(dateObject.year, dateObject.month - 1, dateObject.day)
    } catch (error) { }
    return toSend
  }

  getDatefromObj(dateObject) {
    let toSend = null
    try {
      toSend = moment(new Date(dateObject.year, dateObject.month - 1, dateObject.day)).format('YYYY-MM-DD')
    } catch (error) { }
    return toSend
  }

  isEmpty(s) {
    return !s.length;
  }

  isBlank(s) {
    return this.isEmpty(s.trim());
  }

  onSaveSchedule($action: string) {
    try {
      loading(true)
      let _fromDate, _toDate
      let _fromDate1, _toDate1
      let _fromDate2, _toDate2

      _fromDate = this.getDatefromObj(this.fromDate)
      _toDate = this.getDatefromObj(this.toDate)

      _fromDate1 = this.getDatefromObj(this.fromDate1)
      _toDate1 = this.getDatefromObj(this.toDate1)

      _fromDate2 = this.getDatefromObj(this.fromDate2)
      _toDate2 = this.getDatefromObj(this.toDate2)

      let _duration = 0
      try {
        _duration = moment(_toDate).diff(moment(_fromDate), 'days')
        if (_duration < 0) {
          this._toastr.warning('To Date must be greater than from date')
          loading(false)
          return
        }
      } catch (error) { }

      if (_fromDate && !_toDate) { _toDate = _fromDate }

      const uponBooking = this.from === 'BOOKING_LIST' ? 'Upon Booking' : null
      const toSend = {
        carrierCode: (this.selectedCarrier && this.selectedCarrier.code) ? this.selectedCarrier.code : null,
        carrierName: (this.selectedCarrier && this.selectedCarrier.title) ? this.selectedCarrier.title : null,
        transhipments: this.selectedStops,

        vesselCode: this.vesselCode ? this.vesselCode : null,
        vesselName: this.vesselName ? this.vesselName : null,
        voyage: this.voyageCode ? this.voyageCode : null,

        startPortCode: (this.filterOrigin && this.filterOrigin.code) ? this.filterOrigin.code.replace(' ', '') : null,
        startPortName: (this.filterOrigin && this.filterOrigin.title) ? this.filterOrigin.title : null,
        endPortCode: (this.filterDestination && this.filterDestination.code) ? this.filterDestination.code.replace(' ', '') : null,
        endPortName: (this.filterDestination && this.filterDestination.title) ? this.filterDestination.title : null,
        startDate: _fromDate,
        endDate: _toDate,

        tsVesselCode1: this.getTransferStop() >= 1 ? ((this.vesselCode) ? this.vesselCode : null) : null,
        tsVesselName1: this.getTransferStop() >= 1 ? ((this.vesselName) ? this.vesselName : null) : null,
        tsVoyage1: this.getTransferStop() >= 1 ? ((this.voyageCode) ? this.voyageCode : uponBooking) : null,

        tsPortCode1: this.getTransferStop() >= 1 ? ((this.transPort1 && this.transPort1.code) ? this.transPort1.code.replace(' ', '') : null) : null,
        tsPortName1: this.getTransferStop() >= 1 ? ((this.transPort1 && this.transPort1.title) ? this.transPort1.title : null) : null,
        tsETA1: this.getTransferStop() >= 1 ? (_fromDate1) : null,
        tsETD1: this.getTransferStop() >= 1 ? (_toDate1) : null,

        tsVesselCode2: this.getTransferStop() === 2 ? (this.vesselCode) : null,
        tsVesselName2: this.getTransferStop() === 2 ? (this.vesselName) : null,
        tsVoyage2: this.getTransferStop() === 2 ? ((this.voyageCode) ? this.voyageCode : uponBooking) : null,

        tsPortCode2: this.getTransferStop() === 2 ? ((this.transPort2 && this.transPort2.code) ? this.transPort2.code.replace(' ', '') : null) : null,
        tsPortName2: this.getTransferStop() === 2 ? ((this.transPort2 && this.transPort2.title) ? this.transPort2.title : null) : null,
        tsETA2: this.getTransferStop() === 2 ? (_fromDate2) : null,
        tsETD2: this.getTransferStop() === 2 ? (_toDate2) : null,
        duration: this.selectionDuration,
      }

      const { carrierCode, transhipments, vesselCode, vesselName, voyage, startPortCode, endPortCode, startDate, endDate, } = toSend

      let isObjectValid = true

      if (!carrierCode) {
        this._toastr.warning('Please enter a Carrier')
        loading(false)
        isObjectValid = false
      }

      if (!startDate) {
        this._toastr.warning('Please select Start Date')
        loading(false)
        isObjectValid = false
      }
      if (!endDate) {
        this._toastr.warning('Please select End Date')
        loading(false)
        isObjectValid = false
      }

      if (!transhipments) {
        this._toastr.warning('Please select number of stops')
        loading(false)
        isObjectValid = false
      }

      if (!vesselCode || this.isBlank(vesselCode)) {
        this._toastr.warning('Please enter Vessel Code')
        loading(false)
        isObjectValid = false
      } else if (this.from === 'VIEW_BOOKING' && vesselCode && vesselCode.toLowerCase() === 'upon booking') {
        this._toastr.warning('Please enter Valid Vessel Code', 'Upon Booking Not Allowed')
        loading(false)
        isObjectValid = false
        return
      }

      if (!vesselName || this.isBlank(vesselName)) {
        this._toastr.warning('Please enter Vessel Name')
        loading(false)
        isObjectValid = false
      } else if (this.from === 'VIEW_BOOKING' && vesselName && vesselName.toLowerCase() === 'upon booking') {
        this._toastr.warning('Please enter Valid Vessel Name', 'Upon Booking Not Allowed')
        loading(false)
        isObjectValid = false
        return
      }

      if (!voyage || this.isBlank(voyage)) {
        this._toastr.warning('Please enter Voyage')
        loading(false)
        isObjectValid = false
      } else if (this.from === 'VIEW_BOOKING' && voyage && voyage.toLowerCase() === 'upon booking') {
        this._toastr.warning('Please enter Valid Voyage Number', 'Upon Booking Not Allowed')
        loading(false)
        isObjectValid = false
        return
      }

      if (!startPortCode) {
        this._toastr.warning('Please select Start Port')
        loading(false)
        isObjectValid = false
      }
      if (!endPortCode) {
        this._toastr.warning('Please select End Port')
        loading(false)
        isObjectValid = false
      }

      if (transhipments !== 'direct') {
        const { tsPortCode1, tsPortName1, tsETD1, tsETA1, } = toSend
        if (this.transPort1 && (this.transPort1.id === this.filterOrigin.id || this.transPort1.id === this.filterDestination.id)) {
          this._toastr.warning('Trans Shipment route 1 cannot be same as Origin/Destination')
          loading(false)
          isObjectValid = false
        }
        if (!tsPortCode1) {
          this._toastr.warning('Please select tsPortCode1')
          loading(false)
          isObjectValid = false
        }
        if (!tsPortName1) {
          this._toastr.warning('Please select tsPortName1')
          loading(false)
          isObjectValid = false
        }
      }

      if (transhipments === '2 t/s') {
        const { tsPortCode2, tsPortName2, tsETD2, tsETA2, } = toSend

        if (this.transPort2 && (this.transPort2.id === this.filterOrigin.id || this.transPort2.id === this.filterDestination.id)) {
          this._toastr.warning('Trans Shipment route 2 cannot be same as Origin/Destination')
          loading(false)
          isObjectValid = false
        }
        if (!tsPortCode2) {
          this._toastr.warning('Please select tsPortCode2')
          loading(false)
          isObjectValid = false
        }
        if (!tsPortName2) {
          this._toastr.warning('Please select tsPortName2')
          loading(false)
          isObjectValid = false
        }
      }


      if (transhipments === '1 t/s') {
        const { tsPortCode1, tsPortName1, tsETD1, tsETA1, } = toSend

        if (this.transPort1 && (this.transPort1.id === this.filterOrigin.id || this.transPort1.id === this.filterDestination.id)) {
          this._toastr.warning('Trans Shipment route 1 cannot be same as Origin/Destination')
          loading(false)
          isObjectValid = false
        }
        if (!tsPortCode1) {
          this._toastr.warning('Please select tsPortCode1')
          loading(false)
          isObjectValid = false
        }
        if (!tsPortName1) {
          this._toastr.warning('Please select tsPortName1')
          loading(false)
          isObjectValid = false
        }
      }
      if (isObjectValid) {
        if (this.from && this.from === 'BOOKING_LIST')
          this.saveSchedulesForRequest([toSend])
        else
          this.saveSchedules([toSend])
      } else {
        loading(false)
      }
    } catch (error) {
      console.log(error)
    }
  }

  saveSchedulesForRequest(_schedules) {
    this.isProcessing = true
    this._userService.postScheduleRequest(_schedules).subscribe((res: JsonResponse) => {
      loading(false)
      const { returnText, returnId } = res
      if (returnId > 0) {
        this.forBookingListAciton(res)
      } else {
        this.isProcessing = false
        this._toastr.error(returnText, 'Failed')
      }
    }, err => {
      this.isProcessing = false
      loading(false)
      console.log(err)
      this._toastr.error('Please try again laters', 'Failed')
    })
  }

  saveSchedules(_schedules) {
    this.isProcessing = true
    this._userService.postSchedule(_schedules).subscribe((res: JsonResponse) => {
      loading(false)
      const { returnText, returnId } = res
      if (returnId > 0) {
        this._toastr.success(returnText, 'Success')
        this.isProcessing = false
        this._activeModal.close(res)
      } else {
        this.isProcessing = false
        this._toastr.error(returnText, 'Failed')
      }
    }, err => {
      this.isProcessing = false
      loading(false)
      console.log(err)
      this._toastr.error('Please try again laters', 'Failed')
    })
  }

  forBookingListAciton(res: JsonResponse) {
    try {
      const _addedSchedule: IScheduleResponse = JSON.parse(res.returnObject)
      const _schedule = {
        ..._addedSchedule,
        bookingID: this.bookingDetails.BookingID,
        transfersStop: this.getTransferStop(),
        portCutOffDate: this.getCutOffDate(),
      }
      this._activeModal.close(_schedule)
    } catch (error) {
      this.isProcessing = false
      console.log(error)
    }
  }

  getCutOffDate() {
    try {
      const { year, month, day } = this.fromDate
      return moment(new Date(year, month - 1, day)).subtract(3, "days").format('YYYY-MM-DD')
    } catch (err) {
      console.log(err)
      return null
    }
  }

  getTransferStop() {
    switch (this.selectedStops) {
      case 'direct':
        return 0;
      case '1 t/s':
        return 1;
      case '2 t/s':
        return 2;
      default:
        return 0;
    }
  }

  getCarrierImage($image: string) {
    return getImagePath(ImageSource.FROM_SERVER, $image, ImageRequiredSize.original)
  }

  getProviderImage($image) {
    return getImagePath(ImageSource.FROM_SERVER, getProviderImage($image), ImageRequiredSize.original)
  }

  closeModal(status) {
    this._activeModal.close(status)
    document.getElementsByTagName('html')[0].style.overflowY = 'auto'
  }

  closePopup(val?) {
    this.closeModal(null)
  }

  ngOnDestroy() { }
}

const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
  one &&
  two &&
  two.year === one.year &&
  two.month === one.month &&
  two.day === one.day

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two
    ? false
    : one.year === two.year
      ? one.month === two.month
        ? one.day === two.day
          ? false
          : one.day < two.day
        : one.month < two.month
      : one.year < two.year

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two
    ? false
    : one.year === two.year
      ? one.month === two.month
        ? one.day === two.day
          ? false
          : one.day > two.day
        : one.month > two.month
      : one.year > two.year

export interface IJsonTransferVesselsDetails {
  RouteCode1: string;
  RouteName1?: any;
  R1PolCode: string;
  R1PodCode: string;
  R1EtdDate: Date;
  R1EtaDate: Date;
  R1VesselName: string;
  R1VesselNo: string;
  R1VoyageRefNo: string;
  RouteCode2?: any;
  RouteName2?: any;
  R2PolCode: string;
  R2PodCode: string;
  R2EtdDate: Date;
  R2EtaDate: Date;
  R2VesselName: string;
  R2VesselNo: string;
  R2VoyageRefNo: string;
  RouteCode3?: any;
  RouteName3?: any;
  R3PolCode: string;
  R3PodCode: string;
  R3EtdDate: Date;
  R3EtaDate: Date;
  R3VesselName: string;
  R3VesselNo: string;
  R3VoyageRefNo: string;
  RouteCode4?: any;
  RouteName4?: any;
  R4PolCode?: any;
  R4PodCode?: any;
  R4EtdDate?: any;
  R4EtaDate?: any;
  R4VesselName?: any;
  R4VesselNo?: any;
  R4VoyageRefNo?: any;
  RouteCode5?: any;
  RouteName5?: any;
  R5PolCode?: any;
  R5PodCode?: any;
  R5EtdDate?: any;
  R5EtaDate?: any;
  R5VesselName?: any;
  R5VesselNo?: any;
  R5VoyageRefNo?: any;
}