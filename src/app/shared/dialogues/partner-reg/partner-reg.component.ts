import { Component, OnInit, ViewEncapsulation, Input, Renderer2, ElementRef } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpErrorResponse } from '@angular/common/http';
import { LoginDialogComponent } from '../login-dialog/login-dialog.component';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { EMAIL_REGEX, CustomValidator, ValidateEmail, getLoggedUserData, validateName } from '../../../constants/globalFunctions';
import { Observable } from 'rxjs';
import { CountryDropdown } from '../../country-select/country-select.component';
import { JsonResponse } from '../../../interfaces/JsonResponse';
import { of } from 'rxjs/observable/of';
import { CommonService } from '../../../services/common.service';
import { SharedService } from '../../../services/shared.service';
import { UserCreationService } from '../../../components/pages/user-creation/user-creation.service';
import { Partners } from '../../../components/pages/user-desk/partner/partner.component';




@Component({
  selector: 'app-partner-reg',
  templateUrl: './partner-reg.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./partner-reg.component.scss']
})



export class PartnerRegComponent implements OnInit {

  @Input() shareUserObject: any;
  @Input() isPartner: boolean = false;
  @Input() partnerData: Partners;
  closeResult: string;
  currentJustify = 'justified';
  public creationCorporateType;
  public countryList;
  public toggle: boolean = false;
  public country;
  public cityList;
  public cityValidation;
  public cityValid;
  public companyList;
  public hearList;
  public requiredFields = "This field is required!";
  public selectedImage;
  public select;
  public countryPCode;
  public phoneCountryId;
  public colorEye;
  public term: boolean = true;

  public nameError;
  public codeError;
  public contactError;
  public cityError;
  public emailError;
  public companyError;
  public addressError;

  public corporateUser: boolean = false;
  public company: any;

  public city: any = {
    title: '',
    // imageName: this.getUserLocation(),
    imageName: 'US',
    desc: '',
    code: ''
  }
  isValidFormSubmitted = null;
  userForm;
  public hideLogin: boolean = false;
  public activeIdString: string = "tab-corporate"

  public isSubmitting: boolean = false

  showClose = true

  constructor(
    private modalService: NgbModal,
    private _renderer: Renderer2,
    private el: ElementRef,
    private dropdownservice: CommonService,
    private authService: UserCreationService,
    private _toast: ToastrService,
    private activeModal: NgbActiveModal,
    private _sharedService: SharedService,
    private _location: PlatformLocation
  ) {
    _location.onPopState(() => this.closeModal(false));
  }
  ngOnInit() {

    this.isSubmitting = false
    this.activeIdString = 'tab-corporate'
    this.Account('corporate')

    this.userForm = new FormGroup({
      code: new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(10)]),
      name: new FormControl(null, [Validators.required, Validators.pattern(/^[a-zA-Z\s]*$/), Validators.minLength(2), Validators.maxLength(50)]),
      city: new FormControl(null, [Validators.required, Validators.minLength(3), Validators.maxLength(25)]),
      email: new FormControl(null, [Validators.required, Validators.pattern(EMAIL_REGEX), Validators.maxLength(50)]),
      address: new FormControl(null, [Validators.minLength(2), Validators.maxLength(320)]),
      phone: new FormControl(null, [Validators.pattern(/^(?!(\d)\1+(?:\1+){0}$)\d+(\d+){0}$/), Validators.minLength(7), Validators.maxLength(12)]),
      company: new FormControl('', [Validators.required, CustomValidator.bind(this)]),
    });

    if (this.partnerData) {
      const { partnerData } = this
      this.authService.getPartnerDtl(partnerData.partnerID).subscribe((res: any) => {
        if (res && typeof res === 'object') {

          const _partner: IPartner = res
          this.getCountryList(_partner.phoneCodeCountryID)
          let _partnerCode = _partner.partnerCode
          let _partnerName = _partner.partnerCompanyName

          // if (partnerData.partnerCode) {
          //   _partnerCode = partnerData.partnerCode
          //   _partnerName = partnerData.partnerName.split(',')[1]
          // } else {
          //   _partnerCode = partnerData.partnerName.split(',')[0]
          //   _partnerName = partnerData.partnerName.split(',')[1]
          // }

          this.userForm = new FormGroup({
            code: new FormControl(_partnerCode, [Validators.required, Validators.minLength(2), Validators.maxLength(10)]),
            name: new FormControl(_partner.firstName, [Validators.required, Validators.pattern(/^[a-zA-Z\s]*$/), Validators.minLength(2), Validators.maxLength(50)]),
            city: new FormControl(null, [Validators.required, Validators.minLength(3), Validators.maxLength(25)]),
            email: new FormControl(_partner.primaryEmail, [Validators.required, Validators.pattern(EMAIL_REGEX), Validators.maxLength(50)]),
            address: new FormControl(_partner.partnerAddress, [Validators.minLength(2), Validators.maxLength(320)]),
            phone: new FormControl(_partner.primaryPhone, [Validators.pattern(/^(?!(\d)\1+(?:\1+){0}$)\d+(\d+){0}$/), Validators.minLength(7), Validators.maxLength(12)]),
            company: new FormControl(_partnerName, [Validators.required, CustomValidator.bind(this)]),
          });

          if (partnerData.cityName) {
            this.dropdownservice.getFilteredCity(partnerData.cityName).subscribe((city$: any) => {
              const _city: ICity = city$[0]
              this.userForm.controls['city'].setValue(_city)
            })
          }
        }
      })

    } else {
      this.getCountryList(null)
    }

    this.Account("corporate");
    this.creationCorporateType = true;
    let tab = this.el.nativeElement.querySelector('ul[role="tablist"]');
    this._renderer.setStyle(tab, 'display', 'none');
  }

  getCountryList(_countryId) {
    this.dropdownservice.getCountry().subscribe((res: Array<CountryDropdown>) => {
      let List = res;
      List.map((obj) => {
        obj.desc = JSON.parse(obj.desc);
      })
      this.countryList = List;
      if (_countryId) {
        const _country = List.filter(_countr => _countr.id === _countryId)[0]
        if (_country) {
          this.selCountry = _country
        }
      }
    }, (err: HttpErrorResponse) => {
    })
  }


  getUserLocation() {
    try {
      let countryCode = this._sharedService.getMapLocation().countryCode;
      this.city = this.findCityByCountryCode(countryCode);
      this.flag(this.city, 'city');
    } catch (error) {

    }
  }

  findCityByCountryCode(countryCode: string): SelectedCity {

    let searchedCity: SelectedCity;
    this.cityList.find((obj) => {

      if (JSON.parse(obj.desc)[0].CountryCode.toLowerCase() == countryCode.toLowerCase()) {
        searchedCity = {
          desc: obj.desc,
          title: obj.title,
          code: obj.imageName,
          imageName: obj.imageName,
        }
        return obj;
      }
    });
    return searchedCity;
  }

  errorMessages() {
    if (this.userForm.controls.name.status == "INVALID" && this.userForm.controls.name.touched) {
      this.nameError = true;
    }
    if (this.userForm.controls.code.status == "INVALID" && this.userForm.controls.code.touched) {
      this.codeError = true;
    }
    if (this.userForm.controls.phone.status == "INVALID" && this.userForm.controls.phone.touched) {
      this.contactError = true;
    }
    if (this.userForm.controls.email.status == "INVALID" && this.userForm.controls.email.touched) {
      this.emailError = true;
    }
    if (this.userForm.controls.company.status == "INVALID" && this.userForm.controls.company.touched) {
      this.companyError = true;
    }
    if (this.userForm.controls.address.status == "INVALID" && this.userForm.controls.address.touched) {
      this.addressError = true;
    }


  }


  login() {
    this.activeModal.close();

    this.modalService.open(LoginDialogComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'login-modal',
      backdrop: 'static',
      keyboard: false
    });
    // setTimeout(() => {
    //   if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
    //     document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
    //   }
    // }, 0);

  }
  closeModal(status) {
    this.activeModal.close(status);
    document.getElementsByTagName('html')[0].style.overflowY = 'auto';

  }

  selCountry

  flag(list, type) {
    this.cityValidation = false;
    this.cityValid = false;
    if (typeof (list) == 'object' && list.title && type == 'city') {
      this.selectedImage = list.imageName;
      let description = JSON.parse(list.desc);
      if (this.countryList && this.countryList.length) {
        const objCountry = this.countryList.find(item => item.id === description[0].CountryID);
        this.selCountry = objCountry
        this.select = objCountry
        this.countryPCode = this.select.desc[0].CountryPhoneCode;
        this.phoneCountryId = this.select.id;
      }
    }
    else if (typeof (list) == 'object' && list.title && type == 'country') {
      this.selectedImage = list.code;
      let description = list.desc;
      this.countryPCode = description[0].CountryPhoneCode;
      this.phoneCountryId = list.id
      this.selCountry = list
    }
  }

  spaceHandler(event) {
    if (event.target.value) {
      const end = event.target.selectionEnd;
      if (event.keyCode == 32 && (event.target.value[end - 1] == " " || event.target.value[end] == " ")) {
        event.preventDefault();
        return false;
      }
    }
  }
  emailPassSpaceHandler(event) {
    if (event.keyCode == 32) {
      event.preventDefault();
      return false;
    }
  }
  checkCity(type) {

    let cityName = this.userForm.value.city;
    if (type == 'focusOut') {
      if (typeof (cityName) == "string" && cityName.length > 2 && cityName.length < 26) {
        this.cityValidation = "City should be selected from the dropdown";
        this.cityValid = false;
      }
      if (this.userForm.controls.city.status == "INVALID") {
        this.cityError = true;
      }
    }
    else if (type == 'focus' && this.cityValidation && typeof (cityName) == "string") {

      this.cityValidation = false;
      this.cityValid = true;
    }
    setTimeout(() => {
      this.isCitySearching = false
    }, 0);
  }
  Account(accountType) {
    if (accountType == "corporate") {
      this.corporateUser = true;
    }
    else if (accountType == "individual") {
      this.corporateUser = false;
      this.userForm.controls.company.reset();
    }
  }

  async partnerAddEditAction(userForm) {
    let userItem = getLoggedUserData();
    this.isSubmitting = true
    let valid: boolean = ValidateEmail(userForm.email);

    if (this.userForm.invalid) {
      this.isSubmitting = false
      return;
    }
    else if (!valid) {
      this.isSubmitting = false
      this._toast.warning('Invalid email entered.', 'Failed')
      return
    }

    if ((userForm.company && typeof userForm.company === 'object' && !userItem) || (userForm.company && typeof userForm.company === 'object' && userItem && userItem.IsLogedOut)) {
      try {
        const res: JsonResponse = await this.authService.getCompanyAdmin(userForm.company.id).toPromise() as any
        if (res.returnId > 0) {
          console.log(res);
          // res.returnObject
          const { adminName, adminEmail, adminContact } = res.returnObject
          this._toast.warning(`Please contact ${userForm.company.title}’s admin - ${adminName} at ${adminEmail} to create your account within ${userForm.company.title}`)
        } else {
          this._toast.warning('Please contact ' + userForm.company.title + ' admin to create your account', 'Info')
        }
      } catch (error) {
        this._toast.warning('Please contact ' + userForm.company.title + ' admin to create your account', 'Info')
      }
      this.isSubmitting = false
      return false;
    }

    const { name, code } = userForm
    const fName = validateName(name)
    const lName = validateName(code)
    let country_id = JSON.parse(userForm.city.desc);
    let _sourceId = userItem.ProviderID
    let _sourceType = 'PROVIDER_SIGNUP'

    const obj = {
      partnerID: (this.partnerData && this.partnerData.partnerID) ? this.partnerData.partnerID : -1,
      providerID: getLoggedUserData().ProviderID,
      cityID: userForm.city.id,
      countryID: Number(country_id[0].CountryID),
      countryPhoneCode: this.countryPCode,
      phoneCodeCountryID: this.phoneCountryId,
      firstName: fName,
      lastName: "",
      modifiedBy: (this.partnerData) ? getLoggedUserData().UserID : "",
      partnerCode: userForm.code,
      partnerCompanyName: userForm.company,
      primaryEmail: userForm.email,
      primaryPhone: userForm.phone,
      partnerAddress: userForm.address,
      createdBy: (!this.partnerData) ? getLoggedUserData().UserID : "",
      portalName: 'PROVIDER',
      sourceID: _sourceId,
      sourceType: _sourceType
    }

    let _request

    if (this.partnerData) {
      _request = this.authService.editPartner(obj, getLoggedUserData().UserID)
    } else {
      _request = this.authService.addPartner(obj, getLoggedUserData().UserID)
    }

    _request.subscribe((res: any) => {

      if (res.returnId > 0) {
        this._toast.success(res.returnText);
        this.userForm.reset();
        this.closeModal(true);
      } else {
        this._toast.error(res.returnText);
      }
      this.isSubmitting = false
    }, (err: HttpErrorResponse) => {
      this.isSubmitting = false
    })
  }
  NumberValid(evt) {
    let charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 37 && charCode != 39 && charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }

  confirmPassword(event) {
    let element = event.target.nextSibling;
    if (element.type === "password" && element.value) {
      element.type = "text";
      this.colorEye = "black";
    }
    else {
      element.type = "password";
      this.colorEye = "grey";

    };
  }


  textValidation(event) {
    try {
      const pattern = /[a-zA-Z-][a-zA-Z -]*$/;
      const inputChar = String.fromCharCode(event.charCode);
      if (!pattern.test(inputChar)) {

        if (event.charCode == 0) {
          return true;
        }

        if (event.target.value) {
          const end = event.target.selectionEnd;
          if ((event.which == 32 || event.keyCode == 32) && (event.target.value[end - 1] == " " || event.target.value[end] == " ")) {
            event.preventDefault();
            return false;
          }
        }
        else {
          event.preventDefault();
          return false;
        }
      } else {
        return true;
      }
    } catch (error) {
      return false
    }

  }



  search = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .map(term => (!term || term.length < 3) ? []
        : this.cityList.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1));
  formatter = (x: { title: string, desc: string, imageName: string }) => {
    this.city.imageName = x.imageName;
    this.city.title = x.title;
    this.city.desc = x.desc;
    this.isCitySearching = false
    return x.title;

  };

  isCitySearching: boolean = false
  hasCitySearchFailed: boolean = false
  hasCitySearchSuccess: boolean = false

  search2 = (text$: Observable<string>) =>
    text$
      .debounceTime(300) //debounce time
      .distinctUntilChanged()
      .do(() => { this.isCitySearching = true; this.hasCitySearchFailed = false; this.hasCitySearchSuccess = false; }) // do any action while the user is typing
      .mergeMap(term => {
        let some = of([]); //  Initialize the object to return
        try {
          if (term && term.length >= 3) { //search only if item are more than three
            some = this.dropdownservice.getFilteredCity(term)
              .do((res) => { this.isCitySearching = false; this.hasCitySearchSuccess = true; return res; })
              .catch(() => { this.isCitySearching = false; this.hasCitySearchFailed = true; return []; })
          } else { this.isCitySearching = false; some = of([]); }
        } catch (error) {
          this.isCitySearching = false
        }
        return some;
      })
      .do((res) => { this.isCitySearching = false; return res; })
      .catch(() => { this.isCitySearching = false; return of([]); }); // final server list

  onCitySelect({ target }, city) {
    if (target.value === "") {

      // this.city = {};
    }
  }

  companyPredic = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .map(term => (!term || term.length < 3) ? []
        : this.companyList.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1));
  companyFormater = (x: { title: string, desc: string, imageName: string }) => {
    return x.title;

  };

}

interface SelectedCity {
  title?: String,
  imageName?: String,
  desc?: any;
  code?: string
}

export interface IPartner {
  partnerID: number;
  partnerCode: string;
  firstName: string;
  middleName?: any;
  lastName: string;
  partnerCompanyName: string;
  partnerImage?: any;
  primaryEmail: string;
  secondaryEmail?: any;
  primaryPhone: string;
  secondaryPhone?: any;
  companyID?: any;
  regionID?: any;
  countryID: number;
  countryPhoneCode: string;
  phoneCodeCountryID: number;
  cityID: number;
  isDelete: boolean;
  isActive: boolean;
  createdBy: string;
  createdDateTime: string;
  modifiedBy: string;
  modifiedDateTime: string;
  partnerAddress: string
}

export interface ICity {
  id: number;
  code: string;
  title: string;
  shortName: string;
  imageName: string;
  desc: string;
  webURL?: any;
  sortingOrder?: any;
  type: string;
  lastUpdate?: any;
  latitude?: any;
  longitude?: any;
  isBaseCurrency?: any;
  jsonServices?: any;
  isChinaGateway: boolean;
  shortPortCode?: any;
  portCountryCode?: any;
  roundingOff?: any;
}