import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter, Renderer2 } from '@angular/core'
import { NgbActiveModal, NgbInputDatepicker, NgbDateStruct, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap'
import { ViewBookingService } from '../../../components/pages/user-desk/view-booking/view-booking.service'
import { ICalUnit, IReceiptItem, JsonResponse, ViewBookingDetails } from '../../../interfaces'
import { CountryDropdown } from '../../country-select/country-select.component'
import { NgbDate } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date'
import { getLoggedUserData, isNumber, isMobile } from '../../../constants/globalFunctions'
import { CommonService } from '../../../services/common.service'
import { SharedService } from '../../../services/shared.service'
import { PlatformLocation } from '@angular/common'
import { ToastrService } from "ngx-toastr"
import * as moment from 'moment'
import { Subject } from "rxjs"

const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
  one &&
  two &&
  two.year === one.year &&
  two.month === one.month &&
  two.day === one.day

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two
    ? false
    : one.year === two.year
      ? one.month === two.month
        ? one.day === two.day
          ? false
          : one.day < two.day
        : one.month < two.month
      : one.year < two.year

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two
    ? false
    : one.year === two.year
      ? one.month === two.month
        ? one.day === two.day
          ? false
          : one.day > two.day
        : one.month > two.month
      : one.year > two.year

@Component({
  selector: 'app-add-dispatch',
  templateUrl: './add-dispatch.component.html',
  styleUrls: ['./add-dispatch.component.scss']
})
export class AddDispatchComponent implements OnInit {
  @ViewChild("dp") input: NgbInputDatepicker
  @ViewChild("rangeDp") rangeDp: ElementRef

  // @Input() selectedData: any
  @Output() savedRow = new EventEmitter<any>()

  @ViewChild("container") container: ElementRef

  citiesResults: Object
  searchTerm$ = new Subject<string>()


  public allContainersType: any[] = []
  public allContainers: any[] = []
  public allHandlingType: any[] = []
  public allCustomers: any[] = []
  public allCurrencies: any[] = []
  public filterOrigin: any = {}
  public filterDestination: any = {}
  public userProfile: any
  public selectedCategory: any = null
  public selectedContSize: any = null
  public containerType: string = 'empty'
  public selectedHandlingUnit: any
  public selectedCustomer: any[] = []
  public selectedShipping: any

  isMobile = isMobile()

  public defaultCurrency: any = {
    id: 101,
    shortName: "USD",
    imageName: 'US'
  }
  public selectedCurrency: any = this.defaultCurrency
  public whCurrency: any = this.defaultCurrency
  public startDate: NgbDateStruct
  public maxDate: NgbDateStruct
  public minDate: NgbDateStruct
  public hoveredDate: NgbDateStruct
  public fromDate: any = {
    day: null,
    month: undefined,
    year: undefined
  }
  public toDate: any = {
    day: undefined,
    month: undefined,
    year: undefined
  }

  public model: any
  public disableWarehouse: boolean = false
  isHovered = date =>
    this.fromDate &&
    !this.toDate &&
    this.hoveredDate &&
    after(date, this.fromDate) &&
    before(date, this.hoveredDate)
  isInside = date => after(date, this.fromDate) && before(date, this.toDate)
  isFrom = date => equals(date, this.fromDate)
  isTo = date => equals(date, this.toDate)

  isRateUpdating = false

  vehicleNumer: string
  driveName: string
  driverCell: string
  driverPhoneCode: string

  fullQWeight: any
  fullQWeightUOM: number = 8

  emptyWeight: any
  emptyWeightUOM: number = 8

  totalWeight: any
  totalWeightUOM: number = 8

  numBags: any = 0
  numBagWeigth: any
  numBagWeigthUOM: number = 8

  bagTotalWeight: any
  bagTotalWeightUOM: number = 8

  countryList = []
  selCountry: CountryDropdown
  public countryFlagImage: string
  public transPhoneCode: string
  public activePhone: any
  public activeTransPhone: any
  public phoneCountryId: any
  public mobileCountryId: any
  public phoneCode
  public mobileCountFlagImage: string
  public mobileCode
  public transmobileCode
  pickupDate: NgbDate
  selectedDate

  @Input() bookingData: ViewBookingDetails
  @Input() mode: string
  @Input() recieptData: IReceiptItem
  @Input() whUnits: ICalUnit[] = []
  loading = false


  constructor(
    location: PlatformLocation,
    private _activeModal: NgbActiveModal,
    private _parserFormatter: NgbDateParserFormatter,
    private renderer: Renderer2,
    private _sharedService: SharedService,
    private dateParser: NgbDateParserFormatter,
    private _toastr: ToastrService,
    private _viewBookingService: ViewBookingService,
    private _commonService: CommonService
  ) {
    location.onPopState(() => this.closeModal(null))
    // config.autoClose = false
    // this.fromDate = calendar.getToday()
  }

  async ngOnInit() {
    try {
      const date = new Date()
      this.minDate = { month: date.getMonth() + 1, day: date.getDate(), year: date.getFullYear() }
    } catch (error) { }

    try {
      this.loading = true
      if (!this.whUnits && this.whUnits.length == 0) {
        if (localStorage.getItem('cal_units')) {
          const _calUnits = JSON.parse(localStorage.getItem('cal_units'))
          this.whUnits = _calUnits.filter(
            e => (e.UnitTypeNature === "WEIGHT" || e.UnitTypeNature === "WEIGHT_TON")
          )
        } else {
          const res = await this._viewBookingService.getCalcUnits().toPromise() as any
          if (res && res.returnId > -1) {
            const _calUnits = res.returnObject
            this.whUnits = _calUnits.filter(
              e => (e.UnitTypeNature === "WEIGHT" || e.UnitTypeNature === "WEIGHT_TON")
            )
            localStorage.setItem('cal_units', JSON.stringify(res.returnObject))
          }
        }
      }
      this.loading = false
    } catch (error) {
      this.loading = false
    }


    try {
      if (this.mode === 'edit') {
        const { recieptData } = this
        try {
          const _date = new Date(recieptData.receiptDate)
          this.pickupDate = { year: _date.getFullYear(), month: _date.getMonth() + 1, day: _date.getDate() } as any
          // console.log(this.pickupDate)
        } catch (error) {

        }

        this.vehicleNumer = recieptData.vehicleNo
        this.driveName = recieptData.driverName
        this.driverCell = recieptData.driverMobile

        this.fullQWeight = recieptData.loadedWeight
        this.fullQWeightUOM = recieptData.loadedWeightUOMID

        this.emptyWeight = recieptData.emptyWeight
        this.emptyWeightUOM = recieptData.emptyWeightUOMID

        this.totalWeight = recieptData.goodsWeight
        this.totalWeightUOM = recieptData.goodsWeightUOMID

        this.numBags = recieptData.receiptQty

        this.numBagWeigth = recieptData.eachQtyWeight
        this.numBagWeigthUOM = recieptData.eachQtyWeightUOMID

        this.bagTotalWeight = recieptData.totalQtyWeight
        this.bagTotalWeightUOM = recieptData.totalQtyWeightUOMID
        this.onBagWeightChange(this.numBagWeigth, this.numBagWeigthUOM, this.numBags)
        this.onWeightChange(this.fullQWeight, this.fullQWeightUOM, this.emptyWeight, this.emptyWeightUOM)
      }
    } catch (error) {

    }
    this._commonService.getCountry().subscribe((countryList: any) => {
      if (countryList && countryList.length) {
        countryList.map((obj) => {
          if (typeof (obj.desc) == "string") {
            obj.desc = JSON.parse(obj.desc)
          }
        })
        this._sharedService.countryList.next(countryList)
        this.setCountryListConfig(countryList)
      }
    })


    this._sharedService.countryList.subscribe((state: any) => {
      if (state) {
        this.setCountryListConfig(state)
      }
    })
  }

  setCountryListConfig(state) {
    this.countryList = state
    this.userProfile = getLoggedUserData()
    if (this.mode === 'add') {
      let selectedCountry = this.countryList.find(obj => obj.id == this.userProfile.CountryID)
      this.selectPhoneCode(selectedCountry)
    } else {
      let selectedCountry = this.countryList.find(obj => obj.code == this.recieptData.countryCode)
      this.selectPhoneCode(selectedCountry)
    }
  }



  selectPhoneCode(country) {
    this.countryFlagImage = country.code
    let description = country.desc
    this.phoneCode = description[0].CountryPhoneCode
    this.transPhoneCode = description[0].CountryPhoneCode_OtherLang
    this.phoneCountryId = country.id
    this.selCountry = country
    // console.log(this.selCountry)
  }


  numberValidwithDecimal(event) {
    try {
      const charCode = (event.which) ? event.which : event.keyCode
      const key = event.key
      let isTrue = true
      const currentValue: string = event.target.value
      // console.log(event)

      // console.log('key: *' + key + '*', 'value:*' + currentValue + '*')
      // console.log(key === '.', currentValue.includes('.'))
      if (
        (charCode != 45 || currentValue.toString().includes('-')) && // “-” CHECK MINUS, AND ONLY ONE.
        (charCode != 46 || currentValue.toString().includes('.')) && // “.” CHECK DOT, AND ONLY ONE.
        (charCode < 48 || charCode > 57) ||
        key === '-' ||
        (key === '.' && currentValue.toString().includes('.')) ||
        ((key === '.' || key === '-') && !currentValue)
      ) {
        event.preventDefault()
        isTrue = false
      }
      return isTrue
    } catch (error) {
      event.preventDefault()
      return false
    }
  }



  onDateSelection(date: NgbDateStruct) {
    // if (this.selectedData.mode === "publish" && this.selectedData.data) {
    //   this.onDateSelectionEdit(date)
    // } else {
    this.onDateSelectionNew(date)
    // }
  }

  onDateSelectionEdit(date: NgbDateStruct) {
    let parsed = ''
    const _minDate = new Date(this.minDate.year, this.minDate.month - 1, this.minDate.day)
    const selectedDate = new Date(date.year, date.month - 1, date.day)
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date
    } else if (this.fromDate && !this.toDate && moment(selectedDate).isSameOrAfter(_minDate)) {
      const _fromDate = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day)
      if (moment(selectedDate).isAfter(_fromDate)) {
        this.toDate = date
        this.input.close()
      }
    } else {
      this.toDate = null
      this.fromDate = date
    }
    if (this.fromDate) {
      parsed += this._parserFormatter.format(this.fromDate)
    }
    if (this.toDate) {
      parsed += ' - ' + this._parserFormatter.format(this.toDate)
    }
    this.renderer.setProperty(this.rangeDp.nativeElement, 'value', parsed)
  }

  onDateSelectionNew(date: NgbDateStruct) {
    let parsed = ""
    const _minDate = new Date(this.minDate.year, this.minDate.month - 1, this.minDate.day)
    const selectedDate = new Date(date.year, date.month - 1, date.day)
    if (!this.fromDate && !this.toDate) {
      if (moment(selectedDate).isSameOrAfter(_minDate)) {
        this.fromDate = date
      } else {
        this.fromDate = null
        return
      }
    } else if (this.fromDate && !this.toDate && moment(selectedDate).isAfter(_minDate)) {
      const _fromDate = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day)
      if (moment(selectedDate).isAfter(_fromDate)) {
        this.toDate = date
        this.input.close()
      }
    } else {
      const selectedDate = new Date(date.year, date.month - 1, date.day)
      if (moment(selectedDate).isSameOrAfter(_minDate)) {
        this.toDate = null
        this.fromDate = date
      } else {
        this.fromDate = null
        return
      }
    }
    if (this.fromDate) { parsed += this._parserFormatter.format(this.fromDate) }
    if (this.toDate) { parsed += " - " + this._parserFormatter.format(this.toDate) }
    this.renderer.setProperty(this.rangeDp.nativeElement, 'value', parsed)
  }

  dateChangeEvent($event: NgbDateStruct) {
    // let selectedDate = new Date($event.year, $event.month - 1, $event.day)
    let selectedDate = new Date(this.dateParser.format($event))
    let formattedDate = moment(selectedDate).format('L')
    this.selectedDate = formattedDate
  }


  closeModal(status) {
    this._activeModal.close(status)
    document.getElementsByTagName("html")[0].style.overflowY = "auto"
  }
  months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

  getDateStr($date) {
    const strMonth = Number($date.month) ? this.months[$date.month - 1] + "" : ""
    return $date.day + '-' + strMonth + '-' + $date.year
  }

  saveDispatch() {
    if (!this.pickupDate || !this.pickupDate.day) {
      this._toastr.warning('Please select a date first')
      return
    }
    if (!this.vehicleNumer) {
      this._toastr.warning('Please enter Vehicle Number')
      return
    }
    if (!this.driveName) {
      this._toastr.warning('Please enter Driver Full Name')
      return
    }
    if (!this.driverCell) {
      this._toastr.warning('Please enter Driver Contact Number')
      return
    }
    if (!this.fullQWeight) {
      this._toastr.warning('Please enter Full Truck Weight')
      return
    }
    if (!this.emptyWeight) {
      this._toastr.warning('Please enter Empty Truck Weight')
      return
    }
    if (!this.numBags) {
      this._toastr.warning('Please enter Number of Bags')
      return
    }
    const _mtUnit = 8
    let _weight1 = parseFloat(this.fullQWeight)
    let _weight2 = parseFloat(this.emptyWeight)
    const destinUnitFull = this.whUnits.filter(e => e.UnitTypeID === _mtUnit)
    if (parseFloat(this.fullQWeightUOM as any) !== _mtUnit) {
      const _obj1 = destinUnitFull[0].UnitCalculation.filter(e => e.UnitTypeID === parseFloat(this.fullQWeightUOM as any))
      _weight1 = _weight1 / _obj1[0].UnitTypeCalc
    }
    const destinUnitEmpty = this.whUnits.filter(e => e.UnitTypeID === _mtUnit)
    if (parseFloat(this.emptyWeightUOM as any) !== _mtUnit) {
      const _obj2 = destinUnitEmpty[0].UnitCalculation.filter(e => e.UnitTypeID === parseFloat(this.emptyWeightUOM as any))
      _weight2 = _weight2 / _obj2[0].UnitTypeCalc
    }
    if (_weight1 < _weight2) {
      this._toastr.warning('The weight measurement is incorrect')
      return
    }
    if (!this.numBagWeigth) {
      this._toastr.warning('Please enter Weight per bag')
      return
    }
    let _weight3 = parseFloat(this.numBagWeigth)
    const bagUnit = this.whUnits.filter(e => e.UnitTypeID === _mtUnit)
    if (parseFloat(this.numBagWeigthUOM as any) !== _mtUnit) {
      const _obj2 = bagUnit[0].UnitCalculation.filter(e => e.UnitTypeID === parseFloat(this.numBagWeigthUOM as any))
      _weight3 = _weight3 / _obj2[0].UnitTypeCalc
    }

    const finnWeight = _weight1 - _weight2
    if (finnWeight < _weight3) {
      this._toastr.warning('Total package weight should not exeed total truck weight')
      return
    }

    this.loading = true

    const _receiptDate = this.getDatefromObj(this.pickupDate)
    let _receiptID = -1
    try {
      if (this.mode === 'edit') {
        _receiptID = this.recieptData.receiptID as any
      }
    } catch (error) { }

    const toSend = {
      receiptID: _receiptID,
      bookingID: this.bookingData.BookingID,
      receiptDate: _receiptDate,
      vehicleNo: this.vehicleNumer,
      driverName: this.driveName,
      driverMobile: this.driverCell,
      countryID: this.selCountry.id,
      receiptQty: this.numBags,
      receiptQtyUOMID: '1',

      loadedWeight: this.fullQWeight,
      loadedWeightUOMID: this.fullQWeightUOM,

      emptyWeight: this.emptyWeight,
      emptyWeightUOMID: this.emptyWeightUOM,

      goodsWeight: this.totalWeight,
      goodsWeightUOMID: this.totalWeightUOM,

      eachQtyWeight: this.numBagWeigth,
      eachQtyWeightUOMID: this.numBagWeigthUOM,

      totalQtyWeight: this.bagTotalWeight,
      totalQtyWeightUOMID: this.bagTotalWeightUOM,

      countryPhoneCode: this.selCountry.desc[0].CountryPhoneCode,
    }

    if (this.mode === 'add') {
      this._viewBookingService.saveDispatchReceipt(toSend).subscribe((res: JsonResponse) => {
        this.loading = false
        if (res.returnId > 0) {
          this.closeModal(true)
          this._toastr.success('Receipt Added Successfully')
        } else {
          this._toastr.error(res.returnText, res.returnStatus)
        }
      }, error => {
        this.loading = false
      })
    } else {
      this._viewBookingService.editDispatchReceipt([toSend]).subscribe((res: JsonResponse) => {
        this.loading = false
        if (res.returnId > 0) {
          this.closeModal(true)
          this._toastr.success('Receipt Edited Successfully')
        } else {
          this._toastr.error(res.returnText, res.returnStatus)
        }
      }, error => {
        this.loading = false
      })
    }

  }

  getDatefromObj(dateObject) {
    let toSend = null
    try {
      toSend = moment(new Date(dateObject.year, dateObject.month - 1, dateObject.day)).format('YYYY-MM-DD')
    } catch (error) { }
    return toSend
  }


  onWeightChange($fullQWeight, $fullUnit, $emptyWeight, $emptyUnit) {
    try {
      let _weight1 = parseFloat($fullQWeight)
      let _weight2 = parseFloat($emptyWeight)

      const _mtUnit = 8

      const destinUnitFull = this.whUnits.filter(e => e.UnitTypeID === _mtUnit)
      if (parseFloat($fullUnit) !== _mtUnit) {
        const _obj1 = destinUnitFull[0].UnitCalculation.filter(e => e.UnitTypeID === parseFloat($fullUnit))
        _weight1 = _weight1 / _obj1[0].UnitTypeCalc
      }
      const destinUnitEmpty = this.whUnits.filter(e => e.UnitTypeID === _mtUnit)
      if (parseFloat($emptyUnit) !== _mtUnit) {
        const _obj2 = destinUnitEmpty[0].UnitCalculation.filter(e => e.UnitTypeID === parseFloat($emptyUnit))
        _weight2 = _weight2 / _obj2[0].UnitTypeCalc
      }
      const _weight = _weight1 - _weight2 as any

      if (isNumber(_weight) && _weight !== NaN && _weight !== 'NaN') {
        this.totalWeight = _weight
      } else {
        this.totalWeight = 0
      }
      if (!_weight1 && _weight2) {
        this.totalWeight = -(_weight2)
      }
      if (_weight1 && !_weight2) {
        this.totalWeight = (_weight1)
      }
      if (!_weight1 && !_weight2) {
        this.totalWeight = 0
      }
    } catch (error) {
      console.log(error)
      this.totalWeight = 0
    }
  }

  onBagWeightChange($weigth, $unit, $numBags) {
    const _mtUnit = 8
    let _weight = $weigth
    const destinUnitEmpty = this.whUnits.filter(e => e.UnitTypeID === _mtUnit)
    if (parseFloat($unit) !== _mtUnit) {
      const _obj2 = destinUnitEmpty[0].UnitCalculation.filter(e => e.UnitTypeID === parseFloat($unit))
      _weight = $weigth / _obj2[0].UnitTypeCalc
    }
    this.bagTotalWeight = _weight * parseFloat($numBags)
  }


  counter(action: string) {
    if (action === 'minus' && parseInt(this.numBags) > 0) {
      this.numBags = parseInt(this.numBags) - 1
    } else if (action === 'plus') {
      this.numBags = parseInt(this.numBags) + 1
    }
    this.onBagWeightChange(this.numBagWeigth, this.numBagWeigthUOM, this.numBags)
  }

  closePopup() {
    this.closeModal(false)
  }
}