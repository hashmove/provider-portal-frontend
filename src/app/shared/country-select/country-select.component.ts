import { Component, OnInit, Output, Input, EventEmitter, ViewEncapsulation, ChangeDetectionStrategy, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-country-select',
  templateUrl: './country-select.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./country-select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CountrySelect implements OnInit, OnChanges {


  @Input() countryList: Array<CountryDropdown> = []
  viewCountryList: Array<CountryDropdown> = []
  @Input() selectedCountry: CountryDropdown
  @Output() onSelect = new EventEmitter<CountryDropdown>();
  @Input() isOtherLang: boolean = false
  @Input() isReadOnly: boolean = false
  searchText

  constructor() {

  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.countryList && changes.countryList.currentValue) {
      this.viewCountryList = this.countryList
    }
  }

  onCountrySelect(country: any) {
    this.selectedCountry = country;
    this.onSelect.emit(country)
  }

  setCountryList(term) {
    if (term) {
      try {
        const { countryList } = this
        this.viewCountryList = countryList.filter(v => (v.title.toLowerCase().indexOf(this.searchText.toLowerCase()) > -1)  || (v.desc[0].CountryPhoneCode.toLowerCase().indexOf(this.searchText.toLowerCase()) > -1)).slice(0, 10)
      } catch (error) {
        this.searchText = null
        this.viewCountryList = this.countryList
      }
    } else {
      this.viewCountryList = this.countryList
    }

  }
}

export interface SelectedCurrency {
  sortedCurrencyID: number
  sortedCountryName: string
  sortedCountryFlag: string
  sortedCountryId
}
export interface CountryDropdown {
  id: number,
  code: string,
  title: string,
  shortName: string,
  imageName: string,
  desc: any,
  webURL: string
}