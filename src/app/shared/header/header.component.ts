import { Component, OnInit, OnDestroy } from '@angular/core'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { ConfirmLogoutDialogComponent } from '../dialogues/confirm-logout-dialog/confirm-logout-dialog.component'
import { SharedService } from '../../services/shared.service'
import { baseExternalAssets, isRRV1, newFeatures } from '../../constants/base.url'
import { isJSON, getLoggedUserData, getAccessRights, isArrayValid, isMobile } from '../../constants/globalFunctions'
import { isShareLogin } from '../../http-interceptors/interceptor'
import { Router } from '@angular/router'
import { untilDestroyed } from 'ngx-take-until-destroy'
import { IProviderConfig, JsonResponse } from '../../interfaces'
import { IAppObject } from '../../interfaces/access-rights.interface'
import { CommonService } from '../../services/common.service'
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {

  public logoutDisplay: boolean
  public isLoggedIn: boolean
  public userInfo: any
  public userAvatar: string
  public userData: any

  public isGuestLogin: boolean = false
  public isVirtualAirline: boolean = false
  public billingLink: string = null
  public isRRV1 = isRRV1
  showRepSub = false
  showAccountSub = false
  showPerformance = false
  newAppFeatures = newFeatures
  accessRoutes: IAppObject[] = []
  settingsRoutes: IAppObject[] = []
  public currentBookingsCount = 0
  public spotRateBookings: number = 0
  roleName = null
  loginUser
  isMobile = isMobile()
  showInvoice = false

  constructor(
    private modalService: NgbModal,
    private _sharedService: SharedService,
    private _commonService: CommonService,
    private _router: Router
  ) { }

  loginStatus = ''

  ngOnInit() {
    if (isShareLogin()) {
      this.isGuestLogin = true
      this._sharedService.providerLogo.pipe(untilDestroyed(this)).subscribe(state => {
        if (state) {
          this.userAvatar = state
        } else {
          this.userAvatar = null
        }
      })
    }


    this.getBookingsCount()
    this._sharedService.bookingsRefresh.pipe(untilDestroyed(this)).subscribe(_state => {
      if (_state)
        this.getBookingsCount()
    })

    this.setViewConfig()
    this.setMenus()
    try {
      if (localStorage.getItem('userInfo')) {
        this.loginStatus = getLoggedUserData().UserProfileStatus
        this.isLoggedIn = !getLoggedUserData().IsLogedOut
      }
    } catch { }
    this.setRoleName()
    this.loginUser = getLoggedUserData()
    // console.log(this.loginUser)
    this._sharedService.reloadHeader.subscribe(state => {
      if (state) {
        this.setMenus()
        this.loginUser = getLoggedUserData()
        try { this.loginStatus = 'dashboard' } catch { }
        this.setRoleName()
      }
    })
    this._sharedService.dashboardDetail.subscribe((state: any) => {
      if (state && state.BookingDetails && state.BookingDetails.length) {
        this.isVirtualAirline = state.IsVirtualAirLine
      }
      if (state && state.BillingLink) {
        this.billingLink = state.BillingLink
      }
    })
    this.userInfo = JSON.parse(localStorage.getItem('userInfo'))
    this._sharedService.IsloggedIn.subscribe((state: any) => {
      if (!state) {
        try {
          if (this.userInfo) {
            try {
              this.isLoggedIn = !this.userInfo.IsLogedOut
            } catch (error) { }
            this.userInfo = JSON.parse(localStorage.getItem('userInfo'))
            const userObj = JSON.parse(this.userInfo.returnText)
            this.loginStatus = userObj.UserProfileStatus
            this.isLoggedIn = !userObj.IsLogedOut
          } else {
            this.isLoggedIn = true
          }
        } catch {

        }
      } else {
        this.userInfo = JSON.parse(localStorage.getItem('userInfo'))
        if (this.userInfo) {
          try {
            this.isLoggedIn = !getLoggedUserData().IsLogedOut
          } catch {
            this.isLoggedIn = state
          }
        } else {
          this.isLoggedIn = false
        }
      }
      this.setAvatar()
    })
    this._sharedService.providerLogoUpdate.subscribe((_image: string) => {
      if (_image) {
        this.setSharedLogo(_image)
      }
    })
    this._sharedService.signOutToggler.subscribe((state: any) => {
      this.signOutToggler()
      this.setAvatar()
    })

    this._sharedService.updateAvatar.subscribe((state: any) => {
      if (state && state != null) {
        let userObj = JSON.parse(localStorage.getItem('userInfo'))
        let userData = JSON.parse(userObj.returnText)
        userData.ProviderImage = JSON.stringify(state)
        userObj.returnText = JSON.stringify(userData)
        localStorage.setItem('userInfo', JSON.stringify(userObj))
        this.setAvatar()
      }

    })
  }

  getBookingsCount() {
    const { UserID, ProviderID } = getLoggedUserData()
    this._commonService.getBookingsCount(this.payloadForGroupedBookings({ UserID, ProviderID, SpotRateBookingKey: null } as any))
      .subscribe((res: JsonResponse) => {
        if (res.returnId > 0) {
          this.currentBookingsCount = res.returnObject.totCurrBooking
          this.spotRateBookings = res.returnObject.totSpotReq
        }
      })
  }

  setRoleName() {
    try {
      this.roleName = getAccessRights().RoleName

    } catch {
      this.roleName = null
    }
  }

  setMenus() {
    try {
      const _accessRoutes = getAccessRights().AppObject
      const parentMenus = _accessRoutes.filter(_menu => !_menu.ParentObjectID && _menu.ObjectType === 'MENU')
        .map(_menu => ({ ..._menu, ChildMenus: [], IsSubOpen: false }))
      const childMenusFilt = _accessRoutes.filter(_menu => _menu.ParentObjectID && _menu.ObjectType === 'SUB-MENU')
      const childMenusV1 = (!this.showPerformance || this.isMobile) ? childMenusFilt.filter(_menu => _menu.ObjectBL !== 'PERFORMANCE') : childMenusFilt
      const childMenus = (!this.showInvoice) ? childMenusV1.filter(_menu => _menu.ObjectBL !== 'INVOICES') : childMenusV1
      if (isArrayValid(childMenus, 0)) {
        parentMenus.forEach(_menu => {
          _menu.ChildMenus = childMenus.filter(_subMenu => _subMenu.ParentObjectID === _menu.ObjectID)
        })
      }
      const _settingsBLs = this.isMobile ? ['SUPPORT'] : ['SETTINGS', 'SUPPORT']
      this.settingsRoutes = parentMenus.filter(_menu => _settingsBLs.includes(_menu.ObjectBL))
      this.accessRoutes = parentMenus.filter(_menu => !_settingsBLs.includes(_menu.ObjectBL))
      if (this.isMobile) {
        const _mainBL = ['REPORTS', 'MANAGE_RATES', 'CUSTOMERS', 'USERS', 'PARTNERS', 'SETTINGS']
        this.accessRoutes = this.accessRoutes.filter(_menu => !_mainBL.includes(_menu.ObjectBL))
      }
      // console.log(this.accessRoutes)
    } catch { }
  }

  setViewConfig() {
    const userProfile = getLoggedUserData()
    try {
      this.showPerformance = userProfile.ProviderConfig.IsPerformanceShow
      this.showInvoice = userProfile.ProviderConfig.IsInvoicePaymentShow
    } catch { }
    this._sharedService.dashboardDetail.subscribe(state => {
      if (state && state.FeatureToggleJson) {
        try {
          const _feature: IProviderConfig = JSON.parse(state.FeatureToggleJson)
          this.showPerformance = _feature.IsPerformanceShow
          this.showInvoice = userProfile.ProviderConfig.IsInvoicePaymentShow
        } catch { }
      }
    })
  }

  setSharedLogo($image) {
    this.userAvatar = baseExternalAssets + $image
  }

  setAvatar() {
    if (localStorage.getItem('userInfo')) {
      let userObj = JSON.parse(localStorage.getItem('userInfo'))
      this.userData = JSON.parse(userObj.returnText)
      if (this.userData.ProviderImage && this.userData.ProviderImage != '[]' && isJSON(this.userData.ProviderImage)) {
        this.userAvatar = baseExternalAssets + JSON.parse(this.userData.ProviderImage)[0].DocumentFile
      }
      else {
        this.userAvatar = undefined
      }
    }
  }
  signOutToggler() {
    if (location.pathname.indexOf('otp') >= 0) {
      this.logoutDisplay = false
    }
    else if (location.pathname.indexOf('password') >= 0) {
      this.logoutDisplay = false
    }
    else {
      this.logoutDisplay = true
    }
  }

  login() {
    this._router.navigate(['login'])
  }
  logOut() {
    this.modalService.open(ConfirmLogoutDialogComponent, {
      size: 'sm',
      centered: true,
      windowClass: 'small-modal',
      backdrop: 'static',
      keyboard: false
    })
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden'
      }
    }, 0)
  }

  onReportClick(event) {
    event.preventDefault()
    event.stopPropagation()
    this.showRepSub = !this.showRepSub
  }

  onInvoiceClick(event) {
    event.preventDefault()
    event.stopPropagation()
    this.showAccountSub = !this.showAccountSub
  }

  onSubParentClick($event, route) {
    route.IsSubOpen = !route.IsSubOpen
    $event.preventDefault()
    $event.stopPropagation()
  }

  payloadForGroupedBookings($booking) {
    // console.log($booking)
    return {
      userID: $booking.UserID,
      providerID: $booking.ProviderID,
      bookingNumber: null,
      bookingStatusCode: null,
      shipmentStatusCode: null,
      polID: 0,
      polCode: null,
      polType: null,
      podID: 0,
      podCode: null,
      podType: null,
      cargoType: null,
      shippingModeID: 0,
      fromDate: null,
      toDate: null,
      filterProviderID: 0,
      filterCompanyID: 0,
      carrierID: 0,
      sortBy: 'date',
      sortDirection: 'DESC',
      pageSize: 10,
      pageNumber: 1,
      tab: 'SpecialRequest',
      blNumber: null,
      spotRateBookingKey: $booking.SpotRateBookingKey
    }
  }


  ngOnDestroy() { }
}
