import { ElementRef, Injectable, Input, ViewChild } from "@angular/core";
import { getScreenHeight } from "../../constants/globalFunctions";
import { NgFilesConfig } from "../../directives/ng-files";
import { DocumentFile, MetaInfoKeysDetail, UserDocument, UserInfo } from "../../interfaces";
import { IUploadedInvoiceDet } from "../dialogues/invoice-upload/invoice.interface";
import * as moment from 'moment'
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { IInvoiceCard, IInvoiceTrail } from "../../components/pages/user-desk/invoice-card/invoice-card.interface";

@Injectable()
export class IInvoiceHelper {
    // Document Config 
    public config: NgFilesConfig = {
        acceptExtensions: ['pdf', 'jpg', 'jpeg', 'xlsx', 'xls', 'doc', 'docx', 'png', 'txt'],
        maxFilesCount: 1, maxFileSize: 12 * 1024 * 1000, totalFilesSize: 12 * 12 * 1024 * 1000
    }

    isDocConfigSet = false
    allowedFiles = "pdf, jpg, jpeg, xlsx, xls, doc, docx, png, txt"
    docText = '(supported types: pdf, jpg, jpeg, xlsx, doc, docx, png and txt. Maximum size per document: 10MB)'
    droppedDoc

    public additionalDocuments: UserDocument[] = []
    public currentDocObject: UserDocument
    public additionalDoc: UserDocument = null
    selectedDocument: number
    docTypeList = []
    loading = false

    // Misc
    disableScrollDown = false
    loginUser: UserInfo
    heightLimit = getScreenHeight()
    // uploadedInvoiceDet: IUploadedInvoiceDet = null
    isApproved = false
    remarks = ''
    invDtlID: number = null
    // Reject Work
    isRejected = false
    rejectDocObj
    surchageType = [{ code: 'ADCH', label: 'Other Charges' },
    { code: 'FSUR', label: 'Base Freight' }]
    @ViewChild('lineItem') lineItem: ElementRef;
    isInvoiceEditable = false
    isProvider = true

    @Input() invoice: IInvoiceCard
    @Input() selectedInvoice: IInvoiceTrail
    hasInvApproved = false
    @ViewChild('scrollContainer') scrollContainer: ElementRef;


    constructor() { }


    getNum = (_num: any) =>
        (_num) ? (parseFloat(_num) ? typeof parseFloat(_num) === 'number' ? parseFloat(_num) : 0 : 0) : 0

    // New Doc working

    onDocumentClick($doc: UserDocument, index: number, $event) {
        let newDoc: UserDocument = $doc
        this.currentDocObject = $doc
        if ($doc.DocumentID && $doc.DocumentID > 0 ||
            ($doc.DocumentID && $doc.DocumentID > 0 && !$doc.MetaInfoKeysDetail) || $doc.DocumentTypeCode === 'HM_EXCEL') {
            return
        }
        try {
            if ($doc.DocumentTypeID === -1) {
                setTimeout(() => { this.scrollToBottom() }, 200)
                newDoc.MetaInfoKeysDetail.forEach((element: MetaInfoKeysDetail) => {
                    if (element.DataType.toLowerCase() === 'datetime') {
                        if (element.KeyValue) {
                            element.DateModel = this.generateDateStructure(element.KeyValue)
                        }
                    }
                })
            }
        } catch (error) { }
        this.changeVisibility($doc)
    }

    changeVisibility = (doc: UserDocument) => doc.ShowUpload = !doc.ShowUpload


    generateDateStructure(strDate: string): NgbDateStruct {
        const arr: Array<string> = strDate.split('/')
        const dateModel: NgbDateStruct = { day: parseInt(arr[1]), month: parseInt(arr[0]), year: parseInt(arr[2]) }
        return dateModel
    }

    removeDocObj($event, docId) {
        setTimeout(() => { this.loading = true }, 10)
        const { additionalDocuments } = this
        this.additionalDocuments = additionalDocuments.filter(_doc => _doc !== docId)
        setTimeout(() => { this.loading = false }, 20)
    }

    dateChangeEvent($event: NgbDateStruct, index: number) {
        let selectedDate = new Date($event.year, $event.month - 1, $event.day)
        let formattedDate = moment(selectedDate).format('L')
        this.currentDocObject.MetaInfoKeysDetail[index].KeyValue = formattedDate
    }

    customDragCheck($fileEvent: DocumentFile) {
        const selectedFile: DocumentFile = $fileEvent
        this.currentDocObject.DocumentName = selectedFile.fileName
        this.currentDocObject.DocumentFileContent = selectedFile.fileBaseString
        this.currentDocObject.DocumentUploadedFileType = selectedFile.fileType
    }

    scrollToBottom(): void {
        try {
            this.scrollContainer.nativeElement.scrollTop = this.scrollContainer.nativeElement.scrollHeight;
        } catch (err) {
            console.log(err)
        }
    }

}
