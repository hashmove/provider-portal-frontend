import { HttpErrorResponse } from '@angular/common/http'
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core'
import { NgbActiveModal, NgbDateParserFormatter, NgbDropdownConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { ToastrService } from 'ngx-toastr'
import { firstBy } from 'thenby'
import { DashboardService } from '../../components/pages/user-desk/dashboard/dashboard.service'
import { cloneObject } from '../../components/pages/user-desk/reports/reports.component'
import { ViewBookingService } from '../../components/pages/user-desk/view-booking/view-booking.service'
import { baseExternalAssets, baseApi } from '../../constants/base.url'
import { changeCase, getLoggedUserData, isArrayValid, isMobile } from '../../constants/globalFunctions'
import { NgbDateFRParserFormatter } from '../../constants/ngb-date-parser-formatter'
import { JsonResponse, MetaInfoKeysDetail, UserDocument } from '../../interfaces'
import { GuestService } from '../../services/jwt.injectable'
import { ConfirmDialogContent, ConfirmDialogGenComponent } from '../dialogues/confirm-dialog-generic/confirm-dialog-generic.component'
import { IInvoiceHelper } from './invoice-documents.helper'

@Component({
  selector: 'app-invoice-documents',
  templateUrl: './invoice-documents.component.html',
  styleUrls: ['./invoice-documents.component.scss'],
  providers: [{ provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter }, NgbDropdownConfig],
})
export class InvoiceDocumentsComponent extends IInvoiceHelper implements OnInit {

  isMobile = isMobile()

  constructor(
    public _activeModal: NgbActiveModal,
    public _toastr: ToastrService,
    private bookingService: ViewBookingService,
    private _userService: DashboardService,
    private _jwtService: GuestService,
    private _modalService: NgbModal
  ) {
    super()
  }

  async ngOnInit() {
    this.loginUser = getLoggedUserData()
    this.setData()
  }

  setData() {
    this.loading = true
    this.getUploadedDocuments()
  }



  setEditable(document) {
    console.log(document);
    if (document && document.DocumentID > 0 && document.DocumentFileName &&
      document.IsUploadable && document.DocumentLastStatus.toLowerCase() === 'approved') {
      this.hasInvApproved = true
    } else {
      this.hasInvApproved = false
    }
  }

  getUploadedDocuments() {
    this._userService.getInvoiceDocs(this.invoice.BookingID, this.selectedInvoice.invoiceID).subscribe((res: JsonResponse) => {
      this.loading = false
      const _additionalDocuments: UserDocument[] = res.returnId > 0 && isArrayValid(res.returnObject, 0) ?
        res.returnObject.filter(_doc => (_doc.DocumentNature === 'HM_BOOKING_INVOICE') &&
          (_doc.DocumentSubProcess === 'PROVIDER' || _doc.DocumentSubProcess === 'COMMON')) : []
      console.log(_additionalDocuments);

      this.additionalDocuments = _additionalDocuments.filter(_doc => _doc.DocumentID > 0 &&
        (_doc.InvoiceID === this.selectedInvoice.invoiceID ||
          (this.selectedInvoice.customerInvoiceID && _doc.OtherID === this.selectedInvoice.customerInvoiceID && _doc.OtherType === 'BOOKING_INVOICE_RECEIVABLE') ||
          (this.selectedInvoice.receiptID && _doc.OtherID === this.selectedInvoice.receiptID && _doc.OtherType === 'BOOKING_INVOICE_RECEIPT') ||
          (this.selectedInvoice.paymentID && _doc.OtherType === 'BOOKING_INVOICE_PAYMENT') ||
          (this.selectedInvoice.receiptID && _doc.OtherID === this.selectedInvoice.receiptID && _doc.OtherType === 'INVOICE_RECEIPT') ||
          (this.selectedInvoice.paymentID && _doc.OtherID === this.selectedInvoice.paymentID && _doc.OtherType === 'INVOICE_PAYMENT')
        ))
      this.setEditable(this.additionalDocuments.filter(_doc => _doc.BusinessLogic === 'FF_HM_BOOKING_INVOICE')[0])
      const _addDoc = _additionalDocuments.filter(_doc => _doc.BusinessLogic === 'BOOKING_INVOICE_ADD_DOC')[0]
      this.additionalDoc = _addDoc

      if (this.selectedInvoice.paymentID && this.selectedInvoice.paymentID > 0) {
        this.bookingService.getBookingInvoicePayments(this.selectedInvoice.invoiceID, -1).subscribe((res: JsonResponse) => {
          if (res.returnId > 0) {
            const paidItems: any[] = res.returnObject
            const _paymentIds: number[] = paidItems.map(_payment => _payment.paymentID)
            paidItems.forEach(_payment => {
              this.additionalDocuments.forEach(_doc => {
                if (_doc.OtherID && _payment.paymentID === _doc.OtherID && _doc.OtherType === 'BOOKING_INVOICE_PAYMENT') {
                  _doc.PaymentNo = _payment.paymentNo
                  _doc.PaymentTo = _payment.paymentTo
                  _doc.PaymentToDesc = _payment.paymentToDesc
                }
              })
            })
            const { additionalDocuments } = this
            const _nonPayDocs = additionalDocuments.filter(_doc => _doc.OtherType !== 'BOOKING_INVOICE_PAYMENT')
            const _payDocs = additionalDocuments.filter(_doc => _doc.OtherType === 'BOOKING_INVOICE_PAYMENT' && _paymentIds.includes(_doc.OtherID))
            const _combineDocs = _nonPayDocs.concat(_payDocs).sort(
              firstBy(function (v1, v2) { return v1.DocumentID - v2.DocumentID })
            )
            this.additionalDocuments = _combineDocs
            console.log(this.additionalDocuments)
          }
        })
      }
    }, () => {
      this.loading = false
    })
  }

  refreshDoc() {
    this.loading = true
    this.setData()
  }


  acDownloadAction($url: string, $event) {
    $event.preventDefault()
    $event.stopPropagation()
    if ($url && $url.length > 0) {
      const _urlStr = $url.startsWith('[{') ? JSON.parse($url)[0].DocumentFile : $url
      window.open(baseExternalAssets + _urlStr, '_blank')
      if (this.invoice.BookingStatus.toLowerCase() === 'cancelled') return
    }
  }
  close = () =>
    this._activeModal.close(isArrayValid(this.additionalDocuments, 0) ? true : false)

  getHtmlTxt = (_txt) => _txt.replaceAll('\n', '<br>')

  Download() {
    const toSend = {
      invoiceID: this.selectedInvoice.invoiceID,
      bookingID: this.invoice.BookingID,
      fileType: 'XLSX',
      baseURL: baseExternalAssets,
      shippingModeCode: this.invoice.ShippingModeCode,
      containerLoadType: this.invoice.ContainerLoad,

    }
    const request = new XMLHttpRequest()
    request.open('POST', baseApi + 'booking/DownloadBookingInvoiceDetail', true)
    request.setRequestHeader('Content-Type', 'application/json charset=UTF-8')
    request.setRequestHeader('Authorization', 'Bearer ' + this._jwtService.getJwtToken())
    request.setRequestHeader('Source-Via', '-1|MP|MP|MP')
    request.responseType = 'blob'
    request.send(JSON.stringify(toSend))
    this.loading = true
    request.onload = () => {
      this.loading = false
      if (request.status === 200) {
        try {
          const matches = /'([^']*)'/.exec(request.getResponseHeader('content-disposition'))
          const link = document.createElement('a')
          link.href = window.URL.createObjectURL(new Blob([request.response], { type: `application/xlsx` }))
          link.download = (matches != null && matches[1] ? matches[1] : new Date().valueOf() + `.xlsx`)
          document.body.appendChild(link)
          link.click()
          document.body.removeChild(link)
        } catch (error) { console.log(error) }
      } else
        this._toastr.info('Invoice not available yet', 'Invoice Not Available')
    }
    request.onerror = () => {
      this.loading = false
    }
  }

  fetchDocDtl() {
    const { additionalDocuments, selectedDocument } = this
    if (!selectedDocument) {
      this._toastr.warning('Please select a document first')
      return
    }
    if (additionalDocuments.filter(_doc => _doc.BusinessLogic !== 'BOOKING_INVOICE_ADD_DOC' && _doc.DocumentTypeID === parseInt(selectedDocument as any)).length > 0) {
      this._toastr.warning('This document already exists.')
      return
    }

    this.loading = true
    this.bookingService.getDocObject(selectedDocument).subscribe((res: JsonResponse) => {
      this.loading = false
      if (res.returnId > 0) {
        const _newDoc: UserDocument = { ...res.returnObject, ShowUpload: true }
        this.currentDocObject = _newDoc
        const _arr = [...additionalDocuments, _newDoc]
        this.additionalDocuments = _arr.filter(_doc => _doc.DocumentTypeID > -1)
        this.selectedDocument = undefined
      } else {
        this._toastr.error(res.returnText, res.returnStatus)
      }
    }, (error: any) => {
      this.loading = false
    })

  }

  fileSelectFailedEvent($message: string) {
    this._toastr.error($message, 'Error')
  }

  uploadDocument(index: number, document: UserDocument) {
    this.loading = true
    let toSend: UserDocument = cloneObject(document)
    const { currentDocObject } = this
    try { toSend.DocumentName = currentDocObject.DocumentName } catch { }
    try { toSend.DocumentFileContent = currentDocObject.DocumentFileContent } catch { }
    try { toSend.DocumentUploadedFileType = currentDocObject.DocumentUploadedFileType } catch { }
    let docName: string = ''
    try {
      if (toSend.BusinessLogic === 'BOOKING_INVOICE_ADD_DOC') {
        docName = toSend.MetaInfoKeysDetail.filter(meta => meta.KeyName === 'DOCNAME')[0].KeyValue
        toSend.DocumentName = docName
        try {
          const _docDesc = toSend.MetaInfoKeysDetail.filter(meta => meta.KeyName === 'DOCDESC')[0].KeyValue
          if (_docDesc) {
            toSend.DocumentDesc = _docDesc
          }
        } catch { }
      }
    } catch (error) { }

    if (!toSend.DocumentFileContent && !toSend.DocumentFileName) {
      this._toastr.error('Please select a file to upload', 'Invalid Operation')
      this.loading = false
      return
    }

    let emptyFieldFlag: boolean = false
    let hasInvalidLength: boolean = false
    let emptyFieldName: string = ''

    try {
      if (toSend.MetaInfoKeysDetail) {
        toSend.MetaInfoKeysDetail.forEach((element: MetaInfoKeysDetail) => {
          if (element.IsMandatory && !element.KeyValue) {
            emptyFieldFlag = true
            emptyFieldName = element.KeyNameDesc
            return
          }

          if (element.IsMandatory && element.KeyValue && element.KeyValue.length > element.FieldLength) {
            hasInvalidLength = true
            emptyFieldName = element.KeyNameDesc
            return
          }
        })
      }
    } catch (error) { }

    if (emptyFieldFlag) {
      this._toastr.error(`${emptyFieldName} field is empty`, 'Invalid Operation')
      this.loading = false
      return
    }

    if (hasInvalidLength) {
      this._toastr.error(`${emptyFieldName} field length should be less or equal to 50 charaters`, 'Invalid Operation')
      this.loading = false
      return
    }

    try {
      if (this.additionalDocuments.filter(_doc => _doc.DocumentID > 0
        && _doc.DocumentName.toLowerCase() === toSend.DocumentName.toLowerCase() &&
        toSend.BusinessLogic === 'BOOKING_INVOICE_ADD_DOC'
      ).length > 0) {
        this._toastr.error(`A document already exist with the same name.`, 'Duplicated Document')
        this.loading = false
        return
      }
    } catch { }

    toSend.DocumentID = (toSend.DocumentID) ? toSend.DocumentID : -1
    toSend.OtherID = this.selectedInvoice.invoiceID // HERE
    toSend.OtherType = 'BOOKING_INVOICE' // HERE
    toSend.LoginUserID = (this.loginUser && this.loginUser.UserID) ? this.loginUser.UserID : -1

    for (let ind = 0; ind < this.additionalDocuments.length; ind++) {
      if (ind === index)
        this.additionalDocuments[index].ShowUpload = false
    }

    toSend.DocumentLastStatus = (toSend.DocumentID > 0 && toSend.DocumentFileName) ?
      (toSend.DocumentLastStatus.toLowerCase() === 'approved' ? 'RESET' : 'RE-UPLOAD') : toSend.IsApprovalRequired ? 'DRAFT' : null

    try {
      toSend.ProviderName = ""
      toSend.HashMoveBookingNum = this.invoice.HashMoveBookingNum
      toSend.UserName = this.invoice.Customer.FirstName + ' ' + this.invoice.Customer.LastName
      toSend.UserCompanyName = this.invoice.Customer.CustomerName
      toSend.UserCompanyID = this.invoice.Customer.CustomerID
      toSend.UserCountryPhoneCode = "000" //
      toSend.EmailTo = ""
      toSend.PhoneTo = ""
    } catch (error) {
    }
    this._userService.saveUserDocument(toSend).subscribe((res: JsonResponse) => {
      this.loading = false
      if (res.returnId > 0) {
        this._toastr.success(`Document ${toSend.DocumentID > 0 ? 'Updated' : 'Saved'} Successfully`, res.returnStatus)
        this.getUploadedDocuments()
      } else {
        this._toastr.error(res.returnText, res.returnStatus)
      }
    }, (err: HttpErrorResponse) => {
      this.loading = false
    })
  }

  async getDocumentList() {
    try {
      this.loading = true
      if (!this.docTypeList || this.docTypeList.length === 0) {
        try {
          await this.getOtherDocList()
        } catch (error) { }
      }
      this.loading = false
    } catch { }
    setTimeout(() => { this.loading = false }, 10)
  }

  async getOtherDocList() {
    const res = await this.bookingService.getDocListV2('COMMON', 'BOOKING_INVOICE_ADD_DOC').toPromise() as any
    if (res.returnId > 0) {
      this.docTypeList = [changeCase(res.returnObject, 'camel')]
    }
  }

  async addDocument($event: any) {
    // if (this.invoice.BookingStatus.toLowerCase() === 'cancelled') {
    //   this._toastr.warning('Uploading documents are permitted only to specific users.', 'Restricted')
    //   return
    // }
    setTimeout(() => { this.loading = true }, 10)
    await this.getDocumentList()
    const { additionalDocuments, additionalDoc } = this
    const _existDoc = additionalDocuments.filter(doc => (!doc.DocumentID || doc.DocumentID === -1) && doc.DocumentTypeCode !== "HM_EXCEL")
    console.log(_existDoc)
    if (_existDoc && _existDoc.length > 0)
      return
    const _addDoc: any = {
      ...additionalDoc,
      DocumentID: -1,
      DocumentName: 'ADDITIONAL DOCUMENT',
      DocumentTypeDesc: 'ADDITIONAL DOCUMENT',
      DocumentNature: 'HM_BOOKING_INVOICE',
      DocumentStausRemarks: '',
      DocumentUploadDate: '',
      DocumentFileName: null,
      ShowUpload: true,
      IsDownloadable: false,
      DocumentLastStatus: '',
      BookingID: this.invoice.BookingID
    }
    try {
      _addDoc.MetaInfoKeysDetail.forEach(inp => { inp.KeyValue = '' })
    } catch (error) { }
    this.additionalDocuments.push(_addDoc)
    this.currentDocObject = cloneObject(_addDoc)
    setTimeout(() => { this.loading = false }, 10)
    setTimeout(() => {
      try {
        this.selectedDocument = this.docTypeList.filter(_doc => _doc.businessLogic === 'BOOKING_INVOICE_ADD_DOC')[0].documentTypeID
      } catch { }
    }, 10);

    setTimeout(() => { this.scrollToBottom() }, 10)
  }


  async getDocumentDtl(selectedDocument) {
    const { additionalDocuments } = this
    try {
      const res = await this.bookingService.getDocObject(selectedDocument).toPromise() as any
      if (res.returnId > 0) {
        const _newDoc: UserDocument = {
          ...res.returnObject,
          ShowUpload: false,
          HideCancel: true
        }
        this.currentDocObject = _newDoc
        const _arr = [...additionalDocuments, _newDoc]
        this.additionalDocuments = _arr.filter(_doc => _doc.DocumentTypeID > -1)
      } else {
        this._toastr.error(res.returnText, res.returnStatus)
      }
    } catch { }
  }

  onScroll() {
    const element = this.scrollContainer.nativeElement
    const atBottom = element.scrollHeight - element.scrollTop === element.clientHeight
    this.disableScrollDown = this.disableScrollDown && atBottom ? false : true
  }

  removeDocument($document: UserDocument) {
    const _modalData: ConfirmDialogContent = {
      messageTitle: 'Remove Document',
      messageContent: `Are you sure you would like to remove this document?`,
      buttonTitle: 'Yes',
      data: null
    }

    const modalRef = this._modalService.open(ConfirmDialogGenComponent, {
      size: 'lg', centered: true, windowClass: 'small-modal', backdrop: 'static', keyboard: false
    });
    modalRef.componentInstance.modalData = _modalData;
    modalRef.result.then((result: any) => {
      console.log(result)
      if (result) {
        this._userService.removeDocument($document.DocumentID, this.loginUser.UserID).subscribe(() => {
          this.loading = true
          this.getUploadedDocuments()
        })
      }
    })
  }
}
