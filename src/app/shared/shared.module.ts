import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoaderComponent } from './loader/loader.component';
import { UiTableComponent } from './tables/ui-table/ui-table.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { UpdatePriceComponent } from './dialogues/update-price/update-price.component';
import { AddDocumentComponent } from './dialogues/add-document/add-document.component';
import { VideoDialogueComponent } from './dialogues/video-dialogue/video-dialogue.component';
import { CookieBarComponent } from './dialogues/cookie-bar/cookie-bar.component';
import { PriceLogsComponent } from './dialogues/price-logs/price-logs.component';
import { ScrollbarModule } from 'ngx-scrollbar';
import { CargoDetailsComponent } from './dialogues/cargo-details/cargo-details.component';
import { DndDirective } from '../directives/dnd.directive';
import { DragDropComponent } from './drag-drop/drag-drop.component';
import { FileDropModule } from 'angular2-file-drop';
import { VesselScheduleDialogComponent } from './dialogues/vessel-schedule-dialog/vessel-schedule-dialog.component';
import { AddContainersComponent } from './dialogues/add-containers/add-containers.component';
import { AddBlComponent } from './dialogues/add-bl/add-bl.component';
import { AddCustomerComponent } from './dialogues/add-customer/add-customer.component'
import { SafeUrlPipe } from '../constants/pipe/safeurlfilter';
import { ShareViewBooking } from './dialogues/share-view-booking/share-view-booking.component'
import { AirAfricaCargoDetailsComponent } from './dialogues/air-africa-cargo-dtl/air-africa-cargo-dtl.component';
import { ViaPortComponent } from '../components/pages/user-desk/via-ports/via-port.component';
import { CountrySelect } from './country-select/country-select.component';
import { HashmoveMap } from './map-utils/map.component';
import { AdjustmentLogsComponent } from './dialogues/adjustment-logs/adjustment-logs.component';
import { UserRegDialogComponent } from './dialogues/user-reg-dialog/user-reg-dialog.component';
import { PartnerRegComponent } from './dialogues/partner-reg/partner-reg.component';
import { UploadHistoryDialogComponent } from './dialogues/upload-history/upload-history.component';
import { AddScheduleComponent } from './dialogues/add-schedule/add-schedule.component';
import { WarhouseTableComponent } from './tables/warehouse-table/warehouse-table.component';
import { AddDispatchComponent } from './dialogues/add-dispatch/add-dispatch.component';
import { PipeModule } from '../constants/pipe/pipe.module';
import { ChatComponent } from './dialogues/chat/chat.component';
import { DocumentUploadComponent } from './dialogues/document-upload/document-upload.component';
import { CargoDetailsRateRequestComponent } from './dialogues/cargo-details-rate-request/cargo-details-rate-request.component';
import { ChatRequestComponent } from './dialogues/chat-request/chat-request.component';
import { DocumentUploadRequestComponent } from './dialogues/document-upload-request/document-upload-request.component';
import { CountDownComponent } from '../components/pages/user-desk/count-down/count-down.component';
import { NewFeatureAlertComponent } from './dialogues/new-feature-alert/new-feature-alert.component';
import { InvoiceDocumentsComponent } from './invoice-documents/invoice-documents.component';
import { ViaPortSeaComponent } from './dialogues/via-port/via-port.component';

@NgModule({
  imports: [CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    FileDropModule,
    ScrollbarModule,
    PipeModule
  ],
  declarations: [
    LoaderComponent,
    NewFeatureAlertComponent,
    UiTableComponent,
    UpdatePriceComponent,
    AddDocumentComponent,
    VideoDialogueComponent,
    CookieBarComponent,
    PriceLogsComponent,
    ChatComponent,
    CountDownComponent,
    InvoiceDocumentsComponent,
    ChatRequestComponent,
    CargoDetailsComponent,
    ViaPortSeaComponent,
    DndDirective,
    DragDropComponent,
    VesselScheduleDialogComponent,
    AddContainersComponent,
    AddBlComponent,
    AddCustomerComponent,
    SafeUrlPipe,
    ShareViewBooking,
    AirAfricaCargoDetailsComponent,
    CargoDetailsRateRequestComponent,
    DocumentUploadComponent,
    // InvoiceUploadComponent,
    DocumentUploadRequestComponent,
    ViaPortComponent,
    CountrySelect,
    HashmoveMap,
    AdjustmentLogsComponent,
    AddDispatchComponent,
    UserRegDialogComponent,
    PartnerRegComponent,
    UploadHistoryDialogComponent,
    AddScheduleComponent,
    WarhouseTableComponent
  ],
  exports: [
    LoaderComponent,
    NewFeatureAlertComponent,
    UiTableComponent,
    UpdatePriceComponent,
    AddDocumentComponent,
    CookieBarComponent,
    DndDirective,
    DragDropComponent,
    ShareViewBooking,
    ViaPortComponent,
    CountrySelect,
    HashmoveMap,
    UserRegDialogComponent,
    PartnerRegComponent,
    UploadHistoryDialogComponent,
    WarhouseTableComponent,
    CountDownComponent
  ],
  entryComponents: [
    VideoDialogueComponent,
    UpdatePriceComponent,
    PriceLogsComponent,
    ChatComponent,
    CountDownComponent,
    InvoiceDocumentsComponent,
    ChatRequestComponent,
    CargoDetailsComponent,
    ViaPortSeaComponent,
    VesselScheduleDialogComponent,
    AddBlComponent,
    AddContainersComponent,
    AddCustomerComponent,
    ShareViewBooking,
    AirAfricaCargoDetailsComponent,
    CargoDetailsRateRequestComponent,
    DocumentUploadComponent,
    // InvoiceUploadComponent,
    DocumentUploadRequestComponent,
    ViaPortComponent,
    CountrySelect,
    AdjustmentLogsComponent,
    AddDispatchComponent,
    UserRegDialogComponent,
    PartnerRegComponent,
    UploadHistoryDialogComponent,
    AddScheduleComponent,
    NewFeatureAlertComponent
  ]
})
export class SharedModule { }
