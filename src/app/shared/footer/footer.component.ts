import { Component, OnInit } from '@angular/core';
import { getLoggedUserData } from '../../constants/globalFunctions';
import { CommonService } from "../../services/common.service";

@Component({
  selector: "app-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.scss"]
})
export class FooterComponent implements OnInit {

  public _footerDetail: any = {
    HMSupportAddress: "",
    HMSupportDesc: "Help & Support",
    HMSupportPhone: "+971 4 1234567",
    HMSupportEmail: "support@hashmove.com",
    HMPrivacyURL: "https://www.hashmove.com/privacy-policy.html",
    HMTermsURL: "https://www.hashmove.com/terms-of-use.html",
    HMWebsiteURL: "https://www.hashmove.com/solutions.html",
    HMSLA: "https://hashmove.com/software-license.html"
  };

  constructor(
    private _commonServices: CommonService
  ) { }

  ngOnInit() {
    this.getFooterDetails();
  }

  getFooterDetails() {
    this._commonServices.getHelpSupport((getLoggedUserData()) ? getLoggedUserData().ProviderID : -1).subscribe((res: any) => {
      try {
        this._footerDetail = JSON.parse(res.returnText);
      } catch {}
    });
  }
}
