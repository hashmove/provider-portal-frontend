import { Component, OnInit, OnChanges, SimpleChanges, Input, OnDestroy, Output, EventEmitter } from "@angular/core"
import { ViewBookingService } from "../../../components/pages/user-desk/view-booking/view-booking.service"
import { ConfirmDialogGenComponent } from "../../dialogues/confirm-dialog-generic/confirm-dialog-generic.component"
import { loading, base64Encode, changeCase, getLoggedUserData } from "../../../constants/globalFunctions"
import { AddDispatchComponent } from "../../dialogues/add-dispatch/add-dispatch.component"
import { IDispatchSummary, IReceiptSummary, IReceiptItem, ICalUnit } from "../../../interfaces"
import { SharedService } from "../../../services/shared.service"
import { JsonResponse } from "../../../interfaces/JsonResponse"
import { HMTableHead } from "../ui-table/ui-table.component"
import { NgbModal } from "@ng-bootstrap/ng-bootstrap"
import { PaginationInstance } from "ngx-pagination"
import { ToastrService } from "ngx-toastr"

@Component({
  selector: "app-warehouse-table",
  templateUrl: "./warehouse-table.component.html",
  styleUrls: ["./warehouse-table.component.scss"]
})
export class WarhouseTableComponent implements OnInit, OnChanges, OnDestroy {

  public selectedSort: any = {
    title: "Rate For",
    value: "CustomerName",
    column: "CustomerID"
  };
  // @Input() data
  public maxSize: number = 7;
  public directionLinks: boolean = true;
  public autoHide: boolean = false;
  public checkAllPublish: boolean;
  public checkAllDrafts: boolean = false;
  public LCLRecords: number;
  public FCLRecords: number;

  // pagination vars
  // public pageSize: number = 5;
  public page: number = 1;
  // collectionSize = this.totalRecords

  public thList: Array<HMTableHead> = [
    { title: "Dispatch Date", activeClass: "", sortKey: "CustomerName" },
    { title: "Delivery Date", activeClass: "", sortKey: "CustomerName" },
    { title: "Vehicle No.", activeClass: "", sortKey: "CustomerName" },
    { title: "Driver Full Name", activeClass: "", sortKey: "CustomerName" },
    { title: "No of Bags", activeClass: "", sortKey: "CustomerName" },
    { title: "Weight Bag (MT)", activeClass: "", sortKey: "CustomerName" },
    // { title: "Driver Cell #", activeClass: "", sortKey: "CustomerName" },
    { title: "Total Qty (MT)", activeClass: "", sortKey: "CustomerName" },
  ];
  public thList2: Array<HMTableHead> = [
    { title: "Receipt Date", activeClass: "", sortKey: "CustomerName" },
    // { title: "Vehicle No.", activeClass: "", sortKey: "CustomerName" },
    { title: "Vehicle No. / Driver", activeClass: "", sortKey: "CustomerName" },
    // { title: "Driver Cell #", activeClass: "", sortKey: "CustomerName" },
    { title: "Empty Load (MT) ", activeClass: "", sortKey: "CustomerName" },
    { title: "Full Load (MT)", activeClass: "", sortKey: "CustomerName" },
    { title: "Total Weight (MT)", activeClass: "", sortKey: "CustomerName" },
    { title: "No of Bags", activeClass: "", sortKey: "CustomerName" },
    { title: "Weight Bag (MT)", activeClass: "", sortKey: "CustomerName" },
    { title: "Total Weight (MT)", activeClass: "", sortKey: "CustomerName" },
  ];

  @Input() data
  @Input() tableType
  @Input() refreshSummary
  @Input() searchCriteria
  @Input() isGuestLogin
  @Output() refreshData = new EventEmitter<any>()

  public devicPageConfig: PaginationInstance = {
    id: "dispatchSummary",
    itemsPerPage: 5,
    currentPage: 1
  };
  public devicPageConfigPublish: PaginationInstance = {
    id: "receiptSummary",
    itemsPerPage: 5,
    currentPage: 1
  };

  public labels: any = {
    previousLabel: "",
    nextLabel: "",
    screenReaderPaginationLabel: "Pagination",
    screenReaderPageLabel: "page",
    screenReaderCurrentLabel: `You're on page`
  };

  public totalCount: number;
  public airFreightTypes: any[] = []
  public isVirtualAirline: boolean = false

  pgNum1 = 1
  pgNum2 = this.devicPageConfig.itemsPerPage


  dispatchSummary: IDispatchSummary
  receiptSummary: IReceiptSummary
  selectedReceipt: IReceiptItem
  calUnits: ICalUnit[]
  totalWeight: number = 0
  receivedWeight: number = 0
  remainingdWeight: number = 0
  isPmex = false

  constructor(
    private _modalService: NgbModal,
    private _viewbookingService: ViewBookingService,
    private _sharedService: SharedService,
    private _toastr: ToastrService
  ) { }


  ngOnDestroy(): void {
  }



  ngOnInit() {
    this.setInit()
    if (this.data.BookingSourceVia && this.data.BookingSourceVia.length > 2) {
      SharedService.SET_VB_SOURCE_VIA(this.data.BookingSourceVia)
      try {
        if (this.data.BookingSourceVia.toLowerCase().includes('pmex')) {
          this.isPmex = true
        }
      } catch { }
    }
    if (localStorage.getItem('cal_units')) {
      const _calUnits = JSON.parse(localStorage.getItem('cal_units'))
      this.calUnits = _calUnits.filter(
        e => (e.UnitTypeNature === "WEIGHT" || e.UnitTypeNature === "WEIGHT_TON")
      );
    } else {
      this._viewbookingService.getCalcUnits().subscribe((res: JsonResponse) => {
        if (res && res.returnId > -1) {
          const _calUnits = res.returnObject
          this.calUnits = _calUnits.filter(
            e => (e.UnitTypeNature === "WEIGHT" || e.UnitTypeNature === "WEIGHT_TON")
          );
          localStorage.setItem('cal_units', JSON.stringify(res.returnObject))
        }
      })
    }
  }

  setInit() {
    if (this.tableType === 'dispatch') {
      this.getDispatchSummary()
    } else {
      this.getSummary()
    }
  }

  getDispatchSummary() {
    const id = base64Encode(this.data.BookingID);
    this._viewbookingService.getDispatchReceiptList(id).subscribe((res: JsonResponse) => {
      loading(false)
      if (res.returnId > 0 && res.returnObject && res.returnObject.ReceiptItems && res.returnObject.ReceiptItems.length > 0) {
        const data = changeCase(res.returnObject, 'camel')

        const _receiptList = data.receiptItems.map(_data => {
          return {
            ..._data,
            isChecked: false
          }
        })
        this.receiptSummary = {
          ...data,
          receiptItems: _receiptList
        }

        if (this.searchCriteria.SearchLog && this.searchCriteria.SearchLog.totalWeight) {
          try {
            this.totalWeight = this.searchCriteria.SearchLog.totalWeight / 1000;

            const { receiptItems } = this.receiptSummary
            const _totalDispatchWeight = receiptItems.map(_item => _item.totalQtyWeight).reduce((all, item) => {
              return all + item;
            });
            this.receivedWeight = _totalDispatchWeight;
            this.remainingdWeight = this.totalWeight - this.receivedWeight;
          } catch { }
        }


      }
    }, error => {
      loading(false)
    })
  }

  refreshDispatchSummary() {
    loading(true)
    this.getDispatchSummary()
  }

  getSummary() {
    const id = base64Encode(this.data.BookingID);
    this._viewbookingService.getExpectedDispatch(id).subscribe((res1: JsonResponse) => {
      loading(false)
      if (res1.returnId > 0) {
        // this.dispatchSummary = res1.returnObject
        // this.shared
        this.dispatchSummary = changeCase(res1.returnObject, 'camel')
        this._sharedService.draftSummary.next(this.dispatchSummary)
      }
    }, error => {
      loading(false)
    })
  }


  onCheck($receiptID: number, $receipt) {
    this.receiptSummary.receiptItems.forEach(_recp => {
      if (_recp.receiptID !== $receiptID) {
        _recp.isChecked = false
      }
    })

    const _selectedReceipt = this.receiptSummary.receiptItems.filter(_recp => _recp.receiptID === $receiptID)[0]
    if (_selectedReceipt.isChecked) {
      this.selectedReceipt = _selectedReceipt
    } else {
      this.selectedReceipt = null
    }
  }
  ngOnChanges(changes: SimpleChanges): void {
    // if (changes.refreshSummary.currentValue !== changes.refreshSummary.previousValue) {
    //   this.setInit()
    // }
  }
  getTotalPages(pages, from) {
    let temp: any = pages
    if (from === 'summary') {
      return Math.ceil(temp / this.devicPageConfig.itemsPerPage)
    } else {
      return Math.ceil(temp / this.devicPageConfigPublish.itemsPerPage)
    }

  }

  onPageChange(number, from) {
    if (from === 'summary') {
      this.devicPageConfig.currentPage = number;
      this.pgNum2 = this.devicPageConfig.itemsPerPage
    } else {
      this.devicPageConfigPublish.currentPage = number;
      this.pgNum2 = this.devicPageConfigPublish.itemsPerPage
    }
  }

  addReceipt() {
    const modalRef = this._modalService.open(AddDispatchComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'large-modal',
      backdrop: 'static',
      keyboard: false
    });
    modalRef.componentInstance.bookingData = this.data
    modalRef.componentInstance.whUnits = this.calUnits
    modalRef.componentInstance.mode = 'add'
    modalRef.result.then((result) => {
      if (result) {
        loading(true)
        this.getDispatchSummary()
      }
    });
  }

  onEdit() {
    if (!this.selectedReceipt) {
      return
    }
    const modalRef = this._modalService.open(AddDispatchComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'large-modal',
      backdrop: 'static',
      keyboard: false
    });
    modalRef.componentInstance.bookingData = this.data
    modalRef.componentInstance.whUnits = this.calUnits
    modalRef.componentInstance.mode = 'edit'
    modalRef.componentInstance.recieptData = this.selectedReceipt
    modalRef.result.then((result) => {
      if (result) {
        this.selectedReceipt = null
        loading(true)
        this.getDispatchSummary()
      }
    });
  }

  onDelete() {
    if (!this.selectedReceipt) {
      return
    }
    const { selectedReceipt } = this


    const _modalData = {
      messageTitle: 'Remove Receipt',
      messageContent: `Are you sure you want to remove this Receipt?`,
      buttonTitle: 'Yes',
      data: null
    }

    const modalRef = this._modalService.open(ConfirmDialogGenComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'medium-modal',
      backdrop: 'static',
      keyboard: false
    });
    modalRef.componentInstance.modalData = _modalData;
    modalRef.result.then((result: string) => {
      if (result) {
        loading(true)
        const toSend = [
          {
            "ReceiptID": this.selectedReceipt.receiptID,
            "BookingID": this.data.BookingID,
            "ModifiedBy": getLoggedUserData().UserID
          }
        ]
        this._viewbookingService.deleteDispatchReceipt(toSend).subscribe((res: JsonResponse) => {
          loading(false)
          if (res.returnId > 0) {
            this.devicPageConfigPublish.currentPage = 1
            const { receiptSummary } = this
            let _receiptList = receiptSummary.receiptItems.map(_data => {
              return {
                ..._data,
                isChecked: false
              }
            })
            _receiptList = _receiptList.filter(_recp => _recp.receiptID !== this.selectedReceipt.receiptID)
            this.receiptSummary = {
              ...receiptSummary,
              receiptItems: _receiptList
            }
            this.selectedReceipt = null
            this.getDispatchSummary()
          }
        }, error => {
          loading(false)
        })
      }
    })

    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);
  }

  confirmAllRecieved() {
    const _modalData = {
      messageTitle: 'Received all Goods',
      messageContent: `Are you sure you have received all goods?`,
      buttonTitle: 'Yes',
      data: null
    }

    const modalRef = this._modalService.open(ConfirmDialogGenComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'medium-modal',
      backdrop: 'static',
      keyboard: false
    });
    modalRef.componentInstance.modalData = _modalData;
    modalRef.result.then((result: string) => {
      if (result) {
        const toSend = {
          "IsAllGoodsReceived": true,
          "IsAllGoodsReceivedByUserID": this.data.UserID,
          "BookingID": this.data.BookingID
        }
        loading(true)
        this._viewbookingService.updateIsAllGoodsReceived(toSend).subscribe((res: JsonResponse) => {
          if (res.returnId > 0) {
            this._toastr.success(res.returnText, "Success")
            this.refreshData.emit(true)
          } else {
            this._toastr.error(res.returnText, "Failed")
          }
        }, error => {
          this._toastr.error('There was an error while processing your request, please try later', "Failed")
        })
      }
    })

    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);
  }
}