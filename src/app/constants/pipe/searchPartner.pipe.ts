import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'searchPartner'
})
export class SearchPartnerPipe implements PipeTransform {
    transform(data: any, term: any): any {

        if (!data) return null;
        if (!term) return data;
        return data.filter(function (item) {
            return JSON.stringify(item.searchField).toLowerCase().includes(term.replace(/\s+/g, ' ').toLowerCase());
        })

    }
}
