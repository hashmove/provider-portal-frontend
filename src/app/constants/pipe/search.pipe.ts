import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {
  transform(data: any, term: any): any {

    if (!data) return null;
    if (!term) return data;
    return data.filter(function (item) {
      return Object.values(item)
        .filter(_item => typeof _item === 'string').toString().toLowerCase().includes(term.replace(/\s+/g, ' ').toLowerCase());
    })

  }
}
