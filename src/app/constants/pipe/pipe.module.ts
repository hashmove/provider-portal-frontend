import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TruncatePipe } from './truncateFilter';
import { CheckboxPipe } from './checkbox.pipe';
import { UniquePipe } from './unique-recordFilter';
import { SearchBookingMode } from './dashboardBookingsFilter';
import { SearchPipe } from './search.pipe';
import { SearchPartnerPipe } from './searchPartner.pipe'
import { FreightServicePipe } from './services.pipe';
import { SpaceRemoverPipe } from './space-remover.pipe';
import { AlphaNumPipe } from './alphanum.pipe';
import { LowerCaseInputDirective } from '../../directives/lowercase-inp.directive';
import { SpaceRemoverInpDirective } from '../../directives/space-remover-inp.directive';
import { SpecialCharRemoverDirective } from '../../directives/special-char-remover.directive';
import { PriceDirective } from '../../directives/price.directive';
import { PriceDecimalDirective } from '../../directives/dec-price.directive';
import { SafeHTML } from './safe-html.pipe';
import { AddCharFilterPipe } from './add-char-filter.pipe';
import { PriceDecimalSvnDirective } from '../../directives/dec-price-7.directive';
import { PriceDecimalNegDirective } from '../../directives/dec-price-neg.directive';
import { FAQSearchPipe } from './faq-search.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    TruncatePipe,
    CheckboxPipe,
    UniquePipe,
    SearchBookingMode,
    SearchPipe,
    FAQSearchPipe,
    SearchPartnerPipe,
    FreightServicePipe,
    SpaceRemoverPipe,
    AlphaNumPipe,
    SpecialCharRemoverDirective,
    PriceDirective,
    PriceDecimalDirective,
    PriceDecimalNegDirective,
    PriceDecimalSvnDirective,
    SpaceRemoverInpDirective,
    AddCharFilterPipe,
    SafeHTML,
    LowerCaseInputDirective
  ],
  exports: [
    TruncatePipe,
    CheckboxPipe,
    UniquePipe,
    SearchBookingMode,
    SearchPipe,
    FAQSearchPipe,
    SearchPartnerPipe,
    FreightServicePipe,
    SpaceRemoverPipe,
    AlphaNumPipe,
    SpecialCharRemoverDirective,
    PriceDirective,
    PriceDecimalDirective,
    PriceDecimalNegDirective,
    PriceDecimalSvnDirective,
    SpaceRemoverInpDirective,
    AddCharFilterPipe,
    SafeHTML,
    LowerCaseInputDirective
  ]
})
export class PipeModule { }
