import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
    name: 'alphaNum'
})
export class AlphaNumPipe implements PipeTransform {

    transform(value: any): any {
        if (value === undefined || value === null)
            return '';
        const _value = value.replace(/\s/g, "");
        return _value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
    }

}