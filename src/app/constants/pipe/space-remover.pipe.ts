import { Pipe, PipeTransform } from '@angular/core';
import { isNumeric } from '../globalFunctions';

@Pipe({
    name: 'spaceRemover'
})
export class SpaceRemoverPipe implements PipeTransform {

    transform(value: string): any {
        if (value === undefined || value === null)
            return '';
        if (isNumeric(value))
            return value;
        const _value = value.replace(/\s/g, "");
        const _value2 = _value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
        const value3 = _value2.match(/[0-9]+/g)
        return (value3 && typeof value3 === 'object') ? value3.join("") : value3
    }

}