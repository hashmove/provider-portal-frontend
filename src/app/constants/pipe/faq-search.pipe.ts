import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import { FAQ } from '../../components/pages/user-desk/support/support.interface';

@Pipe({
  name: 'faqSearchPipe'
})
export class FAQSearchPipe implements PipeTransform {
  transform(data: any[], term: any): any {

    if (!data) return null;
    if (!term) return data;
    // return data.filter(function (item) {
    //   return data.filter(_item => typeof _item === 'string').toString().toLowerCase().includes(term.replace(/\s+/g, ' ').toLowerCase());
    // })
    return data.filter(_dat => _dat.SearchField.toLowerCase().includes(term.toLowerCase().replace(/\s+/g, ' ')))
  }
}
