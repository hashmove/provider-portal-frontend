import { Pipe, PipeTransform } from '@angular/core';
import { JsonSurchargeDet } from '../../shared/dialogues/request-add-rate-dialog/request-add-rate.interface';

@Pipe({
  name: 'addCharFilter'
})
export class AddCharFilterPipe implements PipeTransform {
  transform(_charges: JsonSurchargeDet[], searchText: string, addedCharges: JsonSurchargeDet[]): any {
    try {
      const _chargesIds: number[] = addedCharges.filter(_charge => _charge.addChrID).map(_charge => _charge.addChrID)
      return this.getFilteredList(_charges, searchText).filter((_charge) => !_chargesIds.includes(_charge.addChrID))
    } catch (error) {
      return this.getFilteredList(_charges, searchText)
    }
  }

  getFilteredList(_charges: JsonSurchargeDet[], term) {
    if (term)
      return _charges.filter(function (item) { return Object.values(item).filter(_item => typeof _item === 'string').toString().toLowerCase().includes(term.replace(/\s+/g, ' ').toLowerCase()) })
    else
      return _charges
  }
}
