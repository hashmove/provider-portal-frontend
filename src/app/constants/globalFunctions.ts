import { AbstractControl, ValidatorFn, FormControl } from '@angular/forms';
import { baseExternalAssets } from './base.url';
import { Base64 } from 'js-base64';
import * as moment from 'moment'
import { UserInfo } from '../interfaces/billing.interface';
import { isString } from '@ng-bootstrap/ng-bootstrap/util/util';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { IAccessRights } from '../interfaces/access-rights.interface';
declare var Paytabs: any;


export const EMAIL_REGEX: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

export const ValidateEmail = (email: string): boolean => {
  let arr = email.split('@')
  let first = arr[0]
  let second = arr[1].split('.')[0]
  if (first.length > 64) {
    return false
  }
  if (second.length > 255) {
    return false
  }
  return true
}



export function patternValidator(regexp: RegExp): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } => {
    const value = control.value;
    if (!value) {
      return null;
    }
    return !regexp.test(value) ? { 'patternInvalid': { regexp } } : null;
  };
}

export function leapYear(year) {
  return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
}

export function loading(display) {
  let loader = document.getElementsByClassName("overlay")[0] as HTMLElement;
  if (display) {
    loader.classList.add('overlay-bg');
    loader.style.display = "block";
  }
  else if (!display) {
    loader.classList.remove('overlay-bg');
    loader.style.display = "none";
  }
}


export function CustomValidator(control: AbstractControl) {
  if (this.showTranslatedLangSide) {
    let companyRegexp: RegExp = /^(?=.*?[a-zA-Z])[^.]+$/;
    if (!control.value) {
      return {
        required: true
      }
    }

    //   else if (control.value.length < 3 && control.value) {
    //     if (!companyRegexp.test(control.value)) {
    //       return {
    //         pattern: true
    //       }
    //     }
    //     else {
    //       return {
    //         minlength: true
    //       }
    //     }
    //   }
    //   else if (control.value.length > 50 && control.value) {
    //     if (!companyRegexp.test(control.value)) {
    //       return {
    //         pattern: true
    //       }
    //     }
    //     else {
    //       return {
    //         maxlength: true
    //       }
    //     }

    //   }
    //   else {
    //   return false
    //  }

  }
};

export const statusCode = {
  draft: 'draft',
  approved: 'approved',
  rejected: 'rejected',
  cancelled: 'cancelled',
  confirmed: 'confirmed',
  in_transit: 'in-transit',
  re_upload: 're-upload',
  completed: 'completed',
  readytoship: 'ready to ship',
  in_review: 'in-review',
  pending: 'pending',
  goods_stored: 'goods stored',
  expired: 'expired'
}

export function readyForPayment(obj) {
  Paytabs("#express_checkout").expresscheckout({
    settings: {
      merchant_id: "10038289",
      customer_email: "abdur@hashmove.com",
      secret_key: "ekQNVlcQwtHZv93SClVAqo9euW1k1cKgxA4sVgjrJ1qfat8NO3ofsxtXuviwH2MeCRHx81YS3o7dSf1HjpWMXqJrV1XC3KRFCzdK",
      currency: obj.currency,
      amount: obj.dueAmount,
      title: obj.firstName + obj.lastName,
      product_names: "HashMove_Provider",
      order_id: obj.shipmentId,
      url_redirect: window.location.protocol + "//" + window.location.host + "/paytabs.asp",
      display_customer_info: 1,
      display_billing_fields: 1,
      display_shipping_fields: 0,
      language: "en",
      redirect_on_reject: window.location.protocol + "//" + window.location.host + "/paytabs.asp",
    }
  });
}









export const LINKEDIN_REGEX: RegExp = /^(?:(?:http|https):\/\/)?((www|\w\w)\.)?linkedin.com(\w+:{0,1}\w*@)?(\S+)(:([0-9])+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/;
export const FACEBOOK_REGEX: RegExp = /^(?:(?:http|https):\/\/)?(?:www.)?(facebook.com|fb.com)\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-\]*\/)?(?:profile.php\?id=(?=\d.*))?([\w\-]*)?$/;
export const TWITTER_REGEX: RegExp = /(?:(https|http):\/\/)?(?:www\.)?twitter\.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[\w\-]*\/)*([\w\-]*)$/;
export const INSTAGRAM_REGEX: RegExp = /(?:https?:)?\/\/(?:www\.)?(?:instagram\.com|instagr\.am)\/([A-Za-z0-9_](?:(?:[A-Za-z0-9_]|(?:\.(?!\.))){0,28}(?:[A-Za-z0-9_]))?)$/;
export const YOUTUBE_REGEX: RegExp = /^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+$/;
export const URL_REGEX: RegExp = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
export const GEN_URL: RegExp = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-zA-Z0-9]+([\-\.]{1}[a-zA-Z0-9]+)*\.[a-zA-Z]{2,5}(:[0-9]{1,5})?(\/.*)?$/

export const encryptBookingID = (bookingId: number, userId?: any, shippingModeCode?: string, isGuest?: string): string => {
  let toEncrypt: string
  if (isGuest) {
    toEncrypt = bookingId + '|' + userId + '|' + shippingModeCode + '|' + isGuest
  } else {
    toEncrypt = bookingId + '|' + userId + '|' + shippingModeCode
  }

  // const toEncrypt: string = bookingId + '00000' + bookingId
  const toSend: string = Base64.encode(toEncrypt)
  return toSend
}


export function base64Encode(data) {
  return Base64.encode(data)
}

export function base64Decode(_data) {
  return Base64.decode(_data)
}

export enum ImageSource {
  FROM_SERVER,
  FROM_ASSETS
}

export enum ImageRequiredSize {
  original,
  _96x96,
  _80x80,
  _48x48,
  _32x32,
  _24x24,
  _16x16,
}



export const getImagePath = (fileSource: ImageSource, fileName: string, reqSize: ImageRequiredSize): string => {
  let url = ''
  if (fileSource === ImageSource.FROM_ASSETS) {

  }
  if (fileSource === ImageSource.FROM_SERVER) {
    try {
      if (reqSize === ImageRequiredSize.original) {
        url = baseExternalAssets + fileName
      } else if (reqSize === ImageRequiredSize._96x96) {
        url = baseExternalAssets + fileName.replace("original", "96x96")
      } else if (reqSize === ImageRequiredSize._80x80) {
        url = baseExternalAssets + fileName.replace("original", "80x80")
      } else if (reqSize === ImageRequiredSize._48x48) {
        url = baseExternalAssets + fileName.replace("original", "48x48")
      } else if (reqSize === ImageRequiredSize._32x32) {
        url = baseExternalAssets + fileName.replace("original", "32x32")
      } else if (reqSize === ImageRequiredSize._24x24) {
        url = baseExternalAssets + fileName.replace("original", "24x24")
      } else if (reqSize === ImageRequiredSize._16x16) {
        url = baseExternalAssets + fileName.replace("original", "16x16")
      }
    } catch (error) {
      url = baseExternalAssets + fileName
    }
  }

  return url
}

export function getProviderImage(strJsonPath: string) {
  let jsonStr: any = null

  try {
    jsonStr = JSON.parse(strJsonPath)[0].ProviderLogo
  } catch (error) {
    jsonStr = ""
  }
  return jsonStr
}


export function isJSON(str) {

  if (typeof (str) !== 'string') {
    return false;
  }
  try {
    JSON.parse(str);
    return true;
  } catch (e) {
    return false;
  }
}

// Object Keys Changes
export function changeCase(o, toCase) {
  var newO, origKey, newKey, value
  if (o instanceof Array) {
    return o.map(function (value) {
      if (typeof value === "object" && toCase === 'camel') {
        value = changeCase(value, 'camel')
      } else if (typeof value === "object" && toCase === 'pascal') {
        value = changeCase(value, 'pascal')
      }
      return value
    })
  } else {
    newO = {}
    for (origKey in o) {
      if (o.hasOwnProperty(origKey)) {
        if (toCase === 'camel') {
          newKey = (origKey.charAt(0).toLowerCase() + origKey.slice(1) || origKey).toString()
        } else if ('pascal') {
          newKey = (origKey.charAt(0).toUpperCase() + origKey.slice(1) || origKey).toString()
        }

        value = o[origKey]
        if (value instanceof Array || (value !== null && value.constructor === Object)) {
          if (typeof value === "object" && toCase === 'camel') {
            value = changeCase(value, 'camel')
          } else if (typeof value === "object" && toCase === 'pascal') {
            value = changeCase(value, 'pascal')
          }
        }
        newO[newKey] = value
      }
    }
  }
  return newO
}


export const removeDuplicates = (myArr, prop) => {
  return myArr.filter((obj, pos, arr) => {
    return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
  });
}

/** Gets the difference in days, month or year between two dates
  * @param  firstDate First Date
  * @param secDate Second Date
  * @param mode difference mode e.g: 'days', 'months' or 'years'
*/
export function getDateDiff(firstDate: string, secDate: string, mode: any, format: string) {
  const a = moment(firstDate, format);
  const b = moment(secDate, format);
  const test = a.diff(b, mode);
  return test + 1;
}

export function getLoggedUserData(): UserInfo {
  try {
    const userInfo = JSON.parse(localStorage.getItem('userInfo'))
    return JSON.parse(userInfo.returnText);
  } catch (error) {
    return null
  }
}

export const feet2String = (num) => {

  let decVal = '' + num + ''


  if (decVal === '') {
    return "0'"
  }

  let arr = decVal.split('.')

  if (arr.length > 1) {
    let feet = Math.floor(num)
    let inch = Math.round((num - feet) * 12)

    if (inch >= 12) {
      return ((feet + 1) + `'`)
    }
    return (Math.floor(num) + `'` + inch + `"`);
  } else {
    return (num + `'`);
  }
}


export const getJsonDepartureDays = ($jsonData: string, type: string) => {
  let strReturn: string = "";
  try {
    if (type === 'cut-off') {
      const jsonData: any = JSON.parse($jsonData);
      if (jsonData.JsonCutOffDays && jsonData.JsonCutOffDays.length > 1) {
        const arr: Array<any> = jsonData.JsonCutOffDays.split(",");
        arr.forEach(element => {
          strReturn += `${"<span class='text-capitalize'>" +
            element +
            "</span><br>"}`;
        });
      } else {
        strReturn = "N/A";
      }
    } else {
      const jsonData: any = JSON.parse($jsonData);
      if (jsonData.JsonDepartureDays && jsonData.JsonDepartureDays.length > 1) {
        const arr: Array<any> = jsonData.JsonDepartureDays.split(",");
        arr.forEach(element => {
          strReturn += `${"<span class='text-capitalize'>" +
            element +
            "</span><br>"}`;
        });
      } else {
        strReturn = "N/A";
      }
    }
  } catch (error) {
    strReturn = "N/A";
  }
  return strReturn;
}


export const BASIS_STR = {
  PER_BOOKING: 'Per Booking',
  PER_CBM: 'Per CBM',
  PER_CBM_PER_DAY: 'Per CBM Per Day',
  PER_CBM_PER_DAY_DED: 'Per CBM Per Day Dedicated',
  PER_CONTAINER: 'Per Container',
  PER_INVOICE: 'Per Invoice',
  PER_KG: 'Per KG',
  PER_MONTH: 'Per Month',
  PER_PLT_PER_DAY: 'Per Pallet Per Day',
  PER_PLT_PER_DAY_DED: 'Per Pallet Per Day Dedicated',
  PER_SHIPMENT: 'Per Shipment',
  PER_SQFT: 'Per Sqft',
  PER_SQFT_PER_DAY: 'Per Sqft Per Day',
  PER_SQFT_PER_DAY_DED: 'Per Sqft Per Day Dedicated',
  PER_SQM_PER_DAY: 'Per Sqm Per Day',
  PER_SQM_PER_DAY_DED: 'Per Sqm Per Day Dedicated',
  PER_TRUCK: 'Per Truck',
  PER_YEAR: 'Per Year',
}

export function getBasisStr(type): string {
  return BASIS_STR[type]
}


export function getProviderShippingCat() {
  let toReturn = []
  try {
    toReturn = JSON.parse(localStorage.getItem('shippingCategories'))
  } catch (error) {
    toReturn = []
  }
  return toReturn
}



export function getTotalContainerCBM(searchCriteria: any) {
  return searchCriteria.totalShipmentCMB
}

export function getTotalContainerWeight(searchCriteria: any) {
  return searchCriteria.totalVolumetricWeight
}

/** Call this function to get the counter of selected Container/Pallets Etc from FCL/LCL/LCL-Air
  * @param userContainerList The containerList in the searchCriteria
  * @param containerSpecCode The Container/Unit Code for the selected Container/Unit Type from the container List
*/
export function getContainersByType(userContainerList: Array<any>, containerSpecCode: string): number {
  let count = 0

  const unitList = userContainerList.filter(cont => cont.containerCode === containerSpecCode)
  unitList.forEach(pallet => {
    count += pallet.contRequestedQty
  })
  return count
}


/** Call this function to get the Movement Type in FCL/LCL/LCL-Air/GROUND/ETC
  * @param pickupPortType pickup transport mode
  * @param deliveryPortType delivery transport mode
*/
export function getMovementType(pickupPortType, deliveryPortType) {
  let portType = 'PortToPort'

  let strPick = 'Port'
  let strDrop = 'Port'


  switch (pickupPortType) {
    case 'AIR':
      strPick = 'Airport'
      break
    case 'GROUND':
      strPick = 'Ground'
      break
    case 'SEA':
      strPick = 'Port'
      break
    default:
      strPick = 'Port'
      break
  }

  switch (deliveryPortType) {
    case 'AIR':
      strDrop = 'Airport'
      break
    case 'GROUND':
      strDrop = 'Ground'
      break
    case 'SEA':
      strDrop = 'Port'
      break
    default:
      strDrop = 'Port'
      break
  }

  portType = strPick + 'To' + strDrop
  return portType
}

/** Gets the specified marker icon for w.r.t transport Mode
  * @param transMode Transport Mode
*/
export function getMarkerIcon(transMode) {
  let modeIcon = 'icon_pin_location.svg'
  switch (transMode) {
    case 'AIR':
      modeIcon = 'ic_air.svg'
      break;
    case 'LOCATION':
      modeIcon = 'ic_location.svg'
      break;
    case 'SEA':
      modeIcon = 'ic_ship.svg'
      break;
    case 'GROUND':
      modeIcon = 'ic_truck.svg'
      break;
    case 'GROUND_ANIME':
      modeIcon = 'ic_truck_animate.svg'
      break;
    case 'WAREHOUSE':
      modeIcon = 'ic_warehouse.svg'
      break;
    default:
      modeIcon = 'icon_pin_location.svg'
      break;
  }
  return modeIcon
}

/** Gets the specified normal grey icon for w.r.t transport Mode
  * @param transMode Transport Mode
*/

export function getGreyIcon(transMode) {
  let modeIcon = 'icon_pin_location.svg'
  switch (transMode) {
    case 'AIR':
      modeIcon = 'air-lcl-grey.svg'
      break;
    case 'LOCATION':
      modeIcon = 'Icons_Location_light_grey.svg'
      break;
    case 'SEA':
      modeIcon = 'icons_cargo_ship_grey.svg'
      break;
    case 'GROUND':
      modeIcon = 'icons_cargo_truck_grey.svg'
      break;
    default:
      modeIcon = 'Icons_Location_mid_grey.svg'
      break;
  }
  return modeIcon

}

/** Gets the specified animated grey icon for w.r.t transport Mode
  * @param transMode Transport Mode
*/

export function getAnimatedGreyIcon(transMode) {
  let x = '../../'
  let modeIcon
  switch (transMode) {
    case 'AIR':
      modeIcon = `
      <svg id="9da47147-2af0-4541-9126-fe489cb71022" class="ship-animate flag-animation" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 24 24">
        <defs>
          <style>
            .\37 14c9074-16c5-4f8a-8fba-4219870be0a1 {
              fill: #738593;
            }
          </style>
        </defs>
        <g fill="#a3afb8" id="4770d30b-5f7a-489b-b03f-e33ae4f209ee" data-name="plane">
          <path id="e028be17-d5a5-4255-9439-886e15d376f8" data-name="&lt;Path&gt;" class="714c9074-16c5-4f8a-8fba-4219870be0a1"
            d="M21.5,14H3.051a3,3,0,0,1-1.872-.656L0,12.4V10H21.5A2.292,2.292,0,0,1,24,12h0A2.292,2.292,0,0,1,21.5,14Z" />
          <polygon id="c0ae1538-9799-4465-bf1f-29ca5182e666" data-name="&lt;Path&gt;" class="714c9074-16c5-4f8a-8fba-4219870be0a1"
            points="5 11 0 11 0 7 2.474 7 5 11" />
          <g id="c44fbcaf-c122-4795-b609-52d2b77a1d55" data-name="&lt;Group&gt;">
            <polygon id="e514b53e-fa4f-4fa8-9244-c9ea5fd16ce8" data-name="&lt;Path&gt;" class="714c9074-16c5-4f8a-8fba-4219870be0a1"
              points="9.444 14 16.381 14 18 12 10.714 3 7 3 10.143 12 9.444 14" />
            <polygon id="54171f07-8ed6-44b5-9571-7bbf36f3d3c3" data-name="&lt;Path&gt;" class="714c9074-16c5-4f8a-8fba-4219870be0a1"
              points="8.746 16 7 21 10.714 21 14.762 16 8.746 16" />
          </g>
        </g>
      </svg>`
      break;
    case 'LOCATION':
      modeIcon = `
      <svg id="2968af2a-21f9-4f5b-8d14-9319a586596e" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13 16" class="icon-size-20 animated wobble icon-location">
        <defs>
        <style>
          .fill-color {
            fill: #b9c2c8;
          }
        </style>
        </defs>
        <path class="fill-color" d="M6.5,0A6.622,6.622,0,0,0,0,6.737C0,10.458,6.5,16,6.5,16S13,10.458,13,6.737A6.622,6.622,0,0,0,6.5,0Zm0,8a2,2,0,1,1,2-2A2,2,0,0,1,6.5,8Z"/>
      </svg>`

      break;
    case 'SEA':
      modeIcon = `
      <svg class="ship-animate" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
        <g fill="#a3afb8" data-name="ship2">
          <path data-name="&lt;Path&gt;" class="path-ship" d="M18.4,21H5.262a3,3,0,0,1-2.1-.862L0,17.029.042,15H24l-3.1,4.662A3,3,0,0,1,18.4,21Z" />
          <rect data-name="&lt;Rectangle&gt;" class="rect-1" x="2" y="11" width="6" height="3" />
          <rect data-name="&lt;Rectangle&gt;" class="rect-2" x="9" y="11" width="6" height="3" />
          <rect data-name="&lt;Rectangle&gt;" class="rect-5" x="9" y="7" width="6" height="3" />
          <rect data-name="&lt;Rectangle&gt;" class="rect-3" x="16" y="11" width="6" height="3" />
          <rect data-name="&lt;Rectangle&gt;" class="rect-4" x="2" y="7" width="6" height="3" />
          <rect data-name="&lt;Rectangle&gt;" class="rect-6" x="9" y="3" width="6" height="3" />
        </g>
      </svg>`
      break;
    case 'GROUND':
      modeIcon = `
        <svg id="f7905435-1486-49b3-baf9-4dca76d453f9" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" class="moveRightToLeft">
          <defs>
          <style>
            .a2024788-efba-4438-b0be-f26b285b4fd6 {
              fill: #b9c2c9;
            }
          </style>
          </defs>
           <g fill="#a3afb8" id="d1bd0fc5-aee7-4161-bcfc-9917552a4e20" data-name="truck">
            <path id="3b886d42-d469-4fa8-a176-d1a6f9535320" data-name="&lt;Path&gt;" class="a2024788-efba-4438-b0be-f26b285b4fd6" d="M21.5,14h-2c0-4-2.5-6.5-6.5-6.5v-2A8.138,8.138,0,0,1,21.5,14Z"/>
            <g id="dece99b0-ccfe-46cd-9e08-09c04af070eb" data-name="&lt;Group&gt;">
              <path id="1e109160-3fc6-427d-a960-9408ecfded4d" data-name="&lt;Compound Path&gt;" class="a2024788-efba-4438-b0be-f26b285b4fd6" d="M6,16a3.5,3.5,0,1,0,3.5,3.5A3.5,3.5,0,0,0,6,16Zm0,5.25A1.75,1.75,0,1,1,7.75,19.5,1.75,1.75,0,0,1,6,21.25Z"/>
            </g>
            <g id="0a8baa5b-ca15-44f3-b7a0-e022331802c0" data-name="&lt;Group&gt;">
              <g id="c0230789-57cf-47bc-a80f-830d7c300074" data-name="&lt;Group&gt;">
                <path id="b3ca8f69-77be-4bef-880a-c916143eb129" data-name="&lt;Path&gt;" class="a2024788-efba-4438-b0be-f26b285b4fd6" d="M14,3H3.24A3.24,3.24,0,0,0,0,6.24V19H4.331a1.739,1.739,0,0,1,3.337,0H14Z"/>
              </g>
            </g>
            <g id="e2c8b36e-a3d6-4f64-aece-7314231b6800" data-name="&lt;Group&gt;">
              <path id="ab7af316-2d2b-4ee2-8291-e72c5310f1ec" data-name="&lt;Compound Path&gt;" class="a2024788-efba-4438-b0be-f26b285b4fd6" d="M18,16a3.5,3.5,0,1,0,3.5,3.5A3.5,3.5,0,0,0,18,16Zm0,5.25a1.75,1.75,0,1,1,1.75-1.75A1.75,1.75,0,0,1,18,21.25Z"/>
            </g>
            <g id="589008c7-8e5d-4302-9187-3d291cab846d" data-name="&lt;Group&gt;">
              <path id="08fec5ba-4e9f-4b7f-b673-94d24fdb6a92" data-name="&lt;Compound Path&gt;" class="a2024788-efba-4438-b0be-f26b285b4fd6" d="M22,14v3H20.793a3.746,3.746,0,0,0-5.587,0H13.735l-.429-3H22m2-2H11l1,7h4.331a1.739,1.739,0,0,1,3.337,0H24V12Z"/>
            </g>
          </g>
        </svg>`
      break;
    default:
      modeIcon = `
         <svg id="2968af2a-21f9-4f5b-8d14-9319a586596e" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13 16" class="icon-size-20">
          <defs>
          <style>
            .fill-color {
              fill: #b9c2c8;
            }
          </style>
          </defs>
          <path class="fill-color" d="M6.5,0A6.622,6.622,0,0,0,0,6.737C0,10.458,6.5,16,6.5,16S13,10.458,13,6.737A6.622,6.622,0,0,0,6.5,0Zm0,8a2,2,0,1,1,2-2A2,2,0,0,1,6.5,8Z"/>
        </svg>`
      break;
  }
  return modeIcon

}

export function checkDuplicateInObject($propName, $inputArray: Array<any>): DuplicateResp {
  let seenDuplicate = false, testObject = {};
  let dupItem: Array<any> = []

  $inputArray.map((item) => {
    const itemPropertyName = item[$propName];
    if (itemPropertyName in testObject) {
      testObject[itemPropertyName].duplicate = true;
      item.duplicate = true;
      seenDuplicate = true;
      if (!(dupItem.includes(item[$propName]))) {
        dupItem.push(item[$propName])
      }
    }
    else {
      testObject[itemPropertyName] = item;
      // delete item.duplicate;
    }
  });

  return { res: seenDuplicate, payload: dupItem };
}

export function checkDuplicateLatLng($propA, $propB, $inputArray): DuplicateResp {
  let seenDuplicate = false, testObject = {};
  let dupItem = null

  $inputArray.map((item) => {
    const itemPropA = item[$propA];
    const itemPropB = item[$propB];
    if (itemPropA in testObject && itemPropB in testObject) {
      testObject[itemPropA].duplicate = true;
      // testObject[itemPropB].duplicate = true;
      item.duplicate = true;
      seenDuplicate = true;
      dupItem = item
    }
    else {
      testObject[$propA] = item;
      delete item.duplicate;
    }
  });

  return { res: seenDuplicate, payload: dupItem };
}

export function isInArray(value, array: Array<any>): boolean {
  return array.indexOf(value) > -1;
}

export interface DuplicateResp {
  res: boolean;
  payload: Array<any>;
}


export const getTimeStr = (minutes: number): any => {

  if (minutes && minutes > 0) {
    const decHour = Math.floor(minutes / 60)
    const modMin = (minutes % 60)
    const strTime: string = decHour + 'h ' + modMin + 'm'
    return strTime
  } else {
    return null
  }
}

export const DEFAULT_EMPTY_GRAPH: any = {
  color: ['#02bdb6', '#8472d5'],
  title: { text: 'No Data to Show', x: 'center', y: 'center' },
  tooltip: {
    trigger: 'axis',
    // formatter: '{b} <br> {c} ({d}%)',
    axisPointer: {
      type: 'shadow'
    },
    backgroundColor: ['rgba(255,255,255,1)'],
    padding: [20, 24],
    extraCssText: 'box-shadow: 0px 2px 20px 0px rgba(0, 0, 0, 0.2);',
    textStyle: {
      color: '#2b2b2b', //#738593
      decoration: 'none',
      fontFamily: 'Proxima Nova, sans-serif',
      fontSize: 16,
      //fontStyle: 'italic',
      //fontWeight: 'bold'
    }
  },
  legend: {
    data: [] //Hamza
  },
  grid: {
    left: '3%',
    right: '4%',
    bottom: '3%',
    containLabel: true
  },
  xAxis: [
    {
      type: 'category',
      data: [], //Hamza
      axisTick: {
        alignWithLabel: true
      }
    }
  ],
  yAxis: [
    {
      type: 'value',
      // type: 'category',
      // data: ['0', '1k', '2M', '4M', '10M']
    }
  ],
  series: [
    {
      name: '',
      type: 'bar',
      barGap: 0.1,
      barWidth: 10,
      itemStyle: {
        normal: {
          barBorderRadius: 15,
        }
      },
      data: [] //Hamza
    },
  ]
}

let locationIp: string = '0.0.0.0'
export const getDefaultIp = (): string => {
  return locationIp
}

export const setDefaultIp = (newIp) => {
  locationIp = newIp
}

export function validateName(name: string): string {
  let newString = ''
  const arr = name.split(' ')

  const totalLengt = arr.length
  for (let i = 0; i < totalLengt; i++) {
    const nextIndex = i + 1
    if (nextIndex < totalLengt && arr[nextIndex].length > 0) {
      newString = newString.concat(arr[i] + ' ')
    } else {
      newString = newString.concat(arr[i])
    }
  }

  return newString
}

export function getWarehouseBookingDates(cutOffDate): Array<MonthDateModel> {
  const beginDate = moment(cutOffDate).subtract(6, 'days').format()
  let dateArray = [];
  let startDate = moment(beginDate);
  let stopDate = moment(cutOffDate);
  while (startDate <= stopDate) {

    let isActive = true

    if (moment().diff(startDate, 'days') > 0) {
      isActive = false
    }

    const obj: MonthDateModel = {
      isActive: isActive,
      isSelected: false,
      pickupDate: moment(startDate).format('YYYY-MM-DD'),
    }
    dateArray.push(obj)
    startDate = moment(startDate).add(1, 'days');
  }
  return dateArray;
}

export function getAssayereBookingDates(cutOffDate): Array<MonthDateModel> {
  const _endDate = moment(cutOffDate).add(60, 'days').format()
  let dateArray = [];
  let startDate = moment(cutOffDate);
  let stopDate = moment(_endDate);
  while (startDate <= stopDate) {

    let isActive = true
    if (moment().diff(startDate, 'days') > 0) {
      isActive = false
    }
    const obj: MonthDateModel = {
      isActive: isActive,
      isSelected: false,
      pickupDate: moment(startDate).format('YYYY-MM-DD'),
    }
    dateArray.push(obj)
    startDate = moment(startDate).add(1, 'days');
  }
  return dateArray;
}

export interface MonthDateModel {
  isActive: boolean;
  isSelected: boolean;
  pickupDate: string;
}


export const defaultReadRights = {
  "IssuedByUserID": 139,
  "IssuedByUserName": "hashmove",
  "IssuedOn": "2021-01-14T12:49:36.4658583Z",
  "Validity": "2021-02-13T12:49:36.4658583Z",
  "JsonObjectRights": [
    {
      "ObjCode": "VI",
      "ObjName": "ViewInvoice",
      "ObjRights": "R"
    },
    {
      "ObjCode": "UD",
      "ObjName": "UploadDocument",
      "ObjRights": "R"
    },
    {
      "ObjCode": "BC",
      "ObjName": "BookingCancellation",
      "ObjRights": "R"
    },
    {
      "ObjCode": "SI",
      "ObjName": "SupplierInformation",
      "ObjRights": "R"
    },
    {
      "ObjCode": "AI",
      "ObjName": "AgentInformation",
      "ObjRights": "R"
    },
    {
      "ObjCode": "PBS",
      "ObjName": "ProviderBookingStatus",
      "ObjRights": "R"
    },
    {
      "ObjCode": "PUD",
      "ObjName": "ProviderUploadDocument",
      "ObjRights": "R"
    },
    {
      "ObjCode": "PAD",
      "ObjName": "ProviderApproveDocument",
      "ObjRights": "RW"
    },
    {
      "ObjCode": "PRD",
      "ObjName": "ProviderRejectDocument",
      "ObjRights": "RW"
    }
  ]
}

export const disabledRights = {
  "IssuedByUserID": 139,
  "IssuedByUserName": "hashmove",
  "IssuedOn": "2021-01-14T12:49:36.4658583Z",
  "Validity": "2021-02-13T12:49:36.4658583Z",
  "JsonObjectRights": [
    {
      "ObjCode": "VI",
      "ObjName": "ViewInvoice",
      "ObjRights": "R"
    },
    {
      "ObjCode": "UD",
      "ObjName": "UploadDocument",
      "ObjRights": "R"
    },
    {
      "ObjCode": "BC",
      "ObjName": "BookingCancellation",
      "ObjRights": "R"
    },
    {
      "ObjCode": "SI",
      "ObjName": "SupplierInformation",
      "ObjRights": "R"
    },
    {
      "ObjCode": "AI",
      "ObjName": "AgentInformation",
      "ObjRights": "R"
    },
    {
      "ObjCode": "PBS",
      "ObjName": "ProviderBookingStatus",
      "ObjRights": "R"
    },
    {
      "ObjCode": "PUD",
      "ObjName": "ProviderUploadDocument",
      "ObjRights": "R"
    },
    {
      "ObjCode": "PAD",
      "ObjName": "ProviderApproveDocument",
      "ObjRights": "R"
    },
    {
      "ObjCode": "PRD",
      "ObjName": "ProviderRejectDocument",
      "ObjRights": "R"
    }
  ]
}

export function getAlphaNumString(value) {
  if (value === undefined || value === null)
    return '';
  const _value = value.replace(/\s/g, "");
  return _value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
}

export function getPhoneNumber(value: string): string {
  if (value === undefined || value === null)
    return '';
  if (isNumeric(value))
    return value;
  const _value = value.replace(/\s/g, "");
  const _value2 = _value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
  const value3 = _value2.match(/[0-9]+/g)
  if (value3 && typeof value3 === 'object') {
    return value3.join("")
  }
  return value3 as any
}

export function getPriceInNumber(value) {
  if (value === undefined || value === null)
    return '';
  if (isNumeric(value))
    return value;
  const _value = value.replace(/\s/g, "");
  const value2 = _value.replace(/[`~!@#$%^&*()_|+\-=?;:'",<>\{\}\[\]\\\/]/gi, '');
  const value3 = value2.match(/[0-9]+/g)
  if (!value3 || parseFloat(value3) === NaN || parseFloat(value3) as any === 'NaN') {
    return ''
  } else {
    return parseFloat(value3)
  }
}

export function isNumeric(value) {
  return /^\d+$/.test(value);
}

export function getWHUnit(_searchCriteria) {
  const { searchBy } = _searchCriteria
  switch (searchBy) {
    case 'by_pallet':
      return 'PLT'
    case 'by_area':
      return _searchCriteria.AREA_UNIT
    case 'by_unit':
      return 'CBM'
    case 'by_vol_weight':
      return 'CBM'
    case 'by_container':
      return 'CONTAINER'
    default:
      return 'SQFT'
  }
}

export function getWhSpace(_searchCriteria: any) {
  return ((_searchCriteria.searchBy === 'by_unit' || _searchCriteria.searchBy === 'by_vol_weight') && (_searchCriteria.storageType === 'shared' || _searchCriteria.storageType === 'dedicated')) ? _searchCriteria.CBM : (_searchCriteria.searchBy === 'by_pallet') ? _searchCriteria.PLT : (_searchCriteria.searchBy === 'by_area' && _searchCriteria.AREA_UNIT === 'sqft') ? _searchCriteria.SQFT : _searchCriteria.AREA
}

export function getPriceInDecimal(value) {
  try {
    if (value === undefined || value === null)
      return '';

    const _value = value
    let value2 = _value.replace(/[`~!@#$%^&*()_|+\-=?;:'",<>\{\}\[\]\\\/]/gi, '');
    let _number;
    if (getCharacterCount(value2, '.') > 1) {
      const _numSplit = value2.split('.')
      value2 = _numSplit[0] + '.' + (_numSplit[1] ? _numSplit[1] : '0')
    }
    if (value2 && value2.includes('.') && value2.split('.').length > 0 && !value2.split('.')[1] && isNumeric(value2.split('.')[0])) {
      _number = value2
    } else {
      const _extracted = value2.match(/^[0-9]+(\.[0-9][0-9]?)?/gi)
      _number = _extracted ? _extracted.join() : _extracted
    }
    if (!_number || parseFloat(_number) === NaN || parseFloat(_number) as any === 'NaN') {
      return ''
    } else {
      return _number
    }
  } catch (error) {
    console.log(error)
  }
}

export function isNumericV2(value) {
  return typeof getNum(value) === 'number'
}

function getNum(_num) {
  return (_num) ? (parseFloat(_num) ? typeof parseFloat(_num) === 'number' ? parseFloat(_num) : 0 : 0) : 0
}

export function getPriceInDecimalNeg(value) {
  try {
    if (value === undefined || value === null)
      return '';

    const _value = value
    let value2 = _value.replace(/[`~!@#$%^&*()_|+\=?;:'",<>\{\}\[\]\\\/]/gi, '');
    let _number;
    if (getCharacterCount(value2, '.') > 1) {
      const _numSplit = value2.split('.')
      value2 = _numSplit[0] + '.' + (_numSplit[1] ? _numSplit[1] : '0')
    }
    if (value2 && value2.includes('.') && value2.split('.').length > 0 && !value2.split('.')[1] && isNumericV2(value2.split('.')[0])) {
      _number = value2
    } else {
      const hasMinus = value2.includes('-')
      const _cmpValue = value2.replace('-', '')
      const _extracted = _cmpValue.match(/^(-)?[0-9]+(\.[0-9]{1,7})?/gi)
      _number = _extracted ? _extracted.join() : _extracted
      if (hasMinus) {
        _number = '-' + (_number ? _number : '')
      }
    }
    if (!_number || parseFloat(_number) === NaN || parseFloat(_number) as any === 'NaN') {
      return ''
    } else {
      return _number
    }
  } catch (error) {
    console.log(error)
  }
}

export function getPriceInDecimalSvn(value) {
  try {
    if (value === undefined || value === null)
      return '';

    const _value = value
    let value2 = _value.replace(/[`~!@#$%^&*()_|+\-=?;:'",<>\{\}\[\]\\\/]/gi, '');
    let _number;
    if (getCharacterCount(value2, '.') > 1) {
      const _numSplit = value2.split('.')
      value2 = _numSplit[0] + '.' + (_numSplit[1] ? _numSplit[1] : '0')
    }
    if (value2 && value2.includes('.') && value2.split('.').length > 0 && !value2.split('.')[1] && isNumeric(value2.split('.')[0])) {
      _number = value2
    } else {
      const _extracted = value2.match(/^[0-9]+(\.[0-9]{1,7})?/gi)
      _number = _extracted ? _extracted.join() : _extracted
    }
    if (!_number || parseFloat(_number) === NaN || parseFloat(_number) as any === 'NaN') {
      return ''
    } else {
      return _number
    }
  } catch (error) {
    console.log(error)
  }
}

function getCharacterCount(_data, _key) {
  return _data.split(_key).length - 1;
}

export function isArrayValid($array: any[], $length: number) {
  return $array && $array.length > $length ? true : false
}

export const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
  one &&
  two &&
  two.year === one.year &&
  two.month === one.month &&
  two.day === one.day

export const before = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two
    ? false
    : one.year === two.year
      ? one.month === two.month
        ? one.day === two.day
          ? false
          : one.day < two.day
        : one.month < two.month
      : one.year < two.year

export const after = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two
    ? false
    : one.year === two.year
      ? one.month === two.month
        ? one.day === two.day
          ? false
          : one.day > two.day
        : one.month > two.month
      : one.year > two.year

export const toInteger = (value: any): number => (parseInt(`${value}`, 10))
export const isNumber = (value: any) => (!isNaN(toInteger(value)))

export function getRepeatingElements(arry) {
  const uniqueElements = new Set(arry);
  const filteredElements = arry.filter(item => {
    if (uniqueElements.has(item)) {
      uniqueElements.delete(item);
    } else {
      return item;
    }
  });
  return Array.from(new Set(filteredElements))
}

export function getQueryParams(url) {
  let queryParams = {};
  //create an anchor tag to use the property called search
  let anchor = document.createElement('a');
  //assigning url to href of anchor tag
  anchor.href = url;
  //search property returns the query string of url
  let queryStrings = anchor.search.substring(1);
  let params = queryStrings.split('&');

  for (var i = 0; i < params.length; i++) {
    var pair = params[i].split('=');
    queryParams[pair[0]] = decodeURIComponent(pair[1]);
  }
  return queryParams as any;
}

export function matchOtherValidator(otherControlName: string) {

  let thisControl: FormControl;
  let otherControl: FormControl;

  return function matchOtherValidate(control: FormControl) {

    if (!control.parent) {
      return null;
    }

    // Initializing the validator.
    if (!thisControl) {
      thisControl = control;
      otherControl = control.parent.get(otherControlName) as FormControl;
      if (!otherControl) {
        throw new Error('matchOtherValidator(): other control is not found in parent group');
      }
      otherControl.valueChanges.subscribe(() => {
        thisControl.updateValueAndValidity();
      });
    }

    if (!otherControl) {
      return null;
    }

    if (otherControl.value !== thisControl.value) {
      return {
        matchOther: true
      };
    }
    return null;
  }
}

export function getScreenHeight() {
  const { height } = window.screen
  if (height <= 900) {
    return '300px'
  } else if (height <= 1100) {
    return '560px'
  } else {
    return '660px'
  }
}

export function getRoundedNum(_value: number) {
  try {
    let finalValue = _value
    const rightSide = Math.floor(_value)
    let leftSide = _value - rightSide
    if (leftSide > 0) {
      if (leftSide >= 0.01 && leftSide <= 0.50) {
        leftSide = 0.0
      } else if (leftSide > 0.5 && leftSide <= 0.99) {
        leftSide = 1
      }
      finalValue = rightSide + leftSide
    }
    return finalValue
  } catch (error) {
    return _value
  }
}

export function getScreenHeightPayment() {
  const { height } = window.screen
  if (height <= 900) {
    return '365px'
  } else if (height <= 1100) {
    return '565px'
  } else {
    return '600px'
  }
}



export function setAccessRights(rights) {
  localStorage.setItem('acc-rght', base64Encode(rights))
}

export function getAccessRights(): IAccessRights {
  try {
    return JSON.parse(base64Decode(localStorage.getItem('acc-rght')))
  } catch {
    return null
  }
}

export function getActiveRoutes(): string {
  try {
    const _accessRoutes = getAccessRights().AppObject
    // console.log(_accessRoutes)
    const _activeRoutes = _accessRoutes.filter(_route =>
      (_route.ObjectURL && _route.ObjectType === 'MENU' || _route.ObjectType === 'SUB-MENU'))
      .map(_route => _route.ObjectURL.toLowerCase()).join(',')
    return _activeRoutes
  } catch (error) {
    console.log('accessError:', error)
    return ''
  }
}

export const getDeviceType = () => {
  const ua = navigator.userAgent;
  if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(ua)) {
    return "tablet"
  }
  if (
    /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(
      ua
    )
  ) {
    return "mobile"
  }
  return "desktop"
}

export const isMobile = () => getDeviceType() === 'mobile' ? true : false

export const PASSWORD_REGEX: RegExp = /(?=(.*[0-9]))(?=.*[^A-Za-z0-9])(?=.*[a-z])(?=(.*[A-Z]))(?=(.*)).{8,}/m;


export function validatePassword(enteredPassword: string) {
  const _length = /(.{8,})/
  const _upperCase = /(.*[A-Z])/
  const _lowerCase = /(.*[a-z])/
  const _digit = /(.*[0-9])/
  const _specialChar = /([^A-Za-z0-9])/

  let lengthValid = false, upperCaseValid = false, lowerCaseValid = false,
    digitValid = false, specialCharValid = false

  if (_length.test(enteredPassword)) lengthValid = true
  if (_upperCase.test(enteredPassword)) upperCaseValid = true
  if (_lowerCase.test(enteredPassword)) lowerCaseValid = true
  if (_digit.test(enteredPassword)) digitValid = true
  if (_specialChar.test(enteredPassword)) specialCharValid = true

  return {
    lengthValid, upperCaseValid, lowerCaseValid, digitValid, specialCharValid
  }
}
