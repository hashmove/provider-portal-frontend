import { environment } from '../../environments/environment';
import { IBiConfig, IHMPaymentTerms, INewFeatures } from '../services/jwt.injectable';

export let baseApi;
export let baseExternalAssets;
export var basesPartnerUrl = '';
export var portalOrigin = '';
export var isRRV1 = true
export var hmPaymentPolicy = false
export var hmPaymentTerms: IHMPaymentTerms = null
export var newFeatures: INewFeatures = null
export var biConfig: IBiConfig = {
    BI_HEIGHT: '3060px',
    BI_WIDTH: '983px'
}
export var helpDeskData = {
    title: 'Help Desk',
    desc: 'Live Support, Articles, Community',
    link: 'https://desk.zoho.com/portal/hashmovedesk/en/home'
}

export function setBaseApi(_baseApi: string) {
    baseApi = _baseApi
}

export function setBaseExternal(_baseExternalAssets: string) {
    baseExternalAssets = _baseExternalAssets
}

export function setBasePartner(_basePartnerUrl: string) {
    basesPartnerUrl = _basePartnerUrl
}

export function setPortalOrigin(_basePartnerUrl: string) {
    portalOrigin = _basePartnerUrl
}

export function setRRV1(_isRRV1: boolean) {
    isRRV1 = _isRRV1
}

export function setHMPaymentPolicy(_hmPaymentPolicy: boolean) {
    hmPaymentPolicy = _hmPaymentPolicy
}

export function setHMPaymentTerm(_hmPaymentTerms: IHMPaymentTerms) {
    hmPaymentTerms = _hmPaymentTerms
}

export function setNewFeature(_newFeatures: INewFeatures) {
    newFeatures = _newFeatures
}

export function setBiConfig(_biConfig: IBiConfig) {
    biConfig = _biConfig
}

export function setHelpDeskData(_helpDeskData: any) {
    helpDeskData = _helpDeskData
}

if (environment.qa) {
    // QA URL
    baseApi = "http://10.20.1.13:8091/api/";
    baseExternalAssets = "http://10.20.1.13:8091";
    // baseApi = "http://10.20.1.13:7091/api/";
    // baseExternalAssets = "http://10.20.1.13:7091";

}
else if (environment.dev) {
    // DEV URL
    baseApi = "http://10.20.1.13:9091/api/";
    baseExternalAssets = "http://10.20.1.13:9091";
}
else if (environment.uat) {
    // UAT URL
    baseApi = "http://10.20.1.13:7091/api/";
    baseExternalAssets = "http://10.20.1.13:7091";

}
else if (environment.prod) {
    // PROD URL
    // baseApi = "http://partner.hashmove.com:81/api/";
    // baseExternalAssets = "http://partner.hashmove.com:81";

    baseApi = "https://betaapi.hashmove.com/api/";
    baseExternalAssets = "https://betaapi.hashmove.com";
    // baseApi = "https://betademoapi.hashmove.com/api/";
    // baseExternalAssets = "https://betademoapi.hashmove.com";
}
else {
    // PERSONAL URL
    baseApi = "http://10.20.1.53/api/";
    baseExternalAssets = "http://10.20.1.53";
}


