import { Directive, ElementRef, HostListener } from '@angular/core';
import { NgControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { getPriceInDecimal } from '../constants/globalFunctions';
@Directive({
    selector: '[priceDecDir]',
    host: { '(input)': '$event' }
})
export class PriceDecimalDirective {

    lastValue: string;

    private subscription: Subscription;
    constructor(
        public ref: ElementRef,
        private ngControl: NgControl
    ) { }

    @HostListener('input', ['$event']) onInput($event) {
        const ctrl = this.ngControl.control;
        var start = $event.target.selectionStart;
        var end = $event.target.selectionEnd;
        const _value = getPriceInDecimal($event.target.value);
        $event.target.value = _value;
        $event.target.setSelectionRange(start, end);
        $event.preventDefault();

        if (!this.lastValue || (this.lastValue && $event.target.value.length > 0 && this.lastValue !== $event.target.value)) {
            this.ngControl.control.setValue(_value);
        }
    }

    // @HostListener('input', ['$event']) onInput($event) {
    //     const ctrl = this.ngControl.control;
    //     var start = $event.target.selectionStart;
    //     var end = $event.target.selectionEnd;
    //     const _value = getPriceInDecimal($event.target.value)
    //     $event.target.value = _value
    //     $event.target.setSelectionRange(start, end);
    //     $event.preventDefault();

    //     if (!this.lastValue || (this.lastValue && $event.target.value.length > 0 && this.lastValue !== $event.target.value)) {
    //         this.lastValue = this.ref.nativeElement.value = _value;

    //         const evt = document.createEvent('HTMLEvents');
    //         evt.initEvent('input', false, true);
    //         $event.target.dispatchEvent(evt);
    //         // setTimeout(() => {
    //         // }, 0);
    //         this.ngControl.valueAccessor.writeValue(_value);
    //     }
    // }

    // @HostListener('ngModelChange', ['$event'])
    // ngModelChange(value: any) {
    //     const _value = getPriceInDecimal(value)
    //     this.ngControl.valueAccessor.writeValue(_value);
    // }
    ngOnDestroy() {
    }
}

