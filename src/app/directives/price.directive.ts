import { Directive, ElementRef, HostListener } from '@angular/core';
import { NgControl } from '@angular/forms';
import { getPriceInNumber } from '../constants/globalFunctions';
@Directive({
    selector: '[priceDir]',
    host: { '(input)': '$event' }
})
export class PriceDirective {

    lastValue: string;

    constructor(
        public ref: ElementRef,
        private ngControl: NgControl
    ) { }

    @HostListener('input', ['$event']) onInput($event) {
        var start = $event.target.selectionStart;
        var end = $event.target.selectionEnd;
        const _value = getPriceInNumber($event.target.value);
        $event.target.value = _value;
        $event.target.setSelectionRange(start, end);
        $event.preventDefault();

        if (!this.lastValue || (this.lastValue && $event.target.value.length > 0 && this.lastValue !== $event.target.value)) {
            this.ngControl.control.setValue(_value);
        }
    }
    ngOnDestroy() { }
}
