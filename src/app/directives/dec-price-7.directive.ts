import { Directive, ElementRef, HostListener } from '@angular/core';
import { NgControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { getPriceInDecimal, getPriceInDecimalSvn } from '../constants/globalFunctions';
@Directive({
    selector: '[priceDecDirSvn]',
    host: { '(input)': '$event' }
})
export class PriceDecimalSvnDirective {

    lastValue: string;
    constructor(
        public ref: ElementRef,
        private ngControl: NgControl
    ) { }

    @HostListener('input', ['$event']) onInput($event) {
        const ctrl = this.ngControl.control;
        var start = $event.target.selectionStart;
        var end = $event.target.selectionEnd;
        const _value = getPriceInDecimalSvn($event.target.value);
        $event.target.value = _value;
        $event.target.setSelectionRange(start, end);
        $event.preventDefault();
        // 
        if (!this.lastValue || (this.lastValue && $event.target.value.length > 0 && this.lastValue !== $event.target.value)) {
            this.ngControl.control.setValue(_value);
        }
    }
    ngOnDestroy() {
    }
}

