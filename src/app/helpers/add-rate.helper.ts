import { untilDestroyed } from 'ngx-take-until-destroy'
import { Observable } from 'rxjs'
import { debounceTime, map } from 'rxjs/operators'
import { ManageRatesService } from '../components/pages/user-desk/manage-rates/manage-rates.service'
export class AddRateHelpers {

    public static MASTER_PARTNER_LIST: IPartner[] = []
    public selectedPartner: IPartner = null
    public partnersList: IPartner[] = []
    public partnerFilterList: { code: any, title: string, id: number }[] = []

    setCurrentPartner(partnerId) {
        if (partnerId) {
            this.selectedPartner = this.partnersList.filter(_partner => _partner.id === partnerId)[0]
        }
    }

    getAirPartners(id, partnerId, _airFreightService: ManageRatesService) {
        if (AddRateHelpers.MASTER_PARTNER_LIST && AddRateHelpers.MASTER_PARTNER_LIST.length > 0 && partnerId) {
            this.partnersList = AddRateHelpers.MASTER_PARTNER_LIST
            this.setCurrentPartner(partnerId)
        } else {
            _airFreightService.getAirPartnersById(id).pipe(untilDestroyed(this)).subscribe((res: any) => {
                if (res && typeof res === 'object') {
                    AddRateHelpers.MASTER_PARTNER_LIST = res
                    this.partnersList = res
                    if (partnerId) {
                        this.setCurrentPartner(partnerId)
                    }
                }
            }, (err: any) => {
            })
        }
    }

    _partners = (text$: Observable<string>) =>
        text$.pipe(
            debounceTime(200),
            map(term => (!term || term.length < 2) ? []
                : this.partnersList.filter(v => v.title && v.title.toLowerCase().indexOf(term.toLowerCase()) > -1))
        )

    formatter2 = (_partner: { title: string, id: number }) => {
        if (this.partnerFilterList.filter(_part => _part.id === _partner.id).length === 0) {
            this.partnerFilterList.push(_partner as any)
        }
        return
    };

    removePartner(_partner: { title: string, id: number }) {
        this.partnerFilterList = this.partnerFilterList.filter(_part => _part.id !== _partner.id)
    }
}

export interface IPartner {
    id: number
    code: string
    title: string
    shortName: string
    imageName?: any
    desc: string
    webURL?: any
    sortingOrder: number
    type?: any
    lastUpdate?: any
    latitude?: any
    longitude?: any
    isBaseCurrency?: any
    jsonServices?: any
    isChinaGateway?: any
    shortPortCode?: any
    portCountryCode?: any
    roundingOff?: any
}
