import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { HeaderComponent } from '../../shared/header/header.component';
import { FooterComponent } from '../../shared/footer/footer.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmLogoutDialogComponent } from '../../shared/dialogues/confirm-logout-dialog/confirm-logout-dialog.component';
import { UserCreationService } from './user-creation/user-creation.service';
import { BookingInvoiceComponent } from './user-desk/booking-invoice/booking-invoice.component';
import { ReUploadDocComponent } from '../../shared/dialogues/re-upload-doc/re-upload-doc.component';
import { BasicInfoService } from './user-creation/basic-info/basic-info.service';
import { DiscardDraftComponent } from '../../shared/dialogues/discard-draft/discard-draft.component';
import { ConfirmDeleteDialogComponent } from '../../shared/dialogues/confirm-delete-dialog/confirm-delete-dialog.component';
import { SeaRateDialogComponent } from '../../shared/dialogues/sea-rate-dialog/sea-rate-dialog.component';
import { BookingStatusUpdationComponent } from '../../shared/dialogues/booking-status-updation/booking-status-updation.component';
import { AirRateDialogComponent } from '../../shared/dialogues/air-rate-dialog/air-rate-dialog.component';

import { RateHistoryComponent } from '../../shared/dialogues/rate-history/rate-history.component';
import { RateValidityComponent } from '../../shared/dialogues/rate-validity/rate-validity.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedModule } from '../../shared/shared.module'
import { PipeModule } from '../../constants/pipe/pipe.module';
import { UploadComponent } from '../../shared/upload-component/upload-component';
import { CountrySelect } from '../../shared/country-select/country-select.component';
import { ConfirmDialogGenComponent } from '../../shared/dialogues/confirm-dialog-generic/confirm-dialog-generic.component';
import { ScrollbarModule } from 'ngx-scrollbar';
import { ConfirmUpdateDialogComponent } from '../../shared/dialogues/confirm-update/confirm-update-dialog.component';
import { MultiAirlinesComponent } from './user-desk/multi-airlines/multi-airlines.component';
import { CertificateChargesComponent } from './user-desk/certificate-charges/certificate-charges.component';
import { WarehouseRateDialogComponent } from '../../shared/dialogues/warehouse-rate-dialog/warehouse-rate-dialog.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Interceptor } from '../../http-interceptors/interceptor';
import { AssayerContainerInfoComponent } from './user-desk/assayer-container-info/assayer-container-info.component';
import { AssayerUpdateScheduleComponent } from './user-desk/assayer-update-schedule/assayer-update-schedule.component';
import { NguCarouselModule } from '@ngu/carousel';
import { AssayerWarhouseTableComponent } from './user-desk/assayer-warehouse-table/assayer-warehouse-table.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { NewsDialogComponent } from '../../shared/dialogues/news-dialog/news-dialog.component';
import { RateExpieryDialogComponent } from '../../shared/dialogues/rate-expiery-dialog/rate-expiery-dialog.component';
import { ProviderOfficeDialogComponent } from '../../shared/dialogues/provider-office-dialog/provider-office-dialog.component';
import { TermsConditDialogComponent } from '../../shared/dialogues/terms-condition/terms-condition.component';
import { QuillEditorModule } from 'ngx-quill-editor';
import { RequestAddRateDialogComponent } from '../../shared/dialogues/request-add-rate-dialog/request-add-rate-dialog.component';
import { RequestPriceDetailsComponent } from './user-desk/request-price-details/request-price-details.component';
import { RequestStatusUpdationComponent } from '../../shared/dialogues/request-status-updation/request-status-updation.component';
import { RequestUnitCalc } from '../../shared/dialogues/request-unit-calc/request-unit-calc.component';
import { PaymentTermsDialogComponent } from '../../shared/dialogues/payment-terms/payment-terms.component';
// import { NewFeatureAlertComponent } from '../../shared/dialogues/new-feature-alert/new-feature-alert.component';
@NgModule({
  imports: [
    CommonModule,
    PagesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    NgSelectModule,
    SharedModule,
    PipeModule,
    ScrollbarModule,
    NguCarouselModule,
    NgxPaginationModule,
    QuillEditorModule
  ],
  declarations: [
    PagesComponent,
    HeaderComponent,
    FooterComponent,
    // LoginDialogComponent,
    // ForgotPasswordComponent,
    // UpdatePasswordComponent,
    ConfirmLogoutDialogComponent,
    BookingInvoiceComponent,
    RequestPriceDetailsComponent,
    RequestUnitCalc,
    AssayerContainerInfoComponent,
    AssayerUpdateScheduleComponent,
    AssayerWarhouseTableComponent,
    ReUploadDocComponent,
    DiscardDraftComponent,
    ConfirmDeleteDialogComponent,
    ProviderOfficeDialogComponent,
    NewsDialogComponent,
    ConfirmDialogGenComponent,
    // NewFeatureAlertComponent,
    TermsConditDialogComponent,
    PaymentTermsDialogComponent,
    SeaRateDialogComponent,
    RequestAddRateDialogComponent,
    BookingStatusUpdationComponent,
    RequestStatusUpdationComponent,
    AirRateDialogComponent,
    RateHistoryComponent,
    RateValidityComponent,
    UploadComponent,
    ConfirmUpdateDialogComponent,
    MultiAirlinesComponent,
    UploadComponent,
    CertificateChargesComponent,
    WarehouseRateDialogComponent,
    // CancelBookingDialogComponent,
    // ConfirmModifySearchComponent,
    // ShareshippingComponent,
    // ConfirmDeleteAccountComponent,
    // ConfirmBookingDialogComponent,
    // CountrySelect,
    RateExpieryDialogComponent
  ],
  entryComponents: [
    // LoginDialogComponent,
    // ForgotPasswordComponent,
    // UpdatePasswordComponent,
    ConfirmLogoutDialogComponent,
    BookingInvoiceComponent,
    RequestPriceDetailsComponent,
    RequestUnitCalc,
    AssayerContainerInfoComponent,
    AssayerUpdateScheduleComponent,
    AssayerWarhouseTableComponent,
    ReUploadDocComponent,
    DiscardDraftComponent,
    ConfirmDeleteDialogComponent,
    ProviderOfficeDialogComponent,
    NewsDialogComponent,
    ConfirmDialogGenComponent,
    // NewFeatureAlertComponent,
    TermsConditDialogComponent,
    PaymentTermsDialogComponent,
    SeaRateDialogComponent,
    RequestAddRateDialogComponent,
    BookingStatusUpdationComponent,
    RequestStatusUpdationComponent,
    AirRateDialogComponent,
    RateHistoryComponent,
    RateValidityComponent,
    UploadComponent,
    // CountrySelect,
    ConfirmUpdateDialogComponent,
    MultiAirlinesComponent,
    UploadComponent,
    CertificateChargesComponent,
    WarehouseRateDialogComponent,
    RateExpieryDialogComponent
  ],
  providers: [
    UserCreationService,
    BasicInfoService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    }
  ]
})
export class PagesModule { }
