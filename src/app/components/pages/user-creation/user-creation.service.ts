import { Injectable } from '@angular/core';
import { baseApi } from '../../../constants/base.url';
import { HttpClient, HttpParams } from "@angular/common/http";
import { getLoggedUserData } from '../../../constants/globalFunctions';
import { SharedService } from '../../../services/shared.service';
import { getDefaultHMUser } from '../../../services/jwt.injectable';

@Injectable()
export class UserCreationService {

  constructor(
    private _http: HttpClient,
    private _sharedService: SharedService,
  ) { }

  getlabelsDescription(page) {
    let url: string = `languagetranslate/LanguageDictionary/[En-US]/[Ar-AE]/${page}`;
    return this._http.get(baseApi + url);
  }

  getLatlng(country) {
    const params = new HttpParams()
      .set('address', country)
      .set('key', 'AIzaSyBs4tYXYGUA2kDvELgCYcbhYeoVgZCxumg');
    let url: string = `https://maps.googleapis.com/maps/api/geocode/json`;
    return this._http.get(url, { params });
  }


  userLogin(data) {
    let url = "usersprovider/ValidateProvider"
    return this._http.post(baseApi + url, data);
  }

  userLoginV2(data) {
    let url = "usersprovider/ValidateProviderLogin"
    return this._http.post(baseApi + url, data);
  }

  userLogOut(data) {
    let url = "users/UserLogout"
    return this._http.post(baseApi + url, data);
  }
  userforgetpassword(data) {
    let url = "usersprovider/ForgotPassword"
    return this._http.post(baseApi + url, data);
  }
  userupdatepassword(data) {
    let url = "usersprovider/UpdatePassword"
    return this._http.put(baseApi + url, data);
  }

  getJwtToken() {
    return localStorage.getItem('token');
  }

  saveJwtToken(token) {
    localStorage.setItem('token', token);
  }

  saveRefreshToken(refreshToken) {
    localStorage.setItem('refreshToken', refreshToken);
  }

  getRefreshToken() {
    return localStorage.getItem('refreshToken');
  }
  getUserProfileStatus(userID) {
    let url = `usersprovider/GetUserProfileStatus/${userID}`;
    return this._http.get(baseApi + url)
  }


  revalidateToken(body) {
    let url = "token/ResetJWT"
    return this._http.post(baseApi + url, body);
  }

  guestLoginService(body) {
    // const url = 'token/CreateGuestJWT'
    const url = 'token/GuestLogin'
    return this._http.post(baseApi + url, body);
  }

  async logoutAction() {
    let userObj = JSON.parse(localStorage.getItem("userInfo"));
    if (userObj) {
      let loginData = JSON.parse(userObj.returnText);
      loginData.IsLogedOut = true;
      userObj.returnText = JSON.stringify(loginData);
      localStorage.setItem("userInfo", JSON.stringify(userObj));

      const data = {
        PrimaryEmail: loginData.PrimaryEmail,
        UserLoginID: loginData.UserLoginID,
        LogoutDate: new Date().toLocaleString(),
        LogoutRemarks: ""
      }

      try {
        await this.userLogOut(data)
      } catch (error) { }

      this._sharedService.dashboardDetail.next(null);
      this._sharedService.IsloggedIn.next(loginData.IsLogedOut);
      return null
    }

  }

  saveUserDocument(data) {
    const loginUser = getLoggedUserData()
    let _extras: any = null
    try {
      const { city, country, query, region, countryCode } = this._sharedService.getMapLocation()
      _extras = {
        CityName: city,
        CountryName: country,
        IpAddress: query,
        RegionName: region,
        CountryCode: countryCode
      }
    } catch (error) { }
    const toSend = {
      ...data,
      LoginUserID: (loginUser && loginUser.UserID) ? loginUser.UserID : -1,
      IsProvider: true,
      EmailTo: loginUser.PrimaryEmail,
      JsonUploadSource: JSON.stringify(_extras)
    }
    toSend.UploadedBy = 'PROVIDER'
    let url: string = "document/Post";
    return this._http.post(baseApi + url, toSend);
  }

  getAllUsers(userId) {
    let url = "users/GetUserListByAdmin?userID=" + userId;
    return this._http.get(baseApi + url);
  }


  makeAdmin(adminID: any, userID: any) {
    let url =
      "users/UpdateUserAsAdmin?updatedByUserID=" +
      adminID +
      "&userID=" +
      userID;
    return this._http.put(baseApi + url, {});
  }

  removeAdmin(adminID, userID) {
    let url =
      "users/UpdateUserAsNonAdmin?updatedByUserID=" +
      adminID +
      "&userID=" +
      userID;
    return this._http.put(baseApi + url, {});
  }


  getCompanyAdmin(companyId) {
    const url = `users/GetCompanyAdmin/${companyId}`
    return this._http.get(baseApi + url);
  }

  userRegistration(data) {
    let url = "users/post"
    return this._http.post(baseApi + url, data);
  }

  addPartner(data, userid) {
    let url = `Partner/Post`
    return this._http.post(baseApi + url, data);
  }

  editPartner(data, userid) {
    let url = `Partner/Put/${userid}`
    return this._http.put(baseApi + url, data);
  }

  getPartnerDtl(userid) {
    let url = `Partner/Get/${userid}`
    return this._http.get(baseApi + url);
  }


  resendEmail(userId) {
    let url = "users/ResendEmail/" + userId;
    return this._http.get(baseApi + url);
  }


  getHashmovePartnerSettings() {
    let url = "general/GetHashmovePartnerSettings";
    return this._http.get(baseApi + url);
  }

  getProviderDetails(id) {
    let url: string = "provider/GetProviderDetailByProviderID/" + id;
    return this._http.get(baseApi + url);
  }

  userDelete(deletingUserID, deleteByUserID) {
    let url = "users/DeleteAccountRequest/" + deletingUserID + '/' + deleteByUserID
    return this._http.delete(baseApi + url);
  }

  getUserValidity(id) {
    let url: string = `users/GetUserValidity/${id}`;
    return this._http.get(baseApi + url);
  }

  activateUser(data) {
    const url = `users/ActivateUser`
    return this._http.put(baseApi + url, data);
  }

  getUserRoles(roleID) {
    const _url = `approle/GetRoleList/${roleID}`
    return this._http.get(baseApi + _url)
  }

  getPortalUserData(userID) {
    const _url = `users/Get/${userID}`
    return this._http.get(baseApi + _url)
  }

  updateRegUser(loginUserId, data) {
    const _url = `users/UpdateUser/${loginUserId}`
    return this._http.put(baseApi + _url, data)
  }
}


export interface JWTObj {
  token: string
  refreshToken: string
}
