import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RegistrationComponent } from './registration-form/registration.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { OtpconfirmationComponent } from './otpconfirmation/otpconfirmation.component';
import { CreatePasswordComponent } from './create-password/create-password.component';
import { BasicInfoService } from './basic-info.service';
import { BusinessInfoComponent } from './business-info/business-info.component';
import { NgFilesModule } from '../../../../directives/ng-files';
import { UiSwitchModule } from 'ngx-toggle-switch';
import { QuillEditorModule } from 'ngx-quill-editor';
import { SharedModule } from '../../../../shared/shared.module';
import { Interceptor } from '../../../../http-interceptors/interceptor';
import { LoginDialogComponent } from '../../../../shared/dialogues/login-dialog/login-dialog.component';
import { UpdatePasswordComponent } from '../../../../shared/dialogues/update-password/update-password.component';
import { ForgotPasswordComponent } from '../../../../shared/dialogues/forgot-password/forgot-password.component';
import { ShowUrlDialogComponent } from '../../../../shared/dialogues/show-url/show-url-dialog.component';
import { PipeModule } from '../../../../constants/pipe/pipe.module';

@NgModule({
  imports: [
    CommonModule,
    NgFilesModule,
    HttpClientModule,
    QuillEditorModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDZ31Nq5gJ_cCvENKL79eeC6x4zBv9oOGU',
      libraries: ["places", "geometry"]
    }),
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    UiSwitchModule,
    SharedModule,
    PipeModule
  ],
  declarations: [
    RegistrationComponent,
    LoginPageComponent,
    OtpconfirmationComponent,
    CreatePasswordComponent,
    BusinessInfoComponent,
    LoginDialogComponent,
    ForgotPasswordComponent,
    UpdatePasswordComponent,
    ShowUrlDialogComponent
  ],
  entryComponents: [
    LoginDialogComponent,
    ForgotPasswordComponent,
    UpdatePasswordComponent,
    ShowUrlDialogComponent
  ],
  providers: [
    BasicInfoService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    }
  ]
})
export class BasicInfoModule { }
