import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef, NgZone, state, ChangeDetectorRef, AfterViewChecked, AfterViewInit, ɵConsole } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { } from 'googlemaps';
import { UserCreationService } from '../../user-creation.service';
import { SharedService } from '../../../../../services/shared.service';
import { HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { CommonService } from '../../../../../services/common.service';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import {
  loading, CustomValidator, ValidateEmail, EMAIL_REGEX, leapYear, patternValidator,
  YOUTUBE_REGEX, FACEBOOK_REGEX, TWITTER_REGEX, LINKEDIN_REGEX, INSTAGRAM_REGEX, URL_REGEX, GEN_URL
} from '../../../../../constants/globalFunctions';
import { BasicInfoService } from '../basic-info.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router'
import { Base64 } from 'js-base64';
import { UserInfo } from '../../../../../interfaces/billing.interface';
import { of } from 'rxjs/observable/of';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit, AfterViewInit, AfterViewChecked {
  public debounceInput: Subject<string> = new Subject();
  public requiredFields: string = "This field is required";
  public requiredFieldsOthrLng: string = "هذه الخانة مطلوبه";
  public Globalinputfrom: any;
  public Globalinputto: any
  public serviceIds: any[] = [];
  public serviceOffered: any;
  public selectedjobTitle: any
  private selectedLangIdbyCountry: any;
  public jobTitles: any
  public transLangEmail: any;

  public selected_country: any;
  public countryList: any;
  public cityList: any = [];
  public countryFlagImage: string;
  public transPhoneCode: string;
  public activePhone: any;
  public activeTransPhone: any;
  public phoneCountryId: any;
  public mobileCountryId: any
  public phoneCode;

  public mobileCountFlagImage: string;
  public mobileCode;
  public transmobileCode;

  public orgNameError: boolean;
  public transorgNameError: boolean;
  public activeOrgName: boolean;
  public activeTransOrgName: boolean;
  public phoneError: boolean;
  public translangPhoneError: boolean;

  public businessForm: any;
  public personalInfoForm: any;

  public addressAr: any;
  public addressArError: boolean;
  public addressError: boolean;


  public cityAr: any;
  public cityArError: boolean;
  public cityError: boolean;

  public poBoxError: boolean;
  public poBoxArError: boolean;
  public poBoxAr: any;

  public firstNameError: boolean;
  public transfirstNameError: boolean;
  public activeFirstName: any;
  public activeTransFirstName: any;

  public translastNameError: boolean;
  public lastNameError: boolean;
  public activeLastName: any;
  public activeTransLastName: any;

  public jobTitleError: boolean;
  public transjobTitleError: boolean;
  public activejobTitle: any;
  public activeTransjobTitle: any;


  public transEmailError: boolean;
  public EmailError: boolean;
  public telephoneError: boolean;
  public translangtelephoneError: boolean;
  public activetelephone: boolean;
  public activeTransTelephone: boolean;


  public selectedSocialsite: any;
  public socialSites;
  public socialInputValidate;
  public socialLink: ISocialAccount[] = []
  public location: any = {
    lat: undefined,
    lng: undefined
  }
  public zoomlevel: number = 5;
  public arabicNumbers: any = [
    { baseNumber: '0', arabicNumber: '۰' },
    { baseNumber: '1', arabicNumber: '۱' },
    { baseNumber: '2', arabicNumber: '۲' },
    { baseNumber: '3', arabicNumber: '۳' },
    { baseNumber: '4', arabicNumber: '۴' },
    { baseNumber: '5', arabicNumber: '۵' },
    { baseNumber: '6', arabicNumber: '۶' },
    { baseNumber: '7', arabicNumber: '۷' },
    { baseNumber: '8', arabicNumber: '۸' },
    { baseNumber: '9', arabicNumber: '۹' }
  ]
  public onRegistrationForm: boolean = false;
  public showTranslatedLangSide: boolean = false;

  //terms And Cond
  public term: boolean = false;
  lockEmail: boolean = false
  hasEmailSet: boolean = false

  linkedInUrl_error: boolean = false
  fbUrl_error: boolean = false
  twitterUrl_error: boolean = false
  instaUrl_error: boolean = false
  youtubeUrl_error: boolean = false
  webUrl_error: boolean = false


  linkedInUrl: string = null
  fbUrl: string = null
  twitterUrl: string = null
  instaUrl: string = null
  youtubeUrl: string = null
  webUrl: string = null

  constructor(
    private _toastr: ToastrService,
    private _userCreationService: UserCreationService,
    private _commonService: CommonService,
    private _sharedService: SharedService,
    private mapsAPILoader: MapsAPILoader,
    private _router: Router,
    private ngZone: NgZone,
    private cdRef: ChangeDetectorRef,
    private _basicInfoService: BasicInfoService,
  ) { }

  marketUser: UserInfo
  needDefCountry = true

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }
  ngOnInit() {
    try {
      this.marketUser = this._sharedService.marketingUser
      // console.log(this.marketUser)
    }
    catch (err) { }
    this.getsocialList();
    this.getCompanyActivities();
    const { marketUser } = this
    // console.log(marketUser)
    this.businessForm = new FormGroup({
      orgName: new FormControl((marketUser && marketUser.CompanyName) ? marketUser.CompanyName : null, [Validators.required, Validators.maxLength(100), Validators.minLength(4), Validators.pattern(/[a-zA-Z]/)]),
      phone: new FormControl((marketUser && marketUser.PhoneNumer) ? marketUser.PhoneNumer : '', [Validators.required, Validators.pattern(/^(?!(\d)\1+(?:\1+){0}$)\d+(\d+){0}$/), Validators.minLength(7), Validators.maxLength(13)]),
      address: new FormControl((marketUser && marketUser.AddressLine1) ? marketUser.AddressLine1 : null, [Validators.required, Validators.maxLength(200), Validators.minLength(10), Validators.pattern(/^(?=.*?[a-zA-Z])[^%*$=+^<>}{]+$/)]),
      city: new FormControl((marketUser && marketUser.CityID) ? marketUser.CityID : null, [Validators.required, Validators.maxLength(100), Validators.minLength(3), Validators.pattern(/^(?=.*?[a-zA-Z])[^%*$=+^<>}{]+$/)]),
      poBoxNo: new FormControl(null, [Validators.maxLength(16), Validators.minLength(4)]),
      socialUrl: new FormControl((marketUser && marketUser.SocialAccountName) ? marketUser.SocialAccountName : null),

      linkedInUrl: new FormControl(null, [Validators.pattern(LINKEDIN_REGEX)]),
      fbUrl: new FormControl(null, [Validators.pattern(FACEBOOK_REGEX)]),
      twitterUrl: new FormControl(null, [Validators.pattern(TWITTER_REGEX)]),
      instaUrl: new FormControl(null, [Validators.pattern(INSTAGRAM_REGEX)]),
      youtubeUrl: new FormControl(null, [Validators.pattern(YOUTUBE_REGEX)]),
      webUrl: new FormControl(null, [Validators.pattern(GEN_URL)]),

      transLangOrgName: new FormControl('', [CustomValidator.bind(this), Validators.maxLength(100), Validators.minLength(2)]),
      transLangPhone: new FormControl('', [CustomValidator.bind(this), Validators.minLength(7), Validators.maxLength(13)]),
      transAddress: new FormControl(null, [CustomValidator.bind(this), Validators.maxLength(200), Validators.minLength(10), Validators.pattern(/^(?=.*?[a-zA-Z])[^%*$=+^<>}{]+$/)]),
      transCity: new FormControl(null, [CustomValidator.bind(this), Validators.maxLength(100), Validators.minLength(3), Validators.pattern(/^(?=.*?[a-zA-Z])[^%*$=+^<>}{]+$/)]),
      poBoxNoAr: new FormControl(null, [Validators.maxLength(16), Validators.minLength(4)]),
    });
    // console.log((marketUser && marketUser.PrimaryEmail) ? marketUser.PrimaryEmail : null)
    this.personalInfoForm = new FormGroup({
      firstName: new FormControl((marketUser && marketUser.FirstNameBL) ? marketUser.FirstNameBL : null, [Validators.required, Validators.pattern(/[a-zA-Z-][a-zA-Z -]*$/), Validators.minLength(2), Validators.maxLength(100)]),
      transLangfirstName: new FormControl(null, [CustomValidator.bind(this), Validators.minLength(2), Validators.maxLength(100)]),
      lastName: new FormControl((marketUser && marketUser.LastNameBL) ? marketUser.LastNameBL : null, [Validators.required, Validators.pattern(/[a-zA-Z-][a-zA-Z -]*$/), Validators.minLength(2), Validators.maxLength(100)]),
      transLanglastName: new FormControl(null, [CustomValidator.bind(this), Validators.minLength(2), Validators.maxLength(100)]),
      jobTitle: new FormControl((marketUser && marketUser.JobTitleBL) ? marketUser.JobTitleBL : '', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]),
      transLangjobTitle: new FormControl('', [CustomValidator.bind(this), Validators.minLength(3), Validators.maxLength(100)]),
      email: new FormControl((marketUser && marketUser.PrimaryEmail) ? marketUser.PrimaryEmail.toLowerCase() : '', [
        Validators.required,
        Validators.pattern(EMAIL_REGEX),
        Validators.maxLength(320)
      ]),
      transLangEmail: new FormControl(null, [
        CustomValidator.bind(this),
        Validators.pattern(EMAIL_REGEX),
        Validators.maxLength(320)
      ]),
      telephone: new FormControl((marketUser && marketUser.PrimaryPhoneBL) ? marketUser.PrimaryPhoneBL : null, [Validators.required, Validators.pattern(/^(?!(\d)\1+(?:\1+){0}$)\d+(\d+){0}$/), Validators.minLength(7), Validators.maxLength(13)]),
      transLangtelephone: new FormControl(null, [CustomValidator.bind(this), Validators.minLength(7), Validators.maxLength(13)]),
    });
    // console.log(this.businessForm)
    // console.log(this.personalInfoForm)
    if (marketUser && marketUser.PrimaryEmail) {
      this.hasEmailSet = true
    }
    this.personalInfoForm.valueChanges.subscribe(val => {
      if (!val.email && marketUser && marketUser.PrimaryEmail) {
        this.personalInfoForm.controls.email.patchValue(marketUser.PrimaryEmail)
      }
    });


    this._sharedService.countryList.subscribe((state: any) => {
      if (state) {
        this.countryList = state;
        // console.log(this.marketUser)
        if (this.marketUser) {
          this.setCountryBasedData(this.marketUser.CountryID)
        }
        // let selectedCountry = this.countryList.find(obj => obj.id == this.userProfile.CountryID);
        // this.selectPhoneCode(selectedCountry);
      }

    });
    this._sharedService.cityList.subscribe((state: any) => {
      if (state) {
        this.cityList = state;
      }
    });

    this._sharedService.getLocation.subscribe((state: any) => {
      if (state && state.country) {
        let obj = {
          title: state.country
        };
        if (this.needDefCountry) {
          this.getMapLatlng(obj);
        }
      }
    })

  }

  socialError() {
    if (this.businessForm.controls.linkedInUrl.status === "INVALID" && this.businessForm.controls.linkedInUrl.touched) {
      this.linkedInUrl_error = true;
    }
    if (this.businessForm.controls.fbUrl.status === "INVALID" && this.businessForm.controls.fbUrl.touched) {
      this.fbUrl_error = true;
    }
    if (this.businessForm.controls.twitterUrl.status === "INVALID" && this.businessForm.controls.twitterUrl.touched) {
      this.twitterUrl_error = true;
    }
    if (this.businessForm.controls.instaUrl.status === "INVALID" && this.businessForm.controls.instaUrl.touched) {
      this.instaUrl_error = true;
    }
    if (this.businessForm.controls.youtubeUrl.status === "INVALID" && this.businessForm.controls.youtubeUrl.touched) {
      this.youtubeUrl_error = true;
    }
    if (this.businessForm.controls.webUrl.status === "INVALID" && this.businessForm.controls.webUrl.touched) {
      this.webUrl_error = true;
    }
  }

  opneSocialLink(url) {
    if (url) {
      let _url = url
      if (!(_url.includes('http'))) {
        _url = 'http://' + url
      }
      window.open(_url, '_blank')
    }
  }

  ngAfterViewInit() {
    try {
      if (this.marketUser) {

      }
    } catch (error) {

    }
  }
  getsocialList() {
    this._basicInfoService.socialList().subscribe((res: any) => {
      if (res.returnStatus == "Success") {
        this.socialLink = res.returnObject;
        this.selectedSocialsite = this.socialLink[this.socialLink.length - 1];
        if (this.selectedSocialsite.socialMediaPortalsID === 105) {
          this.businessForm.controls['socialUrl'].setValidators([patternValidator(GEN_URL)]);
        }
        const { socialLink } = this

        this.socialLinks = socialLink.map(_social => {
          return {
            id: _social.socialMediaPortalsID,
            type: _social.socialMediaCode,
            title: _social.title,
            link: '',
          }
        })
      }
    })
  }

  getListJobTitle(id) {
    this._basicInfoService.getjobTitles(id).subscribe((res: any) => {
      if (res.returnStatus == 'Success') {
        this.jobTitles = res.returnObject;
        // this.businessForm.reset();
        // this.personalInfoForm.reset();
      }
    }, (err: HttpErrorResponse) => {
      console.log(err);
    })
  }

  getMapLatlng(country) {
    this._userCreationService.getLatlng(country.title).subscribe((res: any) => {
      if (res.status == "OK") {
        this.location = res.results[0].geometry.location;
        if (country.id) {
          this.setCountryBasedData(country.title)
        }
      }
    }, (err: HttpErrorResponse) => {
      console.log(err);
    })
  }

  setCountryBasedData(data) {
    let selectedCountry
    if (data && typeof data === 'number') {
      selectedCountry = this.countryList.find(obj => obj.id == data);
      // console.log(selectedCountry)
      this.selectCountry(selectedCountry)
    } else {
      selectedCountry = this.countryList.find(obj => obj.title.toLowerCase() == data.toLowerCase());
    }
    // this.cityList.forEach(element => {
    //   if (element.desc[0].CountryName === selectedCountry.title) {
    //     this.cityAr = element
    //   }
    // });
    this.selected_country = selectedCountry
    this.selectedLangIdbyCountry = selectedCountry.desc[0].LanguageID;
    this.selectPhoneCode(selectedCountry);
    this.selectTelCode(selectedCountry);
    this.countryWiseMaping(selectedCountry);
    this.getListJobTitle(selectedCountry.id);
  }

  countryWiseMaping(country) {
    if (country && country.id) {
      // this.showTranslatedLangSide = (country.desc[0].RegionCode === 'MET') ? true : false;
      this.transLangEmail = '';
      this.selectedjobTitle = undefined;
      // this.businessForm.reset();
      // this.personalInfoForm.reset();
      // this.getlabelsDescription();
      this.onRegistrationForm = true;
    }
    else {
      this.onRegistrationForm = false;
    }
  }
  selectCountry(country) {
    // console.log(country);

    if (country && country.id) {
      this.needDefCountry = false
      this.getMapLatlng(country);
    }
  }
  selectedSocialLink(obj) {
    this.selectedSocialsite = obj;
    // this.businessForm.controls['socialUrl'].reset();
    if (obj.socialMediaPortalsID === 100) {
      this.businessForm.controls['socialUrl'].setValidators([patternValidator(FACEBOOK_REGEX)]);
      this.socialLinkValidate()
    }
    else if (obj.socialMediaPortalsID === 101) {
      this.businessForm.controls['socialUrl'].setValidators([patternValidator(TWITTER_REGEX)]);
      this.socialLinkValidate()
    } else if (obj.socialMediaPortalsID === 102) {
      this.businessForm.controls['socialUrl'].setValidators([patternValidator(INSTAGRAM_REGEX)]);
      this.socialLinkValidate()
    } else if (obj.socialMediaPortalsID === 103) {
      this.businessForm.controls['socialUrl'].setValidators([patternValidator(LINKEDIN_REGEX)]);
      this.socialLinkValidate()
    } else if (obj.socialMediaPortalsID === 104) {
      this.businessForm.controls['socialUrl'].setValidators([patternValidator(YOUTUBE_REGEX)]);
      this.socialLinkValidate()
    }
    else {
      this.businessForm.controls['socialUrl'].setValidators([patternValidator(GEN_URL)]);
      this.socialLinkValidate()
    }
  }
  socialLinkValidate() {

    if (this.selectedSocialsite && this.businessForm.controls['socialUrl'].value && this.businessForm.controls['socialUrl'].status === 'INVALID') {
      // let index = this.selectedSocialsite.title.toLowerCase().indexOf(this.socialSites.toLowerCase());
      this.socialInputValidate = 'Your social url is not valid';
    }
    else if (!this.businessForm.controls['socialUrl'].value) {
      this.socialInputValidate = '';
    }
    else {
      this.socialInputValidate = '';
    }

  }

  selCountry
  selCountryMob

  selCountryAr
  selCountryMobAr

  selectPhoneCode(country) {
    this.countryFlagImage = country.code;
    let description = country.desc;
    this.phoneCode = description[0].CountryPhoneCode;
    this.transPhoneCode = description[0].CountryPhoneCode_OtherLang;
    this.phoneCountryId = country.id
    this.selCountry = country
  }
  onSelectedCity(obj) {
    if (obj && typeof obj == "object") {
      let description = obj.desc;
      let selectedCountry = this.countryList.find(obj => obj.id == description[0].CountryID);
      if (selectedCountry && typeof selectedCountry == 'object' && Object.keys(selectedCountry).length) {
        let desc = selectedCountry.desc;
        this.countryFlagImage = selectedCountry.code;
        this.mobileCountFlagImage = selectedCountry.code;
        this.phoneCountryId = selectedCountry.id;
        this.mobileCountryId = selectedCountry.id;
        this.mobileCode = desc[0].CountryPhoneCode;
        this.phoneCode = desc[0].CountryPhoneCode;
        this.transPhoneCode = desc[0].CountryPhoneCode_OtherLang;
        this.transmobileCode = desc[0].CountryPhoneCode_OtherLang;
        this.selCountry = selectedCountry
        this.selCountryMob = selectedCountry
      }
    }
    return;
  }
  selectTelCode(country) {
    this.mobileCountFlagImage = country.code;
    let description = country.desc;
    this.mobileCode = description[0].CountryPhoneCode;
    this.transmobileCode = description[0].CountryPhoneCode_OtherLang;
    this.mobileCountryId = country.id
    this.selCountryMob = country
  }
  onModelChange(fromActive, currentActive, $controlName, source, target, $value) {
    if (!this.showTranslatedLangSide) return;
    setTimeout(() => {
      if (currentActive && !fromActive && $value) {
        this.Globalinputfrom = false;
      }
      if (fromActive == false && currentActive && this.businessForm.controls[$controlName].errors || this.Globalinputto) {
        this._commonService.translatedLanguage(source, target, $value).subscribe((res: any) => {
          this.businessForm.controls[$controlName].patchValue(res.data.translations[0].translatedText);
          // console.log('yolo')
          this.Globalinputto = true;
        })
      }
      else if ($value && currentActive && source && target && fromActive == undefined) {
        this._commonService.translatedLanguage(source, target, $value).subscribe((res: any) => {
          this.businessForm.controls[$controlName].patchValue(res.data.translations[0].translatedText);
          // console.log('yolo')
        })
      }
      // else if(currentActive && !$value){
      //   this.fromActive(fromActive);
      // }
    }, 100)
  }

  onTransModel(fromActive, currentActive, $controlName, $value) {
    if (!this.showTranslatedLangSide) return;
    if (currentActive && !fromActive && $value) {
      this.Globalinputto = false;
    }
    if (currentActive && fromActive == false && this.businessForm.controls[$controlName].errors || this.Globalinputfrom) {
      this.debounceInput.next($value);
      this.debounceInput.pipe(debounceTime(400), distinctUntilChanged()).subscribe(value => {
        this._commonService.detectedLanguage(value).subscribe((res: any) => {
          let sourceLang = res.data.detections[0][0].language;
          let target = "en";
          if (sourceLang && target && value) {
            this._commonService.translatedLanguage(sourceLang, target, value).subscribe((res: any) => {
              this.businessForm.controls[$controlName].patchValue(res.data.translations[0].translatedText);
              this.Globalinputfrom = true;
              // console.log('yolo')
            })
          }
        })
      });
    }
    else if (currentActive && $value && fromActive == undefined) {
      if (!this.showTranslatedLangSide) return;
      this.debounceInput.next($value);
      this.debounceInput.pipe(debounceTime(400), distinctUntilChanged()).subscribe(value => {
        this._commonService.detectedLanguage(value).subscribe((res: any) => {
          let sourceLang = res.data.detections[0][0].language;
          let target = "en";
          // if (sourceLang && target && value) {
          //   this.onModelChange(fromActive, currentActive, $controlName, sourceLang, target, value);
          // }
          if (sourceLang && target && value) {
            this._commonService.translatedLanguage(sourceLang, target, value).subscribe((res: any) => {
              this.businessForm.controls[$controlName].patchValue(res.data.translations[0].translatedText);
              // console.log('yolo')
            })
          }
        })
      });
    }

  }

  onNameModelChange(fromActive, currentActive, $controlName, source, target, $value) {
    if (!this.showTranslatedLangSide) return;
    setTimeout(() => {
      if (currentActive && !fromActive && $value) {
        this.Globalinputfrom = false;
      }
      if (fromActive == false && currentActive && this.personalInfoForm.controls[$controlName].errors || this.Globalinputto) {
        this._commonService.translatedLanguage(source, target, $value).subscribe((res: any) => {
          this.personalInfoForm.controls[$controlName].patchValue(res.data.translations[0].translatedText);
          // console.log('yolo')
          this.Globalinputto = true;
        })
      }
      else if ($value && currentActive && source && target && fromActive == undefined) {
        this._commonService.translatedLanguage(source, target, $value).subscribe((res: any) => {
          this.personalInfoForm.controls[$controlName].patchValue(res.data.translations[0].translatedText);
          // console.log('yolo')

        })
      }
      // else if(currentActive && !$value){
      //   this.fromActive(fromActive);
      // }
    }, 100)
  }

  onTransNameModel(fromActive, currentActive, $controlName, $value) {
    if (!this.showTranslatedLangSide) return;
    if (currentActive && !fromActive && $value) {
      this.Globalinputto = false;
    }
    if (currentActive && fromActive == false && this.personalInfoForm.controls[$controlName].errors || this.Globalinputfrom) {
      this.debounceInput.next($value);
      this.debounceInput.pipe(debounceTime(400), distinctUntilChanged()).subscribe(value => {
        this._commonService.detectedLanguage(value).subscribe((res: any) => {
          let sourceLang = res.data.detections[0][0].language;
          let target = "en";
          if (sourceLang && target && value) {
            this._commonService.translatedLanguage(sourceLang, target, value).subscribe((res: any) => {
              this.personalInfoForm.controls[$controlName].patchValue(res.data.translations[0].translatedText);
              this.Globalinputfrom = true;
              // console.log('yolo')
            })
          }
        })
      });
    }
    else if (currentActive && $value && fromActive == undefined) {
      this.debounceInput.next($value);
      this.debounceInput.pipe(debounceTime(400), distinctUntilChanged()).subscribe(value => {
        this._commonService.detectedLanguage(value).subscribe((res: any) => {
          let sourceLang = res.data.detections[0][0].language;
          let target = "en";
          // if (sourceLang && target && value) {
          //   this.onModelChange(fromActive, currentActive, $controlName, sourceLang, target, value);
          // }
          if (sourceLang && target && value) {
            this._commonService.translatedLanguage(sourceLang, target, value).subscribe((res: any) => {
              this.personalInfoForm.controls[$controlName].patchValue(res.data.translations[0].translatedText);
              // console.log('yolo')
            })
          }
        })
      });
    }

  }


  NumberValid(evt) {
    let charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }
  getCompanyActivities() {
    this._basicInfoService.getServiceOffered().subscribe((res: any) => {
      if (res.returnStatus == 'Success') {
        try {
          this.serviceOffered = res.returnObject.map(_service => { return { ..._service, isSelected: false } })
        } catch {
          this.serviceOffered = res.returnObject;
        }
        try {
          if (this.marketUser) {
            this.serviceOffered.forEach(_freight => {
              if (_freight.logServName.toLowerCase().includes('freight')) {
                _freight.isSelected = true
                this.serviceIds.push(_freight.logServID);
                // console.log('yolo')
              }
            });
            // console.log(this.serviceOffered)
          }
        } catch (error) {
          console.log(error)
        }

      }
    }, (err: HttpErrorResponse) => {
      console.log(err);
    })

  }
  serviceSelection(obj, selectedService) {
    let selectedItem = selectedService.classList;
    if (this.serviceIds && this.serviceIds.length) {
      for (var i = 0; i < this.serviceIds.length; i++) {
        if (this.serviceIds[i] == obj.logServID) {
          this.serviceIds.splice(i, 1);
          // selectedItem.remove('active');
          obj.isSelected = false
          return;
        }
      }

    }
    if ((this.serviceIds && !this.serviceIds.length) || (i == this.serviceIds.length)) {
      // selectedItem.add('active');
      obj.isSelected = true
      this.serviceIds.push(obj.logServID);
    }
  }
  errorValidate() {
    if (this.businessForm.controls.orgName.status == "INVALID" && this.businessForm.controls.orgName.dirty) {
      this.orgNameError = true;
      this.transorgNameError = true;
    }
    if (this.businessForm.controls.transLangOrgName.status == "INVALID" && this.businessForm.controls.transLangOrgName.dirty) {
      this.transorgNameError = true;
      this.orgNameError = true;
    }
    if (this.businessForm.controls.address.status == "INVALID" && this.businessForm.controls.address.dirty) {
      this.addressError = true;
      this.addressArError = true;
    }
    if (this.businessForm.controls.transAddress.status == "INVALID" && this.businessForm.controls.transAddress.dirty) {
      this.addressArError = true;
      this.addressError = true;
    }

    if (this.businessForm.controls.city.status == "INVALID" && this.businessForm.controls.city.dirty) {
      this.cityError = true;
      this.cityArError = true;
    }
    if (this.businessForm.controls.transCity.status == "INVALID" && this.businessForm.controls.transCity.dirty) {
      this.cityError = true;
      this.cityArError = true;
    }
    if (this.businessForm.controls.poBoxNo.status == "INVALID" && this.businessForm.controls.poBoxNo.dirty) {
      this.poBoxError = true;
      this.poBoxArError = true;
    }
    if (this.businessForm.controls.poBoxNoAr.status == "INVALID" && this.businessForm.controls.poBoxNoAr.dirty) {
      this.poBoxError = true;
      this.poBoxArError = true;
    }

    if (this.businessForm.controls.phone.status == "INVALID" && this.businessForm.controls.phone.dirty) {
      this.phoneError = true;
      this.translangPhoneError = true;
    }
    if (this.businessForm.controls.transLangPhone.status == "INVALID" && this.businessForm.controls.transLangPhone.dirty) {
      this.phoneError = true;
      this.translangPhoneError = true;
    }
    if (this.personalInfoForm.controls.firstName.status == "INVALID" && this.personalInfoForm.controls.firstName.dirty) {
      this.firstNameError = true;
      this.transfirstNameError = true;
    }
    if (this.personalInfoForm.controls.transLangfirstName.status == "INVALID" && this.personalInfoForm.controls.transLangfirstName.dirty) {
      this.transfirstNameError = true;
      this.firstNameError = true;
    }
    if (this.personalInfoForm.controls.lastName.status == "INVALID" && this.personalInfoForm.controls.lastName.dirty) {
      this.lastNameError = true;
      this.translastNameError = true;
    }
    if (this.personalInfoForm.controls.transLanglastName.status == "INVALID" && this.personalInfoForm.controls.transLanglastName.dirty) {
      this.translastNameError = true;
      this.lastNameError = true;
    }
    if (this.personalInfoForm.controls.jobTitle.status == "INVALID" && this.personalInfoForm.controls.jobTitle.dirty) {
      this.jobTitleError = true;
      this.transjobTitleError = true;
    }
    if (this.personalInfoForm.controls.transLangjobTitle.status == "INVALID" && this.personalInfoForm.controls.transLangjobTitle.dirty) {
      this.transjobTitleError = true;
      this.jobTitleError = true;
    }
    if (this.personalInfoForm.controls.telephone.status == "INVALID" && this.personalInfoForm.controls.telephone.dirty) {
      this.telephoneError = true;
      this.translangtelephoneError = true;
    }
    if (this.personalInfoForm.controls.transLangtelephone.status == "INVALID" && this.personalInfoForm.controls.transLangtelephone.dirty) {
      this.translangtelephoneError = true;
      this.telephoneError = true;
    }
    if (this.personalInfoForm.controls.email.status == "INVALID" && this.personalInfoForm.controls.email.dirty) {
      this.EmailError = true;
      this.transEmailError = true;
    }
    if (this.personalInfoForm.controls.transLangEmail.status == "INVALID" && this.personalInfoForm.controls.transLangEmail.dirty) {
      this.transEmailError = true;
      this.EmailError = true;
    }
  }
  onModelPhoneChange(fromActive, currentActive, $controlName, $value) {
    if (!this.showTranslatedLangSide) return;
    if (currentActive && !fromActive) {
      let number = $value.split('');
      for (let i = 0; i < number.length; i++) {
        this.arabicNumbers.forEach((obj, index) => {
          if (number[i] == obj.baseNumber) {
            number.splice(i, 1, obj.arabicNumber)
          }
        })
      }
      this.businessForm.controls[$controlName].patchValue(number.reverse().join(''));
      // console.log('yolo')
    }
  }

  onModelTransPhoneChange(fromActive, currentActive, $controlName, $value) {
    if (!this.showTranslatedLangSide) return;
    if (currentActive && !fromActive) {
      let number = $value.split('');
      for (let i = 0; i < number.length; i++) {
        this.arabicNumbers.forEach((obj, index) => {
          if (number[i] == obj.baseNumber || number[i] == obj.arabicNumber) {
            number.splice(i, 1, obj.baseNumber)
          }
        })

      }
      this.businessForm.controls[$controlName].patchValue(number.join(''));
      // console.log('yolo')
    }

  }


  onModelTelephoneChange(fromActive, currentActive, $controlName, $value) {
    if (!this.showTranslatedLangSide) return;
    if (currentActive && !fromActive) {
      let number = $value.split('');
      for (let i = 0; i < number.length; i++) {
        this.arabicNumbers.forEach((obj, index) => {
          if (number[i] == obj.baseNumber) {
            number.splice(i, 1, obj.arabicNumber)
          }
        })
      }
      this.personalInfoForm.controls[$controlName].patchValue(number.reverse().join(''));
      // console.log('yolo')
    }
  }

  onModelTransTelephoneChange(fromActive, currentActive, $controlName, $value) {
    if (!this.showTranslatedLangSide) return;
    if (currentActive && !fromActive) {
      let number = $value.split('');
      for (let i = 0; i < number.length; i++) {
        this.arabicNumbers.forEach((obj, index) => {
          if (number[i] == obj.baseNumber || number[i] == obj.arabicNumber) {
            number.splice(i, 1, obj.baseNumber)
          }
        })

      }
      this.personalInfoForm.controls[$controlName].patchValue(number.join(''));
      // console.log('yolo')
    }

  }

  onModeljobtitle(fromActive, currentActive, $controlName, source, target, $value) {
    if (!this.showTranslatedLangSide) return;
    setTimeout(() => {
      if (typeof this.selectedjobTitle == 'object') return;
      if ($value && currentActive && source && target && !fromActive) {
        this._commonService.translatedLanguage(source, target, $value).subscribe((res: any) => {
          this.personalInfoForm.controls[$controlName].patchValue(res.data.translations[0].translatedText);
          // console.log('yolo')
          let obj = {
            baseLanguage: $value,
            otherLanguage: res.data.translations[0].translatedText,
          }
          this.selectedjobTitle = obj
        })
      }

    }, 200)
  }
  onTransModeljobTitle(fromActive, currentActive, $controlName, $value) {
    if (!this.showTranslatedLangSide) return;
    setTimeout(() => {
      if (typeof this.selectedjobTitle == 'object') return;
      if (currentActive && $value && !fromActive) {
        this.debounceInput.next($value);
        this.debounceInput.pipe(debounceTime(400), distinctUntilChanged()).subscribe(value => {
          this._commonService.detectedLanguage(value).subscribe((res: any) => {
            let sourceLang = res.data.detections[0][0].language;
            let target = "en";
            if (sourceLang && target && value) {
              this._commonService.translatedLanguage(sourceLang, target, value).subscribe((res: any) => {
                this.personalInfoForm.controls[$controlName].patchValue(res.data.translations[0].translatedText);
                // console.log('yolo')
                let obj = {
                  baseLanguage: res.data.translations[0].translatedText,
                  otherLanguage: $value
                }
                this.selectedjobTitle = obj
              })
            }
          })
        });
      }
    }, 200)
  }


  oneSpaceHandler(event) {
    if (event.target.value) {
      var end = event.target.selectionEnd;
      if (event.keyCode == 32 && (event.target.value[end - 1] == " " || event.target.value[end] == " ")) {
        event.preventDefault();
        return false;
      }
    }
    else if (event.target.selectionEnd == 0 && event.keyCode == 32) {
      return false;
    }
  }
  spaceHandler(event) {
    if (this.hasEmailSet) {
      event.preventDefault();
      return false
    }
    if (event.charCode == 32) {
      event.preventDefault();
      return false;
    }
  }

  textValidation(event) {
    const pattern = /^[a-zA-Z0-9_]*$/;
    let inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      if (event.charCode == 0) {
        return true;
      }
      if (event.target.value) {
        var end = event.target.selectionEnd;
        if (event.charCode == 32 && (event.target.value[end - 1] == " " || event.target.value[end] == " ")) {
          event.preventDefault();
          return false;
        }
        else if (event.charCode == 32 && !pattern.test(inputChar)) {
          return true;
        }
        else {
          return false;
        }
      }
      else {
        return false;
      }
    }
    else {
      return true;
    }
  }

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => (!term || term.length < 3) ? []
        : this.countryList.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1))
    )
  formatter = (x: { title: string }) => x.title;

  jobSearch = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => (!term || term.length < 2) ? []
        : this.jobTitles.filter(v => v.baseLanguage.toLowerCase().indexOf(term.toLowerCase()) > -1))
    )
  formatterjob = (x: { baseLanguage: string }) => x.baseLanguage;

  jobSearchOtherLng = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map((term: string) => (!term || term.length < 3) ? []
        : this.jobTitles.filter(v => v.baseLanguage.toLowerCase().indexOf(term.toLowerCase()) > -1 || (v.otherLanguage && v.otherLanguage.indexOf(term) > -1))))


  formatterjobOtherLng = (x: { otherLanguage: string }) => x.otherLanguage;





  searchCity = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .map(term => (!term || term.length < 3) ? []
        : this.cityList.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1));
  formatterCity = (x: { title: string }) => x.title;


  registration() {
    loading(true);
    let valid: boolean = ValidateEmail(this.personalInfoForm.value.email);
    if (this.businessForm.invalid || this.personalInfoForm.invalid || !this.serviceIds.length) {
      loading(false);
      (!this.serviceIds.length) ? this._toastr.error('Please select company activities') : this._toastr.error('Some thing missing', 'Failed');
      return;
    }
    else if (!valid) {
      this._toastr.error('Invalid email entered.', 'Failed');
      loading(false);
      return
    }

    if (!this.businessForm.value.city || !this.businessForm.value.city.id) {
      this._toastr.warning('Please select city from Dropdown')
      loading(false);
      return
    }

    let UserObjectOL: any = {}
    let CompanyObjectOL: any = {}

    let UserObjectBL = {
      primaryEmail: this.personalInfoForm.value.email,
      firstName: this.personalInfoForm.value.firstName,
      lastName: this.personalInfoForm.value.lastName,
      primaryPhone: this.personalInfoForm.value.telephone,
      countryPhoneCode: this.mobileCode,
      phoneCodeCountryID: this.mobileCountryId,
      jobTitle: (typeof this.selectedjobTitle === "object") ? this.selectedjobTitle.baseLanguage : this.personalInfoForm.value.jobTitle,
      sourceID: -1,
      sourceType: 'PROVIDER_SIGNUP'
    }

    if (this.showTranslatedLangSide) {
      UserObjectOL = {
        languageID: this.selectedLangIdbyCountry,
        firstName: this.personalInfoForm.value.transLangfirstName,
        lastName: this.personalInfoForm.value.transLanglastName,
        jobTitle: (typeof this.selectedjobTitle === "object") ? this.selectedjobTitle.otherLanguage : this.personalInfoForm.value.transLangjobTitle,
        primaryPhone: this.personalInfoForm.value.transLangtelephone,
        countryPhoneCode: this.transmobileCode,
        phoneCodeCountryID: this.mobileCountryId,
      }
      CompanyObjectOL = {
        companyNameOL: this.businessForm.value.transLangOrgName,
        companyPhone: this.businessForm.value.transLangPhone,
        POBox: (this.businessForm.value.poBoxNoAr) ? this.businessForm.value.poBoxNoAr : null,
        City: this.businessForm.value.transCity.id,
        languageID: this.selectedLangIdbyCountry,
        countryPhoneCode: this.transPhoneCode,
        phoneCodeCountryID: this.phoneCountryId
      }
    }

    let CompanyObjectBL = {
      companyName: this.businessForm.value.orgName,
      companyAddress: this.businessForm.value.address,
      companyPhone: this.businessForm.value.phone,
      poBox: (this.businessForm.value.poBoxNo) ? this.businessForm.value.poBoxNo : null,
      city: this.businessForm.value.city.id,
      countryPhoneCode: this.phoneCode,
      phoneCodeCountryID: this.phoneCountryId
    }

    try {
      if (this.businessForm.value.socialUrl && this.businessForm.value.socialUrl.length > 1) {
        this.addSocial(this.businessForm.value.socialUrl)
      }
    } catch {

    }

    try {
      this.socialLinks.forEach(_social => {
        if (_social.type === 'LNKN' && this.businessForm.controls.linkedInUrl.status !== 'INVALID') {
          _social.link = this.businessForm.value.linkedInUrl
        }
        if (_social.type === 'FCBK' && this.businessForm.controls.fbUrl.status !== 'INVALID') {
          _social.link = this.businessForm.value.fbUrl
        }
        if (_social.type === 'TWTR' && this.businessForm.controls.twitterUrl.status !== 'INVALID') {
          _social.link = this.businessForm.value.twitterUrl
        }
        if (_social.type === 'INST' && this.businessForm.controls.instaUrl.status !== 'INVALID') {
          _social.link = this.businessForm.value.instaUrl
        }
        if (_social.type === 'YUTB' && this.businessForm.controls.youtubeUrl.status !== 'INVALID') {
          _social.link = this.businessForm.value.youtubeUrl
        }
        if (_social.type.toUpperCase() === 'WEB' && this.businessForm.controls.webUrl.status !== 'INVALID') {
          _social.link = this.businessForm.value.webUrl
        }
      })
    } catch (error) {
      console.log(error)
    }

    let _socialLinks = []
    try {
      const { socialLinks } = this
      _socialLinks = socialLinks.map(_social => {
        return {
          socialMediaPortalsID: _social.id,
          socialMediaCode: _social.type,
          title: _social.title,
          linkURL: _social.link
        }
      })
    } catch {
    }

    let obj = {
      logisticServiceID: this.serviceIds,
      countryID: this.selected_country.id,
      redirectUrl: window.location.protocol + "//" + window.location.host + "/password",
      socialMediaPortalsID: (this.selectedSocialsite && Object.keys(this.selectedSocialsite).length && this.socialSites) ? this.selectedSocialsite.socialMediaPortalsID : null,
      linkURL: (this.selectedSocialsite && Object.keys(this.selectedSocialsite).length && this.socialSites) ? this.socialSites : null,
      companyOL: (this.showTranslatedLangSide) ? CompanyObjectOL : null,
      companyBL: CompanyObjectBL,
      userBL: UserObjectBL,
      userOL: (this.showTranslatedLangSide) ? UserObjectOL : null,
      socialMedias: (_socialLinks) ? _socialLinks : null
    }
    this._basicInfoService.createProviderAccount(obj).subscribe((res: any) => {
      if (res.returnStatus == "Success") {
        const _userObj = JSON.parse(res.returnObject)
        let id = _userObj.UserID
        try {
          const toSend = {
            assnWithCode: localStorage.getItem('sourceType').toUpperCase(),
            providerID: _userObj.ProviderID,
            assnWithID: 0,
            createdBy: id
          }
          this._basicInfoService.saveProviderAssociationByCode(toSend).subscribe(res => {
            // console.log(res)
          })
        } catch (error) {

        }
        let key = Base64.encode(id);
        this._router.navigate(['password', key])
        loading(false);
      }
      else {
        loading(false);
        this._toastr.error(res.returnText, '');
      }
    }, (err: HttpErrorResponse) => {
      loading(false);
      console.log(err);
    })
  }

  formatterCty = (x: { title: string, desc: string, imageName: string }) => {
    this.isCitySearching = false
    return x.title;
  };

  isCitySearching: boolean = false
  hasCitySearchFailed: boolean = false
  hasCitySearchSuccess: boolean = false

  search2 = (text$: Observable<string>) =>
    text$
      .debounceTime(300) //debounce time
      .distinctUntilChanged()
      .do(() => { this.isCitySearching = true; this.hasCitySearchFailed = false; this.hasCitySearchSuccess = false; }) // do any action while the user is typing
      .mergeMap(term => {
        let some = of([]); //  Initialize the object to return
        try {
          if (term && term.length >= 3) { //search only if item are more than three
            some = this._commonService.getFilteredCity(term)
              .do((res) => { this.isCitySearching = false; this.hasCitySearchSuccess = true; return res; })
              .catch(() => { this.isCitySearching = false; this.hasCitySearchFailed = true; return []; })
          } else { this.isCitySearching = false; some = of([]); }
        } catch (error) {
          this.isCitySearching = false
        }
        return some;
      })
      .do((res) => { this.isCitySearching = false; return res; })
      .catch(() => { this.isCitySearching = false; return of([]); }); // final server list

  socialLinks: { link: string; type: string, id: number, title: string }[] = []
  hasOptionalSocialTouched = false

  onEmailSocialEnter($type, $event) {
    this.hasOptionalSocialTouched = true

    if (!$event.target.value || $event.target.value === undefined || $event.target.value === null || $event.target.value === '') {
      if (this.socialLinks.length === 0) {
        this.hasOptionalSocialTouched = false
      }
    }

    if (($event.keyCode === 32 || $event.keyCode === 13)) {
      if ($event.target.value) {
        const optEmail: string = $event.target.value
        this.addSocial(optEmail)
        $event.preventDefault()
      }
    }
  }

  addSocial($optSocial: string) {

    const { selectedSocialsite } = this
    const { socialLinks } = this
    const _containsLink = socialLinks.filter(_social => _social.type === selectedSocialsite.socialMediaCode)
    if (_containsLink && _containsLink.length > 0) {
      this._toastr.warning('Link already added', 'Error');
      event.preventDefault();
      return;
    }

    let isValid = true

    if (selectedSocialsite.socialMediaCode === 'FCBK') {
      isValid = FACEBOOK_REGEX.test($optSocial)
      if (!isValid) { this._toastr.warning('Your social url is not valid') }
    } else if (selectedSocialsite.socialMediaCode === 'TWTR') {
      isValid = TWITTER_REGEX.test($optSocial)
      if (!isValid) { this._toastr.warning('Your social url is not valid') }
    } else if (selectedSocialsite.socialMediaCode === 'INST') {
      isValid = INSTAGRAM_REGEX.test($optSocial)
      if (!isValid) { this._toastr.warning('Your social url is not valid') }
    } else if (selectedSocialsite.socialMediaCode === 'LNKN') {
      isValid = LINKEDIN_REGEX.test($optSocial)
      if (!isValid) { this._toastr.warning('Your social url is not valid') }
    } else if (selectedSocialsite.socialMediaCode === 'YUTB') {
      isValid = YOUTUBE_REGEX.test($optSocial)
      if (!isValid) { this._toastr.warning('Your social url is not valid') }
    } else {
      isValid = GEN_URL.test($optSocial)
      if (!isValid) { this._toastr.warning('Your social url is not valid') }
    }
    // FCBK
    // TWTR
    // INST
    // LNKN
    // YUTB
    // Web

    if (isValid) {
      this.socialLinks.push({
        id: selectedSocialsite.socialMediaPortalsID,
        link: $optSocial,
        type: selectedSocialsite.socialMediaCode,
        title: selectedSocialsite.title
      })
      this.businessForm.controls['socialUrl'].setValue('')
    }
  }


  removeSocial({ link, type }) {
    const { socialLinks } = this
    this.hasOptionalSocialTouched = true
    this.socialLinks = socialLinks.filter(_social => _social.type !== type)
  }

}


export interface ISocialAccount {
  socialMediaPortalsID: number;
  socialMediaCode: string;
  shortName: string;
  title: string;
}