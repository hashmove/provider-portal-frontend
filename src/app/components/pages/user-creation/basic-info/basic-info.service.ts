import { Injectable } from '@angular/core';
import { baseApi } from '../../../../constants/base.url';
import { HttpClient } from "@angular/common/http";
import { getLoggedUserData } from '../../../../constants/globalFunctions';
import { SharedService } from '../../../../services/shared.service';

@Injectable()
export class BasicInfoService {


  constructor(
    private _http: HttpClient,
    private _sharedService: SharedService,
  ) { }

  getAccountSetup(id) {
    let url: string = `usersprovider/AccountSetup/${id}`;
    return this._http.get(baseApi + url);
  }
  getjobTitles(id) {
    let url: string = `job/GetJobTitles/${id}`;
    return this._http.get(baseApi + url);
  }

  userRegistration(obj) {
    let url: string = "usersprovider/Post";
    return this._http.post(baseApi + url, obj);
  }

  getUserInfoByOtp(otpKey) {
    let url: string = `otp/GetOTPUser/${otpKey}`;
    return this._http.get(baseApi + url);
  }

  resendOtpCode(obj) {
    let url: string = "otp/ResendOTP";
    return this._http.post(baseApi + url, obj);
  }

  sendOtpCode(otpKey) {
    let url: string = "otp/Post";
    return this._http.post(baseApi + url, otpKey);
  }

  getUserOtpVerified(key) {
    // let url: string = `otp/GetVerifiedOTPUser/${otpKey}/${status}`;
    let url: string = `providerregistration/GetVerifiedUser/${key}`;
    return this._http.get(baseApi + url);

  }
  createPaasword(obj) {
    let url: string = "providerregistration/CreatePassword";
    return this._http.post(baseApi + url, obj);
  }
  createProviderAccount(obj) {
    let url: string = "ProviderRegistration/Register";
    return this._http.post(baseApi + url, obj);
  }
  preSaleSignup(obj) {
    let url: string = "ProviderSignup/Register";
    return this._http.post(baseApi + url, obj);
  }
  getServiceOffered() {
    let url: string = "ProviderRegistration/GetLogisticServices";
    return this._http.get(baseApi + url);
  }
  socialList() {
    let url: string = "socialmedia/GetSocialMediaAccount";
    return this._http.get(baseApi + url);
  }
  docUpload(doc) {
    const loginUser = getLoggedUserData()
    let _extras: any = null
    try {
      const { city, country, query, region, countryCode } = this._sharedService.getMapLocation()
      _extras = {
        CityName: city,
        CountryName: country,
        IpAddress: query,
        RegionName: region,
        CountryCode: countryCode
      }
    } catch (error) { }
    const toSend = {
      ...doc,
      LoginUserID: (loginUser && loginUser.UserID) ? loginUser.UserID : -1,
      IsProvider: true,
      EmailTo: loginUser.PrimaryEmail,
      JsonUploadSource: JSON.stringify(_extras)
    }
    let url: string = "document/Post";
    toSend.UploadedBy = 'PROVIDER';
    return this._http.post(baseApi + url, toSend);
  }
  removeDoc(id) {
    let url: string = "document/Put";
    return this._http.put(baseApi + url, id);
  }
  getbusinessServices(providerID) {
    let url: string = `providerregistration/GetLogisticServicesByProvider/${providerID}`;
    return this._http.get(baseApi + url);
  }
  addBusinessInfo(obj) {
    let url: string = "providerregistration/SetupBusinessProfile";
    return this._http.post(baseApi + url, obj);
  }

  validateUserName(userName) {
    let url: string = `providerregistration/CheckProfileID/${userName}`;
    return this._http.get(baseApi + url);
  }

  getProviderLogisticService(providerID) {
    let url: string = `provider/GetProviderLogisticService/${providerID}`;
    return this._http.get(baseApi + url);
  }


  saveProviderAssociationByCode(data) {
    let url: string = `provider/SaveProviderAssociationByCode`;
    return this._http.post(baseApi + url, data);
  }

  getProviderAssociation(providerID) {
    let url: string = `provider/GetProviderAssociation/${providerID}`;
    return this._http.get(baseApi + url);
  }
  getProviderDetailByProviderID(providerID) {
    let url: string = `provider/GetProviderDetailByProviderID//${providerID}`;
    return this._http.get(baseApi + url);
  }

  getUserRoleRightsForMenu(userID) {
    let url: string = `approlerights/GetUserRoleRightsForMenu/${userID}/PROVIDER`;
    return this._http.get(baseApi + url);
  }
}
