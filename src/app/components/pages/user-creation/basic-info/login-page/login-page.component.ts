import { AfterViewInit, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { loading } from '../../../../../constants/globalFunctions';
import { UpdatePasswordComponent } from '../../../../../shared/dialogues/update-password/update-password.component';

@Component({
  selector: 'app-registration',
  templateUrl: './login-page.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit, AfterViewInit {


  public customerSettings: any = {}
  headingColor: string = '#ffffff'

  public isLogin: boolean = true

  constructor(
    private modalService: NgbModal
  ) { }

  ngOnInit() {
  }



  updatePasswordModal() {
    const modalRef = this.modalService.open(UpdatePasswordComponent, {
      size: "lg",
      centered: true,
      windowClass: "small-modal",
      backdrop: "static",
      keyboard: false
    });

    setTimeout(() => {
      if (
        document
          .getElementsByTagName("body")[0]
          .classList.contains("modal-open")
      ) {
        document.getElementsByTagName("html")[0].style.overflowY = "hidden";
      }
    }, 0);
  }

  compEventListener($event: string) {
    if ($event === 'f_pass') {
      this.isLogin = false
    } else if ($event === 'login') {
      this.isLogin = true
    }
  }

  ngAfterViewInit(){
    loading(false)
  }

}
