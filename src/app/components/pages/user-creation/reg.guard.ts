import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, NavigationStart, NavigationEnd } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BasicInfoService } from './basic-info/basic-info.service';
import { HttpErrorResponse } from '@angular/common/http';
import { SharedService } from '../../../services/shared.service';
import { isUserLogin } from '../../../services/jwt.injectable';
import { getLoggedUserData } from '../../../constants/globalFunctions';

@Injectable()
export class RegGuard implements CanActivate {

  public previousUrl;
  private islogOut: boolean;
  private infoObj;
  constructor(
    private _basicInfoService: BasicInfoService,
    private router: Router,
    private _sharedService: SharedService
  ) {
    // router.events
    //   .filter(event => event instanceof NavigationEnd)
    //   .subscribe(event => {
    //     console.log('prev:', this.previousUrl);
    //     this.previousUrl = event.url;
    //   });
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    this.getloginStatus()
    if (this._sharedService.isRegistrationRequired) {
      if (this.islogOut) {
        return true;
      } else {
        this.router.navigate(['provider/dashboard']);
        return false;
      }
    } else {
      this.router.navigate(['login']);
      return false;
    }
  }

  getloginStatus() {
    if (isUserLogin()) {
      this.infoObj = getLoggedUserData()
      this.islogOut = this.infoObj.IsLogedOut
      return this.islogOut
    } else {
      return this.islogOut = true;
    }
  }
}
