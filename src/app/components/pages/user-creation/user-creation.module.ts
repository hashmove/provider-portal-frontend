import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { UserGuard } from './user.guard'
import { UserCreationRoutingModule } from './user-creation-routing.module';
import { UserCreationComponent } from './user-creation.component';
import { BasicInfoModule } from './basic-info/basic-info.module';
import { RegGuard } from './reg.guard';
@NgModule({
  imports: [
    CommonModule,
    UserCreationRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    BasicInfoModule,
  ],
  declarations: [
    UserCreationComponent,
  ],
  providers: [
    UserGuard,
    RegGuard
  ],
})
export class UserCreationModule { }
