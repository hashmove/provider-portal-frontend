import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { CommonService } from '../../services/common.service';
import { SharedService } from '../../services/shared.service';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { isMobile, loading } from '../../constants/globalFunctions';
import { NgbTooltipConfig } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {
  public ports: any = [];

  public isCookeStored = true;
  public showHeader = false
  public backGround = "#f7f8fc"

  constructor(
    private _commonService: CommonService,
    private _sharedService: SharedService,
    private _router: Router,
    private _tooltipConfig: NgbTooltipConfig
  ) { }

  ngOnInit() {
    if (location.href.includes('login') || location.href.includes('account-activation')) {
      this.showHeader = false
      this.backGround = "background-image: url('../../../../assets/images/login-page-back.jpg');"
    } else {
      this.showHeader = true
      this.backGround = '#f7f8fc'
    }
    this.getCookie();
    this._router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe(() => {
      if (location.href.includes('login')) {
        this.showHeader = false
        this.backGround = "background-image: url('../../../../assets/images/login-page-back.jpg');"
      } else {
        this.showHeader = true
        this.backGround = '#f7f8fc'
      }
    });
    // this.getPortsData()
    // this.getCurrenciesList()
  }

  getCookie() {
    setTimeout(function () {
      const cookieInner = document.querySelector(".cookie-law-info-bar");
      const cookieMain = document.querySelector("app-cookie-bar");
      if (localStorage.getItem('cookiesPopup')) {
        this.isCookeStored = false;
        cookieMain.classList.add("hidePopup");
        cookieInner.classList.add("hidePopup");
      } else {
        // console.log('cookies not generat')
      }
    }, 0.5)
  }

  getCurrenciesList() {
    this._commonService.getCurrencyNew().subscribe((res: any) => {
      this._sharedService.setCurrency(res);
    }, (err: HttpErrorResponse) => {
    })
  }

  ngAfterViewInit() {
    const _href = location.href
    if (!_href.includes('bookings') && !_href.includes('rate-request')) {
      loading(false)
    }
  }
}
