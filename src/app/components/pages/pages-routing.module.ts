import { NgModule, NgModuleFactory, Type } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { UserDeskModule } from './user-desk/user-desk.module';
import { UserCreationModule } from './user-creation/user-creation.module';
import { Observable } from 'rxjs';
import { PublicViewBookingModule } from '../public/public-view-booking/public-view-booking.module';
import { AccountActivationModule } from '../public/account-activation/account-activation.module';

export function userDeskModuleChildren(): Type<any> | NgModuleFactory<any> | Promise<Type<any>> | Observable<Type<any>> {
  return UserDeskModule;
}
export function userCreationModuleChildren(): Type<any> | NgModuleFactory<any> | Promise<Type<any>> | Observable<Type<any>> {
  return UserCreationModule;
}

export function publicViewBookingModuleChildren(): Type<any> | NgModuleFactory<any> | Promise<Type<any>> | Observable<Type<any>> {
  return PublicViewBookingModule;
}

export function accountActivationModuleChildren(): Type<any> | NgModuleFactory<any> | Promise<Type<any>> | Observable<Type<any>> {
  return AccountActivationModule;
}


const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      { path: 'provider', loadChildren: userDeskModuleChildren },
      { path: 'booking-detail/:id/:id2', loadChildren: publicViewBookingModuleChildren },
      { path: 'account-activation/:account_key', loadChildren: accountActivationModuleChildren },
      { path: '', loadChildren: userCreationModuleChildren }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
