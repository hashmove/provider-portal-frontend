import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core'
import { baseExternalAssets, basesPartnerUrl } from '../../../../constants/base.url'
import { encryptBookingID, getLoggedUserData, isArrayValid, loading, isMobile } from '../../../../constants/globalFunctions'
import { SharedService } from '../../../../services/shared.service'
import { untilDestroyed } from 'ngx-take-until-destroy'
import { PaginationInstance } from 'ngx-pagination'
import { Router } from '@angular/router'
import { FilterdBookings, IRequestStatus, JsonResponse, IProviderConfig } from '../../../../interfaces'
import { RateRequestListHelper } from './rate-request-list.helper'
import { IRateRequest } from '../../../../shared/dialogues/request-add-rate-dialog/request-add-rate.interface'
import { ViewBookingService } from '../view-booking/view-booking.service'

@Component({
  selector: 'app-rate-request-list',
  templateUrl: './rate-request-list.component.html',
  styleUrls: ['./rate-request-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RateRequestListComponent extends RateRequestListHelper implements OnInit, OnDestroy {
  public requestList: IRateRequest[] = []
  public maximumSize = 10
  public directionLinks = true
  public responsive = true
  public autoHide = false
  public baseExternalAssets: string = baseExternalAssets
  public paginationConfig: PaginationInstance = { itemsPerPage: 10, currentPage: 1 }
  public currentBookingConfig: PaginationInstance = { id: 'advance', itemsPerPage: 10, currentPage: 1 }
  public userProfile: any

  // bootstrap paginate
  public data: Array<any> = []
  public maxPageSize = 10
  public currentPage = 1
  public totalPages = 1
  public newBookingUrl: string

  public totActiveReqests = 0
  public totRequests = 0

  public selectedRequestStatus: IRequestStatus = null
  public requestStatusList: Array<IRequestStatus> = []
  public requestStatusListFilter: Array<IRequestStatus> = []
  activeRequestTab = 'tab-active'
  IsSpotratAllowed = false
  isMobile = isMobile()

  constructor(
    private _sharedService: SharedService,
    private _router: Router,
    private _bookingService: ViewBookingService
  ) {
    super()
  }

  ngOnInit() {
    this.setViewConfig()
    this._sharedService._profile_id.pipe(untilDestroyed(this)).subscribe(state => {
      if (state)
        this.newBookingUrl = basesPartnerUrl + 'partner/' + state
    })
    this.getRequestStatusList()
    this.userProfile = getLoggedUserData()
  }

  setViewConfig() {
    const userProfile = getLoggedUserData()
    try {
      this.IsSpotratAllowed = !userProfile.ProviderConfig.IsSpotRateRestricted
    } catch { }
    this._sharedService.dashboardDetail.subscribe(state => {
      if (state && state.FeatureToggleJson) {
        try {
          const _feature: IProviderConfig = JSON.parse(state.FeatureToggleJson)
          this.IsSpotratAllowed = !_feature.IsSpotRateRestricted
        } catch { }
      }
    })
  }

  async getRequestStatusList() {
    this._bookingService.getRateRequestStatusList().subscribe((res: JsonResponse) => {
      if (res.returnId > 0 && typeof res.returnObject === 'object' && isArrayValid(res.returnObject, 0)) {
        this.requestStatusList = res.returnObject
        this.requestStatusListFilter = this.requestStatusList.filter(_st =>
          _st.BusinessLogic !== 'PRICE_SENT' &&
          _st.BusinessLogic !== 'REQUESTED_AGAIN' &&
          _st.BusinessLogic !== 'REQUEST' &&
          _st.BusinessLogic !== 'BOOKING_CANCELLED'
        )
      }
      else
        this.requestStatusList = []
    })
  }

  filterByDate(bookings) {
    return bookings.sort(function(a, b) {
      const dateA: any = new Date(a.HashMoveBookingDate)
      const dateB: any = new Date(b.HashMoveBookingDate)
      return dateB - dateA
    })
  }

  viewBookingDetails(bookingId, providerId, shippingModeCode) {
    this._router.navigate(['/provider/booking-detail', encryptBookingID(bookingId, providerId, shippingModeCode)])
  }

  onPageChange = (number) => this.paginationConfig.currentPage = number

  onTabIDChange($event) {
    this.onTabChange({ nextId: $event })
  }

  onTabChange($change) {
    const { nextId } = $change
    this.currentPage = 1
    this.activeRequestTab = nextId
    this.paginationConfig.currentPage = 1
    this.totalPages = this.activeRequestTab === 'tab-active' ? this.totActiveReqests : this.totRequests
  }

  refreshDasboardData() {
    setTimeout(() => {
      this._sharedService.bookingsRefresh.next(true)
    }, 0)
  }

  onRequestListChange($bookingsObject: FilterdBookings) {
    const { data, totSpotReq, totActiveSpotReq } = $bookingsObject
    console.log(data)
    this.totActiveReqests = totActiveSpotReq
    this.totRequests = totSpotReq
    if ($bookingsObject.pageNumber) {
      this.currentPage = $bookingsObject.pageNumber
    }
    this.setSpotRateBookings(data as any)
    this.totalPages = this.activeRequestTab === 'tab-active' ? this.totActiveReqests : this.totRequests
  }

  setSpotRateBookings($bookings: IRateRequest[]) {
    const _groupedBookings = $bookings.map(_request => ({
      ..._request,
      isExpanded: false,
      ChildRequests: [],
      RequestOrigin: _request.Origin,
      RequestDestination: _request.Destination
    }))
    this.requestList = _groupedBookings
    // setTimeout(() => { this.isSettingData = false}, 10);
  }


  getTotalPages = (pages) => Math.ceil(pages / this.currentBookingConfig.itemsPerPage)

  onPageChangeBootstrap(event) { }

  removeBooking($bookingId: number) {
    if ($bookingId > -1) {
      this.refreshDasboardData()
    }
  }

  ngOnDestroy() { }
}
