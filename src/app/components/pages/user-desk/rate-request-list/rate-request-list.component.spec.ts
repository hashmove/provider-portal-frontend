import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RateRequestListComponent } from './rate-request-list.component';

describe('RateRequestListComponent', () => {
  let component: RateRequestListComponent;
  let fixture: ComponentFixture<RateRequestListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RateRequestListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RateRequestListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
