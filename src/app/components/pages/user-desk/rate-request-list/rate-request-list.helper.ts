export class RateRequestListHelper {



    payloadForGroupedBookings($booking) {
        // console.log($booking)
        return {
            userID: $booking.UserID,
            providerID: $booking.ProviderID,
            requestNumber: null,
            bookingStatusCode: null,
            shipmentStatusCode: null,
            polID: 0,
            polCode: null,
            polType: null,
            podID: 0,
            podCode: null,
            podType: null,
            cargoType: null,
            shippingModeID: 0,
            fromDate: null,
            toDate: null,
            filterProviderID: 0,
            filterCompanyID: 0,
            carrierID: 0,
            sortBy: 'date',
            sortDirection: 'DESC',
            pageSize: 10,
            pageNumber: 1,
            tab: 'SpecialRequest',
            // tab: 'ACTIVE', // ALL
            blNumber: null,
            spotRateBookingKey: $booking.SpotRateBookingKey
        }
    }
}