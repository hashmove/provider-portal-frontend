import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { SharedService } from '../../../../services/shared.service';
import { getAccessRights, getLoggedUserData, isArrayValid, loading } from '../../../../constants/globalFunctions';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { IExpiryNotification } from '../user-desk.component';
import { BookingsHelper } from '../all-bookings/bookings.helper';
import { ViewBookingService } from '../view-booking/view-booking.service';
import { JsonResponse } from '../../../../interfaces/JsonResponse';
import { isRRV1, newFeatures } from '../../../../constants/base.url';
import { IProviderConfig } from '../../../../interfaces';
import { IAppObject } from '../../../../interfaces/access-rights.interface';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent extends BookingsHelper implements OnInit, OnDestroy {

  private allBookingsSubscriber
  public currentBookings: any[] = []
  public currentBookingsCount = 0
  public spotRateBookings: number = 0
  public isVirtualAirline: boolean = false
  public providerData
  expNotifications: IExpiryNotification
  isSpotRateRquired = false
  billingLink: string = null
  isRRV1 = isRRV1
  showRepSub = false
  showAccountSub = false
  showPerformance = false
  showInvoice = false
  newAppFeatures = newFeatures
  accessRoutes: IAppObject[] = []
  settingsRoutes: IAppObject[] = []

  constructor(
    private _sharedService: SharedService,
    private _viewBookingService: ViewBookingService
  ) {
    super()
  }

  ngOnInit() {
    this.setViewConfig()
    this.setMenus()
    this.getProviderData()
    this.getBookingsCount()
    this._sharedService.bookingsRefresh.pipe(untilDestroyed(this)).subscribe(_state => {
      if (_state)
        this.getBookingsCount()
    })
    this._sharedService.dashboardDetail.subscribe((state: any) => {
      if (state) {
        try {
          if (state.FeatureToggleJson) {
            const _controls = JSON.parse(state.FeatureToggleJson)
            if (_controls.IsSpotRateRquired)
              this.isSpotRateRquired = _controls.IsSpotRateRquired
          }
          if (state.BillingLink)
            this.billingLink = state.BillingLink
        } catch { }
      }
    })

    this._sharedService.currentBookingCount.pipe(untilDestroyed(this)).subscribe(state => {
      if (state)
        this.currentBookingsCount = state.totCurrBooking
    })

    this._sharedService.expNotifications.pipe(untilDestroyed(this)).subscribe(state => {
      this.expNotifications = state
    })
  }

  setMenus() {
    try {
      const _accessRoutes = getAccessRights().AppObject
      const parentMenus = _accessRoutes.filter(_menu => !_menu.ParentObjectID && _menu.ObjectType === 'MENU')
        .map(_menu => ({ ..._menu, ChildMenus: [], IsSubOpen: false }))
      const childMenusFilt = _accessRoutes.filter(_menu => _menu.ParentObjectID && _menu.ObjectType === 'SUB-MENU')
      const childMenusV1 = (!this.showPerformance) ? childMenusFilt.filter(_menu => _menu.ObjectBL !== 'PERFORMANCE') : childMenusFilt
      const childMenus = (!this.showInvoice) ? childMenusV1.filter(_menu => _menu.ObjectBL !== 'INVOICES') : childMenusV1
      if (isArrayValid(childMenus, 0)) {
        parentMenus.forEach(_menu => {
          _menu.ChildMenus = childMenus.filter(_subMenu => _subMenu.ParentObjectID === _menu.ObjectID)
        })
      }
      const _settingsBLs = ['SUPPORT', 'SETTINGS']
      this.settingsRoutes = parentMenus.filter(_menu => _settingsBLs.includes(_menu.ObjectBL))
      this.accessRoutes = parentMenus.filter(_menu => !_settingsBLs.includes(_menu.ObjectBL))
      // console.log(this.accessRoutes)
    } catch { }
  }

  setViewConfig() {
    const userProfile = getLoggedUserData()
    try {
      this.showPerformance = userProfile.ProviderConfig.IsPerformanceShow
      this.showInvoice = userProfile.ProviderConfig.IsInvoicePaymentShow
    } catch { }
    this._sharedService.dashboardDetail.subscribe(state => {
      if (state && state.FeatureToggleJson) {
        try {
          const _feature: IProviderConfig = JSON.parse(state.FeatureToggleJson)
          this.showPerformance = _feature.IsPerformanceShow
          this.showInvoice = _feature.IsInvoicePaymentShow
        } catch { }
      }
    })
  }

  getProviderData() {
    this.allBookingsSubscriber = this._sharedService.dashboardDetail.subscribe((state: any) => {
      if (state) {
        this.isVirtualAirline = state.IsVirtualAirLine
        this.providerData = state
      }
    })
  }


  loader = () => loading(true)

  getBookingsCount() {
    const { UserID, ProviderID } = getLoggedUserData()
    this._viewBookingService.getBookingsCount(this.payloadForGroupedBookings({ UserID, ProviderID, SpotRateBookingKey: null } as any))
      .subscribe((res: JsonResponse) => {
        if (res.returnId > 0) {
          this.currentBookingsCount = res.returnObject.totCurrBooking
          this.spotRateBookings = res.returnObject.totSpotReq
        }
      })
  }

  resetSubMenus() {
    this.showRepSub = false
    this.showAccountSub = false
  }

  ngOnDestroy() {
    this.allBookingsSubscriber.unsubscribe()
  }
}
