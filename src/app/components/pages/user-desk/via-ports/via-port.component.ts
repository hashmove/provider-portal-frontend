import { Component, OnInit, Input } from '@angular/core';
import { ViaRoutes } from '../../../../shared/dialogues/air-rate-dialog/air-rate-dialog.component';

@Component({
  selector: 'app-via-port',
  templateUrl: './via-port.component.html',
  styleUrls: ['./via-port.component.scss']
})
export class ViaPortComponent implements OnInit {
  @Input() viaPorts: ViaRoutes | any;
  constructor() { }
  ngOnInit() {
    const { viaPorts } = this
    try {
      this.viaPorts = JSON.parse(viaPorts)
    } catch (error) {

    }

  }

}

