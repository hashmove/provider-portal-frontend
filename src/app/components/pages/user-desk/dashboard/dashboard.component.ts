import { Component, OnInit, OnDestroy, ViewEncapsulation, Output, EventEmitter } from '@angular/core';
import { SharedService } from '../../../../services/shared.service';
import { baseExternalAssets } from '../../../../constants/base.url';
import { Router } from '@angular/router';
import { encryptBookingID, getLoggedUserData, isJSON, loading, isMobile } from '../../../../constants/globalFunctions';
import { DashboardService } from './dashboard.service';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { JsonResponse } from '../../../../interfaces/JsonResponse';
import { UserDeskComponent } from '../user-desk.component';
import { CustomersService } from '../customers/customers.service';
import { ViewBookingService } from '../view-booking/view-booking.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit, OnDestroy {
  @Output() activatedTab: EventEmitter<any> = new EventEmitter();
  public providerInfo;
  private dashboardSubscriber;
  public baseExternalAssets: string = baseExternalAssets;
  public selectedBookingMode = 'CURRENT BOOKINGS';
  public bookingYears: any[] = []
  public selectedYear: string = ''
  userProfile: any
  currentBookings = []
  isMobile = isMobile()

  customerCount: number = 0
  currentBookingsCount = 0
  spotRateBookings = 0
  totBookings = 0


  constructor(
    private _sharedService: SharedService,
    private _router: Router,
    private _dashboardService: DashboardService,
    private _customerService: CustomersService,
    private _viewBookingService: ViewBookingService
  ) { }

  ngOnInit() {
    this.userProfile = getLoggedUserData()
    this.dashboardSubscriber = this._sharedService.dashboardDetail.subscribe((state: any) => {
      loading(false)
      if (state) {
        this.providerInfo = state;
        if (this.providerInfo.BookingDetails && this.providerInfo.BookingDetails.length) {
          this.providerInfo.BookingDetails.map(elem => {
            if (elem.CustomerImage && typeof elem.CustomerImage == "string" && elem.CustomerImage != "[]" && isJSON(elem.CustomerImage)) {
              elem.CustomerLogo = JSON.parse(elem.CustomerImage).shift().DocumentFile
            } else if (elem.UserImage) {
              elem.CustomerLogo = elem.UserImage;
            }
          })
        }
      }
    });
    this.getBookingList()

    this._sharedService.currentBookingCount.pipe(untilDestroyed(this)).subscribe(_state => {
      if (_state) {
        this.currentBookingsCount = _state.totCurrBooking
        this.spotRateBookings = _state.totSpotReq
        this.totBookings = _state.totBooking
      }
    })

    this._customerService.getProviderCustomerCount(this.userProfile.ProviderID).subscribe((res: JsonResponse) => {
      try {
        this.customerCount = res.returnObject.customerCount
      } catch (error) { }
    })

    this._dashboardService.getBookingYears().pipe(untilDestroyed(this)).subscribe((res: JsonResponse) => {
      // loading(false)
      if (res.returnId > 0) {
        try {
          this.bookingYears = JSON.parse(res.returnText)
          this.selectedYear = this.bookingYears[1].BookingYear
        } catch (error) {
        }
      }
    })
  }

  onYearSelect($year: any) {
    try {
      if ($year && $year.BookingYear) {
        this.selectedYear = $year.BookingYear
      }
    } catch (error) { }
  }

  ngOnDestroy() {
    this.dashboardSubscriber.unsubscribe();
  }
  viewBookingDetails(bookingId, providerId, shippingModeCode) {
    let id = encryptBookingID(bookingId, providerId, shippingModeCode);
    this._router.navigate(['/provider/booking-detail', id]);
  }

  viewAllBookings() {
    this._router.navigate(['/provider/allbookings']);
  }
  toCustomers() {
    this._router.navigate(['/provider/customers']);
  }
  bookingsTabRedirect(tab) {
    this._router.navigate(['/provider/allbookings']);
    this._sharedService.activatedBookingTab.next(tab);
  }

  refreshDasboardData() {
    this._sharedService.bookingsRefresh.next(true)
    UserDeskComponent.userDasboardDataApiCall(this.userProfile.UserID, this._dashboardService, this._sharedService)
  }

  getBookingList() {
    this._viewBookingService
      .getBookingsByPage(this.payloadForGroupedBookings({ UserID: getLoggedUserData().UserID, SpotRateBookingKey: null } as any))
      .subscribe((res: JsonResponse) => {
        if (res.returnId > 0) {
          const { data } = res.returnObject;
          this.currentBookings = data
          this._sharedService.currentBookingCount.next(res.returnObject)
        }
      });
  }

  payloadForGroupedBookings = ($booking) => ({
    userID: this.userProfile.UserID,
    providerID: this.userProfile.ProviderID,
    bookingNumber: null,
    bookingStatusCode: null,
    shipmentStatusCode: null,
    polID: 0,
    polCode: null,
    polType: null,
    podID: 0,
    podCode: null,
    podType: null,
    cargoType: null,
    shippingModeID: 0,
    fromDate: null,
    toDate: null,
    filterProviderID: 0,
    filterCompanyID: 0,
    carrierID: 0,
    sortBy: "date",
    sortDirection: "DESC",
    pageSize: 5,
    pageNumber: 1,
    tab: 'CURRENT',
    blNumber: null,
    spotRateBookingKey: $booking.SpotRateBookingKey
  })
}
