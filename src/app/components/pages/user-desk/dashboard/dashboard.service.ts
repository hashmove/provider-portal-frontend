import { Injectable } from '@angular/core';
import { baseApi } from '../../../../constants/base.url';
import { HttpClient, HttpParams } from '@angular/common/http';
import { getLoggedUserData } from '../../../../constants/globalFunctions';
import { getDefaultHMUser } from '../../../../services/jwt.injectable';
import { SharedService } from '../../../../services/shared.service';

@Injectable()
export class DashboardService {

  constructor(
    private _http: HttpClient,
    private _sharedService: SharedService
  ) { }


  getdashboardDetails(userId) {
    const url = `usersprovider/GetProviderDashboardSummary/${userId}`;
    return this._http.get(baseApi + url);
  }

  getProviderBillingDashboard(providerId: number, period: string) {
    const url = `general/GetProviderBillingDashboard/${providerId}/${period}`;
    return this._http.get(baseApi + url);
  }

  getProviderBillingDashboardInvoices(providerId: number, period: string) {
    const url = `general/GetProviderBillingDashboardInvoices/${providerId}/${period}`;
    return this._http.get(baseApi + url);
  }

  getContainerDetails(bookingKey, portalName) {
    const url = `booking/GetBookingContainer/${bookingKey}/${portalName}`;
    return this._http.get(baseApi + url);
  }

  getRequestContainer(requestKey, responseId, portalName) {
    const url = `RateRequest/GetRequestContainer/${requestKey}/${responseId}/${portalName}`;
    return this._http.get(baseApi + url);
  }

  updateSpecialPrice(bookingID, loginUserID, data) {
    const url = `booking/UpdateBookingSpecialPrice/${bookingID}/${loginUserID}`;
    return this._http.put(baseApi + url, data);
  }
  updateSpecialPriceV2(bookingID, loginUserID, data) {
    const url = `RateReqResponseFCL/Post`;
    return this._http.post(baseApi + url, data);
  }

  updateBookingAmount(bookingID, data) {
    const url = `booking/UpdateBookingAmount/${bookingID} `;
    return this._http.put(baseApi + url, data);
  }

  saveUserDocument(data) {
    const loginUser = getLoggedUserData()
    let _extras: any = null
    try {
      const { city, country, query, region, countryCode } = this._sharedService.getMapLocation()
      _extras = {
        CityName: city,
        CountryName: country,
        IpAddress: query,
        RegionName: region,
        CountryCode: countryCode
      }
    } catch (error) { }
    const toSend = {
      ...data,
      LoginUserID: (loginUser && loginUser.UserID) ? loginUser.UserID : -1,
      IsProvider: true,
      JsonUploadSource: JSON.stringify(_extras)
    }
    const url: string = "document/Post";
    toSend.UploadedBy = 'PROVIDER';
    return this._http.post(baseApi + url, toSend);
  }

  getBookingSpecialLogs(bookingKey: string, portalType: string) {
    const url = `booking/GetBookingSpecialPriceLog/${bookingKey}/${portalType}`;
    return this._http.get(baseApi + url);
  }

  getRequestPriceLog(requestKey: string, responseTagId: number, portalType: string) {
    const url = `RateRequest/GetRequestPriceLog/${requestKey}/${responseTagId}/${portalType}`;
    return this._http.get(baseApi + url);
  }

  getBookingYears() {
    const url = `general/GetBookingYear`;
    return this._http.get(baseApi + url);
  }

  getLCLUnits() {
    const url = "general/GetUnitType"
    return this._http.get(baseApi + url)
  }

  getCustomerShippingData(customerID: number, customerType: string) {
    const url: string = `shippingModeCatMapping/GetCustomerShippingCriteria/${customerID}/${customerType}`;
    return this._http.get(baseApi + url);
  }

  sendChat(loggedUserID: number, data: any) {
    const url: string = `booking/UpdateBookingChat/${loggedUserID}`;
    return this._http.put(baseApi + url, data);
  }

  getChatList(bookingKey: string, parameterType: string) {
    const url: string = `booking/GetBookingAdditionalParameters/${bookingKey}/${parameterType}`;
    return this._http.get(baseApi + url);
  }



  updateBookingChatStatus(loginUserID: number, data) {
    const url: string = `booking/UpdateBookingChatStatus/${loginUserID}`;
    return this._http.put(baseApi + url, data);
  }

  getChatListV2(requestKey: string, portalType: string, parameterType: string, responseTagId: number) {
    // const url: string = `RateRequest/GetRequestAdditionalParameters/${requestKey}/${parameterType}`;
    const url: string = `RateRequest/GetRequestAdditionalParameters/${requestKey}/${portalType}/${parameterType}/${responseTagId}`;
    return this._http.get(baseApi + url);
  }

  sendChatV2(loggedUserID: number, data: any) {
    const url: string = `RateRequest/UpdateRequestChat/${loggedUserID}`;
    return this._http.put(baseApi + url, data);
  }

  updateRequestChatStatusV2(loginUserID: number, data) {
    const url: string = `RateRequest/UpdateRequestChatStatus/${loginUserID}`;
    return this._http.put(baseApi + url, data);
  }

  getUserBasicDetail(data) {
    const url: string = `users/GetUserBasicDetail`;
    return this._http.post(baseApi + url, data);
  }

  getOtherDocument(otherType, otherTypeID) {
    const url = `Document/GetOtherDocument/${otherType}/${otherTypeID}`;
    return this._http.get(baseApi + url);
  }

  getInvoiceDocs(bookingID, invoiceID) {
    const url = `booking/GetBookingInvoiceDocument/${bookingID}/${invoiceID}`;
    return this._http.get(baseApi + url);
  }

  getPaymentTerms(_bookingKey) {
    const url = `TermsCondition/GetHashmoveBookingCommissionTermsCondition/${_bookingKey}`;
    return this._http.get(baseApi + url);
  }

  removeDocument(docId, userID) {
    let url: string = `Document/Delete/${docId}/${userID}`;
    return this._http.delete(baseApi + url);
  }

}
