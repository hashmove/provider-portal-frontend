import { Component, OnInit, ViewEncapsulation, AfterViewInit, ChangeDetectorRef, AfterViewChecked, Input, SimpleChanges, ViewChild } from '@angular/core'
import { Router } from '@angular/router'
import { loading, encryptBookingID, isArrayValid, isMobile } from '../../../../constants/globalFunctions'
import { FilterdBookings } from '../../../../interfaces'
import { SharedService } from '../../../../services/shared.service'
import { IInvoiceCard } from '../invoice-card/invoice-card.interface'
import { ViewBookingService } from '../view-booking/view-booking.service'
import { InvoicetListHelper } from './invoice-list.helper'
@Component({
  selector: 'app-invoice-list',
  templateUrl: './invoice-list.component.html',
  styleUrls: ['./invoice-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InvoiceListComponent extends InvoicetListHelper implements OnInit, AfterViewInit, AfterViewChecked {
  @Input() isQuoteActiveBooking = false
  @Input() pageTypeBooking = 'booking'
  @Input() isRRV2 = false
  @ViewChild('bookingsTab') bookingsTab: any

  isMobile = isMobile()

  constructor(
    private _shareService: SharedService,
    private _router: Router,
    private _cdr: ChangeDetectorRef,
    private _bookingService: ViewBookingService,
  ) { super() }

  ngOnInit() {
    setTimeout(() => {
      loading(false)
    }, 100)
    this.isQuoteActive = this.isQuoteActiveBooking
    this.isViewLoaded = false
    this.isSettingData = true
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  newSearch() {
  }

  ngAfterViewInit() {
    this.isViewLoaded = true
  }

  ngAfterViewChecked() {
    this._cdr.detectChanges()
  }


  getContainerLoadTypeList(containerLoadType: string) {
    let strCont: any = ''
    try { strCont = containerLoadType.split('|') }
    catch (error) { strCont = '' }
    return strCont
  }

  viewBookingDetails = (bookingId: any) =>
    this._router.navigate(['/user/booking-detail', encryptBookingID(bookingId)])

  getTotalPages = () => Math.ceil(this.invoiceList.length / this.invoiceConfig.itemsPerPage)

  removeBooking($bookingId: number) {
    if ($bookingId > -1)
      this.refreshDasboardData()
  }
  refreshDasboardData = () => setTimeout(() => { this._shareService.bookingsRefresh.next(true) }, 0)

  onPageChangeBootstrap() { }

  onBookingListChange($bookingsObject: FilterdBookings) {
    if ($bookingsObject) {
      const { data, totCurrBooking, totBooking } = $bookingsObject
      this.currentInvoiceCount = totCurrBooking
      this.totalInvoiceCount = totBooking
      if ($bookingsObject.pageNumber)
        this.currentPage = $bookingsObject.pageNumber
      this.setSpotRateBookings(data as any)
      this.totalPages = this.activeInvoiceTab === 'tab-active' ? this.currentInvoiceCount : this.totalInvoiceCount
    } else {
      this.loading = false
      setTimeout(() => { this.isSettingData = false }, 10)
    }
  }

  setSpotRateBookings($bookings: IInvoiceCard[]) {
    this.invoiceList = $bookings
    setTimeout(() => { this.isSettingData = false }, 10)
  }

  onTabIDChange($event) {
    this.onTabChange({ nextId: $event })
  }

  onTabChange($change) {
    const { nextId } = $change
    this.currentPage = 1
    this.activeInvoiceTab = nextId
    this.invoiceConfig.currentPage = 1
    this.invoiceList = []
    this.totalPages = this.activeInvoiceTab === 'tab-active' ? this.currentInvoiceCount : this.totalInvoiceCount
  }

  tonavigate() { }

  async getRequestStatusList() {
    try {
      const res = await this._bookingService.getRateRequestStatusList().toPromise() as any
      this.requestStatusList = (res.returnId > 0 && typeof res.returnObject === 'object' && isArrayValid(res.returnObject, 0)) ?
        res.returnObject : []
    } catch { }
  }
}
