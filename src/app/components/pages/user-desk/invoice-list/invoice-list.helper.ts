import { Injectable } from "@angular/core"
import { PaginationInstance } from "ngx-pagination"
import { IBookingDtl, IRequestStatus } from "../../../../interfaces"
import { IInvoiceCard } from "../invoice-card/invoice-card.interface"
@Injectable()
export class InvoicetListHelper {

    public isQuoteActive: boolean = false
    loading = false
    invoiceList: IInvoiceCard[]

    public dashboardData: any = []
    public bookingList: IBookingDtl[]
    public isViewLoaded = false
    public maxSize = 7
    public directionLinks = true
    public autoHide = false
    public invoiceConfig: PaginationInstance = { id: 'advance', itemsPerPage: 10, currentPage: 1 }
    public labels: any = { previousLabel: '', nextLabel: '', }
    public isSettingData = true
    public data: Array<any> = []
    public maxPageSize = 10
    public currentPage = 1
    public totalPages = 1
    totCurrBooking = 0
    isHomePage = false
    public requestStatusList: IRequestStatus[] = []

    //Invoice 
    activeInvoiceTab = 'tab-active'
    currentInvoiceCount = 0
    totalInvoiceCount = 0

    constructor() {

    }

    payloadForGroupedBookings($booking) {
        return {
            userID: $booking.UserID,
            providerID: 0,
            requestNumber: null,
            bookingStatusCode: null,
            shipmentStatusCode: null,
            polID: 0,
            polCode: null,
            polType: null,
            podID: 0,
            podCode: null,
            podType: null,
            cargoType: null,
            shippingModeID: 0,
            fromDate: null,
            toDate: null,
            filterProviderID: 0,
            filterCompanyID: 0,
            carrierID: 0,
            sortBy: 'price',
            sortDirection: 'ASC',
            pageSize: 10,
            pageNumber: 1,
            tab: 'ACTIVE', // ALL
            blNumber: null,
            spotRateBookingKey: $booking.SpotRateBookingKey
        }
    }



}