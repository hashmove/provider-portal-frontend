import { Component, OnInit, Input, Output, EventEmitter, OnChanges, OnDestroy } from '@angular/core';
import { ViewBookingDetails, TrackingMonitoring, QaualitMonitorResp, QualityMonitoringAlertData, LineMarker } from '../../../../../interfaces/view-booking.interface';
import { Polyline } from '@agm/core/services/google-maps-types';
import { GuestService } from '../../../../../services/jwt.injectable';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { getGreyIcon, loading, getTimeStr, isMobile } from '../../../../../constants/globalFunctions';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { HttpErrorResponse } from '@angular/common/http';
import { JsonResponse } from '../../../../../interfaces/JsonResponse';
import { ViewBookingService } from '../../view-booking/view-booking.service';

@Component({
  selector: 'app-tracking-monitoring',
  templateUrl: './tracking-monitoring.component.html',
  styleUrls: ['./tracking-monitoring.component.scss']
})
export class TrackingMonitoringComponent implements OnInit, OnChanges, OnDestroy {

  @Input() data: ViewBookingDetails
  @Input() trackingData: TrackingMonitoring
  @Input() monitorData: QaualitMonitorResp
  @Input() isTracking: boolean
  @Input() isMonitoring: boolean
  @Input() isContDtl: boolean
  @Input() shouldTrackingOpen: boolean
  @Output() containerCallback = new EventEmitter<any>()
  container: QualityMonitoringAlertData

  pastRoutes: Array<Polyline> = []
  futureRoutes: Array<Polyline> = []
  currentLocation: Polyline
  containerList: QualityMonitoringAlertData[] = []

  public viewDetailStatus: boolean = false;
  searchCriteria: any

  public isGuestLogin: boolean = false
  public paramSubscriber: any;
  public guestBookingKey: any
  public bookingDetails: ViewBookingDetails;
  public isTrackingOnBooking: boolean = false;
  public loading: boolean = true
  public userCountry: any;
  isMobile = isMobile()

  constructor(
    private _jwtService: GuestService,
    private _router: ActivatedRoute,
    private _toastr: ToastrService,
    private _bookingService: ViewBookingService
  ) { }

  async ngOnInit() {
    let url = window.location.href
    if (url.includes('tracking')) {
      let x = url.split('tracking')
      let d3key = x[1].split('/')
      const res = await this._jwtService.shareLogin(d3key[2])
      this.isGuestLogin = true
      // localStorage.setItem('isGuestLogin', JSON.stringify(this.isGuestLogin))
      this.isContDtl = false
      this.shouldTrackingOpen = true
      this.getParams()
    } else {
      this.searchCriteria = JSON.parse(this.data.JsonSearchCriteria)
      localStorage.setItem('curr_mode', this.data.ShippingModeCode);
    }
  }

  ngOnChanges() {
    if (this.trackingData) {
      this.setTracking()
    }
  }

  setTracking() {

  }


  getIcon($mode: string) {
    return getGreyIcon($mode)
  }

  onContainerClick($event: QualityMonitoringAlertData) {
    try {
      const { ContainerNo } = $event
      this.container = $event
      const { AlertData } = this.monitorData

      const filContainers = AlertData.filter(cont => cont.ContainerNo === ContainerNo)
      console.log(filContainers);

      this.containerList = filContainers
    } catch (error) {
      console.log(error);

    }
    this.containerCallback.emit($event)
  }

  viewDetail($event: any) {
    this.viewDetailStatus = !this.viewDetailStatus;
  }


  getFlightTime(time: number) {
    return getTimeStr(time)
  }

  getParams() {
    this.paramSubscriber = this._router.params.subscribe(params => {
      let bookingId = params['id']; // (+) converts string 'id' to a number
      this.guestBookingKey = bookingId
      if (bookingId) {
        this.getBookingDetail(bookingId);
      }
    });
  }


  /**
   * GET BOOKING DETAILS
   *
   * @param {number} bookingId
   * @memberof TrackingMonitoringComponent
   */
  public miniMap: LineMarker
  public showWarehouseMap: boolean = false
  public showMap: boolean = false
  public showDtlMap: boolean = false
  public shouldMapOpen: boolean = true
  public wareMiniMapData = {
    WHLatitude: 0,
    WHLongitude: 0
  }
  public isMonitoringOnBooking: boolean = false
  getBookingDetail(bookingId) {
    this._bookingService.getBookingDetails(bookingId).pipe(untilDestroyed(this)).subscribe((res: any) => {
      if (res.returnId > 0) {
        this.bookingDetails = JSON.parse(res.returnText);
        this.data = JSON.parse(res.returnText);
        if (this.data.BookingContainerDetail) {
          this.data.BookingContainerDetail.forEach(e => {
            e.parsedJsonContainerInfo = JSON.parse(e.JsonContainerInfo)
          })
        }
        this.searchCriteria = JSON.parse(this.data.JsonSearchCriteria)
        const countryData = JSON.parse(this.data.JsonBookingLocation)
        const { EtdUtc, ShippingModeCode } = this.data

        try {
          if (ShippingModeCode !== 'WAREHOUSE') {
            // if (this.data.JsonShippingDestInfo && isJSON(this.data.JsonShippingDestInfo)) {
            //   this.data.JsonShippingDestInfo = JSON.parse(this.data.JsonShippingDestInfo);
            //   this.editSupDestToggler = true;
            // }
            // else if (!this.data.JsonShippingDestInfo) {
            //   let object = this.countryList.find(obj => obj.title == this.data.PodCountry);
            //   this.supFlagImgDest = object.code;
            //   let description = object.desc;
            //   this.supDestphoneCode = description[0].CountryPhoneCode;
            //   this.supDestCountryId = object.id

            // }
            // if (this.data.JsonShippingOrgInfo && isJSON(this.data.JsonShippingOrgInfo)) {
            //   this.data.JsonShippingOrgInfo = JSON.parse(this.data.JsonShippingOrgInfo)
            //   this.editSupOrgToggler = true;
            // }
            // else if (!this.data.JsonShippingOrgInfo) {
            //   let object = this.countryList.find(obj => obj.title == this.data.PolCountry);
            //   this.supFlagImgOrg = object.code;
            //   let description = object.desc;
            //   this.supOrgphoneCode = description[0].CountryPhoneCode;
            //   this.supOrgCountryId = object.id

            // }
            // if (this.data.JsonAgentOrgInfo && isJSON(this.data.JsonAgentOrgInfo)) {
            //   this.data.JsonAgentOrgInfo = JSON.parse(this.data.JsonAgentOrgInfo);
            // }
            // if (this.data.JsonAgentDestInfo && isJSON(this.data.JsonAgentDestInfo)) {
            //   this.data.JsonAgentDestInfo = JSON.parse(this.data.JsonAgentDestInfo);
            // }
          }
        } catch { }

        if (countryData) {
          this.userCountry = countryData
        } else {
          this.userCountry = {
            country: ''
          }
        }

        let date2Process = ''

        if (ShippingModeCode.toUpperCase() !== 'WAREHOUSE') {
          date2Process = EtdUtc
        } else if (ShippingModeCode.toUpperCase() === 'WAREHOUSE') {
          date2Process = this.data.StoredFromLcl
        }
        // this.dateList = getWarehouseBookingDates(date2Process)
        if (this.data.LoadPickupDate) {

          // this.loadPickupDate = this.data.LoadPickupDate
          // this.setLoadPickupDate(this.loadPickupDate)

        }

        const { BookingContainerDetail } = this.data
        if (BookingContainerDetail && BookingContainerDetail.length) {
          BookingContainerDetail.forEach(container => {
            if (container.IsTrackingRequired) {
              this.isTrackingOnBooking = true;
              this.isMonitoringOnBooking = true;
              this.isMonitoring = this.isMonitoringOnBooking
            }
            if (container.IsQualityMonitoringRequired) {
              this.isMonitoringOnBooking = true;
              this.isTrackingOnBooking = true;
              this.isMonitoring = this.isMonitoringOnBooking
            }
          })
        }

        // this.data.ProviderDisplayImage = getImagePath(ImageSource.FROM_SERVER, this.data.ProviderImage, ImageRequiredSize._48x48)
        // this.data.CarrierDisplayImage = getImagePath(ImageSource.FROM_SERVER, this.data.CarrierImage, ImageRequiredSize._48x48)
        try {
          // this.ProviderEmails = this.data.ProviderEmail.split(';');
          // this.ProviderPhones = this.data.ProviderPhone.split(';');
        } catch (error) {

        }
        const { PodCode, PolCode } = this.data
        const { BookingDocumentDetail, BookingRouteMapInfo } = this.data
        this.data.PolImage = PolCode.split(' ')[0].toLowerCase()
        this.data.PodImage = PodCode.split(' ')[0].toLowerCase()

        if (this.data.ShippingModeCode.toLowerCase() === 'truck') {
          // this.isTruck = true
        }

        if (BookingDocumentDetail && BookingDocumentDetail.length > 0) {

          const add_docs_cust: any = BookingDocumentDetail.filter(doc => doc.DocumentNature === 'ADD_DOC' && doc.DocumentSubProcess !== 'PROVIDER')[0]
          // const add_docs_prov: any = BookingDocumentDetail.filter(doc => doc.DocumentNature === 'ADD_DOC' && doc.DocumentSubProcess === 'PROVIDER')[0]

          const adDocCustomers: any = BookingDocumentDetail.filter(doc => doc.DocumentNature === 'ADD_DOC' && doc.DocumentSubProcess !== 'PROVIDER' && doc.DocumentID > 0) as any
          adDocCustomers.forEach(doc => {
            doc.ShowUpload = false;
            doc.DateModel = ''
          })
          // this.additionalDocumentsCustomers = adDocCustomers

          const adDocProviders: any = BookingDocumentDetail.filter(doc => doc.DocumentNature === 'ADD_DOC' && doc.DocumentSubProcess === 'PROVIDER' && doc.DocumentID > 0) as any
          adDocProviders.forEach(doc => {
            doc.ShowUpload = false;
            doc.DateModel = ''
          })
          // this.additionalDocumentsProviders = adDocProviders

          // this.add_docs_cust = add_docs_cust
          // this.add_docs_prov = add_docs_prov
          try {
            // this.resetAccordian()
          } catch (error) {

          }

          // this.originDocList = BookingDocumentDetail.filter((e) => e.DocumentSubProcess === 'ORIGIN');
          // this.destinDocList = BookingDocumentDetail.filter((e) => e.DocumentSubProcess === 'DESTINATION');

          // const destDocClone: BookingDocumentDetail[] = cloneObject(this.destinDocList)
          // this.destinDocList = destDocClone.sort(compareValues('IsUploadable', "asc"));
        }
        if (BookingRouteMapInfo) {
          this.showDtlMap = true
          this.showMap = true

          BookingRouteMapInfo.RouteInfo.forEach(route => {
            if (!route.Latitude || !route.Longitude) {
              this.shouldMapOpen = false
              return
            }
          })

          if (this.data.ShippingModeCode === 'AIR') {
            // this.setAirCraftInfo(BookingRouteMapInfo)
          }
          try {
            const polFiltered = BookingRouteMapInfo.RouteInfo.filter((e) => e.PortCode === this.data.PolCode)[0];
            this.data.PolLatitude = (polFiltered.Latitude) ? polFiltered.Latitude : 0;
            this.data.PolLongitude = (polFiltered.Longitude) ? polFiltered.Longitude : 0;


            const podFiltered = BookingRouteMapInfo.RouteInfo.filter((e) => e.PortCode === this.data.PodCode)[0];
            this.data.PodLatitude = (podFiltered.Latitude) ? podFiltered.Latitude : 0;
            this.data.PodLongitude = (podFiltered.Longitude) ? podFiltered.Longitude : 0;

            const { PolLatitude, PolLongitude, PolModeOfTrans, PodLatitude, PodLongitude, PodModeOfTrans } = this.data

            this.miniMap = {
              LatitudeA: (PolLatitude) ? PolLatitude : 0,
              LongitudeA: (PolLongitude) ? PolLongitude : 0,
              transPortModeA: PolModeOfTrans,
              LatitudeB: (PodLatitude) ? PodLatitude : 0,
              LongitudeB: (PodLongitude) ? PodLongitude : 0,
              transPortModeB: PodModeOfTrans,
            }
          } catch (err) {
          }
        } else if (this.data.WHLatitude && this.data.WHLongitude) {
          this.showWarehouseMap = true
          const { WHLatitude, WHLongitude } = this.data
          this.wareMiniMapData = {
            WHLatitude: (WHLatitude) ? WHLatitude : 0,
            WHLongitude: (WHLongitude) ? WHLongitude : 0
          }
        } else if (this.data.PolLatitude && this.data.PolLongitude && this.data.PodLatitude && this.data.PodLongitude) {
          this.showMap = true
          const { PolLatitude, PolLongitude, PolModeOfTrans, PodLatitude, PodLongitude, PodModeOfTrans } = this.data
          this.miniMap = {
            LatitudeA: (PolLatitude) ? PolLatitude : 0,
            LongitudeA: (PolLongitude) ? PolLongitude : 0,
            transPortModeA: PolModeOfTrans,
            LatitudeB: (PodLatitude) ? PodLatitude : 0,
            LongitudeB: (PodLongitude) ? PodLongitude : 0,
            transPortModeB: PodModeOfTrans,
          }
        }
        this.loading = false
        this.changeView('track')
      } else {
        this._toastr.error('Unable to find this booking. Please check the link and try again', 'Failed to Fetch Data')
        this.loading = false
      }
    }, (err: HttpErrorResponse) => {
      this.loading = false
    })
  }

  public qualityMonitorData: QaualitMonitorResp
  changeView($type: string) {
    switch ($type) {
      case 'track':
        if (!this.shouldMapOpen) {
          this._toastr.warning('Tracking Cordinates Mismatched')
        }
        if (!this.isMonitoringOnBooking && !this.isTrackingOnBooking && !this.bookingDetails.EtdUtc) {
          this._toastr.warning('No data available for this Booking')
          return
        }
        loading(true)
        const { guestBookingKey } = this

        // if (this.isTrackingOnBooking) {
        if (this.searchCriteria.searchMode !== 'truck-ftl') {
          this._bookingService.getTrackingInformation(guestBookingKey).subscribe((res: JsonResponse) => {
            const { returnId, returnText } = res
            loading(false)
            console.log(res)
            if (returnId > 0) {
              this.trackingData = JSON.parse(returnText)
            } else {
              this._toastr.error(returnText, 'Error')
              this.trackingData = {
                ContainerDetails: [],
                EtaDetails: {
                  EtaLcl: '',
                  EtaUtc: '',
                  Status: ''
                },
                Polylines: [],
                RouteInformation: '',
                VesselInfo: {
                  VesselName: '',
                  VesselType: '',
                  VesselFlag: '',
                  VesselPhotos: [],
                }
              }
            }
            this.isTracking = true;
            this.isContDtl = false
          }, (error: HttpErrorResponse) => {
            this._toastr.error(error.message, 'Error')
            loading(false)
            this.trackingData = {
              ContainerDetails: [],
              EtaDetails: {
                EtaLcl: '',
                EtaUtc: '',
                Status: ''
              },
              Polylines: [],
              RouteInformation: '',
              VesselInfo: {
                VesselName: '',
                VesselType: '',
                VesselFlag: '',
                VesselPhotos: [],
              }
            }
            this.isTracking = true;
            this.isContDtl = false
          })
        }


        if (this.isMonitoringOnBooking || this.isTrackingOnBooking) {
          this._bookingService.getQualityMonitoringInformation(guestBookingKey).subscribe((res: JsonResponse) => {
            loading(false)
            const { returnId, returnText } = res
            if (returnId > 0) {
              this.qualityMonitorData = JSON.parse(returnText)
              this.monitorData = this.qualityMonitorData
            } else {
              this._toastr.error(returnText, 'Error')
              this.qualityMonitorData = {
                AlertData: [],
                MonitoringData: [],
                ProgressIndicators: [],
                bookingQualityDetails: [],
                qualityAlertCountDetails: []
              }
              this.monitorData = this.qualityMonitorData
            }
            this.isTracking = true;
            this.isContDtl = false
          }, (error: HttpErrorResponse) => {
            loading(false)
            this._toastr.error(error.message, 'Error')
            this.qualityMonitorData = {
              AlertData: [],
              MonitoringData: [],
              ProgressIndicators: [],
              bookingQualityDetails: [],
              qualityAlertCountDetails: []
            }
            this.monitorData = this.qualityMonitorData
            this.isTracking = true;
            this.isContDtl = false
          })
        }

        break;
      case 'view':
        this.isTracking = false
        this.isContDtl = false
        break;
      case 'container':
        this.isTracking = true;
        this.isContDtl = true
        break;
      default:
        break;
    }
  }

  ngOnDestroy() {
    try {
      localStorage.removeItem('curr_mode')
    } catch (error) { }
  }

}
