import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { ToastrService } from 'ngx-toastr';
import { getImagePath, ImageSource, ImageRequiredSize } from '../../../../constants/globalFunctions';



@Component({
  selector: "app-multi-airlines",
  templateUrl: "./multi-airlines.component.html",
  styleUrls: ["./multi-airlines.component.scss"]
})
export class MultiAirlinesComponent implements OnInit {
  closeResult: string;

  selectedTypeBasis: string = 'FOB'
  desc: string = ''
  selectedFrequency: string = 'Daily'
  chargeableWeight: number = 0
  @Input() airlines: Array<MultiAirline> = [];

  isViewOnly: boolean = false

  constructor(
    private _activeModal: NgbActiveModal,
    private _toast: ToastrService,
  ) { }

  ngOnInit() {

  }

  getCarrierImage($image: string) {
    return getImagePath(ImageSource.FROM_SERVER, $image, ImageRequiredSize.original)
  }


  closeModal() {
    this._activeModal.close(false);
  }
}

export interface MultiAirline {
  CarrierID: number;
  CarrierName: string;
  CarrierImage: string;
  TransfersStop: number;
  SortingOrder: number;
  EtaInDays: number;
  ProductName: string;
  PolModeOfTrans: string;
  PolID: number;
  PolCode: string;
  PolName: string;
  PolCountryCode: string;
  PolCountryName: string;
  PodModeOfTrans: string;
  PodID: number;
  PodCode: string;
  PodName: string;
  PodCountryCode: string;
  PodCountryName: string;
  MaxTransitDays: number;
  MinTransitDays: number;
  JsonPortVia: string;
  AircraftTypeCode?: string;
}