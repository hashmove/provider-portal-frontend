import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestSpotRatesComponent } from './request-spot-rates.component';

describe('RequestSpotRatesComponent', () => {
  let component: RequestSpotRatesComponent;
  let fixture: ComponentFixture<RequestSpotRatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestSpotRatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestSpotRatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
