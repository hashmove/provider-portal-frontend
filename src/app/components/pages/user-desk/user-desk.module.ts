import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UserDeskComponent } from './user-desk.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserDeskRoutingModule } from './user-desk-routing.module';
import { ViewBookingService } from './view-booking/view-booking.service';
import { DashboardService } from './dashboard/dashboard.service';
import { AgmCoreModule } from '@agm/core';
import { UserGuard } from '../user-creation/user.guard';
import { AllBookingsComponent } from './all-bookings/all-bookings.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { ReportsComponent } from './reports/reports.component';
import { SupportComponent } from './support/support.component';
import { DealsComponent } from './deals/deals.component';
import { BillingComponent } from './billing/billing.component';
import { SettingsComponent } from './settings/settings.component';
import { NgxEchartsModule } from 'ngx-echarts';
import { DataMapComponent } from './data-map/data-map.component';
import { SharedModule } from '../../../shared/shared.module';
import { ReportsService } from './reports/reports.service';
import { DataTablesModule } from 'angular-datatables';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BillingService } from './billing/billing.service';
import { NgFilesModule } from '../../../directives/ng-files';
import { SettingService } from './settings/setting.service';
import { QuillEditorModule } from 'ngx-quill-editor';
import { UiSwitchModule } from 'ngx-toggle-switch';
import { PaymentResultComponent } from './payment-result/payment-result.component';
import { WarehouseService } from './manage-rates/warehouse-list/warehouse.service';
import { BookingsCardComponent } from './bookings-card/bookings-card.component';
import { DynamicScriptLoaderService } from '../../../services/dynamic-script-loader.service';
import { PipeModule } from '../../../constants/pipe/pipe.module';
import { MobileNavComponent } from './mobile-nav/mobile-nav.component';
import { LightboxModule } from 'ngx-lightbox';
import { ScrollbarModule } from 'ngx-scrollbar';
import { MakeBookingComponent } from './make-booking/make-booking.component';
import { MakeBookingAirComponent } from './make-booking-air/make-booking-air.component';
import { CustomersComponent } from './customers/customers.component'
import { CustomersService } from './customers/customers.service';
import { ViaPortComponent } from './via-ports/via-port.component';
import { PartnersComponent } from './partner/partner.component';
import { BookingsFilterComponent } from './bookings-filter/bookings-filter.component';
import { ProviderUsersComponent } from './provider-users/provider-users.component';
import { ColorPickerModule } from 'ngx-color-picker';
import { GoogleAnalyticsComponent } from './google-analytics/google-analytics.component';
import { SeaFreightService } from './manage-rates/sea-freight/sea-freight.service';
import { ManageRatesService } from './manage-rates/manage-rates.service';
import { RateRequestListComponent } from './rate-request-list/rate-request-list.component';
import { RateRequestCardComponent } from './rate-request-card/rate-request-card.component';
import { ScheduleCardComponent } from './schedule-card/schedule-card.component';
import { RateRequestCardChildComponent } from './rate-request-card-child/rate-request-card-child.component';
import { ScheduleCardV2Component } from './schedule-card-v2/schedule-card-v2.component';
import { PowerBiComponent } from './power-bi.component/power-bi.component';
import { AirFreightService } from './manage-rates/air-freight/air-freight.service';
import { BookingsFilterRequestComponent } from './bookings-filter-request/bookings-filter-request.component';
import { InvoiceCardComponent } from './invoice-card/invoice-card.component';
import { InvoiceListComponent } from './invoice-list/invoice-list.component';
import { InvoiceFilterComponent } from './invoice-filter/invoice-filter.component';
import { InvoiceUploadComponent } from '../../../shared/dialogues/invoice-upload/invoice-upload.component';
import { InvoicePaymentComponent } from '../../../shared/dialogues/invoice-payment/invoice-payment.component';
import { UserRouteGuard } from '../../../services/user-routes.guard';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    NgFilesModule,
    QuillEditorModule,
    UserDeskRoutingModule,
    UiSwitchModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBs4tYXYGUA2kDvELgCYcbhYeoVgZCxumg',
      libraries: ["places", "geometry"]
    }),
    NgxPaginationModule,
    NgxEchartsModule,
    SharedModule,
    DataTablesModule,
    FormsModule,
    ReactiveFormsModule,
    PipeModule,
    LightboxModule,
    ScrollbarModule,
    ColorPickerModule,
  ],
  declarations: [
    UserDeskComponent,
    SideBarComponent,
    DashboardComponent,
    // ViewBookingComponent,
    AllBookingsComponent,
    RateRequestListComponent,
    InvoiceListComponent,
    ReportsComponent,
    PowerBiComponent,
    SupportComponent,
    DealsComponent,
    BillingComponent,
    SettingsComponent,
    MobileNavComponent,
    DataMapComponent,
    PaymentResultComponent,
    BookingsCardComponent,
    InvoiceCardComponent,
    // CountDownComponent,
    // ScheduleCardComponent,
    ScheduleCardV2Component,
    RateRequestCardComponent,
    RateRequestCardChildComponent,
    MakeBookingComponent,
    MakeBookingAirComponent,
    CustomersComponent,
    PartnersComponent,
    BookingsFilterComponent,
    InvoiceFilterComponent,
    BookingsFilterRequestComponent,
    ProviderUsersComponent,
    GoogleAnalyticsComponent,
    InvoiceUploadComponent,
    InvoicePaymentComponent
    // ViaPortComponent
  ],
  entryComponents: [
    InvoiceUploadComponent,
    InvoicePaymentComponent
  ],
  providers: [
    ViewBookingService,
    DashboardService,
    ReportsService,
    UserGuard,
    BillingService,
    SettingService,
    WarehouseService,
    DynamicScriptLoaderService,
    CustomersService,
    SeaFreightService,
    AirFreightService,
    ManageRatesService,
    UserRouteGuard
  ]
})
export class UserDeskModule { }
