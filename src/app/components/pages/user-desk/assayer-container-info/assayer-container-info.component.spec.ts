import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssayerContainerInfoComponent } from './assayer-container-info.component';

describe('AssayerContainerInfoComponent', () => {
  let component: AssayerContainerInfoComponent;
  let fixture: ComponentFixture<AssayerContainerInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssayerContainerInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssayerContainerInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
