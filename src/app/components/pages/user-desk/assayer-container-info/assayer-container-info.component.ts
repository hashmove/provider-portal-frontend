import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-assayer-container-info',
  templateUrl: './assayer-container-info.component.html',
  styleUrls: ['./assayer-container-info.component.scss']
})
export class AssayerContainerInfoComponent implements OnInit {

  @Input() bookingDetails
  @Input() history
  public totalCount: number = 0
  public hasContainers: boolean = false
  constructor(
    public _activeModal: NgbActiveModal,
  ) { }

  ngOnInit() {
    console.log(this.history)
  }

  close() {
    this._activeModal.close()
  }




}
