import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { DashboardService } from './dashboard/dashboard.service';
import { SharedService } from '../../../services/shared.service';
import { ProviderAst } from '@angular/compiler';
import { JsonResponse } from '../../../interfaces/JsonResponse';
import { getLoggedUserData, isMobile, loading } from '../../../constants/globalFunctions';
import { GuestService, isUserLogin } from '../../../services/jwt.injectable';
import { CommonService } from '../../../services/common.service';
import { NgbTooltipConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-user-desk',
  templateUrl: './user-desk.component.html',
  styleUrls: ['./user-desk.component.scss']
})
export class UserDeskComponent implements OnInit, OnDestroy {

  constructor(
    private _dashboardService: DashboardService,
    private _sharedService: SharedService,
    private _commonService: CommonService,
    private _tooltipConfig: NgbTooltipConfig
  ) { }

  userProfile

  ngOnInit() {
    if (isUserLogin()) {
      this.userProfile = getLoggedUserData()
      this.getDashboardData(this.userProfile.UserID);
      this._commonService.getExpiringRateCount(this.userProfile.ProviderID).subscribe((res: JsonResponse) => {
        if (res.returnId > 0)
          this._sharedService.expNotifications.next(res.returnObject)
      })
    }
  }

  getDashboardData(id) {
    UserDeskComponent.userDasboardDataApiCall(id, this._dashboardService, this._sharedService)

    this._dashboardService.getCustomerShippingData(this.userProfile.ProviderID, 'PROVIDER').subscribe((res: JsonResponse) => {
      const { returnId, returnText, returnObject } = res
      if (returnId > 0) {
        localStorage.setItem('shippingCategories', returnText)
      }
    })
  }

  static userDasboardDataApiCall(id, _dashboardService, _sharedService) {
    loading(true)
    _dashboardService.getdashboardDetails(id).subscribe((res: JsonResponse) => {
      const _href = location.href
      if (!_href.includes('bookings') && !_href.includes('rate-request')) {
        loading(false)
      }

      if (res.returnId > 0) {
        _sharedService.dashboardDetail.next(res.returnObject);
        try {
          const { ProfileID } = res.returnObject
          SharedService.PROFILE_ID = ProfileID
          SharedService.SET_PROFILE_ID(ProfileID)
          _sharedService._profile_id.next(ProfileID)
          GuestService.setSourceVia()
        } catch (error) {

        }
        try {
          localStorage.setItem('isVirtualAirline', JSON.stringify(res.returnObject.IsVirtualAirLine))
        } catch (error) {

        }
      }
    }, (err: HttpErrorResponse) => {
      console.log(err);
      loading(false)
    })
  }

  ngOnDestroy() { }

}

export interface WarehouseRateDetail {
  WHID: number;
  WarehouseRateCount: number;
  WarehouseRateExpiredCount: number;
}

export interface IExpiryNotification {
  TotalRateExpiredCount: number;
  SeaRateCount: number;
  SeaRateExpiredCount: number;
  LCLRateCount: number;
  LCLRateExpiredCount: number;
  FCLRateCount: number;
  FCLRateExpiredCount: number;
  AirRateCount: number;
  AirRateExpiredCount: number;
  GroundRateCount: number;
  GroundRateExpiredCount: number;
  WarehouseRateCount: number;
  WarehouseRateExpiredCount: number;
  WarehouseRateDetail: WarehouseRateDetail[];
}