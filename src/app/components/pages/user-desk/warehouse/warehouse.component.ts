import { Component, OnInit, ViewChild, NgZone, ViewEncapsulation, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { MapsAPILoader } from '@agm/core';
import { } from 'googlemaps';
import { loading, isJSON, getImagePath, ImageRequiredSize, ImageSource, EMAIL_REGEX, getLoggedUserData, isMobile } from '../../../../constants/globalFunctions';
import { Observable } from 'rxjs';
import { WarehouseService } from '../manage-rates/warehouse-list/warehouse.service';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { UserCreationService } from '../../user-creation/user-creation.service';
import { SharedService } from '../../../../services/shared.service';
import { NgFilesService, NgFilesConfig, NgFilesStatus, NgFilesSelected } from '../../../../directives/ng-files';
import { DocumentFile } from '../../../../interfaces/document.interface';
import { JsonResponse } from '../../../../interfaces/JsonResponse';
import { BasicInfoService } from '../../user-creation/basic-info/basic-info.service';
import { baseExternalAssets } from '../../../../constants/base.url';
import { ConfirmDeleteDialogComponent } from '../../../../shared/dialogues/confirm-delete-dialog/confirm-delete-dialog.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Lightbox } from 'ngx-lightbox';
import { SeaRateDialogComponent } from '../../../../shared/dialogues/sea-rate-dialog/sea-rate-dialog.component';
import { WarehouseRateDialogComponent } from '../../../../shared/dialogues/warehouse-rate-dialog/warehouse-rate-dialog.component';
import { cloneObject } from '../reports/reports.component';
import { RateValidityComponent } from '../../../../shared/dialogues/rate-validity/rate-validity.component';
import { CommonService } from '../../../../services/common.service';
import { SeaFreightService } from '../manage-rates/sea-freight/sea-freight.service';
import { CurrencyControl } from '../../../../services/currency.service';
import { VideoDialogueComponent } from '../../../../shared/dialogues/video-dialogue/video-dialogue.component';
import { ManageRatesService } from '../manage-rates/manage-rates.service';
import { RateHistoryComponent } from '../../../../shared/dialogues/rate-history/rate-history.component';
import { WarehouseAddCharges } from '../../../../interfaces/warehouse.interface';
import { of } from 'rxjs/observable/of';
import { compareValues } from '../billing/billing.component';
import { CountryDropdown } from '../../../../shared/country-select/country-select.component';

@Component({
  selector: 'app-warehouse',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './warehouse.component.html',
  styleUrls: ['./warehouse.component.scss']
})
export class WarehouseComponent implements OnInit {
  @Input() whID: any;
  @Input() step: number;
  @Output() wareHouseTemplateChange = new EventEmitter();
  @ViewChild('stepper') public _stepper: any;
  @ViewChild('searchElement') public searchElement: any;
  @ViewChild('city') cityValue: ElementRef

  @ViewChild('_wh_name') _wh_name: ElementRef
  @ViewChild('_wh_phone') _wh_phone: ElementRef
  @ViewChild('_wh_email') _wh_email: ElementRef

  public requiredFields: string = "This field is required";

  public baseExternalAssets: string = baseExternalAssets;
  public zoomlevel: number = 5;
  private userProfile: any
  public warehouseTypeFull: boolean = true;
  public leaseTerm: any[] = [];
  public units: any[] = [];
  public facilities: any[] = [];
  public warehouseUsageType: any[] = [];
  public ceilingsHeight: any[] = [];
  // public cityList: any[] = [];
  public currencyList: any[] = [];
  public selectedMiniLeaseTerm: any;
  public isRealEstate: boolean = false;
  public fixedAmount = true

  isMobile = isMobile()

  //generalForm
  public generalForm: any;

  // locationForm
  public locationForm: any;

  // propertyDetailForm
  public propertyDetailForm: any;

  //commisionForm
  public commissionForm: any

  //map working
  public location: any = { lat: undefined, lng: undefined };
  public draggable: boolean = true;

  public geoCoder: any;


  // form Errors
  public whNameError: boolean = false;
  public whDetailError: boolean = false;
  public cityError: boolean = false;
  public addressError: boolean = false;
  public poBoxError: boolean = false;
  public warehouseSpaceError: boolean = false;
  public hashmoveSpaceError: boolean = false;
  public minLeaseValueOneError: boolean = false;
  public minLeaseValueTwoError: boolean = false;
  public minLeaseValueThreeError: boolean = false;
  public minWareContainerError: boolean = false;
  public commissionValueError: boolean = false;
  public percentValueError: boolean = false;
  public cityValidation: string;

  // space validation
  public hashmovespaceMsg: string = undefined;
  public leaseSpaceMsg: string = undefined;
  public offeredHashmoveSpaceUnit: string = 'sqft';


  //facilities
  public checkedFacilities: boolean = false;


  // gallery
  private fileStatus = undefined;
  private docTypeId = null;
  public uploadedGalleries: any[] = [];
  public warehouseDocx: any = {};


  // edit warehouse detail

  public warehouseDetail: any;
  public activeStep: number = this.step



  public config: NgFilesConfig = {
    acceptExtensions: ['jpg', 'png', 'bmp', 'mp4'],
    maxFilesCount: 5,
    maxFileSize: 5 * 1024 * 1000,
    totalFilesSize: 5 * 5 * 1024 * 1000
  };

  //term & cond.
  private toolbarOptions = [
    ['bold', 'italic', 'underline'],        // toggled buttons

    [{ 'list': 'ordered' }, { 'list': 'bullet' }],
    [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
    [{ 'direction': 'rtl' }],                         // text direction

    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

    [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
    [{ 'align': [] }],

    ['clean']                                         // remove formatting button
  ];

  public editorOptions = {
    placeholder: "insert content...",
    modules: {
      toolbar: this.toolbarOptions
    }
  };
  public editorContent: any;
  public disableEditor: boolean = false;




  warehouseCharges: any;
  publishloading: boolean;
  fromDate: any;
  toDate: any;
  filterbyCustomer: any;
  isCustomer: any;
  isMarketplace: any;
  totalPublishedRecords: any;
  checkedallpublishRates: boolean;
  public warehousePublishedRates: any[] = []
  autocomplete: any

  public defaultCurrency: any = {
    id: 101,
    shortName: "USD",
    imageName: 'US'
  };
  public selectedCurrency: any = this.defaultCurrency;
  public allCurrencies: any[] = [];
  public checkAllPublish: boolean = false
  minLeaseTermCont
  hasContainer: boolean = false
  warehouseUserInfo
  countryList: any[] = []
  countryFlagImage
  phoneCode
  transPhoneCode
  phoneCountryId
  selCountry: CountryDropdown

  whUsers: IWhUser[] = []
  selectedWHUser: IWhUser
  isFullWarehouse = false

  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private _redirect: Router,
    private _warehouseService: WarehouseService,
    private _toastr: ToastrService,
    private _sharedService: SharedService,
    private _userCreationService: UserCreationService,
    private _basicInfoService: BasicInfoService,
    private ngFilesService: NgFilesService,
    private _modalService: NgbModal,
    private _lightbox: Lightbox,
    private _commonService: CommonService,
    private _seaFreightService: SeaFreightService,
    private _currencyControl: CurrencyControl,
    private _manageRatesService: ManageRatesService,

  ) { }

  ngOnInit() {
    this.ngFilesService.addConfig(this.config, 'config');
    this._userCreationService.getAllUsers(getLoggedUserData().UserID).subscribe((res: JsonResponse) => {
      const { returnId, returnStatus } = res
      if (res.returnObject && res.returnObject.length > 0) {
        this.whUsers = res.returnObject
        this.whUsers.forEach(element => {
          if (element.userImage) {
            element.userImage = baseExternalAssets + element.userImage;
          }
        });
        this.whUsers = this.whUsers.sort(
          compareValues("firstName", "asc")
        );
      }
    });
    this.userProfile = getLoggedUserData()
    this.selectedCurrency = JSON.parse(localStorage.getItem("userCurrency"));
    this.getProviderSettings()
    if (this.step === 1) {
      this.activeStep = this.step
      this.getPricingDetails()
    }
    this._sharedService.getLocation.subscribe((state: any) => {
      if (state && state.country) {
        this.getMapLatlng(state.country);
      }
    })
    this._sharedService.currencyList.subscribe((state: any) => {
      if (state) {
        this.currencyList = state;
      }
    });
    this._commonService.getCountry().subscribe((countryList: any) => {
      if (countryList && countryList.length) {
        countryList.map((obj) => {
          if (typeof (obj.desc) == "string") {
            obj.desc = JSON.parse(obj.desc);
          }
        })
        // console.log(this.countryList)
        this._sharedService.countryList.next(countryList);
        this.setCountryListConfig(countryList)
      }
    })


    this._sharedService.countryList.subscribe((state: any) => {
      if (state) {
        // console.log(this.countryList)
        this.setCountryListConfig(state)
      }
    });

    let countryBound: string
    this.getplacemapLoc(countryBound);
    this.getDetail();

    // this._sharedService.cityList.subscribe((state: any) => {
    //   if (state) {

    //   }
    // });
    this.generalForm = new FormGroup({
      whName: new FormControl(null, [Validators.required, Validators.maxLength(100), Validators.minLength(5), Validators.pattern(/[a-zA-Z]/)]),
      whDetail: new FormControl(null, [Validators.required, Validators.maxLength(1000), Validators.minLength(10)]),
    });
    this.locationForm = new FormGroup({
      city: new FormControl(null, [Validators.required, Validators.maxLength(50), Validators.minLength(5), Validators.pattern(/^(?=.*?[a-zA-Z])[^%*$=+^<>}{]+$/)]),
      address: new FormControl(null, [Validators.required, Validators.maxLength(200), Validators.minLength(1), Validators.pattern(/^(?=.*?[a-zA-Z])[^%*$=+^<>}{]+$/)]),
      poBox: new FormControl(null, [Validators.required, Validators.maxLength(16), Validators.minLength(4)]),
    });
    this.propertyDetailForm = new FormGroup({
      warehouseSpace: new FormControl(null, [Validators.required, Validators.min(1), Validators.max(10000000)]),
      warehouseSpaceUnit: new FormControl(null, [Validators.required]),
      // hashmoveSpace: new FormControl(null, [warehouseValidator.bind(this)]),
      hashmoveSpace: new FormControl(null, [Validators.min(1), Validators.max(10000000)]),
      ceilingHeight: new FormControl(null),
      // ceilingUnit: new FormControl(null),
      minLeaseValueOne: new FormControl(null, [this.warehouseValidator.bind(this, 'sqft')]),
      minLeaseValueTwo: new FormControl(null, [this.warehouseValidator.bind(this, 'sqm')]),
      minLeaseValueThree: new FormControl(null, [this.warehouseValidator.bind(this, 'pallet')]),
      minWareContainer: new FormControl(null, [this.warehouseValidator.bind(this, 'Container')]),
    });
    this.warehouseUserInfo = new FormGroup({
      name: new FormControl({ value: null }, [Validators.required, Validators.minLength(2), Validators.maxLength(100)]),
      email: new FormControl({ value: null, disabled: true }, [
        Validators.required,
        Validators.pattern(EMAIL_REGEX),
        Validators.minLength(1),
        Validators.maxLength(320)
      ]),
      contact: new FormControl({ value: null, disabled: true }, [Validators.pattern(/^(?!(\d)\1+(?:\1+){0}$)\d+(\d+){0}$/), Validators.minLength(7), Validators.maxLength(13)]),
    });

    Observable.fromEvent(this.cityValue.nativeElement, "focusout")
      // get value
      .map((evt: any) => evt.target.value)
      // text length must be > 2 chars
      //.filter(res => res.length > 2)
      // emit after 1s of silence
      .debounceTime(200)
      // emit only if data changes since the last emit
      // subscription
      .subscribe((text: string) => {
        this.checkCity();
      });
  }
  getProviderSettings() {
    try {
      if (localStorage.getItem('customerSettings')) {
        const _customerSettings = JSON.parse(localStorage.getItem('customerSettings'))
        if (_customerSettings.isWHUsageTypeShowOnSearch) {
          this.isFullWarehouse = true
        } else {
          this.isFullWarehouse = false
        }
      } else {
        this.isFullWarehouse = false
      }
    } catch {
      this.isFullWarehouse = false
    }
  }

  setCountryListConfig(state) {
    this.countryList = state;
    const userProfile = getLoggedUserData()
    if (!this.warehouseDetail || (this.warehouseDetail && !this.warehouseDetail.WHContactInfo)) {
      let selectedCountry = this.countryList.find(obj => obj.id == userProfile.CountryID);
      this.selectPhoneCode(selectedCountry);
    } else {
      let selectedCountry = this.countryList.find(obj => obj.code == this.warehouseDetail.WHContactInfo.CountryCode);
      this.selectPhoneCode(selectedCountry);
    }
  }

  selectPhoneCode(country) {
    try {
      this.countryFlagImage = country.code;
      let description = country.desc;
      this.phoneCode = description[0].CountryPhoneCode;
      this.transPhoneCode = description[0].CountryPhoneCode_OtherLang;
      this.phoneCountryId = country.id
      this.selCountry = country
    } catch (error) {

    }
    // console.log(this.selCountry)
  }

  onContentChanged({ quill, html, text }) {
    this.editorContent = html
    // console.log(this.editorContent)
  }

  isEmpty(htmlString) {
    if (htmlString) {
      const parser = new DOMParser();
      const { textContent } = parser.parseFromString(htmlString, "text/html").documentElement;
      return !textContent.trim();
    } else {
      return true;
    }
  };

  saveTermNcond() {
    let obj = {
      providerID: this.userProfile.ProviderID,
      termsAndConditions: this.editorContent,
      transportType: "WAREHOUSE",
      modifiedBy: this.userProfile.UserID
    }
    this._manageRatesService.termNCondition(obj).subscribe((res: any) => {
      if (res.returnStatus == "Success") {
        this._toastr.success("Term and Condition saved Successfully", "");
        this._sharedService.termNcondLCL.next(this.editorContent);
        // this.disableEditor = true;
      }
    })
  }

  commissionFormGenerate() {
    this.commissionForm = new FormGroup({
      commissionCurrency: new FormControl(this.selectedCurrency, [warehouseValidatorCommissionCurr.bind(this)]),
      commissionValue: new FormControl(null, [warehouseValidatorCommissionVal.bind(this)]),
      percentValue: new FormControl(null, [warehouseValidatorCommission.bind(this)]),
    });
  }

  getDetail() {
    if (this.whID) {
      this.getWareHouseDetail(this.userProfile.ProviderID, this.whID);
    } else {
      this.editorContent = null
    }
  }

  errorValidate() {
    if (this.generalForm.controls.whName.status == "INVALID" && this.generalForm.controls.whName.touched) {
      this.whNameError = true;
    }
    if (this.generalForm.controls.whDetail.status == "INVALID" && this.generalForm.controls.whDetail.touched) {
      this.whDetailError = true;
    }
    if (this.locationForm.controls.city.status == "INVALID" && this.locationForm.controls.city.touched) {
      this.cityError = true;
    }
    if (this.locationForm.controls.address.status == "INVALID" && this.locationForm.controls.address.touched) {
      this.addressError = true;
    }
    if (this.locationForm.controls.poBox.status == "INVALID" && this.locationForm.controls.poBox.touched) {
      this.poBoxError = true;
    }
    if (this.propertyDetailForm.controls.warehouseSpace.status == "INVALID" && this.propertyDetailForm.controls.warehouseSpace.touched) {
      this.warehouseSpaceError = true;
    }
    if (this.propertyDetailForm.controls.hashmoveSpace.status == "INVALID" && this.propertyDetailForm.controls.hashmoveSpace.touched) {
      this.hashmoveSpaceError = true;
    }
    if (this.propertyDetailForm.controls.minLeaseValueOne.status == "INVALID" && this.propertyDetailForm.controls.minLeaseValueOne.touched) {
      this.minLeaseValueOneError = true;
    }
    if (this.propertyDetailForm.controls.minLeaseValueTwo.status == "INVALID" && this.propertyDetailForm.controls.minLeaseValueTwo.touched) {
      this.minLeaseValueTwoError = true;
    }
    if (this.propertyDetailForm.controls.minLeaseValueThree.status == "INVALID" && this.propertyDetailForm.controls.minLeaseValueThree.touched) {
      this.minLeaseValueThreeError = true;
    }
    if (this.propertyDetailForm.controls.minWareContainer.status == "INVALID" && this.propertyDetailForm.controls.minWareContainer.touched) {
      this.minWareContainerError = true;
    }
    if (this.isRealEstate && this.commissionForm.controls.commissionValue.status == "INVALID" && this.commissionForm.controls.commissionValue.touched) {
      this.commissionValueError = true;
    }
    if (this.isRealEstate && this.commissionForm.controls.percentValue.status == "INVALID" && this.commissionForm.controls.percentValue.touched) {
      this.percentValueError = true;
    }
  }
  spaceValidate() {
    if (this.warehouseTypeFull) return;
    if (Number(this.propertyDetailForm.controls.warehouseSpace.value) && Number(this.propertyDetailForm.controls.warehouseSpace.value) < Number(this.propertyDetailForm.controls.hashmoveSpace.value)) {
      this.propertyDetailForm.controls.hashmoveSpace.status = 'INVALID';
      this.hashmoveSpaceError = true;
      this.hashmovespaceMsg = "Offered space should be less than or equal to Total warehouse Space";
      this.validateLeaseTerm();
    }
    else if (Number(this.propertyDetailForm.controls.warehouseSpace.value) && Number(this.propertyDetailForm.controls.hashmoveSpace.value) && Number(this.propertyDetailForm.controls.hashmoveSpace.value) > Number(this.propertyDetailForm.controls.warehouseSpace.value)) {
      this.propertyDetailForm.controls.hashmoveSpace.status = 'INVALID';
      this.hashmoveSpaceError = true;
      this.hashmovespaceMsg = "Total warehouse Space should be greater than offered space";
      this.validateLeaseTerm();
    }
    else {
      this.validateLeaseTerm();
      this.hashmovespaceMsg = undefined;
    }
  }
  validateLeaseTerm() {
    if (this.offeredHashmoveSpaceUnit == 'sqft') {
      this.minLeaseValueTwoError = false;
      if (Number(this.propertyDetailForm.controls.minLeaseValueOne.value <= Number(this.propertyDetailForm.controls.hashmoveSpace.value))) {
        this.leaseSpaceMsg = undefined;
      }
      else if (Number(this.propertyDetailForm.controls.minLeaseValueOne.value && Number(this.propertyDetailForm.controls.minLeaseValueOne.value) > Number(this.propertyDetailForm.controls.hashmoveSpace.value))) {
        this.leaseSpaceMsg = "Minimum lease value should be less than or equal to Offered space";
        this.minLeaseValueOneError = true;

      }
    }
    else if (this.offeredHashmoveSpaceUnit == 'sqm') {
      this.minLeaseValueOneError = false;
      if (Number(this.propertyDetailForm.controls.minLeaseValueTwo.value <= Number(this.propertyDetailForm.controls.hashmoveSpace.value))) {
        this.leaseSpaceMsg = undefined;
      }
      else if (Number(this.propertyDetailForm.controls.minLeaseValueTwo.value && Number(this.propertyDetailForm.controls.minLeaseValueTwo.value) > Number(this.propertyDetailForm.controls.hashmoveSpace.value))) {
        this.leaseSpaceMsg = "Minimum lease value should be less than or equal to Offered space";
        this.minLeaseValueTwoError = true;
      }
    } else if (this.offeredHashmoveSpaceUnit == 'pallet') {
      this.minLeaseValueThreeError = false;
      if (Number(this.propertyDetailForm.controls.minLeaseValueThree.value <= Number(this.propertyDetailForm.controls.hashmoveSpace.value))) {
        this.leaseSpaceMsg = undefined;
      }
      else if (Number(this.propertyDetailForm.controls.minLeaseValueThree.value && Number(this.propertyDetailForm.controls.minLeaseValueThree.value) > Number(this.propertyDetailForm.controls.hashmoveSpace.value))) {
        this.leaseSpaceMsg = "Minimum 1 pallet is required";
        this.minLeaseValueThreeError = true;
      }
    } else if (this.offeredHashmoveSpaceUnit.toLowerCase() == 'container') {
      this.minWareContainerError = false;
      if (Number(this.propertyDetailForm.controls.minWareContainer.value <= Number(this.propertyDetailForm.controls.hashmoveSpace.value))) {
        this.leaseSpaceMsg = undefined;
      }
      else if (Number(this.propertyDetailForm.controls.minWareContainer.value && Number(this.propertyDetailForm.controls.minWareContainer.value) > Number(this.propertyDetailForm.controls.hashmoveSpace.value))) {
        this.leaseSpaceMsg = "Minimum 1  is required";
        this.minWareContainerError = true;
      }
    }
  }
  minLeaseSpaceValidate(type) {
    if (this.warehouseTypeFull) return;
    if (type == 'sqft' && this.offeredHashmoveSpaceUnit == 'sqft') {
      this.minLeaseValueTwoError = false;
      if (Number(this.propertyDetailForm.controls.hashmoveSpace.value) && Number(this.propertyDetailForm.controls.minLeaseValueOne.value) > Number(this.propertyDetailForm.controls.hashmoveSpace.value)) {
        this.propertyDetailForm.controls.minLeaseValueOne.status = 'INVALID';
        this.minLeaseValueOneError = true;
        this.leaseSpaceMsg = "Minimum lease value should be less than or equal to Offered space";
      }
      else {
        this.leaseSpaceMsg = undefined;
      }
    }
    if (type == 'sqm' && this.offeredHashmoveSpaceUnit == 'sqm') {
      this.minLeaseValueOneError = false;
      if (Number(this.propertyDetailForm.controls.hashmoveSpace.value) && Number(this.propertyDetailForm.controls.minLeaseValueTwo.value) > Number(this.propertyDetailForm.controls.hashmoveSpace.value)) {
        this.propertyDetailForm.controls.minLeaseValueTwo.status = 'INVALID';
        this.minLeaseValueTwoError = true;
        this.leaseSpaceMsg = "Minimum lease value should be less than or equal to  Offered space";
      }
      else {
        this.leaseSpaceMsg = undefined;
      }
    }

    if (type == 'pallet' && this.offeredHashmoveSpaceUnit == 'pallet') {
      this.minLeaseValueThreeError = false;
      if (Number(this.propertyDetailForm.controls.hashmoveSpace.value) && Number(this.propertyDetailForm.controls.minLeaseValueThree.value) > Number(this.propertyDetailForm.controls.hashmoveSpace.value)) {
        this.propertyDetailForm.controls.minLeaseValueThree.status = 'INVALID';
        this.minLeaseValueThreeError = true;
        this.leaseSpaceMsg = "Minimum lease value should be less than or equal to  Offered space";
      }
      else {
        this.leaseSpaceMsg = undefined;
      }
    }
    if (type.toLowerCase() == 'container' && this.offeredHashmoveSpaceUnit.toLowerCase() == 'container') {
      this.minWareContainerError = false;
      // console.log('asdas')
      if (Number(this.propertyDetailForm.controls.hashmoveSpace.value) && Number(this.propertyDetailForm.controls.minWareContainer.value) > Number(this.propertyDetailForm.controls.hashmoveSpace.value)) {
        this.propertyDetailForm.controls.minWareContainer.status = 'INVALID';
        this.minWareContainerError = true;
        this.leaseSpaceMsg = "Minimum lease value should be less than or equal to  Offered space";
      }
      else {
        this.leaseSpaceMsg = undefined;
      }
    }

  }
  getplacemapLoc(countryBound) {
    this.mapsAPILoader.load().then(() => {
      this.geoCoder = new google.maps.Geocoder;
      this.autocomplete = new google.maps.places.Autocomplete(this.searchElement.nativeElement);
      try {
        if (countryBound) {
          this.autocomplete.setComponentRestrictions({ 'country': [countryBound.toLowerCase()] });
        }
      } catch (error) {
        console.warn(error);
      }

      this.autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = this.autocomplete.getPlace();
          // console.log(place)
          this.locationForm.controls['address'].setValue(this.searchElement.nativeElement.value);
          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          //set latitude, longitude and zoom
          this.location.lat = place.geometry.location.lat();
          this.location.lng = place.geometry.location.lng();
          this.zoomlevel = 14;
        });
      });
    });

  }
  getMapLatlng(region) {
    this._userCreationService.getLatlng(region).subscribe((res: any) => {
      if (res.status == "OK") {
        this.location = res.results[0].geometry.location;
      }
    })
  }
  markerDragEnd($event) {
    // console.log($event);
    this.geoCoder.geocode({ 'location': { lat: $event.coords.lat, lng: $event.coords.lng } }, (results, status) => {
      // console.log(results);
      // console.log(status);
      if (status === 'OK') {
        if (results[0]) {
          // console.log(results[0].formatted_address);
          // this.locationForm.controls['address'].setValue(results[0].formatted_address);
          // this.searchElement.nativeElement.value = results[0].formatted_address;
          // console.log(this.searchElementRef.nativeElement.value);
          // infowindow.setContent(results[0].formatted_address);
        } else {
          this._toastr.error('No results found', '');
        }
      } else {
        this._toastr.error('Geocoder failed due to: ' + status, '');
      }

    });
  }


  public addPricingButtonText: string = 'save & add pricing'
  getWareHouseDetail(providerId, id) {
    loading(true)
    this._warehouseService.getWarehouseList(providerId, id).subscribe((res: JsonResponse) => {
      loading(false)
      if (res.returnId > 0 && res.returnObject) {
        this.getWarehousetermNcond();
        this.isRealEstate = res.returnObject.IsRealEstate;
        if (this.isRealEstate) {
          this.commissionFormGenerate();
        }
        const data = [
          'WH_MIN_LEASE_TERM',
          'UNIT_AREA',
          'WH_USAGE_TYPE',
          'UNIT_VOLUME',
          'WH_CEILING_HEIGHT'
        ]
        this.warehouseDocx = res.returnObject.documentType;
        if (res.returnObject && !Number(id)) {
          this.facilities = res.returnObject.WHFacilitiesProviding;
          try {
            this.facilities = res.returnObject.WHFacilitiesProviding.filter(_fac => _fac.BusinessLogic !== 'IsFireExtinguisherAvailable' && _fac.BusinessLogic !== 'IsCCTVAvailable')
          } catch (error) {
            this.facilities = res.returnObject.WHFacilitiesProviding;
          }
        }
        else if (Number(id)) {
          // console.log('here')
          this.addPricingButtonText = 'update'
          this._sharedService.mainScrollListener.next(true)
          this.warehouseDetail = res.returnObject.WHModel[0];
          // console.log(this.warehouseDetail)
          try {
            if (this.warehouseDetail.WHContactInfo) {
              // CompanyID: 133
              // ContactNumber: "+923330376538"
              // CountryCode: "PK"
              // CountryPhoneCode: "+92"
              // Email: "skashan@texpo.com"
              // PhoneCodeCountryID: 100
              // UserID: 139
              // UserName: "Masroor Durani"

              const { WHContactInfo } = this.warehouseDetail
              const { whUsers } = this

              this.selectedWHUser = whUsers.filter(_user => _user.userID === WHContactInfo.UserID)[0]
              const { selectedWHUser } = this

              if (selectedWHUser) {
                this.warehouseUserInfo.controls['name'].patchValue(selectedWHUser);
                this.warehouseUserInfo.controls['email'].patchValue(selectedWHUser.emailAddress ? selectedWHUser.emailAddress : '');
                this.warehouseUserInfo.controls['contact'].patchValue(selectedWHUser.phoneNumber ? selectedWHUser.phoneNumber : '');

                let selectedCountry = this.countryList.find(obj => obj.code == WHContactInfo.CountryCode);
                this.selectPhoneCode(selectedCountry);
              }
            }

          } catch (error) {

          }
          // this.warehouseDetail.Location = this.cityList.find(elem => elem.id == this.warehouseDetail.CityID).title
          this.setData(this.warehouseDetail);
          this.setCountryListConfig(this.countryList)
        }
        this.getvaluesDropDown(data);
        this.getPricingDetails()
      }
    }, (err: HttpErrorResponse) => {
      loading(false);
    })
  }
  getWarehousetermNcond() {
    if (this.whID > 0) {
      this._warehouseService.WHtermNconditionV2(this.userProfile.ProviderID, this.whID).subscribe((res: JsonResponse) => {
        if (res.returnId > 0) {
          // console.log(res)
          if (res.returnObject && res.returnObject.TermsCondition) {
            this.editorContent = res.returnObject.TermsCondition;
            // this.disableEditor = true;
          } else {
            // this.disableEditor = false;
          }
        }
      })
    }
  }

  public disableWarehouseSelection: boolean = false
  setData(obj: any) {

    try {
      if (this.warehouseDetail.OfferedHashMoveAreaUnit && this.warehouseDetail.OfferedHashMoveAreaUnit.toLowerCase() === 'container') {
        this.hasContainer = true
      } else if (this.warehouseDetail.TotalCoveredAreaUnit && this.warehouseDetail.TotalCoveredAreaUnit.toLowerCase() === 'container') {
        this.hasContainer = true
      } else {
        this.hasContainer = false
      }
    } catch (error) {
      this.hasContainer = false
    }

    if (obj.Latitude) {
      this.location.lat = Number(obj.Latitude);
    }
    if (obj.Longitude) {
      this.location.lng = Number(obj.Longitude);
    }
    if (this.location && this.location.lat && this.location.lng) {
      this.zoomlevel = 14;
    }
    this.warehouseTypeFull = (obj.UsageType.toUpperCase() == 'SHARED') ? false : true;
    this.fixedAmount = (obj.ComissionType == 'Fixed_Amount') ? true : false;
    if (obj.FacilitiesProviding && isJSON(obj.FacilitiesProviding)) {
      this.facilities = JSON.parse(obj.FacilitiesProviding);
      try {
        this.facilities = this.facilities.filter(_fac => _fac.BusinessLogic !== 'IsFireExtinguisherAvailable' && _fac.BusinessLogic !== 'IsCCTVAvailable')
      } catch (error) {

      }
      this.checkedFacilities = this.facilities.some(obj => obj.IsAllowed == true);
    }
    if (obj.WHName) {
      this.generalForm.controls['whName'].setValue(obj.WHName);
    }
    if (obj.WHDesc) {
      this.generalForm.controls['whDetail'].setValue(obj.WHDesc);
    }
    this.disableWarehouseSelection = true
    // this.allWareHouseList.forEach((element, index) => {
    //   const albumArr = []
    //   const docsArray: Array<any> = JSON.parse(element.WHGallery)
    //   try {
    //     docsArray.forEach(doc => {
    //       if (doc.DocumentUploadedFileType.toLowerCase() === 'png' || doc.DocumentUploadedFileType.toLowerCase() === 'jpg' || doc.DocumentUploadedFileType.toLowerCase() === 'jpeg') {
    //         const album = {
    //           src: baseExternalAssets + '/' + doc.DocumentFile,
    //           caption: '&nbsp;',
    //           thumb: baseExternalAssets + '/' + doc.DocumentFile
    //         };
    //         albumArr.push(album)
    //         this.allWareHouseList[index].WHParsedMedia = albumArr;
    //       } else if (doc.DocumentUploadedFileType.toLowerCase() === 'mp4') {
    //         this.videoURL = baseExternalAssets + '/' + doc.DocumentFile
    //         this.allWareHouseList[index].videoURL = this.videoURL;
    //       }
    //     })
    //   } catch (error) { }
    // });
    if (obj.WHGallery && obj.WHGallery != "[]" && isJSON(obj.WHGallery)) {
      this.uploadedGalleries = JSON.parse(obj.WHGallery);
      const albumArr = []
      this.uploadedGalleries.forEach((elem) => {
        // const album = {
        //   src: baseExternalAssets + elem.DocumentFile,
        //   caption: elem.DocumentFileName,
        //   thumb: baseExternalAssets + elem.DocumentFile,
        //   DocumentUploadedFileType: elem.DocumentUploadedFileType
        // };
        // albumArr.push(album);

        if (elem.DocumentUploadedFileType.toLowerCase() === 'png' || elem.DocumentUploadedFileType.toLowerCase() === 'jpg' || elem.DocumentUploadedFileType.toLowerCase() === 'jpeg') {
          const album = {
            src: baseExternalAssets + elem.DocumentFile,
            // caption: elem.DocumentFileName,
            thumb: baseExternalAssets + elem.DocumentFile,
            DocumentUploadedFileType: elem.DocumentUploadedFileType
          };
          albumArr.push(album);
          // this.warehouseDetail.WHParsedMedia = albumArr
          this.warehouseDetail['WHParsedMedia'] = albumArr;
        } else if (elem.DocumentUploadedFileType.toLowerCase() === 'mp4') {
          this.warehouseDetail.videoURL = baseExternalAssets + '/' + elem.DocumentFile;
        }
      })
      this.docTypeId = this.uploadedGalleries[0].DocumentID;
    }
    if (obj.WHAddress) {
      this.locationForm.controls['address'].setValue(obj.WHAddress);
    }
    if (obj.WHPOBoxNo) {
      this.locationForm.controls['poBox'].setValue(obj.WHPOBoxNo);
    }
    if (obj.City) {
      try {
        const cityObj = (JSON.parse(obj.City))
        const object = {
          id: cityObj.ID,
          code: cityObj.Code,
          title: cityObj.Title,
          shortName: cityObj.ShortName,
          imageName: cityObj.ImageName,
          desc: cityObj.Desc,
          webURL: cityObj.WebURL,
          sortingOrder: cityObj.SortingOrder,
          type: cityObj.Type,
          lastUpdate: cityObj.LastUpdate,
          latitude: cityObj.Latitude,
          longitude: cityObj.Longitude,
          isBaseCurrency: cityObj.IsBaseCurrency,
          jsonServices: cityObj.JsonServices,
          isChinaGateway: cityObj.IsChinaGateway,
          shortPortCode: cityObj.ShortPortCode,
          portCountryCode: cityObj.PortCountryCode,
          roundingOff: cityObj.RoundingOff,
        }
        this.warehouseDetail.Location = object.title
        this.locationForm.controls['city'].setValue(object);
      } catch (error) {

      }
    }
    if (obj.TotalCoveredArea) {
      this.propertyDetailForm.controls['warehouseSpace'].setValue(obj.TotalCoveredArea);
    }
    if (!this.warehouseTypeFull && obj.CeilingHeight) {
      this.propertyDetailForm.controls['ceilingHeight'].setValue(obj.CeilingHeight);
    }
    if (!this.warehouseTypeFull && obj.OfferedHashMoveArea) {
      this.propertyDetailForm.controls['hashmoveSpace'].setValue(obj.OfferedHashMoveArea);
    }
    if (!this.warehouseTypeFull && obj.WHMinSQFT) {
      if (this.warehouseDetail.OfferedHashMoveAreaUnit.toLowerCase() === 'sqft') {
        this.propertyDetailForm.controls['minLeaseValueOne'].setValue(obj.WHMinSQFT);
      } else {
        this.propertyDetailForm.controls['minLeaseValueTwo'].setValue(obj.WHMinSQFT);
      }
    }
    if (!this.warehouseTypeFull && obj.WHMinCBM) {
      this.propertyDetailForm.controls['minLeaseValueTwo'].setValue(obj.WHMinCBM);
    }
    if (!this.warehouseTypeFull && obj.WHMinPallet) {
      this.propertyDetailForm.controls['minLeaseValueThree'].setValue(obj.WHMinPallet);
    }
    if (!this.warehouseTypeFull && obj.WHMinContainer) {
      this.propertyDetailForm.controls['minWareContainer'].setValue(obj.WHMinPallet);
    }

    if (this.isRealEstate && obj.ComissionValue) {
      try {
        this.commissionForm.controls['commissionValue'].setValue(this._currencyControl.applyRoundByDecimal(parseFloat(obj.ComissionValue), 2));
      } catch (error) {
        this.commissionForm.controls['commissionValue'].setValue(0);
      }
    }
    if (this.isRealEstate && obj.ComissionCurrencyID) {
      let object = this.currencyList.find(elem => elem.id == obj.ComissionCurrencyID)
      // this.commissionForm.controls['commissionCurrency'].setValue(object);
      // console.log(this.selectedCurrency)
      this.commissionForm.controls['commissionCurrency'].setValue(this.selectedCurrency);
    }
    if (this.isRealEstate && !this.fixedAmount && obj.Percent) {
      try {
        this.commissionForm.controls['percentValue'].setValue(this._currencyControl.applyRoundByDecimal(parseFloat(obj.Percent), 2));
      } catch (error) {
        this.commissionForm.controls['percentValue'].setValue(0);
      }
    }

  }

  fixedAmountToggler() {
    this.fixedAmount = !this.fixedAmount;
    if (this.fixedAmount) {
      this.commissionForm.controls['percentValue'].reset()
    } else {
      if (this.isRealEstate && !this.fixedAmount && this.warehouseDetail.Percent) {
        this.commissionForm.controls['percentValue'].setValue(this._currencyControl.applyRoundByDecimal(parseFloat(this.warehouseDetail.Percent), 2));
      }
    }
  }

  addFacilities(obj, $event) {
    this.facilities.map((elem) => {
      if (obj.BusinessLogic == elem.BusinessLogic) {
        elem.IsAllowed = $event.target.checked;
      }
    })
    this.checkedFacilities = this.facilities.some(obj => obj.IsAllowed == true);
  }

  addMinimumLeaseTerm(obj) {
    this.selectedMiniLeaseTerm = obj;
  }
  changeWarehouseType() {
    this.warehouseTypeFull = !this.warehouseTypeFull;
    if (this.warehouseTypeFull) {
      this.propertyDetailForm.controls.hashmoveSpace.reset();
      this.propertyDetailForm.controls.minLeaseValueOne.reset();
      this.propertyDetailForm.controls.minLeaseValueTwo.reset();
      this.propertyDetailForm.controls.minLeaseValueThree.reset();
      this.propertyDetailForm.controls.minWareContainer.reset();
      this.leaseTerm = this.allLeaseTerms.filter(e => e.codeValShortDesc !== 'DAY')
      this.leaseTerm = this.leaseTerm.sort((a, b) => a.sortingOrder - b.sortingOrder);
    } else {
      this.leaseTerm = this.allLeaseTerms.sort((a, b) => a.sortingOrder - b.sortingOrder);
    }
  }
  numberValid(evt) {
    try {
      let charCode = (evt.which) ? evt.which : evt.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
      return true;
    } catch (error) {
      return false
    }
  }
  numberValidwithDecimal(evt) {
    let charCode = (evt.which) ? evt.which : evt.keyCode;
    if (evt.target.value && evt.target.value[evt.target.value.length - 1] == '.') {
      if (charCode > 31
        && (charCode < 48 || charCode > 57))
        return false;

      return true;
    }
    if (charCode != 46 && charCode > 31
      && (charCode < 48 || charCode > 57))
      return false;

    return true;
  }
  oneSpaceHandler(event) {
    if (event.target.value) {
      var end = event.target.selectionEnd;
      if (event.keyCode == 32 && (event.target.value[end - 1] == " " || event.target.value[end] == " ")) {
        event.preventDefault();
        return false;
      }
    }
    else if (event.target.selectionEnd == 0 && event.keyCode == 32) {
      return false;
    }
  }
  spaceHandler(event) {
    if (event.charCode == 32) {
      event.preventDefault();
      return false;
    }
  }
  selectedspaceUnit(unit) {
    this.offeredHashmoveSpaceUnit = this.units.find(obj => obj.codeVal == unit).codeValDesc;
    this.resetMinLeaseTerms()
    this.validateLeaseTerm();
  }

  resetMinLeaseTerms() {
    const { offeredHashmoveSpaceUnit } = this
    if (offeredHashmoveSpaceUnit === 'sqft') {
      this.propertyDetailForm.controls['minLeaseValueTwo'].setValue(0)
      this.propertyDetailForm.controls['minLeaseValueThree'].setValue(0)
      this.propertyDetailForm.controls['minWareContainer'].setValue(0)
    } else if (offeredHashmoveSpaceUnit === 'sqm') {
      this.propertyDetailForm.controls['minLeaseValueOne'].setValue(0)
      this.propertyDetailForm.controls['minLeaseValueThree'].setValue(0)
      this.propertyDetailForm.controls['minWareContainer'].setValue(0)
    } else if (offeredHashmoveSpaceUnit === 'pallet') {
      this.propertyDetailForm.controls['minLeaseValueOne'].setValue(0)
      this.propertyDetailForm.controls['minLeaseValueTwo'].setValue(0)
      this.propertyDetailForm.controls['minWareContainer'].setValue(0)
    } else if (offeredHashmoveSpaceUnit && offeredHashmoveSpaceUnit.toLowerCase() === 'container') {
      this.propertyDetailForm.controls['minLeaseValueOne'].setValue(0)
      this.propertyDetailForm.controls['minLeaseValueTwo'].setValue(0)
      this.propertyDetailForm.controls['minLeaseValueThree'].setValue(0)
    }
  }

  public allLeaseTerms = []
  getvaluesDropDown(data) {
    this._warehouseService.getDropDownValuesWarehouse(data).subscribe((res: any) => {
      if (res && res.length) {
        this.allLeaseTerms = res.filter(obj => obj.codeType == 'WH_MIN_LEASE_TERM');
        if (this.isFullWarehouse) {
          this.warehouseUsageType = res.filter(obj => obj.codeType == 'WH_USAGE_TYPE');
          if (!this.whID) {
            this.warehouseTypeFull = true
          }
        } else {
          this.warehouseUsageType = res.filter(obj => obj.codeType == 'WH_USAGE_TYPE' && obj.codeVal !== 'FULL');
          // console.log(this.warehouseUsageType)
          this.warehouseTypeFull = false
        }
        // console.log(this.warehouseTypeFull)
        if ((this.warehouseDetail && this.warehouseDetail.UsageType === 'FULL') || this.warehouseTypeFull) {
          this.leaseTerm = this.allLeaseTerms.filter(e => e.codeValShortDesc !== 'DAY')
          this.leaseTerm = this.leaseTerm.sort((a, b) => a.sortingOrder - b.sortingOrder);
        } else {
          this.leaseTerm = this.allLeaseTerms.sort((a, b) => a.sortingOrder - b.sortingOrder);
        }

        this.ceilingsHeight = res.filter(obj => obj.codeType == 'WH_CEILING_HEIGHT');
        const _units: any[] = res.filter(obj => obj.codeType != 'WH_MIN_LEASE_TERM' && obj.codeType != 'WH_USAGE_TYPE' && obj.codeType != 'WH_CEILING_HEIGHT' && obj.codeVal.toUpperCase() != 'SQCM' && obj.codeVal.toUpperCase() !== 'CBM');
        const _units1 = _units.map(_unit => { return { ..._unit, isSelected: false } })
        const _areaUnits = _units1.filter(_unit => _unit.codeType === 'UNIT_AREA')
        const _otherUnits = _units1.filter(_unit => _unit.codeType !== 'UNIT_AREA')
        const _units2 = _areaUnits.concat(_otherUnits)
        _units2.forEach(_unit => {
          try {
            if (this.warehouseDetail && this.warehouseDetail.OfferedHashMoveAreaUnit) {
              let _termToSearch = this.warehouseDetail.OfferedHashMoveAreaUnit.toLowerCase() === 'pallet' ? 'PLT' : this.warehouseDetail.OfferedHashMoveAreaUnit.toLowerCase()
              if (_unit.codeVal.toLowerCase() === _termToSearch.toLowerCase()) {
                _unit.isSelected = true
                this.offeredHashmoveSpaceUnit = this.warehouseDetail.OfferedHashMoveAreaUnit
              }
            } else if (_unit.codeVal.toLowerCase() === 'sqft') {
              _unit.isSelected = true
              this.offeredHashmoveSpaceUnit = 'sqft'
              this.propertyDetailForm.controls['warehouseSpaceUnit'].setValue(_unit.codeVal);
            }
          } catch (error) {
          }
        })
        // console.log(this.offeredHashmoveSpaceUnit)

        try {
          if (this.hasContainer) {
            this.propertyDetailForm.controls['minWareContainer'].setValue(this.warehouseDetail.WHMinContainer);
          }
        } catch (error) {
          // console.log(error)
        }
        this.units = _units2
        // console.log(this.units)
        // console.log(this.warehouseDetail)
        try {
          if (!Number(this.whID)) {
            if (this.ceilingsHeight) {
              let ceilingID = this.ceilingsHeight.find(obj => obj.codeVal == 4).codeVal
              this.propertyDetailForm.controls['ceilingHeight'].setValue(ceilingID);
            }
            let object = this.units.find(obj => obj.codeVal == 'SQFT');
            // console.log(object)
            this.propertyDetailForm.patchValue({
              warehouseSpaceUnit: object.codeVal,
              // ceilingUnit: object.codeValDesc,
            });
            this.offeredHashmoveSpaceUnit = object.codeValDesc;
          }
          else {
            if (this.warehouseDetail.TotalCoveredAreaUnit) {
              let _termToCompare = 'SQFT'
              try {
                if (this.warehouseDetail.OfferedHashMoveAreaUnit) {
                  _termToCompare = this.warehouseDetail.OfferedHashMoveAreaUnit.toLowerCase() === 'pallet' ? 'PLT' : this.warehouseDetail.OfferedHashMoveAreaUnit
                }
              } catch {

              }
              let object = this.units.find(obj => obj.codeVal.toUpperCase() == _termToCompare.toUpperCase());
              // console.log(object)
              this.propertyDetailForm.patchValue({
                warehouseSpaceUnit: object.codeVal,
                // ceilingUnit: object.codeValDesc,
              });
              // console.log(this.propertyDetailForm.controls.warehouseSpaceUnit)
              this.offeredHashmoveSpaceUnit = (object.codeValDesc === 'PLT') ? 'pallet' : object.codeValDesc;
              // console.log(this.offeredHashmoveSpaceUnit)
              // console.log(object)
              this.resetMinLeaseTerms()
            }
            if (this.warehouseDetail.MinLeaseTermValue && this.warehouseDetail.MinLeaseTermUnit) {
              this.selectedMiniLeaseTerm = this.leaseTerm.find(obj => obj.codeVal == this.warehouseDetail.MinLeaseTermValue && obj.codeValShortDesc == this.warehouseDetail.MinLeaseTermUnit)
            }
            // if (this.offeredHashmoveSpaceUnit.toLowerCase() === 'container') {
            this.minLeaseTermCont = this.warehouseDetail.MinLeaseTermValue
            // console.log(this.minLeaseTermCont)
            // }
          }
        } catch (error) {
          console.log(error)
        }
      }
    }, (err: HttpErrorResponse) => {
      console.log(err);
      loading(false);
    })
  }

  selectDocx(selectedFiles: NgFilesSelected): void {
    let toUpload = selectedFiles
    if (selectedFiles.status !== NgFilesStatus.STATUS_SUCCESS) {
      if (selectedFiles.status == 1) this._toastr.error('Please select 5 or less file(s) to upload.', '')
      else if (selectedFiles.status == 2) this._toastr.error('File size should not exceed 5 MB. Please upload smaller file.', '')
      else if (selectedFiles.status == 4) this._toastr.error('File format is not supported. Please upload supported format file.', '')
      return;
    }
    else {
      try {
        if (this.uploadedGalleries.length + selectedFiles.files.length > this.config.maxFilesCount) {
          this._toastr.error('Please select 5 or less file(s) to upload.', '');
          return;
        }
        const { uploadedGalleries } = this
        const currVids = uploadedGalleries.filter(galleryDocx => ((galleryDocx.DocumentUploadedFileType && galleryDocx.DocumentUploadedFileType.length > 0 && galleryDocx.DocumentUploadedFileType.toLowerCase() === 'mp4') || (galleryDocx.FileContent && galleryDocx.FileContent.length > 0 && galleryDocx.FileContent[0].documentUploadedFileType.toLowerCase() === 'mp4')))
        const { files } = selectedFiles
        const newVids = files.filter(file => file.type.includes('mp4') || file.type.includes('video'))
        if (newVids && newVids.length > 0) {
          if (currVids && currVids.length > 0) {
            this._toastr.error('Only one video is allowed.', '');
            try {
              newVids.forEach(newVid => {
                let idx = toUpload.files.indexOf(newVid)
                toUpload.files.splice(idx, 1)
              })
            } catch (error) {
              this._toastr.warning('Unable to select file, please try again', '')
              console.warn(error)
              return
            }
          }
        }

        this.onFileChange(toUpload)
      } catch (error) {
        console.warn(error);
      }

    }
  }

  onFileChange(event) {
    let flag = 0;
    if (event) {
      try {
        const allDocsArr = []
        const fileLength: number = event.files.length
        for (let index = 0; index < fileLength; index++) {
          let reader = new FileReader();
          const element = event.files[index];
          let file = element
          reader.readAsDataURL(file);
          reader.onload = () => {
            const selectedFile: DocumentFile = {
              fileName: file.name,
              fileType: file.type,
              fileUrl: reader.result,
              fileBaseString: (reader as any).result.split(',').pop()
            }
            if (event.files.length <= this.config.maxFilesCount) {
              const docFile = JSON.parse(this.generateDocObject(selectedFile));
              allDocsArr.push(docFile);
              flag++
              if (flag === fileLength) {
                this.uploadedGalleries = this.uploadedGalleries.concat(allDocsArr);
              }
            }
            else {
              this._toastr.error('Please select only ' + this.config.maxFilesCount + 'file to upload', '');
            }
          }
        }
      }
      catch (err) {
        // console.log(err);
      }
    }

  }
  generateDocObject(selectedFile): any {
    let object = this.warehouseDocx;
    object.DocumentID = this.docTypeId;
    object.DocumentLastStatus = this.fileStatus;
    object.UserID = this.userProfile.UserID;
    object.ProviderID = this.userProfile.ProviderID;
    object.WHID = this.whID;
    object.DocumentFileContent = null;
    object.DocumentName = null;
    object.DocumentUploadedFileType = null;
    object.FileContent = [{
      documentFileName: selectedFile.fileName,
      documentFile: selectedFile.fileBaseString,
      docUrl: selectedFile.fileUrl,
      documentUploadedFileType: selectedFile.fileType.split('/').pop()
    }]
    return JSON.stringify(object);
  }
  async uploadDocuments(docFiles: Array<any>) {
    const totalDocLenght: number = docFiles.length
    for (let index = 0; index < totalDocLenght; index++) {
      try {
        docFiles[index].WHID = this.whID;
        const resp: JsonResponse = await this.docSendService(docFiles[index])
        if (resp.returnStatus = 'Success') {
          try {
            let resObj = JSON.parse(resp.returnText);
            this.docTypeId = resObj.DocumentID;
            this.fileStatus = resObj.DocumentLastStaus;
            let fileObj = JSON.parse(resObj.DocumentFile);

            fileObj.forEach(element => {
              element.DocumentFile = baseExternalAssets + element.DocumentFile;
            });
            if (index !== (totalDocLenght - 1)) {
              docFiles[index + 1].DocumentID = resObj.DocumentID;
              docFiles[index + 1].DocumentLastStatus = resObj.DocumentLastStaus;
            }
            this.uploadedGalleries = fileObj;
            this._toastr.success("File upload successfully", "");
            this.activeStep = 1
          } catch {
            loading(false)
          }
        }
        else {
          loading(false)
          this._toastr.error("Error occured on upload", "");
        }
      } catch (error) {
        loading(false)
        this._toastr.error("Error occured on upload", "");
      }
    }
    this.getWareHouseDetail(this.userProfile.ProviderID, this.whID);
  }

  async docSendService(doc: any) {
    const resp: JsonResponse = await this._basicInfoService.docUpload(doc).toPromise()
    return resp
  }

  removeSelectedDocx(index, obj) {
    if (obj && obj.DocumentFile) {
      obj.DocumentFile = obj.DocumentFile.split(baseExternalAssets).pop();
      obj.DocumentID = this.docTypeId;
      this._basicInfoService.removeDoc(obj).subscribe((res: any) => {
        if (res.returnId > 0) {
          this._toastr.success('Remove selected document succesfully', "");
          this.uploadedGalleries.splice(index, 1);
          if (!this.uploadedGalleries || (this.uploadedGalleries && !this.uploadedGalleries.length)) {
            this.docTypeId = null;
          }
        }
        else {
          this._toastr.error(res.returnText, "");
        }
      }, (err: HttpErrorResponse) => {
        console.log(err);
      })
    }
    else {
      this.uploadedGalleries.splice(index, 1);
    }
  }
  openGallery(index, albumArr): void {
    this._lightbox.open(albumArr, index, { disableScrolling: true, centerVertically: true, alwaysShowNavOnTouchDevices: true });
  }
  closeLightBox(): void {
    this._lightbox.close();
  }

  cancelWarehouse() {
    const modalRef = this._modalService.open(ConfirmDeleteDialogComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'small-modal',
      backdrop: 'static',
      keyboard: false
    });
    modalRef.result.then((result) => {
      if (result == "close") {
        this.wareHouseTemplateChange.emit(false);
      }
    });
    let obj = {
      data: this.whID,
      type: "CancelWarehouse"
    }
    modalRef.componentInstance.deleteIds = obj;
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);
  }



  checkCity() {
    let cityName = this.locationForm.value.city;
    if (typeof (cityName) == "string") {
      this.cityValidation = "City should be selected from the dropdown";
      this.locationForm.controls.city.status = "INVALID";
      this.cityError = true;
    }
    else if (typeof (cityName) == "object") {
      this.cityValidation = undefined;
      this.cityError = false;
    }
  }

  testWarehouse() {
    // console.log('generalForm', this.generalForm.invalid)
    // console.log('uploadedGalleries', !this.uploadedGalleries.length)
    // console.log('locationForm', this.locationForm.invalid)
    // console.log('propertyDetailForm', this.propertyDetailForm.invalid)
    // console.log('hashmovespaceMsg', this.hashmovespaceMsg)
    // console.log('offeredHashmoveSpaceUnit', this.offeredHashmoveSpaceUnit.toLowerCase() === 'container' && !this.minLeaseTermCont)
    // console.log('cityValidation', this.cityValidation)
    // console.log('leaseSpaceMsg', this.leaseSpaceMsg)
    return
  }

  addwareHouse() {



    if (!this.locationForm.value.city || !this.locationForm.value.city.id) {
      this._toastr.warning('Please select city from Dropdown')
      return
    }
    try {
      if (this.offeredHashmoveSpaceUnit.toLowerCase() === 'container' && !this.minLeaseTermCont) {
        this._toastr.warning('Please specify minimum lease term')
        return
      }
    } catch (error) {

    }
    let _countryID = -1
    try {
      _countryID = JSON.parse(this.locationForm.value.city.desc)[0].CountryID
    } catch {
    }

    let unitToSave = this.offeredHashmoveSpaceUnit
    if (this.offeredHashmoveSpaceUnit.toLowerCase() === 'pallet') {
      unitToSave = 'PLT'
    } else if (!this.warehouseTypeFull) {
      unitToSave = this.offeredHashmoveSpaceUnit.toLowerCase()
    } else if (!this.warehouseTypeFull) {
      unitToSave = 'sqft'
    }

    const { selectedWHUser } = this

    if (typeof this.warehouseUserInfo.value.name == "object") {
      if (selectedWHUser) {
        this.warehouseUserInfo.controls['name'].patchValue(selectedWHUser);
        this.warehouseUserInfo.controls['email'].patchValue(selectedWHUser.emailAddress ? selectedWHUser.emailAddress : '');
        this.warehouseUserInfo.controls['contact'].patchValue(selectedWHUser.phoneNumber ? selectedWHUser.phoneNumber : '');
      }
    } else {
      this._toastr.warning('User must be selected from list', 'Invalid User')
      this.wh_name = true
      loading(false)
      return
    }

    if (!selectedWHUser || !selectedWHUser.firstName) {
      this._toastr.warning('User must be selected to add warhouse', 'Invalid User')
      this.wh_name = true
      loading(false)
      return
    }


    const _minLeasValue = this.offeredHashmoveSpaceUnit === 'sqft' ? this.propertyDetailForm.value.minLeaseValueOne : this.propertyDetailForm.value.minLeaseValueTwo

    let obj = {
      whid: this.whID,
      providerID: this.userProfile.ProviderID,
      whName: this.generalForm.value.whName,
      whDesc: this.generalForm.value.whDetail,
      countryID: _countryID,
      cityID: this.locationForm.value.city.id,
      cityName: this.locationForm.value.city.title,
      // countryName: "string",
      whpoBoxNo: this.locationForm.value.poBox,
      whAddress: this.locationForm.value.address,
      // gLocID: 0,
      latitude: this.location.lat,
      longitude: this.location.lng,
      totalCoveredArea: this.propertyDetailForm.value.warehouseSpace,
      totalCoveredAreaUnit: unitToSave,
      usageType: (this.warehouseTypeFull) ? 'FULL' : 'SHARED',
      whFacilitiesProviding: this.facilities,
      isBlocked: false,
      offeredHashMoveArea: (!this.warehouseTypeFull) ? this.propertyDetailForm.value.hashmoveSpace : null,
      offeredHashMoveAreaUnit: (!this.warehouseTypeFull) ? this.offeredHashmoveSpaceUnit : null,
      // ceilingHeight: (!this.warehouseTypeFull) ? this.propertyDetailForm.value.ceilingHeight : null,
      ceilingHeight: null,
      ceilingLenght: (!this.warehouseTypeFull) ? 0 : null,
      ceilingWidth: (!this.warehouseTypeFull) ? 0 : null,
      ceilingUnit: (!this.warehouseTypeFull) ? 'ft' : null,
      // minLeaseTermValue: this.offeredHashmoveSpaceUnit.toLowerCase() === 'container' ? this.minLeaseTermCont : this.selectedMiniLeaseTerm.codeVal,
      // minLeaseTermUnit: this.offeredHashmoveSpaceUnit.toLowerCase() === 'container' ? 'DAYS' : this.selectedMiniLeaseTerm.codeValShortDesc,
      minLeaseTermValue: this.minLeaseTermCont,
      minLeaseTermUnit: 'DAYS',
      // WHMinSQFT: (!this.warehouseTypeFull) ? this.propertyDetailForm.value.minLeaseValueOne : null,
      WHMinSQFT: (!this.warehouseTypeFull) ? _minLeasValue : null,
      // WHMinCBM: (!this.warehouseTypeFull) ? this.propertyDetailForm.value.minLeaseValueTwo : null,
      WHMinCBM: null,
      WHMinPallet: (!this.warehouseTypeFull) ? this.propertyDetailForm.value.minLeaseValueThree : null,
      WHMinContainer: (!this.warehouseTypeFull) ? this.propertyDetailForm.value.minWareContainer : null,
      // comissionType: (this.isRealEstate) ? ((this.fixedAmount) ? 'Fixed_Amount' : 'Fixed_Percent') : null,
      // comissionCurrencyID: (this.isRealEstate) ? this.commissionForm.value.commissionCurrency.id : null,
      // comissionValue: (this.isRealEstate) ? this.commissionForm.value.commissionValue : null,
      // percent: (this.isRealEstate) ? ((!this.fixedAmount) ? this.commissionForm.value.percentValue : null) : null,
      createdBy: getLoggedUserData().UserID,
      modifiedBy: getLoggedUserData().UserID,
      TC: this.editorContent,
      contactPersonUserID: selectedWHUser.userID,
    }
    loading(true)
    this._warehouseService.addWarehouseDetail(obj).subscribe((res: any) => {
      loading(false)
      if (res.returnId > 0) {
        this.whID = res.returnObject[0].WHID;
        if (Number(this.whID) > 0) {
          loading(true)
          this.uploadedGalleries = this.uploadedGalleries.filter(obj => obj.BusinessLogic);
          this.uploadDocuments(this.uploadedGalleries);

        }
        this._toastr.success('Warehouse detail saved', '')
        this._sharedService.mainScrollListener.next(true)
        this._stepper.next();
        this.getRatesData()
      } else {
        this._toastr.error(res.returnText, '')
      }
    }, (err: HttpErrorResponse) => {
      loading(false)
      console.log(err);
    })
  }

  formatterCity = (x) => {
    this.isCitySearching = false

    const { desc, title } = x
    try {
      const country = desc[0]
      const { CountryCode } = country
      this.autocomplete.setComponentRestrictions({ 'country': CountryCode.toLowerCase() });
    } catch (error) {
      // console.warn(error);
    }
    return title
  };
  isCitySearching: boolean = false
  hasCitySearchFailed: boolean = false
  hasCitySearchSuccess: boolean = false

  searchCity = (text$: Observable<string>) =>
    text$
      .debounceTime(300) //debounce time
      .distinctUntilChanged()
      .do(() => { this.isCitySearching = true; this.hasCitySearchFailed = false; this.hasCitySearchSuccess = false; }) // do any action while the user is typing
      .mergeMap(term => {
        let some = of([]); //  Initialize the object to return
        try {
          if (term && term.length >= 3) { //search only if item are more than three
            some = this._commonService.getFilteredCity(term)
              .do((res) => { this.isCitySearching = false; this.hasCitySearchSuccess = true; return res; })
              .catch(() => { this.isCitySearching = false; this.hasCitySearchFailed = true; return []; })
          } else { this.isCitySearching = false; some = of([]); }
        } catch (error) {
          this.isCitySearching = false
        }
        return some;
      })
      .do((res) => { this.isCitySearching = false; return res; })
      .catch(() => { this.isCitySearching = false; return of([]); }); // final server list

  currency = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .map(term => (!term || term.length < 3) ? []
        : this.currencyList.filter(v => v.shortName.toLowerCase().indexOf(term.toLowerCase()) > -1));
  formatterCurrency = (x: { shortName: string }) => x.shortName;


  /**
   * [ADD WAREHOUSE RATES MODAL]
   * @param  rowId [description]
   * @return       [description]
   */

  addWarehouseRate(rowId) {
    if (!this.warehouseDetail) {
      return
    }

    let obj;
    if (rowId > 0) {
      obj = this.warehouseDetail;
    } else {
      obj = null
    }
    let modalRef
    if (this.hasContainer) {
      modalRef = this._modalService.open(WarehouseRateDialogComponent, {
        size: 'lg',
        centered: true,
        windowClass: 'large-modal',
        backdrop: 'static',
        keyboard: false
      });
    } else {
      modalRef = this._modalService.open(SeaRateDialogComponent, {
        size: 'lg',
        centered: true,
        windowClass: 'large-modal',
        backdrop: 'static',
        keyboard: false
      });
    }

    modalRef.result.then((result) => {
      this.getAllPublishRates(this.whID)
    });

    modalRef.componentInstance.savedRow.subscribe((emmitedValue) => {
      this.getAllPublishRates(this.whID)
    });

    // PER_CBM
    // PER_SQFT
    // PER_PLT_PER_DAY
    let _termToSearch = 'PER_SQFT'
    try {
      switch (this.warehouseDetail.OfferedHashMoveAreaUnit.toLowerCase()) {
        case 'sqm':
          _termToSearch = 'PER_SQM'
          break;
        case 'sqft':
          _termToSearch = 'PER_SQFT'
          break;
        case 'pallet':
          _termToSearch = 'PER_PLT_PER_DAY'
          break;

        default:
          _termToSearch = 'PER_SQFT'
          break;
      }
    } catch (error) {

    }
    const _addCharges = this.warehouseCharges.filter(_charge => _charge.addChrBasis == 'PER_BOOKING' || _charge.addChrBasis === _termToSearch)
    let object = {
      forType: 'WAREHOUSE',
      data: obj,
      addList: _addCharges,
      mode: 'draft',
      customers: this.allCustomers,
      drafts: this.warehouseTypes,
      usageType: this.warehouseDetail.UsageType
    }
    modalRef.componentInstance.selectedData = object;
    modalRef.componentInstance.warehouseUnit = this.warehouseDetail.OfferedHashMoveAreaUnit;
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);

  }

  public warehouseTypes: any[] = []
  getDropdownsList() {
    this._commonService.getMstCodeVal('WH_STORAGE_TYPE').subscribe((res: any) => {
      this.warehouseTypes = res
      loading(false)
    }, (err) => {
      loading(false)
    })
  }

  public allCustomers: any[] = []
  /**
   *
   * Getting list of all customers
   * @param {number} ProviderID
   * @memberof SeaFreightComponent
   */
  getAllCustomers() {
    loading(true)
    this.allCustomers = JSON.parse(localStorage.getItem('customers'))
    if (this.allCustomers) {
      this.allCustomers.forEach(e => {
        e.CustomerImageParsed = getImagePath(ImageSource.FROM_SERVER, e.CustomerImage, ImageRequiredSize._48x48)
      })
      loading(false)
    } else {
      this._seaFreightService.getAllCustomers(this.userProfile.ProviderID).subscribe((res: any) => {
        if (res.returnId > 0) {
          this.allCustomers = res.returnObject
          loading(false)
          this.allCustomers.forEach(e => {
            e.CustomerImageParsed = getImagePath(ImageSource.FROM_SERVER, e.CustomerImage, ImageRequiredSize._48x48)
          })
          localStorage.setItem('customers', JSON.stringify(this.allCustomers))
        } else {
          try {
            localStorage.removeItem('customers')
          } catch { }
        }
      }, (err) => {
        loading(false)
        try {
          localStorage.removeItem('customers')
        } catch { }
      })
    }
  }

  /**
   *
   * Get Additional Port Charges
   * @memberof WarehouseFreightComponent
   */
  getAdditionalData() {
    loading(true)
    const additionalCharges = JSON.parse(localStorage.getItem('additionalCharges'))
    if (additionalCharges) {
      this.warehouseCharges = additionalCharges.filter(e => e.modeOfTrans === 'WAREHOUSE' && e.addChrType === 'ADCH')
    } else {
      this._seaFreightService.getAllAdditionalCharges(this.userProfile.ProviderID).subscribe((res: any) => {
        this.warehouseCharges = res.filter(e => e.modeOfTrans === 'WAREHOUSE' && e.addChrType === 'ADCH')
        localStorage.setItem('additionalCharges', JSON.stringify(res.filter(e => e.addChrType === 'ADCH')))
        loading(false)
      }, (err) => {
        loading(false)
      })
    }
    loading(false)
  }

  /**
   *
   * EVENT EMITTER OBSERVABLE FOR UI TABLE COMPONENT
   * @param {object} event
   * @memberof SeaFreightComponent
   */
  tableCheckedRows(event) {
    if (typeof event.list[0] === 'object') {
      if (event.list[0].type === 'history') {
        this.rateHistory(event.list[0].id, 'Rate_WH')
      }
    } else {
      this.delPublishRates = event.list
    }
  }

  /**
   *
   * RATE HISTORY POPUP TRIGGER
   * @param {number} recId //table row id
   * @param {string} fortype // fcl or lcl
   * @memberof SeaFreightComponent
   */
  rateHistory(recId, fortype) {
    const modalRef = this._modalService.open(RateHistoryComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'upper-medium-modal-history',
      backdrop: 'static',
      keyboard: false
    });

    let obj = {
      id: recId,
      type: fortype,
      shippingLines: null,
      customers: null,
      ports: null
    }
    modalRef.componentInstance.getRecord = obj;
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);
  }

  /**
   *
   * EVENT EMITTER OBSERVABLE FOR SORTING IN UI TABLE
   * @param {string} type //fcl or lcl
   * @param {object} event // sorting object {columnName, columnDirection}
   * @memberof SeaFreightComponent
   */
  sortedFilters(type, event) {
    this.sortColumn = event.column
    this.sortColumnDirection = event.direction
    this.getAllPublishRates(this.whID)
  }

  public pageNo: number = 1
  /**
   * PAGING EVENT EMITTER OBSERVABLE FOR UI TABLE
   *
   * @param {string} type //fcl or lcl
   * @param {number} event //page number 0,1,2...
   * @memberof SeaFreightComponent
   */
  paging(event) {
    this.pageNo = event.page;
    this.getAllPublishRates(event.whid, event.page)
  }

  /**
   *
   * FILTER BUTTON ACTION
   * @param {string} type //fcl or lcl
   * @memberof SeaFreightComponent
   */

  /**
   *
   * GET ALL PUBLISHED RATES FOR FCL OR LCL
   * @param {string} type // fcl or lcl
   * @memberof SeaFreightComponent
   */
  public sortColumn: string = 'EffectiveFrom'
  public sortColumnDirection: string = 'ASC'
  public pageSize: number = 20
  public filteredRecords: number;

  getAllPublishRates(warehouseID, number?) {
    loading(true)
    this.publishloading = true;
    let obj = {
      pageNo: (number) ? number : this.pageNo,
      pageSize: this.pageSize,
      providerID: this.userProfile.ProviderID,
      whid: warehouseID,
      effectiveFrom: (this.fromDate && this.fromDate.month) ? this.fromDate.month + '/' + this.fromDate.day + '/' + this.fromDate.year : null,
      effectiveTo: (this.toDate && this.toDate.month) ? this.toDate.month + '/' + this.toDate.day + '/' + this.toDate.year : null,
      sortColumn: this.sortColumn,
      sortColumnDirection: this.sortColumnDirection,
      customerID: (this.filterbyCustomer ? parseInt(this.filterbyCustomer) : null),
      customerType: (this.isCustomer ? 'CUSTOMER' : (this.isMarketplace ? 'MARKETPLACE' : null)),
      jsonCustomerDetail: null
    }
    this._warehouseService.getAllPublishedrates(obj).subscribe((res: any) => {
      this.publishloading = false;
      loading(false)
      if (res.returnId > 0) {
        this.totalPublishedRecords = res.returnObject.recordsTotal
        this.filteredRecords = res.returnObject.recordsFiltered
        const _warehousePublishedRates = cloneObject(res.returnObject.data);
        if (_warehousePublishedRates) {
          _warehousePublishedRates.forEach(e => {
            if (this.warehouseDetail) {
              e.usageType = this.warehouseDetail.UsageType
              if (e.pricingJson) {
                e.parsedpricingJson = JSON.parse(e.pricingJson)
                const priceJSON: WarehouseAddCharges[] = JSON.parse(e.pricingJson)
                // e.cbmArr = e.parsedpricingJson.filter(e => e.priceBasis === 'PER_CBM_PER_DAY')
                // e.sqftArr = e.parsedpricingJson.filter(e => e.priceBasis === 'PER_SQFT_PER_DAY')
                // e.pltArr = e.parsedpricingJson.filter(e => e.priceBasis === 'PER_PLT_PER_DAY')

                // e.cbmArrDed = e.parsedpricingJson.filter(e => e.priceBasis === 'PER_CBM_PER_DAY_DED')
                // e.sqftArrDed = e.parsedpricingJson.filter(e => e.priceBasis === 'PER_SQFT_PER_DAY_DED')
                // e.pltArrDed = e.parsedpricingJson.filter(e => e.priceBasis === 'PER_PLT_PER_DAY_DED')


                e.monthArr = e.parsedpricingJson.filter(e => e.priceBasis === 'PER_MONTH')
                e.yearArr = e.parsedpricingJson.filter(e => e.priceBasis === 'PER_YEAR')

                if (priceJSON.filter(_price => _price.sWHType === 'FULL').length > 0) {
                  e.whRateType = 'full'
                } else {
                  e.whRateType = 'shared'
                }

                // console.log(((e.sqftArr.length > 0) ? (e.sqftArr[0].price) : (e.yearArr.length > 0) ? e.yearArr[0].price : 0));

                e.whPrice1 = (e.monthArr && e.monthArr.length > 0) ? (e.monthArr[0].price ? e.monthArr[0].price : 0) : (priceJSON.filter(_price => _price.sWHType === 'SHARED').length > 0 ? priceJSON.filter(_price => _price.sWHType === 'SHARED')[0].addChrName : '')
                e.whPrice2 = (e.yearArr && e.yearArr.length > 0) ? (e.yearArr[0].price ? e.yearArr[0].price : 0) : (priceJSON.filter(_price => _price.sWHType === 'SHARED').length > 0 ? priceJSON.filter(_price => _price.sWHType === 'SHARED')[0].price : 0)
                e.whPrice3 = (priceJSON.filter(_price => _price.sWHType === 'SHARED').length > 0 && priceJSON.filter(_price => _price.sWHType === 'SHARED')[0].addOnPrice) ? this._currencyControl.applyRoundByDecimal(priceJSON.filter(_price => _price.sWHType === 'SHARED')[0].addOnPrice, 2).toLocaleString() + ' / ' + priceJSON.filter(_price => _price.sWHType === 'SHARED')[0].addOnBasis : ''

                e.whPrice4 = priceJSON.filter(_price => _price.sWHType === 'DEDICATED').length > 0 ? priceJSON.filter(_price => _price.sWHType === 'DEDICATED')[0].addChrName : ''
                e.whPrice5 = priceJSON.filter(_price => _price.sWHType === 'DEDICATED').length > 0 ? priceJSON.filter(_price => _price.sWHType === 'DEDICATED')[0].price : 0
                e.whPrice6 = (priceJSON.filter(_price => _price.sWHType === 'DEDICATED').length > 0 && priceJSON.filter(_price => _price.sWHType === 'DEDICATED')[0].addOnPrice) ? this._currencyControl.applyRoundByDecimal(priceJSON.filter(_price => _price.sWHType === 'DEDICATED')[0].addOnPrice, 2).toLocaleString() + ' / ' + priceJSON.filter(_price => _price.sWHType === 'DEDICATED')[0].addOnBasis : ''

                e.whType = "normal"
                try {
                  e.containerPrice = e.parsedpricingJson.filter(e => e.priceBasis === 'PER_CONT_PER_DAY' || e.priceBasis === 'PER_CONT_PER_MONTH' || e.priceBasis === 'PER_CONT_PER_YEAR')
                  if (e.containerPrice.length > 0) {
                    e.whType = "container"
                    e.whPrice7 = (e.containerPrice && e.containerPrice.length) ? e.containerPrice[0].price : 0
                    e.whPrice8 = (priceJSON.filter(_price => _price.sWHType === 'SHARED').length > 0 && priceJSON.filter(_price => _price.sWHType === 'SHARED')[0].addOnPrice) ? this._currencyControl.applyRoundByDecimal(priceJSON.filter(_price => _price.sWHType === 'SHARED')[0].addOnPrice, 2).toLocaleString() + ' / ' + priceJSON.filter(_price => _price.sWHType === 'SHARED')[0].addOnBasis : ''
                  }
                } catch (error) {
                }
              }
            }
          })
          this.warehousePublishedRates = _warehousePublishedRates
        } else {
          this.warehousePublishedRates = []
        }
        this.checkedallpublishRates = false;
      }
    }, error => {
      loading(false)
    })
  }


  /**
   *
   * DELETE PUBLISHDED RECORD FOR FCL
   * @returns
   * @memberof SeaFreightComponent
   */
  public delPublishRates: any[] = []
  delPublishedRecord() {
    if (!this.delPublishRates.length) return;
    const modalRef = this._modalService.open(ConfirmDeleteDialogComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'small-modal',
      backdrop: 'static',
      keyboard: false
    });
    modalRef.result.then((result) => {
      if (result == "Success") {
        this.checkAllPublish = false
        // console.log(this.checkAllPublish);

        this.getAllPublishRates(this.whID)
        this.paging({ page: 1, whid: this.whID })
        this.checkedallpublishRates = false
        this.delPublishRates = []
        this.pageNo = 1
      }
    });
    let obj = {
      data: this.delPublishRates,
      type: "warehouse"
    }
    modalRef.componentInstance.deleteIds = obj;
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);
  }

  /**
   *
   * EDIT PUBLISH RATE POPUP MODAL ACTION
   * @param {string} type //fcl or lcl
   * @returns
   * @memberof SeaFreightComponent
   */
  rateValidity(type) {
    if (!this.delPublishRates.length) return;
    let updateValidity = [];
    for (let i = 0; i < this.warehousePublishedRates.length; i++) {
      for (let y = 0; y < this.delPublishRates.length; y++) {
        if (this.warehousePublishedRates[i].whPricingID == this.delPublishRates[y]) {
          updateValidity.push(this.warehousePublishedRates[i])
        }
      }
    }
    if (updateValidity && updateValidity.length > 1) {
      const modalRef = this._modalService.open(RateValidityComponent, {
        size: 'lg',
        centered: true,
        windowClass: 'upper-medium-modal',
        backdrop: 'static',
        keyboard: false
      });
      modalRef.result.then((result) => {
        this.getAllPublishRates(this.whID);
        this.checkedallpublishRates = false
        this.delPublishRates = [];
      });
      let obj = {
        data: updateValidity,
        type: type
      }
      modalRef.componentInstance.validityData = obj;
    } else if (updateValidity && updateValidity.length === 1) {
      let modalRef2;
      if (this.hasContainer) {
        modalRef2 = this._modalService.open(WarehouseRateDialogComponent, {
          size: 'lg',
          centered: true,
          windowClass: 'large-modal',
          backdrop: 'static',
          keyboard: false
        });
      } else {
        modalRef2 = this._modalService.open(SeaRateDialogComponent, {
          size: 'lg',
          centered: true,
          windowClass: 'large-modal',
          backdrop: 'static',
          keyboard: false
        });
      }

      modalRef2.result.then((result) => {
        this.getAllPublishRates(this.whID);
        this.checkedallpublishRates = false
        this.delPublishRates = [];
      });
      let _termToSearch = 'PER_SQFT'
      try {
        switch (this.warehouseDetail.OfferedHashMoveAreaUnit.toLowerCase()) {
          case 'sqm':
            _termToSearch = 'PER_SQM'
            break;
          case 'sqft':
            _termToSearch = 'PER_SQFT'
            break;
          case 'pallet':
            _termToSearch = 'PER_PLT_PER_DAY'
            break;

          default:
            _termToSearch = 'PER_SQFT'
            break;
        }
      } catch (error) {

      }
      const _addCharges = this.warehouseCharges.filter(_charge => _charge.addChrBasis == 'PER_BOOKING' || _charge.addChrBasis === _termToSearch)
      let object = {
        forType: type.toUpperCase(),
        data: updateValidity.length > 1 ? updateValidity : updateValidity[0],
        addList: _addCharges,
        customers: this.allCustomers,
        drafts: this.warehouseTypes,
        mode: 'publish'
      }
      modalRef2.componentInstance.selectedData = object;
      modalRef2.componentInstance.warehouseUnit = this.warehouseDetail.OfferedHashMoveAreaUnit;
    }
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);
  }


  /**
   *
   *  Get all priving details for single warehouse
   * @memberof WarehouseComponent
   */
  getPricingDetails() {
    if (this.activeStep === 1) {
      this.getRatesData()
    }
  }

  getRatesData() {
    this.getDropdownsList()
    this.getAllCustomers()
    this.getAdditionalData()
    this.getAllPublishRates(this.whID)
  }


  /**
   * UPDATE COMISSION SAVE REQUEST
   *
   * @memberof WarehouseComponent
   */
  updateComission() {
    loading(true)
    let obj = {
      whid: this.whID,
      comissionType: (this.fixedAmount ? 'Fixed_Amount' : 'Fixed_Percent'),
      comissionCurrencyID: this.commissionForm.value.commissionCurrency.id,
      comissionValue: this.commissionForm.value.commissionValue,
      percent: this.commissionForm.value.percentValue,
      modifiedBy: this.userProfile.UserID
    }
    this._warehouseService.updateComission(obj).subscribe((res: JsonResponse) => {
      loading(false)
      const { returnId, returnText } = res
      if (returnId > 0) {
        if (!this.fixedAmount && this.isRealEstate) {
          this.warehouseDetail.Percent = this.commissionForm.value.percentValue
        }
        this._toastr.success('Comission updated successfully', 'Success')
      } else {
        this._toastr.error(returnText)
      }
    }, (err) => {
      loading(false)
    })
  }

  openVideo(videoURL, event) {
    event.preventDefault()
    event.stopPropagation()
    const modalRefVideo = this._modalService.open(VideoDialogueComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'video',
      backdrop: 'static',
      keyboard: false
    });
    modalRefVideo.componentInstance.videoURL = videoURL
  }

  onKeypress2(event) {
    // console.log(event.target.value);
    let charCode = (event.which) ? event.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    if (event.target.value > 500) {
      // this.propertyDetailForm.controls['ceilingHeight'].setValue(999);
      // return false
    }
  }
  warehouseValidator($type: any, control: AbstractControl) {
    const { offeredHashmoveSpaceUnit } = this
    if (!this.warehouseTypeFull) {
      let regexp: RegExp = /^[0-9]*$/;
      if (!control.value && $type === offeredHashmoveSpaceUnit) {
        return {
          required: true
        }
      }
      // else if (control.value.length > 5 && control.value && $type === offeredHashmoveSpaceUnit) {
      //   if (!regexp.test(control.value)) {
      //     return {
      //       pattern: true
      //     }
      //   }
      //   else if ($type === offeredHashmoveSpaceUnit) {
      //     return {
      //       maxlength: true
      //     }
      //   }

      // }
      else {
        return false;
      }
    }
  };

  wh_name = false
  wh_contact = false
  wh_email = false
  errorMessages() {

    if (this.warehouseUserInfo.controls.name.status === "INVALID" && this.warehouseUserInfo.controls.name.touched) {
      this.wh_name = true;
    }
    if (this.warehouseUserInfo.controls.contact.status === "INVALID" && this.warehouseUserInfo.controls.contact.touched) {
      this.wh_contact = true;
    }
    if (this.warehouseUserInfo.controls.email.status === "INVALID" && this.warehouseUserInfo.controls.email.touched) {
      this.wh_email = true;
    }
  }

  resetForm() {
    this.warehouseUserInfo.reset()
    this.wh_name = false
    this.wh_contact = false
    this.wh_email = false
  }

  updateBlInfo() {

  }


  whUsersTypeAhead = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .map(term => (!term) ? this.whUsers
        : this.whUsers.filter(v => (v.firstName + ' ' + v.lastName).toLowerCase().indexOf(term.toLowerCase()) > -1));

  formatterUser = (_user: IWhUser) => {
    // console.log(_user)
    this.selectedWHUser = _user

    this._wh_name.nativeElement.value = _user.firstName + ' ' + _user.lastName
    this._wh_email.nativeElement.value = _user.emailAddress ? _user.emailAddress : ''
    this._wh_phone.nativeElement.value = _user.phoneNumber ? _user.phoneNumber : ''

    if (typeof _user == "object") {
      let _selectedCountry = this.countryList.find(obj => obj.desc[0].CountryPhoneCode == _user.countryPhoneCode);
      this.selectPhoneCode(_selectedCountry);
    }
    let _toReturn = ''
    if (_user && _user.firstName) {
      _toReturn = _user.firstName + " " + _user.lastName
      this.wh_name = false
    } else {
      _toReturn = ''
    }

    return _toReturn
  };

  public onFocus(e: Event): void {
    e.stopPropagation();
    setTimeout(() => {
      const inputEvent: Event = new Event('input');
      e.target.dispatchEvent(inputEvent);
    }, 0);
  }

}

export function warehouseValidatorCommission(control: AbstractControl) {
  if (this.isRealEstate && !this.fixedAmount) {
    let regexp: RegExp = /^[0-9]*$/;
    if (!control.value) {
      return {
        required: true
      }
    }
    // else if (control.value.length < 1 && control.value) {
    //   if (!regexp.test(control.value)) {
    //     return {
    //       pattern: true
    //     }
    //   }
    //   else {
    //     return {
    //       minlength: true
    //     }
    //   }
    // }
    else if (control.value.length > 5 && control.value) {
      if (!regexp.test(control.value)) {
        return {
          pattern: true
        }
      }
      else {
        return {
          maxlength: true
        }
      }

    }
    else {
      return false;
    }
  }
  else {
    return false;
  }
};
export function warehouseValidatorCommissionVal(control: AbstractControl) {
  if (this.isRealEstate) {
    let regexp: RegExp = /^[0-9]*$/;
    if (!control.value) {
      return {
        required: true
      }
    }
    // else if (control.value.length < 1 && control.value) {
    //   if (!regexp.test(control.value)) {
    //     return {
    //       pattern: true
    //     }
    //   }
    //   else {
    //     return {
    //       minlength: true
    //     }
    //   }
    // }
    else if (control.value.length > 5 && control.value) {
      if (!regexp.test(control.value)) {
        return {
          pattern: true
        }
      }
      else {
        return {
          maxlength: true
        }
      }

    }
    else {
      return false;
    }
  }
};
export function warehouseValidatorCommissionCurr(control: AbstractControl) {
  if (this.isRealEstate) {
    if (!control.value) {
      return {
        required: true
      }
    }
    else {
      return false;
    }
  }
};

export interface IWhUser {
  userID: number;
  firstName: string;
  lastName: string;
  loginID: string;
  emailAddress: string;
  countryPhoneCode: string;
  phoneNumber: string;
  userImage: string;
  userStatus: string;
  isAdmin: boolean;
  isCorporateUser: boolean;
  isVerified: boolean;
  regDate: string;
  countryID: number;
  countryName: string;
  cityName: string;
  countryCode: string;
  lastLoginDate?: string;
  lastLoginCountryID?: number;
  lastLoginCountryName: string;
  lastLoginIP: string;
  totalBookings?: any;
}
