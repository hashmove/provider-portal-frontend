import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssayerUpdateScheduleComponent } from './assayer-update-schedule.component';

describe('AssayerUpdateScheduleComponent', () => {
  let component: AssayerUpdateScheduleComponent;
  let fixture: ComponentFixture<AssayerUpdateScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssayerUpdateScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssayerUpdateScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
