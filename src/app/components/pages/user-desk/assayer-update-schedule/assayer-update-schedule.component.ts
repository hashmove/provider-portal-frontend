import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NguCarousel, NguCarouselService, NguCarouselStore } from '@ngu/carousel';
import { MonthDateModel, getLoggedUserData, loading, getAssayereBookingDates } from '../../../../constants/globalFunctions';
import { CommonService } from '../../../../services/common.service';
import { SharedService } from '../../../../services/shared.service';
import { IBookingInpectionDetail } from '../view-booking/view-booking.component';
import { ToastrService } from 'ngx-toastr';
import { ViewBookingService } from '../view-booking/view-booking.service';
import { JsonResponse } from '../../../../interfaces/JsonResponse';
@Component({
  selector: 'app-assayer-update-schedule',
  templateUrl: './assayer-update-schedule.component.html',
  styleUrls: ['./assayer-update-schedule.component.scss']
})
export class AssayerUpdateScheduleComponent implements OnInit, AfterViewInit {

  @Input() bookingDetails
  @Input() searchCriteria
  @Input() schData: IBookingInpectionDetail
  @Input() status: string = 'PENDING'
  @Input() comments: string = ''
  @Input() commType: string = ''

  public carouselOne: NguCarousel = {
    grid: { xs: 2, sm: 3, md: 6, lg: 6, all: 0 },
    slide: 1,
    speed: 400,
    interval: 4000,
    point: { visible: false },
    load: 2,
    touch: true,
    loop: false,
    custom: 'banner'
  }
  public dateList: Array<MonthDateModel> = []
  public lastSelectedIndex: number = -1
  public selectedDate: MonthDateModel
  public pickupTime = {
    hour: 0,
    minute: 0
  }
  public loadPickupDate: string
  public arrowChange = false;
  public searchMode
  public lpdate: string = '1900-01-01'
  public lpHr: string = '00'
  public lpMin: string = '00'
  assayerName: string
  contactNum = ''
  mode = 'add'
  countryList = []
  countryFlagImage
  phoneCode
  transPhoneCode
  phoneCountryId
  selCountry
  private carouselToken: string;

  constructor(
    public _activeModal: NgbActiveModal,
    private _commonService: CommonService,
    private _sharedService: SharedService,
    private _toastr: ToastrService,
    private _viewBookingService: ViewBookingService,
    private carousel: NguCarouselService
  ) { }

  ngOnInit() {
    let date2Process = ''
    if (this.mode === 'edit' && this.schData && this.schData.ScheduleDateTime) {
      date2Process = new Date(this.schData.ScheduleDateTime) as any
    } else {
      date2Process = new Date() as any
    }
    this.dateList = getAssayereBookingDates(date2Process)
    if (this.mode === 'edit') {
      this.assayerName = this.schData.InspectorName
      this.contactNum = this.schData.InspectorMobileNo
      this.setLoadPickupDate(this.schData.ScheduleDateTime)
    }
    this._commonService.getCountry().subscribe((countryList: any) => {
      if (countryList && countryList.length) {
        countryList.map((obj) => {
          if (typeof (obj.desc) == 'string')
            obj.desc = JSON.parse(obj.desc)
        })
        this._sharedService.countryList.next(countryList);
        this.setCountryListConfig(countryList)
      }
    })

    this._sharedService.countryList.subscribe((state: any) => {
      if (state) this.setCountryListConfig(state)
    })
  }

  initDataFn = (key: NguCarouselStore) => this.carouselToken = key.token

  ngAfterViewInit() {
    setTimeout(() => {
      if (this.mode === 'edit')
        this.carousel.moveToSlide(this.carouselToken, this.dateList.findIndex(_date => _date.isSelected), false)
    }, 10)
  }

  setCountryListConfig(state) {
    this.countryList = state;
    const userProfile = getLoggedUserData()
    if (this.mode === 'add') {
      this.selectPhoneCode(this.countryList.find(obj => obj.id == userProfile.CountryID))
    } else {
      this.selectPhoneCode(this.countryList.find(obj => obj.code == this.schData.CountryCode))
    }
  }

  selectPhoneCode(country) {
    this.countryFlagImage = country.code;
    this.phoneCode = country.desc[0].CountryPhoneCode;
    this.transPhoneCode = country.desc[0].CountryPhoneCode_OtherLang;
    this.phoneCountryId = country.id
    this.selCountry = country
  }

  setLoadPickupDate($loadPickupDate: string) {
    if ($loadPickupDate) {
      const lpdArr: Array<string> = $loadPickupDate.split('T')
      if (lpdArr && lpdArr.length > 1) {
        this.lpdate = lpdArr[0]
        const lpTimeArr: Array<string> = lpdArr[1].split(':')
        if (lpTimeArr && lpTimeArr.length > 1) {
          this.lpHr = lpTimeArr[0]
          this.lpMin = lpTimeArr[1]
        }
      }

      if (this.lpdate) {
        this.dateList.map(date => {
          if (date.pickupDate === this.lpdate) {
            date.isSelected = true
            this.selectedDate = {
              isActive: true,
              isSelected: true,
              pickupDate: this.lpdate
            }
          }
        })
      }
      if (this.lpHr) {
        this.pickupTime.hour = parseInt(this.lpHr)
        this.pickupTime.minute = parseInt(this.lpMin)
      }
    } else {
      this.loadPickupDate = null
      this.lpdate = '1900-01-01'
      this.lpHr = '00'
      this.lpMin = '00'
      this.selectedDate = null
      this.dateList.forEach(date => date.isSelected = false)
      this.pickupTime = { hour: 0, minute: 0 }
    }
  }

  close = () => this._activeModal.close()

  onDateSelect($item: MonthDateModel, $index: number) {
    if (!this.dateList[$index].isActive)
      return
    if (this.lastSelectedIndex === -1) {
      this.dateList.forEach(date => date.isSelected = false)
    } else {
      if (this.lastSelectedIndex !== -1)
        this.dateList[this.lastSelectedIndex].isSelected = false
    }
    this.lastSelectedIndex = (!this.dateList[$index].isSelected) ? -1 : $index
    this.dateList[$index].isSelected = !this.dateList[$index].isSelected
    this.selectedDate = this.dateList[$index]
  }

  submit() {
    if (!this.selectedDate) {
      this._toastr.warning('Please select date first')
      return
    }
    if (!this.assayerName) {
      this._toastr.warning('Please enter name.')
      return
    }
    if (!this.contactNum) {
      this._toastr.warning('please enter contact number.')
      return
    }
    const { hour, minute } = this.pickupTime
    const { pickupDate } = this.selectedDate
    const dateString = pickupDate + 'T' + this.leadingZero(hour) + ':' + this.leadingZero(minute) + ':00'
    let _certificateId = 1
    try {
      _certificateId = this.bookingDetails.BookingCertificateDetail.filter(_cert => _cert.InspectionNature === this.commType)[0].CertificateID
    } catch (error) {
      _certificateId = 1
    }
    const _statusToSend = (this.schData && this.schData.InspectionStatus) ? 'RESCHEDULED' : 'PENDING'
    const toSend = {
      BookingInspectionID: (this.schData && this.schData.BookingInspectionID) ? this.schData.BookingInspectionID : -1,
      BookingID: this.bookingDetails.BookingID,
      CommodityID: this.bookingDetails.BookingCommodityDetail[0].Commodity,
      CertificateID: _certificateId,
      ContainerSpecID: null,
      ScheduleDateTime: dateString,
      ScheduleDateTimeUtc: dateString,
      ScheduleBy: 'ASSAYER',
      ScheduleByUserID: getLoggedUserData().UserID,
      InspectorName: this.assayerName,
      CountryPhoneCode: this.selCountry.desc[0].CountryPhoneCode,
      InspectorMobileNo: this.contactNum,
      InspectorEmail: null,
      InspectionStatus: _statusToSend,
      InspectionComments: (this.schData && this.schData.InspectionComments) ? this.schData.InspectionComments : null,
      InspectionNature: this.commType ? this.commType : 'COMMODITY_INSPECTION',
      CountryID: this.selCountry.id,
    }
    loading(true)
    if (this.mode === 'add') {
      this._viewBookingService.saveBookingInspection({ ...toSend, CreatedBy: getLoggedUserData().UserID }).subscribe((res: JsonResponse) => {
        loading(false)
        if (res.returnId > 0) {
          document.getElementsByTagName('html')[0].style.overflowY = 'auto';
          this._activeModal.close(res.returnObject)
        }
      }, () => loading(false))
    } else {
      let _toSend: any = { ...toSend, ModifiedBy: getLoggedUserData().UserID, }
      if (this.commType === 'CONTAINER_STUFFING') {
        _toSend = {
          ..._toSend,
          AcceptedBy: 'ASSAYER',
          AcceptedByUserID: getLoggedUserData().UserID,
          AcceptedStatus: (this.schData && this.schData.AcceptedStatus) ? this.schData.AcceptedStatus : 'PURPOSERESCHEDULE',
        }
      }
      this._viewBookingService.editBookingInspection(_toSend).subscribe((res: JsonResponse) => {
        loading(false)
        if (res.returnId > 0) {
          document.getElementsByTagName('html')[0].style.overflowY = 'auto';
          this._activeModal.close(res.returnObject)
        }
      }, () => loading(false))
    }
  }

  numberValidwithDecimal(evt) {
    let charCode = (evt.which) ? evt.which : evt.keyCode
    if (evt.target.value && evt.target.value[evt.target.value.length - 1] == '.') {
      if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false
      return true
    }
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
      return false
    return true
  }

  leadingZero = (num: number): string => (num < 10) ? '0' + num : num + ''
}
