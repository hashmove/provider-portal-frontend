import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertificateChargesComponent } from './certificate-charges.component';

describe('CertificateChargesComponent', () => {
  let component: CertificateChargesComponent;
  let fixture: ComponentFixture<CertificateChargesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertificateChargesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificateChargesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
