import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PlatformLocation } from '@angular/common';
import { PmexCharges } from '../view-booking/view-booking.component';

@Component({
  selector: 'app-certificate-charges',
  templateUrl: './certificate-charges.component.html',
  styleUrls: ['./certificate-charges.component.scss']
})
export class CertificateChargesComponent implements OnInit {
  @Input() pmexCharges: PmexCharges;

  constructor(
    private _activeModal: NgbActiveModal,
    private location: PlatformLocation,
  ) {
    location.onPopState(() => this.closeModal());
  }

  ngOnInit() {
  }

  closeModal() {
    this._activeModal.close();
    document.getElementsByTagName('html')[0].style.overflowY = 'auto';

  }

}
