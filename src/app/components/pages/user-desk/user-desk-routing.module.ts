import { NgModule, Type, NgModuleFactory } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserGuard } from '../user-creation/user.guard';
import { UserDeskComponent } from './user-desk.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AllBookingsComponent } from './all-bookings/all-bookings.component';
import { ReportsComponent } from './reports/reports.component';
import { SupportComponent } from './support/support.component';
import { SettingsComponent } from './settings/settings.component';
import { MobileNavComponent } from './mobile-nav/mobile-nav.component';
import { ManageRatesModule } from './manage-rates/manage-rates.module';
import { PaymentResultComponent } from './payment-result/payment-result.component';
import { Observable } from 'rxjs';
import { MakeBookingComponent } from './make-booking/make-booking.component';
import { MakeBookingAirComponent } from './make-booking-air/make-booking-air.component';
import { CustomersComponent } from './customers/customers.component';
import { PublicViewBookingModule } from '../../public/public-view-booking/public-view-booking.module';
import { PartnersComponent } from './partner/partner.component';
import { ProviderUsersComponent } from './provider-users/provider-users.component';
import { RateRequestListComponent } from './rate-request-list/rate-request-list.component';
import { InvoiceListComponent } from './invoice-list/invoice-list.component';
import { PowerBiComponent } from './power-bi.component/power-bi.component';
import { GoogleAnalyticsComponent } from './google-analytics/google-analytics.component';
import { getActiveRoutes } from '../../../constants/globalFunctions';
import { UserRouteGuard } from '../../../services/user-routes.guard';

export function manageRatesModuleChildren(): Type<any> | NgModuleFactory<any> | Promise<Type<any>> | Observable<Type<any>> {
  return ManageRatesModule;
}

export function publicViewBookingModuleChildren(): Type<any> | NgModuleFactory<any> | Promise<Type<any>> | Observable<Type<any>> {
  return PublicViewBookingModule;
}

const routes = {
  path: '',
  component: UserDeskComponent,
  canActivate: [UserGuard],
  children: [
    { path: 'dashboard', component: DashboardComponent },
    { path: 'allbookings', component: AllBookingsComponent, canActivate: [UserRouteGuard] },
    { path: 'rate-requests', component: RateRequestListComponent, canActivate: [UserRouteGuard] },
    { path: 'invoices', component: InvoiceListComponent, canActivate: [UserRouteGuard] },
    { path: 'reports/page-analytics', component: GoogleAnalyticsComponent, canActivate: [UserRouteGuard] },
    { path: 'reports/performance', component: PowerBiComponent, canActivate: [UserRouteGuard] },
    { path: 'settings', component: SettingsComponent, canActivate: [UserRouteGuard] },
    { path: 'customers', component: CustomersComponent, canActivate: [UserRouteGuard] },
    { path: 'partners', component: PartnersComponent, canActivate: [UserRouteGuard] },
    { path: 'users', component: ProviderUsersComponent, canActivate: [UserRouteGuard] },
    { path: 'support', component: SupportComponent, canActivate: [UserRouteGuard] },
    { path: 'rate-requests-v1', component: AllBookingsComponent },
    { path: 'nav', component: MobileNavComponent },
    { path: 'payment_result', component: PaymentResultComponent },
    { path: 'booking-detail/:id', loadChildren: publicViewBookingModuleChildren },
    { path: 'make-booking', component: MakeBookingComponent },
    { path: 'make-booking-air', component: MakeBookingAirComponent },
    { path: 'manage-rates', loadChildren: manageRatesModuleChildren },
    { path: '**', redirectTo: 'dashboard', pathMatch: 'full' } as any
  ]
}

@NgModule({
  imports: [RouterModule.forChild([routes])],
  exports: [RouterModule]
})
export class UserDeskRoutingModule { }
