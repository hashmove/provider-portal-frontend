import { Component, OnInit, Input, ViewChild, ViewEncapsulation, Output, EventEmitter, OnChanges, SimpleChanges, OnDestroy } from '@angular/core'
import { getLoggedUserData, removeDuplicates, loading, getImagePath, ImageSource, ImageRequiredSize, getProviderImage, changeCase, after, before, equals, isArrayValid, isMobile } from '../../../../constants/globalFunctions'
import { BookingStatus, ICarrierFilter, IProviderFilter, ICompanyFilter, FilterdBookings, JsonResponse, IRequestStatus } from '../../../../interfaces'
import { NgbDateStruct, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap'
import { ViewBookingService } from '../view-booking/view-booking.service'
import { SharedService } from '../../../../services/shared.service'
import { BookingsHelper } from '../all-bookings/bookings.helper'
import { debounceTime } from 'rxjs/operators/debounceTime'
import { untilDestroyed } from 'ngx-take-until-destroy'
import { Observable } from 'rxjs/Observable'
import { ToastrService } from 'ngx-toastr'
import { map } from 'rxjs/operators/map'
import * as moment from 'moment'
@Component({
  selector: 'app-bookings-filter',
  templateUrl: './bookings-filter.component.html',
  styleUrls: ['./bookings-filter.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class BookingsFilterComponent extends BookingsHelper implements OnInit, OnChanges, OnDestroy {

  // public hmBookingNum = 'HMQ-2109090000009'
  public hmBookingNum = ''
  public selectedBookStatusCode = null
  public selectedShipStatusCode = null
  public selectedCargoType = null
  public selectedRequestStatus: IRequestStatus = null

  public cargoTypeList = ['FCL', 'LCL']

  public selectedShippingModeCode = null
  public isMarketplace = false
  public filterbyContainerType
  public fromDate
  public toDate
  public filterOrigin: any = {}
  public filterDestination: any = {}
  public bookingStatuses: Array<BookingStatus> = []
  public shippingStatuses: Array<BookingStatus> = []
  public requestStatusList: Array<IRequestStatus> = []

  public selectedCarrier: ICarrierFilter
  public selectedProvider: IProviderFilter
  public selectedCompany: ICompanyFilter
  public allShippingLines = []

  public carriersList: Array<ICarrierFilter> = []
  public providersList: Array<IProviderFilter> = []
  public bookingList: Array<FilterdBookings> = []
  public companyList: Array<ICompanyFilter> = []
  public shippingModes: Array<ICarrierFilter> = []
  public sortBy = 'date' // or price

  @ViewChild('d') eventDate
  @ViewChild('calenderField') calenderField: any
  hoveredDate: any
  public minDate: any
  public maxDate: any
  ports = []

  @Output() onListChange = new EventEmitter<string>()
  @Input() currentTab = 'tab-current'
  @Input() currPage = 1


  loginUser = getLoggedUserData()
  isVirtual = false
  public blNumber = ''
  groundArray = []
  isInit = false
  isMobile = isMobile()

  constructor(
    private _parserFormatter: NgbDateParserFormatter,
    private _bookingService: ViewBookingService,
    private _toastr: ToastrService,
    private _dataService: SharedService
  ) {
    super()
  }

  async ngOnInit() {
    setTimeout(async () => {
      this.groundArray = changeCase(await this._bookingService.getPortsData('ground').toPromise() as any, 'camel')
    }, 0);
    loading(true)
    this.setPageConfig()
    setTimeout(() => {
      try {
        if (localStorage.getItem('isVirtualAirline') && JSON.parse(localStorage.getItem('isVirtualAirline')))
          this.isVirtual = true
      } catch { }
    }, 0)

    this._dataService.bookingsRefresh.pipe(untilDestroyed(this)).subscribe(state => {
      if (state) {
        try {
          localStorage.removeItem('book-filter')
        } catch { }
        this.filterRecords()
        this._dataService.bookingsRefresh.next(null)
      }
    })
    this.setInitDate()
    try {
      const _modes = await this._bookingService.getShipModes().toPromise() as Array<any>
      try {
        const res: JsonResponse = await this._bookingService.getLogisticServices(this.loginUser.ProviderID).toPromise() as any
        const logisticServices = res.returnObject
        const freightList: Array<string> = []
        logisticServices.forEach(servie => {
          const { LogServCode } = servie
          if (LogServCode.toUpperCase() === 'SEA_FFDR')
            freightList.push('SEA')
          else if (LogServCode.toUpperCase() === 'AIR_FFDR')
            freightList.push('AIR')
          else if (LogServCode.toUpperCase() === 'TRUK')
            freightList.push('TRUCK')
          else if (LogServCode.toUpperCase() === 'WRHS')
            freightList.push('WAREHOUSE')
          else if (LogServCode.toUpperCase() === 'ASYER' || LogServCode.toUpperCase() === 'ASSAYER')
            freightList.push('ASSAYER')
        })
        const _filteredModes = _modes.filter(freight => freightList.includes(freight.code))
        this.shippingModes = _filteredModes
        if (_filteredModes.length === 1) {
          this.selectedShippingModeCode = _filteredModes[0].code
        }
      } catch {
        const _filtered = _modes.filter(mode => mode.code !== 'RAIL' && mode.code !== 'HYPERLOOP')
        this.shippingModes = _filtered
      }
    } catch { }
    try { await this.getBookingStatuses((this.selectedShippingModeCode) ? this.selectedShippingModeCode : 'SEA') } catch { }
    try { await this.getShippingStatus() } catch { }
    // if() {
    // }
    try { await this.setPortsData((this.selectedShippingModeCode) ? this.selectedShippingModeCode : 'SEA', false) } catch { }

    try {
      const _companies = await this._bookingService.getAllCompanies(this.loginUser.ProviderID).toPromise() as any
      this.companyList = _companies
    } catch { }

    try {
      const _carriers = await this._bookingService.getAllCarriers().toPromise() as any
      this.carriersList = _carriers
    } catch { }
    this.filterRecords()

  }

  isHovered = date =>
    this.fromDate &&
    !this.toDate &&
    this.hoveredDate &&
    after(date, this.fromDate) &&
    before(date, this.hoveredDate)
  isInside = date => after(date, this.fromDate) && before(date, this.toDate)
  isFrom = date => equals(date, this.fromDate)
  isTo = date => equals(date, this.toDate)


  async getRequestStatusList() {

    try {
      const res = await this._bookingService.getRateRequestStatusList().toPromise() as JsonResponse
      if (res.returnId > 0 && typeof res.returnObject === 'object' && isArrayValid(res.returnObject, 0))
        this.requestStatusList = res.returnObject
      else
        this.requestStatusList = []
    } catch { }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.carriersList && this.carriersList.length > 0 && changes.currPage && changes.currPage.currentValue) {
      if ((changes.currPage && changes.currPage.currentValue) || (this.carriersList && this.carriersList.length > 0))
        this.onFilterAction()
    }
    if (this.carriersList && this.carriersList.length > 0 && changes.currentTab && changes.currentTab.currentValue)
      this.onFilterAction()
  }

  setInitDate() {
    const date = new Date()
    this.minDate = { month: date.getMonth() + 1, day: date.getDate(), year: date.getFullYear() }
    this.maxDate = {
      year: ((this.minDate.month === 12) ? date.getFullYear() + 1 : date.getFullYear()),
      month: moment(date).add(30, 'days').month() + 1,
      day: moment(date).add(30, 'days').date()
    }
  }
  clearFilter(event) {
    event.preventDefault()
    event.stopPropagation()
    this.fromDate = {}
    this.toDate = {}
    this.isMarketplace = false
    this.filterDestination = {}
    this.filterOrigin = {}
    this.hmBookingNum = null
    this.selectedBookStatusCode = null
    this.selectedShipStatusCode = null
    this.selectedShippingModeCode = null
    this.selectedProvider = {} as any
    this.selectedCarrier = {} as any
    this.selectedCompany = {} as any
    this.selectedCargoType = null
    this.sortBy = 'date'
    this.currPage = 1
    this.blNumber = null
    try {
      localStorage.removeItem('book-filter')
    } catch { }
    setTimeout(() => {
      try { this.calenderField.nativeElement.value = null }
      catch { }
    }, 0)
    this.filterRecords()
  }

  portsFilter = (text$: Observable<string>) => (text$.debounceTime(200).
    map(term => !term || term.length < 3 ? [] : this.getFilteredPorts(term, this.selectedShippingModeCode)))

  getFilteredPorts(term, transportMode) {
    const { ports } = this
    let toSend = []
    if (transportMode === 'AIR') {
      const _firstIteration: Array<any> = ports.filter(v => v.code.toLowerCase().indexOf(term.toLowerCase()) > -1)
      const _secondIteration: Array<any> = ports.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1)
      const _combinedArr = _firstIteration.concat(_secondIteration)
      toSend = removeDuplicates(_combinedArr, 'code')
    } else {
      if (this.groundArray && this.groundArray.length > 0) {
        const { groundArray } = this
        toSend = ports.concat(groundArray).filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1)
      } else {
        toSend = ports.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1)
      }
    }
    return toSend
  }

  filterPortsFormatter = (x) => x.title
  carrierFilter = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => (!term || term.length < 2) ? []
        : this.carriersList.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1))
    )
  filterCarrierFormatter = (x) => x.title

  companyFilter = (text$: Observable<string>) => text$.pipe(debounceTime(200),
    map(term => (!term || term.length < 2) ? []
      : this.companyList.filter(v => v.Title.toLowerCase().indexOf(term.toLowerCase()) > -1))
  )

  filtercompanyFormatter = (x) => x.Title

  showbutton() {
    try { this.eventDate.toggle() } catch { }
  }

  onDateSelection(date: NgbDateStruct) {
    let parsed = ''
    if (!this.fromDate && !this.toDate)
      this.fromDate = date
    else if (this.fromDate && !this.toDate && after(date, this.fromDate))
      this.toDate = date
    else {
      this.toDate = null
      this.fromDate = date
    }
    if (this.fromDate)
      parsed += this._parserFormatter.format(this.fromDate)
    if (this.toDate)
      parsed += ' - ' + this._parserFormatter.format(this.toDate)
    setTimeout(() => {
      this.calenderField.nativeElement.value = parsed
      if (this.toDate)
        this.eventDate.toggle()
    }, 0)
  }

  shippings = (text$: Observable<string>) =>
    text$.pipe(debounceTime(200), map(term => (!term || term.length < 3) ? []
      : this.ports.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1 ||
        v.shortName.toLowerCase().indexOf(term.toLowerCase()) > -1 || v.code.toLowerCase().indexOf(term.toLowerCase()) > -1)))

  shippingFormatter = (x: { title: string }) => x.title

  async getBookingStatuses(selectedShippingModeCode) {
    const toSend = (selectedShippingModeCode === 'WAREHOUSE') ? 'WAREHOUSE' : 'BOOKING'
    try {
      const res = await this._bookingService.getBookingStatuses(toSend).toPromise() as JsonResponse
      if (res.returnId > 0) {
        const { returnObject } = res
        this.bookingStatuses = returnObject.filter(status => status.BusinessLogic.toUpperCase() !== 'IN-TRANSIT' && status.BusinessLogic.toUpperCase() !== 'COMPLETED')
      }
    } catch { }
  }

  async getShippingStatus() {
    try {
      const res = await this._bookingService.getBookingStatuses('SHIPMENT').toPromise() as JsonResponse
      if (res.returnId > 0) {
        this.shippingStatuses = res.returnObject
        this.shippingStatuses.push({ BusinessLogic: 'PENDING', StatusCode: 'PENDING', StatusID: -1, StatusName: 'Pending', })
      }
    } catch { }
  }

  async setPortsData(selectedShippingModeCode, isLoading: boolean) {
    const _ports: Array<any> = (localStorage.getItem('shippingPortDetails')) ? JSON.parse(localStorage.getItem('shippingPortDetails')) : []
    const ports_to_get = (selectedShippingModeCode.toLowerCase() === 'truck') ? 'sea' : selectedShippingModeCode.toLowerCase()
    const filteredPorts: Array<any> = _ports.filter(port => port.type.toLowerCase().includes(ports_to_get))
    const hasMode = filteredPorts.length > 0 ? true : false
    if (hasMode) {
      this.ports = filteredPorts
    } else {
      try {
        if (isLoading)
          loading(true)
        const res = await this._bookingService.getPortsData(ports_to_get.toUpperCase()).toPromise()
        if (isLoading)
          loading(false)
        const newPorts = _ports.concat(res)
        const _filteredPorts: Array<any> = newPorts.filter(port => port.type.toLowerCase().includes(ports_to_get))
        this.ports = _filteredPorts
        localStorage.setItem('shippingPortDetails', JSON.stringify(newPorts))
      } catch {
        if (isLoading) {
          loading(false)
        }
      }
    }
  }

  getDatefromObj(dateObject) {
    let toSend = null
    try {
      toSend = moment(new Date(dateObject.year, dateObject.month - 1, dateObject.day)).format('YYYY-MM-DD')
    } catch (error) { }
    return toSend
  }

  isEmpty(s) {
    return !s.length;
  }

  onFilterAction() {
    this.filterRecords()
  }

  filterRecords() {
    loading(true)
    let _fromDate, _toDate
    try {
      _fromDate = this.fromDate.day ? this.getDatefromObj(this.fromDate) : null
    } catch { }
    try {
      _toDate = this.toDate.day ? this.getDatefromObj(this.toDate) : null
    } catch { }

    if (_fromDate && !_toDate)
      _toDate = _fromDate

    let tabToSend
    switch (this.currentTab) {
      case 'tab-current':
        tabToSend = 'CURRENT'
        break
      case 'tab-past':
        tabToSend = 'PAST'
        break
      case 'tab-special':
        tabToSend = 'SpecialRequest'
        break
      case 'tab-total':
        tabToSend = 'TOTAL'
        break

      default:
        tabToSend = 'CURRENT'
        break
    }
    let _selectedShippingModeId: any
    try {
      if (this.shippingModes.length > 0) {
        const { selectedShippingModeCode } = this
        _selectedShippingModeId = this.shippingModes.filter(mode => mode.code.toLowerCase() === selectedShippingModeCode.toLowerCase())[0].id
      }
    } catch { }
    const toSend = {
      userID: this.loginUser.UserID,
      providerID: this.loginUser.ProviderID,
      bookingNumber: (this.hmBookingNum) ? this.hmBookingNum.replace(/\s/g, '') : null,
      bookingStatusCode: (this.selectedBookStatusCode && this.selectedBookStatusCode !== 'null') ? this.selectedBookStatusCode : null,
      shipmentStatusCode: (this.selectedShipStatusCode && this.selectedShipStatusCode !== 'null') ? this.selectedShipStatusCode : null,
      polID: (this.filterOrigin && this.filterOrigin.id) ? this.filterOrigin.id : 0,
      polCode: (this.filterOrigin && this.filterOrigin.code) ? this.filterOrigin.code : null,
      polType: (this.filterOrigin && this.filterOrigin.code && this.selectedShippingModeCode && this.selectedShippingModeCode !== 'null') ? this.filterOrigin.type.toUpperCase() : null,
      podID: (this.filterDestination && this.filterDestination.id) ? this.filterDestination.id : 0,
      podCode: (this.filterDestination && this.filterDestination.code) ? this.filterDestination.code : null,
      podType: (this.filterDestination && this.filterDestination.code && this.selectedShippingModeCode && this.selectedShippingModeCode !== 'null') ? this.filterDestination.type.toUpperCase() : null,
      cargoType: (this.selectedCargoType && this.selectedCargoType !== 'null') ? this.selectedCargoType : null,
      shippingModeID: (_selectedShippingModeId) ? _selectedShippingModeId : 0,
      fromDate: (_fromDate) ? _fromDate : null,
      toDate: (_toDate) ? _toDate : null,
      filterProviderID: 0,
      filterCompanyID: (this.selectedCompany && this.selectedCompany.ID) ? this.selectedCompany.ID : 0,
      carrierID: (this.selectedCarrier && this.selectedCarrier.id) ? this.selectedCarrier.id : 0,
      sortBy: this.sortBy,
      sortDirection: 'DESC',
      pageSize: 10,
      pageNumber: this.currPage,
      blNumber: (this.blNumber) ? this.blNumber : null,
      tab: tabToSend,
    }
    if (this.currentTab === 'tab-special') {
      this._bookingService.getSpecialBookings(toSend).subscribe((res: JsonResponse) => {
        this.onBookingResponse(res)
      }, err => {
        loading(false)
        console.log(err)
        this._toastr.error('Please try again laters', 'Failed')
      })
    } else {
      this._bookingService.getBookingsByPage(toSend).subscribe((res: JsonResponse) => {
        this.onBookingResponse(res)
      }, err => {
        loading(false)
        console.log(err)
        this._toastr.error('Please try again laters', 'Failed')
      })
    }
  }

  onBookingResponse(res: JsonResponse) {
    loading(false)
    const { returnText, returnObject, returnId } = res
    if (returnId > 0)
      this.onListChange.emit({ ...returnObject, pageNumber: this.currPage })
    else
      this._toastr.error(returnText, 'Failed')
  }

  onSortChange($sortBy: string) {
    this.sortBy = $sortBy
    this.onFilterAction()
  }

  onModeChange($mode: string) {
    const { selectedShippingModeCode } = this
    if (selectedShippingModeCode !== 'WAREHOUSE' || selectedShippingModeCode !== 'HYPERLOOP') {
      this.filterOrigin = {}
      this.filterDestination = {}
      this.blNumber = null
    }
    this.setPortsData($mode, true)
    this.selectedShipStatusCode = null
    this.selectedBookStatusCode = null
    this.selectedCargoType = null
    this.filterOrigin = {} as any
    this.filterDestination = {} as any
    this.selectedCarrier = {} as any
    this.getBookingStatuses($mode)
  }

  getCarrierImage = ($image: string) => getImagePath(ImageSource.FROM_SERVER, $image, ImageRequiredSize.original)

  getProviderImage = ($image) => getImagePath(ImageSource.FROM_SERVER, getProviderImage($image), ImageRequiredSize.original)

  ngOnDestroy() { }
}