import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddCustomerComponent } from '../../../../shared/dialogues/add-customer/add-customer.component';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { baseExternalAssets } from '../../../../constants/base.url';
import { getImagePath, ImageSource, ImageRequiredSize, isJSON, loading, getLoggedUserData } from '../../../../constants/globalFunctions';
import { PaginationInstance } from 'ngx-pagination';
import { CustomersService } from '../customers/customers.service';
import { firstBy } from 'thenby';
import { SharedService } from '../../../../services/shared.service';
import { PartnerRegComponent } from '../../../../shared/dialogues/partner-reg/partner-reg.component';
import { ConfirmDialogGenComponent } from '../../../../shared/dialogues/confirm-dialog-generic/confirm-dialog-generic.component';
import { ToastrService } from 'ngx-toastr';
import { JsonResponse } from '../../../../interfaces/JsonResponse';


@Component({
  selector: 'app-partner',
  templateUrl: './partner.component.html',
  styleUrls: ['./partner.component.scss']
})
export class PartnersComponent implements OnInit, OnDestroy {
  public userProfile: any
  public customers: any[] = []
  public loading: boolean = false
  public searchUser: any;

  //Pagination work
  public maxSize: number = 7;
  public directionLinks: boolean = true;
  public autoHide: boolean = false;
  public userPaginationConfig: PaginationInstance = {
    id: "users",
    itemsPerPage: 20,
    currentPage: 1
  };

  pgNum1 = 1
  pgNum2 = this.userPaginationConfig.itemsPerPage

  public labels: any = {
    previousLabel: "",
    nextLabel: ""
  };
  public partners: Partners[] = []
  public mainList: Partners[] = []
  selectedFilterOpt: string = 'Name'
  dashboardData

  constructor(
    private _modalService: NgbModal,
    private _customersService: CustomersService,
    private _sharedService: SharedService,
    private _toastr: ToastrService
  ) { }

  ngOnInit() {
    this.userProfile = getLoggedUserData()
    this._sharedService.dashboardDetail.subscribe(data => {
      if (data !== null) {
        this.dashboardData = data;
      }
    });
    this.getProviderPartnerList()
  }

  addPartner($partner) {
    const modalRef = this._modalService.open(PartnerRegComponent, {
      size: "lg",
      backdrop: "static",
      keyboard: false
    });

    let obj = {
      UserID: this.userProfile.UserID,
      CompanyName: this.dashboardData.CompanyName,
      CompanyID: this.dashboardData.CompanyID
    };

    modalRef.componentInstance.shareUserObject = obj;
    modalRef.componentInstance.isPartner = true;
    modalRef.componentInstance.partnerData = $partner;

    modalRef.result.then(result => {
      if (result) {
        this.getProviderPartnerList();
      }
    });
    setTimeout(() => {
      if (
        document
          .getElementsByTagName("body")[0]
          .classList.contains("modal-open")
      ) {
        document.getElementsByTagName("html")[0].style.overflowY = "hidden";
      }
    }, 0);
  }


  /**
   * GET CUSTOMERS FOR PROVIDER
   *
   * @memberof CustomersComponent
   */
  getProviderPartnerList() {
    loading(true)
    this._customersService.getProviderPartnerList(this.userProfile.ProviderID).pipe(untilDestroyed(this)).subscribe((res: any) => {
      loading(false)
      if (res.returnId > 0) {
        this.mainList = res.returnObject
        this.mainList.forEach(_customer => {
          const {
            partnerName,
            cityName,
            countryName,
            partnerAddress
          } = _customer
          _customer.searchField = partnerName + ' ' + cityName + ' ' + countryName + '' + partnerAddress
        })
        this.partners = res.returnObject
        this.onOptionChange('Date added')
      }
    }, (err: any) => {
      loading(false)
    })
  }

  /**
   * [Get UI image path from server]
   * @param  $image [description]
   * @param  type   [description]
   * @return        [description]
   */
  getProviderImage($image: string) {
    if (isJSON($image)) {
      const providerImage = JSON.parse($image)
      return baseExternalAssets + '/' + providerImage[0].DocumentFile
    } else {
      return getImagePath(ImageSource.FROM_SERVER, $image, ImageRequiredSize.original)
    }
  }

  onOptionChange($option: string) {
    this.selectedFilterOpt = $option
    const { mainList } = this

    // console.log($option);

    if ($option === 'Date added') {
      // console.log($option);

      this.partners = mainList.sort(function (a, b) {
        const _a = Number(new Date(a.createdDate).getTime())
        const _b = Number(new Date(b.createdDate).getTime())
        // console.log(_a, _b);

        return _a - _b;
      });
    }
    if ($option === 'Name') {
      this.partners = mainList.sort(
        firstBy('partnerName')
        // .thenBy('createdDate')
        // .thenBy('cityName')
        // .thenBy('partnerAddress')
      )
    }
    if ($option === 'Location') {
      this.partners = mainList.sort(
        firstBy('countryName')
        // .thenBy('createdDate')
        // .thenBy('partnerName')
        // .thenBy('partnerAddress')
      )
    }
    if ($option === 'Address') {
      this.partners = mainList.sort(
        firstBy('partnerAddress')
        // .thenBy('createdDate')
        // .thenBy('partnerName')
        // .thenBy('cityName')
      )
    }
    // Name
    // Date added
    // Location
    // Address
  }

  onPageChange(number: any) {
    this.userPaginationConfig.currentPage = number;
    const { currentPage, itemsPerPage } = this.userPaginationConfig
    if (this.userPaginationConfig.currentPage > 1) {
      this.pgNum1 = itemsPerPage * (currentPage - 1)
      const totalPages = Math.ceil(this.partners.length / itemsPerPage)
      this.pgNum2 = (currentPage !== totalPages) ? (itemsPerPage * currentPage) : this.partners.length
    }
  }

  removePartner($partnerID: number) {

    const _modalData = {
      messageTitle: 'Remove Partner',
      messageContent: `Are you sure you want to remove this partner?`,
      buttonTitle: 'Yes',
      data: null
    }

    const modalRef = this._modalService.open(ConfirmDialogGenComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'medium-modal',
      backdrop: 'static',
      keyboard: false
    });

    modalRef.result.then((result: string) => {
      if (result) {
        loading(true)
        const { UserID, ProviderID } = getLoggedUserData()
        this._customersService.deletePartner($partnerID, UserID, ProviderID).subscribe((res: JsonResponse) => {
          loading(false)
          if (res.returnId > 0) {
            const { partners } = this
            loading(false)
            this.partners = partners.filter(_partner => _partner.partnerID !== $partnerID)
            if (res.returnCode === '2') {
              this._toastr.info(res.returnText, res.returnStatus)
            } else {
              this._toastr.success(res.returnText, 'Success')
            }
          } else {
            loading(false)
            this._toastr.error(res.returnText, 'Failed')
          }
        }, error => {
          this._toastr.error('Unable to process your request, please try again later.', 'Failed')
          loading(false)
        })
      }
    })

    modalRef.componentInstance.modalData = _modalData;
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);
  }

  ngOnDestroy() {

  }

}


export interface Partners {
  mapID: number;
  partnerID: number;
  partnerCode: string;
  partnerName: string;
  createdDate: any;
  cityID?: any;
  cityName?: any;
  countryID?: any;
  countryCode?: any;
  countryName?: any;
  partnerAddress: string;
  contactPerson: string;
  primaryPhone: string;
  primaryEmail: string;
  partnerLogo?: any;
  searchField?: any
}
