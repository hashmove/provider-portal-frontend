import { Component, OnInit, Output, Input, EventEmitter, OnChanges, SimpleChanges, ViewChild, ElementRef } from '@angular/core'
import * as moment from 'moment'

@Component({
    selector: 'app-count-down',
    templateUrl: './count-down.component.html',
    styleUrls: ['./count-down.component.scss']
})
export class CountDownComponent implements OnInit, OnChanges {

    @Input() expiryDate: string
    @Input() bookDate: string
    @Input() bookingID: -1
    @Output() timoutAction = new EventEmitter<any>()

    appCountDays: string
    appCountHours: string
    appCountMinutes: string
    appCountSeconds: string
    isExpired = false

    constructor() {

    }

    ngOnInit() {
        const second = 1000, minute = second * 60, hour = minute * 60, day = hour * 24
        const countDown = new Date(this.expiryDate).getTime()
        const _counterInterval = setInterval(() => {
            const now = new Date().getTime()
            let distance = countDown - now
            const _appCountHours = Math.floor((distance) / (hour)) as any
            const _appCountMinutes = Math.floor((distance % (hour)) / (minute)) as any
            const _appCountSeconds = Math.floor((distance % (minute)) / second) as any
            if (distance >= 0) {
                this.appCountHours = padDigits(_appCountHours, 2)
                this.appCountMinutes = padDigits(_appCountMinutes, 2)
                this.appCountSeconds = padDigits(_appCountSeconds, 2)
            } else {
                this.isExpired = true
                this.appCountHours = padDigits(0, 2)
                this.appCountMinutes = padDigits(0, 2)
                this.appCountSeconds = padDigits(0, 2)
                this.timoutAction.emit({ bookingID: this.bookingID, expired: true })
                clearInterval(_counterInterval)
            }
        }, 0)
    }

    getNum = (_num: any) => (_num) ? (parseFloat(_num) ? typeof parseFloat(_num) === 'number' ? parseFloat(_num) : 0 : 0) : 0

    ngOnChanges(changes: SimpleChanges) { }
}

function padDigits(number, digits) {
    return Array(Math.max(digits - String(number).length + 1, 0)).join(0 as any) + number
}