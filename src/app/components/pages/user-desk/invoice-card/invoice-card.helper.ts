import { NgbModalOptions } from '@ng-bootstrap/ng-bootstrap'
import { baseExternalAssets } from '../../../../constants/base.url'
import { getWHUnit, isJSON, getImagePath, ImageSource, ImageRequiredSize, getProviderImage, isArrayValid } from '../../../../constants/globalFunctions'
import { BookingsHelper } from '../all-bookings/bookings.helper'
import { IInvoiceTrail } from './invoice-card.interface'

export class InvoiceCardHelper extends BookingsHelper {

    modalLargeObject: NgbModalOptions = {
        size: 'lg', backdrop: 'static', centered: true, windowClass: 'large-modal', keyboard: false
    }
    modalDocumentObject: NgbModalOptions = {
        size: 'lg', backdrop: 'static', centered: true, windowClass: 'document-modal', keyboard: false
    }
    modalSmallObject: NgbModalOptions = {
        size: 'lg', backdrop: 'static', centered: true, windowClass: 'small-modal', keyboard: false
    }
    modalMediumbject: NgbModalOptions = {
        size: 'lg', centered: true, windowClass: 'medium-modal', backdrop: 'static', keyboard: false
    }

    cancelBookingMessage = {
        messageTitle: 'Discard Request',
        messageContent: 'Are you sure you want to discard your request?',
        openedFrom: 'cancel-booking',
        data: '',
        buttonTitle: 'Yes'
    }

    discardRequestMessage = {
        messageTitle: 'Discard Spot Rate',
        messageContent: 'Are you sure you want to discard this spot rate request?',
        openedFrom: '/user/dashboard',
        buttonTitle: 'Yes, I want to discard',
    }

    discardBookingMessage = {
        messageTitle: 'Discard Booking',
        messageContent: 'Are you sure you want to discard the booking?',
        openedFrom: '/user/dashboard',
        buttonTitle: 'Yes, I want to discard',
    }

    statusGroups: IInvoiceTrail[] = []

    constructor() { super() }

    setWarehouseData(_searchCriteria, _bookings) {
        try {
            return {
                IsTransportAvailable: _bookings.IsTransportAvailable,
                videoURL: null,
                WHParsedMedia: null,
                WHID: null,
                WHName: _bookings.WHName,
                WHDesc: _bookings.WHDesc,
                CountryID: null,
                CityID: null,
                WHAddress: _bookings.WHAddress,
                UsageType: _bookings.UsageType ? _bookings.UsageType : _searchCriteria.storageType.toUpperCase(),
                TotalCoveredArea: null,
                TotalCoveredAreaUnit: getWHUnit(_searchCriteria),
                WHGallery: null,
                CountryCode: null,
                CountryName: null,
                CityName: _searchCriteria.CityName ? _searchCriteria.CityName.split(',')[0] : _bookings.WHCityName,
                CityShortName: null,
                IsBonded: _bookings.IsBondedWarehouse,
                EffectiveFrom: _searchCriteria.StoreFrom,
                EffectiveTo: _searchCriteria.StoreUntill,
                CurrencyCode: _bookings.CurrencyCode,
                Price: null,
                AddChrName: null,
                WHLatitude: null,
                WHLongitude: null,
                ContainerSpecID: null,
                StorageType: _bookings.StorageType,
                ContainerSpecDesc: _searchCriteria.searchBy === 'by_container' ?
                    _searchCriteria.SearchCriteriaContainerDetail[0].contSize : null,
                CreditDays: null,
                ProviderImage: _bookings.ProviderImage,
                StorageTypeTitle: _bookings.StorageTypeTitle,
            }
        } catch {
            return null
        }
    }

    setCBM($containers) {
        const _totalCBM = $containers.reduce((a, b) => +a + +b.contRequestedCBM, 0)
        return _totalCBM ? Math.ceil(_totalCBM) : 1
    }

    getUIImage = (img: string) => (isJSON(img)) ? baseExternalAssets + '/' + JSON.parse(img)[0].DocumentFile :
        getImagePath(ImageSource.FROM_SERVER, img, ImageRequiredSize.original)

    getProviderImage = ($image: string) => getImagePath(ImageSource.FROM_SERVER, getProviderImage($image), ImageRequiredSize.original)

    isParent = (bookings) => bookings.HMParentBookingNum ? true : false

    getAddInvObject = (): IInvoiceTrail => ({
        invoiceID: -1,
        customerInvoiceID: -1,
        receiptID: -1,
        paymentID: -1,
        invoiceAmount: 0,
        invoiceNo: null,
        invoiceUpload: [],
        isInvoiceUploaded: false,
        uploadStatus: null,
        invoiceApproval: [],
        isInvoiceApproved: false,
        approvalStatus: null,
        invoiceCustomerUpload: [],
        isInvoiceCustomerUploaded: false,
        customerUploadStatus: null,
        paymentProcessCustomer: [],
        customerLogs: [],
        isCustPaymentPrcocessed: false,
        custPayStatus: null,
        paymentToPartner: [],
        paymentLogs: [],
        isPartnerPaymentPrcocessed: false,
        ffPayStatus: null,
        invUploadMsg: 'Pending Upload'
    })

    addInvoice() {
        if (isArrayValid(this.statusGroups, 0) && this.statusGroups.filter(_inv => _inv.invoiceID === -1).length > 0)
            return
        this.statusGroups.push(this.getAddInvObject())
    }

}
