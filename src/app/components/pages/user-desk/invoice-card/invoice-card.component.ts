import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { ToastrService } from 'ngx-toastr'
import { Router } from '@angular/router'
import { InvoiceCardHelper } from './invoice-card.helper'
import { statusCode, getLoggedUserData, getDateDiff, encryptBookingID, loading, isArrayValid, isMobile } from '../../../../constants/globalFunctions'
import { JsonResponse } from '../../../../interfaces'
import { CargoDetailsComponent } from '../../../../shared/dialogues/cargo-details/cargo-details.component'
import { ITermOption } from '../../../../shared/dialogues/payment-terms/payment-terms.interface'
import { IBookingSchedule } from '../../../../shared/dialogues/vessel-schedule-dialog/schedule.interface'
import { ViewBookingService } from '../view-booking/view-booking.service'
import { InvoiceUploadComponent } from '../../../../shared/dialogues/invoice-upload/invoice-upload.component'
import { IInvoiceCard, IInvoiceTrail, InvStatuDetail } from './invoice-card.interface'
import { firstBy } from 'thenby'
import { InvoiceDocumentsComponent } from '../../../../shared/invoice-documents/invoice-documents.component'
import { InvoicePaymentComponent } from '../../../../shared/dialogues/invoice-payment/invoice-payment.component'
@Component({
  selector: 'app-invoice-card',
  templateUrl: './invoice-card.component.html',
  styleUrls: ['./invoice-card.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InvoiceCardComponent extends InvoiceCardHelper implements OnInit {

  @Input() invoice: IInvoiceCard
  @Input() isChild = false
  @Output() bookingRemove = new EventEmitter<number>()
  @Output() updateBookDesc = new EventEmitter<string>()
  public dateDifference: any
  public specialStatus: string
  public cachedLogs: any[] = []
  public isExpanded = false
  public termsData: any
  public expiryDate
  public totalCBM = 0
  public showSchedule = false
  public routesDirection = []
  public bookingSchedule: IBookingSchedule
  public statusCode = statusCode
  userData: any
  paymentTermsOptions: ITermOption[] = []
  showDtl = false
  searchMode = 'sea-fcl'
  isInvViewActive = true
  isMobile = isMobile()

  constructor(
    private _router: Router,
    private _modalService: NgbModal,
    private _bookingService: ViewBookingService,
    private _toastr: ToastrService,
  ) {
    super()
  }

  ngOnInit() {
    this.userData = getLoggedUserData()
    this.searchMode = this.invoice.ShippingModeCode.toLowerCase() + '-' + this.invoice.ContainerLoad.toLowerCase()
    this.getInvoiceTrail(false)
    try {
      this.dateDifference = getDateDiff(this.invoice.WarehouseInfo.StoredUntilUtc, this.invoice.WarehouseInfo.StoredFromUtc, 'days', 'YYYY-MM-DD')
      if (this.searchMode === 'sea-lcl')
        this.totalCBM = Math.ceil(this.invoice.TotalCBM)
    } catch { }
  }

  getInvoiceTrail(byPassCheck: boolean) {
    setTimeout(() => {
      if ((this.invoice.InvoiceInfo && isArrayValid(this.invoice.InvoiceInfo, 0)) || byPassCheck) {
        this._bookingService.getInvoiceDet(this.invoice.BookingID).subscribe((res: JsonResponse) => {
          loading(false)
          this.statusGroups = []
          if (res.returnId > 0 && isArrayValid(res.returnObject, 0)) {
            this.invoice.InvoiceDet = res.returnObject
            this.statusGroups = []
            this.setInvoiceStatuses()
          } else {
            this.setInitInvoice()
          }
        }, () => {
          loading(false)
          this.setInitInvoice()
        })
      } else {
        this.setInitInvoice()
      }
    }, 0);
  }

  setInitInvoice() {
    this.statusGroups = []
    this.statusGroups.push(this.getAddInvObject())
    this.isInvViewActive = true
  }

  setInvoiceStatuses() {
    const { InvoiceDet } = this.invoice
    // this.isInvViewActive = InvoiceDet.length === 1 ? true : false
    InvoiceDet.forEach(invoiceDet => {
      // console.log('invoiceDet:', invoiceDet);
      let _statusGroup: IInvoiceTrail = {
        invoiceID: invoiceDet.InvoiceID,
        customerInvoiceID: invoiceDet.CustomerInvoiceID,
        receiptID: invoiceDet.ReceiptID,
        paymentID: invoiceDet.PaymentID,
        invoiceAmount: invoiceDet.InvoiceAmount,
        invoiceNo: invoiceDet.InvoiceNo,
        customerlastStatus: invoiceDet.CustomerStatusBL,
        lastStatus: invoiceDet.LastInvoiceStatusBL,
        invoiceUpload: isArrayValid(invoiceDet.InvStatuDetail, 0) ? invoiceDet.InvStatuDetail.filter(_sub => _sub.StatusGroup === 'INVOICE_UPLOAD').sort(firstBy('StatusDate', { direction: -1 })) : [],
        invoiceApproval: isArrayValid(invoiceDet.InvStatuDetail, 0) ? invoiceDet.InvStatuDetail.filter(_sub => _sub.StatusGroup === 'INVOICE_APPROVAL').sort(firstBy('StatusDate', { direction: -1 })) : [],
        paymentProcessCustomer: isArrayValid(invoiceDet.InvStatuDetail, 0) ? invoiceDet.InvStatuDetail.filter(_sub => _sub.StatusGroup === 'PAYMENT_PROCESS_CUSTOMER').sort(firstBy('StatusDate', { direction: -1 })) : [],
        paymentToPartner: isArrayValid(invoiceDet.InvStatuDetail, 0) ? invoiceDet.InvStatuDetail.filter(_sub => _sub.StatusGroup === 'PAYMENT_TO_PARTNER').sort(firstBy('StatusDate', { direction: -1 })) : [],
      } as any
      invoiceDet
      _statusGroup = {
        ..._statusGroup,
        isInvoiceUploaded: isArrayValid(_statusGroup.invoiceUpload, 0) ?
          _statusGroup.invoiceUpload.map(_state => _state.StatusBL).includes('INVOICE_UPLOADED') : false,
        uploadStatus: isArrayValid(_statusGroup.invoiceUpload, 0) ? _statusGroup.invoiceUpload[0] : null,

        isInvoiceApproved: isArrayValid(_statusGroup.invoiceApproval, 0) ?
          _statusGroup.invoiceApproval.map(_state => _state.StatusBL).includes('INVOICE_APPROVED') : false,
        approvalStatus: isArrayValid(_statusGroup.invoiceApproval, 0) ? _statusGroup.invoiceApproval[0] : null,

        isCustPaymentPrcocessed: isArrayValid(_statusGroup.paymentProcessCustomer, 0) ?
          _statusGroup.paymentProcessCustomer.map(_state => _state.StatusBL).includes('PAYMENT_RECEIVED') : false,
        custPayStatus: isArrayValid(_statusGroup.paymentProcessCustomer, 0) ? _statusGroup.paymentProcessCustomer[0] : null,
        customerLogs: this.getCustomerLogs(_statusGroup.paymentProcessCustomer),

        isPartnerPaymentPrcocessed: isArrayValid(_statusGroup.paymentToPartner, 0) ?
          _statusGroup.paymentToPartner.map(_state => _state.StatusBL).join(',').includes('PAID') || _statusGroup.paymentToPartner.map(_state => _state.StatusBL).includes('IN-PROCESS') : false,
        ffPayStatus: isArrayValid(_statusGroup.paymentToPartner, 0) ? _statusGroup.paymentToPartner[0] : null,
        invUploadMsg: invoiceDet.LastInvoiceStatusBL === 'INVOICE_REJECTED' ? 'Re-upload Your Invoice' : 'Upload Your Invoice',
        paymentLogs: (isArrayValid(_statusGroup.paymentToPartner, 0) && _statusGroup.paymentToPartner[0].StatusBL !== 'IN-PROCESS') ? _statusGroup.paymentToPartner.filter(_log => _log.StatusBL !== 'IN-PROCESS') : [],
      }
      this.statusGroups.push(_statusGroup)
    })
  }

  viewBookingDetails(bookingId, userId, shippingModeCode) {
    if (this.invoice.BookingTab.toLowerCase() === 'saved' || this.invoice.BookingTab.toLowerCase() === 'specialrequest')
      return
    this._router.navigate(['/user/booking-detail', encryptBookingID(bookingId, userId, shippingModeCode)])
  }

  openCargoDetails() {
    const { invoice } = this
    const modalRef = this._modalService.open(CargoDetailsComponent, {
      size: 'sm', backdrop: 'static', centered: true, windowClass: 'carge-detail', keyboard: false
    })
    modalRef.componentInstance.data = invoice
    modalRef.componentInstance.containerCount = this.invoice.ContainerCount
  }


  uploadDocuments(_invoice) {
    const modalRef = this._modalService.open(InvoiceUploadComponent, this.modalLargeObject)
    modalRef.result.then(() => {
      this.getInvoiceTrail(true)
    })
    modalRef.componentInstance.invoice = this.invoice
    modalRef.componentInstance.selectedInvoice = _invoice
  }

  viewUploadedInvoice(_invoice: IInvoiceTrail) {
    const modalRef = this._modalService.open(InvoiceDocumentsComponent, this.modalLargeObject)
    modalRef.componentInstance.invoice = this.invoice
    modalRef.componentInstance.selectedInvoice = _invoice
  }

  onApprove(statusGroup: IInvoiceTrail, type: string) {
    if (!statusGroup ||
      !statusGroup.custPayStatus ||
      statusGroup.custPayStatus.StatusBL !== 'PAYMENT_RECEIVED' ||
      statusGroup.ffPayStatus.StatusBL === 'IN-PROCESS'
    ) {
      return
    }
    const modalRef = this._modalService.open(InvoicePaymentComponent, this.modalLargeObject)
    modalRef.componentInstance.invoice = this.invoice
    modalRef.componentInstance.selectedInvoice = statusGroup
    modalRef.componentInstance.groupLogic = type
  }

  getCustomerLogs = (customerLogs: InvStatuDetail[]) =>
    customerLogs.filter(_log => _log.StatusBL !== 'AWAITING_PAYMENT')

  getInvModeIcon(mode: string) {
    if (mode === 'WAREHOUSE') {
      return 'icon_warehouse_blue.svg'
    }
    if (mode === 'AIR') {
      return 'icon_plane_blue.svg'
    }
    if (mode === 'TRUCK') {
      return 'Icons_Cargo_Truck_blue.svg'
    }
    if (mode === 'SEA') {
      return 'icons_cargo_ship_blue.svg'
    }
  }
}
