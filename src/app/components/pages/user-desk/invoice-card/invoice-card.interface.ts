export interface IInvoiceCustomer {
    CustomerID: number
    FirstName: string
    LastName: string
    CustomerName: string
    CustomerUserName: string
    CustomerImage: string
    HMCommisionValue: number
    HMCustomerNature: string
}

export interface IOrigin {
    PolType: string
    PolCode: string
    PolName: string
    PolInputAddress: string
    PolAddress?: any
    PolCountry: string
    PolCityName?: any
    PolCountryCode: string
}

export interface IDestination {
    PodType: string
    PodCode: string
    PodName: string
    PodInputAddress: string
    PodAddress?: any
    PodCountry: string
    PodCountryCode: string
    PodCityName?: any
}

export interface IWarehouseInfo {
    WHID?: any
    WHName: string
    WHCityCode: string
    WHCityName: string
    WHCountryCode: string
    WHCountryName: string
    WHGLocCode: string
    WHGLocName: string
    WHAddress: string
    WHLatitude: string
    WHLongitude: string
    WHImages: string
    WHMinimumLeaseTerm: string
    StoredFromUtc?: any
    StoredUntilUtc?: any
    WHDesc: string
    OfferedAreaUnit: string
    UsageType: string
    IsBondedWarehouse: boolean
    IsTransportAvailable: boolean
    StorageType: string
    StorageTypeTitle: string
}

export interface IProvider {
    ProviderName: string
    ProviderImage: string
    ProviderEmail?: any
    ProviderPhone?: any
    ProviderID: number
}

export interface ICarrier {
    CarrierID: number
    CarrierName: string
    CarrierImage: string
}

export interface IAirDet {
    PickupFrom: string
    PickupTo: string
    ChargeableWeight: number
    BookingWeight: number
    AircraftTypeCode?: any
    ProductName?: any
    IsVirtualAirLine: boolean
    PriceBasisType: string
    JsonPortVia?: any
}

export interface IAssayer {
    TotalDocCharges: number
    CurCodeDocCharges: string
    CommodityName?: any
    TotalWeight?: any
    NumberOfContainers?: any
    CommodityInpectionStatus?: any
    ContainerStuffingStatus?: any
    PolicyName?: any
    PolicyNumber?: any
}

export interface IInvoiceCard {
    TotalCBM: number
    BookingID: number
    Customer: IInvoiceCustomer
    Origin: IOrigin
    Destination: IDestination
    WarehouseInfo: IWarehouseInfo
    Provider: IProvider
    Carrier: ICarrier
    AirDet: IAirDet
    Assayer: IAssayer
    HashMoveBookingNum: string
    HashMoveBookingDate: string
    BookingType: string
    EtaUtc?: any
    ShippingMode: string
    ShippingModeImage: string
    BookingStatus: string
    EtdUtc?: any
    ContainerLoad: string
    UserID: number
    BookingTab: string
    ContainerCount: number
    BookingTotalAmount: number
    CurrencyID: number
    CurrencyCode: string
    BaseCurrTotalAmount: number
    BaseCurrencyID: number
    BaseCurrencyCode: string
    ShippingModeCode: string
    BookingStatusCode: string
    BookingDesc: string
    ShippingCatName: string
    ShippingCatImage: string
    StatusID?: any
    IsBookedByProvider: boolean
    IsLumpSum: boolean
    BookingSourceVia: string
    SpotRateBookingKey: string
    InvoiceDet: IInvoiceDet[]
    InvoiceInfo: IInvoiceInfo[]
    IsInvoiceDetRequiredOnUpload?: boolean
}

export interface IInvoiceInfo {
    CurrencyCode: string
    CurrencyID: number
    InvoiceAmount: number
    InvoiceDate: string
    InvoiceID: number
}

export interface InvStatuDetail {
    StatusID: number
    StatusDate: string
    StatusName: string
    StatusBL: string
    StatusRemarks: string
    StatusColorCode: string
    StatusGroup: string
    RowID: number
    ReasonName?: string
}

export interface IInvoiceDet {
    InvoiceAmount: number
    InvoiceID: number
    InvoiceNo?: string
    InvoiceDate: string
    CustomInvoiceNo?: string
    CustomerInvoiceDate?: string
    CustomerInvoiceID?: number
    CustomerStatusBL?: string
    LastInvoiceStatusID: number
    LastStatusName: string
    LastInvoiceStatusBL: string
    LastInvoiceStatusRemarks: string
    LastStatusColorCode: string
    LastStatusGroup: string
    InvStatuDetail: InvStatuDetail[]

    ReceiptDate?: string
    PaymentDate?: string
    PaymentAmount?: number
    PaymentID?: number
    ReceiptAmount?: number
    ReceiptID?: number
}

export interface IInvoiceTrail {
    invoiceID: number
    customerInvoiceID: number
    receiptID: number
    paymentID: number
    invoiceNo: string
    lastStatus?: string
    customerlastStatus?: string
    invoiceAmount: number

    invoiceUpload: InvStatuDetail[]
    isInvoiceUploaded: boolean
    uploadStatus: InvStatuDetail

    invoiceApproval: InvStatuDetail[]
    isInvoiceApproved: boolean
    approvalStatus: InvStatuDetail

    invoiceCustomerUpload: InvStatuDetail[]
    isInvoiceCustomerUploaded: boolean
    customerUploadStatus: InvStatuDetail

    paymentProcessCustomer: InvStatuDetail[]
    customerLogs: InvStatuDetail[]
    isCustPaymentPrcocessed: boolean
    custPayStatus: InvStatuDetail

    paymentToPartner: InvStatuDetail[]
    paymentLogs: InvStatuDetail[]
    isPartnerPaymentPrcocessed: boolean
    ffPayStatus: InvStatuDetail
    invUploadMsg: string
}