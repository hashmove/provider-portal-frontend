import { Component, OnInit, Input, EventEmitter, Output, ViewEncapsulation } from '@angular/core'
import { isJSON, getImagePath, ImageRequiredSize, ImageSource, encryptBookingID, loading, getLoggedUserData, getWHUnit, getAnimatedGreyIcon, changeCase, isMobile } from '../../../../constants/globalFunctions'
import { Router } from '@angular/router'
import { baseExternalAssets } from '../../../../constants/base.url'
import { NgbModal, NgbTooltipConfig } from '@ng-bootstrap/ng-bootstrap'
import { UpdatePriceComponent } from '../../../../shared/dialogues/update-price/update-price.component'
import { PriceLogsComponent } from '../../../../shared/dialogues/price-logs/price-logs.component'
import { CargoDetailsComponent } from '../../../../shared/dialogues/cargo-details/cargo-details.component'
import { AirAfricaCargoDetailsComponent } from '../../../../shared/dialogues/air-africa-cargo-dtl/air-africa-cargo-dtl.component'
import { ConfirmDialogGenComponent } from '../../../../shared/dialogues/confirm-dialog-generic/confirm-dialog-generic.component'
import { JsonResponse } from '../../../../interfaces/JsonResponse'
import { ToastrService } from 'ngx-toastr'
import { SharedService } from '../../../../services/shared.service'
import { ViewBookingService } from '../view-booking/view-booking.service'
import { SearchCriteriaContainerDetail } from '../../../../shared/dialogues/cargo-details/cargo-details.interface'
import { ChatComponent } from '../../../../shared/dialogues/chat/chat.component'
import { SeaRateDialogComponent } from '../../../../shared/dialogues/sea-rate-dialog/sea-rate-dialog.component'
import { BookingCardHelper } from './booking-card.helper'
import { SeaFreightService } from '../manage-rates/sea-freight/sea-freight.service'
import { DocumentUploadComponent } from '../../../../shared/dialogues/document-upload/document-upload.component'
import { BookingStatusUpdationComponent } from '../../../../shared/dialogues/booking-status-updation/booking-status-updation.component'
import { PaymentTermsDialogComponent } from '../../../../shared/dialogues/payment-terms/payment-terms.component'
import * as moment from 'moment'

@Component({
  selector: 'app-bookings-card',
  templateUrl: './bookings-card.component.html',
  styleUrls: ['./bookings-card.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BookingsCardComponent extends BookingCardHelper implements OnInit {

  @Input() booking: any
  @Output() bookingRemove = new EventEmitter<number>()

  isMobile = isMobile()
  constructor(
    private _router: Router,
    private modalService: NgbModal,
    private _sharedService: SharedService,
    private _seaFreightService: SeaFreightService,
    private _toastr: ToastrService,
    private _bookingService: ViewBookingService,
    private _tooltipConfig: NgbTooltipConfig
  ) {
    super()
  }

  ngOnInit() {
    if (this.isMobile)
      this._tooltipConfig.disableTooltip = true
    try {
      const { BookingExpiryHours, IsBookingExpiryApplied } = getLoggedUserData().ProviderConfig
      if (IsBookingExpiryApplied && this.booking.IsBookingExpiryApplied_Customer) {
        this.showTimer = true
        const _bookDate = new Date(this.booking.HashMoveBookingDate + "Z")
        const _48HDays = ['thursday', 'friday', 'saturday']
        const _24HDays = ['sunday']
        const _bookStrDay = moment(_bookDate).format('dddd').toLowerCase()
        const _addedHours = _48HDays.includes(_bookStrDay) ? 48 : _24HDays.includes(_bookStrDay) ? 24 : 0
        const _expHour = BookingExpiryHours + _addedHours;
        this.expiryDate = moment(_bookDate).add(_expHour, 'hours').format()
        this.bookDate = moment(_bookDate).format()
      }
    } catch (error) {
      console.log(error)
    }
    try {
      this.searchCriteria = JSON.parse(this.booking.JsonSearchCriteria)
      if (this.searchCriteria.searchMode === 'sea-lcl')
        this.setCBM(this.searchCriteria.SearchCriteriaContainerDetail)
    } catch { }
    try {
      if (this.booking.JSONSpecialRequestLogs)
        this.booking.parsedJSONSpecialRequestLogs = JSON.parse(this.booking.JSONSpecialRequestLogs)
    } catch { }
    this.setHmCommission()
    if (this.booking.BookingSourceVia && this.booking.BookingSourceVia.includes('ENTERPRISE'))
      this.isEnterprise = true
    this._sharedService.dashboardDetail.subscribe((state: any) => {
      if (state) {
        try {
          if (state.FeatureToggleJson) {
            const _controls = JSON.parse(state.FeatureToggleJson)
            if (_controls.HMCommissionDisplayFor) {
              if (_controls.HMCommissionDisplayFor === 'ALL')
                this.showCommission = true
              else if (_controls.HMCommissionDisplayFor === 'ELM' && this.isEnterprise)
                this.showCommission = true
              else
                this.showCommission = false
            }
          }
        } catch (error) { console.log(error) }
      }
    })

    if (localStorage.getItem('isVirtualAirline')) {
      try {
        this.isVirtualAirline = JSON.parse(localStorage.getItem('isVirtualAirline'))
      } catch (error) { }
    }
    try {
      if (this.booking.ShippingModeCode.toLowerCase() === 'warehouse')
        this.setWarehouseData()
    } catch (error) { }
  }

  setCBM($containers: SearchCriteriaContainerDetail[]) {
    const _totalCBM = $containers.reduce((a, b) => +a + +b.contRequestedCBM, 0);
    this.totalCBM = _totalCBM ? Math.ceil(_totalCBM) : 1
  }

  setHmCommission() {
    if (this.booking.BookingTab === 'SpecialRequest') {
      try {
        if (this.booking.BookingSpecialAmount && this.booking.BookingSpecialAmount > 0)
          this.hmCommisssion = this.booking.BookingSpecialAmount * (this.booking.HMCommisionRate / 100)
        else
          this.hmCommisssion = this.booking.HMCommisionValue
      } catch {
        this.hmCommisssion = this.booking.HMCommisionValue
      }
    } else
      this.hmCommisssion = this.booking.HMCommisionValue
  }

  getCustomerImage($image: string) {
    if (isJSON($image))
      return baseExternalAssets + '/' + JSON.parse($image)[0].DocumentFile
    else
      return getImagePath(ImageSource.FROM_SERVER, $image, ImageRequiredSize.original)
  }

  viewBookingDetails() {
    loading(true)
    if (this.booking.BookingTab.toLowerCase() === 'saved' || this.booking.BookingTab.toLowerCase() === 'specialrequest')
      return
    this._router.navigate(['/provider/booking-detail', encryptBookingID(this.booking.BookingID, this.booking.ProviderID, this.booking.ShippingModeCode)])
  }

  updatePrice() {
    if (this.booking.BookingSourceVia && this.booking.BookingSourceVia.length > 2)
      SharedService.SET_VB_SOURCE_VIA(this.booking.BookingSourceVia)
    const modalRef = this.modalService.open(UpdatePriceComponent, {
      size: 'lg', centered: true, windowClass: 'large-modal', backdrop: 'static', keyboard: false
    })
    modalRef.result.then((result) => {
      if (result) {
        this.booking.BookingSpecialAmount = result.price ? result.price : 0
        this.booking.SpecialRequestStatus = result.status
        this.booking.SpecialRequestStatusBL = result.statusBl
        this.booking.CurrencyID = result.currencyID
        this.booking.CurrencyCode = result.currencyCode
        this.cachedLogs = result.logs
        try {
          if (result.perRatePrice) {
            this.booking.PerUnitRate = result.perRatePrice
            this.booking.PerUnitRateType = result.rateType
          }
        } catch { }
        if (this.booking.EtdUtc && result.isScheduleUpdate)
          this.getVesselSchedule()

        if (result.carrierName) {
          this.booking.CarrierName = result.carrierName
          this.booking.CarrierID = result.carrierId
          this.booking.CarrierImage = result.carrierImage
        }
        this.priceSent = true
        this.setHmCommission()
      }
    })
    modalRef.componentInstance.data = this.booking
    if (this.searchCriteria && this.searchCriteria.CONT)
      modalRef.componentInstance.searchCriteria = this.searchCriteria
  }

  viewLogs() {
    const modalRef = this.modalService.open(PriceLogsComponent, {
      size: 'lg', centered: true, windowClass: 'large-modal', backdrop: 'static', keyboard: false
    })
    modalRef.result.then(() => { })
    modalRef.componentInstance.data = { booking: this.booking, logs: this.cachedLogs }
  }

  openChat(bookings) {
    const modalRef = this.modalService.open(ChatComponent, {
      size: 'lg', centered: true, windowClass: 'chat-modal', backdrop: 'static', keyboard: false
    })
    modalRef.result.then(result => {
      this.booking.ProviderUnReadMsgCount = 0
      if (result) {
        this.booking.SpecialRequestStatusBL = 'PRICE_SENT'
        this.booking.SpecialRequestStatus = 'PRICE SENT'
      }
    })
    modalRef.componentInstance.data = { booking: this.booking, logs: this.cachedLogs }
  }

  openCargoDetails() {
    const { booking } = this
    if (booking.IsVirtualAirLine) {
      const modalRef = this.modalService.open(AirAfricaCargoDetailsComponent, {
        size: 'lg', centered: true, windowClass: 'carge-detail', backdrop: 'static', keyboard: false
      })
      modalRef.result.then((result) => { })
      modalRef.componentInstance.data = booking
    } else {
      const modalRef = this.modalService.open(CargoDetailsComponent, {
        size: 'lg', centered: true, windowClass: 'carge-detail', backdrop: 'static', keyboard: false
      })
      modalRef.result.then((result) => { })
      modalRef.componentInstance.data = booking
      if (this.searchCriteria && this.searchCriteria.CONT)
        modalRef.componentInstance.containerCount = this.searchCriteria.CONT
    }
  }

  setWarehouseData() {
    const { booking } = this
    const _searchCriteria = JSON.parse(booking.JsonSearchCriteria)
    this.searchCriteria = _searchCriteria
    this.warehouseRate = {
      IsTransportAvailable: booking.IsTransportAvailable,
      videoURL: null,
      WHParsedMedia: null,
      WHID: null,
      WHName: booking.WHName,
      WHDesc: booking.WHDesc,
      CountryID: null,
      CityID: null,
      WHAddress: booking.WHAddress,
      UsageType: booking.UsageType ? booking.UsageType : _searchCriteria.storageType.toUpperCase(),
      TotalCoveredArea: null,
      TotalCoveredAreaUnit: getWHUnit(_searchCriteria),
      WHGallery: null,
      CountryCode: null,
      CountryName: null,
      CityName: _searchCriteria.CityName ? _searchCriteria.CityName.split(',')[0] : booking.WHCityName,
      CityShortName: null,
      IsBonded: booking.IsBondedWarehouse,
      EffectiveFrom: _searchCriteria.StoreFrom,
      EffectiveTo: _searchCriteria.StoreUntill,
      CurrencyCode: booking.CurrencyCode,
      Price: null,
      AddChrName: null,
      WHLatitude: null,
      WHLongitude: null,
      ContainerSpecID: null,
      StorageType: booking.StorageType,
      ContainerSpecDesc: _searchCriteria.searchBy === 'by_container' ? _searchCriteria.SearchCriteriaContainerDetail[0].contSize : null,
      CreditDays: null,
      ProviderImage: booking.ProviderImage,
      StorageTypeTitle: booking.StorageTypeTitle,
    }
  }

  discardSpotBooking(bookingId) {
    const modalRef = this.modalService.open(ConfirmDialogGenComponent, {
      size: 'lg', centered: true, windowClass: 'medium-modal', backdrop: 'static', keyboard: false
    })

    modalRef.componentInstance.modalData = {
      messageTitle: 'Discard Spot Rate',
      messageContent: 'Are you sure you want to discard this spot rate request?',
      buttonTitle: 'Yes, I want to discard',
      data: null
    }
    modalRef.result.then(result => {
      if (result) {
        const { booking } = this
        loading(true)
        this._bookingService.discardSpotRateRequest(booking.BookingID, getLoggedUserData().UserID, booking.ProviderID, 'PROVIDER').subscribe((res: JsonResponse) => {
          loading(false)
          if (res.returnId > 0)
            this.bookingRemove.emit(bookingId)
          else
            this._toastr.error(res.returnText, res.returnStatus)
        }, () => { })
      }
    })
  }

  async openSeaDialog() {
    const _additionalSeaCharges = await this.getAdditionalData(this._seaFreightService)
    const modalRef = this.modalService.open(SeaRateDialogComponent, {
      size: 'lg', centered: true, windowClass: 'large-modal', backdrop: 'static', keyboard: false
    });
    modalRef.result.then((result) => { });
    const object = {
      forType: 'FCL',
      data: null,
      addList: _additionalSeaCharges,
      mode: 'draft',
      customers: [],
      drafts: [],
    }
    modalRef.componentInstance.selectedData = object;
    modalRef.componentInstance.editorContent = null
  }


  uploadDocuments() {
    const modalRef = this.modalService.open(DocumentUploadComponent, {
      size: "lg", backdrop: "static", centered: true, windowClass: 'document-modal', keyboard: false
    });
    modalRef.result.then(result => { })
    modalRef.componentInstance.booking = this.booking
  }

  getVesselSchedule() {
    this.showSchedule = !this.showSchedule
    if (this.showSchedule) {
      loading(true)
      this._bookingService.getBookingRouteDetail(encryptBookingID(this.booking.BookingID, this.booking.ProviderID, this.booking.ShippingModeCode)).subscribe((res: JsonResponse) => {
        loading(false)
        if (res.returnId > 0 && typeof res.returnObject === 'object') {
          setTimeout(() => {
            this.showSchedule = true
            this.bookingSchedule = res.returnObject
          }, 900);
        } else {
          this.showSchedule = false
          this.bookingSchedule = null
        }
      }, () => this.showSchedule = false)
    }
  }


  cancelBooking() {
    const modalRef = this.modalService.open(BookingStatusUpdationComponent, {
      size: 'lg',
      windowClass: 'medium-modal',
      centered: true,
      backdrop: 'static',
      keyboard: false
    }
    );
    modalRef.result.then((res) => {
      if (res)
        this._sharedService.bookingsRefresh.next(true)
    });
    modalRef.componentInstance.modalData = {
      type: 'cancel',
      bookingID: this.booking.BookingID,
      bookingStatus: this.booking.BookingStatus,
      loginID: getLoggedUserData().UserID,
      providerID: this.booking.ProviderID,
      booking: this.booking,
      searchCarrierList: []
    }
  }

  openPaymentTerms(action) {
    loading(true)
    this._bookingService.getBookingPaymentTerms(this.booking.BookingID).subscribe((res: JsonResponse) => {
      loading(false)
      try {
        if (res.returnId > 0) {
          this.paymentTermsOptions = res.returnObject.filter(_term => _term.IsSelected)
          this.openTermsModal(action)
        } else {
          if (res.returnCode === '2') {
            this._toastr.info(res.returnText, res.returnStatus)
          } else {
            this._toastr.error(res.returnText, res.returnStatus)
          }
        }
      } catch { }
    }, error => { loading(false) })
  }

  openTermsModal(action) {
    const modalRef = this.modalService.open(PaymentTermsDialogComponent, {
      size: 'lg', centered: true, windowClass: 'termsAndCondition', backdrop: 'static', keyboard: false
    })
    modalRef.componentInstance.paymentTerms = []
    modalRef.componentInstance.paymentTermsOptions = this.paymentTermsOptions
    modalRef.componentInstance.action = action
    modalRef.componentInstance.from = 'RR'
    modalRef.componentInstance.shippingModeCode = this.booking.ShippingModeCode
    modalRef.componentInstance.request = this.booking
  }

  getQuoteNumber() {
    try {
      return this.booking.SpotRateBookingKey.split('|')[0]
    } catch { return '' }
  }

  toRequestList() {
    SharedService.rateRequestIdV2 = this.getQuoteNumber()
    this._router.navigate(['/provider/rate-requests'])
  }
}
