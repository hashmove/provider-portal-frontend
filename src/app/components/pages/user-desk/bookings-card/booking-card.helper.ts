import { baseExternalAssets } from "../../../../constants/base.url"
import { getLoggedUserData, loading } from "../../../../constants/globalFunctions"
import { ITermOption } from "../../../../shared/dialogues/payment-terms/payment-terms.interface"
import { IBookingSchedule } from "../../../../shared/dialogues/vessel-schedule-dialog/schedule.interface"
import { SeaFreightService } from "../manage-rates/sea-freight/sea-freight.service"

export class BookingCardHelper {

    public baseExternalAssets: string = baseExternalAssets
    public statusCode = {
        DRAFT: 'draft',
        APPROVED: 'approved',
        REJECTED: 'rejected',
        CANCELLED: 'cancelled',
        CONFIRMED: 'confirmed',
        IN_TRANSIT: 'in-transit',
        RE_UPLOAD: 're-upload',
        COMPLETED: 'completed',
        READY_TO_SHIP: 'ready to ship',
        IN_REVIEW: 'in-review',
        PENDING: 'pending',
        REQUESTED_AGAIN: 'requested_again',
        EXPIRED: 'expired'
    }
    isEnterprise = false
    hmCommisssion = 0
    isCommisionForAll = false
    showCommission = false
    priceSent = false
    totalCBM = 0
    showSchedule = false
    routesDirection = []
    bookingSchedule: IBookingSchedule
    public cachedLogs: any[] = []
    warehouseRate: any
    searchCriteria: any
    public isVirtualAirline = false
    paymentTermsOptions: ITermOption[] = []
    expiryDate: string = null
    bookDate: string = null
    showTimer = false

    constructor() { }

    private static SEA_CHARGES = []

    async getAdditionalData(_seaFreightService: SeaFreightService) {
        loading(true)
        const additionalCharges = (localStorage.hasOwnProperty('additionalCharges')) ? JSON.parse(localStorage.getItem('additionalCharges')) : null

        if (additionalCharges) {
            BookingCardHelper.SEA_CHARGES = additionalCharges.filter(e => e.modeOfTrans === 'SEA' && e.addChrType === 'ADCH')
        } else {
            try {
                const _res = await _seaFreightService.getAllAdditionalCharges(getLoggedUserData().ProviderID).toPromise() as any[]
                BookingCardHelper.SEA_CHARGES = _res.filter(e => e.modeOfTrans === 'SEA' && e.addChrType === 'ADCH')
            } catch { }
        }
        loading(false)
        return BookingCardHelper.SEA_CHARGES
    }

}

export interface BookingRouteMapInfo {
    route: string;
    transitTime: number;
    carrierName: string;
    carrierImage: string;
    freeTimeAtPort: number;
    routeInfo: any[];
    jsonTransferVesselsDetails: string;
}

export interface ISchedule {
    bookingID: number;
    portCutOffUtc?: any;
    portCutOffLcl?: any;
    etdUtc?: any;
    etaUtc?: any;
    etdLcl?: any;
    etaLcl?: any;
    transitTime: number;
    etaInDays?: any;
    transfersStop?: any;
    carrierID: number;
    carrierCode: string;
    carrierName: string;
    carrierImage: string;
    vesselCode: string;
    vesselName: string;
    voyageRefNum: string;
    bookingRouteMapInfo: BookingRouteMapInfo;
}
