import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleCardV2Component } from './schedule-card-v2.component';

describe('ScheduleCardV2Component', () => {
  let component: ScheduleCardV2Component;
  let fixture: ComponentFixture<ScheduleCardV2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduleCardV2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleCardV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
