import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewEncapsulation } from '@angular/core'
import { NgbModal, NgbPopoverConfig } from '@ng-bootstrap/ng-bootstrap'
import { getAnimatedGreyIcon, getImagePath, ImageRequiredSize, ImageSource, isMobile } from '../../../../constants/globalFunctions'
import { IRequestSchedule } from '../../../../shared/dialogues/request-add-rate-dialog/request-add-rate.interface'
import { ViaPortSeaComponent } from '../../../../shared/dialogues/via-port/via-port.component'

@Component({
  selector: 'app-schedule-card-v2',
  templateUrl: './schedule-card-v2.component.html',
  styleUrls: ['./schedule-card-v2.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [NgbPopoverConfig]
})
export class ScheduleCardV2Component implements OnInit, OnChanges {

  @Input() requestSchedule: IRequestSchedule = null
  routesDirection = []
  portCount = 0
  portConnectData: IRouteConnData[] = []
  isMobile = isMobile()
  
  constructor(
    config: NgbPopoverConfig,
    private _modalService: NgbModal
  ) { }

  ngOnInit(): void {
    this.setRouteDirections(this.requestSchedule.RouteAnimation)
    this.setConnectData()
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.bookingSchedule) {
      this.setConnectData()
    }
  }

  setRouteDirections(route) {
    const listOfRoutes = route.split('^')
    this.routesDirection = []
    const dummyRoutes = []
    const _route = []
    for (let i = 0; i < listOfRoutes.length; i++) {
      const childRoute = listOfRoutes[i]
      _route.push(childRoute.split('|'))
      dummyRoutes.push(childRoute.split('|'))
    }
    this.routesDirection = _route
    this.portCount = this.routesDirection.length
  }

  getCarrierImage = ($image) => getImagePath(ImageSource.FROM_SERVER, $image, ImageRequiredSize.original)

  getAnimatedImageByMode = ($transMode: string) => getAnimatedGreyIcon($transMode)

  setConnectData() {
    try {
      let PortCode = 'N/A', EtdDate = 'N/A', EtaDate = 'N/A', VesselName = 'N/A', VesselNo = 'N/A', VoyageRefNo = 'N/A'
      if (this.portCount && this.portCount > 2) {
        const { RouteDetails } = this.requestSchedule
        if (this.portCount >= 3) {
          PortCode = RouteDetails.R1PolCode
          VesselNo = RouteDetails.R1VesselNo
          VesselName = RouteDetails.R1VesselName
          VoyageRefNo = RouteDetails.R1VoyageRefNo
          EtdDate = RouteDetails.R1EtdDate ? new Date(RouteDetails.R1EtdDate) : null as any
          EtaDate = RouteDetails.R1EtaDate ? new Date(RouteDetails.R1EtaDate) : null as any
          this.portConnectData.push({
            PortCode,
            EtdDate,
            EtaDate,
            VesselName,
            VesselNo,
            VoyageRefNo,
            OriginPortName: RouteDetails.R1PolName,
            DestinPortName: RouteDetails.R1PodName,
            PodCode: RouteDetails.R1PodCode
          })

          PortCode = RouteDetails.R2PolCode
          VesselNo = RouteDetails.R2VesselNo
          VesselName = RouteDetails.R2VesselName
          VoyageRefNo = RouteDetails.R2VoyageRefNo
          EtdDate = RouteDetails.R2EtdDate ? new Date(RouteDetails.R2EtdDate) : null as any
          EtaDate = RouteDetails.R2EtaDate ? new Date(RouteDetails.R2EtaDate) : null as any
          this.portConnectData.push({
            PortCode,
            EtdDate,
            EtaDate,
            VesselName,
            VesselNo,
            VoyageRefNo,
            OriginPortName: RouteDetails.R2PolName,
            DestinPortName: RouteDetails.R2PodName,
            PodCode: RouteDetails.R2PodCode
          })
        }
        if (this.portCount === 4) {
          PortCode = RouteDetails.R3PolCode
          VesselNo = RouteDetails.R3VesselNo
          VesselName = RouteDetails.R3VesselName
          VoyageRefNo = RouteDetails.R3VoyageRefNo
          EtdDate = RouteDetails.R3EtdDate ? new Date(RouteDetails.R3EtdDate) : null as any
          EtaDate = RouteDetails.R3EtaDate ? new Date(RouteDetails.R3EtaDate) : null as any
          this.portConnectData.push({
            PortCode,
            EtdDate,
            EtaDate,
            VesselName,
            VesselNo,
            VoyageRefNo,
            OriginPortName: RouteDetails.R3PolName,
            DestinPortName: RouteDetails.R3PodName,
            PodCode: RouteDetails.R3PodCode
          })
        }
      }
      console.log(this.portConnectData)
    } catch { }
  }

  codeHasRoute(_code: string): boolean {
    try {
      return this.portConnectData.filter(_port => _port.PortCode.toLowerCase() === _code.toLowerCase()).length > 0 ? true : false
    } catch {
      return false
    }
  }

  getPortData = (_code: string) => this.portConnectData.filter(_port => _port.PortCode === _code)[0]

  openViaPort(portData) {
    const modalRef = this._modalService.open(ViaPortSeaComponent, {
      size: 'sm', centered: true, backdrop: 'static', keyboard: false
    })
    modalRef.componentInstance.portData = portData
    modalRef.componentInstance.portConnectData = this.portConnectData

  }
}

interface IRouteConnData {
  PortCode: string;
  PodCode: string
  EtdDate: string
  EtaDate: string
  VesselName: string
  VesselNo: string
  VoyageRefNo: string
  OriginPortName?: string
  DestinPortName?: string
}