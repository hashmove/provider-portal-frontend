import { Component, OnInit, Input } from '@angular/core'
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { PlatformLocation } from '@angular/common'
import { getBasisStr, removeDuplicates, getLoggedUserData } from '../../../../constants/globalFunctions'
import { JsonResponse } from '../../../../interfaces/JsonResponse'
import { HttpErrorResponse } from '@angular/common/http'
import { DashboardService } from '../dashboard/dashboard.service'
import { SharedService } from '../../../../services/shared.service'
import { ToastrService } from 'ngx-toastr'
import { CurrencyControl } from '../../../../services/currency.service'
import { CommonService } from '../../../../services/common.service'
import { ConfirmUpdateDialogComponent } from '../../../../shared/dialogues/confirm-update/confirm-update-dialog.component'
import { BookingSurChargeDetail } from '../../../../interfaces'

@Component({
  selector: 'app-booking-invoice',
  templateUrl: './booking-invoice.component.html',
  styleUrls: ['./booking-invoice.component.scss']
})
export class BookingInvoiceComponent implements OnInit {
  @Input() BookingInvoiceDet: any
  @Input() searchCriteria: any
  @Input() isSpecialRequest: boolean = false
  public vasTotal = 0
  public freightData
  public additionalData
  public valueAddedData
  public taxData
  public currencyCode
  public totalAmount: any = 0
  public discountedAmount: number = null
  public adjustedAmount: number = null
  public discountData: BookingSurChargeDetail = null
  public selectedCurrency: any = {}
  lclViewContainers: Array<any> = []
  public currencyList: any[] = []
  public actualIndividualPrice
  public specialRequestComments: string = ""
  actionType: string = 'add'
  adjustmentAmounts: Array<BookingSurChargeDetail> = null
  originAmount: number = 0
  showVAS = false

  isUpdatable: boolean = false
  lengthUnits
  volumeUnits
  loading = false

  constructor(
    private _activeModal: NgbActiveModal,
    private location: PlatformLocation,
    private _dashboardService: DashboardService,
    private _sharedService: SharedService,
    private _toast: ToastrService,
    private _currencyService: CurrencyControl,
    private _commonService: CommonService,
    private _modalService: NgbModal
  ) {
    location.onPopState(() => this.closeModal())
  }

  ngOnInit() {
    if ((this.BookingInvoiceDet.BookingStatus.toLowerCase() === 'in-review' || this.BookingInvoiceDet.BookingStatus.toLowerCase() === 'confirmed' || this.BookingInvoiceDet.ShippingStatus.toLowerCase() === 'in-transit'))
      this.isUpdatable = true

    this._sharedService.currencyList.subscribe((state: any) => {
      if (state) {
        this.currencyList = state
        this.selectedCurrency = this.currencyList.find(e => e.id === this.BookingInvoiceDet.CurrencyID)
      }
    })
    if (this.searchCriteria.searchMode === 'air-lcl' && this.searchCriteria.cargoLoadType === 'by_unit') {
      this.getWarehousingUnits()
    }
    if (this.searchCriteria.searchMode === 'sea-lcl') {
      this.lclViewContainers = this.prepareLclContainers()
      const { lclViewContainers } = this
      this.searchCriteria.lclViewContainers = lclViewContainers
      localStorage.setItem('searchCriteria', JSON.stringify(this.searchCriteria))
    }
    if (this.BookingInvoiceDet) {
      this.currencyCode = this.BookingInvoiceDet.CurrencyCode
      const billingData = (this.BookingInvoiceDet.ShippingModeCode == "SEA") ? this.BookingInvoiceDet.BookingPriceDetail.filter((e) => e.TransMode.toLowerCase() === 'read') : this.BookingInvoiceDet.BookingPriceDetail.filter((e) => e.TransMode.toLowerCase() === 'write')
      this.freightData = billingData.filter((element: any) => element.SurchargeType === 'FSUR')
      this.additionalData = billingData.filter((element: any) => element.SurchargeType === 'ADCH')
      this.valueAddedData = billingData.filter((element: any) => element.SurchargeCode === 'INSR' || element.SurchargeCode === 'TRCK' || element.SurchargeCode === 'QLTY')
      this.taxData = billingData.filter((element: any) => element.SurchargeType === 'TAX')
      this.freightData.forEach(element => { this.totalAmount += element.TotalAmount })
      this.additionalData.forEach(element => { this.totalAmount += element.TotalAmount })
      this.valueAddedData.forEach(element => {
        if (element !== 'INSR')
          this.totalAmount += element.TotalAmount
      })

      try {
        const { valueAddedData } = this
        if (valueAddedData && valueAddedData.length > 0) {
          const vasTotalArr = valueAddedData.map(_vas => _vas.TotalAmount)
          this.vasTotal = vasTotalArr.reduce((all, item) => all + item)
          if (!this.vasTotal) this.vasTotal = 0
        }
      } catch { }
      try {
        const discountObj: any = this.BookingInvoiceDet.BookingPriceDetail.filter((element) => element.SurchargeCode === 'SRDISC' && element.TransMode.toLowerCase() === 'write')[0]
        if (discountObj) {
          this.discountData = discountObj
          this.discountedAmount = discountObj.TotalAmount
        }
      } catch (error) {
        this.discountedAmount = 0
      }

      try {
        const _adjustment: Array<BookingSurChargeDetail> = this.BookingInvoiceDet.BookingPriceDetail.filter((element) => element.SurchargeCode === 'ADJMT' && element.TransMode.toLowerCase() === 'write')
        if (_adjustment && _adjustment.length > 0) {
          this.adjustmentAmounts = _adjustment
          this.adjustedAmount = _adjustment.reduce((a, b) => a + b['TotalAmount'], 0)
        }
      } catch (error) {
        this.adjustedAmount = 0
      }

      this.taxData.forEach(_tax => { this.totalAmount += _tax.TotalAmount })
      if (this.discountedAmount) this.totalAmount += this.discountedAmount
      if (this.adjustedAmount) this.totalAmount += this.adjustedAmount
      try { this.originAmount = parseFloat(this.totalAmount) } catch (error) { }
    }
  }

  getIndividualWeightPrice(amount) {
    try {
      return amount / this.searchCriteria.chargeableWeight
    } catch (error) { return 0 }
  }

  getLenghtUnit(id) {
    let _return = 'cm'
    try {
      const { lengthUnits } = this
      const data = lengthUnits.filter(unit => unit.UnitTypeID === id)[0]
      _return = data.UnitTypeShortName
    } catch (error) { }
    return _return
  }

  getVolumeUnit(id) {
    let _return = 'cm'
    try {
      const { volumeUnits } = this
      const data = volumeUnits.filter(unit => unit.UnitTypeID === id)[0]
      _return = data.UnitTypeShortName
    } catch (error) { }
    return _return
  }

  getWarehousingUnits() {
    if (!localStorage.getItem("units")) {
      this._dashboardService.getLCLUnits().subscribe((res: JsonResponse) => {
        const unitsResponse = res
        this.setWarehousingUnits(unitsResponse.returnObject)
        localStorage.setItem("units", JSON.stringify(unitsResponse.returnObject))
      })
    } else {
      this.setWarehousingUnits(JSON.parse(localStorage.getItem("units")))
    }
  }

  setWarehousingUnits(unitsResponse) {
    this.lengthUnits = unitsResponse.filter(_unit => _unit.UnitTypeNature === "LENGTH")
    this.volumeUnits = unitsResponse.filter(_unit => _unit.UnitTypeNature === "VOLUME")
  }

  prepareLclContainers() {
    const { SearchCriteriaContainerDetail, LclChips } = this.searchCriteria
    const _filteredContainers = removeDuplicates(SearchCriteriaContainerDetail, 'contSpecID')
    let finArr = []
    _filteredContainers.forEach(contaier => {
      const { contSpecID, containerCode, ContainerSpecDesc } = contaier
      const _filteredChips = LclChips.filter(chip => chip.contSpecID === contSpecID)
      finArr.push({
        contSpecID, containerCode, containerSpecDesc: ContainerSpecDesc, LclChips: _filteredChips
      })
    })
    return finArr
  }

  getBasisString = (type) => getBasisStr(type)

  closeModal() {
    this._activeModal.close()
    document.getElementsByTagName('html')[0].style.overflowY = 'auto'
  }

  numberValidwithDecimal(evt) {
    let charCode = (evt.which) ? evt.which : evt.keyCode
    if (evt.target.value && evt.target.value[evt.target.value.length - 1] == '.') {
      if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false
      return true
    }
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
      return false
    return true
  }

  async submitPrice() {
    const { originAmount } = this
    if (!this.actualIndividualPrice) {
      this._toast.warning('Price cannot be empty')
      return
    }
    if (!this.specialRequestComments) {
      this._toast.warning('Comments cannot be empty')
      return
    }
    if (this.actionType === 'subtract' && this.actualIndividualPrice >= originAmount) {
      this._toast.warning('The discounted amount must be less then the original amount')
      return
    }

    if (this.totalAmount < 1) {
      this._toast.warning('The Final amount is close to zero, please Enter a valid amount')
      return
    }

    this.loading = true
    const baseCurr = JSON.parse(localStorage.getItem('CURR_MASTER'))
    const res2: JsonResponse = await this._commonService.getExchangeRateList(baseCurr.fromCurrencyID).toPromise()
    const exchangeData = res2.returnObject
    const exchangeRate = exchangeData.rates.filter(rate => rate.currencyID === this.selectedCurrency.id)[0]

    let obj: any
    let newPrice = this.getNumber(this.actualIndividualPrice)
    let newBasePrice = this._currencyService.getPriceToBase((newPrice), true, exchangeRate.rate)
    let sign = '+'

    if (this.actionType === 'subtract') {
      newPrice = newPrice * (-1)
      newBasePrice = newBasePrice * (-1)
      sign = '-'
    }

    const { UserID } = getLoggedUserData()
    try {
      obj = {
        bookingID: this.BookingInvoiceDet.BookingID,
        currencyID: this.selectedCurrency.id,
        actualIndividualPrice: newPrice,
        baseIndividualPrice: this._currencyService.getPriceToBase((newPrice), true, exchangeRate.rate),
        exchangeRate: exchangeRate.rate,
        userID: UserID,
        requestStatus: 'PRICE_SENT',
        shippingModeCode: this.BookingInvoiceDet.ShippingModeCode,
        baseCurrencyID: this.BookingInvoiceDet.BaseCurrencyID,
        comments: this.specialRequestComments,
        loginUserID: UserID,
        bookingAmountStatus: (this.adjustedAmount) ? 'UPDATE' : "INSERT",
        priceFactor: sign,
        currExcgRateListID: exchangeRate ? exchangeRate.currExcgRateListID : null,
      }
    } catch (error) { }

    const modalRef = this._modalService.open(ConfirmUpdateDialogComponent, {
      size: 'lg', centered: true, windowClass: 'small-modal', backdrop: 'static', keyboard: false
    })
    modalRef.result.then((result) => {
      if (result)
        this._updatePrice(obj)
    })
    modalRef.componentInstance.amount = this.totalAmount
    modalRef.componentInstance.code = this.currencyCode
  }

  onAdjustChagne($event) {
    const { originAmount } = this
    if (this.actionType === 'subtract' && this.actualIndividualPrice >= originAmount) {
      this._toast.warning('The discounted amount must be less then the original amount')
      return
    }
    if ($event || $event === 0) {
      let finAmount = 0
      if (this.actionType === 'subtract') {
        finAmount = originAmount - this.actualIndividualPrice
      } else {
        finAmount = originAmount + this.actualIndividualPrice
      }
      this.totalAmount = finAmount
    } else {
      this.totalAmount = originAmount
    }
  }

  onOptionChange = ($event) => this.onAdjustChagne(this.actualIndividualPrice)

  getNumber($input) {
    let _num = 0
    try { _num = parseFloat($input) } catch (error) { }
    return _num
  }

  getContainerCount(baseCharge: any) {
    const { SearchCriteriaContainerDetail } = this.searchCriteria
    let _contQty = 0
    try {
      const _container = SearchCriteriaContainerDetail.filter(_cont => (baseCharge.contSpecID && _cont.contSpecID === baseCharge.contSpecID) || (baseCharge.ContainerSpecID && _cont.contSpecID === baseCharge.ContainerSpecID))[0]
      if (_container) {
        _contQty = _container.contRequestedQty
      }
    } catch { }
    return _contQty
  }

  getContainerName(baseCharge: any) {
    const { SearchCriteriaContainerDetail } = this.searchCriteria
    let _contStr = ''
    try {
      const _container = SearchCriteriaContainerDetail.filter(_cont => (baseCharge.contSpecID && _cont.contSpecID === baseCharge.contSpecID) || (baseCharge.ContainerSpecID && _cont.contSpecID === baseCharge.ContainerSpecID))[0]
      if (_container) _contStr = _container.contSize
    } catch { }
    return _contStr
  }

  getDaysMonthStr(basis: string, days: number) {
    let toReturn = days
    if (basis.toLowerCase().includes('month')) {
      toReturn = Math.ceil(days / 30)
    } else if (basis.toLowerCase().includes('year')) {
      toReturn = Math.ceil(days / 365)
    } else {
      toReturn = days
    }
    return toReturn
  }

  getWhRateType(basis: string, period) {
    let toReturn = 'day'
    try {
      if (basis.toLowerCase().includes('month')) {
        toReturn = period > 1 ? 'months' : 'month'
      } else if (basis.toLowerCase().includes('year')) {
        toReturn = period > 1 ? 'years' : 'year'
      } else {
        toReturn = period > 1 ? 'days' : 'day'
      }
    } catch (error) {

    }
    return toReturn
  }

  getWhUnit(baseCharge?) {
    let _retStr = ''
    try {
      if (this.searchCriteria.searchBy === 'by_container') {
        try {
          if (baseCharge) {
            const _container = this.searchCriteria.SearchCriteriaContainerDetail.filter(_cont => (baseCharge.contSpecID && _cont.contSpecID === baseCharge.contSpecID) || (baseCharge.ContainerSpecID && _cont.contSpecID === baseCharge.ContainerSpecID))[0]
            _retStr = (_container.contRequestedQty > 1) ? `${(_container.contRequestedQty)} Containers` : `${(_container.contRequestedQty)} Container`
          } else {
            _retStr = (this.searchCriteria.CONT > 1) ? `${(this.searchCriteria.CONT)} Containers` : `${(this.searchCriteria.CONT)} Container`
          }
        } catch (error) {
          _retStr = (this.searchCriteria.CONT > 1) ? `${(this.searchCriteria.CONT)} Containers` : `${(this.searchCriteria.CONT)} Container`
        }
      } else if (this.searchCriteria.searchBy === 'by_area') {
        _retStr = `${this.searchCriteria.AREA_UNIT === 'sqft' ? this.searchCriteria.SQFT : this.searchCriteria.AREA} ${this.searchCriteria.AREA_UNIT.toUpperCase()}`
      } else if (this.searchCriteria.searchBy === 'by_pallet') {
        _retStr = `${this.searchCriteria.PLT} PLT`
      } else {
        _retStr = `${this.searchCriteria.CBM} CBM`
      }
    } catch (error) {

    }
    return _retStr
  }

  getSimplifiedStr(str: string) {
    let _str = ''
    try { _str = str.replace('S', '') } catch (error) { }
    return _str
  }

  getWhSurchargeName(surchange: string) {
    let finString = surchange
    try {
      const scArr = surchange.split('/')
      finString = scArr[0] + '/' + scArr[2] + ' /' + scArr[1]
    } catch (error) { }
    return finString.toLowerCase()
  }

  getDaysCount(base: string) {
    let toRet = null
    let finn = 'day'
    try {
      if (base.toLowerCase().includes('month')) {
        toRet = 30
      } else if (base.toLowerCase().includes('year')) {
        toRet = 365
      }
      if (toRet) finn = `${toRet} days`
    } catch (error) { }
    return finn
  }

  _updatePrice(obj) {
    this._dashboardService.updateBookingAmount(this.BookingInvoiceDet.BookingID, obj).subscribe((res: JsonResponse) => {
      this.loading = false
      if (res.returnId > 0) {
        this._toast.success('Price updated successfully', 'Success')
        this._activeModal.close(true)
      } else {
        this._toast.error(res.returnText, 'Failed')
      }
    }, () => this.loading = false)
  }
}
