import { Component, OnInit, Input, ViewEncapsulation, Output, EventEmitter, OnChanges, SimpleChanges, OnDestroy, AfterViewInit } from '@angular/core'
import { removeDuplicates, loading, changeCase, after } from '../../../../constants/globalFunctions'
import { JsonResponse, IRequestStatus } from '../../../../interfaces'
import { NgbDateStruct, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap'
import { ViewBookingService } from '../view-booking/view-booking.service'
import { SharedService } from '../../../../services/shared.service'
import { untilDestroyed } from 'ngx-take-until-destroy'
import { Observable } from 'rxjs/Observable'
import { ToastrService } from 'ngx-toastr'
import { BookingsFilterHelper } from './bookings-filter.helper'
@Component({
  selector: 'app-bookings-filter-request',
  templateUrl: './bookings-filter-request.component.html',
  styleUrls: ['./bookings-filter-request.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class BookingsFilterRequestComponent extends BookingsFilterHelper implements OnInit, AfterViewInit, OnChanges, OnDestroy {

  @Output() onListChange = new EventEmitter<string>()
  @Input() currentTab = 'tab-active'
  @Input() currPage = 1
  @Input() requestStatusList: Array<IRequestStatus> = []
  @Output() tabChangeEvent = new EventEmitter<string>()

  constructor(
    private _parserFormatter: NgbDateParserFormatter,
    private _bookingService: ViewBookingService,
    private _toastr: ToastrService,
    private _dataService: SharedService
  ) {
    super()
  }

  async ngOnInit() {
    setTimeout(async () => {
      this.groundArray = changeCase(await this._bookingService.getPortsData('ground').toPromise() as any, 'camel')
    }, 0);
    loading(true)
    try {
      const _reqID = this._dataService.rateRequestId ? this._dataService.rateRequestId : SharedService.rateRequestIdV2 ? SharedService.rateRequestIdV2 : null
      if (_reqID) {
        this.hmBookingNum = _reqID
        this.filterExistingResult = true
        this.currentTab = 'tab-total'
        this.tabChangeEvent.emit('tab-total')
        this._dataService.rateRequestId = null
        SharedService.rateRequestIdV2 = null
      } else
        this.filterExistingResult = false
    } catch { }
    this._dataService.bookingsRefresh.pipe(untilDestroyed(this)).subscribe(state => {
      if (state) {
        try {
          localStorage.removeItem('book-filter')
        } catch { }
        this.filterRecords()
        this._dataService.bookingsRefresh.next(null)
      }
    })
    this.setInitDate()
    try {
      const _modes = await this._bookingService.getShipModes().toPromise() as Array<any>
      try {
        const res: JsonResponse = await this._bookingService.getLogisticServices(this.loginUser.ProviderID).toPromise() as any
        const _freightList = this.getFreightModes(res.returnObject)
        const _filteredModes = _modes.filter(freight => _freightList.includes(freight.code))
        this.shippingModes = _filteredModes
        if (_filteredModes.length === 1)
          this.selectedShippingModeCode = _filteredModes[0].code
      } catch {
        const _filtered = _modes.filter(mode => mode.code !== 'RAIL' && mode.code !== 'HYPERLOOP')
        this.shippingModes = _filtered
      }
    } catch { }
    try { await this.getBookingStatuses((this.selectedShippingModeCode) ? this.selectedShippingModeCode : 'SEA') } catch { }
    try { await this.getShippingStatus() } catch { }
    try { await this.setPortsData((this.selectedShippingModeCode) ? this.selectedShippingModeCode : 'SEA', false) } catch { }
    try {
      const _companies = await this._bookingService.getAllCompanies(this.loginUser.ProviderID).toPromise() as any
      this.companyList = _companies
    } catch { }
    try {
      const _carriers = await this._bookingService.getAllCarriers().toPromise() as any
      this.carriersList = _carriers
    } catch { }
    this.filterRecords()
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.carriersList && this.carriersList.length > 0 && changes.currPage && changes.currPage.currentValue) {
      if ((changes.currPage && changes.currPage.currentValue) || (this.carriersList && this.carriersList.length > 0))
        this.filterRecords()
    }
    if (this.carriersList && this.carriersList.length > 0 && changes.currentTab && changes.currentTab.currentValue)
      this.filterRecords()
  }

  ngAfterViewInit() { }

  clearFilter(event) {
    event.preventDefault()
    event.stopPropagation()
    this.currPage = 1
    this.resetFields()
    try { localStorage.removeItem('book-filter') } catch { }
    setTimeout(() => {
      try { this.calenderField.nativeElement.value = null }
      catch { }
    }, 0)
    this.filterRecords()
  }

  getFilteredPorts(term, transportMode) {
    const { ports } = this
    let toSend = []
    if (transportMode === 'AIR') {
      const _firstIteration: Array<any> = ports.filter(v => v.code.toLowerCase().indexOf(term.toLowerCase()) > -1)
      const _secondIteration: Array<any> = ports.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1)
      const _combinedArr = _firstIteration.concat(_secondIteration)
      toSend = removeDuplicates(_combinedArr, 'code')
    } else {
      if (this.groundArray && this.groundArray.length > 0) {
        toSend = ports.concat(this.groundArray).filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1)
      } else {
        toSend = ports.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1)
      }
    }
    return toSend
  }

  showbutton() {
    try { this.eventDate.toggle() } catch { }
  }

  onDateSelection(date: NgbDateStruct) {
    let parsed = ''
    if (!this.fromDate && !this.toDate)
      this.fromDate = date
    else if (this.fromDate && !this.toDate && after(date, this.fromDate))
      this.toDate = date
    else {
      this.toDate = null
      this.fromDate = date
    }
    if (this.fromDate)
      parsed += this._parserFormatter.format(this.fromDate)
    if (this.toDate)
      parsed += ' - ' + this._parserFormatter.format(this.toDate)
    setTimeout(() => {
      this.calenderField.nativeElement.value = parsed
      if (this.toDate)
        this.eventDate.toggle()
    }, 0)
  }

  async getBookingStatuses(selectedShippingModeCode) {
    const toSend = (selectedShippingModeCode === 'WAREHOUSE') ? 'WAREHOUSE' : 'BOOKING'
    try {
      const res = await this._bookingService.getBookingStatuses(toSend).toPromise() as JsonResponse
      if (res.returnId > 0)
        this.bookingStatuses = res.returnObject
          .filter(status => status.BusinessLogic.toUpperCase() !== 'IN-TRANSIT' && status.BusinessLogic.toUpperCase() !== 'COMPLETED')
    } catch { }
  }

  async getShippingStatus() {
    try {
      const res = await this._bookingService.getBookingStatuses('SHIPMENT').toPromise() as JsonResponse
      if (res.returnId > 0) {
        this.shippingStatuses = res.returnObject
        this.shippingStatuses.push({ BusinessLogic: 'PENDING', StatusCode: 'PENDING', StatusID: -1, StatusName: 'Pending', })
      }
    } catch { }
  }

  async setPortsData(selectedShippingModeCode, isLoading: boolean) {
    const _ports: Array<any> = (localStorage.getItem('shippingPortDetails')) ? JSON.parse(localStorage.getItem('shippingPortDetails')) : []
    const ports_to_get = (selectedShippingModeCode.toLowerCase() === 'truck') ? 'sea' : selectedShippingModeCode.toLowerCase()
    const filteredPorts: Array<any> = _ports.filter(port => port.type.toLowerCase().includes(ports_to_get))
    const hasMode = filteredPorts.length > 0 ? true : false
    if (hasMode) {
      this.ports = filteredPorts
    } else {
      try {
        if (isLoading) loading(true)
        const res = await this._bookingService.getPortsData(ports_to_get.toUpperCase()).toPromise()
        if (isLoading) loading(false)
        const newPorts = _ports.concat(res)
        this.ports = newPorts.filter(port => port.type.toLowerCase().includes(ports_to_get))
        localStorage.setItem('shippingPortDetails', JSON.stringify(newPorts))
      } catch {
        if (isLoading) loading(false)
      }
    }
  }

  filterRecords() {
    loading(true)
    const saveObject = this.getSaveObject(this.currentTab, this.currPage)
    this._bookingService.getSpecialBookingsV2(saveObject).subscribe((res: JsonResponse) => {
      this.onBookingResponse(res)
    }, () => {
      loading(false)
      this._toastr.error('Please try again laters', 'Failed')
    })
  }

  onBookingResponse(res: JsonResponse) {
    loading(false)
    const { returnObject } = res
    if (res.returnId > 0)
      this.onListChange.emit({ ...returnObject, pageNumber: this.currPage })
    else
      this._toastr.error(res.returnText, 'Failed')
  }

  onSortChange($sortBy: string) {
    this.sortBy = $sortBy
    this.filterRecords()
  }

  onModeChange($mode: string) {
    const { selectedShippingModeCode } = this
    if (selectedShippingModeCode !== 'WAREHOUSE' || selectedShippingModeCode !== 'HYPERLOOP') {
      this.filterOrigin = {}
      this.filterDestination = {}
      this.blNumber = null
    }
    this.setPortsData($mode, true)
    this.resetFreightData()
    this.getBookingStatuses($mode)
  }

  portsFilter = (text$: Observable<string>) => (text$.debounceTime(200).map(term =>
    !term || term.length < 3 ? [] : this.getFilteredPorts(term, this.selectedShippingModeCode)))

  ngOnDestroy() { }

}
