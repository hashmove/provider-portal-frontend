import { ViewChild } from "@angular/core"
import { after, before, equals, getImagePath, getLoggedUserData, getProviderImage, ImageRequiredSize, ImageSource, isMobile } from "../../../../constants/globalFunctions"
import { BookingStatus, FilterdBookings, ICarrierFilter, ICompanyFilter, IProviderFilter, IRequestStatus } from "../../../../interfaces"
import { debounceTime } from 'rxjs/operators/debounceTime'
import { Observable } from 'rxjs/Observable'
import { map } from 'rxjs/operators/map'
import * as moment from 'moment'

export class BookingsFilterHelper {

    @ViewChild('d') eventDate
    @ViewChild('calenderField') calenderField: any
    public hmBookingNum = ''
    public selectedBookStatusCode = null
    public selectedShipStatusCode = null
    public selectedCargoType = null
    public selectedRequestStatus: IRequestStatus = null
    public cargoTypeList = ['FCL', 'LCL']
    public selectedShippingModeCode = null
    public isMarketplace = false
    public filterbyContainerType
    public fromDate
    public toDate
    public filterOrigin: any = {}
    public filterDestination: any = {}
    public bookingStatuses: Array<BookingStatus> = []
    public shippingStatuses: Array<BookingStatus> = []
    public selectedCarrier: ICarrierFilter
    public selectedProvider: IProviderFilter
    public selectedCompany: ICompanyFilter
    public allShippingLines = []
    public carriersList: Array<ICarrierFilter> = []
    public providersList: Array<IProviderFilter> = []
    public bookingList: Array<FilterdBookings> = []
    public companyList: Array<ICompanyFilter> = []
    public shippingModes: Array<ICarrierFilter> = []
    public sortBy = 'date' // or price
    public minDate: any
    public maxDate: any
    public blNumber = ''
    hoveredDate: any
    ports = []
    loginUser = getLoggedUserData()
    isVirtual = false
    groundArray = []
    isInit = false
    filterExistingResult: any
    isMobile = isMobile()

    constructor() { }

    setInitDate() {
        const date = new Date()
        this.minDate = { month: date.getMonth() + 1, day: date.getDate(), year: date.getFullYear() }
        this.maxDate = {
            year: ((this.minDate.month === 12) ? date.getFullYear() + 1 : date.getFullYear()),
            month: moment(date).add(30, 'days').month() + 1,
            day: moment(date).add(30, 'days').date()
        }
    }

    getFreightModes(_services: any[]) {
        const logisticServices = _services
        const _freightList: Array<string> = []
        logisticServices.forEach(servie => {
            const { LogServCode } = servie
            if (LogServCode.toUpperCase() === 'SEA_FFDR')
                _freightList.push('SEA')
            else if (LogServCode.toUpperCase() === 'AIR_FFDR')
                _freightList.push('AIR')
            else if (LogServCode.toUpperCase() === 'TRUK')
                _freightList.push('TRUCK')
            else if (LogServCode.toUpperCase() === 'WRHS')
                _freightList.push('WAREHOUSE')
            else if (LogServCode.toUpperCase() === 'ASYER' || LogServCode.toUpperCase() === 'ASSAYER')
                _freightList.push('ASSAYER')
        })
        return _freightList
    }

    resetFields() {
        this.resetFreightData()
        this.fromDate = {}
        this.toDate = {}
        this.isMarketplace = false
        this.hmBookingNum = null
        this.selectedCarrier = {} as any
        this.selectedShippingModeCode = null
        this.selectedProvider = {} as any
        this.selectedCompany = {} as any
        this.sortBy = 'date'
        this.blNumber = null
        this.selectedRequestStatus = null
    }

    resetFreightData() {
        this.selectedShipStatusCode = null
        this.selectedBookStatusCode = null
        this.selectedCargoType = null
        this.filterOrigin = {} as any
        this.filterDestination = {} as any
        this.selectedCarrier = {} as any
    }

    getSaveObject(currentTab, currPage) {
        let _fromDate, _toDate
        try {
            _fromDate = this.fromDate.day ? this.getDatefromObj(this.fromDate) : null
        } catch { }
        try {
            _toDate = this.toDate.day ? this.getDatefromObj(this.toDate) : null
        } catch { }

        if (_fromDate && !_toDate) _toDate = _fromDate
        let tabToSend
        switch (currentTab) {
            case 'tab-active':
                tabToSend = 'ACTIVE'
                break
            case 'tab-total':
                tabToSend = 'ALL'
                break
            default:
                tabToSend = 'ACTIVE'
                break
        }
        let _selectedShippingModeId: any
        try {
            if (this.shippingModes.length > 0) {
                const { selectedShippingModeCode } = this
                _selectedShippingModeId = this.shippingModes.filter(mode => mode.code.toLowerCase() === selectedShippingModeCode.toLowerCase())[0].id
            }
        } catch { }
        return {
            userID: this.loginUser.UserID,
            providerID: this.loginUser.ProviderID,
            requestNumber: (this.hmBookingNum) ? this.hmBookingNum.replace(/\s/g, '') : null,
            requestStatusCode: (this.selectedRequestStatus && this.selectedRequestStatus !== 'null') ? this.selectedRequestStatus : null,
            polID: (this.filterOrigin && this.filterOrigin.id) ? this.filterOrigin.id : 0,
            polCode: (this.filterOrigin && this.filterOrigin.code) ? this.filterOrigin.code : null,
            polType: (this.filterOrigin && this.filterOrigin.code && this.selectedShippingModeCode && this.selectedShippingModeCode !== 'null') ? this.filterOrigin.type.toUpperCase() : null,
            podID: (this.filterDestination && this.filterDestination.id) ? this.filterDestination.id : 0,
            podCode: (this.filterDestination && this.filterDestination.code) ? this.filterDestination.code : null,
            podType: (this.filterDestination && this.filterDestination.code && this.selectedShippingModeCode && this.selectedShippingModeCode !== 'null') ? this.filterDestination.type.toUpperCase() : null,
            cargoType: (this.selectedCargoType && this.selectedCargoType !== 'null') ? this.selectedCargoType : null,
            shippingModeID: (_selectedShippingModeId) ? _selectedShippingModeId : 0,
            fromDate: (_fromDate) ? _fromDate : null,
            toDate: (_toDate) ? _toDate : null,
            filterProviderID: 0,
            filterCompanyID: (this.selectedCompany && this.selectedCompany.ID) ? this.selectedCompany.ID : 0,
            carrierID: (this.selectedCarrier && this.selectedCarrier.id) ? this.selectedCarrier.id : 0,
            sortBy: this.sortBy,
            sortDirection: 'DESC',
            pageSize: 10,
            pageNumber: currPage,
            blNumber: (this.blNumber) ? this.blNumber : null,
            tab: tabToSend,
        }
    }

    getDatefromObj(dateObject) {
        let toSend = null
        try {
            toSend = moment(new Date(dateObject.year, dateObject.month - 1, dateObject.day)).format('YYYY-MM-DD')
        } catch (error) { }
        return toSend
    }

    isEmpty = (data) => !data.length;
    getCarrierImage = ($image: string) => getImagePath(ImageSource.FROM_SERVER, $image, ImageRequiredSize.original)
    getProviderImage = ($image) => getImagePath(ImageSource.FROM_SERVER, getProviderImage($image), ImageRequiredSize.original)
    isHovered = date => this.fromDate && !this.toDate && this.hoveredDate && after(date, this.fromDate) && before(date, this.hoveredDate)
    isInside = date => after(date, this.fromDate) && before(date, this.toDate)
    isFrom = date => equals(date, this.fromDate)
    isTo = date => equals(date, this.toDate)

    filterPortsFormatter = (x) => x.title
    filterCarrierFormatter = (x) => x.title
    filtercompanyFormatter = (x) => x.Title
    shippingFormatter = (x: { title: string }) => x.title

    carrierFilter = (text$: Observable<string>) =>
        text$.pipe(debounceTime(200), map(term => (!term || term.length < 2) ? []
            : this.carriersList.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1)))

    companyFilter = (text$: Observable<string>) => text$.pipe(debounceTime(200), map(term => (!term || term.length < 2)
        ? [] : this.companyList.filter(v => v.Title.toLowerCase().indexOf(term.toLowerCase()) > -1)))

    shippings = (text$: Observable<string>) =>
        text$.pipe(debounceTime(200), map(term => (!term || term.length < 3) ? []
            : this.ports.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1 ||
                v.shortName.toLowerCase().indexOf(term.toLowerCase()) > -1 || v.code.toLowerCase().indexOf(term.toLowerCase()) > -1)))
}
