import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewEncapsulation } from "@angular/core"
import { NgbModal, NgbPopoverConfig } from "@ng-bootstrap/ng-bootstrap"
import { getAnimatedGreyIcon, getImagePath, ImageRequiredSize, ImageSource, isMobile } from "../../../../constants/globalFunctions"
import { IBookingSchedule, } from "../../../../shared/dialogues/vessel-schedule-dialog/schedule.interface"
import { ViaPortSeaComponent } from "../../../../shared/dialogues/via-port/via-port.component"
@Component({
  selector: 'app-schedule-card',
  templateUrl: './schedule-card.component.html',
  styleUrls: ['./schedule-card.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [NgbPopoverConfig]
})
export class ScheduleCardComponent implements OnInit, OnChanges {

  @Input() bookingSchedule: IBookingSchedule = null
  routesDirection = []
  portCount: number = 0
  portConnectData: IRouteConnData[] = []

  isMobile = isMobile()

  constructor(
    config: NgbPopoverConfig,
    private _modalService: NgbModal
  ) { }

  ngOnInit(): void {
    this.setRouteDirections(this.bookingSchedule.bookingRouteMapInfo.route)
    this.setConnectData()
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.bookingSchedule) {
      this.setConnectData()
    }
  }

  setRouteDirections(route) {
    let listOfRoutes = route.split("^")
    this.routesDirection = []
    const dummyRoutes = []
    const _route = []
    for (let i = 0; i < listOfRoutes.length; i++) {
      let childRoute = listOfRoutes[i]
      _route.push(childRoute.split("|"))
      dummyRoutes.push(childRoute.split("|"))
    }
    this.routesDirection = _route
    this.portCount = this.routesDirection.length
  }

  getCarrierImage = ($image) => getImagePath(ImageSource.FROM_SERVER, $image, ImageRequiredSize.original)

  getAnimatedImageByMode = ($transMode: string) => getAnimatedGreyIcon($transMode)

  setConnectData() {
    try {
      let PortCode = 'N/A', EtdDate = 'N/A', EtaDate = 'N/A', VesselName = 'N/A', VesselNo = 'N/A', VoyageRefNo = 'N/A'
      if (this.portCount && this.portCount > 2) {
        const _transferDtl: any = JSON.parse(this.bookingSchedule.bookingRouteMapInfo.jsonTransferVesselsDetails)
        if (this.portCount >= 3) {
          PortCode = _transferDtl.R1PolCode
          VesselNo = _transferDtl.R1VesselNo
          VesselName = _transferDtl.R1VesselName
          VoyageRefNo = _transferDtl.R1VoyageRefNo
          EtdDate = _transferDtl.R1EtdDate ? new Date(_transferDtl.R1EtdDate) : null as any
          EtaDate = _transferDtl.R1EtaDate ? new Date(_transferDtl.R1EtaDate) : null as any
          this.portConnectData.push({ PortCode, EtdDate, EtaDate, VesselName, VesselNo, VoyageRefNo, OriginPortName: this.getPortName(_transferDtl.R1PolCode), DestinPortName: this.getPortName(_transferDtl.R1PodCode), PodCode: _transferDtl.R1PodCode })

          PortCode = _transferDtl.R2PolCode
          VesselNo = _transferDtl.R2VesselNo
          VesselName = _transferDtl.R2VesselName
          VoyageRefNo = _transferDtl.R2VoyageRefNo
          EtdDate = _transferDtl.R2EtdDate ? new Date(_transferDtl.R2EtdDate) : null as any
          EtaDate = _transferDtl.R2EtaDate ? new Date(_transferDtl.R2EtaDate) : null as any
          this.portConnectData.push({ PortCode, EtdDate, EtaDate, VesselName, VesselNo, VoyageRefNo, OriginPortName: this.getPortName(_transferDtl.R2PolCode), DestinPortName: this.getPortName(_transferDtl.R2PodCode), PodCode: _transferDtl.R2PodCode })
        }
        if (this.portCount === 4) {
          PortCode = _transferDtl.R3PolCode
          VesselNo = _transferDtl.R3VesselNo
          VesselName = _transferDtl.R3VesselName
          VoyageRefNo = _transferDtl.R3VoyageRefNo
          EtdDate = _transferDtl.R3EtdDate ? new Date(_transferDtl.R3EtdDate) : null as any
          EtaDate = _transferDtl.R3EtaDate ? new Date(_transferDtl.R3EtaDate) : null as any
          this.portConnectData.push({ PortCode, EtdDate, EtaDate, VesselName, VesselNo, VoyageRefNo, OriginPortName: this.getPortName(_transferDtl.R3PolCode), DestinPortName: this.getPortName(_transferDtl.R3PodCode), PodCode: _transferDtl.R3PodCode })
        }
      }
      // console.log(this.portConnectData)
    } catch { }
  }

  getPortName(_portCode: string) {
    try {
      const { routeInfo } = this.bookingSchedule.bookingRouteMapInfo
      return routeInfo.filter(_route => _route.portCode === _portCode)[0].portName.replace(_portCode + ', ', '')
    } catch (error) {
      return _portCode ? _portCode : 'N/A'
    }
  }

  codeHasRoute(_code: string): boolean {
    try {
      return this.portConnectData.filter(_port => _port.PortCode.toLowerCase() === _code.toLowerCase()).length > 0 ? true : false
    } catch {
      return false
    }
  }

  getPortData(_code: string): IRouteConnData {
    return this.portConnectData.filter(_port => _port.PortCode === _code)[0]
  }

  openViaPort(portData) {
    const modalRef = this._modalService.open(ViaPortSeaComponent, {
      size: 'sm', centered: true, backdrop: 'static', keyboard: false
    })
    modalRef.componentInstance.portData = portData
    modalRef.componentInstance.portConnectData = this.portConnectData
  }

}

interface IRouteConnData {
  PortCode: string;
  PodCode: string
  EtdDate: string
  EtaDate: string
  VesselName: string
  VesselNo: string
  VoyageRefNo: string
  OriginPortName?: string
  DestinPortName?: string
}