export interface IPortalUser {
    userID: number
    firstName: string
    lastName: string
    loginID: string
    emailAddress: string
    countryPhoneCode: string
    phoneNumber: string
    userImage: string
    userStatus: string
    isAdmin: boolean
    isCorporateUser: boolean
    isVerified: boolean
    regDate: string
    countryID: number
    countryName: string
    cityName: string
    companyName?: string
    companyID?: number
    countryCode: string
    lastLoginDate?: string
    lastLoginCountryID?: number
    lastLoginCountryName: string
    lastLoginIP: string
    totalBookings?: number
    sourceID: number
    sourceType: string
}

export interface IUsersRights {
    USERS_ADMIN_SECTION: number
    USERS_USER_SECTION: number
}
