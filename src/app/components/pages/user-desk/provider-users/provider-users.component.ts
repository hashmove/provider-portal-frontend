import { Component, OnInit } from '@angular/core';
import { firstBy } from 'thenby';
import { PaginationInstance } from 'ngx-pagination';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { baseExternalAssets } from '../../../../constants/base.url';
import { compareValues } from '../billing/billing.component';
import { loading, getLoggedUserData, getAccessRights } from '../../../../constants/globalFunctions';
import { SharedService } from '../../../../services/shared.service';
import { UserCreationService } from '../../user-creation/user-creation.service';
import { UserRegDialogComponent } from '../../../../shared/dialogues/user-reg-dialog/user-reg-dialog.component';
import { JsonResponse } from '../../../../interfaces/JsonResponse';
import { HttpErrorResponse } from '@angular/common/http';
import { ConfirmDialogGenComponent } from '../../../../shared/dialogues/confirm-dialog-generic/confirm-dialog-generic.component';
import { CommonService } from '../../../../services/common.service';
import { IUsersRights } from '../../../../interfaces/access-rights.interface';
import { IPortalUser } from './provider-users.interface';

@Component({
  selector: "app-provider-users",
  templateUrl: "./provider-users.component.html",
  styleUrls: ["./provider-users.component.scss"]
})
export class ProviderUsersComponent implements OnInit {
  public userObj: any = {};
  public dashboardData: any = {};
  public response: any;
  public userList: any;
  public users: any;
  public searchUser: any;
  public hasDeleteRights: boolean = true;
  public totalUsers: number = 0;
  public adminUsers = [];
  public nonAdminUsers = [];
  public showCompanyInfo: boolean = true;

  //Pagination work
  public maxSize: number = 7;
  public directionLinks: boolean = true;
  public autoHide: boolean = false;
  public userPaginationConfig: PaginationInstance = {
    id: "users",
    itemsPerPage: 10,
    currentPage: 1
  };

  public labels: any = {
    previousLabel: "",
    nextLabel: ""
  };
  public currentSort: string = "name";
  accessRights: IUsersRights

  constructor(
    private _userService: UserCreationService,
    private _sharedService: SharedService,
    private _commonService: CommonService,
    private _toast: ToastrService,
    private _modalService: NgbModal
  ) { }

  ngOnInit() {
    this.userObj = getLoggedUserData()
    this.getActionRights()
    if (
      !this.userObj.IsAdmin &&
      this.userObj.IsCorporateUser &&
      this.userObj.IsVerified
    ) {
      this.hasDeleteRights = false;
    } else {
      this.hasDeleteRights = true;
    }
    this.getAllUsers(this.userObj.UserID);
    this._sharedService.dashboardDetail.subscribe(data => {
      if (data !== null) {
        this.dashboardData = data;
      }
    });
  }

  getActionRights() {
    const accRights = getAccessRights()
    this._commonService.getRoleActions(accRights.RoleID, 'USERS', 'PROVIDER', 'MENU').subscribe((res: JsonResponse) => {
      this.accessRights = (res.returnId > 0) ? res.returnObject : null
    })
  }

  public superAdmin: any;

  getAllUsers(userId) {
    this._userService.getAllUsers(userId).subscribe((res: JsonResponse) => {
      const { returnId, returnStatus } = res
      if (returnStatus.toLowerCase() === 'success') {
        this.response = res;
        const userList = this.response.returnObject;
        try {
          userList.forEach(_user => {
            const { firstName, lastName, countryName, emailAddress, phoneNumber } = _user
            _user.searchField = firstName + ' ' + lastName + ' ' + countryName + ' ' + emailAddress + ' ' + phoneNumber
          })
        } catch { }

        this.userList = userList
        // console.log(this.userList);

        // this._dataService.setUsersCount(this.userList);
        this.totalUsers = this.userList.length;
        this.userList.forEach(element => {
          if (element.userImage) {
            element.userImage = baseExternalAssets + element.userImage;
          }
        });
        this.nonAdminUsers = this.userList.filter(e => !e.isAdmin);
        this.adminUsers = this.userList.filter(e => e.isAdmin);
        this.userList = this.userList.sort(
          compareValues("firstName", "asc")
        );
        this.nonAdminUsers = this.nonAdminUsers.sort(
          compareValues("firstName", "asc")
        );
        this.superAdmin = this.adminUsers.sort(compareValues("regDate", "asc"));
        if (this.userObj.UserID === this.superAdmin[0].userID)
          this.showCompanyInfo = false;
      }

    });
  }

  openRegister(_user: IPortalUser) {
    const modalRef = this._modalService.open(UserRegDialogComponent, {
      size: "lg", backdrop: "static", keyboard: false
    });

    let obj = {
      ..._user,
      companyName: this.dashboardData.CompanyName,
      companyID: this.dashboardData.CompanyID
    };

    modalRef.componentInstance.shareUserObject = obj;
    modalRef.componentInstance.mode = _user ? 'edit' : 'add'
    modalRef.result.then(result => {
      if (result === "success")
        this.getAllUsers(obj.userID);
    });
    setTimeout(() => {
      if (
        document
          .getElementsByTagName("body")[0]
          .classList.contains("modal-open")
      ) {
        document.getElementsByTagName("html")[0].style.overflowY = "hidden";
      }
    }, 0);
  }

  deleteUserx(user) {
    // const modalRef = this._modalService.open(ConfirmDeleteAccountComponent, {
    const modalRef = this._modalService.open(null, {
      size: "lg",
      centered: true,
      windowClass: "small-modal",
      backdrop: "static",
      keyboard: false
    });
    let obj = {
      deletingUserID: user.UserID,
      deleteByUserID: this.userObj.UserID,
      type: "user"
    };
    modalRef.componentInstance.account = obj;

    setTimeout(() => {
      if (
        document
          .getElementsByTagName("body")[0]
          .classList.contains("modal-open")
      ) {
        document.getElementsByTagName("html")[0].style.overflowY = "hidden";
      }
    }, 0);
  }

  onPageChange(number: any) {
    this.userPaginationConfig.currentPage = number;
  }

  public resp: any;

  makeAdmin(adminID, userID) {
    if (this.adminUsers.length === 5) {
      this._toast.warning('5 admin limit exceeded,\n You have to remove an admin in case to add another', 'Warning')
      return;
    }
    loading(true);
    this._userService.makeAdmin(adminID, userID).subscribe(
      (res: Response) => {
        this.resp = res;
        const userList = this.resp.returnObject;
        // this._dataService.setUsersCount(this.userList);
        try {
          userList.forEach(_user => {
            const { firstName, lastName, countryName, emailAddress, phoneNumber } = _user
            _user.searchField = firstName + ' ' + lastName + ' ' + countryName + ' ' + emailAddress + ' ' + phoneNumber
          })
          // console.log(userList)
        } catch (error) {
          console.log(error)
        }

        this.userList = userList
        this.totalUsers = this.userList.length;
        this.userList.forEach(element => {
          if (element.userImage) {
            element.userImage = baseExternalAssets + element.userImage;
          }
        });
        this.nonAdminUsers = this.userList.filter(e => !e.isAdmin);
        this.adminUsers = this.userList.filter(e => e.isAdmin);
        this.nonAdminUsers = this.nonAdminUsers.sort(
          compareValues("firstName", "asc")
        );
        loading(false);
        this._toast.success(this.resp.returnText, this.resp.returnStatus);
      },
      (err: any) => {
      }
    );
  }

  public removeResp: any;
  removeAdmin(adminID, userID) {
    loading(true);
    this._userService.removeAdmin(adminID, userID).subscribe(
      (res: Response) => {
        this.removeResp = res;
        this.userList = this.removeResp.returnObject;
        // this._dataService.setUsersCount(this.userList);
        this.totalUsers = this.userList.length;
        this.userList.forEach(element => {
          if (element.userImage) {
            element.userImage = baseExternalAssets + element.userImage;
          }
        });
        this.nonAdminUsers = this.userList.filter(e => !e.isAdmin);
        this.adminUsers = this.userList.filter(e => e.isAdmin);
        this.nonAdminUsers = this.nonAdminUsers.sort(
          compareValues("firstName", "asc")
        );
        loading(false);
        this._toast.success(
          this.removeResp.returnText,
          this.removeResp.returnStatus
        );
      },
      (err: any) => {
      }
    );
  }

  sortUsers(type, value) {
    this.currentSort = value;
    const { userList } = this
    let newFilt = userList.sort(firstBy(type));
    this.userList = newFilt
  }

  deleteUser(user: IUser, type: string) {
    const _modalData = {
      messageTitle: "Delete Account",
      messageContent: "Are you sure you want to delete the account?",
      buttonTitle: "Yes, I want to delete the account",
      data: null
    }

    const modalRef = this._modalService.open(ConfirmDialogGenComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'medium-modal',
      backdrop: 'static',
      keyboard: false
    });

    modalRef.componentInstance.modalData = _modalData;

    modalRef.result.then(result => {
      if (result) {
        const loggedUser = getLoggedUserData()
        loading(true)
        this._userService.userDelete(user.userID, loggedUser.UserID).subscribe((res: any) => {
          loading(false)
          if (res.returnStatus == "Error") {
            this._toast.error(res.returnText);
          }
          else if (res.returnStatus == "Success") {
            if (type === 'user') {
              this._toast.info("Your request to delete your account has been submitted to the HashMove Support Team.", '');
            } else {
              this._toast.info("Your request to delete your account has been submitted to the HashMove Support Team.", '');
            }
            this.getAllUsers(loggedUser.UserID);
          }
        }, (err: HttpErrorResponse) => {
          loading(false)
        })
      }
    });
  }
}


export interface IUser {
  userID: number;
  firstName: string;
  lastName: string;
  loginID: string;
  emailAddress: string;
  countryPhoneCode: string;
  phoneNumber: string;
  userImage: string;
  userStatus: string;
  isAdmin: boolean;
  isCorporateUser: boolean;
  isVerified: boolean;
  regDate: string;
  countryID: number;
  countryName: string;
  cityName: string;
  countryCode: string;
  lastLoginDate?: string;
  lastLoginCountryID?: number;
  lastLoginCountryName: string;
  lastLoginIP: string;
  totalBookings?: number;
  sourceID: number;
  sourceType: string;
}