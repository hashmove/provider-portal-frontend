import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core'
import { baseExternalAssets, basesPartnerUrl } from '../../../../constants/base.url'
import { encryptBookingID, getLoggedUserData, isMobile } from '../../../../constants/globalFunctions'
import { SharedService } from '../../../../services/shared.service'
import { untilDestroyed } from 'ngx-take-until-destroy'
import { PaginationInstance } from 'ngx-pagination'
import { BookingsHelper } from './bookings.helper'
import { Router } from '@angular/router'
import { FilterdBookings } from '../../../../interfaces'

@Component({
  selector: 'app-all-bookings',
  templateUrl: './all-bookings.component.html',
  styleUrls: ['./all-bookings.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AllBookingsComponent extends BookingsHelper implements OnInit, OnDestroy {

  public pastBookings: any[] = []
  public specialBookings: any[] = []
  public currentBookings: any[] = []
  public totalBookings: any[] = []
  public maximumSize: number = 10
  public directionLinks: boolean = true
  public responsive: boolean = true
  public autoHide: boolean = false
  public baseExternalAssets: string = baseExternalAssets
  public paginationConfig: PaginationInstance = {
    itemsPerPage: 10, currentPage: 1
  }
  userProfile: any

  //bootstrap paginate
  public data: Array<any> = []
  public maxPageSize: number = 10
  public currentPage: number = 1
  public totalPages: number = 1
  newBookingUrl: string
  isMobile = isMobile()

  constructor(
    private _sharedService: SharedService,
    private _router: Router
  ) {
    super()
  }

  ngOnInit() {
    this.setPageConfig()
    this._sharedService._profile_id.pipe(untilDestroyed(this)).subscribe(state => {
      if (state) {
        this.newBookingUrl = basesPartnerUrl + 'partner/' + state
      }
    })

    this.userProfile = getLoggedUserData()
    try {
    } catch (error) { }
  }

  filterByDate(bookings) {
    return bookings.sort(function (a, b) {
      let dateA: any = new Date(a.HashMoveBookingDate)
      let dateB: any = new Date(b.HashMoveBookingDate)
      return dateB - dateA
    })
  }

  viewBookingDetails(bookingId, providerId, shippingModeCode) {
    let id = encryptBookingID(bookingId, providerId, shippingModeCode)
    this._router.navigate(['/provider/booking-detail', id])
  }

  onPageChange(number) {
    this.paginationConfig.currentPage = number
  }

  public currentBookingConfig: PaginationInstance = {
    id: 'advance',
    itemsPerPage: 10,
    currentPage: 1
  }

  onTabChange($change) {
    const { nextId } = $change
    this.currentPage = 1
    this.activeIdString = nextId
    this.paginationConfig.currentPage = 1
  }

  refreshDasboardData() {
    setTimeout(() => {
      this._sharedService.bookingsRefresh.next(true)
    }, 0)
  }

  totBooking: number = 0
  totCurrBooking: number = 0
  totPastBooking: number = 0
  totSaveBooking: number = 0
  totSpotReq: number = 0

  onBookingListChange($bookingsObject: FilterdBookings) {
    const { data, totBooking, totCurrBooking, totPastBooking, totSaveBooking, totSpotReq } = $bookingsObject

    this.totBooking = totBooking
    this.totCurrBooking = totCurrBooking
    this._sharedService.currentBookingCount.next($bookingsObject)
    this.totPastBooking = totPastBooking
    this.totSaveBooking = totSaveBooking
    this.totSpotReq = totSpotReq
    if ($bookingsObject.pageNumber) {
      this.currentPage = $bookingsObject.pageNumber
    }

    switch (this.activeIdString) {
      case 'tab-current':
        this.currentBookings = data
        this.totalPages = totCurrBooking
        this.specialBookings = []
        this.totalBookings = []
        break
      case 'tab-past':
        this.totalPages = totPastBooking
        this.currentBookings = []
        this.pastBookings = data
        this.totalBookings = []
        break
      case 'tab-special':
        this.specialBookings = data
        this.totalPages = totSpotReq
        this.currentBookings = []
        this.totalBookings = []
        break
      case 'tab-total':
        this.totalBookings = data
        this.totalPages = totBooking
        this.currentBookings = []
        this.specialBookings = []
        break
      default:
        break
    }
  }

  getTotalPages(pages) {
    let temp: any = pages
    return Math.ceil(temp / this.currentBookingConfig.itemsPerPage)
  }

  onPageChangeBootstrap(event) { }

  removeBooking($bookingId: number) {
    if ($bookingId > -1) {
      this.refreshDasboardData()
    }
  }

  ngOnDestroy() {
  }

}
