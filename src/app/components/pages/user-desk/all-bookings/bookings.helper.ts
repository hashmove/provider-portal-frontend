export class BookingsHelper {

    public pageType: string = 'bookings'
    public activeIdString: string = null


    payloadForGroupedBookings($booking) {
        // console.log($booking)
        return {
            userID: $booking.UserID,
            providerID: $booking.ProviderID,
            bookingNumber: null,
            bookingStatusCode: null,
            shipmentStatusCode: null,
            polID: 0,
            polCode: null,
            polType: null,
            podID: 0,
            podCode: null,
            podType: null,
            cargoType: null,
            shippingModeID: 0,
            fromDate: null,
            toDate: null,
            filterProviderID: 0,
            filterCompanyID: 0,
            carrierID: 0,
            sortBy: 'date',
            sortDirection: 'DESC',
            pageSize: 10,
            pageNumber: 1,
            tab: 'SpecialRequest',
            blNumber: null,
            spotRateBookingKey: $booking.SpotRateBookingKey
        }
    }

    setPageConfig() {
        if (location.href.includes('rate-requests-v1')) {
            this.pageType = 'rate-requests-v1'
            this.activeIdString = 'tab-special'
        } else {
            this.pageType = 'bookings'
            this.activeIdString = 'tab-current'
        }
    }
}