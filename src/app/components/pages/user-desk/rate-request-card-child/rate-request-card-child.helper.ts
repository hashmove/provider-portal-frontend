import { NgbModalOptions } from '@ng-bootstrap/ng-bootstrap'
import { getImagePath, getLoggedUserData, ImageRequiredSize, ImageSource, loading } from '../../../../constants/globalFunctions'
import { SeaFreightService } from '../manage-rates/sea-freight/sea-freight.service'
export class RateRequestCardChildHelper {

    private static SEA_CHARGES = []
    cargoDetModal: NgbModalOptions = {
        size: 'lg', centered: true, windowClass: 'carge-detail', backdrop: 'static', keyboard: false
    }
    largeModal: NgbModalOptions = {
        size: 'lg', centered: true, windowClass: 'large-modal', backdrop: 'static', keyboard: false
    }
    mediumModal: NgbModalOptions = {
        size: 'lg', windowClass: 'medium-modal', centered: true, backdrop: 'static', keyboard: false
    }

    chatModal: NgbModalOptions = {
        size: 'lg', centered: true, windowClass: 'chat-modal', backdrop: 'static', keyboard: false
    }

    async getAdditionalData(_seaFreightService: SeaFreightService, _mode:string) {
        loading(true)
        const additionalCharges = (localStorage.hasOwnProperty('additionalCharges')) ? JSON.parse(localStorage.getItem('additionalCharges')) : null
        if (additionalCharges) {
            RateRequestCardChildHelper.SEA_CHARGES = additionalCharges.filter(e => e.modeOfTrans === _mode && e.addChrType === 'ADCH')
        } else {
            try {
                const _res = await _seaFreightService.getAllAdditionalCharges(getLoggedUserData().ProviderID).toPromise() as any[]
                RateRequestCardChildHelper.SEA_CHARGES = _res.filter(e => e.modeOfTrans === _mode && e.addChrType === 'ADCH')
            } catch { }
        }
        loading(false)
        return RateRequestCardChildHelper.SEA_CHARGES
    }

    getImage = ($image) => getImagePath(ImageSource.FROM_SERVER, $image, ImageRequiredSize.original)

    enabledRequestStatuses = ['PENDING', 'ACTIVE', 'PARTIAL_BOOKED']
    enabledResponseStatuses = ['PRICE_SENT', 'REQUESTED_AGAIN']

}
