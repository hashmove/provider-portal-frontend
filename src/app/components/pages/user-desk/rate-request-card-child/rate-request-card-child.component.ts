import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core'
import { encryptBookingID, loading, getLoggedUserData, changeCase, isArrayValid, isMobile } from '../../../../constants/globalFunctions'
import { baseExternalAssets } from '../../../../constants/base.url'
import { NgbModal, NgbTooltipConfig } from '@ng-bootstrap/ng-bootstrap'
import { PriceLogsComponent } from '../../../../shared/dialogues/price-logs/price-logs.component'
import { TermsConditDialogComponent } from '../../../../shared/dialogues/terms-condition/terms-condition.component'
import { JsonResponse } from '../../../../interfaces/JsonResponse'
import { ToastrService } from 'ngx-toastr'
import { SharedService } from '../../../../services/shared.service'
import { ViewBookingService } from '../view-booking/view-booking.service'
import { SeaFreightService } from '../manage-rates/sea-freight/sea-freight.service'
import { DocumentUploadComponent } from '../../../../shared/dialogues/document-upload/document-upload.component'
import { RateRequestCardChildHelper } from './rate-request-card-child.helper'
import { RequestAddRateDialogComponent } from '../../../../shared/dialogues/request-add-rate-dialog/request-add-rate-dialog.component'
import { IContanerInput, IRateData, IRateRequest, IRequestSchedule, JsonContainerDet, JsonSurchargeDet } from '../../../../shared/dialogues/request-add-rate-dialog/request-add-rate.interface'
import { CargoDetailsRateRequestComponent } from '../../../../shared/dialogues/cargo-details-rate-request/cargo-details-rate-request.component'
import { ChatRequestComponent } from '../../../../shared/dialogues/chat-request/chat-request.component'
import { RequestStatusUpdationComponent } from '../../../../shared/dialogues/request-status-updation/request-status-updation.component'
import * as moment from 'moment'
import { CurrencyControl } from '../../../../services/currency.service'
import { RequestPriceDetailsComponent } from '../request-price-details/request-price-details.component'
import { IRequestStatus, UserInfo } from '../../../../interfaces'
import { DashboardService } from '../dashboard/dashboard.service'
import { Router } from '@angular/router'
import { ConfirmDialogContent, ConfirmDialogGenComponent } from '../../../../shared/dialogues/confirm-dialog-generic/confirm-dialog-generic.component'
import { ViaPortSeaComponent } from '../../../../shared/dialogues/via-port/via-port.component'

@Component({
  selector: 'app-rate-request-card-child',
  templateUrl: './rate-request-card-child.component.html',
  styleUrls: ['./rate-request-card-child.component.scss']
})
export class RateRequestCardChildComponent extends RateRequestCardChildHelper implements OnInit {

  @Input() request: IRateRequest
  @Input() isRequestEnabled = false
  @Input() IsSpotratAllowed = false
  @Output() bookingRemove = new EventEmitter<number>()
  @Output() responseUpdate = new EventEmitter<boolean>()
  @Output() addRate = new EventEmitter<boolean>()
  @Input() requestStatusList: Array<IRequestStatus> = []
  public baseExternalAssets: string = baseExternalAssets
  public cachedLogs: any[] = []
  public isExpanded = false
  public statusCode = {
    DRAFT: 'draft',
    APPROVED: 'approved',
    REJECTED: 'rejected',
    CANCELLED: 'cancelled',
    DISCARDED: 'discarded',
    DISCARD: 'discard',
    DECLINED: 'declined',
    CONFIRMED: 'confirmed',
    IN_TRANSIT: 'in-transit',
    RE_UPLOAD: 're-upload',
    COMPLETED: 'completed',
    READY_TO_SHIP: 'ready to ship',
    IN_REVIEW: 'in-review',
    PENDING: 'pending',
    REQUESTED_AGAIN: 'requested_again'
  }

  priceSent = false
  totalCBM = 0
  showSchedule = false
  routesDirection = []
  requestSchedule: IRequestSchedule
  searchMode = ''
  searchCriteria = null
  etaDate = null
  exchangeRateList = []
  containers = []
  currencyList = []
  userProfile: UserInfo = null
  respondedCBM = 0
  requestedCBM = 0

  inputContainers: IContanerInput[] = []
  isResponseEnabled = true
  enableChat = true
  termsData = null
  paymentTerm
  isMobile = isMobile()

  constructor(
    private modalService: NgbModal,
    private _sharedService: SharedService,
    private _seaFreightService: SeaFreightService,
    private _toastr: ToastrService,
    private _bookingService: ViewBookingService,
    private _dashboardService: DashboardService,
    private _currencyControl: CurrencyControl,
    private _router: Router,
  
  ) {
    super()
  }

  ngOnInit() {
    this.searchMode = this.request.ShippingModeCode.toLowerCase() + '-' + this.request.ContainerLoad.toLowerCase()
    this.userProfile = getLoggedUserData()
    this.currencyList = this._currencyControl.getCurrencyList()
    this.exchangeRateList = this._currencyControl.getExchangeRateList().rates
    this.setEtaDate()
    this.setResponseStatus()
    // if(this.request.ResponseID === 774) {
    //   this.getContainerDetails()
    // }
    if (this.searchMode === 'sea-lcl')
      this.totalCBM = Math.ceil(this.request.TotalCBM)
  }

  setResponseStatus() {
    if (this.isRequestEnabled && this.enabledResponseStatuses.includes(this.request.ResponseStatusBL)) {
      this.isResponseEnabled = true
    } else {
      this.isResponseEnabled = false
    }

    if (this.isResponseEnabled || this.request.ResponseStatusBL === 'ACTIVE') {
      this.enableChat = true
    } else {
      this.enableChat = false
    }
  }

  setEtaDate() {
    if (this.request.VesselDetail && this.request.VesselDetail.DepartureDate && this.request.VesselDetail.TransitDays) {
      const { DepartureDate, TransitDays } = this.request.VesselDetail
      this.etaDate = moment(DepartureDate).add(TransitDays, 'days').format('YYYY-MM-DD')
    }
  }

  getVesselSchedule() {
    this.showSchedule = !this.showSchedule
    this.requestSchedule = this.request.VesselDetail
    if (this.showSchedule && this.request.VesselDetail) {
      this.requestSchedule = this.request.VesselDetail
    } else {
      this.showSchedule = false
      this.requestSchedule = null
    }
    // if (this.showSchedule) {
    //   loading(true)
    //   this._bookingService.getResponseData(this.request.ResponseID ? this.request.ResponseID : 0).subscribe((res: JsonResponse) => {
    //     loading(false)
    //     if (res.returnId > 0 && typeof res.returnObject === 'object' && res.returnObject.JsonTransferVesselDetail) {
    //       setTimeout(() => {
    //         this.showSchedule = true
    //         this.requestSchedule = JSON.parse(res.returnObject.JsonTransferVesselDetail)
    //         console.log(this.requestSchedule)
    //       }, 900)
    //     } else {
    //       this.showSchedule = false
    //       this.requestSchedule = null
    //     }
    //   }, error => {
    //     this.showSchedule = false
    //   })
    // }
  }

  setSchedule(state: boolean, data: any) {
    this.showSchedule = state
    this.requestSchedule = data
  }

  onBookDescChange($bookingDesc: string) {
    this.request.BookingDesc = $bookingDesc
    if (this.request.ChildRequests && this.request.ChildRequests.length > 0)
      this.request.ChildRequests.forEach(_booking => { _booking.BookingDesc = $bookingDesc })
  }

  openPriceLog = () => {
    const modalRef = this.modalService.open(PriceLogsComponent, this.largeModal)
    modalRef.componentInstance.data = { request: this.request, logs: this.cachedLogs }
    modalRef.componentInstance.isRV2 = true
  }

  openChat() {
    if (!this.IsSpotratAllowed) {
      this.spotRateLocked()
      return
    }
    const modalRef = this.modalService.open(ChatRequestComponent, this.chatModal)
    modalRef.result.then(result => {
      this.request.ProviderUnReadMsgCount = 0
    })
    modalRef.componentInstance.request = this.request
    modalRef.componentInstance.isEnabled = this.enableChat
  }

  openCargoDetails() {
    loading(true)
    if (this.request.ContainerLoad === 'LCL') {
      this._bookingService.getRequestCriteria(this.request.RequestID).subscribe((res: JsonResponse) => {
        if (res.returnId > 0) {
          this.openCargo(JSON.parse(res.returnObject))
        } else {
          this.openCargo(null)
        }
      }, error => this.openCargo(null))
    } else {
      this.openCargo(null)
    }
  }

  openCargo(searchCriteria) {
    loading(true)
    const _id = encryptBookingID(this.request.RequestID, this.request.ProviderID, this.request.ShippingModeCode);
    this._dashboardService.getRequestContainer(_id, (this.request.ResponseID ? this.request.ResponseID : 0), 'PROVIDER').subscribe((res: JsonResponse) => {
      const modalRef = this.modalService.open(CargoDetailsRateRequestComponent, {
        size: 'lg', centered: true, windowClass: 'carge-detail', backdrop: 'static', keyboard: false
      })
      modalRef.componentInstance.request = this.request
      modalRef.componentInstance.searchCriteria = searchCriteria
      modalRef.componentInstance.contResp = res
    })

  }


  async openSeaDialog() {
    if (!this.IsSpotratAllowed) {
      this.spotRateLocked()
      return
    }
    const _additionalSeaCharges = await this.getAdditionalData(this._seaFreightService, this.request.ShippingModeCode)
    const modalRef = this.modalService.open(RequestAddRateDialogComponent, this.largeModal)
    modalRef.result.then((_state) => {
      this.responseUpdate.emit(true)
    })
    const object = {
      forType: this.request.ContainerLoad,
      addList: _additionalSeaCharges,
      mode: 'request',
      customers: [],
      drafts: [],
      rateRequest: this.request
    }
    modalRef.componentInstance.requestData = object
    modalRef.componentInstance.editorContent = null
  }

  uploadDocuments = () => this.modalService.open(DocumentUploadComponent, this.largeModal).componentInstance = this.request

  cancelBooking() {
    if (!this.IsSpotratAllowed) {
      this.spotRateLocked()
      return
    }
    const modalRef = this.modalService.open(RequestStatusUpdationComponent, {
      size: 'lg', windowClass: 'medium-modal', centered: true, backdrop: 'static', keyboard: false
    })
    modalRef.componentInstance.request = this.request
    modalRef.result.then((res) => {
      if (res)
        this._sharedService.bookingsRefresh.next(true)
    })
  }

  getContainerDetails() {
    const { request } = this
    this.inputContainers = []
    loading(true)
    this.respondedCBM = 0
    this.requestedCBM = 0
    if (this.searchMode !== 'air-lcl') {
      this._dashboardService.getRequestContainer(this.getBookingKeyUser(request), this.request.ResponseID ? this.request.ResponseID : 0, 'PROVIDER').subscribe((res: JsonResponse) => {
        this.containers = res.returnObject
        if (this.containers && this.containers.length > 0) {
          this.containers.forEach(_container => {
            const _selectedCurrency = this.currencyList.find(e => e.id === this.getCurrency())
            this.inputContainers.push({
              container: _container,
              price: null,
              doublePrice: null,
              reqQty: _container.RequestedQty,
              resQty: _container.RequestedQty,
              orginCharges: [{ currency: _selectedCurrency }] as any,
              destinCharges: [{ currency: _selectedCurrency }] as any,
              currency: _selectedCurrency,
              currencyID: null,
              basePrice: null,
              isValidPrice: true,
              isValidQty: true,
              isValidCurr: true
            })
            if (this.searchMode === 'sea-lcl') {
              this.respondedCBM += _container.RequestedCBM
              this.requestedCBM += _container.RequestedCBM
            }

          })
          try {
            if (this.searchMode === 'sea-lcl') {
              this.respondedCBM = this._currencyControl.applyRoundByDecimal(this.requestedCBM, 3)
              this.requestedCBM = this._currencyControl.applyRoundByDecimal(this.respondedCBM, 3)
            }
          } catch { }
          this.setRateData()
        } else {
          this.setRateData()
        }
      }, error => loading(false))
    } else {
      const _selectedCurrency = this.currencyList.find(e => e.id === this.getCurrency())
      this.inputContainers.push({
        container: null,
        price: null,
        doublePrice: null,
        reqQty: null,
        resQty: null,
        orginCharges: [{ currency: _selectedCurrency }] as any,
        destinCharges: [{ currency: _selectedCurrency }] as any,
        currency: _selectedCurrency,
        currencyID: _selectedCurrency.id,
        basePrice: 0,
        isValidPrice: true,
        isValidQty: true,
        isValidCurr: true
      })
      if (this.searchMode === 'air-lcl') {
        this.respondedCBM = this._currencyControl.applyRoundByDecimal(this.request.AirDet.ChargeableWeight, 3)
        this.requestedCBM = this._currencyControl.applyRoundByDecimal(this.request.AirDet.ChargeableWeight, 3)
      }
      this.setRateData()

    }
  }

  setRateData() {
    this._bookingService.getResponseData(this.request.ResponseID ? this.request.ResponseID : 0).subscribe((res: JsonResponse) => {
      if (res.returnId > 0) {
        const _rateData: IRateData = changeCase(res.returnObject, 'camel')

        for (let index = 0; index < this.inputContainers.length; index++) {
          try {
            const _jsonContainerDet: JsonContainerDet = JSON.parse(_rateData.jsonContainerDet as any)[index]
            const _container = this.inputContainers[index]

            const _contCurr = this.currencyList.find(e => e.id === (_jsonContainerDet.currencyID ? _jsonContainerDet.currencyID : this.getCurrency()))
            _container.price = _jsonContainerDet.price
            _container.basePrice = _jsonContainerDet.basePrice
            _container.resQty = _jsonContainerDet.respondedQty
            _container.reqQty = _jsonContainerDet.requestedQty
            _container.currency = _contCurr

            try {
              if (_jsonContainerDet.jsonSurchargeDet.filter(_charge => _charge.imp_Exp === 'EXPORT').length > 0) {
                _container.orginCharges = _jsonContainerDet.jsonSurchargeDet.filter(_charge => _charge.imp_Exp === 'EXPORT').map(_charge => {
                  const _selectedCurrency = this.currencyList.find(e => e.id === (_charge.currId ? _charge.currId : this.getCurrency()))
                  return { ..._charge, currency: _selectedCurrency }
                })
              }
            } catch { }
            try {
              if (_jsonContainerDet.jsonSurchargeDet.filter(_charge => _charge.imp_Exp === 'IMPORT').length > 0) {
                _container.destinCharges = _jsonContainerDet.jsonSurchargeDet.filter(_charge => _charge.imp_Exp === 'IMPORT').map(_charge => {
                  const _selectedCurrency = this.currencyList.find(e => e.id === (_charge.currId ? _charge.currId : this.getCurrency()))
                  return { ..._charge, currency: _selectedCurrency }
                })
              }
            } catch { }

          } catch { }
        }
        loading(false)
        if (this.request.ContainerLoad === 'LCL') {
          this._bookingService.getRequestCriteria(this.request.RequestID).subscribe((_res: JsonResponse) => {
            // console.log(_res)
            if (_res.returnId > 0) {
              this.viewInvoice(JSON.parse(_res.returnObject))
            } else {
              this.viewInvoice(null)
            }
          }, error => this.viewInvoice(null))
        } else {
          this.viewInvoice(null)
        }
      } else {
        loading(false)
      }
    }, error => loading(false))
  }

  getRateObject(_currUtil: CurrencyControl, inputContainers: IContanerInput[]): JsonContainerDet[] {
    const objDraft: JsonContainerDet[] = []
    try {
      const { exchangeRateList } = this
      // const _itContainers: IContanerInput[] = this.searchMode === 'sea-lcl' ? [inputContainers[0]] : inputContainers
      const _itContainers: IContanerInput[] = inputContainers
      // console.log(_itContainers)

      _itContainers.forEach(_inptCont => {
        const { price, resQty, container, orginCharges, destinCharges } = _inptCont

        const _originCharge: JsonSurchargeDet[] = isArrayValid(orginCharges, 0) ? orginCharges.filter(_charge => _charge.price) : []
        const _destCharge: JsonSurchargeDet[] = isArrayValid(destinCharges, 0) ? destinCharges.filter(_charge => _charge.price) : []
        const _jsonSurchargeDet: JsonSurchargeDet[] = _originCharge.concat(_destCharge)

        let _surChargeFinn: JsonSurchargeDet[] = []
        if (isArrayValid(_jsonSurchargeDet, 0)) {
          _surChargeFinn = _jsonSurchargeDet
            .filter(_charge => _charge.price && parseFloat(_charge.price))
            .map(_charge => {
              const __exchangeRate = exchangeRateList.filter(rate => rate.currencyID === _charge.currency.id)[0]
              return {
                ..._charge,
                currId: _charge.currency.id,
                basePrice: _currUtil.getPriceToBase(parseFloat(_charge.price), false, __exchangeRate.rate),
                exchangeRate: __exchangeRate.rate,
                // currExcgRateListID: _exchangeRate.currExcgRateListID
              }
            })
        }


        const _exchangeRate = exchangeRateList.filter(rate => rate.currencyID === _inptCont.currency.id)[0]
        const _requestQty = this.searchMode === 'sea-lcl' ? container.RequestedCBM : this.searchMode === 'air-lcl' ? this.respondedCBM : container.RequestedQty
        objDraft.push({
          containerData: this.searchMode !== 'air-lcl' ? _inptCont.container : null,
          price: price,
          basePrice: _currUtil.getPriceToBase(parseFloat(price ? price : 0), false, _exchangeRate.rate),
          containerSpecID: this.searchMode !== 'air-lcl' ? container.ContainerSpecID : null,
          currencyID: _inptCont.currency.id,
          exchangeRate: _exchangeRate ? _exchangeRate.rate : null,
          // currExcgRateListID: _exchangeRate ? _exchangeRate.currExcgRateListID : null,
          requestedQty: _requestQty,
          respondedQty: resQty ? resQty : _requestQty,
          jsonSurchargeDet: isArrayValid(_surChargeFinn, 0) ? _surChargeFinn : null
        })
      })
    } catch (error) {
      console.log(error)
      return []
    }
    console.log(objDraft)
    return objDraft
  }

  viewInvoice(searchCriteria) {
    const modalRef = this.modalService.open(RequestPriceDetailsComponent, {
      size: 'lg', centered: true, windowClass: 'price-breakdown-popup bar-class', backdrop: 'static', keyboard: false
    })


    modalRef.componentInstance.pricingJSON = this.getRateObject(this._currencyControl, this.inputContainers)
    modalRef.componentInstance.request = this.request
    modalRef.componentInstance.exchangeRateList = this.exchangeRateList
    modalRef.componentInstance.searchCriteria = searchCriteria
    modalRef.componentInstance.totalUnit = this.respondedCBM
  }

  getStatusColor() {
    try {
      return this.requestStatusList.filter(_status => _status.BusinessLogic === this.request.ResponseStatusBL)[0].StatusColorCode
    } catch {
      return 'hashmove-badge-secondary'
    }
  }

  getBookingKeyUser = (_request: IRateRequest) => encryptBookingID(_request.RequestID, _request.Customer.UserID, _request.ShippingModeCode)

  getCurrency = () =>
    this.request.CurrencyID ? this.request.CurrencyID : this.userProfile.CurrencyID

  viewBookingDetails() {
    this._router.navigate(['/provider/booking-detail', encryptBookingID(this.request.BookingID, this.request.ProviderID, this.request.ShippingModeCode)], { queryParams: { dir_from: 'req_list' } })
  }

  onAwaitClick() {
    this.addRate.emit(true)
  }

  openPaymentTerms(action) {
    loading(true)
    this._bookingService.getRequestPaymentTerms(this.request.RequestID, 0, 0).subscribe((res: JsonResponse) => {
      loading(false)
      if (res.returnId > 0) {
        this.paymentTerm = changeCase(res.returnObject, 'pascal')
        console.log(this.paymentTerm.TermsConditionText)
        this.openTermsDialog(this.paymentTerm.TermsConditionText, action)
      } else {
        if (res.returnCode === '2') {
          this._toastr.info(res.returnText, res.returnStatus)
        } else {
          this._toastr.error(res.returnText, res.returnStatus)
        }
      }
    }, error => loading(false))
  }

  openTermsDialog(termsCondition: string, action: any) {
    const modalRef = this.modalService.open(TermsConditDialogComponent, {
      size: 'lg', centered: true, windowClass: 'termsAndCondition', backdrop: 'static', keyboard: false
    })
    modalRef.componentInstance.modalData = {
      data: { provider: null, termsCondition, action },
    }
    modalRef.componentInstance.type = 'PAY_TERMS'
  }

  spotRateLocked() {
    const _modalData: ConfirmDialogContent = {
      messageTitle: 'Access Restricted',
      messageContent: `
      <div class="d-flex flex-column">
        <span>Access Restricted due to Non-Compliance of ELM Terms & Agreement</span>
        <span>Please contact us at <a href="mailto:sales@hashmove.com">sales@hashmove.com</a></span>
      </div>
      `,
      buttonTitle: 'Close',
      data: null
    }

    const modalRef = this.modalService.open(ConfirmDialogGenComponent, {
      size: 'lg', centered: true, windowClass: 'restrict-modal', backdrop: 'static', keyboard: false
    });

    modalRef.result.then((result: string) => {
    })

    modalRef.componentInstance.modalData = _modalData;
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);
  }
}
