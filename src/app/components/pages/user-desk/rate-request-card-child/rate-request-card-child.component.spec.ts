import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RateRequestCardChildComponent } from './rate-request-card-child.component';

describe('RateRequestCardChildComponent', () => {
  let component: RateRequestCardChildComponent;
  let fixture: ComponentFixture<RateRequestCardChildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RateRequestCardChildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RateRequestCardChildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
