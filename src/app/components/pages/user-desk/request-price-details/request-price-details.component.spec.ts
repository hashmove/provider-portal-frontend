import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestPriceDetailsComponent } from './request-price-details.component';

describe('RequestPriceDetailsComponent', () => {
  let component: RequestPriceDetailsComponent;
  let fixture: ComponentFixture<RequestPriceDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestPriceDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestPriceDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
