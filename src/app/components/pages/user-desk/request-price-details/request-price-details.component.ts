import { Component, OnInit, Input } from '@angular/core'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { getBasisStr, getLoggedUserData, isArrayValid } from '../../../../constants/globalFunctions'
import { SharedService } from '../../../../services/shared.service'
import { ToastrService } from 'ngx-toastr'
import { Rate, UserInfo } from '../../../../interfaces'
import { Currency, IContanerInput, IRateRequest, JsonContainerDet, JsonSurchargeDet, SearchCriteria } from '../../../../shared/dialogues/request-add-rate-dialog/request-add-rate.interface'
import { CurrencyControl } from '../../../../services/currency.service'
@Component({
  selector: 'app-request-price-details',
  templateUrl: './request-price-details.component.html',
  styleUrls: ['./request-price-details.component.scss']
})
export class RequestPriceDetailsComponent implements OnInit {
  @Input() pricingJSON: JsonContainerDet[] = []
  @Input() request: IRateRequest
  @Input() searchCriteria: SearchCriteria = null
  @Input() exchangeRateList: Rate[] = []
  @Input() totalUnit: any
  searchMode = 'sea-fcl'
  userProfile: UserInfo
  chargeBasis = ['PER_KG', 'PER_CONTAINER', 'PER_CBM', 'PER_TRUCK']
  fsurBasic = 'Per Container'

  cbmTotalPrice = 0
  cbmTotalDisplayPrice = 0
  isMultiContainer = false
  loading = false

  constructor(
    private _activeModal: NgbActiveModal,
    private _sharedService: SharedService,
    private _currencyControl: CurrencyControl,
    private _toast: ToastrService
  ) { }
  public freightData: JsonContainerDet[] = []
  public additionalData: JsonSurchargeDet[] = []
  public currencyCode
  public totalAmount: any = 0

  public selectedCurrency: Currency
  public currencyList: Currency[] = []
  public actualIndividualPrice
  public specialRequestComments = ''
  originAmount = 0

  ngOnInit() {
    this.cbmTotalPrice = 0
    this.cbmTotalDisplayPrice = 0
    this.userProfile = getLoggedUserData()
    const _state = this._sharedService.currencyList.getValue()
    this.currencyList = _state

    this.selectedCurrency = this.currencyList.find(e => e.id === this.pricingJSON[0].currencyID)
    this.searchMode = this.request.ShippingModeCode.toLowerCase() + '-' + this.request.ContainerLoad.toLowerCase()

    switch (this.searchMode) {
      case 'sea-fcl':
        this.fsurBasic = 'Per Container'
        break;
      case 'sea-lcl':
        this.fsurBasic = 'Per CBM'
        break;
      case 'air-lcl':
        this.fsurBasic = 'Per Kg'
        break;
      case 'truck-ftl':
        this.fsurBasic = 'Per Truck'
        break;
      default:
        this.fsurBasic = 'Per Container'
        break;
    }


    if (isArrayValid(this.pricingJSON, 0)) {
      const { pricingJSON } = this
      this.currencyCode = this.selectedCurrency.code
      const _parentExchangeRate = this.pricingJSON[0].exchangeRate
      // console.log(pricingJSON)

      const baseCurrIDs: number[] = pricingJSON.map(_base => _base.currencyID)
      const addCurrIDs: number[] = []
      pricingJSON.forEach(_base => {
        if (isArrayValid(_base.jsonSurchargeDet, 0)) {
          _base.jsonSurchargeDet.forEach(_charge => addCurrIDs.push(_charge.currId))
        }
      })

      const _unqCurrBase = Array.from(new Set(baseCurrIDs))
      const _unqCurrAdd = Array.from(new Set(addCurrIDs))
      let isSameCurrency = false

      if (_unqCurrBase.length < 2 && _unqCurrAdd.length < 2 && _unqCurrBase[0] === _unqCurrAdd[0])
        isSameCurrency = true
      // console.log(_unqCurrBase, _unqCurrAdd, isSameCurrency)
      // Container
      this.freightData = pricingJSON.map(_price => {
        const isSameBae = _price.currencyID === pricingJSON[0].currencyID
        const _currCode = this.currencyList.find(e => e.id === _price.currencyID).code
        const _resQty = this._currencyControl.applyRoundByDecimal(_price.respondedQty, 3)
        return {
          ..._price,
          total: !isSameBae ? this.getPrice(this._currencyControl.getNewPrice(this.getNum(_price.basePrice), _parentExchangeRate), _resQty, true, '')
            : this.getPrice(this.getNum(_price.price), _resQty, true, ''),
          currCode: _currCode,
          displayPrice: this.getNum(_price.price) * this.getNum(_resQty)
        }
      })

      try {
        if (this.searchMode === 'sea-lcl') {
          this.cbmTotalPrice = this.freightData.map(_freight => _freight.total).reduce((all, item) => (all + item), 0)
          this.cbmTotalDisplayPrice = this.freightData.map(_freight => _freight.displayPrice).reduce((all, item) => (all + item), 0)
        }
      } catch { }

      // Add Charge
      try {
        // console.log('totalUnit:', this.totalUnit)
        pricingJSON.map(_price => {
          const _respondedQty = this.searchMode === 'sea-lcl' || this.searchMode === 'air-lcl' ? this.totalUnit : _price.respondedQty
          try {
            if (isArrayValid(_price.jsonSurchargeDet, 0)) {
              _price.jsonSurchargeDet.forEach(_charge => {
                const isSameAddChar = _charge.currId === pricingJSON[0].currencyID
                this.additionalData.push({
                  ..._charge,
                  // total: this._currencyControl.getNewPrice(_charge.basePrice, _parentExchangeRate),
                  total: !isSameAddChar ? this.getPrice(this._currencyControl.getNewPrice(this.getNum(_charge.basePrice), _parentExchangeRate), _respondedQty, false, _charge.addChrBasis)
                    : this.getPrice(this.getNum(_charge.price), _respondedQty, false, _charge.addChrBasis),
                  displayPrice: this.getPrice(this.getNum(_charge.price), _respondedQty, false, _charge.addChrBasis),
                  containerName: pricingJSON.length > 1 && _price.containerData && _price.containerData.ContainerSpecDesc ? _price.containerData.ContainerSpecDesc : null
                })
              })
            }
          } catch { }
        })
      } catch { }
      const { additionalData } = this

      try {
        const _per_ship = additionalData.filter(_charge => _charge.addChrBasis === 'PER_SHIPMENT')
        const _per_unit = additionalData.filter(_charge => _charge.addChrBasis !== 'PER_SHIPMENT')
        this.additionalData = _per_ship.concat(_per_unit)
      } catch { }

      if (this.searchMode === 'sea-fcl' || this.searchMode === 'truck-ftl') {
        this.isMultiContainer = this.freightData.length > 1 ? true : false
      }

      this.freightData.forEach(element => {
        this.totalAmount += element.total
      })
      this.additionalData.forEach(element => {
        this.totalAmount += element.total
      })
      console.log(this.additionalData)
      try {
        this.originAmount = (this.totalAmount)
      } catch (error) { }
    }
  }

  getNum = (_num: any) => (_num) ? (parseFloat(_num) ? typeof parseFloat(_num) === 'number' ? parseFloat(_num) : 0 : 0) : 0

  getCurrForTotal(inpCont: IContanerInput[]) {
    try {
      return isArrayValid(inpCont, 0) ? (inpCont[0].currency && inpCont[0].currency.id) ? inpCont[0].currency.id : this.getCurrency() : this.getCurrency()
    } catch {
      return this.getCurrency()
    }
  }

  getCurrency = () =>
    this.request.CurrencyID ? this.request.CurrencyID : this.userProfile.CurrencyID

  getPrice = (price, qty, isMain, basis) =>
    (isMain || this.isMulti(basis)) ? this.getNum(price) * this.getNum(qty) : this.getNum(price)

  isMulti = (basis: any) => this.chargeBasis.includes(basis)

  getBasisString = (type) => getBasisStr(type)

  closeModal() {
    this._activeModal.close()
    document.getElementsByTagName('html')[0].style.overflowY = 'auto'
  }

  numberValidwithDecimal(evt) {
    const charCode = (evt.which) ? evt.which : evt.keyCode
    if (evt.target.value && evt.target.value[evt.target.value.length - 1] === '.') {
      if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false
      return true
    }
    if (charCode !== 46 && charCode > 31 && (charCode < 48 || charCode > 57))
      return false

    return true
  }

  getNumber($input) {
    let _num = 0
    try { _num = parseFloat($input) } catch (error) { }
    return _num
  }

  getDaysMonthStr(basis: string, days: number) {
    let toReturn = days
    if (basis.toLowerCase().includes('month')) {
      toReturn = Math.ceil(days / 30)
    } else if (basis.toLowerCase().includes('year')) {
      toReturn = Math.ceil(days / 365)
    } else {
      toReturn = days
    }
    return toReturn
  }

  getSimplifiedStr(str: string) {
    let _str = ''
    try { _str = str.replace('S', '') } catch (error) { }
    return _str
  }

  getDaysCount(base: string) {
    let toRet = null
    let finn = 'day'
    try {
      if (base.toLowerCase().includes('month'))
        toRet = 30
      else if (base.toLowerCase().includes('year'))
        toRet = 365
      if (toRet) finn = `${toRet} days`
    } catch { }
    return finn
  }
}
