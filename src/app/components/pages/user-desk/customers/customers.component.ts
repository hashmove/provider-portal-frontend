import { Component, OnInit, OnDestroy } from '@angular/core'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { AddCustomerComponent } from '../../../../shared/dialogues/add-customer/add-customer.component'
import { CustomersService } from './customers.service'
import { untilDestroyed } from 'ngx-take-until-destroy'
import { baseExternalAssets } from '../../../../constants/base.url'
import { getImagePath, ImageSource, ImageRequiredSize, isJSON, loading, getLoggedUserData } from '../../../../constants/globalFunctions'
import { PaginationInstance } from 'ngx-pagination'
import { firstBy } from 'thenby'
import { ConfirmDialogContent, ConfirmDialogGenComponent } from '../../../../shared/dialogues/confirm-dialog-generic/confirm-dialog-generic.component'
import { UserInfo } from '../../../../interfaces/billing.interface'
import { JsonResponse } from '../../../../interfaces/JsonResponse'
import { ToastrService } from 'ngx-toastr'
import { ICustomers } from '../../../../interfaces/customer.interface'
@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit, OnDestroy {
  public userProfile: UserInfo
  public customers: ICustomers[] = []
  public loading: boolean = false
  public searchUser: any
  public maxSize: number = 7
  public directionLinks: boolean = true
  public autoHide: boolean = false
  public userPaginationConfig: PaginationInstance = {
    id: "users", itemsPerPage: 10, currentPage: 1
  }
  public labels: any = { previousLabel: "", nextLabel: "" }
  public currentSort: string = "name"
  public isVirtualAirline: boolean = false
  public mainList: ICustomers[] = []
  selectedFilterOpt: string = 'Date added'

  constructor(
    private _modalService: NgbModal,
    private _customersService: CustomersService,
    private _toastr: ToastrService
  ) { }

  ngOnInit() {
    this.userProfile = getLoggedUserData()
    loading(true)
    this.getProviderCustomerList()
  }

  addCustomer() {
    const modalRef = this._modalService.open(AddCustomerComponent, {
      size: 'lg', centered: true, windowClass: 'small-modal', backdrop: 'static', keyboard: false
    })
    modalRef.componentInstance.searchData = {
      providerID: this.userProfile.ProviderID, email: this.userProfile.PrimaryEmail
    }
    modalRef.result.then((result) => {
      if (result) this.getProviderCustomerList()
    })
  }

  /**
   * GET CUSTOMERS FOR PROVIDER
   *
   * @memberof CustomersComponent
   */
  getProviderCustomerList() {
    this._customersService.getProviderCustomerList(this.userProfile.ProviderID).pipe(untilDestroyed(this)).subscribe((res: any) => {
      loading(false)
      if (res.returnId > 0) {
        this.mainList = res.returnObject
        this.mainList.forEach(_customer => {
          const { companyName, cityName, countryName, contactPerson } = _customer
          _customer.searchField = companyName + ' ' + cityName + ' ' + countryName + '' + contactPerson
        })
        this.customers = res.returnObject
        this.onOptionChange('Date added', -1)
      }
    }, (err: any) => loading(false))
  }

  /**
   * [Get UI image path from server]
   * @param  $image [description]
   * @param  type   [description]
   * @return        [description]
   */
  getProviderImage($image: string) {
    if (isJSON($image)) {
      return baseExternalAssets + '/' + JSON.parse($image)[0].DocumentFile
    } else {
      return getImagePath(ImageSource.FROM_SERVER, $image, ImageRequiredSize.original)
    }
  }

  onPageChange = (number: any) => this.userPaginationConfig.currentPage = number

  onOptionChange($option: string, $order: number, $event?: any) {
    this.selectedFilterOpt = $option
    const { mainList } = this
    if ($option === 'Date added') {
      this.mainList = mainList.sort(firstBy('createdDate', { direction: $order }))
      this.customers = this.mainList
    }
    if ($option === 'Name') {
      this.mainList = mainList.sort(firstBy(function (v) { return v.companyName.toLowerCase() }, { direction: $order }))
      this.customers = this.mainList
    }
    if ($option === 'Location') {
      this.mainList = mainList.sort(firstBy(function (v) { return v.cityName.toLowerCase() }, { direction: $order }))
      this.customers = this.mainList
    }
    if ($option === 'Person') {
      this.mainList = mainList.sort(firstBy(function (v) { return v.contactPerson.toLowerCase() }, { direction: $order }))
      this.customers = this.mainList
    }
    if ($option === 'Bookings') {
      this.mainList = mainList.sort(firstBy('bookingCount', { direction: $order }))
      this.customers = this.mainList
    }

    if ($event) {
      let count = $event.currentTarget.parentElement.parentElement.parentElement.children
      for (let i = 0; i < count.length; i++) {
        for (let j = 1; j < count[i].children.length; j++) {
          if (count[i].children[j].children[0].classList.length) {
            count[i].children[j].children[0].classList.remove('active')
            count[i].classList.remove('active')
          }
        }
        $event.currentTarget.parentElement.parentElement.classList.add('active')
        $event.currentTarget.classList.add('active')
      }
    }
  }

  approveCustomer($customer: ICustomers, type: string) {
    let _modalData: ConfirmDialogContent
    if (type === 'approve') {
      _modalData = {
        messageTitle: 'Approve Customer',
        messageContent: `Are you sure you want to approve this customer?`,
        buttonTitle: 'Yes',
        data: null
      }
    } else {
      _modalData = {
        messageTitle: 'Reject Customer',
        messageContent: `Are you sure you want to reject this customer?`,
        buttonTitle: 'Yes',
        data: null
      }
    }
    const modalRef = this._modalService.open(ConfirmDialogGenComponent, {
      size: 'lg', centered: true, windowClass: 'medium-modal', backdrop: 'static', keyboard: false
    })
    modalRef.result.then((result: string) => {
      if (result) {
        const toSend = {
          userID: $customer.userID,
          companyID: $customer.companyID,
          approverID: this.userProfile.UserID,
          approvalStatus: (type === 'approve') ? 'APPROVED' : 'REJECTED',
        }
        loading(true)
        this._customersService.approveCustomer(toSend).subscribe((res: JsonResponse) => {
          if (res.returnId > 0) {
            this._toastr.success(res.returnText)
            this.getProviderCustomerList()
          } else {
            loading(false)
            this._toastr.error(res.returnText)
          }
        }, () => loading(false))
      }
    })

    modalRef.componentInstance.modalData = _modalData
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open'))
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden'
    }, 0)
  }

  refresh() {
    loading(true)
    this.getProviderCustomerList()
  }

  onCustomerChange() {
    setTimeout(() => {
      const { mainList, searchUser } = this
      this.customers = mainList.filter(function (item) {
        return item.searchField.toLowerCase().includes(searchUser.replace(/\s+/g, ' ').toLowerCase())
      })
    }, 10)
  }

  ngOnDestroy() { }
}
