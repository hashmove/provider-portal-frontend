import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnDestroy, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { untilDestroyed } from 'ngx-take-until-destroy';
import * as pbi from 'powerbi-client';
import { baseApi, biConfig } from '../../../../constants/base.url';
import { loading, getLoggedUserData } from '../../../../constants/globalFunctions';
import { UserInfo } from '../../../../interfaces';
import { SharedService } from '../../../../services/shared.service';
import * as JQ from 'jquery'
import { IBiConfig } from '../../../../services/jwt.injectable';

@Component({
  selector: 'app-power-bi',
  templateUrl: './power-bi.component.html',
  styleUrls: ['./power-bi.component.scss'],
  encapsulation: ViewEncapsulation.None,
})


export class PowerBiComponent implements OnInit, OnDestroy {

  public screenHeight: number;
  report: pbi.Embed;
  @ViewChild('reportContainer') reportContainer: ElementRef;
  hasData = true
  loggedUser: UserInfo
  reportId: null
  biConfigDef: IBiConfig = biConfig

  constructor(
    private _sharedService: SharedService,
    private http: HttpClient
  ) { }

  isDataLoaded = false
  dashData


  async ngOnInit() {
    console.log(this.biConfigDef);
    
    this.loggedUser = getLoggedUserData()
    this._sharedService.dashboardDetail.pipe(untilDestroyed(this)).subscribe((data: any) => {
      loading(false)
      if (data !== null) {
        if (!this.isDataLoaded) {
          this.screenHeight = (window.screen.height);
          console.log(data)
          this.dashData = data
          this.getToken();
          this.isDataLoaded = true
        }
      }
    });
  }

  showReportV2({ token, reportID }) {
    // Embed URL    
    let embedUrl = powerBI.reportBaseURL;
    let embedReportId = reportID;

    let settings: pbi.IEmbedSettings = {
      filterPaneEnabled: false,
      navContentPaneEnabled: false,
      // background: 1
    };

    // let _providerName = ''
    // try {
    //   if (this._sharedService.dashboardDetail.getValue() && this._sharedService.dashboardDetail.getValue().ProviderName) {
    //     _providerName = this._sharedService.dashboardDetail.getValue().ProviderName
    //   } else {
    //     _providerName = this.loggedUser.CompanyName
    //   }
    // } catch {
    //   _providerName = this.loggedUser.CompanyName
    // }

    let flt: pbi.models.IBasicFilter[] = [{
      $schema: 'http://powerbi.com/product/schema#basic',
      target: { table: 'Provider_Mst', column: 'ProviderID' },
      operator: 'In',
      values: [this.loggedUser.ProviderID],
      filterType: pbi.models.FilterType.Basic,
      requireSingleSelection: true,
    }];

    let config: pbi.IEmbedConfiguration = {
      type: 'report',
      tokenType: pbi.models.TokenType.Embed,
      accessToken: token,
      embedUrl: embedUrl,
      id: embedReportId,
      filters: flt,
      settings: settings,
      //pageName: "ReportSectionecbdc55cc8f71d5a5d7f"
    };

    let reportContainer = this.reportContainer.nativeElement;

    let powerbi = new pbi.service.Service(pbi.factories.hpmFactory, pbi.factories.wpmpFactory, pbi.factories.routerFactory);

    this.report = powerbi.embed(reportContainer, config);

    // this.report.off("loaded");
    this.report.on("loaded", ($event) => {
      console.log("loaded");
      // console.log($event.target);
      // // console.log($($event.target).find('.displayAreaViewport')[0]);
      // console.log($($event.target).find('iframe').find('body'));
      // $($event.target).find('.displayAreaViewport').addClass('yoloxas')
      // $($event.target).find('.displayAreaViewport').css("background", "#f7f8fc");

      // this.setTokenExpirationListener(Token.expiration, 2);
    });
    this.report.on("error", () => {
      console.log("Error");
    });
  }

  getToken() {

    let strToken: string = "";
    this.http.get(baseApi + 'token/PowerBi/GetEmbedToken').subscribe((data: any) => {
      strToken = data.token;
      console.log("getToken ", data.token);
      this.showReportV2(data);
    }, error => {
      this.hasData = false
    })
  }

  ngOnDestroy() {

  }

}

export const powerBI = {
  reportBaseURL: 'https://app.powerbi.com/reportEmbed',
  qnaBaseURL: 'https://app.powerbi.com/qnaEmbed',
  tileBaseURL: 'https://app.powerbi.com/embed',
  // groupID: '9cd794c8-0520-4a1d-a0a8-a74367085b70',
  // reportID: 'fd19810b-5224-4a74-a926-82258650786f'
}