import { Injectable } from '@angular/core';
import { baseApi } from '../../../../constants/base.url';
import { HttpClient, HttpParams } from "@angular/common/http";

@Injectable()
export class SettingService {

  constructor(private _http: HttpClient) { }

  getSettingInfo(userId) {
    let url = `provider/GetSettings/${userId}`;
    return this._http.get(baseApi + url);
  }
  deSelectService(obj) {
    let url = "provider/SaveProviderLogistics";
    return this._http.post(baseApi + url, obj);
  }
  selectProviderService(obj) {
    let url = "provider/SaveProviderLogistics";
    return this._http.post(baseApi + url, obj);
  }
  updateRealRstate(obj) {
    let url = "provider/UpdateWarehouseOwner";
    return this._http.put(baseApi + url, obj);
  }
  deSelectAssociationService(obj) {
    let url = "provider/SaveProviderAssociation";
    return this._http.post(baseApi + url, obj);
  }
  selectAssociationService(obj) {
    let url = "provider/SaveProviderAssociation";
    return this._http.post(baseApi + url, obj);
  }
  companyAbout(obj) {
    let url = "provider/UpdateAboutBusiness";
    return this._http.put(baseApi + url, obj);
  }

  changePassword(obj) {
    let url = "users/ChangePassword";
    return this._http.put(baseApi + url, obj);
  }
  deactivateAccount(deletingUserID, deleteByUserID) {
    let url = `users/DeleteAccountRequest/${deletingUserID}/${deleteByUserID}`;
    return this._http.delete(baseApi + url);
  }
  personalSetting(obj) {
    let url = "provider/UpdatePersonalSettings";
    return this._http.put(baseApi + url, obj);
  }

  businessSettingOld(obj) {
    let url = "provider/UpdateBusinessSettings";
    return this._http.put(baseApi + url, obj);
  }

  getProviderBank(providerID) {
    let url = `provider/GetProviderBank/${providerID}`;
    return this._http.get(baseApi + url);
  }

  setProviderBank(data) {
    let url = `provider/SaveProviderBank`;
    return this._http.post(baseApi + url, data);
  }

  updateProviderIndustryIDs(providerID, industryID, isSelected) {
    let url: string = `provider/UpdateProviderIndustryIDs/${providerID}/${industryID}/${isSelected}`;
    return this._http.put(baseApi + url, {});
  }


  get(data) {
    let url = `provider/g`;
    return this._http.get(baseApi + url, data);
  }

  addEditBusinessAddress(data) {
    let url = `provider/SaveProviderOffice`;
    return this._http.post(baseApi + url, data);
  }

  deleteBusinessAddress(providerOfficeID) {
    let url = `provider/DeleteProviderOffice/${providerOfficeID}`
    return this._http.delete(baseApi + url)
  }

  getProviderOfficeList(providerID) {
    let url = `provider/GetProviderOfficeList/${providerID}`
    return this._http.get(baseApi + url)
  }

  getGoogleAccessToken() {
    let url = `token/GetGoogleAccessToken`
    return this._http.get(baseApi + url)
  }

  updateProviderToggles(providerID, isSelected) {
    let url = `provider/UpdateProviderToggles/Office/${providerID}/${isSelected}`
    return this._http.put(baseApi + url, {})
  }

  getProviderNewsList(providerId) {
    let url = `provider/GetProviderNewsList/${providerId}`
    return this._http.get(baseApi + url)
  }

  addProviderNews(data) {
    let url = `provider/SaveProviderNews`
    return this._http.post(baseApi + url, data)
  }

  deleteProviderNews(newsId) {
    let url = `provider/DeleteProviderNews/${newsId}`
    return this._http.delete(baseApi + url)
  }

  updateProviderNewsToggles(providerID, isSelected) {
    let url = `provider/UpdateProviderToggles/News/${providerID}/${isSelected}`
    return this._http.put(baseApi + url, {})
  }


  updateProviderLogisticImage($provLogServID, $logServSelectedImage) {
    // let url = `provider/UpdateProviderLogisticImage/${$provLogServID}/${$logServSelectedImage}`
    let url = `provider/UpdateProviderLogisticImage`
    const toSend = {
      "provLogServID": $provLogServID,
      "logServSelectedImage": $logServSelectedImage
    }
    return this._http.put(baseApi + url, toSend)
  }

  updateIsShowServiceOnProfile($provLogServID, $isOnProfile) {
    let url = `provider/UpdateIsShowServiceOnProfile/${$provLogServID}/${$isOnProfile}`
    return this._http.put(baseApi + url, {})
  }

  updateServiceDisplayOnProfile(providerID, isSelected) {
    let url = `provider/UpdateProviderToggles/Service/${providerID}/${isSelected}`
    return this._http.put(baseApi + url, {})
  }


  businessSetting(obj) {
    let url = "provider/UpdateBusinessInformationSettings";
    return this._http.put(baseApi + url, obj);
  }

  updateRateValidity(data) {
    const url = 'provider/UpdateManageRateValiditySettings'
    return this._http.put(baseApi + url, data)
  }

  updateHomePageSettings(data) {
    const url = 'provider/UpdateHomePageSettings'
    return this._http.put(baseApi + url, data)
  }


  updatePlatformSettings(data) {
    const url = 'provider/UpdatePlatformSettings'
    return this._http.put(baseApi + url, data)
  }

  updatePlatformSettingsV2(providerID, data) {
    const url = `provider/UpdateProviderFeatureToggle?providerID=${providerID}`
    return this._http.put(baseApi + url, data)
  }


  updateSocialMediaSettings(data) {
    const url = 'provider/UpdateSocialMediaSettings'
    return this._http.put(baseApi + url, data)
  }

  updateIndustrySettings(data, providerId) {
    const url = `provider/UpdateIndustrySettings?providerID=${providerId}`
    return this._http.put(baseApi + url, data)
  }

  updateBannerSettings(data) {
    const url = `provider/UpdateBannerSettings`
    return this._http.put(baseApi + url, data)
  }

  updateOfficeSettings(data) {
    const url = `provider/UpdateOfficeSettings`
    return this._http.put(baseApi + url, data)
  }

  updateAssociationList(data) {
    const url = `provider/UpdateMembershipSettings`
    return this._http.put(baseApi + url, data)
  }

  getNotificationEmails($userID) {
    const url = `provider/GetProviderEmailNotifications/${$userID}`
    return this._http.get(baseApi + url)
  }

  updateNotificationEmails($data, $loginID) {
    const url = `provider/UpdateProviderEmailNotifications/${$loginID}`
    return this._http.put(baseApi + url, $data)
  }
}