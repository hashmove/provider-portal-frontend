import { ViewChild, ElementRef } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { baseExternalAssets } from "../../../../constants/base.url";
import { isMobile } from "../../../../constants/globalFunctions";
import { NgFilesConfig } from "../../../../directives/ng-files";
import { AssociationModel, FreightModel, IBankInfo, IStatistic, IIndustry, IAddressInfo, INews, IRateNotification, IEmailNotifResp, IEmailNotif, Document, IAssociation } from "../../../../interfaces";
import { ISettingsRights } from "../../../../interfaces/access-rights.interface";
import { IUserSettings } from "./settings.interface";

export class SettingsHelper {
    public loadingServices: boolean = false;
    public loadingAssoc: boolean = false;
    public loadingValAdded: boolean = false;
    public baseExternalAssets: string = baseExternalAssets;
    public activeAccordions: string[] = ['PersonalInfo', 'BusinessInfo', 'BusinessProfile', 'Gallery', 'AwardsCert', 'ChangePassword', 'ProfileBannerGallery', 'MarketGallery', 'BankAccountInfo'];
    public selectedDocxlogo: any;
    public docTypeIdLogo = null;
    public docTypeIdProfileLogo = null;
    public docTypeIdCert = null;
    public docTypeIdGallery = null;
    public docTypeIdMarketGallery = null;
    public docTypeIdProfileBanner = null;
    public docTypeIdLiscence = null;
    public uploadedlogo: any;
    public uploadedProfileLogo: Document;
    public companyLogoDocx: any;
    public profileLogoDocx: any;
    public certficateDocx: any;
    public galleriesDocx: any;
    public marketGalleryDocx: any;
    public liscenceDocx: any;
    public profileBannerDocx: any;
    public serverImageDocx: any;
    public uploadedGalleries: any[] = [];
    public uploadedMarketGallery: any[] = [];
    public uploadedProfileBanner: any[] = [];
    public uploadedCertificates: any[] = [];
    public uploadedLiscence: any[] = [];
    public fileStatusLogo = undefined;
    public fileStatusProfileLogo = undefined;
    public fileStatusGallery = undefined;
    public fileStatusMarketGallery = undefined;
    public fileStatusProfileBanner = undefined;
    public fileStatusCert = undefined;
    public fileStatusLiscence = undefined;
    public userProfile: any;
    public config: NgFilesConfig = {
        acceptExtensions: ['jpg', 'png', 'bmp'],
        maxFilesCount: 12,
        maxFileSize: 12 * 1024 * 1000,
        totalFilesSize: 12 * 12 * 1024 * 1000
    };
    public info: IUserSettings;
    public personalInfoForm: any;
    public credentialInfoForm: FormGroup;
    public jobTitles: any;
    public cityList: any;
    public regionList: any[] = [];
    public currencyList: any[] = [];
    public countryList: any[] = [];
    public countryFlagImage: string;
    public mobileCountFlagImage: string;
    public phoneCountryId: any;
    public mobileCountryId: any;
    public phoneCode;
    public mobileCode;
    public personalInfoToggler: boolean = false;
    public businessInfoToggler: boolean = false;
    public businessInfoForm: any;
    public isPrivateMode: boolean;
    public isMarketPlace: boolean;
    public IsExclChargesAllow: boolean;
    public IsCustomerApprovalRequired: boolean;
    public IsTrackingQualityRequired: boolean = false;
    public IsShowWarehouseOnProfile: boolean = false;
    public IsShowSeaRateOnProfile: boolean = false;
    public IsShowSeaLCLRateOnProfile: boolean = false;
    public IsShowAirRateOnProfile: boolean = false;
    public IsShowGroundRateOnProfile: boolean = false;
    public IsShowStatisticsOnProfile: boolean = false;
    public IsShowCompanyLogoOnProfile: boolean = true;
    public IsMarketingTagLineRequired: boolean = true;
    public IsShowOfficeOnProfile: boolean = false;
    public profileLogoCenter: boolean = false;
    public profileLogoLeft: boolean = false;
    public IsTaglineColor: boolean = false;
    public IsStatisticColor: boolean = false;
    public IsShowSearchBoxOnProfile: boolean = false;
    public IsShowNewsOnProfile: boolean = false;
    public IsShowServiceOnProfile: boolean = false;
    // Association
    public allAssociations: AssociationModel[] = [];
    public masterAssociations: AssociationModel[] = [];
    public selectedAssn: AssociationModel = null;
    // Value Added Services
    public valAddedServices: any[] = [];
    public valueService: any[] = [];
    // social
    public socialWebs: {
        SocialMediaPortalsID: number;
        SocialMediaCode: string;
        Title: string;
        LinkURL: string;
    }[] = [];
    public selectedSocialsite: {
        SocialMediaPortalsID: number;
        SocialMediaCode: string;
        Title: string;
        LinkURL: string;
    };
    public socialInputValidate: string;
    public socialSites;
    public freightServices: FreightModel[] = [];
    public frtService: FreightModel[] = [];
    public seaTypeToggler: boolean = false;
    public airTypeToggler: boolean = false;
    public groundTypeToggler: boolean = false;
    public wareHouseTypeToggler: boolean = false;
    public assayerToggler: boolean = false;
    public insuranceToggler: boolean = false;
    public IsRealEstate: boolean = null;
    // about Editor
    public editorContent: any;
    public editable: boolean;
    public toolbarOptions = [
        ['bold', 'italic', 'underline'], // toggled buttons
        [{ 'list': 'ordered' }, { 'list': 'bullet' }],
        [{ 'script': 'sub' }, { 'script': 'super' }], // superscript/subscript
        [{ 'direction': 'rtl' }], // text direction
        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
        [{ 'color': [] }, { 'background': [] }], // dropdown with defaults from theme
        [{ 'align': [] }],
        ['clean'] // remove formatting button
    ]
    public editorOptions = {
        placeholder: "insert content...",
        modules: { toolbar: this.toolbarOptions }
    }
    @ViewChild('editor') editContainer: ElementRef
    //Bank 
    public bankInfoForm: any
    public savedBankInfo: IBankInfo
    bankName_error: boolean = false
    accountTitle_error: boolean = false
    accountNumber_error: boolean = false
    branchName_error: boolean = false
    branchAddress_error: boolean = false
    branchCity_error: boolean = false
    public bankInfoToggler: boolean = false;
    // show more
    public showTogglerText: string;
    public showMoreTextLength: number = 700;
    basicEmailRegex: RegExp = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    linkedInUrl_error: boolean = false
    fbUrl_error: boolean = false
    twitterUrl_error: boolean = false
    instaUrl_error: boolean = false
    youtubeUrl_error: boolean = false
    webUrl_error: boolean = false
    linkedInUrl: string = null
    fbUrl: string = null
    twitterUrl: string = null
    instaUrl: string = null
    youtubeUrl: string = null
    webUrl: string = null
    marketingTagLineHeadingError = false
    marketingTagLineContentError = false
    isWHBookEndAlert = false
    wHBookEndAlertDays: number = 7
    //Statistics Work
    statisticsArray: IStatistic[] = []
    numOfTEUs: number = 0
    numOfPorts: number = 0
    numOfYears: number = 0
    numOfSatisfaction: number = 0
    numOfCovered: number = 0
    numOfWorldwide: number = 0
    //Industry Solution Work
    allIndustriesSolution: IIndustry[] = []
    industriesSolution: IIndustry[] = []
    public loadingIndustry: boolean = false;
    public isOfficeAdd: boolean = false
    public officeAddressList: IAddressInfo[] = []
    IsShowOfficePinBorderColor: boolean = false
    officePinBorderColor
    officeToggler: boolean = false
    //Tagline Color work
    public selectedTaglineColor: string = '#2883e9';
    public selectedStatisticsColor: string = '#2883e9';
    newsList: INews[] = []
    newExVal
    notifArray: IRateNotification[] = []
    emailNotif: { ExpireInDays: number, IsEnable: boolean } = null
    notifToggler = false
    // Home
    homePageForm
    hompageToggler
    footerColor: string = '#2883e9'
    isFooterColor: boolean = false
    //Platform Controls
    platformToggler: boolean = false
    //Social Media
    socialToggler: boolean = false
    socialMediaForm
    //Social Media
    industryToggler: boolean = false
    selectedIndustries: any[] = []
    industryColor: string = '#2883e9'
    isIndustryColor: boolean = false
    listenSocialChanges = true
    listenBusinessChanges = true
    isSpotRateRquired = false
    isDisplayCommodityNameOnSearchBox = false
    listenHomeChanges = false
    //personalInfoForm validators
    firstName_error = false
    lastName_error = false
    jobTitle_error = false
    currency_error = false
    region_error = false
    mobile_error = false
    city_error = false
    preventNewsAcc = false
    preventOfferServices = false
    preventOfficeLocation = false
    controlRecText = 'Keep this feature enabled.'
    controlRecTextDisable = 'Keep this feature disabled.'
    orgName_error: boolean = false
    phone_error: boolean = false
    address_error: boolean = false
    business_email_error: boolean = false
    b_city_error: boolean = false
    public mailNoficiationForm: FormGroup;
    public mailNotificatoinToggler: boolean = false
    public listenNotifChanges: boolean = true
    apiServerEmails: IEmailNotifResp[] = []
    isMobile = isMobile()
    notifInputClass = isMobile() ? 'col-12' : 'col-6'
    masterMailNotif: IEmailNotif[] = [
        { moduleID: -1, moduleBL: 'ALL', isMode: false, moduleTitle: 'All Emails', hasErrors: false, mailList: [], mailInput: null, isMail: true, styleClass: 'col-12', hasTouched: false },
        { moduleID: -1, moduleBL: 'head_break', isMode: false, moduleTitle: 'Booking Email', hasErrors: false, mailList: [], mailInput: null, isMail: false, styleClass: 'col-6', hasTouched: false },
        { moduleID: -1, moduleBL: 'SEA_FCL', isMode: true, moduleTitle: 'Sea Freight FCL', hasErrors: false, mailList: [], mailInput: null, isMail: true, styleClass: 'col-6', hasTouched: false },
        { moduleID: -1, moduleBL: 'SEA_LCL', isMode: true, moduleTitle: 'Sea Freight LCL', hasErrors: false, mailList: [], mailInput: null, isMail: true, styleClass: 'col-6', hasTouched: false },
        { moduleID: -1, moduleBL: 'AIR', isMode: true, moduleTitle: 'Air Freight', hasErrors: false, mailList: [], mailInput: null, isMail: true, styleClass: 'col-6', hasTouched: false },
        { moduleID: -1, moduleBL: 'TRUCK', isMode: true, moduleTitle: 'Ground Transport', hasErrors: false, mailList: [], mailInput: null, isMail: true, styleClass: 'col-6', hasTouched: false },
        { moduleID: -1, moduleBL: 'WAREHOUSE', isMode: true, moduleTitle: 'Warehousing', hasErrors: false, mailList: [], mailInput: null, isMail: true, styleClass: 'col-6', hasTouched: false },
        { moduleID: -1, moduleBL: 'INSURANCE', isMode: true, moduleTitle: 'Insurance', hasErrors: false, mailList: [], mailInput: null, isMail: true, styleClass: 'col-6', hasTouched: false },
        { moduleID: -1, moduleBL: 'ASSAYER', isMode: false, moduleTitle: 'Assayer', hasErrors: false, mailList: [], mailInput: null, isMail: true, styleClass: 'col-6', hasTouched: false },
        // { moduleID: -1, moduleBL: 'break', moduleTitle: null, hasErrors: false, mailList: [], mailInput: null, isMail: false, styleClass: 'col-6', hasTouched: false },
        // { moduleID: -1, moduleBL: 'OTHERS', moduleTitle: 'General (Customer Approvals, User Delete Requests, etc.)', hasErrors: false, mailList: [], mailInput: null, isMail: true, styleClass: 'col-12', hasTouched: false },
    ]
    mailNotif: IEmailNotif[] = []
    affSearch
    showProfileURL: boolean = true
    availModes: string[] = []
    accessRights: ISettingsRights = null
    isInsuranceDisplayServiceShow = false
    isPremiumTrackingPlatformControlShow = false
    roleName = null
    bannerImages: any[] = []
    fr_services = ['SEA_FFDR', 'AIR_FFDR', 'TRUK', 'WRHS', 'ASYER', 'INSR']
    alternateEmails: Array<string> = []
    socialLinks: any[] = []
    hasOptionalEmailTouched: boolean = false
    hasOptionalSocialTouched: boolean = false

    selectedBannerImage: any = null
    bannerText: string = null
    selectedProfileBannerTextColor: string = null
    bannerToggler: boolean = false
    public IsShowBannerText: boolean = false
    public IsProfileBannerTextColor: boolean = false
    associationArray: IAssociation[] = []
    associationToggler: boolean = false
    matchReg
    portalUrl: string = ''
    selCountry
    selCountryTel
    newPushedEmails = []
    isCitySearching: boolean = false
    hasCitySearchFailed: boolean = false
    hasCitySearchSuccess: boolean = false
    isBankSearching: boolean = false
    hasBankSearchFailed: boolean = false
    hasBankSearchSuccess: boolean = false


}