import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core'
import { JsonResponse } from '../../../../interfaces/JsonResponse'
import { BasicInfoService } from '../../user-creation/basic-info/basic-info.service'
import { ToastrService } from 'ngx-toastr'
import { DocumentFile } from '../../../../interfaces/document.interface'
import { baseExternalAssets } from '../../../../constants/base.url'
import { NgFilesService, NgFilesStatus, NgFilesSelected } from '../../../../directives/ng-files'
import { SettingService } from './setting.service'
import {
  EMAIL_REGEX, isJSON, loading, GEN_URL, patternValidator, FACEBOOK_REGEX,
  TWITTER_REGEX, INSTAGRAM_REGEX, LINKEDIN_REGEX, YOUTUBE_REGEX,
  ValidateEmail, getImagePath, ImageRequiredSize, ImageSource, getLoggedUserData,
  getPhoneNumber, isArrayValid, getAccessRights, PASSWORD_REGEX, validatePassword
} from '../../../../constants/globalFunctions'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { Observable } from 'rxjs'
import { debounceTime, map } from 'rxjs/operators'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { SharedService } from '../../../../services/shared.service'
import { ConfirmDeleteDialogComponent } from '../../../../shared/dialogues/confirm-delete-dialog/confirm-delete-dialog.component'
import { untilDestroyed } from 'ngx-take-until-destroy'
import { CommonService } from '../../../../services/common.service'
import { of } from 'rxjs/observable/of'
import { isNumber } from '../../../../shared/dialogues/sea-rate-dialog/sea-rate-dialog.component'
import { ConfirmDialogContent, ConfirmDialogGenComponent } from '../../../../shared/dialogues/confirm-dialog-generic/confirm-dialog-generic.component'
import { NewsDialogComponent } from '../../../../shared/dialogues/news-dialog/news-dialog.component'
import { cloneObject } from '../reports/reports.component'
import { ProviderOfficeDialogComponent } from '../../../../shared/dialogues/provider-office-dialog/provider-office-dialog.component'
import {
  AssociationModel, FreightModel, IAddressInfo, IBankInfo, IEmailNotif, IEmailNotifResp,
  IIndustry, INews, IProviderConfig, IRateNotification, IStatistic
} from '../../../../interfaces'
import { SettingsHelper } from './settings.helper'
@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent extends SettingsHelper implements OnInit, OnDestroy {

  newPasswordStatus = {
    lengthValid: false, upperCaseValid: false, lowerCaseValid: false,
    digitValid: false, specialCharValid: false
  }
  showPassValidator = false
  newPass

  constructor(
    private modalService: NgbModal,
    private _basicInfoService: BasicInfoService,
    private _toastr: ToastrService,
    private ngFilesService: NgFilesService,
    private _settingService: SettingService,
    private _sharedService: SharedService,
    private _commonService: CommonService,
  ) {
    super()
  }

  ngOnInit() {
    this.userProfile = getLoggedUserData()
    this.getActionRights()
    try {
      this.roleName = getAccessRights().RoleName
    } catch {
      this.roleName = null
    }
    this.ngFilesService.addConfig(this.config, 'config')
    try { localStorage.removeItem('gaChartData') } catch { }
    this.credentialInfoForm = new FormGroup({
      currentPassword: new FormControl(null, { validators: [Validators.required, Validators.maxLength(40)], updateOn: 'blur' }),
      newPassword: new FormControl(null, { validators: [Validators.required, Validators.minLength(8), Validators.maxLength(40), Validators.pattern(PASSWORD_REGEX)], updateOn: 'blur' }),
      confirmPassword: new FormControl('', { validators: [Validators.required, Validators.minLength(8), Validators.maxLength(40)], updateOn: 'blur' }),
    })
    this.personalInfoForm = new FormGroup({
      firstName: new FormControl(null, [Validators.required, Validators.pattern(/[a-zA-Z-][a-zA-Z -]*$/), Validators.minLength(2), Validators.maxLength(100)]),
      lastName: new FormControl(null, [Validators.required, Validators.pattern(/[a-zA-Z-][a-zA-Z -]*$/), Validators.minLength(2), Validators.maxLength(100)]),
      jobTitle: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]),
      city: new FormControl(null, [Validators.required, Validators.maxLength(100), Validators.minLength(3)]),
      currency: new FormControl(null, [Validators.required, Validators.maxLength(5), Validators.minLength(2)]),
      region: new FormControl(null, [Validators.maxLength(50), Validators.minLength(3)]),
      email: new FormControl(null, [Validators.required, Validators.pattern(EMAIL_REGEX), Validators.maxLength(320)]),
      mobile: new FormControl(null, [Validators.required, Validators.minLength(7), Validators.maxLength(13)]),
    })

    this.bankInfoForm = new FormGroup({
      bankName: new FormControl(null, [Validators.required]),
      accountTitle: new FormControl(null, [Validators.required, Validators.maxLength(150), Validators.minLength(10)]),
      accountNumber: new FormControl(null, [Validators.required, Validators.maxLength(30), Validators.minLength(10)]),
      branchName: new FormControl(null, [Validators.required, Validators.maxLength(100), Validators.minLength(4)]),
      branchAddress: new FormControl(null, [Validators.required, Validators.maxLength(500), Validators.minLength(10)]),
      branchCity: new FormControl(null, [Validators.required, Validators.maxLength(100), Validators.minLength(1)]),
    })

    this.mailNoficiationForm = new FormGroup({})

    this.businessInfoForm = new FormGroup({
      orgName: new FormControl(null, [Validators.required, Validators.maxLength(100), Validators.minLength(4)]),
      phone: new FormControl(null, [Validators.required, Validators.pattern(/^(?!(\d)\1+(?:\1+){0}$)\d+(\d+){0}$/), Validators.minLength(7), Validators.maxLength(13)]),
      address: new FormControl(null, [Validators.required, Validators.maxLength(200), Validators.minLength(10)]),
      city: new FormControl(null, [Validators.required, Validators.maxLength(100), Validators.minLength(3)]),
      poBoxNo: new FormControl(null, [Validators.maxLength(16), Validators.minLength(4)]),
      business_email: new FormControl(null, [
        Validators.required,
        Validators.pattern(EMAIL_REGEX)
      ]),
      alternateEmails_field: new FormControl(null),
    })

    this.socialMediaForm = new FormGroup({
      linkedInUrl: new FormControl(null, [Validators.pattern(LINKEDIN_REGEX)]),
      fbUrl: new FormControl(null, [Validators.pattern(FACEBOOK_REGEX)]),
      twitterUrl: new FormControl(null, [Validators.pattern(TWITTER_REGEX)]),
      instaUrl: new FormControl(null, [Validators.pattern(INSTAGRAM_REGEX)]),
      youtubeUrl: new FormControl(null, [Validators.pattern(YOUTUBE_REGEX)]),
      webUrl: new FormControl(null, [Validators.pattern(GEN_URL)]),
      profileUrl: new FormControl(null),
    })

    this.homePageForm = new FormGroup({
      marketingTagLineHeading: new FormControl(null, [Validators.required, Validators.maxLength(100)]),
      marketingTagLineContent: new FormControl(null, [Validators.required, Validators.maxLength(200)]),
    })


    this.getUserDetail(this.userProfile.UserID)
    this.onChanges()
  }

  getActionRights() {
    const accRights = getAccessRights()
    this._commonService.getRoleActions(accRights.RoleID, 'SETTINGS', 'PROVIDER', 'MENU').subscribe((res: JsonResponse) => {
      this.accessRights = (res.returnId > 0) ? res.returnObject : null
    })
  }

  getNotificationEmail() {
    this.listenNotifChanges = false
    this._settingService.getNotificationEmails(this.info.ProviderID).subscribe((res: JsonResponse) => {
      if (res.returnId > 0) {
        this.apiServerEmails = res.returnObject
        this.setMailNotifs()
      }
    })
  }

  validatePhoneNumber() {
    try {
      const _value = this.businessInfoForm.get('phone').value
      this.businessInfoForm.controls['phone'].patchValue(getPhoneNumber(_value))
    } catch { }
  }

  errorMessagesPersonalInfo() {
    try {
      const _value = this.personalInfoForm.get('mobile').value
      this.personalInfoForm.controls['mobile'].patchValue(getPhoneNumber(_value))
    } catch { }

    if (this.personalInfoForm.controls.firstName.touched && this.personalInfoForm.controls.firstName.status === "INVALID") {
      this.firstName_error = true
    }
    if (this.personalInfoForm.controls.lastName.touched && this.personalInfoForm.controls.lastName.status === "INVALID") {
      this.lastName_error = true
    }

    if (this.personalInfoForm.controls.jobTitle.touched && this.personalInfoForm.controls.jobTitle.status === "INVALID") {
      this.jobTitle_error = true
    }

    if (this.personalInfoForm.controls.currency.touched && this.personalInfoForm.controls.currency.status === "INVALID") {
      this.currency_error = true
    }

    if (this.personalInfoForm.controls.region.touched && this.personalInfoForm.controls.region.status === "INVALID") {
      this.region_error = true
    }

    if (this.personalInfoForm.controls.mobile.touched && this.personalInfoForm.controls.mobile.status === "INVALID") {
      this.mobile_error = true
    }

    try {
      if (this.personalInfoForm.controls.city.touched && (!this.personalInfoForm.controls.city || !this.personalInfoForm.controls.city.id)) {
        this.city_error = true
      }
    } catch { }
  }

  errorMessageBusiness() {
    if (this.businessInfoForm.controls.business_email.touched && this.businessInfoForm.controls.business_email.status === "INVALID") {
      this.business_email_error = true
    }
    if (this.businessInfoForm.controls.phone.touched && this.businessInfoForm.controls.phone.status === "INVALID") {
      this.phone_error = true
    }
    if (this.businessInfoForm.controls.phone.touched && this.businessInfoForm.controls.phone.status === "INVALID") {
      this.phone_error = true
    }
    if (this.businessInfoForm.controls.address.touched && this.businessInfoForm.controls.address.status === "INVALID") {
      this.address_error = true
    }
    try {
      if (this.businessInfoForm.controls.city.touched && (!this.businessInfoForm.controls.city || !this.businessInfoForm.controls.city.id)) {
        this.b_city_error = true
      }
    } catch { }
  }

  onTabChange($change) {
    this.resetPasswordForm()
  }

  getchBankInfo() {
    this._settingService.getProviderBank(this.userProfile.ProviderID).subscribe((res: JsonResponse) => {
      if (res.returnId > 0) {
        this.savedBankInfo = res.returnObject
        this.setbankInfo()
      }
    })
  }

  setbankInfo() {
    try {
      const { savedBankInfo } = this
      this.bankInfoForm.controls['accountTitle'].setValue(savedBankInfo.accountTitle)
      this.bankInfoForm.controls['accountNumber'].setValue(savedBankInfo.accountNo)
      this.bankInfoForm.controls['branchName'].setValue(savedBankInfo.branchName)
      this.bankInfoForm.controls['branchAddress'].setValue(savedBankInfo.branchAddress)

      this.bankInfoForm.controls['bankName'].setValue(savedBankInfo.bankName)
      try {
        const _branchCityDetail = JSON.parse(savedBankInfo.branchCityDetail)
        const obj = {
          id: _branchCityDetail.ID,
          code: _branchCityDetail.Code,
          title: _branchCityDetail.Title,
          shortName: _branchCityDetail.ShortName,
          imageName: _branchCityDetail.ImageName,
          desc: _branchCityDetail.Desc,
          webURL: _branchCityDetail.WebURL,
          sortingOrder: _branchCityDetail.SortingOrder,
          type: _branchCityDetail.Type,
          lastUpdate: _branchCityDetail.LastUpdate,
          latitude: _branchCityDetail.Latitude,
          longitude: _branchCityDetail.Longitude,
          isBaseCurrency: _branchCityDetail.IsBaseCurrency,
          jsonServices: _branchCityDetail.JsonServices,
          isChinaGateway: _branchCityDetail.IsChinaGateway,
          shortPortCode: _branchCityDetail.ShortPortCode,
          portCountryCode: _branchCityDetail.PortCountryCode,
          roundingOff: _branchCityDetail.RoundingOff,
        }
        this.bankInfoForm.controls['branchCity'].setValue(obj)
      } catch (error) {

      }

      try {
        const _bankDetail = JSON.parse(savedBankInfo.bankDetail)
        const _bankObj = {
          id: _bankDetail.ID,
          code: _bankDetail.Code,
          title: _bankDetail.Title,
          shortName: _bankDetail.ShortName,
          imageName: _bankDetail.ImageName,
          desc: _bankDetail.Desc,
          webURL: _bankDetail.WebURL,
          sortingOrder: _bankDetail.SortingOrder,
          type: _bankDetail.Type,
          lastUpdate: _bankDetail.LastUpdate,
          latitude: _bankDetail.Latitude,
          longitude: _bankDetail.Longitude,
          isBaseCurrency: _bankDetail.IsBaseCurrency,
          jsonServices: _bankDetail.JsonServices,
          isChinaGateway: _bankDetail.IsChinaGateway,
          shortPortCode: _bankDetail.ShortPortCode,
          portCountryCode: _bankDetail.PortCountryCode,
          roundingOff: _bankDetail.RoundingOff,
        }
        this.bankInfoForm.controls['bankName'].setValue(_bankObj)

      } catch {

      }
    } catch (error) {
      console.log(error)
    }
    this.bankInfoToggler = false
  }

  resetBankInfo() {
    this.bankName_error = false
    this.accountTitle_error = false
    this.accountNumber_error = false
    this.branchName_error = false
    this.branchAddress_error = false
    this.branchCity_error = false
    this.bankInfoForm.reset()
    this.setbankInfo()
  }

  onSelect = ($event) => setTimeout(() => { this.errorMessages() }, 10)

  errorMessages() {
    const _bankValue = this.bankInfoForm.controls.bankName.value
    if (this.bankInfoForm.controls.bankName.status == "INVALID" && this.bankInfoForm.controls.bankName.dirty) {
      this.bankName_error = (_bankValue && typeof _bankValue === 'object' && _bankValue.id) ? false : true
    } else if (this.bankInfoForm.controls.bankName.dirty) {
      this.bankName_error = (_bankValue && typeof _bankValue === 'object' && _bankValue.id) ? false : true
    }
    try {
      if (this.bankInfoForm.controls.accountTitle.status === "INVALID" && this.bankInfoForm.controls.accountTitle.touched)
        this.accountTitle_error = true
    } catch { }
    try {
      if (this.bankInfoForm.controls.accountNumber.status === "INVALID" && this.bankInfoForm.controls.accountNumber.touched)
        this.accountNumber_error = true
    } catch { }
    try {
      if (this.bankInfoForm.controls.branchName.status === "INVALID" && this.bankInfoForm.controls.branchName.touched)
        this.branchName_error = true
    } catch { }
    try {
      if (this.bankInfoForm.controls.branchAddress.status === "INVALID" && this.bankInfoForm.controls.branchAddress.touched)
        this.branchAddress_error = true
    } catch { }
    const _cityValue = this.bankInfoForm.controls.branchCity.value
    if (this.bankInfoForm.controls.branchCity.status == "INVALID" && this.bankInfoForm.controls.branchCity.dirty) {
      this.branchCity_error = (_cityValue && typeof _cityValue === 'object' && _cityValue.id) ? false : true
    } else if (this.bankInfoForm.controls.branchCity.dirty) {
      this.branchCity_error = (_cityValue && typeof _cityValue === 'object' && _cityValue.id) ? false : true
    }
  }

  errorMessageHomePage(touched: boolean) {
    try {
      if (touched) {
        if (this.homePageForm.controls.marketingTagLineHeading.status === "INVALID" &&
          this.homePageForm.controls.marketingTagLineHeading.touched)
          this.marketingTagLineHeadingError = true
      } else {
        if (this.homePageForm.controls.marketingTagLineHeading.status === "INVALID")
          this.marketingTagLineHeadingError = true
      }
    } catch { }
    try {
      if (touched) {
        if (this.homePageForm.controls.marketingTagLineContent.status === "INVALID" &&
          this.homePageForm.controls.marketingTagLineContent.touched)
          this.marketingTagLineContentError = true
      } else {
        if (this.homePageForm.controls.marketingTagLineContent.status === "INVALID")
          this.marketingTagLineContentError = true
      }
    } catch { }
  }

  socialError() {
    if (this.socialMediaForm.controls.linkedInUrl.status === "INVALID" && this.socialMediaForm.controls.linkedInUrl.touched)
      this.linkedInUrl_error = true
    if (this.socialMediaForm.controls.fbUrl.status === "INVALID" && this.socialMediaForm.controls.fbUrl.touched)
      this.fbUrl_error = true
    if (this.socialMediaForm.controls.twitterUrl.status === "INVALID" && this.socialMediaForm.controls.twitterUrl.touched)
      this.twitterUrl_error = true
    if (this.socialMediaForm.controls.instaUrl.status === "INVALID" && this.socialMediaForm.controls.instaUrl.touched)
      this.instaUrl_error = true
    if (this.socialMediaForm.controls.youtubeUrl.status === "INVALID" && this.socialMediaForm.controls.youtubeUrl.touched)
      this.youtubeUrl_error = true
    if (this.socialMediaForm.controls.webUrl.status === "INVALID" && this.socialMediaForm.controls.webUrl.touched)
      this.webUrl_error = true
  }

  opneSocialLink(url) {
    if (url) {
      let _url = url
      if (!(_url.includes('http')))
        _url = 'http://' + url
      window.open(_url, '_blank')
    }
  }

  onChanges(): void {
    this.personalInfoForm.valueChanges.subscribe(val => {
      for (const key in this.personalInfoForm.controls) {
        if (this.personalInfoForm.controls.hasOwnProperty(key)) {
          if (this.personalInfoForm.controls[key].dirty)
            this.personalInfoToggler = true
        }
      }
    })
    this.socialMediaForm.valueChanges.subscribe(() => {
      for (const key in this.socialMediaForm.controls) {
        if (this.socialMediaForm.controls.hasOwnProperty(key) && this.listenSocialChanges)
          if (this.socialMediaForm.controls[key].dirty)
            this.socialToggler = true
      }
    })
    this.homePageForm.valueChanges.subscribe(() => {
      for (const key in this.homePageForm.controls) {
        if (this.homePageForm.controls.hasOwnProperty(key) && this.listenHomeChanges)
          if (this.homePageForm.controls[key].dirty)
            this.hompageToggler = true
      }
    })

    this.bankInfoForm.valueChanges.subscribe(() => {
      for (const key in this.bankInfoForm.controls) {
        if (this.bankInfoForm.controls.hasOwnProperty(key))
          if (this.bankInfoForm.controls[key].dirty)
            this.bankInfoToggler = true
      }
    })
    this.businessInfoForm.valueChanges.subscribe(() => {
      for (const key in this.businessInfoForm.controls) {
        if (this.businessInfoForm.controls.hasOwnProperty(key) && this.listenBusinessChanges)
          if (this.businessInfoForm.controls[key].dirty)
            this.businessInfoToggler = true
      }
    })

    this.businessInfoForm.controls['alternateEmails_field'].valueChanges.pipe(untilDestroyed(this)).subscribe(value => {
      if (!value && this.newPushedEmails.length === 0)
        this.hasOptionalEmailTouched = false
      this.businessInfoToggler = true
    })

    // this.credentialInfoForm.controls['newPassword'].valueChanges.pipe(untilDestroyed(this)).subscribe(_value => {
    //   if (_value) {
    //     this.onNewPassChange(_value)
    //   }
    // })

    this.credentialInfoForm.valueChanges.subscribe((data) => {
      console.log(data)
      console.log(this.credentialInfoForm)
    })

    // this.credentialInfoForm.get('newPassword').valueChanges.subscribe(val => {
    //   //compare value and set other control value
    //   console.log('val', val)
    // })

  }

  onNewPassChange(_value) {
    console.log('new pass:', _value)
    this.credentialInfoForm.controls['newPassword'].setValue(_value)
    this.newPasswordStatus = validatePassword(_value)
    console.log(this.newPasswordStatus)
    this.showPassValidator = true
  }

  onContentChanged = ({ quill, html, text }) => this.editorContent = html

  selectedSocialLink(obj) {
    this.selectedSocialsite = obj
    this.businessInfoForm.controls['socialUrl'].reset()
    if (obj.socialMediaPortalsID === 100) {
      this.businessInfoForm.controls['socialUrl'].setValidators([patternValidator(FACEBOOK_REGEX)])
      this.socialLinkValidate()
    } else if (obj.socialMediaPortalsID === 101) {
      this.businessInfoForm.controls['socialUrl'].setValidators([patternValidator(TWITTER_REGEX)])
      this.socialLinkValidate()
    } else if (obj.socialMediaPortalsID === 102) {
      this.businessInfoForm.controls['socialUrl'].setValidators([patternValidator(INSTAGRAM_REGEX)])
      this.socialLinkValidate()
    } else if (obj.socialMediaPortalsID === 103) {
      this.businessInfoForm.controls['socialUrl'].setValidators([patternValidator(LINKEDIN_REGEX)])
      this.socialLinkValidate()
    } else if (obj.socialMediaPortalsID === 104) {
      this.businessInfoForm.controls['socialUrl'].setValidators([patternValidator(YOUTUBE_REGEX)])
      this.socialLinkValidate()
    } else {
      this.socialLinkValidate()
    }
  }

  socialLinkValidate() {
    let isValid = true
    if (this.selectedSocialsite && this.businessInfoForm.controls['socialUrl'].value && this.businessInfoForm.controls['socialUrl'].status === 'INVALID') {
      this.socialInputValidate = 'Your social url is not valid'
      if (!this) { this._toastr.warning('Your social url is not valid') }
      isValid = false
    } else if (!this.businessInfoForm.controls['socialUrl'].value) {
      this.socialInputValidate = ''
    } else {
      this.socialInputValidate = ''
    }
    return isValid
  }

  socialLinkValidate2($text) { }

  getCities(info) {
    this.getRegions()
    this.getCountries()
    this.getCurrencies()
    this.setPersonalInfo(info)
    try { this.setProfileSettings(info) } catch (error) { console.warn(error) }
    loading(false)
  }

  getRegions() {
    this._sharedService.regionList.subscribe((state: any) => {
      if (state)
        this.regionList = state
    })
  }

  getCountries() {
    this._sharedService.countryList.subscribe((state: any) => {
      if (state)
        this.countryList = state
    })
  }

  getCurrencies() {
    this._sharedService.currencyList.subscribe((state: any) => {
      if (state)
        this.currencyList = state
    })
  }

  getListJobTitle(info) {
    this._basicInfoService.getjobTitles(info.CountryID).subscribe((res: JsonResponse) => {
      if (res.returnId > 0) {
        this.jobTitles = res.returnObject
        this.getCities(info)
      }
    })
  }

  showToggler() {
    const elem = this.editContainer as any
    if (elem.nativeElement.classList.contains('showMore')) {
      elem.nativeElement.classList.remove('showMore')
      this.showTogglerText = "Show Less"
    } else {
      elem.nativeElement.classList.add('showMore')
      this.showTogglerText = "Show More"
    }
  }

  getUserDetail(UserID) {
    loading(true)
    this._settingService.getSettingInfo(UserID).subscribe((res: JsonResponse) => {
      if (res.returnId > 0) {
        this.info = res.returnObject
        this.getNotificationEmail()
        this.setProfileBannerSettings()
        try {
          this.emailNotif = cloneObject(this.info.RateExpiryNotificationSettings)
        } catch { }
        try {
          if (this.userProfile.ProviderNatureBL && (this.userProfile.ProviderNatureBL === 'SDF'))
            this.showProfileURL = false
        } catch { }

        setTimeout(() => {
          try {
            this.notifArray = cloneObject(this.info.RateExpirySettings)
          } catch { }
        }, 0)

        try {
          this._settingService.getProviderNewsList(this.info.ProviderID).subscribe((res: JsonResponse) => {
            if (res.returnId > 0)
              this.newsList = res.returnObject
          })
        } catch { }

        try {
          this.IsShowOfficeOnProfile = this.info.IsShowOfficeOnProfile
          this.preventOfficeLocation = this.IsShowOfficeOnProfile
        } catch { }

        try {
          this.IsShowServiceOnProfile = this.info.IsShowServiceOnProfile
          this.preventOfferServices = this.IsShowServiceOnProfile
        } catch { }

        try {
          this.IsShowNewsOnProfile = this.info.IsShowNewsOnProfile
          this.preventNewsAcc = this.IsShowNewsOnProfile
        } catch { }

        try { this.IsMarketingTagLineRequired = this.info.IsMarketingTagLineRequired } catch { }
        try { this.IsShowSearchBoxOnProfile = this.info.IsShowSearchBoxOnProfile } catch { }

        try {
          if (this.info.DefaultTaglineBackGroundColor && this.info.TaglineBackGroundColor) {
            this.IsTaglineColor = true
            this.selectedTaglineColor = this.info.TaglineBackGroundColor
          } else {
            this.IsTaglineColor = false
            this.selectedTaglineColor = this.info.DefaultTaglineBackGroundColor
          }
        } catch { }

        try {
          if (this.info.DefaultIndustryIconBorderColor && this.info.IndustryIconBorderColor) {
            this.isIndustryColor = true
            this.industryColor = this.info.IndustryIconBorderColor
          } else {
            this.isIndustryColor = false
            this.industryColor = this.info.DefaultIndustryIconBorderColor
          }
        } catch { }

        try {
          if (this.info.DefaultFooterStripColor && this.info.FooterStripColor) {
            this.isFooterColor = true
            this.footerColor = this.info.FooterStripColor
          } else {
            this.isFooterColor = false
            this.footerColor = this.info.DefaultFooterStripColor
          }
        } catch { }

        try {
          if (this.info.DefaultStatisticBackGroundColor && this.info.StatisticBackGroundColor) {
            this.IsStatisticColor = true
            this.selectedStatisticsColor = this.info.StatisticBackGroundColor
          } else {
            this.IsStatisticColor = false
            this.selectedStatisticsColor = this.info.DefaultStatisticBackGroundColor
          }
        } catch { }

        this.setOfficeColor()
        this._settingService.getProviderOfficeList(this.userProfile.ProviderID).subscribe((res: JsonResponse) => {
          if (res.returnId > 0)
            this.officeAddressList = res.returnObject
        })

        if (this.info.IsBankInfoRequired)
          this.getchBankInfo()
        try { this.industriesSolution = cloneObject(this.info.IndustrySettings) } catch { }

        try {
          if (this.info.CompanyNamePlacementOnProfile === 'hide') {
            this.IsShowCompanyLogoOnProfile = false
            this.profileLogoCenter = false
            this.profileLogoLeft = false
          } else {
            this.IsShowCompanyLogoOnProfile = true
            if (this.info.CompanyNamePlacementOnProfile === 'center') {
              this.profileLogoCenter = true
              this.profileLogoLeft = false
            } else {
              this.profileLogoCenter = false
              this.profileLogoLeft = true
            }
          }
        } catch { }

        try {
          const _statisticsArray = this.info.StatisticsSettings
          this.statisticsArray = _statisticsArray.map(_stat => ({
            ..._stat, MinValue: parseInt(_stat.MinValue), MaxValue: parseInt(_stat.MaxValue)
          }))
        } catch { }

        this.socialWebs = this.info.SocialMedia
        const _allAssociations: AssociationModel[] = cloneObject(this.info.Association)
        const _allAssn = _allAssociations.map((_aff: AssociationModel) => ({
          ..._aff,
          IsValidRef: _aff.AssnWithRefNo ? true : false,
          IsValidName: _aff.AssnWithName ? true : false,
          IsValidWeb: _aff.AssnWithWebAdd ? true : false,
          IsEdit: false,
          IsAdded: false,
          AssInpName: _aff.AssnWithName,
        }))

        this.masterAssociations = _allAssn
        this.allAssociations = _allAssn.filter(_assn => _assn.IsSelected)

        this.galleriesDocx = this.info.Gallery
        this.marketGalleryDocx = this.info.MarketingGallery
        this.certficateDocx = this.info.AwardCertificate
        this.companyLogoDocx = this.info.CompanyLogo
        this.profileLogoDocx = this.info.ProfileLogo
        this.liscenceDocx = this.info.TradeLicense
        this.profileBannerDocx = this.info.ProfileBanner
        this.serverImageDocx = this.info.LogisticServiceDoc

        this.valAddedServices = this.info.ValueAddedServices
        this.valueService = this.valAddedServices.filter(obj => obj.IsSelected && obj.ProvLogServID)
        const _freightServices: FreightModel[] = this.info.LogisticService
        try {
          this.freightServices = _freightServices.map(_freight => {
            let _imgArr = null
            if (_freight.LogServImages && _freight.LogServImages.length > 0) {
              const _parsedArray = JSON.parse(_freight.LogServImages)
              _imgArr = _parsedArray.map(_servDoc => { return _servDoc.DocumentFile })
            }
            return {
              ..._freight,
              LogServImagesArr: _imgArr,
              isUploadOpen: (_freight.IsSelected && (!_imgArr || _imgArr.length === 0)) ? true : false,
            }
          })
        } catch (error) {
          this.freightServices = _freightServices
        }

        this.setSocialLinks()
        this.frtService = this.freightServices.filter(obj => obj.IsSelected && obj.ProvLogServID)
        this.setFreightServices()
        if (this.info.About) {
          this.editorContent = this.info.About
          this.editable = false
          if (this.editorContent.length > this.showMoreTextLength) {
            const elem = this.editContainer as any
            if (elem && elem.nativeElement.classList.contains('showMore')) {
              elem.nativeElement.classList.remove('showMore')
              this.showTogglerText = "Show Less"
            } else {
              if (elem) {
                elem.nativeElement.classList.add('showMore')
                this.showTogglerText = "Show More"
              }
            }
          }
        } else {
          this.editable = true
        }

        if (this.info.UploadedCompanyLogo && this.info.UploadedCompanyLogo[0].DocumentFileName &&
          this.info.UploadedCompanyLogo[0].DocumentFileName != "[]" &&
          isJSON(this.info.UploadedCompanyLogo[0].DocumentFileName)) {
          let logo = this.info.UploadedCompanyLogo[0]
          this.uploadedlogo = JSON.parse(logo.DocumentFileName)[0]
          this.docTypeIdLogo = this.uploadedlogo.DocumentID
          this.uploadedlogo.DocumentFile = this.baseExternalAssets + this.uploadedlogo.DocumentFile
        }
        if (this.info.UploadedProfileLogo && this.info.UploadedProfileLogo[0].DocumentFileName &&
          this.info.UploadedProfileLogo[0].DocumentFileName != "[]" &&
          isJSON(this.info.UploadedProfileLogo[0].DocumentFileName)) {
          let logo = this.info.UploadedProfileLogo[0]
          this.uploadedProfileLogo = JSON.parse(logo.DocumentFileName)[0]
          this.docTypeIdProfileLogo = this.uploadedProfileLogo.DocumentID
          this.uploadedProfileLogo.DocumentFile = this.baseExternalAssets + this.uploadedProfileLogo.DocumentFile
        }
        if (this.info.UploadedGallery && this.info.UploadedGallery[0].DocumentFileName &&
          this.info.UploadedGallery[0].DocumentFileName != "[]" &&
          isJSON(this.info.UploadedGallery[0].DocumentFileName)) {
          let gallery = this.info.UploadedGallery[0]
          this.uploadedGalleries = JSON.parse(gallery.DocumentFileName)
          this.docTypeIdGallery = this.uploadedGalleries[0].DocumentID
          this.uploadedGalleries.map(obj => {
            obj.DocumentFile = this.baseExternalAssets + obj.DocumentFile
          })
        }
        if (this.info.UploadedMarketingGallery && this.info.UploadedMarketingGallery[0].DocumentFileName &&
          this.info.UploadedMarketingGallery[0].DocumentFileName != "[]" &&
          isJSON(this.info.UploadedMarketingGallery[0].DocumentFileName)) {
          let gallery = this.info.UploadedMarketingGallery[0]
          this.uploadedMarketGallery = JSON.parse(gallery.DocumentFileName)
          this.docTypeIdMarketGallery = this.uploadedMarketGallery[0].DocumentID
          this.uploadedMarketGallery.map(obj => {
            obj.DocumentFile = this.baseExternalAssets + obj.DocumentFile
          })
        }

        if (this.info.UploadedAwardCertificate && this.info.UploadedAwardCertificate[0].DocumentFileName &&
          this.info.UploadedAwardCertificate[0].DocumentFileName != "[]" &&
          isJSON(this.info.UploadedAwardCertificate[0].DocumentFileName)) {
          let certificate = this.info.UploadedAwardCertificate[0]
          this.uploadedCertificates = JSON.parse(certificate.DocumentFileName)
          this.docTypeIdCert = this.uploadedCertificates[0].DocumentID
          this.uploadedCertificates.map(obj => {
            obj.DocumentFile = this.baseExternalAssets + obj.DocumentFile
          })
        }
        if (this.info.UploadedTradeLicense && this.info.UploadedTradeLicense[0].DocumentFileName &&
          this.info.UploadedTradeLicense[0].DocumentFileName != "[]" &&
          isJSON(this.info.UploadedTradeLicense[0].DocumentFileName)) {
          let tradeLiscence = this.info.UploadedTradeLicense[0]
          this.uploadedLiscence = JSON.parse(tradeLiscence.DocumentFileName)
          this.docTypeIdLiscence = this.uploadedLiscence[0].DocumentID
          this.uploadedLiscence.map(obj => {
            obj.DocumentFile = this.baseExternalAssets + obj.DocumentFile
          })
        }
        if (this.info.UploadedLogisticServiceDoc && this.info.UploadedLogisticServiceDoc.length > 0) {
          this.info.UploadedLogisticServiceDoc.forEach(_seriviceDoc => {
            const { DocumentFileName, LogServID } = _seriviceDoc
            if (DocumentFileName !== "[]" && isJSON(DocumentFileName)) {
              this.freightServices.forEach(_freight => {
                try {
                  const _parsedImage = JSON.parse(DocumentFileName)[0].DocumentFile
                  if (_freight.ProvLogServID === LogServID) {
                    if (_freight.LogServImagesArr && _freight.LogServImagesArr.length > 0) {
                      _freight.LogServImagesArr.push(_parsedImage)
                    } else {
                      _freight.LogServImagesArr = [_parsedImage]
                    }
                  }
                } catch (error) {
                  console.log(error)
                }
              })
            }
          })
        }
        this.getListJobTitle(this.info)
      } else {
        this._toastr.error(res.returnText, res.returnStatus)
        loading(false)
      }
    }, () => {
      loading(false)
      this._toastr.error('There was an erroer while processing your request, Please try again later', "Failed")
    })
  }

  setOfficeColor() {
    this.officeToggler = false
    try {
      if (this.info.DefaultOfficePinBorderColor && this.info.OfficePinBorderColor) {
        this.IsShowOfficePinBorderColor = true
        this.officePinBorderColor = this.info.OfficePinBorderColor
      } else {
        this.IsShowOfficePinBorderColor = false
        this.officePinBorderColor = this.info.DefaultOfficePinBorderColor
      }
    } catch (error) {
      console.log(error)
    }
  }

  setProfileBannerSettings() {
    try {
      if (this.info.BannerSettings) {
        const { BannerSettings } = this.info
        this.bannerImages = JSON.parse(BannerSettings.BannerImages).filter(_image => !_image.DocumnetID)
        this.selectedBannerImage = {
          DocumentFile: BannerSettings.BannerSelectedImage,
          DocumentID: BannerSettings.BannerSelectedImageID
        }
        this.bannerText = this.info.BannerText
        this.bannerToggler = false
        this.IsShowBannerText = this.info.IsShowBannerText
        if (this.info.DefaultBannerTextColor && this.info.BannerTextColor) {
          this.IsProfileBannerTextColor = true
          this.selectedProfileBannerTextColor = this.info.BannerTextColor
        } else {
          this.IsProfileBannerTextColor = false
          this.selectedProfileBannerTextColor = this.info.DefaultBannerTextColor
        }
      } else {
        this.bannerText = null
        this.bannerToggler = false
        this.IsShowBannerText = false
        this.IsProfileBannerTextColor = false
        this.selectedProfileBannerTextColor = this.info.DefaultBannerTextColor
      }
    } catch (error) {
      console.log('Banner Error:', error)
    }

    if (this.info.UploadedProfileBanner && this.info.UploadedProfileBanner[0].DocumentFileName &&
      this.info.UploadedProfileBanner[0].DocumentFileName != "[]" &&
      isJSON(this.info.UploadedProfileBanner[0].DocumentFileName)) {
      let gallery = this.info.UploadedProfileBanner[0]
      this.uploadedProfileBanner = JSON.parse(gallery.DocumentFileName)
      const _parsedBannerImage = JSON.parse(gallery.DocumentFileName)
      const _bannerImage = _parsedBannerImage[_parsedBannerImage.length - 1]
      this.bannerImages.push(_bannerImage)

      this.docTypeIdProfileBanner = this.uploadedProfileBanner[0].DocumentID
      this.uploadedProfileBanner.map(obj => {
        obj.DocumentFile = this.baseExternalAssets + obj.DocumentFile
      })
    }
  }

  setFreightServices() {
    if (this.frtService && this.frtService.length) {
      this.availModes = []
      try {
        let data = this.frtService.find(obj => obj.LogServCode == 'WRHS')
        if (data && Object.keys(data).length) {
          this.wareHouseTypeToggler = true
          this.IsRealEstate = data.IsRealEstate
          this.availModes.push('WAREHOUSE')
        }
        data = this.frtService.find(obj => obj.LogServCode === 'SEA_FFDR')
        if (data && Object.keys(data).length) {
          this.seaTypeToggler = true
          this.availModes.push('SEA')
        }
        data = this.frtService.find(obj => obj.LogServCode === 'AIR_FFDR')
        if (data && Object.keys(data).length) {
          this.airTypeToggler = true
          this.availModes.push('AIR')
        }
        data = this.frtService.find(obj => obj.LogServCode === 'TRUK')
        if (data && Object.keys(data).length) {
          this.groundTypeToggler = true
          this.availModes.push('TRUCK')
        }
        data = this.frtService.find(obj => obj.LogServCode === 'ASYER')
        if (data && Object.keys(data).length) {
          this.assayerToggler = true
        }
        data = this.frtService.find(obj => obj.LogServCode === 'INSR')
        if (data && Object.keys(data).length) {
          this.insuranceToggler = true
          this.availModes.push('INSURANCE')
        }
      } catch {
      }
    }
  }

  onLogoPosChange(event, from) {
    this.hompageToggler = true
    if (from === 'leftLogo') {
      this.profileLogoLeft = event
      this.profileLogoCenter = !event
    } else if (from === 'centerLogo') {
      this.profileLogoLeft = !event
      this.profileLogoCenter = event
    }
  }

  onValueChange = (rate, rate2) => this.hompageToggler = true

  updatePassword() {
    if (this.credentialInfoForm.value.currentPassword === this.credentialInfoForm.value.newPassword) {
      this._toastr.error('New password must be different from current password', '')
      return
    }
    const obj = {
      email: this.userProfile.LoginID,
      password: this.credentialInfoForm.value.newPassword,
      confirmPassword: this.credentialInfoForm.value.confirmPassword,
      currentPassword: this.credentialInfoForm.value.currentPassword
    }
    this._settingService.changePassword(obj).subscribe((res: JsonResponse) => {
      if (res.returnId > 0) {
        this.showPassValidator = false
        this.newPass = null
        this.credentialInfoForm.reset()
        this._toastr.success(res.returnText, '')
      } else {
        this._toastr.error(res.returnText, '')
      }
    })
  }

  async setPersonalInfo(info) {
    this.personalInfoForm.controls['email'].setValue(info.LoginID)
    this.personalInfoForm.controls['firstName'].setValue(info.FirstName)
    this.personalInfoForm.controls['lastName'].setValue(info.LastName)
    if (info.JobTitle) {
      const obj = this.jobTitles.find(elem => elem.baseLanguage == info.JobTitle)
      if (obj && Object.keys(obj).length) {
        this.personalInfoForm.controls['jobTitle'].setValue(obj)
      } else {
        this.personalInfoForm.controls['jobTitle'].setValue({ baseLanguage: info.JobTitle })
      }
    }
    if (info.UserCity) {
      try {
        const cityObj = (JSON.parse(info.UserCity))
        const obj = {
          id: cityObj.ID,
          code: cityObj.Code,
          title: cityObj.Title,
          shortName: cityObj.ShortName,
          imageName: cityObj.ImageName,
          desc: cityObj.Desc,
          webURL: cityObj.WebURL,
          sortingOrder: cityObj.SortingOrder,
          type: cityObj.Type,
          lastUpdate: cityObj.LastUpdate,
          latitude: cityObj.Latitude,
          longitude: cityObj.Longitude,
          isBaseCurrency: cityObj.IsBaseCurrency,
          jsonServices: cityObj.JsonServices,
          isChinaGateway: cityObj.IsChinaGateway,
          shortPortCode: cityObj.ShortPortCode,
          portCountryCode: cityObj.PortCountryCode,
          roundingOff: cityObj.RoundingOff,
        }
        this.personalInfoForm.controls['city'].setValue(obj)
      } catch (error) {
        console.log(error)
      }
    } else {
      this.personalInfoForm.controls['city'].setValue(null)
    }
    if (info.CurrencyID) {
      const obj = this.currencyList.find(elem => elem.id == info.CurrencyID)
      this.personalInfoForm.controls['currency'].setValue(obj)
    } else {
      this.personalInfoForm.controls['currency'].setValue(null)
    }

    if (info.RegionID) {
      let _regionList = []
      if (this.regionList && this.regionList.length > 0) {
        _regionList = this.regionList
      } else {
        const _regions = await this._commonService.getRegions().toPromise() as any
        this.regionList = _regions
        _regionList = _regions
      }
      try {
        const obj = _regionList.find(elem => elem.regionID == info.RegionID)
        this.personalInfoForm.controls['region'].setValue(obj.regionID)
      } catch (error) { }

    } else {
      this.personalInfoForm.controls['region'].setValue(null)
    }

    try {
      if (!this.countryList || this.countryList.length === 0) {
        const _countryList = await this._commonService.getCountry().toPromise() as any
        if (_countryList && _countryList.length) {
          this.countryList = _countryList.map((obj) => {
            if (typeof (obj.desc) == "string")
              obj.desc = JSON.parse(obj.desc)
          })
        }
      }
    } catch (error) { }

    try {
      let selectedCountry = this.countryList.find(obj => obj.id == info.PhoneCodeCountryID)
      if (selectedCountry && Object.keys(selectedCountry).length) {
        this.selectPhoneCode(selectedCountry)
      } else {
        selectedCountry = this.countryList.find(obj => obj.id == info.CountryID)
        if (selectedCountry && Object.keys(selectedCountry).length) {
          this.selectPhoneCode(selectedCountry)
        }
      }
      if (selectedCountry && typeof selectedCountry == 'object' && Object.keys(selectedCountry).length) {
        let description = selectedCountry.desc
        info.PrimaryPhone = info.PrimaryPhone.replace(description[0].CountryPhoneCode, "")
      }
    } catch (error) { }

    this.personalInfoForm.controls['mobile'].setValue(info.PrimaryPhone)
    this.personalInfoToggler = false
  }

  resetPersonalInfo() {
    this.personalInfoForm.reset()
    this.setPersonalInfo(this.info)
  }

  resetbusinessInfo() {
    this.businessInfoForm.reset()
    this.setBusinessInfo(this.info)
  }

  setBusinessInfo(info) {
    this.listenBusinessChanges = false
    this.hasOptionalEmailTouched = false
    this.hasOptionalSocialTouched = false
    this.newPushedEmails = []
    try {
      this.businessInfoForm.controls['orgName'].setValue(info.ProviderName)
      this.businessInfoForm.controls['address'].setValue(info.ProviderAddress)
      this.businessInfoForm.controls['poBoxNo'].setValue(info.POBox)
      this.businessInfoForm.controls['business_email'].setValue(info.BusinessEmail)
      this.portalUrl = info.ProfileID
    } catch (error) {
      console.log('businessInfoForm:', error)
    }

    if (info.AlternateEmails) {
      try {
        this.alternateEmails = info.AlternateEmails.split('').filter(email => (email && email.length > 1))
      } catch (error) {
        console.warn(error)
      }
    }
    if (info.ProviderCity) {
      try {
        const cityObj = (JSON.parse(info.ProviderCity))
        const obj = {
          id: cityObj.ID,
          code: cityObj.Code,
          title: cityObj.Title,
          shortName: cityObj.ShortName,
          imageName: cityObj.ImageName,
          desc: cityObj.Desc,
          webURL: cityObj.WebURL,
          sortingOrder: cityObj.SortingOrder,
          type: cityObj.Type,
          lastUpdate: cityObj.LastUpdate,
          latitude: cityObj.Latitude,
          longitude: cityObj.Longitude,
          isBaseCurrency: cityObj.IsBaseCurrency,
          jsonServices: cityObj.JsonServices,
          isChinaGateway: cityObj.IsChinaGateway,
          shortPortCode: cityObj.ShortPortCode,
          portCountryCode: cityObj.PortCountryCode,
          roundingOff: cityObj.RoundingOff,
        }
        this.businessInfoForm.controls['city'].setValue(obj)
      } catch (error) {
        console.log(error)
      }
    }
    let selectedCountry = this.countryList.find(obj => obj.id == info.ProviderPhoneCodeCountryID)
    if (selectedCountry && Object.keys(selectedCountry).length) {
      this.selectTelCode(selectedCountry)
    } else {
      selectedCountry = this.countryList.find(obj => obj.id == info.CountryID)
      if (selectedCountry && Object.keys(selectedCountry).length) {
        this.selectTelCode(selectedCountry)
      }
    }
    if (selectedCountry && typeof selectedCountry == 'object' && Object.keys(selectedCountry).length) {
      let description = selectedCountry.desc
      info.ProviderPhone = info.ProviderPhone.replace(description[0].CountryPhoneCode, "")
    }
    this.businessInfoForm.controls['phone'].setValue(info.ProviderPhone)
    this.businessInfoToggler = false
    this.listenBusinessChanges = true
  }

  setProfileSettings(info) {
    this.isPrivateMode = info.IsPrivateMode
    this.isMarketPlace = info.IsMarketPlace
    this.IsExclChargesAllow = info.IsExclChargesAllow
    this.IsCustomerApprovalRequired = info.IsCustomerApprovalRequired
    this.IsTrackingQualityRequired = info.IsTrackingQualityRequired
    this.IsShowWarehouseOnProfile = info.IsShowWarehouseOnProfile
    this.IsShowSeaRateOnProfile = info.IsShowSeaRateOnProfile
    this.IsShowSeaLCLRateOnProfile = info.IsShowSeaLCLRateOnProfile
    this.IsShowAirRateOnProfile = info.IsShowAirRateOnProfile
    this.IsShowGroundRateOnProfile = info.IsShowGroundRateOnProfile
    this.IsShowStatisticsOnProfile = info.IsShowStatisticsOnProfile
    try {
      this.isWHBookEndAlert = info.IsWHBookEndAlert
      this.wHBookEndAlertDays = info.WHBookEndAlertDays
    } catch (error) { }

    try {
      if (this.info.FeatureToggleJson) {
        const { FeatureToggleJson } = this.info
        const _controls: IProviderConfig = JSON.parse(FeatureToggleJson)
        if (_controls.IsSpotRateRquired)
          this.isSpotRateRquired = _controls.IsSpotRateRquired
        if (_controls.IsDisplayCommodityNameOnSearchBox)
          this.isDisplayCommodityNameOnSearchBox = _controls.IsDisplayCommodityNameOnSearchBox
        if (_controls.IsInsuranceDisplayServiceShow)
          this.isInsuranceDisplayServiceShow = _controls.IsInsuranceDisplayServiceShow
        if (_controls.IsPremiumTrackingPlatformControlShow)
          this.isPremiumTrackingPlatformControlShow = _controls.IsPremiumTrackingPlatformControlShow
      }
    } catch (error) {
      console.log(error)
    }

    this.hasOptionalEmailTouched = false
    this.hasOptionalSocialTouched = false
    this.newPushedEmails = []
    try {
      this.businessInfoForm.controls['orgName'].setValue(info.ProviderName)
      this.businessInfoForm.controls['address'].setValue(info.ProviderAddress)
      this.businessInfoForm.controls['poBoxNo'].setValue(info.POBox)
      this.businessInfoForm.controls['business_email'].setValue(info.BusinessEmail)
      this.portalUrl = info.ProfileID
    } catch (error) {
      console.log('businessInfoForm:', error)
    }

    this.setHomePageData(info)
    if (info.AlternateEmails) {
      try {
        this.alternateEmails = info.AlternateEmails.split('').filter(email => (email && email.length > 1))
      } catch (error) {
        console.warn(error)
      }
    }
    if (info.ProviderCity) {
      try {
        const cityObj = (JSON.parse(info.ProviderCity))
        const obj = {
          id: cityObj.ID,
          code: cityObj.Code,
          title: cityObj.Title,
          shortName: cityObj.ShortName,
          imageName: cityObj.ImageName,
          desc: cityObj.Desc,
          webURL: cityObj.WebURL,
          sortingOrder: cityObj.SortingOrder,
          type: cityObj.Type,
          lastUpdate: cityObj.LastUpdate,
          latitude: cityObj.Latitude,
          longitude: cityObj.Longitude,
          isBaseCurrency: cityObj.IsBaseCurrency,
          jsonServices: cityObj.JsonServices,
          isChinaGateway: cityObj.IsChinaGateway,
          shortPortCode: cityObj.ShortPortCode,
          portCountryCode: cityObj.PortCountryCode,
          roundingOff: cityObj.RoundingOff,
        }

        this.businessInfoForm.controls['city'].setValue(obj)
      } catch (error) { }
    }
    let selectedCountry = this.countryList.find(obj => obj.id == info.ProviderPhoneCodeCountryID)
    if (selectedCountry && Object.keys(selectedCountry).length) {
      this.selectTelCode(selectedCountry)
    } else {
      selectedCountry = this.countryList.find(obj => obj.id == info.CountryID)
      if (selectedCountry && Object.keys(selectedCountry).length)
        this.selectTelCode(selectedCountry)
    }
    if (selectedCountry && typeof selectedCountry == 'object' && Object.keys(selectedCountry).length) {
      let description = selectedCountry.desc
      info.ProviderPhone = info.ProviderPhone.replace(description[0].CountryPhoneCode, "")
    }
    this.businessInfoForm.controls['phone'].setValue(info.ProviderPhone)
    if (info.SocialMediaAccountID && info.ProviderWebAdd) {
      let obj = this.socialWebs.find(elem => elem.SocialMediaPortalsID == info.SocialMediaAccountID)
      this.selectedSocialLink(obj)
      this.businessInfoForm.controls['socialUrl'].setValue(info.ProviderWebAdd)
    } else {
      this.selectedSocialsite = this.socialWebs[this.socialWebs.length - 1]
    }
    this.businessInfoToggler = false
    this.listenBusinessChanges = true
  }

  setHomePageData(info) {
    this.listenHomeChanges = false
    try {
      this.homePageForm.controls['marketingTagLineHeading'].setValue(info.MarketingTagLineHeading)
      this.homePageForm.controls['marketingTagLineContent'].setValue(info.MarketingTagLineContent)
    } catch { }
    this.listenHomeChanges = true
  }

  freightService(obj, selectedService, index) {
    if (obj.LogServCode && obj.LogServCode === 'NVOCC' && obj.IsSelected) {
      this._toastr.info('Please contact support@hashmove.com to enable or disable this service')
      return
    }
    let selectedItem = selectedService.classList
    if (this.frtService && this.frtService.length) {
      for (var i = 0; i < this.frtService.length; i++) {
        if (this.frtService[i].LogServID == obj.LogServID) {
          const { freightServices } = this
          const fr_lenght = freightServices.filter(service => this.fr_services.includes(service.LogServCode) && service.IsSelected).length
          if (fr_lenght === 1 && this.fr_services.includes(selectedItem.LogServCode)) {
            this._toastr.info("All Freight Services cannot be removed.", '')
            return
          }
          if (this.frtService.length > 1 && obj.IsRemovable) {
            this.loadingServices = true
            this.frtService.splice(i, 1)
            selectedItem.remove('active')
            if (obj.LogServCode == "WRHS") {
              this.wareHouseTypeToggler = false
              this.availModes = this.availModes.filter(_mode => _mode !== 'WAREHOUSE')
              this.IsRealEstate = false
            }
            if (obj.LogServCode === "SEA_FFDR") {
              this.seaTypeToggler = false
              this.availModes = this.availModes.filter(_mode => _mode !== 'SEA')
            }
            if (obj.LogServCode === "AIR_FFDR") {
              this.airTypeToggler = false
              this.availModes = this.availModes.filter(_mode => _mode !== 'AIR')
            }
            if (obj.LogServCode === "TRUK") {
              this.groundTypeToggler = false
              this.availModes = this.availModes.filter(_mode => _mode !== 'TRUCK')
            }
            if (obj.LogServCode === "INSR") {
              this.insuranceToggler = false
              this.availModes = this.availModes.filter(_mode => _mode !== 'INSURANCE')
            }
            this.removeServices(obj)
            this.freightServices[index].IsSelected = false
          } else {
            if (!obj.IsRemovable) {
              if (obj.LogServCode == "WRHS") {
                this._toastr.info("Service can not be removed. Please first removed the warehouse", '')
              } else {
                this._toastr.info("Service can not be removed. Please first removed the rates", '')
              }
            } else {
              this._toastr.info("At least one service is mandatory.", '')
            }
          }
          return
        }
      }
    }

    if ((this.frtService && !this.frtService.length) || (i === this.frtService.length)) {
      this.loadingServices = true
      selectedItem.add('active')
      this.freightServices[index].IsSelected = true
      this.frtService.push(obj)
      this.selectServices(obj, 'freight')
      if (obj.LogServCode == "WRHS") {
        this.wareHouseTypeToggler = true
        this.availModes.push('WAREHOUSE')
      }
      if (obj.LogServCode === "SEA_FFDR") {
        this.seaTypeToggler = false
        this.availModes.push('SEA')
      }
      if (obj.LogServCode === "AIR_FFDR") {
        this.airTypeToggler = false
        this.availModes.push('AIR')
      }
      if (obj.LogServCode === "TRUK") {
        this.groundTypeToggler = false
        this.availModes.push('TRUCK')
      }
      if (obj.LogServCode === "INSR") {
        this.insuranceToggler = false
        this.availModes.push('INSURANCE')
      }
      this.setMailNotifs()
    }
  }

  valAdded(obj, selectedService) {
    let selectedItem = selectedService.classList
    if (this.valueService && this.valueService.length) {
      for (var i = 0; i < this.valueService.length; i++) {
        if (this.valueService[i].LogServID == obj.LogServID) {
          this.loadingValAdded = true
          this.valueService.splice(i, 1)
          selectedItem.remove('active')
          this.removeServices(obj)
          return
        }
      }
    }
    if ((this.valueService && !this.valueService.length) || (i == this.valueService.length)) {
      this.loadingValAdded = true
      selectedItem.add('active')
      this.valueService.push(obj)
      this.selectServices(obj, 'vas')
    }
  }

  selectPhoneCode(country) {
    this.mobileCountFlagImage = country.code
    this.mobileCode = country.desc[0].CountryPhoneCode
    this.mobileCountryId = country.id
    this.selCountry = country
    this.personalInfoToggler = true
  }

  selectTelCode(list) {
    this.countryFlagImage = list.code
    this.phoneCode = list.desc[0].CountryPhoneCode
    this.phoneCountryId = list.id
    this.selCountryTel = list
    this.businessInfoToggler = true
  }

  selectServices(_freight: FreightModel, from: string) {

    const object = {
      provLogServID: _freight.ProvLogServID,
      providerID: this.userProfile.ProviderID,
      logServID: _freight.LogServID,
      isActive: true,
      createdBy: (_freight.ProvLogServID && _freight.ProvLogServID > 0) ? -1 : this.userProfile.UserID,
      modifiedBy: (_freight.ProvLogServID && _freight.ProvLogServID > 0) ? this.userProfile.UserID : -1,
    }
    this._settingService.selectProviderService(object).subscribe((res: JsonResponse) => {
      this.loadingServices = false
      this.loadingValAdded = false
      if (res.returnId > 0) {

        if (from === 'vas') {
          this.valAddedServices.forEach(_freight => {
            if (_freight.LogServID === _freight.LogServID)
              _freight.ProvLogServID = res.returnObject.provLogServID
          })
        } else if (from === 'freight') {
          this.freightServices.forEach(_freight => {
            if (_freight.LogServID === _freight.LogServID) {
              if (res.returnObject.logServCode !== 'WRHS') {
                _freight.ProvLogServID = res.returnObject.provLogServID
              } else {
                _freight.ProvLogServID = res.returnObject.provLogServID
                this.IsRealEstate = res.returnObject.isRealEstate
              }
            }
          })
        }
      }
    })
  }

  removeServices(obj: FreightModel) {
    const object = {
      provLogServID: obj.ProvLogServID,
      providerID: this.userProfile.ProviderID,
      logServID: obj.LogServID,
      isActive: false,
      createdBy: -1,
      modifiedBy: (obj.ProvLogServID && obj.ProvLogServID > 0) ? this.userProfile.UserID : -1,
    }
    this._settingService.deSelectService(object).subscribe((res: JsonResponse) => {
      this.loadingServices = false
      this.loadingValAdded = false
      if (res.returnId !== 0)
        this._toastr.error(res.returnText, '')
    }, () => {
      this.loadingValAdded = false
      this.loadingServices = false
    })
  }

  realEstateSel(type, event) {
    let wareHouseTypeSel = this.frtService.some(obj => obj.LogServCode == "WRHS" && obj.IsRemovable)
    if (!wareHouseTypeSel) {
      event.preventDefault()
      if (this.IsRealEstate && type != 'realEstate') {
        this._toastr.info("Service can not be update. Please first removed the warehouse", '')
      } else if (!this.IsRealEstate && type != 'owner') {
        this._toastr.info("Service can not be update. Please first removed the warehouse", '')
      }
      return
    }
    this.IsRealEstate = (type == 'owner') ? false : true
    const object = {
      providerID: this.userProfile.ProviderID,
      modifiedBy: this.userProfile.UserID,
      isRealEstate: this.IsRealEstate,
    }
    this._settingService.updateRealRstate(object).subscribe((res: JsonResponse) => {
      if (res.returnId > 0) {
        this._toastr.success('Updated successfully', '')
      } else {
        this._toastr.error('Error Occured', '')
      }
    })
  }

  companyAboutUs() {
    const object = {
      providerID: this.userProfile.ProviderID,
      about: this.editorContent,
      ModifiedBy: this.userProfile.UserID
    }
    this._settingService.companyAbout(object).subscribe((res: JsonResponse) => {
      if (res.returnId > 0) {
        this._toastr.success('Updated Successfully.', '')
        this.editable = false
        setTimeout(() => {
          if (this.editorContent.length > this.showMoreTextLength) {
            let elem = this.editContainer as any
            if (elem && elem.nativeElement.classList.contains('showMore')) {
              elem.nativeElement.classList.remove('showMore')
              this.showTogglerText = "Show Less"
            } else {
              if (elem) {
                elem.nativeElement.classList.add('showMore')
                this.showTogglerText = "Show More"
              }
            }
          } else {
            this.showTogglerText = undefined
          }
        }, 0)
      } else {
        this._toastr.error(res.returnText, '')
      }
    })
  }

  textValidation(event) {
    const pattern = /^[a-zA-Z0-9_]*$/
    let inputChar = String.fromCharCode(event.charCode)
    if (!pattern.test(inputChar)) {
      if (event.charCode == 0) {
        return true
      }
      if (event.target.value) {
        var end = event.target.selectionEnd
        if (event.charCode == 32 && (event.target.value[end - 1] == " " || event.target.value[end] == " ")) {
          event.preventDefault()
          return false
        } else if (event.charCode == 32 && !pattern.test(inputChar)) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    } else {
      return true
    }
  }

  oneSpaceHandler(event) {
    if (event.target.value) {
      var end = event.target.selectionEnd
      if (event.keyCode == 32 && (event.target.value[end - 1] == " " || event.target.value[end] == " ")) {
        event.preventDefault()
        return false
      }
    } else if (event.target.selectionEnd == 0 && event.keyCode == 32) {
      return false
    }
  }

  spaceHandler(event) {
    if (event.charCode == 32) {
      event.preventDefault()
      return false
    }
  }

  numberValid(evt) {
    const charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false
    return true
  }

  removeSelectedDocx(index, obj, type) {
    loading(true)
    obj.DocumentFile = obj.DocumentFile.split(baseExternalAssets).pop()
    if (type == 'logo')
      obj.DocumentID = this.docTypeIdLogo

    if (type == 'profile') {
      obj.DocumentID = this.docTypeIdProfileLogo
    } else if (type == 'gallery') {
      obj.DocumentID = this.docTypeIdGallery
    } else if (type == 'market-docs') {
      obj.DocumentID = this.docTypeIdMarketGallery
    } else if (type == 'certificate') {
      obj.DocumentID = this.docTypeIdCert
    } else if (type == 'liscence') {
      obj.DocumentID = this.docTypeIdLiscence
    } else if (type == 'profile-banner') {
      obj.DocumentID = this.docTypeIdProfileBanner
    }

    this._basicInfoService.removeDoc(obj).subscribe((res: JsonResponse) => {
      loading(false)
      if (res.returnId > 0) {
        this._toastr.success('Remove selected document succesfully', "")
        if (type == 'logo') {
          this.uploadedlogo = {}
        }
        if (type == 'profile') {
          this.uploadedProfileLogo = {} as any
        } else if (type == 'gallery') {
          this.uploadedGalleries.splice(index, 1)
          if (!this.uploadedGalleries || (this.uploadedGalleries && !this.uploadedGalleries.length)) {
            this.docTypeIdGallery = null
          }
        } else if (type == 'market-docs') {
          this.uploadedMarketGallery.splice(index, 1)
          if (!this.uploadedMarketGallery || (this.uploadedMarketGallery && !this.uploadedMarketGallery.length)) {
            this.docTypeIdMarketGallery = null
          }
        } else if (type == 'certificate') {
          this.uploadedCertificates.splice(index, 1)
          if (!this.uploadedCertificates || (this.uploadedCertificates && !this.uploadedCertificates.length)) {
            this.docTypeIdCert = null
          }
        } else if (type == 'liscence') {
          this.uploadedLiscence.splice(index, 1)
          if (!this.uploadedLiscence || (this.uploadedLiscence && !this.uploadedLiscence.length)) {
            this.docTypeIdLiscence = null
          }
        } else if (type == 'profile-banner') {
          this.uploadedProfileBanner.splice(index, 1)
          if (!this.uploadedProfileBanner || (this.uploadedProfileBanner && !this.uploadedProfileBanner.length)) {
            this.docTypeIdProfileBanner = null
          }
        }
      } else {
        this._toastr.error('Error Occured', "")
      }
    }, (err) => {
      loading(false)
      console.log(err)
    })
  }

  selectDocx(selectedFiles: NgFilesSelected, type, freight?: FreightModel): void {
    if (selectedFiles.status !== NgFilesStatus.STATUS_SUCCESS) {
      if (selectedFiles.status == 1) this._toastr.error('Please select 12 or less file(s) to upload.', '')
      else if (selectedFiles.status == 2) this._toastr.error('File size should not exceed 5 MB. Please upload smaller file.', '')
      else if (selectedFiles.status == 4) this._toastr.error('File format is not supported. Please upload supported format file.', '')
      return
    } else {
      try {
        if (type == 'certificate') {
          if (this.uploadedCertificates.length + selectedFiles.files.length > this.config.maxFilesCount) {
            this._toastr.error('Please select 12 or less file(s) to upload.', '')
            return
          }
        } else if (type == 'logo') {
          if (selectedFiles.files.length > 1) {
            this._toastr.error('Please select only 1 file to upload.', '')
            return
          }
        } else if (type == 'profile') {
          if (selectedFiles.files.length > 1) {
            this._toastr.error('Please select only 1 file to upload.', '')
            return
          }
        } else if (type == 'liscence') {
          if (this.uploadedLiscence.length + selectedFiles.files.length > this.config.maxFilesCount) {
            this._toastr.error('Please select 12 or less file(s) to upload.', '')
            return
          }
        } else if (type == 'gallery') {
          if (this.uploadedGalleries.length + selectedFiles.files.length > this.config.maxFilesCount) {
            this._toastr.error('Please select 12 or less file(s) to upload.', '')
            return
          }
        } else if (type == 'market-docs') {
          if (this.uploadedMarketGallery.length + selectedFiles.files.length > this.config.maxFilesCount) {
            this._toastr.error('Please select 12 or less file(s) to upload.', '')
            return
          }
        } else if (type == 'profile-banner') {
          if (selectedFiles.files.length > 1) {
            this._toastr.error('Please select only one file to upload.', '')
            return
          }
        } else if (type == 'service-upload') {
          if (selectedFiles.files.length > 1) {
            this._toastr.error('Please select only one file to upload.', '')
            return
          }
        }
        this.onFileChange(selectedFiles, type, freight)
      } catch (error) {
        console.log(error)
      }
    }
  }

  onFileChange(event, type, freight?: FreightModel) {
    let flag = 0
    if (event) {
      try {
        const allDocsArr = []
        const fileLenght: number = event.files.length
        for (let index = 0; index < fileLenght; index++) {
          let reader = new FileReader()
          const element = event.files[index]
          let file = element
          reader.readAsDataURL(file)
          reader.onload = () => {
            const selectedFile: DocumentFile = {
              fileName: file.name,
              fileType: file.type,
              fileUrl: reader.result,
              fileBaseString: (reader as any).result.split(',').pop()
            }
            if (event.files.length <= this.config.maxFilesCount) {
              const docFile = JSON.parse(this.generateDocObject(selectedFile, type, freight))
              allDocsArr.push(docFile)
              flag++
              if (flag === fileLenght)
                this.uploadDocuments(allDocsArr, type, freight)
            } else {
              this._toastr.error('Please select only ' + this.config.maxFilesCount + 'file to upload', '')
            }
          }
        }
      } catch (err) {
        console.log(err)
      }
    }
  }

  generateDocObject(selectedFile, type, freight?: FreightModel): any {
    let object = null
    if (type == 'logo') {
      object = this.companyLogoDocx
      object.DocumentID = this.docTypeIdLogo
      object.DocumentLastStatus = this.fileStatusLogo
    } if (type == 'profile') {
      object = this.profileLogoDocx
      object.DocumentID = this.docTypeIdProfileLogo
      object.DocumentLastStatus = this.fileStatusProfileLogo
    } else if (type == 'gallery') {
      object = this.galleriesDocx
      object.DocumentID = this.docTypeIdGallery
      object.DocumentLastStatus = this.fileStatusGallery
    } else if (type == 'market-docs') {
      object = this.marketGalleryDocx
      object.DocumentID = this.docTypeIdMarketGallery
      object.DocumentLastStatus = this.fileStatusMarketGallery
    } else if (type == 'certificate') {
      object = this.certficateDocx
      object.DocumentID = this.docTypeIdCert
      object.DocumentLastStatus = this.fileStatusCert
    } else if (type == 'liscence') {
      object = this.liscenceDocx
      object.DocumentID = this.docTypeIdLiscence
      object.DocumentLastStatus = this.fileStatusLiscence
    } else if (type == 'profile-banner') {
      object = this.profileBannerDocx
      object.DocumentID = this.docTypeIdProfileBanner
      object.DocumentLastStatus = this.fileStatusProfileBanner
    } else if (type == 'service-upload') {
      object = this.serverImageDocx
      object.DocumentID = -1
      object.OtherID = freight.ProvLogServID
      object.OtherType = "LOGISTIC_SERVICE"
    } else {
      console.warn('doc-type not defined')
    }
    object.UserID = this.userProfile.UserID
    object.ProviderID = this.userProfile.ProviderID
    object.DocumentFileContent = null
    object.DocumentName = null
    object.DocumentUploadedFileType = null
    object.FileContent = [{
      documentFileName: selectedFile.fileName,
      documentFile: selectedFile.fileBaseString,
      documentUploadedFileType: selectedFile.fileType.split('/').pop()
    }]
    return JSON.stringify(object)
  }

  async uploadDocuments(docFiles: Array<any>, type, freight?: FreightModel) {
    loading(true)
    const totalDocLenght: number = docFiles.length
    for (let index = 0; index < totalDocLenght; index++) {
      try {
        const resp: JsonResponse = await this.docSendService(docFiles[index])
        if (resp.returnId > 0) {
          let resObj = JSON.parse(resp.returnText)
          if (type == 'logo') {
            this.docTypeIdLogo = resObj.DocumentID
            this.fileStatusLogo = resObj.DocumentLastStaus
          }
          if (type == 'profile') {
            this.docTypeIdProfileLogo = resObj.DocumentID
            this.fileStatusProfileLogo = resObj.DocumentLastStaus
          } else if (type == 'gallery') {
            this.docTypeIdGallery = resObj.DocumentID
            this.fileStatusGallery = resObj.DocumentLastStaus
          } else if (type == 'market-docs') {
            this.docTypeIdMarketGallery = resObj.DocumentID
            this.fileStatusMarketGallery = resObj.DocumentLastStaus
          } else if (type == 'profile-banner') {
            this.docTypeIdProfileBanner = resObj.DocumentID
            this.fileStatusProfileBanner = resObj.DocumentLastStaus
          } else if (type == 'certificate') {
            this.docTypeIdCert = resObj.DocumentID
            this.fileStatusCert = resObj.DocumentLastStaus
          } else if (type == 'liscence') {
            this.docTypeIdLiscence = resObj.DocumentID
            this.fileStatusLiscence = resObj.DocumentLastStaus
          } else if (type == 'service-upload') {
            // this.docTypeIdLiscence = resObj.DocumentID
            // this.fileStatusLiscence = resObj.DocumentLastStaus
          }
          const fileObj = JSON.parse(resObj.DocumentFile)
          if (type == 'logo') {
            let avatar = Object.assign([], fileObj)
            this._sharedService.updateAvatar.next(avatar)
          }
          fileObj.forEach(element => {
            element.DocumentFile = baseExternalAssets + element.DocumentFile
          })
          if (index !== (totalDocLenght - 1)) {
            docFiles[index + 1].DocumentID = resObj.DocumentID
            docFiles[index + 1].DocumentLastStatus = resObj.DocumentLastStaus
          }

          if (type == 'logo') {
            this.uploadedlogo = fileObj[0]
          }
          if (type == 'profile') {
            this.uploadedProfileLogo = fileObj[0]
          } else if (type == 'gallery') {
            this.uploadedGalleries = fileObj
          } else if (type == 'market-docs') {
            this.uploadedMarketGallery = fileObj
          } else if (type == 'certificate') {
            this.uploadedCertificates = fileObj
          } else if (type == 'liscence') {
            this.uploadedLiscence = fileObj
          } else if (type == 'profile-banner') {
            this.uploadedProfileBanner = fileObj
            this.getUserDetail(this.userProfile.UserID)
          } else if (type == 'service-upload') {
            this.getUserDetail(this.userProfile.UserID)
          }
          this._toastr.success("File upload successfully", "")
        }
        else {
          this._toastr.error("Error occured on upload", "")
        }
      } catch (error) {
        loading(false)
        this._toastr.error("Error occured on upload", "")
      }
    }
    loading(false)
  }

  docSendService = async (doc: any) => await this._basicInfoService.docUpload(doc).toPromise()

  deactivate() {
    const modalRef = this.modalService.open(ConfirmDeleteDialogComponent, {
      size: 'lg', centered: true, windowClass: 'small-modal', backdrop: 'static', keyboard: false
    })
    modalRef.componentInstance.deleteIds = {
      data: this.userProfile.UserID, type: "DelAccount"
    }
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open'))
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden'
    }, 0)
  }

  updatePersonalInfo() {
    try {
      if (this.personalInfoForm.value.mobile) {
        const _mobNum = getPhoneNumber(this.personalInfoForm.value.mobile)
        this.personalInfoForm.controls['mobile'].setValue(_mobNum)
      }
    } catch { }

    let _CountryID = null
    try {
      try {
        _CountryID = JSON.parse(this.personalInfoForm.value.city.desc)[0].CountryID
      } catch {
        _CountryID = this.personalInfoForm.value.city.desc[0].CountryID
      }
    } catch (error) { }
    let obj = {
      userID: this.userProfile.UserID,
      firstName: this.personalInfoForm.value.firstName,
      lastName: this.personalInfoForm.value.lastName,
      jobTitle: this.personalInfoForm.value.jobTitle.baseLanguage,
      cityID: this.personalInfoForm.value.city.id,
      countryID: _CountryID,
      mobileNumberPrimary: this.personalInfoForm.value.mobile,
      countryPhoneCode: this.mobileCode,
      phoneCodeCountryID: this.mobileCountryId,
      regionID: (!this.personalInfoForm.value.region) ? null : this.personalInfoForm.value.region,
      currencyID: this.personalInfoForm.value.currency.id,
      updatedBy: this.userProfile.UserID
    }
    loading(true)
    this._settingService.personalSetting(obj).subscribe((res: JsonResponse) => {
      if (res.returnId > 0) {
        this._toastr.success('Personal Information Updated', '')
        this.personalInfoToggler = false
        let userObj = JSON.parse(localStorage.getItem('userInfo'))
        let userData = JSON.parse(userObj.returnText)
        userData.CurrencyID = this.personalInfoForm.value.currency.id
        userObj.returnText = JSON.stringify(userData)
        localStorage.setItem('userInfo', JSON.stringify(userObj))
        this.getUserDetail(this.userProfile.UserID)
      }
    })
  }

  insertEmail() {
    const { basicEmailRegex } = this
    try {
      if (this.businessInfoForm.controls['alternateEmails_field'].value) {
        const _email = this.businessInfoForm.controls['alternateEmails_field'].value
        if (String(_email).match(basicEmailRegex)) {
          this.addEmail(_email)
        }
      }
    } catch (error) { }
    this.businessInfoForm.controls['alternateEmails_field'].setValue('')
  }

  updatebussinesInfo() {
    loading(true)
    let _CountryID = null
    try {
      _CountryID = this.businessInfoForm.value.city.desc[0].CountryID
    } catch (error) { }
    let obj = {
      userID: this.userProfile.UserID,
      providerID: this.userProfile.ProviderID,
      address: this.businessInfoForm.value.address,
      poBox: this.businessInfoForm.value.poBoxNo,
      cityID: this.businessInfoForm.value.city.id,
      countryID: _CountryID,
      telephone: this.businessInfoForm.value.phone,
      businessEmail: this.businessInfoForm.value.business_email,
      countryPhoneCode: this.phoneCode,
      phoneCodeCountryID: this.phoneCountryId,
      modifiedBy: this.userProfile.UserID,
      alternateEmails: null,
    }
    this._settingService.businessSetting(obj).subscribe((res: JsonResponse) => {
      loading(false)
      if (res.returnId > 0) {
        this._toastr.success('Business Information Updated', 'Success')
        this.businessInfoToggler = false
        this.newPushedEmails = []
        this.hasOptionalEmailTouched = false
        this.hasOptionalSocialTouched = false
        this.getUserDetail(this.userProfile.UserID)
      } else {
        this._toastr.error(res.returnText, 'Failed')
      }
    })
  }

  setSocialLinks() {
    try {
      const { socialWebs } = this
      const _filteredLinks = socialWebs
      this.listenSocialChanges = false
      this.socialLinks = _filteredLinks.map(_social => ({
        id: _social.SocialMediaPortalsID,
        type: _social.SocialMediaCode,
        title: _social.Title,
        link: _social.LinkURL
      }))
      const { socialLinks } = this
      socialLinks.forEach(_social => {
        if (_social.type === 'LNKN') {
          this.socialMediaForm.controls.linkedInUrl.setValue(_social.link)
          this.linkedInUrl = _social.link
        }
        if (_social.type === 'FCBK') {
          this.socialMediaForm.controls.fbUrl.setValue(_social.link)
          this.fbUrl = _social.link
        }
        if (_social.type === 'TWTR') {
          this.socialMediaForm.controls.twitterUrl.setValue(_social.link)
          this.twitterUrl = _social.link
        }
        if (_social.type === 'INST') {
          this.socialMediaForm.controls.instaUrl.setValue(_social.link)
          this.instaUrl = _social.link
        }
        if (_social.type === 'YUTB') {
          this.socialMediaForm.controls.youtubeUrl.setValue(_social.link)
          this.youtubeUrl = _social.link
        }
        if (_social.type.toUpperCase() === 'WEB') {
          this.socialMediaForm.controls.webUrl.setValue(_social.link)
          this.webUrl = _social.link
        }
      })

    } catch (error) {
      console.log(error)
    }
    this.listenSocialChanges = true
  }

  removeChip(item, idx, type) {
    this.hasOptionalEmailTouched = true
    this.businessInfoToggler = true
    this.alternateEmails.splice(idx, 1)
  }

  removeSocial({ link, type }) {
    const { socialLinks } = this
    this.hasOptionalSocialTouched = true
    this.socialLinks = socialLinks.filter(_social => _social.type !== type)
  }

  onEmailSocialEnter($type, $event) {
    this.hasOptionalEmailTouched = true
    this.hasOptionalSocialTouched = true

    if (!$event.target.value || $event.target.value === undefined || $event.target.value === null || $event.target.value === '') {
      if (this.newPushedEmails.length === 0) {
        this.hasOptionalEmailTouched = false
        this.hasOptionalSocialTouched = false
      }
    }

    if (($event.keyCode === 32 || $event.keyCode === 13)) {
      if ($event.target.value) {
        const optEmail: string = $event.target.value
        if ($type === 'email') {
          this.businessInfoToggler = true
          this.addEmail(optEmail)
        } else {
          this.addSocial(optEmail)
        }
        $event.preventDefault()
      }
    }
  }

  addEmail(optEmail) {
    const { basicEmailRegex } = this
    if (!basicEmailRegex.test(optEmail)) {
      this._toastr.error('Invalid email entered.', 'Error')
      event.preventDefault()
      return
    }
    let valid: boolean = ValidateEmail(optEmail)
    if (!valid) {
      this._toastr.warning('Invalid email entered.', 'Error')
      event.preventDefault()
      return
    }
    this.alternateEmails.forEach(element => {
      if (element === optEmail) {
        let idx = this.alternateEmails.indexOf(element)
        this.alternateEmails.splice(idx, 1)
        this._toastr.warning('Email already added', 'Error')
        event.preventDefault()
        return
      }
    })
    if (this.alternateEmails.length === 10) {
      this._toastr.warning('Email Limit Exceeded', 'Error')
      event.preventDefault()
      return
    }
    this.alternateEmails.push(optEmail)
    this.newPushedEmails.push(optEmail)
    this.businessInfoForm.controls['alternateEmails_field'].setValue('')

  }

  addSocial($optSocial: string) {

    const { selectedSocialsite } = this
    const { socialLinks } = this
    const _containsLink = socialLinks.filter(_social => _social.type === selectedSocialsite.SocialMediaCode)
    if (_containsLink && _containsLink.length > 0) {
      this._toastr.warning('Link already added', 'Error')
      event.preventDefault()
      return
    }

    let isValid = true
    if (selectedSocialsite.SocialMediaCode === 'FCBK') {
      isValid = FACEBOOK_REGEX.test($optSocial)
      if (!isValid) { this._toastr.warning('Your social url is not valid') }
    } else if (selectedSocialsite.SocialMediaCode === 'TWTR') {
      isValid = TWITTER_REGEX.test($optSocial)
      if (!isValid) { this._toastr.warning('Your social url is not valid') }
    } else if (selectedSocialsite.SocialMediaCode === 'INST') {
      isValid = INSTAGRAM_REGEX.test($optSocial)
      if (!isValid) { this._toastr.warning('Your social url is not valid') }
    } else if (selectedSocialsite.SocialMediaCode === 'LNKN') {
      isValid = LINKEDIN_REGEX.test($optSocial)
      if (!isValid) { this._toastr.warning('Your social url is not valid') }
    } else if (selectedSocialsite.SocialMediaCode === 'YUTB') {
      isValid = YOUTUBE_REGEX.test($optSocial)
      if (!isValid) { this._toastr.warning('Your social url is not valid') }
    } else {
      isValid = GEN_URL.test($optSocial)
      if (!isValid) { this._toastr.warning('Your social url is not valid') }
    }

    if (isValid) {
      this.socialLinks.push({
        id: selectedSocialsite.SocialMediaPortalsID,
        link: $optSocial,
        type: selectedSocialsite.SocialMediaCode,
        title: selectedSocialsite.Title
      })
      this.socialMediaForm.controls['socialUrl'].setValue('')
    }
  }

  jobSearch = (text$: Observable<string>) =>
    text$.pipe(debounceTime(200), map(term => (!term || term.length < 2) ? []
      : this.jobTitles.filter(v => v.baseLanguage.toLowerCase().indexOf(term.toLowerCase()) > -1)))

  formatterjob = (x: { baseLanguage: string }) => x.baseLanguage

  searchCity = (text$: Observable<string>) =>
    text$.debounceTime(200).map(term => (!term || term.length < 3) ? []
      : this.cityList.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1))

  formatterCity = (x: { title: string }) => x.title

  currency = (text$: Observable<string>) =>
    text$.debounceTime(200).map(term => (!term || term.length < 3) ? []
      : this.currencyList.filter(v => v.shortName.toLowerCase().indexOf(term.toLowerCase()) > -1))
  formatterCurrency = (x: { shortName: string }) => x.shortName

  formatterCty = (_city) => {
    this.isCitySearching = false
    return _city.title
  }

  search2 = (text$: Observable<string>) =>
    text$.debounceTime(300).distinctUntilChanged()
      .do(() => { this.isCitySearching = true; this.hasCitySearchFailed = false; this.hasCitySearchSuccess = false; }) // do any action while the 
      .concatMap(term => {
        let some = of([])
        try {
          if (term && term.length >= 3) { //search only if item are more than three
            some = this._commonService.getFilteredCity(term)
              .do((res) => { this.isCitySearching = false; this.hasCitySearchSuccess = true; return res; })
              .catch(() => { this.isCitySearching = false; this.hasCitySearchFailed = true; return []; })
          } else { this.isCitySearching = false; some = of([]); }
        } catch (error) {
          this.isCitySearching = false
        }
        return some
      }).do((res) => { this.isCitySearching = false; return res; })
      .catch(() => { this.isCitySearching = false; return of([]); }); // final server list

  search3 = (text$: Observable<string>) =>
    text$.debounceTime(300).distinctUntilChanged()
      .do(() => { this.isCitySearching = true; this.hasCitySearchFailed = false; this.hasCitySearchSuccess = false; }) // do any action while the 
      .concatMap(term => {
        let some = of([]) //  Initialize the object to return
        try {
          if (term && term.length >= 3) { //search only if item are more than three
            some = this._commonService.getFilteredCityV1(term)
              .do((res) => { this.isCitySearching = false; this.hasCitySearchSuccess = true; return res; })
              .catch(() => { this.isCitySearching = false; this.hasCitySearchFailed = true; return []; })
          } else { this.isCitySearching = false; some = of([]); }
        } catch (error) {
          this.isCitySearching = false
        }
        return some
      }).do((res) => { this.isCitySearching = false; return res; })
      .catch(() => { this.isCitySearching = false; return of([]); }); // final server list

  formatterBank = (_bank) => {
    this.isBankSearching = false
    return _bank.title
  }

  bankSearch = (text$: Observable<string>) =>
    text$.debounceTime(300).distinctUntilChanged()
      .concatMap(term => {
        let some = of([]) //  Initialize the object to return
        try {
          if (term && term.length >= 3) { //search only if item are more than three
            some = this._commonService.getFilteredBank(term)
              .do((res) => { this.isBankSearching = false; this.hasBankSearchSuccess = true; return res; })
              .catch(() => { this.isBankSearching = false; this.hasBankSearchFailed = true; return []; })
          } else { this.isBankSearching = false; some = of([]); }
        } catch (error) {
          this.isBankSearching = false
        }
        return some
      }).do((res) => { this.isBankSearching = false; return res; })
      .catch(() => { this.isBankSearching = false; return of([]); }); // final server list

  saveBankData() {
    if (this.bankInfoForm.invalid)
      return
    const { value } = this.bankInfoForm
    const toSend = {
      provBankID: (this.savedBankInfo && this.savedBankInfo.provBankID && this.savedBankInfo.provBankID > 0) ? this.savedBankInfo.provBankID : -1,
      providerID: this.userProfile.ProviderID,
      bankID: value.bankName.id,
      accountTitle: value.accountTitle,
      accountNo: value.accountNumber,
      swiftCode: "",
      branchName: value.branchName,
      branchAddress: value.branchAddress,
      branchCityID: value.branchCity.id,
      createdBy: this.userProfile.UserID,
      modifiedBy: this.userProfile.UserID,
    }
    loading(true)
    this._settingService.setProviderBank(toSend).subscribe((res: JsonResponse) => {
      loading(false)
      if (res.returnId > 0) {
        this._toastr.success('Information updated sucessfully', "Success")
        this.bankInfoToggler = false
        this.getchBankInfo()
      } else {
        this._toastr.error(res.returnText, res.returnStatus)
      }
    }, () => {
      loading(false)
      this._toastr.error('An error occured while processing request, please try later')
    })
  }

  numberValidStatistic(event, data, maxValue) {
    this.businessInfoToggler = true
    try {
      const charCode = (event.which) ? event.which : event.keyCode
      const key = event.key
      if (key === '.') {
        event.preventDefault()
        return false
      }
      let isTrue = true
      const currentValue: string = event.target.value
      const newValue = currentValue + key
      if (isNumber(newValue) && parseInt(newValue) > maxValue) {
        event.preventDefault()
        return false
      }
      if (
        (charCode != 45 || currentValue.toString().includes('-')) && // “-” CHECK MINUS, AND ONLY ONE.
        (charCode != 46 || currentValue.toString().includes('.')) && // “.” CHECK DOT, AND ONLY ONE.
        (charCode < 48 || charCode > 57) ||
        key === '-' ||
        (key === '.' && currentValue.toString().includes('.')) ||
        ((key === '.' || key === '-') && !currentValue)
      ) {
        event.preventDefault()
        isTrue = false
      }
      return isTrue
    } catch (error) {
      event.preventDefault()
      return false
    }
  }

  counter($action: string, $stat: IStatistic) {
    this.hompageToggler = true
    try {
      let _statValue = 0
      try {
        _statValue = $stat.StatisticValue ? parseInt($stat.StatisticValue) : 0
      } catch {
        _statValue = 0
      }
      if ($action === 'add') {
        _statValue++
      } else if (_statValue > 1) {
        _statValue--
      }
      $stat.StatisticValue = _statValue as any
    } catch { }
  }

  notifCounter($action: string, $type, $index: number) {
    this.notifToggler = true
    try {
      let _model = 0
      switch ($type) {
        case 'ExtendedDays':
          _model = this.notifArray[$index].ExtendedDays ? this.notifArray[$index].ExtendedDays : 0
          if ($action === 'add') {
            _model++
          } else if (_model > 1) {
            _model--
          }
          this.notifArray[$index].ExtendedDays = _model
          break
        case 'IncreaseRatePercentage':
          _model = this.notifArray[$index].IncreaseRatePercentage ? this.notifArray[$index].IncreaseRatePercentage : 0
          if ($action === 'add') {
            _model++
          } else if (_model > 1) {
            _model--
          }
          this.notifArray[$index].IncreaseRatePercentage = _model
          break
        case 'BeforeExpiryDay':
          _model = this.notifArray[$index].BeforeExpiryDay ? this.notifArray[$index].BeforeExpiryDay : 0
          if ($action === 'add') {
            _model++
          } else if (_model > 1) {
            _model--
          }
          this.notifArray[$index].BeforeExpiryDay = _model
          break
        default:
          break
      }
    } catch (err) {
      console.log(err)
    }
  }

  notifCounter2($action: string) {
    this.notifToggler = true
    try {
      let _model = this.emailNotif.ExpireInDays ? this.emailNotif.ExpireInDays : 0
      if ($action === 'add') {
        _model++
      } else if (_model > 1) {
        _model--
      }
      this.emailNotif.ExpireInDays = _model
    } catch { }
  }

  resetRateValidity() {
    const { info } = this
    this.notifArray = []
    this.notifArray = cloneObject(info.RateExpirySettings)
    this.emailNotif = cloneObject(info.RateExpiryNotificationSettings)
    this.notifToggler = false
  }

  updateRateNotifications() {
    const toSend = {
      userID: this.userProfile.UserID,
      providerID: this.userProfile.ProviderID,
      modifiedBy: this.userProfile.UserID,
      jsonManageRateValidity: this.notifArray,
      jsonManageRateExpiryNotification: this.emailNotif
    }
    loading(true)
    this._settingService.updateRateValidity(toSend).subscribe((res: JsonResponse) => {
      loading(false)
      if (res.returnId > 0) {
        this._toastr.success(res.returnText, res.returnStatus)
        this.notifToggler = false
        this.getUserDetail(this.userProfile.UserID)
      } else {
        this._toastr.success(res.returnText, res.returnStatus)
      }
    }, () => loading(false))
  }

  updateHomePageSettings() {
    let _jsonStatistics = []
    try {
      if (this.IsShowStatisticsOnProfile) {
        try {
          const { statisticsArray } = this
          const newArr = statisticsArray.map(_stat => ({
            ProviderStatisticID: _stat.ProviderStatisticID,
            StatisticName: _stat.StatisticName,
            BusinessLogic: _stat.BusinessLogic,
            StatisticImage: _stat.StatisticImage,
            StatisticValue: _stat.StatisticValue
          }))
          const _Valid = newArr.filter(_stat => _stat.StatisticValue && parseInt(_stat.StatisticValue) > 0)
          const _validatiobObj = statisticsArray.filter(_stat => _stat.StatisticValue && parseInt(_stat.StatisticValue) > 0)
          if (this.IsShowStatisticsOnProfile && _Valid.length < 3) {
            this._toastr.warning('Minimum 3 fields should be entered', 'Statistics')
            loading(false)
            return
          } else {
            let _valid = true
            _validatiobObj.forEach(_stat => {
              const _value = parseInt(_stat.StatisticValue)
              if (_value < _stat.MinValue) {
                this._toastr.error(`Value should be greater or equal to ${_stat.MinValue}`, _stat.StatisticName)
                _valid = false
              } else if (_value > _stat.MaxValue) {
                this._toastr.error(`Value should be less or equal to ${_stat.MaxValue}`, _stat.StatisticName)
                _valid = false
              }
            })
            if (!_valid) {
              loading(false)
              return
            }
            _jsonStatistics = _Valid
          }
        } catch {
          _jsonStatistics = []
        }
      }
    } catch (error) {
      console.log(error)
    }
    let _logoOption = 'center'
    try {
      if (this.IsShowCompanyLogoOnProfile) {
        _logoOption = (this.profileLogoCenter) ? 'center' : 'left'
      } else {
        _logoOption = 'hide'
      }
    } catch { }
    if (this.IsMarketingTagLineRequired && this.homePageForm.invalid) {
      this._toastr.warning('Please fill the required information for tagline', 'Insufficent Data')
      this.errorMessageHomePage(false)
      return
    }

    const toSend = {
      userID: this.userProfile.UserID,
      providerID: this.userProfile.ProviderID,
      modifiedBy: this.userProfile.UserID,
      companyNamePlacementOnProfile: _logoOption,
      isMarketingTagLineRequired: this.IsMarketingTagLineRequired,
      marketingTagLineHeading: this.homePageForm.value.marketingTagLineHeading,
      marketingTagLineContent: this.homePageForm.value.marketingTagLineContent,
      taglineBackGroundColor: this.IsTaglineColor && this.selectedTaglineColor ? this.selectedTaglineColor : null,
      statisticBackGroundColor: this.IsStatisticColor && this.selectedStatisticsColor ? this.selectedStatisticsColor : null,
      footerStripColor: this.isFooterColor && this.footerColor ? this.footerColor : null,
      isShowStatisticsOnProfile: this.IsShowStatisticsOnProfile,
      jsonStatistics: _jsonStatistics
    }
    loading(true)
    this._settingService.updateHomePageSettings(toSend).subscribe((res: JsonResponse) => {
      loading(false)
      if (res.returnId > 0) {
        this._toastr.success(res.returnText, res.returnStatus)
        this.hompageToggler = false
        this.getUserDetail(this.userProfile.UserID)
      } else {
        this._toastr.error(res.returnText, res.returnStatus)
      }
    }, () => {
      loading(false)
      this._toastr.error('Please try again later', "Failed")
    })
  }

  resetHomePageData() {
    this.hompageToggler = false
    try {
      this.homePageForm.reset()
    } catch { }
    this.setHomePageData(this.info)
    try {
      const _statisticsArray = this.info.StatisticsSettings
      this.statisticsArray = _statisticsArray.map(_stat => {
        return ({
          ..._stat,
          MinValue: parseInt(_stat.MinValue),
          MaxValue: parseInt(_stat.MaxValue)
        })
      })
    } catch { }
    this.IsShowStatisticsOnProfile = this.info.IsShowStatisticsOnProfile
    try {
      if (this.info.CompanyNamePlacementOnProfile === 'hide') {
        this.IsShowCompanyLogoOnProfile = false
        this.profileLogoCenter = false
        this.profileLogoLeft = false
      } else {
        this.IsShowCompanyLogoOnProfile = true
        if (this.info.CompanyNamePlacementOnProfile === 'center') {
          this.profileLogoCenter = true
          this.profileLogoLeft = false
        } else {
          this.profileLogoCenter = false
          this.profileLogoLeft = true
        }
      }
    } catch { }

    try {
      this.IsMarketingTagLineRequired = this.info.IsMarketingTagLineRequired
    } catch { }
    try {
      if (this.info.DefaultTaglineBackGroundColor && this.info.TaglineBackGroundColor) {
        this.IsTaglineColor = true
        this.selectedTaglineColor = this.info.TaglineBackGroundColor
      } else {
        this.IsTaglineColor = false
        this.selectedTaglineColor = this.info.DefaultTaglineBackGroundColor
      }
    } catch { }
    try {
      if (this.info.DefaultStatisticBackGroundColor && this.info.StatisticBackGroundColor) {
        this.IsStatisticColor = true
        this.selectedStatisticsColor = this.info.StatisticBackGroundColor
      } else {
        this.IsStatisticColor = false
        this.selectedStatisticsColor = this.info.DefaultStatisticBackGroundColor
      }
    } catch { }
    try {
      if (this.info.DefaultFooterStripColor && this.info.FooterStripColor) {
        this.IsStatisticColor = true
        this.selectedStatisticsColor = this.info.FooterStripColor
      } else {
        this.IsStatisticColor = false
        this.selectedStatisticsColor = this.info.DefaultFooterStripColor
      }
    } catch { }

    try {
      if (this.info.DefaultFooterStripColor && this.info.FooterStripColor) {
        this.isFooterColor = true
        this.footerColor = this.info.FooterStripColor
      } else {
        this.isFooterColor = false
        this.footerColor = this.info.DefaultFooterStripColor
      }
    } catch (error) {
      console.log(error)
    }
    this.setBusinessInfo(this.info)
  }

  updatePlatformControls() {
    if (this.isWHBookEndAlert && (!this.wHBookEndAlertDays || this.wHBookEndAlertDays === 0)) {
      this._toastr.warning('Please enter Email Reminder days for warehouse rates and should be atleast 1')
      return
    }
    const toSend = {
      userID: this.userProfile.UserID,
      providerID: this.userProfile.ProviderID,
      modifiedBy: this.userProfile.UserID,
      isPrivateMode: this.isPrivateMode,
      isMarketPlace: this.isMarketPlace,
      isExclChargesAllow: this.IsExclChargesAllow,
      isCustomerApprovalRequired: this.IsCustomerApprovalRequired,
      isShowWarehouseOnProfile: this.IsShowWarehouseOnProfile,
      isShowSeaRateOnProfile: this.IsShowSeaRateOnProfile,
      isShowSeaLCLRateOnProfile: this.IsShowSeaLCLRateOnProfile,
      isShowAirRateOnProfile: this.IsShowAirRateOnProfile,
      isShowGroundRateOnProfile: this.IsShowGroundRateOnProfile,
      isWHBookEndAlert: this.isWHBookEndAlert,
      whBookEndAlertDays: (this.isWHBookEndAlert && this.wHBookEndAlertDays) ? this.wHBookEndAlertDays : 0,
      isTrackingQualityRequired: this.IsTrackingQualityRequired,
      isShowSearchBoxOnProfile: this.IsShowSearchBoxOnProfile
    }
    const _controls = {
      IsSpotRateRquired: this.isSpotRateRquired,
      IsDisplayCommodityNameOnSearchBox: this.isDisplayCommodityNameOnSearchBox
    }
    loading(true)
    Observable.forkJoin([
      this._settingService.updatePlatformSettings(toSend),
      this._settingService.updatePlatformSettingsV2(this.userProfile.ProviderID, _controls)
    ]).subscribe((res: JsonResponse[]) => {
      loading(false)
      if (res[0].returnId > 0) {
        this._toastr.success(res[0].returnText, res[0].returnStatus)
        this.platformToggler = false
        this.getUserDetail(this.userProfile.UserID)
        try {
          const _dashboardData = this._sharedService.dashboardDetail.getValue()
          const _updatedData = { ..._dashboardData, FeatureToggleJson: JSON.stringify(_controls) }
          this._sharedService.dashboardDetail.next(_updatedData)
        } catch { }
      } else {
        this._toastr.error(res[0].returnText, res[0].returnStatus)
      }
    }, () => loading(false))
  }

  resetPlatformControl() {
    const { info } = this
    this.isPrivateMode = info.IsPrivateMode
    this.isMarketPlace = info.IsMarketPlace
    this.IsExclChargesAllow = info.IsExclChargesAllow
    this.IsCustomerApprovalRequired = info.IsCustomerApprovalRequired
    this.IsShowWarehouseOnProfile = info.IsShowWarehouseOnProfile
    this.IsShowSeaRateOnProfile = info.IsShowSeaRateOnProfile
    this.IsShowSeaLCLRateOnProfile = info.IsShowSeaLCLRateOnProfile
    this.IsShowAirRateOnProfile = info.IsShowAirRateOnProfile
    this.IsShowGroundRateOnProfile = info.IsShowGroundRateOnProfile
    try {
      this.isWHBookEndAlert = info.IsWHBookEndAlert
      this.wHBookEndAlertDays = info.WHBookEndAlertDays
    } catch (error) {
    }
    try {
      if (this.info.FeatureToggleJson) {
        const { FeatureToggleJson } = this.info
        const _controls = JSON.parse(FeatureToggleJson)
        if (_controls.IsSpotRateRquired)
          this.isSpotRateRquired = _controls.IsSpotRateRquired
        if (_controls.IsDisplayCommodityNameOnSearchBox)
          this.isDisplayCommodityNameOnSearchBox = _controls.IsDisplayCommodityNameOnSearchBox
      }
    } catch (error) {
      console.log(error)
    }
    this.IsTrackingQualityRequired = info.IsTrackingQualityRequired
    this.IsShowSearchBoxOnProfile = this.info.IsShowSearchBoxOnProfile
    this.platformToggler = false
  }

  updateSocailMedia() {
    try {
      if (this.socialMediaForm.value.socialUrl && this.socialMediaForm.value.socialUrl.length > 1)
        this.addSocial(this.socialMediaForm.value.socialUrl)
    } catch { }
    try {
      this.socialLinks.forEach(_social => {
        if (_social.type === 'LNKN' && this.socialMediaForm.controls.linkedInUrl.status !== 'INVALID') {
          _social.link = this.socialMediaForm.value.linkedInUrl
        }
        if (_social.type === 'FCBK' && this.socialMediaForm.controls.fbUrl.status !== 'INVALID') {
          _social.link = this.socialMediaForm.value.fbUrl
        }
        if (_social.type === 'TWTR' && this.socialMediaForm.controls.twitterUrl.status !== 'INVALID') {
          _social.link = this.socialMediaForm.value.twitterUrl
        }
        if (_social.type === 'INST' && this.socialMediaForm.controls.instaUrl.status !== 'INVALID') {
          _social.link = this.socialMediaForm.value.instaUrl
        }
        if (_social.type === 'YUTB' && this.socialMediaForm.controls.youtubeUrl.status !== 'INVALID') {
          _social.link = this.socialMediaForm.value.youtubeUrl
        }
        if (_social.type.toUpperCase() === 'WEB' && this.socialMediaForm.controls.webUrl.status !== 'INVALID') {
          _social.link = this.socialMediaForm.value.webUrl
        }
      })
    } catch (error) {
      console.log(error)
    }

    let _socialLinks = []
    try {
      const { socialLinks } = this
      _socialLinks = socialLinks.map(_social => ({
        socialMediaPortalsID: _social.id,
        socialMediaCode: _social.type,
        title: _social.title,
        linkURL: _social.link
      }))
    } catch { }

    const toSend = {
      userID: this.userProfile.UserID,
      providerID: this.userProfile.ProviderID,
      modifiedBy: this.userProfile.UserID,
      website: (this.selectedSocialsite && Object.keys(this.selectedSocialsite).length && this.socialSites) ? this.socialSites : null,
      socialMedias: _socialLinks
    }
    this.listenSocialChanges = false
    loading(true)
    this._settingService.updateSocialMediaSettings(toSend).subscribe((res: JsonResponse) => {
      this.listenSocialChanges = true
      loading(false)
      if (res.returnId > 0) {
        this.socialToggler = false
        this._toastr.success(res.returnText, res.returnStatus)
        this.getUserDetail(this.userProfile.UserID)
      } else {
        this._toastr.error(res.returnText, res.returnStatus)
      }
    }, () => {
      this._toastr.error('Error while processing your request, please try again later', 'Failed')
      loading(false)
    })
  }

  resetSocialMedia() {
    this.socialToggler = false
    this.socialMediaForm.reset()
    this.setSocialLinks()
  }

  addMoreAddress($office?: IAddressInfo) {
    const modalRef = this.modalService.open(ProviderOfficeDialogComponent, {
      size: 'lg', centered: true, windowClass: 'large-modal', backdrop: 'static', keyboard: false
    })

    modalRef.componentInstance.countryList = this.countryList
    modalRef.componentInstance.userProfile = this.userProfile
    modalRef.componentInstance.action = $office ? 'edit' : 'add'
    modalRef.componentInstance.editOfficeData = $office ? $office : null
    modalRef.result.then((result: string) => {
      if (result)
        this.getOfficeList()
    })
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open'))
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden'
    }, 0)
  }

  deleteAddress($office: IAddressInfo) {
    const _modalData: ConfirmDialogContent = {
      messageTitle: 'Delete Address',
      messageContent: `Are you sure you want to delete this address?`,
      buttonTitle: 'Yes',
      data: null
    }

    const modalRef = this.modalService.open(ConfirmDialogGenComponent, {
      size: 'lg', centered: true, windowClass: 'medium-modal', backdrop: 'static', keyboard: false
    })

    modalRef.result.then((result: string) => {
      if (result) {
        this._settingService.deleteBusinessAddress($office.ProviderOfficeID).subscribe((res: JsonResponse) => {
          loading(false)
          this.getOfficeList()
          if (res.returnId > 0) {
            this._toastr.success('Address deleted successfully', 'Success')
          } else {
            this._toastr.error(res.returnText, 'Failed')
          }
        }, () => loading(false))
      }
    })

    modalRef.componentInstance.modalData = _modalData
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open'))
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden'
    }, 0)
  }

  getOfficeList() {
    loading(true)
    this._settingService.getProviderOfficeList(this.userProfile.ProviderID).subscribe((res: JsonResponse) => {
      loading(false)
      if (res.returnId > 0) {
        this.officeAddressList = res.returnObject
      } else {
        this.officeAddressList = []
      }
    }, () => {
      loading(false)
      this.officeAddressList = []
    })
  }

  onOfficeVisibilityChange() {
    this.preventOfficeLocation = true
    this.IsShowOfficeOnProfile = !this.IsShowOfficeOnProfile
    this._settingService.updateProviderToggles(this.userProfile.ProviderID, this.IsShowOfficeOnProfile).subscribe(
      () => loading(false), () => loading(false))
    setTimeout(() => { this.preventOfficeLocation = this.IsShowOfficeOnProfile }, 10)
  }

  onNewsVisibilityChange(settingsAcc) {
    this.preventNewsAcc = true
    this.IsShowNewsOnProfile = !this.IsShowNewsOnProfile
    this._settingService.updateProviderNewsToggles(this.userProfile.ProviderID, this.IsShowNewsOnProfile).subscribe(
      () => loading(false), () => loading(false))
    setTimeout(() => { this.preventNewsAcc = this.IsShowNewsOnProfile }, 10)
  }

  onColorChange = (event) => this.hompageToggler = true


  onColorChangeIndustry = (event) => this.industryToggler = true

  onOfficePinBorderColor = (event) => this.officeToggler = true

  updateOfficeSettings() {
    const toSend = {
      userID: this.userProfile.UserID,
      providerID: this.userProfile.ProviderID,
      officePinBorderColor: this.IsShowOfficePinBorderColor && this.officePinBorderColor ? this.officePinBorderColor : null
    }
    loading(true)
    this._settingService.updateOfficeSettings(toSend).subscribe((res: JsonResponse) => {
      loading(false)
      if (res.returnId > 0) {
        this.officeToggler = false
        this._toastr.success(res.returnText, res.returnStatus)
        this.getUserDetail(this.userProfile.UserID)
      } else {
        this._toastr.error(res.returnText, res.returnStatus)
      }
    }, () => {
      this._toastr.error('There was an erroer while processing your request, Please try again later', "Failed")
      loading(false)
    })
  }

  addNews($news) {
    const modalRef = this.modalService.open(NewsDialogComponent, {
      size: 'lg', centered: true, windowClass: 'large-modal', backdrop: 'static', keyboard: false
    })
    modalRef.result.then((result: string) => {
      if (result)
        this.getNewsList()
    })
    modalRef.componentInstance.newsData = $news
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open'))
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden'
    }, 0)
  }

  deleteNews($news: INews) {
    const _modalData: ConfirmDialogContent = {
      messageTitle: 'Delete News',
      messageContent: `Are you sure you want to delete this news?`,
      buttonTitle: 'Yes',
      data: null
    }
    const modalRef = this.modalService.open(ConfirmDialogGenComponent, {
      size: 'lg', centered: true, windowClass: 'medium-modal', backdrop: 'static', keyboard: false
    })
    modalRef.result.then((result: string) => {
      if (result) {
        this._settingService.deleteProviderNews($news.NewsID).subscribe((res: JsonResponse) => {
          loading(false)
          this.getOfficeList()
          if (res.returnId > 0) {
            this._toastr.success('News deleted successfully', 'Success')
            this.getNewsList()
          } else {
            this._toastr.error(res.returnText, 'Failed')
          }
        }, () => loading(false))
      } else {
        loading(false)
      }
    })
    modalRef.componentInstance.modalData = _modalData
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open'))
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden'
    }, 0)
  }

  getNewsList() {
    loading(true)
    this._settingService.getProviderNewsList(this.info.ProviderID).subscribe((res: JsonResponse) => {
      loading(false)
      this.newsList = res.returnId > 0 ? res.returnObject : []
    }, () => loading(false))
  }

  getServiceImage = ($imagePath) => getImagePath(ImageSource.FROM_SERVER, $imagePath, ImageRequiredSize.original)

  filterService = (freight: FreightModel) => !freight.LogServImagesArr

  onImageSelect(freight: FreightModel, image: string) {
    loading(true)
    this._settingService.updateProviderLogisticImage(freight.ProvLogServID, image).subscribe((res: JsonResponse) => {
      loading(false)
      if (res.returnId > 0)
        freight.LogServSelectedImage = image
    }, () => loading(false))
  }

  onServiceVisibilityChange($service: FreightModel) {
    $service.IsServiceOnProfile = !$service.IsServiceOnProfile
    loading(true)
    this._settingService.updateIsShowServiceOnProfile($service.ProvLogServID, $service.IsServiceOnProfile).subscribe(res => {
      loading(false)
    }, () => loading(false))
  }

  onServiceDisplayChange($event) {
    this.preventOfferServices = true
    this.IsShowServiceOnProfile = !this.IsShowServiceOnProfile
    this._settingService.updateServiceDisplayOnProfile(this.userProfile.ProviderID, this.IsShowServiceOnProfile).subscribe(res =>
      loading(false), () => loading(false))
    setTimeout(() => { this.preventOfferServices = this.IsShowServiceOnProfile }, 10)
  }

  getModeText($mode: string) {
    let _text = ''
    switch ($mode) {
      case "SEA":
        _text = 'Sea'
        break
      case "AIR":
        _text = 'Air'
        break
      case "TRUCK":
        _text = 'Ground'
        break
      case "WAREHOUSE":
        _text = 'Warehouse'
        break
      default:
        _text = '$mode'
        break
    }
    return _text
  }

  getModeIcon($mode: string) {
    let _text = ''
    switch ($mode) {
      case "SEA":
        _text = 'icon_ship02.svg'
        break
      case "AIR":
        _text = 'icon_plane.svg'
        break
      case "TRUCK":
        _text = 'icons_cargo_truck_grey_dark.svg'
        break
      case "WAREHOUSE":
        _text = 'Icons_Warehousing_Grey_dark.svg'
        break
      default:
        _text = 'ic_ship.svg'
        break
    }
    return _text
  }

  selectIndustry(obj: IIndustry, indOff) {
    obj.IsSelected = !obj.IsSelected
    this.industryToggler = true
  }


  updateIndustrySettings() {
    const toSend = {
      industryIconBorderColor: this.isIndustryColor && this.industryColor ? this.industryColor : null,
      industrySettings: this.industriesSolution
    }
    loading(true)
    this._settingService.updateIndustrySettings(toSend, this.userProfile.ProviderID).subscribe((res: JsonResponse) => {
      loading(false)
      if (res.returnId > 0) {
        this._toastr.success('Industry updated successfully', 'Success')
        this.industryToggler = false
        this.getUserDetail(this.userProfile.UserID)
      } else {
        this._toastr.error(res.returnText, res.returnStatus)
      }
    }, () => loading(false))
  }

  reseIndustry() {
    this.industryToggler = false
    this.industriesSolution = this.info.IndustrySettings
    try {
      if (this.info.DefaultIndustryIconBorderColor && this.info.IndustryIconBorderColor) {
        this.isIndustryColor = true
        this.industryColor = this.info.IndustryIconBorderColor
      } else {
        this.isIndustryColor = false
        this.industryColor = this.info.DefaultIndustryIconBorderColor
      }
    } catch (error) {
      console.log(error)
    }
  }

  onBannerImageSelect($image: any) {
    this.selectedBannerImage = $image
    this.bannerToggler = true
  }

  onProfileBannerTextColorChange = (event) => this.bannerToggler = true

  updateProfileBannerData() {
    const toSend = {
      userID: this.userProfile.UserID,
      providerID: this.userProfile.ProviderID,
      isShowBannerText: this.IsShowBannerText,
      bannerText: this.bannerText,
      bannerTextColor: this.IsProfileBannerTextColor && this.selectedProfileBannerTextColor ? this.selectedProfileBannerTextColor : null,
      bannerSelectedImageID: (this.selectedBannerImage && this.selectedBannerImage.DocumentID) ? this.selectedBannerImage.DocumentID : -1,
      bannerSelectedImage: (this.selectedBannerImage && this.selectedBannerImage.DocumentFile) ? this.selectedBannerImage.DocumentFile : null
    }
    loading(true)
    this._settingService.updateBannerSettings(toSend).subscribe((res: JsonResponse) => {
      loading(false)
      if (res.returnId > 0) {
        this.bannerToggler = false
        this._toastr.success(res.returnText, res.returnStatus)
        this.getUserDetail(this.userProfile.UserID)
      } else {
        this._toastr.error(res.returnText, res.returnStatus)
      }
    }, () => {
      loading(false)
      this._toastr.error('There was an erroer while processing your request, Please try again later', "Failed")
    })
  }

  selectAssociation(_aff: AssociationModel) {
    this.associationToggler = true
    _aff.IsSelected = !_aff.IsSelected
  }

  validateRef = (_aff: AssociationModel) => _aff.IsValidRef = (_aff.AssnWithRefNo) ? true : false

  validateWeb = (_aff: AssociationModel) =>
    _aff.IsValidWeb = (!_aff.AssnWithWebAdd || !GEN_URL.test(_aff.AssnWithWebAdd)) ? false : true

  validateName = (_aff: AssociationModel) => _aff.IsValidName = (!_aff.AssInpName) ? false : true

  setRefNumber = (_aff: AssociationModel) => _aff.IsEdit = false

  onAffEdit(_aff: AssociationModel, $event) {
    if (_aff.IsSelected) _aff.IsEdit = true
    $event.preventDefault()
    $event.stopPropagation()
  }

  resetAssociation() {
    this.allAssociations = cloneObject(this.info.Association)
    this.associationToggler = false
  }

  ngNameChat = (_aff: AssociationModel) => _aff.AssnWithName = _aff.AssInpName

  updateAssociationList() {
    const { allAssociations } = this
    try {
      const _checAff = allAssociations.filter(_aff => _aff.IsSelected)
      if (!_checAff || _checAff.length === 0) {
        this._toastr.warning('Please Select atleast on Affiliation')
        document.getElementById(`aff-${allAssociations[0].AssnWithCode}`)
          .scrollIntoView({ behavior: 'smooth', block: 'center' })
        return
      }
      const _affs = allAssociations
        .filter(_aff => _aff.IsSelected &&
          (!_aff.IsValidRef || (_aff.AssnWithCode === 'OTHER' && (!_aff.IsValidName || !_aff.IsValidWeb))))
      if (isArrayValid(_affs, 0)) {
        loading(false)
        try {
          document.getElementById(`aff-${_affs[0].AssnWithCode}`).scrollIntoView({ behavior: 'smooth', block: 'center' })
        } catch { }
        if (_affs.filter(_aff => _aff.IsEdit && _aff.IsSelected).length > 0) {
          this._toastr.warning('Please Submit the unfinished changes.', 'Affiliation Error')
        } else {
          this._toastr.warning('Please enter required information on the selected affiliations.', 'Affiliation Error')
        }
        return
      }
    } catch { }
    const assToSend = allAssociations.map((_assoc: AssociationModel) => {
      return {
        providerID: this.userProfile.ProviderID,
        createdBy: (_assoc.ProviderAssnID && _assoc.ProviderAssnID > 0) ? -1 : this.userProfile.UserID,
        modifiedBy: (_assoc.ProviderAssnID && _assoc.ProviderAssnID > 0) ? this.userProfile.UserID : -1,
        assnWithID: _assoc.AssnWithID,
        providerAssnID: _assoc.ProviderAssnID,
        isActive: _assoc.IsSelected ? _assoc.IsSelected : false,
        assnWithBL: _assoc.AssnWithBL,
        assnWithRefNo: _assoc.AssnWithRefNo,
        otherAssnWithName: _assoc.AssnWithBL && _assoc.AssnWithBL.toLowerCase() === 'other' ? _assoc.AssnWithName : null,
        otherAssnWithImage: _assoc.AssnWithBL && _assoc.AssnWithBL.toLowerCase() === 'other' ? _assoc.AssnWithImage : null,
        otherAssnWithWebAdd: _assoc.AssnWithBL && _assoc.AssnWithBL.toLowerCase() === 'other' ? _assoc.AssnWithWebAdd : null
      }
    })
    loading(true)
    this._settingService.updateAssociationList(assToSend).subscribe((res: JsonResponse) => {
      loading(false)
      if (res.returnId > 0) {
        this.associationToggler = false
        this._toastr.success(res.returnText, res.returnStatus)
        this.getUserDetail(this.userProfile.UserID)
      } else {
        this._toastr.error(res.returnText, res.returnStatus)
      }
    }, () => {
      loading(false)
      this._toastr.error('There was an erroer while processing your request, Please try again later', "Failed")
    })
  }

  setMailNotifs() {
    const { apiServerEmails } = this
    this.mailNotificatoinToggler = false
    const { masterMailNotif } = this
    this.mailNotif = []
    masterMailNotif.forEach(_mail => {
      const _obj = apiServerEmails.filter(_e => _e.ModuleBL === _mail.moduleBL)
      let _mails = [], _modID = -1
      if (isArrayValid(_obj, 0)) {
        _modID = _obj[0].ModuleID
        const { EmailAddresses } = _obj[0]
        if (EmailAddresses)
          _mails = EmailAddresses.map(_m => ({ e_mail: _m, isRemovable: true }))
      }
      if (_mail.isMode && this.availModes.includes(_mail.moduleBL.split('_')[0]))
        this.mailNotif.push({ ..._mail, moduleID: _modID, mailList: _mails })
      else if (!_mail.isMode)
        this.mailNotif.push({ ..._mail, moduleID: _modID, mailList: _mails })
    })

    const { mailNotif } = this
    this.mailNoficiationForm = new FormGroup({})
    mailNotif.filter(_mail => _mail.isMail).forEach(_mail => {
      this.mailNoficiationForm.addControl(_mail.moduleBL, new FormControl())
    })
    this.mailNoficiationForm.valueChanges.pipe(untilDestroyed(this)).subscribe(val => {
      for (const key in this.mailNoficiationForm.controls) {
        if (this.mailNoficiationForm.controls.hasOwnProperty(key) && this.listenNotifChanges) {
          if (this.mailNoficiationForm.controls[key].dirty) {
            this.mailNotificatoinToggler = true
          }
        }
      }
    })
    setTimeout(() => { this.listenNotifChanges = true }, 10)
  }

  onMailNotifInput($mail: IEmailNotif, $event: any) {
    $mail.hasTouched = true
    if (!$event.target.value || $event.target.value === undefined || $event.target.value === null || $event.target.value === '') {
      if (this.newPushedEmails.length === 0)
        $mail.hasTouched = false
    }
    if (($event.keyCode === 32 || $event.keyCode === 13)) {
      if ($event.target.value) {
        const _email: string = $event.target.value
        this.addNotifEmail($mail, _email)
        $event.preventDefault()
      }
    }
  }

  addNotifEmail($mailNotif: IEmailNotif, $mail: string) {
    const { basicEmailRegex } = this
    if (!basicEmailRegex.test($mail)) {
      this._toastr.error('Invalid email entered.', 'Error')
      return
    }

    if (!ValidateEmail($mail)) {
      this._toastr.warning('Invalid email entered.', 'Error')
      return
    }

    if ($mailNotif.mailList.length === 12) {
      this._toastr.warning('Email Limit Exceeded', 'Error')
      return
    }

    const { mailList } = $mailNotif
    const _mailExists = mailList.filter(_mail => _mail.e_mail === $mail)
    if (isArrayValid(_mailExists, 0)) {
      this._toastr.warning('Email already added', 'Error')
      return
    }

    this.mailNotificatoinToggler = true
    $mailNotif.mailList.push({ e_mail: $mail, isRemovable: true })
    this.mailNoficiationForm.controls[$mailNotif.moduleBL].setValue('')
  }

  removeNotifEmail($mailNotif: IEmailNotif, $index: number) {
    this.mailNotificatoinToggler = true
    $mailNotif.mailList.splice($index, 1)
  }

  insertNotifEmails() {
    const { basicEmailRegex, mailNotif } = this
    try {
      mailNotif.filter(_mail => _mail.isMail).forEach(_mail => {
        if (this.mailNoficiationForm.controls[_mail.moduleBL].value) {
          const { value } = this.mailNoficiationForm.controls[_mail.moduleBL]
          if (String(value).match(basicEmailRegex))
            this.addNotifEmail(_mail, value)
        }
        this.mailNoficiationForm.controls[_mail.moduleBL].setValue('')
      })
    } catch (_error) {
      console.log('Auto Email Add Error: ', _error)
    }
  }

  saveNotificationEmails() {
    this.insertNotifEmails()
    const { mailNotif } = this
    const _allEmail = mailNotif.filter(_mail => _mail.moduleBL === 'ALL')[0]
    if (!isArrayValid(_allEmail.mailList, 0)) {
      this._toastr.error("Please add at least 1 email in the ''All'' section.", "Insufficent Data")
      return
    }

    const _toSend: IEmailNotifResp[] = mailNotif.filter(_mail => _mail.isMail).map(_mail => {
      const _mailList = _mail.mailList.map(_m => _m.e_mail)
      return {
        ProviderID: this.info.ProviderID,
        ModuleBL: _mail.moduleBL,
        ModuleID: _mail.moduleID,
        EmailAddresses: isArrayValid(_mailList, 0) ? _mailList : null,
      }
    })

    loading(true)
    this._settingService.updateNotificationEmails(_toSend, this.userProfile.UserID).subscribe((res: JsonResponse) => {
      loading(false)
      if (res.returnId > 0) {
        this.getNotificationEmail()
        this._toastr.success('Emails updated Successfully', 'Success')
      } else {
        this._toastr.error(res.returnText, res.returnStatus)
      }
    }, () => {
      loading(false)
      this._toastr.error('There was a problem while process you request, Please try latere.')
    })
  }

  showNotifControl($mail: IEmailNotif) {
    let showControl = false
    if ($mail.moduleBL === 'SEA_FCL' && this.seaTypeToggler)
      showControl = true
    if ($mail.moduleBL === 'SEA_LCL' && this.seaTypeToggler)
      showControl = true
    if ($mail.moduleBL === 'AIR' && this.airTypeToggler)
      showControl = true
    if ($mail.moduleBL === 'TRUCK' && this.groundTypeToggler)
      showControl = true
    if ($mail.moduleBL === 'WAREHOUSE' && this.wareHouseTypeToggler)
      showControl = true
    if ($mail.moduleBL === 'ASSAYER' && this.assayerToggler)
      showControl = true
    if ($mail.moduleBL === 'INSURANCE' && this.insuranceToggler)
      showControl = true
    if ($mail.moduleBL === 'ALL' || $mail.moduleBL === 'OTHERS')
      showControl = true
    return showControl
  }

  affSearchType = (text$: Observable<string>) => text$.pipe(debounceTime(200), map(term => this.filterAff(term)))

  filterAff(term: string) {
    if (term === '' || !term) {
      return this.masterAssociations.filter(_aff => _aff.AssnWithCode !== 'OTHER')
    } else {
      return this.masterAssociations.filter(_aff => JSON.stringify(_aff).toLowerCase().indexOf(term.toLowerCase()) > -1 && _aff.AssnWithCode !== 'OTHER')
    }
  }

  affFormatter = (_aff: AssociationModel) => _aff.AssnWithName ? _aff.AssnWithName : _aff.AssnWithCode

  addAssociation() {
    if (this.selectedAssn && typeof this.selectedAssn === 'object') {
      const { selectedAssn } = this
      if (this.allAssociations.filter(_aff => _aff.AssnWithID === selectedAssn.AssnWithID && _aff.AssnWithCode !== 'OTHER').length === 0) {
        selectedAssn.IsSelected = true
        this.allAssociations.push(cloneObject(selectedAssn))
      }
      this.selectedAssn = null
    }
  }

  public onFocus(e: Event): void {
    e.stopPropagation()
    setTimeout(() => { e.target.dispatchEvent(new Event('input')) }, 0)
  }

  onKeyPress(e: any) {
    if (e.keyCode === 13) {
      e.preventDefault()
      e.stopPropagation()
      this.addAssociation()
      return
    }
  }

  confirmPassword(event) {
    const element = event.currentTarget.nextSibling.nextSibling
    element.type = (element.type === "password" && element.value) ? 'text' : 'password'
  }

  resetPasswordForm() {
    this.newPass = null
    this.showPassValidator = false
    this.newPasswordStatus = {
      lengthValid: false, upperCaseValid: false, lowerCaseValid: false,
      digitValid: false, specialCharValid: false
    }
    this.credentialInfoForm.reset()
  }

  ngOnDestroy() { }
}
