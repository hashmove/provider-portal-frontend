import { EventEmitter, Injectable, Input, Output, ViewChild } from "@angular/core";
import { after, before, equals, getLoggedUserData, removeDuplicates } from "../../../../constants/globalFunctions";
import { IRequestStatus, BookingStatus, ICarrierFilter, IProviderFilter, ICompanyFilter, FilterdBookings } from "../../../../interfaces";
import * as moment from 'moment'

@Injectable()
export class InvoiceFilterHelper {
    public hmBookingNum
    public selectedBookStatusCode = null
    public selectedShipStatusCode = null
    public selectedInvoiceStatusCode = null
    public selectedPaymentStatusCode = null
    public selectedCargoType = null
    public selectedRequestStatus: IRequestStatus = null
    public cargoTypeList = ['FCL', 'LCL']
    public selectedShippingModeCode = null
    public isMarketplace = false
    public filterbyContainerType
    public fromDate
    public toDate
    public filterOrigin: any = {}
    public filterDestination: any = {}
    public bookingStatuses: Array<BookingStatus> = []
    public shippingStatuses: Array<BookingStatus> = []
    public requestStatusList: Array<IRequestStatus> = []
    public selectedCarrier: ICarrierFilter
    public selectedProvider: IProviderFilter
    public selectedCompany: ICompanyFilter
    public allShippingLines = []
    public carriersList: Array<ICarrierFilter> = []
    public providersList: Array<IProviderFilter> = []
    public bookingList: Array<FilterdBookings> = []
    public companyList: Array<ICompanyFilter> = []
    public shippingModes: Array<ICarrierFilter> = []
    public sortBy = 'date' // or price
    @ViewChild('d') eventDate
    @ViewChild('calenderField') calenderField: any
    hoveredDate: any
    public minDate: any
    public maxDate: any
    ports = []
    loginUser = getLoggedUserData()
    isVirtual = false
    public blNumber = ''
    groundArray = []
    isInit = false
    currPageMask = 1
    invoiceStatusList: BookingStatus[] = []
    paymentStatusList: BookingStatus[] = []

    isHovered = date => this.fromDate && !this.toDate && this.hoveredDate && after(date, this.fromDate) && before(date, this.hoveredDate)
    isInside = date => after(date, this.fromDate) && before(date, this.toDate)
    isFrom = date => equals(date, this.fromDate)
    isTo = date => equals(date, this.toDate)
    @Input() public currentTab = 'tab-current'
    
    constructor() {

    }

    clearSearchData() {
        this.fromDate = {}
        this.toDate = {}
        this.isMarketplace = false
        this.filterDestination = {}
        this.filterOrigin = {}
        this.hmBookingNum = null
        this.selectedBookStatusCode = null
        this.selectedShipStatusCode = null
        this.selectedShippingModeCode = null
        this.selectedInvoiceStatusCode = null
        this.selectedPaymentStatusCode = null
        this.selectedProvider = {} as any
        this.selectedCarrier = {} as any
        this.selectedCompany = {} as any
        this.selectedCargoType = null
        this.sortBy = 'date'
        this.currPageMask = 1
        this.blNumber = null
        try { localStorage.removeItem('book-filter') } catch { }
        setTimeout(() => {
            try { this.calenderField.nativeElement.value = null } catch { }
        }, 0)
    }

    setInitDate() {
        const date = new Date()
        this.minDate = { month: date.getMonth() + 1, day: date.getDate(), year: date.getFullYear() }
        this.maxDate = {
            year: ((this.minDate.month === 12) ? date.getFullYear() + 1 : date.getFullYear()),
            month: moment(date).add(30, 'days').month() + 1,
            day: moment(date).add(30, 'days').date()
        }
    }


    getFilteredPorts(term, transportMode) {
        const { ports } = this
        let toSend = []
        if (transportMode === 'AIR') {
            const _firstIteration: Array<any> = ports.filter(v => v.code.toLowerCase().indexOf(term.toLowerCase()) > -1)
            const _secondIteration: Array<any> = ports.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1)
            const _combinedArr = _firstIteration.concat(_secondIteration)
            toSend = removeDuplicates(_combinedArr, 'code')
        } else {
            if (this.groundArray && this.groundArray.length > 0) {
                const { groundArray } = this
                toSend = ports.concat(groundArray).filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1)
            } else {
                toSend = ports.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1)
            }
        }
        return toSend
    }

    getFilterObject() {
        let _fromDate, _toDate
        _fromDate = this.fromDate && this.fromDate.day ? this.getDatefromObj(this.fromDate) : null
        _toDate = this.toDate && this.toDate.day ? this.getDatefromObj(this.toDate) : null
        let _selectedShippingModeId: any
        try {
            if (this.shippingModes.length > 0) {
                const { selectedShippingModeCode } = this
                _selectedShippingModeId = this.shippingModes.filter(mode => mode.code.toLowerCase() === selectedShippingModeCode.toLowerCase())[0].id
            }
        } catch { }
        return {
            userID: this.loginUser.UserID,
            providerID: this.loginUser.ProviderID,
            bookingNumber: (this.hmBookingNum) ? this.hmBookingNum.replace(/\s/g, '') : null,
            bookingStatusCode: (this.selectedBookStatusCode && this.selectedBookStatusCode !== 'null') ? this.selectedBookStatusCode : null,
            shipmentStatusCode: (this.selectedShipStatusCode && this.selectedShipStatusCode !== 'null') ? this.selectedShipStatusCode : null,
            invoiceStatusCode: (this.selectedInvoiceStatusCode && this.selectedInvoiceStatusCode !== 'null') ? this.selectedInvoiceStatusCode : null,
            paymentStatusCode: (this.selectedPaymentStatusCode && this.selectedPaymentStatusCode !== 'null') ? this.selectedPaymentStatusCode : null,
            polID: (this.filterOrigin && this.filterOrigin.id) ? this.filterOrigin.id : 0,
            polCode: (this.filterOrigin && this.filterOrigin.code) ? this.filterOrigin.code : null,
            polType: (this.filterOrigin && this.filterOrigin.code && this.selectedShippingModeCode && this.selectedShippingModeCode !== 'null') ? this.filterOrigin.type.toUpperCase() : null,
            podID: (this.filterDestination && this.filterDestination.id) ? this.filterDestination.id : 0,
            podCode: (this.filterDestination && this.filterDestination.code) ? this.filterDestination.code : null,
            podType: (this.filterDestination && this.filterDestination.code && this.selectedShippingModeCode && this.selectedShippingModeCode !== 'null') ? this.filterDestination.type.toUpperCase() : null,
            cargoType: (this.selectedCargoType && this.selectedCargoType !== 'null') ? this.selectedCargoType : null,
            shippingModeID: (_selectedShippingModeId) ? _selectedShippingModeId : 0,
            fromDate: (_fromDate) ? _fromDate : null,
            toDate: (_toDate) ? _toDate : null,
            filterProviderID: 0,
            filterCompanyID: (this.selectedCompany && this.selectedCompany.ID) ? this.selectedCompany.ID : 0,
            carrierID: (this.selectedCarrier && this.selectedCarrier.id) ? this.selectedCarrier.id : 0,
            sortBy: this.sortBy,
            sortDirection: 'DESC',
            pageSize: 10,
            pageNumber: this.currPageMask,
            blNumber: (this.blNumber) ? this.blNumber : null,
            tab: this.currentTab === 'tab-active' ? 'CURRENT' : 'TOTAL',
        }
    }

    getDatefromObj(dateObject) {
        let toSend = null
        try {
            toSend = moment(new Date(dateObject.year, dateObject.month - 1, dateObject.day)).format('YYYY-MM-DD')
        } catch (error) { }
        return toSend
    }

}