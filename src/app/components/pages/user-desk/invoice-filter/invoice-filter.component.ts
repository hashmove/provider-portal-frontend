import { Component, OnInit, ViewEncapsulation, OnChanges, SimpleChanges, OnDestroy, EventEmitter, Input, Output } from '@angular/core'
import { loading, getImagePath, ImageSource, ImageRequiredSize, getProviderImage, changeCase, after, isArrayValid, isMobile } from '../../../../constants/globalFunctions'
import { NgbDateStruct, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap'
import { ViewBookingService } from '../view-booking/view-booking.service'
import { SharedService } from '../../../../services/shared.service'
import { debounceTime } from 'rxjs/operators/debounceTime'
import { untilDestroyed } from 'ngx-take-until-destroy'
import { Observable } from 'rxjs/Observable'
import { ToastrService } from 'ngx-toastr'
import { map } from 'rxjs/operators/map'
import * as moment from 'moment'
import { InvoiceFilterHelper } from './invoice-filter.helper.component'
import { BookingStatus, JsonResponse } from '../../../../interfaces'
import { firstBy } from 'thenby'

@Component({
  selector: 'app-invoice-filter',
  templateUrl: './invoice-filter.component.html',
  styleUrls: ['./invoice-filter.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InvoiceFilterComponent extends InvoiceFilterHelper implements OnInit, OnChanges, OnDestroy {
  @Output() public onListChange = new EventEmitter<any>()
  @Input() public currPage = 1
  isMobile = isMobile()

  constructor(
    private _parserFormatter: NgbDateParserFormatter,
    private _bookingService: ViewBookingService,
    private _toastr: ToastrService,
    private _dataService: SharedService
  ) {
    super()
  }

  async ngOnInit() {
    setTimeout(async () => {
      this.groundArray = changeCase(await this._bookingService.getPortsData('ground').toPromise() as any, 'camel')
    }, 0);
    loading(true)
    setTimeout(() => {
      try {
        if (localStorage.getItem('isVirtualAirline') && JSON.parse(localStorage.getItem('isVirtualAirline')))
          this.isVirtual = true
      } catch { }
    }, 0)

    this._dataService.bookingsRefresh.pipe(untilDestroyed(this)).subscribe(state => {
      if (state) {
        this.filterRecords()
        this._dataService.bookingsRefresh.next(null)
        localStorage.removeItem('book-filter')
      }
    })
    this.setInitDate()
    try {
      const _modes = await this._bookingService.getShipModes().toPromise() as Array<any>
      try {
        const res: JsonResponse = await this._bookingService.getLogisticServices(this.loginUser.ProviderID).toPromise() as any
        const logisticServices = res.returnObject
        const freightList: Array<string> = []
        logisticServices.forEach(servie => {
          const { LogServCode } = servie
          if (LogServCode.toUpperCase() === 'SEA_FFDR')
            freightList.push('SEA')
          else if (LogServCode.toUpperCase() === 'AIR_FFDR')
            freightList.push('AIR')
          else if (LogServCode.toUpperCase() === 'TRUK')
            freightList.push('TRUCK')
          else if (LogServCode.toUpperCase() === 'WRHS')
            freightList.push('WAREHOUSE')
          else if (LogServCode.toUpperCase() === 'ASYER' || LogServCode.toUpperCase() === 'ASSAYER')
            freightList.push('ASSAYER')
        })
        const _filteredModes = _modes.filter(freight => freightList.includes(freight.code))
        this.shippingModes = _filteredModes
        if (_filteredModes.length === 1)
          this.selectedShippingModeCode = _filteredModes[0].code
      } catch {
        const _filtered = _modes.filter(mode => mode.code !== 'RAIL' && mode.code !== 'HYPERLOOP')
        this.shippingModes = _filtered
      }
    } catch { }
    try { await this.getBookingStatuses((this.selectedShippingModeCode) ? this.selectedShippingModeCode : 'SEA') } catch { }
    try { await this.getShippingStatus() } catch { }
    try { await this.setPortsData((this.selectedShippingModeCode) ? this.selectedShippingModeCode : 'SEA', false) } catch { }
    try {
      const _companies = await this._bookingService.getAllCompanies(this.loginUser.ProviderID).toPromise() as any
      this.companyList = _companies
    } catch { }
    try {
      const _carriers = await this._bookingService.getAllCarriers().toPromise() as any
      this.carriersList = _carriers
    } catch { }
    this.getInvoiceStatus()
    this.getPaymentStatus()
    this.filterRecords()
  }

  getInvoiceStatus() {
    this._bookingService.getBookingInvoiceStatus().subscribe((res: JsonResponse) => {
      if (res.returnId > 0) {
        const _invoiceStatusList: BookingStatus[] = res.returnObject.filter(_status =>
          _status.BusinessLogic !== 'INVOICE_SENT_TO_CUSTOMER' &&
          _status.BusinessLogic !== 'CUSTOMER_INVOICE_UPLOADED' &&
          _status.BusinessLogic !== 'CUSTOMER_INVOICE_REUPLOADED' &&
          // _status.BusinessLogic !== 'PAYMENT_RECEIVED' &&
          _status.BusinessLogic !== 'PARTIAL_PAYMENT_RECEIVED' &&
          _status.BusinessLogic !== 'PARTIAL_PAID'
        )
        this.invoiceStatusList = _invoiceStatusList.sort(firstBy('StatusName'))
      }
    })
  }
  getPaymentStatus() {
    this._bookingService.getBookingPaymentStatus().subscribe((res: JsonResponse) => {
      if (res.returnId > 0) {
        this.paymentStatusList = res.returnObject
      }
    })
  }

  async getRequestStatusList() {
    try {
      const res = await this._bookingService.getRateRequestStatusList().toPromise() as JsonResponse
      if (res.returnId > 0 && typeof res.returnObject === 'object' && isArrayValid(res.returnObject, 0))
        this.requestStatusList = res.returnObject
      else
        this.requestStatusList = []
    } catch { }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.currPage && changes.currPage.currentValue) {
      this.currPageMask = changes.currPage.currentValue
      this.currPageMask = changes.currPage.currentValue
    }
    if (this.carriersList && this.carriersList.length > 0 && changes.currPage && changes.currPage.currentValue) {
      if ((changes.currPage && changes.currPage.currentValue) || (this.carriersList && this.carriersList.length > 0))
        this.onFilterAction()
    }
    if (this.carriersList && this.carriersList.length > 0 && changes.currentTab && changes.currentTab.currentValue)
      this.onFilterAction()
  }

  clearFilter(event) {
    event.preventDefault()
    event.stopPropagation()
    this.currPageMask = 1
    this.currPage = 1
    this.clearSearchData()
    this.filterRecords()
  }


  filterPortsFormatter = (port) => port.title
  filterCarrierFormatter = (carrier) => carrier.title

  portsFilter = (text$: Observable<string>) => (text$.pipe(debounceTime(200),
    map(term => !term || term.length < 3 ?
      [] : this.getFilteredPorts(term, this.selectedShippingModeCode))))


  carrierFilter = (text$: Observable<string>) => text$.pipe(debounceTime(200), map(term => (!term || term.length < 2) ? []
    : this.carriersList.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1)))

  companyFilter = (text$: Observable<string>) => text$.pipe(debounceTime(200), map(term => (!term || term.length < 2) ? []
    : this.companyList.filter(v => v.Title.toLowerCase().indexOf(term.toLowerCase()) > -1)))

  filtercompanyFormatter = (company) => company.Title

  showbutton() {
    try { this.eventDate.toggle() } catch { }
  }

  onDateSelection(date: NgbDateStruct) {
    let parsed = ''
    if (!this.fromDate && !this.toDate)
      this.fromDate = date
    else if (this.fromDate && !this.toDate && after(date, this.fromDate))
      this.toDate = date
    else {
      this.toDate = null
      this.fromDate = date
    }
    if (this.fromDate)
      parsed += this._parserFormatter.format(this.fromDate)
    if (this.toDate)
      parsed += ' - ' + this._parserFormatter.format(this.toDate)
    setTimeout(() => {
      this.calenderField.nativeElement.value = parsed
      if (this.toDate)
        this.eventDate.toggle()
    }, 0)
  }

  shippings = (text$: Observable<string>) => text$.pipe(debounceTime(200), map(term => (!term || term.length < 3) ? []
    : this.ports.filter(v => v.title.toLowerCase().indexOf(term.toLowerCase()) > -1 ||
      v.shortName.toLowerCase().indexOf(term.toLowerCase()) > -1 || v.code.toLowerCase().indexOf(term.toLowerCase()) > -1)))

  shippingFormatter = (port) => port.title

  async getBookingStatuses(selectedShippingModeCode) {
    const toSend = (selectedShippingModeCode === 'WAREHOUSE') ? 'WAREHOUSE' : 'BOOKING'
    try {
      const res = await this._bookingService.getBookingStatuses(toSend).toPromise() as JsonResponse
      if (res.returnId > 0) {
        this.bookingStatuses = res.returnObject
          .filter(status => status.BusinessLogic.toUpperCase() !== 'IN-TRANSIT' && status.BusinessLogic.toUpperCase() !== 'COMPLETED')
      }
    } catch { }
  }

  async getShippingStatus() {
    try {
      const res = await this._bookingService.getBookingStatuses('SHIPMENT').toPromise() as JsonResponse
      if (res.returnId > 0) {
        this.shippingStatuses = res.returnObject
        this.shippingStatuses.push({ BusinessLogic: 'PENDING', StatusCode: 'PENDING', StatusID: -1, StatusName: 'Pending', })
      }
    } catch { }
  }

  async setPortsData(selectedShippingModeCode, isLoading: boolean) {
    const _ports: Array<any> = (localStorage.getItem('shippingPortDetails')) ? JSON.parse(localStorage.getItem('shippingPortDetails')) : []
    const ports_to_get = (selectedShippingModeCode.toLowerCase() === 'truck') ? 'sea' : selectedShippingModeCode.toLowerCase()
    const filteredPorts: Array<any> = _ports.filter(port => port.type.toLowerCase().includes(ports_to_get))
    const hasMode = filteredPorts.length > 0 ? true : false
    if (hasMode) {
      this.ports = filteredPorts
    } else {
      try {
        if (isLoading)
          loading(true)
        const res = await this._bookingService.getPortsData(ports_to_get.toUpperCase()).toPromise()
        if (isLoading)
          loading(false)
        const newPorts = _ports.concat(res)
        const _filteredPorts: Array<any> = newPorts.filter(port => port.type.toLowerCase().includes(ports_to_get))
        this.ports = _filteredPorts
        localStorage.setItem('shippingPortDetails', JSON.stringify(newPorts))
      } catch {
        if (isLoading)
          loading(false)
      }
    }
  }

  getDatefromObj(dateObject) {
    let toSend = null
    try {
      toSend = moment(new Date(dateObject.year, dateObject.month - 1, dateObject.day)).format('YYYY-MM-DD')
    } catch (error) { }
    return toSend
  }

  isEmpty = (s) => !s.length;
  onFilterAction = () => this.filterRecords()

  filterRecords() {
    loading(true)
    const toSend = this.getFilterObject()
    this._bookingService.getInvoices(toSend).subscribe((res: JsonResponse) => {
      this.onBookingResponse(res)
    }, () => {
      loading(false)
      this._toastr.error('Please try again later', 'Failed')
    })
  }

  onBookingResponse(res: JsonResponse) {
    loading(false)
    if (res.returnId > 0)
      this.onListChange.emit({ ...res.returnObject, pageNumber: this.currPage })
    else
      this._toastr.error(res.returnText, 'Failed')
  }

  onSortChange($sortBy: string) {
    this.sortBy = $sortBy
    this.onFilterAction()
  }

  onModeChange($mode: string) {
    const { selectedShippingModeCode } = this
    if (selectedShippingModeCode !== 'WAREHOUSE' || selectedShippingModeCode !== 'HYPERLOOP') {
      this.filterOrigin = {}
      this.filterDestination = {}
      this.blNumber = null
    }
    this.setPortsData($mode, true)
    this.selectedShipStatusCode = null
    this.selectedBookStatusCode = null
    this.selectedCargoType = null
    this.filterOrigin = {} as any
    this.filterDestination = {} as any
    this.selectedCarrier = {} as any
    this.getBookingStatuses($mode)
  }

  getCarrierImage = ($image: string) => getImagePath(ImageSource.FROM_SERVER, $image, ImageRequiredSize.original)

  getProviderImage = ($image) => getImagePath(ImageSource.FROM_SERVER, getProviderImage($image), ImageRequiredSize.original)

  ngOnDestroy() { }
}