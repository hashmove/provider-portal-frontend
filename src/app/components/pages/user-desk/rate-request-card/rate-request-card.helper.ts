import { getLoggedUserData, loading } from '../../../../constants/globalFunctions'
import { SeaFreightService } from '../manage-rates/sea-freight/sea-freight.service'
export class RateRequestCardHelper {

    private static SEA_CHARGES = []

    payloadForGroupedRequest($request) {
        return {
            requestID: $request.RequestID,
            userID: getLoggedUserData().UserID,
            providerID: getLoggedUserData().ProviderID,
            requestStatusBL: '',
            carrierID: 0,
            filterProviderID: 0,
            pageSize: 100,
            pageNumber: 1,
            sortBy: 'price',
            sortDirection: 'ASC',
        }
    }
    async getAdditionalData(_seaFreightService: SeaFreightService, _mode:string) {
        loading(true)
        // const additionalCharges = null
        console.log(_mode)
        const additionalCharges = (localStorage.hasOwnProperty('additionalCharges')) ? JSON.parse(localStorage.getItem('additionalCharges')) : null
        if (additionalCharges) {
            RateRequestCardHelper.SEA_CHARGES = additionalCharges.filter(e => e.modeOfTrans === _mode && e.addChrType === 'ADCH')
        } else {
            try {
                const _res = await _seaFreightService.getAllAdditionalCharges(getLoggedUserData().ProviderID).toPromise() as any[]
                RateRequestCardHelper.SEA_CHARGES = _res.filter(e => e.modeOfTrans === _mode && e.addChrType === 'ADCH')
            } catch { }
        }
        loading(false)
        return RateRequestCardHelper.SEA_CHARGES
    }

    enabledRequestStatuses = ['PENDING', 'ACTIVE', 'PARTIAL_BOOKED']
    enabledResponseStatuses = ['PRICE_SENT', 'REQUESTED_AGAIN']
}
