import { Component, OnInit, Input, EventEmitter, Output, ViewEncapsulation } from '@angular/core'
import { changeCase, encryptBookingID, getImagePath, ImageRequiredSize, ImageSource, loading, isMobile } from '../../../../constants/globalFunctions'
import { baseExternalAssets } from '../../../../constants/base.url'
import { NgbModal, NgbTooltipConfig } from '@ng-bootstrap/ng-bootstrap'
import { UpdatePriceComponent } from '../../../../shared/dialogues/update-price/update-price.component'
import { JsonResponse } from '../../../../interfaces/JsonResponse'
import { ToastrService } from 'ngx-toastr'
import { SharedService } from '../../../../services/shared.service'
import { ViewBookingService } from '../view-booking/view-booking.service'
import { SeaFreightService } from '../manage-rates/sea-freight/sea-freight.service'
import { RateRequestCardHelper } from './rate-request-card.helper'
import { RequestAddRateDialogComponent } from '../../../../shared/dialogues/request-add-rate-dialog/request-add-rate-dialog.component'
import { IRateRequest } from '../../../../shared/dialogues/request-add-rate-dialog/request-add-rate.interface'
import { CargoDetailsRateRequestComponent } from '../../../../shared/dialogues/cargo-details-rate-request/cargo-details-rate-request.component'
import { ChatRequestComponent } from '../../../../shared/dialogues/chat-request/chat-request.component'
import { RequestStatusUpdationComponent } from '../../../../shared/dialogues/request-status-updation/request-status-updation.component'
import { DocumentUploadRequestComponent } from '../../../../shared/dialogues/document-upload-request/document-upload-request.component'
import { PriceLogsComponent } from '../../../../shared/dialogues/price-logs/price-logs.component';
import { IRequestStatus } from '../../../../interfaces'
import { DashboardService } from '../dashboard/dashboard.service'
import { IPaymentTerms, ITermOption } from '../../../../shared/dialogues/payment-terms/payment-terms.interface'
import { PaymentTermsDialogComponent } from '../../../../shared/dialogues/payment-terms/payment-terms.component'
import { ConfirmDialogContent, ConfirmDialogGenComponent } from '../../../../shared/dialogues/confirm-dialog-generic/confirm-dialog-generic.component'

@Component({
  selector: 'app-rate-request-card',
  templateUrl: './rate-request-card.component.html',
  styleUrls: ['./rate-request-card.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RateRequestCardComponent extends RateRequestCardHelper implements OnInit {

  @Input() request: IRateRequest
  @Input() requestStatusList: Array<IRequestStatus> = []
  @Input() IsSpotratAllowed = true
  @Output() bookingRemove = new EventEmitter<number>()
  public isExpanded = false

  public baseExternalAssets: string = baseExternalAssets
  public statusCode = {
    DRAFT: 'draft',
    APPROVED: 'approved',
    REJECTED: 'rejected',
    CANCELLED: 'cancelled',
    DECLINED: 'declined',
    DISCARD: 'discard',
    CONFIRMED: 'confirmed',
    IN_TRANSIT: 'in-transit',
    RE_UPLOAD: 're-upload',
    COMPLETED: 'completed',
    READY_TO_SHIP: 'ready to ship',
    IN_REVIEW: 'in-review',
    PENDING: 'pending',
    REQUESTED_AGAIN: 'requested_again'
  }
  searchMode = ''
  priceSent = false
  isRequestEnabled = true
  totalCBM = 0
  paymentTerm: IPaymentTerms
  paymentTermsOptions: ITermOption[] = []
  isEnterprise = false
  isMobile = isMobile()

  constructor(
    private modalService: NgbModal,
    private _sharedService: SharedService,
    private _seaFreightService: SeaFreightService,
    private _dashboardService: DashboardService,
    private _bookingService: ViewBookingService,
    private _toastr: ToastrService,
  ) {
    super()
  }

  ngOnInit() {
    this.searchMode = this.request.ShippingModeCode.toLowerCase() + '-' + this.request.ContainerLoad.toLowerCase()

    try {
      // this.isEnterprise = this.request.Customer.HMCustomerNature && this.request.Customer.HMCustomerNature.length > 0
      this.isEnterprise = this.request.Customer.IsEnterpriseCompany
    } catch { }

    if (this.enabledRequestStatuses.includes(this.request.RequestStatusBL) && this.request.ProviderRequestActive) {
      this.isRequestEnabled = true
    } else {
      this.isRequestEnabled = false
    }
    if (this.searchMode === 'sea-lcl')
      this.totalCBM = Math.ceil(this.request.TotalCBM)

    if (this.request.IsClosed) {
      this.isRequestEnabled = false
    }
  }

  openPriceLog = () => {
    const modalRef = this.modalService.open(PriceLogsComponent, {
      size: 'lg', centered: true, windowClass: 'large-modal', backdrop: 'static', keyboard: false
    })
    modalRef.componentInstance.data = { request: this.request, logs: [] }
    modalRef.componentInstance.isRV2 = true
  }

  updatePrice() {
    if (this.request.BookingSourceVia && this.request.BookingSourceVia.length > 2)
      SharedService.SET_VB_SOURCE_VIA(this.request.BookingSourceVia)
    const modalRef = this.modalService.open(UpdatePriceComponent, {
      size: 'lg', centered: true, windowClass: 'large-modal', backdrop: 'static', keyboard: false
    })
    modalRef.result.then((result) => {
      if (result) {
        this.request.RequestStatus = result.status
        this.request.RequestStatusBL = result.statusBl
        this.request.CurrencyID = result.currencyID
        this.request.CurrencyCode = result.currencyCode
        this.priceSent = true
      }
    })
    modalRef.componentInstance.data = this.request
  }

  openChat() {
    if (!this.IsSpotratAllowed) {
      this.spotRateLocked()
      return
    }
    const modalRef = this.modalService.open(ChatRequestComponent, {
      size: 'lg', centered: true, windowClass: 'chat-modal', backdrop: 'static', keyboard: false
    })
    modalRef.result.then(result => {
      this.request.ProviderUnReadMsgCount = 0
      // if (result) {
      //   this.request.RequestStatusBL = 'PRICE_SENT'
      //   this.request.RequestStatus = 'PRICE SENT'
      // }
    })
    modalRef.componentInstance.request = this.request
    modalRef.componentInstance.isEnabled = this.isRequestEnabled
  }

  openCargoDetails() {
    if (this.request.ContainerLoad === 'LCL') {
      loading(true)
      this._bookingService.getRequestCriteria(this.request.RequestID).subscribe((res: JsonResponse) => {
        // console.log(res)
        if (res.returnId > 0) {
          this.openCargo(JSON.parse(res.returnObject))
        } else {
          this.openCargo(null)
        }
      }, error => this.openCargo(null))
    } else {
      this.openCargo(null)
    }
  }

  openCargo(searchCriteria) {
    loading(true)
    const _id = encryptBookingID(this.request.RequestID, this.request.ProviderID, this.request.ShippingModeCode);
    this._dashboardService.getRequestContainer(_id, (this.request.ResponseID ? this.request.ResponseID : 0), 'PROVIDER').subscribe((res: JsonResponse) => {
      const modalRef = this.modalService.open(CargoDetailsRateRequestComponent, {
        size: 'lg', centered: true, windowClass: 'carge-detail', backdrop: 'static', keyboard: false
      })
      modalRef.componentInstance.request = this.request
      modalRef.componentInstance.searchCriteria = searchCriteria
      modalRef.componentInstance.contResp = res
    })

  }

  async openSeaDialog() {
    if (!this.IsSpotratAllowed) {
      this.spotRateLocked()
      return
    }
    const _additionalSeaCharges = await this.getAdditionalData(this._seaFreightService, this.request.ShippingModeCode)
    const modalRef = this.modalService.open(RequestAddRateDialogComponent, {
      size: 'lg', centered: true, windowClass: 'large-modal', backdrop: 'static', keyboard: false
    })
    modalRef.result.then((_state) => {
      if (_state) {

        this.getResponses(false)
      }
    })
    const object = {
      forType: this.request.ContainerLoad,
      addList: _additionalSeaCharges,
      mode: 'request',
      customers: [],
      drafts: [],
      rateRequest: this.request
    }
    modalRef.componentInstance.requestData = object
    modalRef.componentInstance.editorContent = null
  }

  uploadDocuments() {
    const modalRef = this.modalService.open(DocumentUploadRequestComponent, {
      size: 'lg', backdrop: 'static', centered: true, windowClass: 'document-modal', keyboard: false
    })
    modalRef.result.then(result => { })
    modalRef.componentInstance.request = this.request
  }

  cancelBooking() {
    if (!this.IsSpotratAllowed) {
      this.spotRateLocked()
      return
    }
    const modalRef = this.modalService.open(RequestStatusUpdationComponent, {
      size: 'lg', windowClass: 'medium-modal', centered: true, backdrop: 'static', keyboard: false
    })

    modalRef.componentInstance.request = this.request
    modalRef.result.then((res) => {
      if (res)
        this._sharedService.bookingsRefresh.next(true)
    })
  }

  onBookDescChange($bookingDesc: string) {
    this.request.BookingDesc = $bookingDesc
    if (this.request.ChildRequests && this.request.ChildRequests.length > 0)
      this.request.ChildRequests.forEach(_booking => { _booking.BookingDesc = $bookingDesc })
  }

  viewQuotes() {
    if (this.request.ResponseCount > 0 && !this.isExpanded)
      this.getResponses(true)
    else
      this.isExpanded = !this.isExpanded
  }

  async getResponses(expand) {
    loading(true)
    try {
      const { request } = this
      const _res: JsonResponse = await this._bookingService
        .getRequestResponses(this.payloadForGroupedRequest(request)).toPromise() as any
      if (_res.returnId > 0 && _res.returnObject && _res.returnObject.length > 0) {
        const _responses = _res.returnObject.map(_response => ({ ...request, ..._response, }))
        this.request.ChildRequests = _responses
        if (expand) {
          this.isExpanded = !this.isExpanded
        }
        if (this.request.ChildRequests.length > this.request.ResponseCount) {
          this.request.ResponseCount = this.request.ChildRequests.length
        }
      } else {
      }
      loading(false)
    } catch { loading(false) }
  }

  onChildResponse() {
    this.getResponses(false)
  }

  getStatusColor() {
    try {
      return this.requestStatusList.filter(_status => _status.BusinessLogic === this.request.RequestStatusBL)[0].StatusColorCode
    } catch {
      return 'hashmove-badge-secondary'
    }
  }

  getImage = ($image) => getImagePath(ImageSource.FROM_SERVER, $image, ImageRequiredSize.original)

  openPaymentTerms(action) {
    const { RequestID, } = this.request
    loading(true)
    this._bookingService.getRequestPaymentTerms(RequestID, 0, 0).subscribe((res: JsonResponse) => {
      loading(false)
      if (res.returnId > 0) {
        this.paymentTermsOptions = res.returnObject.filter(_term => _term.IsSelected)
        this.openTermsModal(action)
      } else {
        if (res.returnCode === '2')
          this._toastr.info(res.returnText, res.returnStatus)
        else
          this._toastr.error(res.returnText, res.returnStatus)
      }
    }, () => loading(false))
  }

  openTermsModal(action) {
    const modalRef = this.modalService.open(PaymentTermsDialogComponent, {
      size: 'lg', centered: true, windowClass: 'termsAndCondition', backdrop: 'static', keyboard: false
    })
    modalRef.componentInstance.paymentTerms = []
    modalRef.componentInstance.paymentTermsOptions = this.paymentTermsOptions
    modalRef.componentInstance.action = action
    modalRef.componentInstance.from = 'RR'
    modalRef.componentInstance.request = this.request
    modalRef.componentInstance.shippingModeCode = this.request.ShippingModeCode
    modalRef.componentInstance.isEnterprise = this.isEnterprise
  }

  spotRateLocked() {
    const _modalData: ConfirmDialogContent = {
      messageTitle: 'Access Restricted',
      messageContent: `
      <div class="d-flex flex-column">
        <span>Access Restricted due to Non-Compliance of ELM Terms & Agreement</span>
        <span>Please contact us at <a href="mailto:sales@hashmove.com">sales@hashmove.com</a></span>
      </div>
      `,
      buttonTitle: 'Close',
      data: null
    }

    const modalRef = this.modalService.open(ConfirmDialogGenComponent, {
      size: 'lg', centered: true, windowClass: 'restrict-modal', backdrop: 'static', keyboard: false
    });

    modalRef.result.then((result: string) => { })

    modalRef.componentInstance.modalData = _modalData;
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);
  }
}
