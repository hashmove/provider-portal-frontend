import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RateRequestCardComponent } from './rate-request-card.component';

describe('RateRequestCardComponent', () => {
  let component: RateRequestCardComponent;
  let fixture: ComponentFixture<RateRequestCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RateRequestCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RateRequestCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
