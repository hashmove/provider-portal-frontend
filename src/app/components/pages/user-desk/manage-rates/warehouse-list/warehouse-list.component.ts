import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { WarehouseService } from './warehouse.service';
import { getLoggedUserData, isJSON, loading } from '../../../../../constants/globalFunctions';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Lightbox } from 'ngx-lightbox';
import { baseExternalAssets } from '../../../../../constants/base.url';
import { PlatformLocation } from '@angular/common';
import { PaginationInstance } from 'ngx-pagination';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmDeleteDialogComponent } from '../../../../../shared/dialogues/confirm-delete-dialog/confirm-delete-dialog.component';
import { SharedService } from '../../../../../services/shared.service';
import { VideoDialogueComponent } from '../../../../../shared/dialogues/video-dialogue/video-dialogue.component'
import { ManageRatesService } from '../manage-rates.service';
import { firstBy } from 'thenby';

@Component({
  selector: 'app-warehouse-list',
  templateUrl: './warehouse-list.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./warehouse-list.component.scss']
})
export class WarehouseListComponent implements OnInit {

  public baseExternalAssets: string = baseExternalAssets;
  public userProfile: any;
  public allWareHouseListMain: any[] = [];
  public allWareHouseList: any[] = [];
  // public cityList: any[] = [];
  public wareHouseTitle: string;
  public autoHide: boolean = false;
  public responsive: boolean = true;
  public directionLinks: boolean = true;
  public videoURL: any;
  public paginationConfig: PaginationInstance = {
    itemsPerPage: 5, currentPage: 1
  }
  //filter

  public inActiveStatus: boolean = true;
  public activeStatus: boolean = true;
  public warehouseCharges: any[] = [];
  public step: number = 0

  //loading
  public loading: boolean = false

  // wareHouseDetTemplate
  public wareHouseDetTemplate: boolean = false;
  public warehouseID: any;
  fetchingWarehouseList: boolean = true


  constructor(
    private _warehouseService: WarehouseService,
    private _modalService: NgbModal,
    private _lightbox: Lightbox,
    private _location: PlatformLocation,
    private _toast: ToastrService,
    private _sharedService: SharedService,
    private _manageRatesService: ManageRatesService
  ) {
    _location.onPopState(() => this.closeLightBox());
  }

  ngOnInit() {
    this.userProfile = getLoggedUserData()
    this.getWhlist(this.userProfile.ProviderID);
  }

  sortWarehouses() {
    // WHID
    try {
      const { allWareHouseList, allWareHouseListMain } = this
      this.allWareHouseList = allWareHouseList.sort(
        firstBy(function (v1, v2) { return v2.WHID - v1.WHID; })
      )
      this.allWareHouseListMain = allWareHouseListMain.sort(
        firstBy(function (v1, v2) { return v2.WHID - v1.WHID; })
      )
    } catch (error) {

    }
  }

  getWhlist(providerId) {
    this.loading = true
    this.fetchingWarehouseList = true
    const wId = 0;
    this.fetchingWarehouseList = true
    this._warehouseService.getWarehouseList(providerId, wId).subscribe((res: any) => {
      loading(false)
      if (res.returnStatus == "Success") {
        if (res.returnObject && res.returnObject.WHModel && res.returnObject.WHModel.length) {
          this.allWareHouseList = res.returnObject.WHModel;
          this.allWareHouseListMain = res.returnObject.WHModel;
          this.sortWarehouses()

          // console.log(this.allWareHouseListMain);
          if (this.allWareHouseList && this.allWareHouseList.length) {
            this.allWareHouseList.map((obj) => {
              if (obj.FacilitiesProviding && obj.FacilitiesProviding != "[]" && isJSON(obj.FacilitiesProviding)) {
                obj.FacilitiesProviding = JSON.parse(obj.FacilitiesProviding).filter(_fac => _fac.BusinessLogic !== 'IsFireExtinguisherAvailable' && _fac.BusinessLogic !== 'IsCCTVAvailable')
              }
              // if (obj.CityID && this.cityList && this.cityList.length) {
              //   obj.Location = this.cityList.find(elem => elem.id == obj.CityID).title
              // }
              this.allWareHouseListMain.forEach(warehouse => {
                const { WHName, WHAddress, TotalCoveredArea, TotalCoveredAreaUnit, Location } = warehouse
                warehouse.searchFiled = WHName + ' ' + WHAddress + '' + Location + ' ' + TotalCoveredArea + ' ' + TotalCoveredAreaUnit
              })
              this.allWareHouseList.forEach(element => {
                const albumArr = []
                const docsArray: Array<any> = JSON.parse(element.WHGallery)
                let idx = this.allWareHouseList.indexOf(element);
                try {
                  docsArray.forEach(doc => {
                    if (doc.DocumentUploadedFileType.toLowerCase() === 'png' || doc.DocumentUploadedFileType.toLowerCase() === 'jpg' || doc.DocumentUploadedFileType.toLowerCase() === 'jpeg') {
                      const album = {
                        src: baseExternalAssets + '/' + doc.DocumentFile,
                        // caption: elem.DocumentFileName,
                        thumb: baseExternalAssets + '/' + doc.DocumentFile,
                        DocumentUploadedFileType: doc.DocumentUploadedFileType
                      };
                      albumArr.push(album)
                      this.allWareHouseList[idx].WHParsedMedia = albumArr;
                    } else if (doc.DocumentUploadedFileType.toLowerCase() === 'mp4') {
                      this.videoURL = baseExternalAssets + '/' + doc.DocumentFile
                      this.allWareHouseList[idx].videoURL = this.videoURL;
                    }
                  })
                } catch (error) { loading(false) }
              });
            })
          }
        }

      }
      this.fetchingWarehouseList = false
      this.loading = false;
      this.fetchingWarehouseList = false
      loading(false)
    }, (err: HttpErrorResponse) => {
      loading(false)
      this.fetchingWarehouseList = false
      this.loading = false;
      this.fetchingWarehouseList = false
    })
  }

  openGallery(index, albumArr): void {
    this._lightbox.open(albumArr, index, { disableScrolling: true, centerVertically: true, alwaysShowNavOnTouchDevices: true });
  }

  closeLightBox(): void {
    this._lightbox.close();
  }
  addAnotherWarehouse() {
    this.wareHouseDetTemplate = true;
    this.warehouseID = '0'
    this.step = 0;
    // this._router.navigate(['provider/add-warehouse', 0])
  }

  onPageChange(number) {
    this.paginationConfig.currentPage = number;
  }

  statusType(type: string, event) {
    if (type == 'activeStatus') {
      if (this.inActiveStatus) {
        this.activeStatus = !this.activeStatus;
      }
      else {
        event.preventDefault();
      }
    }
    else if (type == 'inActiveStatus') {
      if (this.activeStatus) {
        this.inActiveStatus = !this.inActiveStatus;
      }
      else {
        event.preventDefault();
      }
    }
  }


  deleteWarehouse(whid) {
    const modalRef = this._modalService.open(ConfirmDeleteDialogComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'small-modal',
      backdrop: 'static',
      keyboard: false
    });
    modalRef.result.then((result) => {
      if (result == "Success") {
        this.paginationConfig.currentPage = 1
        this._toast.success('Warehouse delete successfully', '')
        let index = this.allWareHouseList.findIndex(obj => obj.WHID == whid);
        if (index >= 0) {
          this.allWareHouseList.splice(index, 1);
        }
      }
    });
    let obj = {
      data: whid,
      type: "DelWarehouse"
    }
    modalRef.componentInstance.deleteIds = obj;
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);

  }
  activeToggler(wh, whStatus) {
    wh.IsBlocked = whStatus;
    let obj = {
      whid: wh.WHID,
      status: !whStatus,
      modifiedBy: this.userProfile.UserID
    }
    this.onPageChange(1)
    this._warehouseService.activeWarehouseToggler(obj).subscribe((res: any) => {
      if (res.returnStatus == "Success") {
        this._toast.success('Warehouse update successfully', '')
      }
    })
  }
  goToDetail(whId, type) {
    // let id = encryptBookingID(whId);
    this.wareHouseDetTemplate = true;
    this.warehouseID = whId;
    this.step = (type === 'edit') ? 0 : ((type === 'rate') ? 1 : 0)
    this._sharedService.mainScrollListener.next(true)
    // this._router.navigate(['/provider/warehouse-detail', whId]);
  }

  warehouseList() {
    this.wareHouseDetTemplate = !this.wareHouseDetTemplate;
    this.getWhlist(this.userProfile.ProviderID);
  }

  openVideo(videoURL, event) {
    event.preventDefault()
    event.stopPropagation()
    const modalRefVideo = this._modalService.open(VideoDialogueComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'video',
      backdrop: 'static',
      keyboard: false
    });
    modalRefVideo.componentInstance.videoURL = videoURL
  }


  onWarhouseTextChange(text: string) {
    const { allWareHouseListMain } = this
    this.allWareHouseList = allWareHouseListMain.filter(
      v => v.searchFiled.toLowerCase().indexOf(text.toLowerCase()) > -1
    )
    this.sortWarehouses()
    this.paginationConfig.currentPage = 1

  }

  //term & cond.
  private toolbarOptions = [
    ['bold', 'italic', 'underline'],
    [{ 'list': 'ordered' }, { 'list': 'bullet' }],
    [{ 'script': 'sub' }, { 'script': 'super' }],
    [{ 'direction': 'rtl' }],
    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
    [{ 'color': [] }, { 'background': [] }],
    [{ 'align': [] }],
    ['clean']
  ];


  public editorOptions = {
    placeholder: "insert content...",
    modules: {
      toolbar: this.toolbarOptions
    }
  };

  public editorContent: any;
  public disableEditor: boolean;

  getWarehousetermNcond() {
    this._warehouseService.WHtermNcondition(this.userProfile.ProviderID).subscribe((res: any) => {
      if (res.returnStatus == "Success") {
        if (res.returnObject && res.returnObject.TermsCondition) {
          this.editorContent = res.returnObject.TermsCondition;
          this.disableEditor = true;
        } else {
          this.disableEditor = false;
        }
      }
    })

  }


  onContentChanged({ quill, html, text }) {
    this.editorContent = html
  }

  isEmpty(htmlString) {
    if (htmlString) {
      const parser = new DOMParser();
      const { textContent } = parser.parseFromString(htmlString, "text/html").documentElement;
      return !textContent.trim();
    } else {
      return true;
    }
  };

  saveTermNcond() {
    let obj = {
      providerID: this.userProfile.ProviderID,
      termsAndConditions: this.editorContent,
      transportType: "WAREHOUSE",
      modifiedBy: this.userProfile.UserID
    }
    this._manageRatesService.termNCondition(obj).subscribe((res: any) => {
      if (res.returnStatus == "Success") {
        this._toast.success("Term and Condition saved Successfully", "");
        this._sharedService.termNcondLCL.next(this.editorContent);
        this.disableEditor = true;
      }
    })
  }

}
