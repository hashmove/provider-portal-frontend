import { Injectable } from '@angular/core';
import { baseApi } from '../../../../constants/base.url';
import { HttpClient } from "@angular/common/http";
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';

@Injectable()
export class ManageRatesService {
  queryUrl: string = '?search=';
  constructor(private _http: HttpClient) { }

  termNCondition(obj) {
    let url: string = "provider/SaveTermsAndConditions";
    return this._http.post(baseApi + url, obj);
  }

  getShippingData() {
    let url: string = "shippingModeCatMapping/GetShippingCriteria";
    return this._http.get(baseApi + url);
  }

  getPortsData($portType?: string) {
    let portType = $portType
    if (!portType) {
      portType = 'SEA'
    }
    let url: string = `Ports/GetPortsList/0/${portType}`;
    return this._http.get(baseApi + url);
  }

  getContainersMapping() {
    let url: string = "shippingModeCatMapping/GetContainerSpecMapping";
    return this._http.get(baseApi + url);
  }

  getAllCustomers(ProviderID) {
    let url: string = `provider/GetProviderCustomerMapping/${ProviderID}`;
    return this._http.get(baseApi + url);
  }

  getAllAdditionalCharges(ProviderID) {
    let url: string = `AdditionalCharge/GetAll/${ProviderID}`;
    return this._http.get(baseApi + url);
  }

  addCustomCharge(data) {
    let url: string = `AdditionalCharge/Post`;
    return this._http.post(baseApi + url, data);
  }

  getSurchargeBasis(ContainerLoadType) {
    let url: string = `AdditionalCharge/GetSurchargeBasis/${ContainerLoadType}`;
    return this._http.get(baseApi + url);
  }
  getAllDrafts(type, ProviderID, ContainerLoad) {
    let url: string = `providerrate${type}/GetAllDrafts/${ProviderID}/${ContainerLoad}`;
    return this._http.get(baseApi + url);
  }

  getAllCities(filterVal) {
    let url: string = `city/GetCityDropDownDetail/1/${filterVal}`;
    return this._http.get(baseApi + url);
  }

  getWarehousePricing(modeofTrans) {
    let url: string = `AdditionalCharge/GetBaseChagesList/${modeofTrans}`;
    return this._http.get(baseApi + url);
  }


  /**
   *
   * GET ALL CARRIER'S LIST
   * @param {string} modeofTrans
   * @returns
   * @memberof ManageRatesService
   */
  getAllCarriers() {
    let url: string = `carrier/GetDropDownDetail/100`;
    return this._http.get(baseApi + url);
  }

  /**
   *
   * GET ALL CARGO TYPES
   * @returns
   * @memberof ManageRatesService
   */
  getShippingCategory() {
    let url: string = `shippingCategory/GetDropDownDetail/100`;
    return this._http.get(baseApi + url);
  }


  /**
   *
   * GET PROVIDER LOG SERV
   * @param {number} providerID
   * @returns
   * @memberof ManageRatesService
   */
  getProviderLogisticService(providerID) {
    let url: string = `provider/GetProviderLogisticService/${providerID}`;
    return this._http.get(baseApi + url);
  }


  getTermsAndCondition(mode: string, providerID: any) {
    let url: string = `provider/GetProviderTermsCondition/${mode}/${providerID}`;
    return this._http.get(baseApi + url);
  }


  getUploadSheetList(documentTypeID, providerId) {
    let url: string = `Document/GetProviderUploadDocList/${documentTypeID}/${providerId}`;
    return this._http.get(baseApi + url);
  }

  getaddOnPeriods() {
    let url: string = `MstCodeVal/GetMstCodeValMultipleList/ADDON_PERIOD`;
    return this._http.get(baseApi + url);
  }

  getRateExpirySettings(providerID) {
    let url: string = `provider/GetRateExpirySettings/${providerID}`;
    return this._http.get(baseApi + url);
  }

  updateAllFCLPublishExpiringRates(data) {
    let url: string = `providerratefcl/UpdateAllFCLPublishExpiringRates`;
    return this._http.put(baseApi + url, data);
  }

  updateAllLCLPublishExpiringRates(data) {
    let url: string = `providerratelcl/UpdateAllLCLPublishExpiringRates`;
    return this._http.put(baseApi + url, data);
  }

  getAirPartnersById($providerID) {
    let url: string = `Partner/GetDropDownDetailForProvider/0/${$providerID}`;
    return this._http.get(baseApi + url);
  }
}
