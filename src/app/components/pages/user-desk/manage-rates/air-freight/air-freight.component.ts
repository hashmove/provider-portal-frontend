import { Component, OnInit, ViewChild, ViewEncapsulation, ElementRef, Renderer2, QueryList, AfterViewInit, OnDestroy } from '@angular/core';
import {
  NgbDatepicker,
  NgbInputDatepicker,
  NgbDateStruct,
  NgbCalendar,
  NgbDateAdapter,
  NgbDateParserFormatter,
  NgbModal
} from '@ng-bootstrap/ng-bootstrap';
import { trigger, state, animate, transition, style } from '@angular/animations';
import { DiscardDraftComponent } from '../../../../../shared/dialogues/discard-draft/discard-draft.component';
import { Observable, Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { AirFreightService } from './air-freight.service';
import { getJwtToken } from '../../../../../services/jwt.injectable';
import { SharedService, LogisticServices } from '../../../../../services/shared.service';
import { baseExternalAssets } from '../../../../../constants/base.url';
import { ConfirmDeleteDialogComponent } from '../../../../../shared/dialogues/confirm-delete-dialog/confirm-delete-dialog.component';
// import { NgModel } from '@angular/forms';
import * as moment from 'moment';
// import { DataTableDirective } from 'angular-datatables';
import { AirRateDialogComponent } from '../../../../../shared/dialogues/air-rate-dialog/air-rate-dialog.component';
import { NgbDateFRParserFormatter } from '../../../../../constants/ngb-date-parser-formatter';
import { ManageRatesService } from '../manage-rates.service';
import { ToastrService } from 'ngx-toastr';
import { FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
import { RateValidityComponent } from '../../../../../shared/dialogues/rate-validity/rate-validity.component';
import { RateHistoryComponent } from '../../../../../shared/dialogues/rate-history/rate-history.component';
import { loading, changeCase, getImagePath, ImageSource, ImageRequiredSize, removeDuplicates, getLoggedUserData, isMobile } from '../../../../../constants/globalFunctions';
import { HttpErrorResponse } from '@angular/common/http';
import { CommonService } from '../../../../../services/common.service';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { cloneObject } from '../../reports/reports.component';
import { SeaFreightService } from '../sea-freight/sea-freight.service';
import { NgFilesSelected, NgFilesStatus, NgFilesConfig, NgFilesService } from '../../../../../directives/ng-files';
import { DocumentFile } from '../../../../../interfaces/document.interface';
import { JsonResponse } from '../../../../../interfaces/JsonResponse';
import { BasicInfoService } from '../../../user-creation/basic-info/basic-info.service';
import { UploadHistoryDialogComponent } from '../../../../../shared/dialogues/upload-history/upload-history.component';
import { IExpiryNotification } from '../../user-desk.component';
import { AddRateHelpers } from '../../../../../helpers/add-rate.helper';
import { IRateNotification } from '../../../../../interfaces';


declare var $;
const now = new Date();
const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
  one && two && two.year === one.year && two.month === one.month && two.day === one.day;

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day < two.day : one.month < two.month : one.year < two.year;

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day > two.day : one.month > two.month : one.year > two.year;

@Component({
  selector: 'app-air-freight',
  templateUrl: './air-freight.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./air-freight.component.scss'],
  providers: [{ provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter }],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   // :enter is alias to 'void => *'
        style({ opacity: 0 }),
        animate(500, style({ opacity: 1 }))
      ]),
      transition(':leave', [   // :leave is alias to '* => void'
        animate(500, style({ opacity: 0 }))
      ])
    ])
  ]
})
export class AirFreightComponent extends AddRateHelpers implements OnInit, OnDestroy {

  private draftRates: any;
  public rateValidityText = "Edit Rate / Validity";
  public dtOptionsByAir: DataTables.Settings | any = {};
  public dtOptionsByAirDraft: DataTables.Settings | any = {};
  @ViewChild('draftBYair') tabledraftByAir;
  @ViewChild('publishByair') tablepublishByAir;
  @ViewChild("dp") input: NgbInputDatepicker;
  // @ViewChild(NgModel) datePick: NgModel;
  @ViewChild('rangeDp') rangeDp: ElementRef;
  // @ViewChild(DataTableDirective) dtElement: DataTableDirective;

  public dataTablepublishByAir: any;
  public datatabledraftByAir: any;
  public allRatesList: any;
  public publishloading: boolean;
  public draftloading: boolean = true;
  public allAirLines: any[] = [];
  public allCargoType: any[] = []
  public allPorts: any[] = [];
  public allCurrencies: any[] = [];
  public allDraftRatesByAIR: any[] = [];
  public draftDataBYAIR: any[] = [];
  public draftslist: any[] = [];
  public delPublishRates: any[] = [];
  public publishRates: any[] = [];
  public filterOrigin: any = {};
  public filterDestination: any = {};
  public startDate: NgbDateStruct;
  public maxDate: NgbDateStruct;
  public minDate: NgbDateStruct;
  public hoveredDate: NgbDateStruct;
  public fromDate: any;
  public toDate: any;
  public model: any;

  private _subscription: Subscription;
  private _selectSubscription: Subscription;

  public userProfile: any;
  
  isMobile = isMobile()
  
  // filterartion variable;

  public filterbyAirLine;
  public filterbyCargoType;
  public checkedallpublishRates: boolean = false;
  public checkedalldraftRates: boolean = false;
  // term and condition

  isHovered = date =>
    this.fromDate && !this.toDate && this.hoveredDate && after(date, this.fromDate) && before(date, this.hoveredDate)
  isInside = date => after(date, this.fromDate) && before(date, this.toDate);
  isFrom = date => equals(date, this.fromDate);
  isTo = date => equals(date, this.toDate);


  public disable: boolean = false;
  public editorContent: any;
  private toolbarOptions = [
    ['bold', 'italic', 'underline'],        // toggled buttons

    [{ 'list': 'ordered' }, { 'list': 'bullet' }],
    [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
    [{ 'direction': 'rtl' }],                         // text direction

    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

    [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
    [{ 'align': [] }],

    ['clean']                                         // remove formatting button
  ];
  public editorOptions = {
    placeholder: "insert content...",
    modules: {
      toolbar: this.toolbarOptions
    }
  };


  // AIR NEW WORKING
  public airCharges: any[] = []
  public allCustomers: any[] = []
  public isCustomer: boolean = false
  public isMarketplace: boolean = false
  public isValidRates: boolean = true
  public isExpiredRates: boolean = true
  public filterbyCustomer;
  public isVirtualAirline: boolean

  public selectedService: LogisticServices
  public selectedAirline: any = null;

  public config: NgFilesConfig = {
    acceptExtensions: ['xls', 'xlsx'],
    maxFilesCount: 1,
    maxFileSize: 12 * 1024 * 1000,
    totalFilesSize: 12 * 12 * 1024 * 1000
  };

  expiredRateList: any[] = []
  expNotifications: IExpiryNotification = null
  expirySettings: IRateNotification
  isExpirySectionOpenX = false
  public sortColumnExp: string = 'RecentUpdate'
  public sortColumnDirectionExp: string = 'ASC'
  public pageNoExp: number = 1;
  public totalPublishedRecordsExp: number;

  airTemplateUrl = 'TemplatesFiles/Rates/AIR/AIR.xlsx'
  displayMonths = isMobile() ? 1 : 2

  constructor(
    private modalService: NgbModal,
    private _airFreightService: AirFreightService,
    private _sharedService: SharedService,
    private element: ElementRef,
    private renderer: Renderer2,
    private _parserFormatter: NgbDateParserFormatter,
    private _manageRatesService: ManageRatesService,
    private _toast: ToastrService,
    private _commonService: CommonService,
    private _seaFreightService: SeaFreightService,
    private _basicInfoService: BasicInfoService,
    private ngFilesService: NgFilesService,
    private _toastr: ToastrService
  ) {
    super()
  }

  ngOnInit() {
    loading(false)
    this.userProfile = getLoggedUserData()


    this._sharedService.expNotifications.pipe(untilDestroyed(this)).subscribe(state => {
      this.expNotifications = state
      if (state) {
        this.getExpiredRates()
      }
    })

    this._manageRatesService.getRateExpirySettings(this.userProfile.ProviderID).subscribe((res: JsonResponse) => {
      if (res.returnId > 0) {
        const _resObj: IRateNotification[] = res.returnObject.RateExpirySettings
        this.expirySettings = _resObj.filter(_rate => _rate.ModeOfTrans === 'AIR')[0]
      }
    })

    this.setTermsAndCondit('air-lcl')

    try {
      const services = JSON.parse(localStorage.getItem('selectedServices'))
      this.selectedService = services.filter(service => service.LogServCode === 'AIR_FFDR')[0]
      try {
        const allowed_extensions = this.selectedService.UploadRatesDocumnet[0].AllowedFileTypes
        const _extArr = allowed_extensions.split(',')
        const arrExt = []
        _extArr.forEach(_ext => {
          arrExt.push(_ext)
        });
        this.config.acceptExtensions = arrExt
        this.ngFilesService.addConfig(this.config, 'config');
      } catch (error) {

      }
    } catch (error) {
    }

    if (localStorage.hasOwnProperty('isVirtualAirline')) {
      // const virtualAirline = JSON.parse(localStorage.getItem('isVirtualAirline'))
      this.isVirtualAirline = JSON.parse(localStorage.getItem('isVirtualAirline'))
      this.getProductAir()
    }
    this.getAirPartners(this.userProfile.ProviderID, null, this._manageRatesService)

    if (!this.isVirtualAirline) {
      this.getAllCustomers()
    }

    this.startDate = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    this.maxDate = { year: now.getFullYear() + 1, month: now.getMonth() + 1, day: now.getDate() };
    this.minDate = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    // this.allCargoType = (localStorage.getItem('cargoTypes')) ? JSON.parse(localStorage.getItem('cargoTypes')) : [] //Old
    // this.allCargoType = this._sharedService.getFilteredCargos('AIR', 'cargoTypes') //New
    this.getShippingCategory()
    setTimeout(() => {
      if (localStorage.getItem('carriersList')) {
        const carriers = JSON.parse(localStorage.getItem('carriersList'))
        this.allAirLines = carriers.filter(e => e.type === 'AIR');
      } else {
        this._manageRatesService.getAllCarriers().pipe(untilDestroyed(this)).subscribe((res: any) => {
          const carriers = res
          this.allAirLines = carriers.filter(e => e.type === 'AIR');
          localStorage.setItem('carriersList', JSON.stringify(res))
        }, (err: any) => {
        })
      }
    }, 500);

    this.getAllPublishRates(1);
    this.getAllDrafts()
    this.getAdditionalData()
    if (localStorage.getItem('AirPortDetails')) {
      this.allPorts = JSON.parse(localStorage.getItem('AirPortDetails'))
    } else {
      this.getPortsData()
    }
    this._sharedService.draftRowAddAir.pipe(untilDestroyed(this)).subscribe(state => {
      if (state && Object.keys(state).length) {
        this.setRowinDraftTable(state, 'popup not open');
      }
    })
    this._sharedService.termNcondAir.subscribe(state => {
      if (state) {
        this.editorContent = state;
      }
    })
  }


  onEditorBlured(quill) {
  }

  onEditorFocused(quill) {
  }

  onEditorCreated(quill) {
  }

  getAllCustomers() {
    loading(true)
    this.allCustomers = JSON.parse(localStorage.getItem('customers'))
    // console.log(this.allCustomers);

    if (this.allCustomers) {
      this.allCustomers.forEach(e => {
        e.CustomerImageParsed = getImagePath(ImageSource.FROM_SERVER, e.CustomerImage, ImageRequiredSize._48x48)
      })
      loading(false)
    } else {
      this._seaFreightService.getAllCustomers(this.userProfile.ProviderID).subscribe((res: any) => {
        if (res.returnId > 0) {
          this.allCustomers = res.returnObject
          // console.log(this.allCustomers);

          loading(false)
          this.allCustomers.forEach(e => {
            e.CustomerImageParsed = getImagePath(ImageSource.FROM_SERVER, e.CustomerImage, ImageRequiredSize._48x48)
          })
          localStorage.setItem('customers', JSON.stringify(this.allCustomers))
        } else {
          try {
            localStorage.removeItem('customers')
          } catch { }
        }
      }, (err) => {
        loading(false)
        try {
          localStorage.removeItem('customers')
        } catch { }
      })
    }
  }

  onContentChanged({ quill, html, text }) {
    this.editorContent = html
  }

  isEmpty(htmlString) {
    if (htmlString) {
      const parser = new DOMParser();
      const { textContent } = parser.parseFromString(htmlString, "text/html").documentElement;
      return !textContent.trim();
    } else {
      return true;
    }
  };


  setCargoTypes() {
    const allCargoType: any = (localStorage.getItem('cargoTypes')) ? JSON.parse(localStorage.getItem('cargoTypes')) : []
    this.allCargoType = this._sharedService.getFilteredCargos('AIR', allCargoType, 'id') //New
  }

  getShippingCategory() {
    if (localStorage.getItem('cargoTypes')) {
      this.setCargoTypes()
    } else {
      this._manageRatesService.getShippingCategory().pipe(untilDestroyed(this)).subscribe((res: any) => {
        localStorage.setItem('cargoTypes', JSON.stringify(res))
        this.setCargoTypes()
      }, (err: any) => {
      })
    }
  }

  ngOnDestroy() {

  }

  clearFilter(event) {
    event.preventDefault();
    event.stopPropagation();
    // if ((this.filterbyAirLine && this.filterbyAirLine != 'undefined') ||
    //   (this.filterbyCargoType && this.filterbyCargoType != 'undefined') ||
    //   (this.filterDestination && Object.keys(this.filterDestination).length) ||
    //   (this.filterOrigin && Object.keys(this.filterOrigin).length) ||
    //   (this.fromDate && Object.keys(this.fromDate).length) ||
    //   (this.toDate && Object.keys(this.toDate).length)
    // ) {
    this.selectedAirline = null;
    this.selectedProduct = null;
    this.selectedPartner = null;
    this.renderer.setProperty(this.rangeDp.nativeElement, 'value', null);
    this.model = null;
    this.fromDate = null;
    this.toDate = null;
    this.filterbyAirLine = 'undefined';
    this.filterbyCargoType = 'undefined';
    this.filterDestination = {};
    this.filterOrigin = {};
    this.filterbyCustomer = 'undefined';
    this.isMarketplace = false
    this.isCustomer = false
    this.isExpiredRates = true
    this.isValidRates = true
    this.partnerFilterList = []
    this.getAllPublishRates(1)

  }
  filter() {
    this.getAllPublishRates(1)
  }
  addRatesManually() {
    this.updatePopupRates(0, 'AIR');
  }

  setRowinDraftTable(obj, type) {
    if (typeof obj.slab == "string") {
      obj.slab = JSON.parse(obj.slab);
    }
    this.draftDataBYAIR.unshift(obj);
    if (this.allDraftRatesByAIR && this.allDraftRatesByAIR.length) {
      this.draftslist = this.allDraftRatesByAIR.concat(this.draftDataBYAIR);
    } else {
      this.draftslist = this.draftDataBYAIR;
    }
    if (type == 'openPopup') {
      this.updatePopupRates(obj.CarrierPricingSetID, 'AIR');
    }
    this._sharedService.updatedDraftsAir.next(this.draftslist)
  }

  updatePopupRates(rowId, type) {
    this.allCustomers = JSON.parse(localStorage.getItem('customers'))
    // console.log(this.allCustomers);

    this.airCharges = JSON.parse(localStorage.getItem('additionalCharges'))
    let obj
    if (rowId > 0) {
      obj = this.draftslist.find(obj => obj.carrierPricingDraftID == rowId);
    } else {
      obj = null
    }
    const modalRef = this.modalService.open(AirRateDialogComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'large-modal',
      backdrop: 'static',
      keyboard: false
    });
    modalRef.componentInstance.savedRow.subscribe((emmitedValue) => {
      // console.log(emmitedValue)
      this.setAddDraftData(emmitedValue);
      this.getAllDrafts()
    });
    modalRef.result.then((result) => {
      const additionalChargesChanges = JSON.parse(localStorage.getItem('additionalChargesChange'))
      if (additionalChargesChanges && parseInt(additionalChargesChanges) === 1) {

      }
      if (result) {
        this.getAllDrafts()
        this.getAllPublishRates(1);
      }
    }, (reason) => {
    });

    let object = {
      forType: type,
      data: obj,
      addList: this.airCharges,
      mode: 'draft',
      customers: this.allCustomers,
      drafts: this.draftslist
    }
    modalRef.componentInstance.selectedData = object;
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);

  }

  setAddDraftData(data) {
    // console.log(data)
    if (data) {
      data.forEach(element => {
        if (element.JsonSurchargeDet) {
          let importCharges = []
          let exportCharges = []
          let parsedJsonSurchargeDet = JSON.parse(element.JsonSurchargeDet)
          importCharges = parsedJsonSurchargeDet.filter(e => e.Imp_Exp === 'IMPORT');
          exportCharges = parsedJsonSurchargeDet.filter(e => e.Imp_Exp === 'EXPORT');
          element.parsedJsonSurchargeDet = importCharges.concat(exportCharges)
        }
        let dataObj = changeCase(element, 'pascal')
        this.draftslist.forEach(e => {
          if (e.CarrierPricingDraftID === element.carrierPricingDraftID) {
            let idx = this.draftslist.indexOf(e)
            this.draftslist.splice(idx, 1)
          }
        })
        this.draftslist.unshift(dataObj)
      });
    }
  }
  addAnotherRates() {
    this.addRatesManually();
  }
  addRatesByAirManually() {
    if ((!this.allDraftRatesByAIR || (this.allDraftRatesByAIR && !this.allDraftRatesByAIR.length)) && (!this.draftDataBYAIR || (this.draftDataBYAIR && !this.draftDataBYAIR.length))) {
      this.addRatesManually();
    }
  }
  filterBydate(date, type) {
    if (!date && this.fromDate && this.toDate) {
      this.fromDate = null;
      this.toDate = null;
      this.getAllPublishRates(1);
    }
    else {
      return;
    }
  }

  onDateSelection(date: NgbDateStruct) {
    let parsed = '';
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && after(date, this.fromDate)) {
      this.toDate = date;
      // this.model = `${this.fromDate.year} - ${this.toDate.year}`;
      this.input.close();
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
    if (this.fromDate) {
      parsed += this._parserFormatter.format(this.fromDate);
    }
    if (this.toDate) {
      parsed += ' - ' + this._parserFormatter.format(this.toDate);
    }

    this.renderer.setProperty(this.rangeDp.nativeElement, 'value', parsed);
    if (this.fromDate && this.fromDate.month && this.toDate && this.toDate.month) {
      this.getAllPublishRates(1);
    }
  }

  allservicesByAir() {
    loading(true)
    this._sharedService.dataLogisticServiceBySea.subscribe(state => {
      if (state && state.length) {
        loading(false)
        for (let index = 0; index < state.length; index++) {
          if (state[index].LogServName == "SEA") {
            this.allAirLines = state[index].DropDownValues.AirLine;
            this.allCargoType = state[index].DropDownValues.Category;
            // this.allPorts = state[index].DropDownValues.AirPort;
            // this.allCurrencies = state[index].DropDownValues.UserCurrency;
            if (state[index].TCAIR) {
              this.editorContent = state[index].TCAIR;
              this.disable = true;
            } else {
              this.disable = false;
            }
            // this._sharedService.updatedDraftsAir.pipe(untilDestroyed(this)).subscribe((res: any) => {
            //   if (!res) {
            //     if (state[index].DraftDataAir && state[index].DraftDataAir.length) {
            //       state[index].DraftDataAir.map(elem => {
            //         if (typeof elem.slab == "string") {
            //           elem.slab = JSON.parse(elem.slab)
            //         }
            //       });
            //       this.allDraftRatesByAIR = state[index].DraftDataAir;
            //       // this.draftslist = this.allDraftRatesByAIR;
            //     }
            //   }
            //   else if (res && res.length) {
            //     // this.draftslist = res;
            //   }
            // })
            // this.generateDraftTable();
            // this.draftloading = true;
          }
        }
      }
    })
  }

  filterByroute(obj) {
    if (typeof obj == 'object') {
      this.getAllPublishRates(1);
    }
    else if (!obj) {
      this.getAllPublishRates(1);
    }
    else {
      return;
    }
  }

  filtertionPort(obj) {
    if ((typeof obj == "object" && Object.keys(obj).length) || (typeof obj == "string" && obj)) this.getAllPublishRates();
  }

  public filteredRecords: number;
  public pageSize: number = 20;
  public sortColumn: string = 'RecentUpdate'
  public sortColumnDirection: string = 'ASC'
  getAllPublishRates(pageNo?) {
    loading(true)
    this.publishloading = true;
    let param: any = {}
    let obj = {
      isValidRates: this.isValidRates,
      isExpiredRates: this.isExpiredRates,
      pageNo: pageNo ? pageNo : this.pageNo,
      pageSize: this.pageSize,
      providerID: this.userProfile.ProviderID,
      carrierID: (this.selectedAirline && this.selectedAirline.id) ? this.selectedAirline.id : null,
      shippingCatID: (this.filterbyCargoType == 'undefined') ? null : this.filterbyCargoType,
      polID: this.orgfilter(),
      podID: this.destfilter(),
      effectiveFrom: (this.fromDate && this.fromDate.month) ? this.fromDate.month + '/' + this.fromDate.day + '/' + this.fromDate.year : null,
      effectiveTo: (this.toDate && this.toDate.month) ? this.toDate.month + '/' + this.toDate.day + '/' + this.toDate.year : null,
      sortColumn: this.sortColumn,
      sortColumnDirection: this.sortColumn === 'RecentUpdate' ? 'DESC' : this.sortColumnDirection,
      customerID: (this.filterbyCustomer ? parseInt(this.filterbyCustomer) : null),
      customerType: (this.isCustomer ? 'CUSTOMER' : (this.isMarketplace ? 'MARKETPLACE' : null)),
      aircraftTypeID: null,
      partnerID: (this.partnerFilterList && this.partnerFilterList.length > 0) ? this.partnerFilterList.map(_partner => _partner.id) : [],
    }
    if (this.isVirtualAirline) {
      param = {
        ...obj,
        // partnerID: (this.selectedPartner) ? parseInt(this.selectedPartner.id) : null,
        productID: parseInt(this.selectedProduct)
      }
    } else {
      param = { ...obj }
    }
    this._airFreightService.getAllrates(param).subscribe((res: any) => {
      this.publishloading = false;
      loading(false)
      if (res.returnId > 0) {
        this.totalPublishedRecords = res.returnObject.recordsTotal
        this.filteredRecords = res.returnObject.recordsFiltered
        this.allRatesList = cloneObject(res.returnObject.data);
        this.checkedallpublishRates = false;
      }
    }, err => {
      this._toast.error('Server Error, please try again later')
      loading(false)
    })
    this.getExpiryRates()
  }

  getExpiredRates() {
    loading(true)
    this.publishloading = true;
    let obj = {
      isValidRates: false,
      isExpiredRates: true,
      providerID: this.userProfile.ProviderID,
      pageNo: this.pageNoExp,
      pageSize: this.pageSize,
      carrierID: null,
      shippingCatID: null,
      containerSpecID: null,
      polID: null,
      podID: null,
      effectiveFrom: null,
      effectiveTo: null,
      customerID: null,
      customerType: null,
      sortColumn: this.sortColumnExp,
      sortColumnDirection: this.sortColumn === 'RecentUpdate' ? 'DESC' : this.sortColumnDirectionExp,
      expiryDays: 7
    }
    this._airFreightService.getAllrates(obj).subscribe((res: any) => {
      this.publishloading = false;
      loading(false)
      if (res.returnId > 0) {
        this.totalPublishedRecordsExp = res.returnObject.recordsTotal
        this.expiredRateList = res.returnObject.data
      }
    })
  }

  /**
   * PAGING EVENT EMITTER OBSERVABLE FOR UI TABLE
   *
   * @param {string} type //fcl or lcl
   * @param {number} event //page number 0,1,2...
   * @memberof SeaFreightComponent
   */
  expPaging(type: any, event: any, tableType: string) {
    if (tableType === 'publish') {
      this.pageNoExp = event;
      this.getExpiredRates()
    }
  }

  sortedFiltersExp(event) {
    this.sortColumnExp = event.column
    this.sortColumnDirectionExp = event.direction
    this.getExpiredRates()
  }

  changeExpVisibility() {
    this.isExpirySectionOpenX = !this.isExpirySectionOpenX
    console.log(this.isExpirySectionOpenX)
  }

  getExpiryRates() {
    this._commonService.getExpiringRateCount(this.userProfile.ProviderID).subscribe((res: JsonResponse) => {
      if (res.returnId > 0) {
        this._sharedService.expNotifications.next(res.returnObject)
      } else { }
    })
  }

  /**
     *
     * Emitter for sorting columns
     * @param {*} type
     * @param {*} event
     * @memberof AIRTransportComponent
     */
  sortedFilters(type, event) {
    this.sortColumn = event.column
    this.sortColumnDirection = event.direction
    this.getAllPublishRates(1)
  }


  selectedItem(type, alltableOption) {
    if (type == 'add') {
      alltableOption.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var data = this.node();
        data.classList.add('selected');
        // ... do something with data(), or this.node(), etc
      });
    }
    else {
      alltableOption.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var node = this.node();
        node.classList.remove('selected');
        node.children[0].children[0].children[0].checked = false;
        // ... do something with data(), or this.node(), etc
      });
    }
  }
  orgfilter() {
    if (this.filterOrigin && typeof this.filterOrigin == "object" && Object.keys(this.filterOrigin).length) {
      return this.filterOrigin.PortID;
    }
    else if (this.filterOrigin && typeof this.filterOrigin == "string") {
      return -1;
    }
    else if (!this.filterOrigin) {
      return null;
    }

  }
  destfilter() {
    if (this.filterDestination && typeof this.filterDestination == "object" && Object.keys(this.filterDestination).length) {
      return this.filterDestination.PortID;
    }
    else if (this.filterDestination && typeof this.filterDestination == "string") {
      return -1;
    }
    else if (!this.filterDestination) {
      return null;
    }

  }

  discardDraft() {
    let discardarr = [];
    this.draftslist.forEach(elem => {
      discardarr.push(elem.carrierPricingDraftID)
    })
    const modalRef = this.modalService.open(DiscardDraftComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'small-modal',
      backdrop: 'static',
      keyboard: false
    });
    modalRef.result.then((result) => {
      if (result == "Success") {
        this.draftslist = [];
        this.allDraftRatesByAIR = [];
        this.draftDataBYAIR = [];
        this.publishRates = [];
        this._sharedService.updatedDraftsAir.next(this.draftslist)
        this.getAllDrafts();
      }
    }, (reason) => {
    });
    let obj = {
      data: discardarr,
      type: "draftAirRate"
    }
    modalRef.componentInstance.deleteIds = obj;
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);
  }


  delPubRecord() {
    if (!this.delPublishRates.length) return;
    const modalRef = this.modalService.open(ConfirmDeleteDialogComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'small-modal',
      backdrop: 'static',
      keyboard: false
    });
    modalRef.result.then((result) => {
      if (result == "Success") {
        this.checkedallpublishRates = false;
        if (this.allRatesList.length == this.delPublishRates.length) {
          this.allRatesList = [];
          this.delPublishRates = [];
        }
        else {
          for (var i = 0; i < this.delPublishRates.length; i++) {
            for (let y = 0; y < this.allRatesList.length; y++) {
              if (this.delPublishRates[i] == this.allRatesList[y].carrierPricingID) {
                this.allRatesList.splice(y, 1);
              }
            }
          }
          if (i == this.delPublishRates.length) {
            this.delPublishRates = [];
          }
        }
        // this.getAllPublishRates('fcl', 1)
        this.paging(1, 'publish')
        this.pageNo = 1
        this.draftPageNo = 1
      }
    }, (reason) => {
    });
    let obj = {
      data: this.delPublishRates,
      type: "publishAirRate"
    }
    modalRef.componentInstance.deleteIds = obj;
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);
  }

  deletepublishRecord() {
    this.delPubRecord()
  }

  // publishRate() {
  //   this._airFreightService.publishDraftRate(this.publishRates).subscribe((res: any) => {
  //     if (res.returnStatus == "Success") {
  //       for (var i = 0; i < this.publishRates.length; i++) {
  //         for (let y = 0; y < this.draftslist.length; y++) {
  //           if (this.draftslist[y].CarrierPricingSetID == this.publishRates[i]) {
  //             this.draftslist.splice(y, 1);
  //           }
  //         }
  //       }
  //       if (this.publishRates.length == i) {
  //         this.checkedalldraftRates = false;
  //         this.publishRates = [];
  //         this.generateDraftTable();
  //         this.getAllPublishRates();
  //       }
  //     }
  //   })
  // }

  publishRate() {
    let param;
    loading(true)
    // console.log(this.publishRates);

    try {
      param = {
        pricingIDList: (this.draftslist.length === this.publishRates.length) ? [-1] : this.publishRates,
        providerID: this.userProfile.ProviderID
      }
    } catch (error) {
      loading(false)
    }

    this._airFreightService.publishDraftRate(param).subscribe((res: any) => {
      loading(false)
      if (res.returnStatus == "Success") {
        for (var i = 0; i < this.publishRates.length; i++) {
          for (let y = 0; y < this.draftslist.length; y++) {
            if (this.draftslist[y].ProviderPricingDraftID == this.publishRates[i]) {
              this.draftslist.splice(y, 1);
            }
          }
        }
        this._toast.success(res.returnText, 'Success')
        this.checkedalldraftRates = false;
        this.publishRates = [];
        this.getAllPublishRates(1);
        this.getAllDrafts() // todo remove is when we get the mechanism for transfer data between components
      } else {
        this._toast.error(res.returnText, 'Publish Failed')
      }
    }, error => {
      loading(false)
    })
  }

  ports = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => (!term || term.length < 3)
        ? []
        : this.getFilterdPorts(term))
    )
  portsFormatter = (x: { PortName: string }) => x.PortName;

  getFilterdPorts(term): any[] {
    const { allPorts } = this
    let toSend = []
    try {
      const _firstIteration: Array<any> = allPorts.filter(v => v.PortCode.toLowerCase().indexOf(term.toLowerCase()) > -1)
      const _secondIteration: Array<any> = allPorts.filter(v => v.PortName.toLowerCase().indexOf(term.toLowerCase()) > -1)
      const _combinedArr = _firstIteration.concat(_secondIteration)
      toSend = removeDuplicates(_combinedArr, "PortCode");
      // console.log(toSend);

    } catch (error) {
      // console.log(error);
    }
    return toSend
  }
  formatter = (x: { PortName: string }) => x.PortName;



  /**
   *
   * RATE HISTORY DETAILS OF PUBLISHED RATES
   * @param {number} recId
   * @param {string} fortype
   * @memberof AirFreightComponent
   */
  rateHistory(recId, fortype) {
    const modalRef = this.modalService.open(RateHistoryComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'upper-medium-modal-history',
      backdrop: 'static',
      keyboard: false
    });
    // console.log(this.allAirLines)
    let obj = {
      id: recId,
      type: fortype,
      shippingLines: this.allAirLines,
      customers: this.allCustomers,
      ports: this.allPorts
    }
    modalRef.componentInstance.getRecord = obj;
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);
  }

  // rateValidity() {
  //   if (!this.delPublishRates.length) return;
  //   let updateValidity = [];
  //   for (let i = 0; i < this.allRatesList.length; i++) {
  //     for (let y = 0; y < this.delPublishRates.length; y++) {
  //       if (this.allRatesList[i].carrierPricingSetID == this.delPublishRates[y]) {
  //         updateValidity.push(this.allRatesList[i])
  //       }
  //     }
  //   }
  //   const modalRef = this.modalService.open(RateValidityComponent, {
  //     size: 'lg',
  //     centered: true,
  //     windowClass: 'upper-medium-modal',
  //     backdrop: 'static',
  //     keyboard: false
  //   });
  //   modalRef.result.then((result) => {
  //     if (result == 'Success') {
  //       this.getAllPublishRates();
  //       this.checkedallpublishRates = false
  //       this.delPublishRates = [];
  //     }
  //   });
  //   let obj = {
  //     data: updateValidity,
  //     type: "rateValidityAIR"
  //   }
  //   modalRef.componentInstance.validityData = obj;
  //   setTimeout(() => {
  //     if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
  //       document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
  //     }
  //   }, 0);
  // }

  /**
   *
   * EDIT PUBLISH RATE POPUP MODAL ACTION
   * @param {string} type //fcl or lcl
   * @returns
   * @memberof SeaFreightComponent
   */
  rateValidity() {
    this.allCustomers = JSON.parse(localStorage.getItem('customers'))
    console.log(this.allCustomers);
    if (!this.delPublishRates.length) return;
    let updateValidity = [];
    for (let i = 0; i < this.allRatesList.length; i++) {
      for (let y = 0; y < this.delPublishRates.length; y++) {
        if (this.allRatesList[i].carrierPricingID == this.delPublishRates[y]) {
          updateValidity.push(this.allRatesList[i])
        }
      }
    }
    if (updateValidity && updateValidity.length > 1) {
      const modalRef = this.modalService.open(RateValidityComponent, {
        size: 'lg',
        centered: true,
        windowClass: 'upper-medium-modal',
        backdrop: 'static',
        keyboard: false
      });
      modalRef.result.then((result) => {
        if (result) {
          this.getAllPublishRates(1);
          this.checkedallpublishRates = false
          this.delPublishRates = [];
        }
      });
      let obj = {
        data: updateValidity,
        type: 'rateValidityAIR'
      }
      modalRef.componentInstance.validityData = obj;
    } else if (updateValidity && updateValidity.length === 1) {
      const modalRef2 = this.modalService.open(AirRateDialogComponent, {
        size: 'lg',
        centered: true,
        windowClass: 'large-modal',
        backdrop: 'static',
        keyboard: false
      });
      modalRef2.result.then((result) => {
        if (result) {
          this.getAllPublishRates(1);
          this.checkedallpublishRates = false
          this.delPublishRates = [];
        }
      });
      // console.log(this.allCustomers);

      let object = {
        forType: 'AIR',
        data: updateValidity,
        addList: this.airCharges,
        customers: this.allCustomers,
        mode: 'publish'
      }
      // console.log(this.allCustomers);      
      modalRef2.componentInstance.selectedData = object;
    }
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);
  }



  saveTermNcond() {
    let obj = {
      providerID: this.userProfile.ProviderID,
      termsAndConditions: this.editorContent,
      transportType: "AIR",
      modifiedBy: this.userProfile.UserID
    }
    this._manageRatesService.termNCondition(obj).subscribe((res: any) => {
      if (res.returnStatus == "Success") {
        this._toast.success("Term and Condition saved Successfully", "");
        this._sharedService.termNcondAir.next(this.editorContent);
        this.disable = true;
      }
    })
  }


  /**
   *
   * Get Airports Dropdown List
   * @memberof AirFreightComponent
   */
  getPortsData() {
    loading(true)
    this._commonService.getPortsData('Airports').subscribe((res: any) => {
      this.allPorts = res
      localStorage.setItem("AirPortDetails", JSON.stringify(res));
      loading(false)
    }, (err: HttpErrorResponse) => {
      loading(false)
    })
  }

  /**
   *
   * Get Additional Port Charges
   * @memberof AirFreightComponent
   */
  getAdditionalData() {
    loading(true)
    const additionalCharges = (localStorage.hasOwnProperty('additionalCharges')) ? JSON.parse(localStorage.getItem('additionalCharges')) : null
    if (additionalCharges) {
      this.airCharges = additionalCharges.filter(e => e.modeOfTrans === 'AIR' && e.addChrType === 'ADCH')
    } else {
      this._seaFreightService.getAllAdditionalCharges(this.userProfile.ProviderID).subscribe((res: any) => {
        this.airCharges = res.filter(e => e.modeOfTrans === 'AIR' && e.addChrType === 'ADCH')
        localStorage.setItem('additionalCharges', JSON.stringify(res.filter(e => e.addChrType === 'ADCH')))
        loading(false)
      }, (err) => {
        loading(false)
      })
    }
    loading(false)
  }


  /**
   *
   * GET ALL DRAFT RATES FOR AIR
   * @memberof AirFreightComponent
   */
  getAllDrafts() {
    loading(true)
    this._airFreightService.getAirDraftRates(this.userProfile.ProviderID).pipe(untilDestroyed(this)).subscribe((res: any) => {
      loading(false)
      this.draftslist = res.returnObject
    }, (err: any) => {
      loading(false)
    })
  }

  /**
   * PAGING EVENT EMITTER OBSERVABLE FOR UI TABLE
   *
   * @param {string} type //fcl or lcl
   * @param {number} event //page number 0,1,2...
   * @memberof AirFreightComponent
   */
  public pageNo: number = 1
  public draftPageNo: any
  public totalPublishedRecords: number;
  paging(event: any, tableType: string) {
    if (tableType === 'publish') {
      this.pageNo = event;
      this.getAllPublishRates(event)
    } else {
      this.draftPageNo = event;
    }
  }

  /**
   *
   * EVENT EMITTER OBSERVABLE FOR UI TABLE COMPONENT
   * @param {object} event
   * @memberof AIRFreightComponent
   */
  tableCheckedRows(event) {
    if (event.type === 'publishFCL') {
      if (typeof event.list[0] === 'object') {
        if (event.list[0].type === 'history') {
          if (event.list[0].load === 'FCL') {
            this.rateHistory(event.list[0].id, 'Rate_FCL')
          } else if (event.list[0].load === 'LCL') {
            this.rateHistory(event.list[0].id, 'Rate_LCL')
          } else if (event.list[0].load === 'Rate_AIR') {
            this.rateHistory(event.list[0].id, 'Rate_AIR')
          }
        }
      } else {
        this.delPublishRates = event.list
      }
    } else if (event.type === 'draftFCL') {
      if (typeof event.list[0] === 'object') {
        if (event.list[0].type === 'delete') {
          if (event.list[0].load === 'LCL') {
            this.deleteRow(event.list[0].id)
          } else if (event.list[0].load === 'FCL') {
            this.deleteRow(event.list[0].id)
          }
        } else if (event.list[0].type === 'edit') {
          this.updatePopupRates(event.list[0].id, event.list[0].load)
        }
      } else {
        this.publishRates = event.list;
        // console.log(this.publishRates);

      }
    }
  }

  /**
   *
   * DELETE ROWM FROM DRAFTS TABLE FOR AIR
   * @param {number} id //row identifier
   * @memberof AIRFreightComponent
   */
  deleteRow(id) {
    const modalRef = this.modalService.open(ConfirmDeleteDialogComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'small-modal',
      backdrop: 'static',
      keyboard: false
    });
    modalRef.result.then((result) => {
      if (result) {
        this.getAllDrafts()
        this.pageNo = 1
        this.draftPageNo = 1
      }
    }, (reason) => {
    });
    let obj = {
      data: [id],
      type: "draftAirRate"
    }
    modalRef.componentInstance.deleteIds = obj;
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);
  }

  /**
   *
   *  GET AERO AFRICA FREIGHT
   * @type {any}
   * @memberof AirFreightComponent
   */
  public selectedProduct: any = null;
  public selectedPartner: any = null;
  public productsList: any = []
  getProductAir() {
    this._airFreightService.getProductAir().pipe(untilDestroyed(this)).subscribe((res: any) => {
      this.productsList = res
    }, (err: any) => {
      this._toast.error(err, 'Error')
    })
  }





  selectDocx(selectedFiles: NgFilesSelected, type): void {
    // console.log(selectedFiles);

    if (selectedFiles.status !== NgFilesStatus.STATUS_SUCCESS) {
      if (selectedFiles.status == 4) {
        this._toast.error('File format is not supported. Please upload supported format file.', '')
        return
      }
    }
    try {
      // if (selectedFiles.files.length > 2) {
      //   this._toast.error('Please select only 1 file to upload.', '')
      //   return;
      // }
      this.onFileChange(selectedFiles, type)
    } catch (error) {
      console.log(error);
    }


  }

  onFileChange(event, type) {
    let flag = 0;
    if (event) {
      try {
        const allDocsArr = []
        const fileLenght: number = event.files.length
        let reader = new FileReader();
        const element = event.files[0];
        let file = element
        reader.readAsDataURL(file);
        reader.onload = () => {
          const selectedFile: DocumentFile = {
            fileName: file.name,
            fileType: file.type,
            fileUrl: reader.result,
            fileBaseString: (reader as any).result.split(',').pop()
          }
          if (event.files.length < 2) {
            const docFile = JSON.parse(this.generateDocObject(selectedFile));
            allDocsArr.push(docFile);
            flag++
            if (flag === fileLenght) {
              this.uploadDocuments(allDocsArr, type)
            }
          }
          else {
            this._toast.error('Please select only ' + 1 + 'file to upload', '');
          }

        }
      }
      catch (err) {
        console.log(err);
      }
    }

  }
  async uploadDocuments(docFiles: Array<any>, type) {
    loading(true)
    const totalDocLenght: number = docFiles.length
    for (let index = 0; index < totalDocLenght; index++) {
      try {
        const resp: JsonResponse = await this.docSendService(docFiles[index])
        if (resp.returnStatus = 'Success') {
          let resObj = JSON.parse(resp.returnText);
          this._toast.success("Once validated, this rate sheet will be displayed as Unpublished Rates within 2-3 business days", 'File Uploaded Successfully');
        }
        else {
          this._toast.error("Error occured on upload", "");
        }
      } catch (error) {
        loading(false)
        this._toast.error("Error occured on upload", "");
      }
    }
    loading(false)
  }


  async docSendService(doc: any) {
    const resp: JsonResponse = await this._basicInfoService.docUpload(doc).toPromise()
    return resp
  }

  generateDocObject(selectedFile): any {
    let object = null

    object = this.selectedService.UploadRatesDocumnet[0];
    object.DocumentID = -1;
    object.UserID = this.userProfile.UserID;
    object.ProviderID = this.userProfile.ProviderID;
    object.DocumentFileContent = null;
    object.DocumentName = 'RATES';
    object.DocumentUploadedFileType = null;
    const fileTypeArr = selectedFile.fileName.split('.')
    const strFileType = fileTypeArr[fileTypeArr.length - 1]
    object.FileContent = [{
      documentFileName: selectedFile.fileName,
      documentFile: selectedFile.fileBaseString,
      documentUploadedFileType: strFileType
    }]
    object.ContainerLoadType = 'AIR'
    return JSON.stringify(object);
  }

  airlines = (text$: Observable<string>) => text$.pipe(
    debounceTime(200),
    map(term =>
      (term === '') ? this.allAirLines : this.allAirLines.filter(v => v.title && v.title.toLowerCase().indexOf(term.toLowerCase()) > -1))
  )

  airLineFormatter = (x: { title: string }) => {
    return x.title
  };

  getShippingLineImage($image: string) {
    return getImagePath(
      ImageSource.FROM_SERVER,
      "/" + $image,
      ImageRequiredSize.original
    );
  }

  public onFocus(e: Event): void {
    e.stopPropagation();
    setTimeout(() => {
      const inputEvent: Event = new Event('input');
      e.target.dispatchEvent(inputEvent);
    }, 0);
  }

  setTermsAndCondit(containerLoad: string) {
    this._manageRatesService.getTermsAndCondition(containerLoad, this.userProfile.ProviderID).subscribe((res: JsonResponse) => {
      const { returnId, returnObject } = res
      if (returnId > 0) {
        const { TermsCondition } = returnObject;
        this._sharedService.termNcondAir.next(TermsCondition);
        if (!this.isEmpty(returnObject)) {
          this.disable = true
        }
      }
    })
  }



  async openUploadHistory() {
    loading(true)
    const _docTypeId = this.selectedService.UploadRatesDocumnet[0].DocumentTypeID
    this._manageRatesService.getUploadSheetList(_docTypeId, this.userProfile.ProviderID).subscribe((res: JsonResponse) => {
      loading(false)
      const { returnId, returnText, returnObject } = res
      if (returnId > 0) {
        const _data = JSON.parse(returnText)
        if (_data && _data.length > 0) {
          try {
            const modalRef = this.modalService.open(UploadHistoryDialogComponent, {
              size: 'lg',
              centered: true,
              windowClass: 'large-modal',
              backdrop: 'static',
              keyboard: false
            });
            // console.log(this.allAirLines)
            modalRef.componentInstance._sheetList = _data;
            setTimeout(() => {
              if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
                document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
              }
            }, 0);
          } catch (error) {

          }
        } else {
          this._toast.warning('There are no uploaded sheets at the moment')
        }
      } else {
        this._toast.error(res.returnText)
      }
    }, error => loading(false))
  }


  partnerFilterList: { code: any, title: string, id: number }[] = []

  downloadDoc(_filename) {
    const _url = baseExternalAssets + _filename
    window.open(_url, '_blank');
    // var http = new XMLHttpRequest();
    // http.open('HEAD', _url);
    // http.onreadystatechange = () => {
    //   if (http.readyState === XMLHttpRequest.DONE) {
    //     if (http.status === 200) {
    //     } else {
    //       this._toastr.warning('The file is not available at the moment, please try again later', 'File Not Available')
    //     }
    //   }
    // };
    // http.send();
  }

}

export interface IUploadRateList {
  DocumentUploadDate: string;
  DocumentID: string;
  DocumentFileName: string;
  DocumentFile: string;
  DocumentUploadedFileType: string;
  IsApproved: boolean;
  DocumentLastStatus: string;
  CityName: string;
  CountryName: string;
  IpAddress: string;
  CountryCode: string;
  UploadBy: string;
}
