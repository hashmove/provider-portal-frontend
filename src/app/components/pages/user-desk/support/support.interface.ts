export interface Tutorial {
    TutorialID: number;
    TutorialTitle: string;
    TutorialDesc: string;
    TutorialGroup: string;
    ProcessName: string;
    ModuleName: string;
    ThumbnailURL: string;
    TutorialURL: string;
    TutorialType: string;
    SortingOrder: number;
    ReadMore?: boolean
    ReadLessText?: string
    SearchField?: string
}

export interface An {
    AnsID: number;
    AnsText: string;
    AnsImage?: any;
    AnsRequired: string;
}

export interface FAQ {
    QueID: number;
    QueText: string;
    ProcessName: string;
    ModuleName: string;
    QueType: string;
    Ans: An[];
    IsOpen?: boolean
    SearchField?: string
}

export interface ISupportFAQs {
    Tutorials: Tutorial[];
    FAQs: FAQ[];
}