import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../../services/common.service';
import { getLoggedUserData, isMobile, loading, removeDuplicates } from '../../../../constants/globalFunctions';
import { Tutorial, FAQ } from './support.interface';
import { JsonResponse } from '../../../../interfaces';
import { cloneObject } from '../reports/reports.component';
import { baseExternalAssets, helpDeskData } from '../../../../constants/base.url';

@Component({
  selector: 'app-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.scss']
})
export class SupportComponent implements OnInit {
  public helpSupport: any;
  public HelpDataLoaded: boolean;

  faqSearch: any = null
  tutSearch: any = null
  masterTutorials: Tutorial[] = []
  tutorials: Tutorial[] = []
  faqs: FAQ[] = []
  tutCats: { code: string, title: string }[] = []
  showFAQ = false
  baseExternalAssets = baseExternalAssets
  maxCharLenght = 65
  selectedCat = 'All'
  helpDeskData = helpDeskData
  isMobile = isMobile()

  constructor(private _commonService: CommonService) { }

  ngOnInit() {
    this._commonService.getHelpSupport(getLoggedUserData().ProviderID).subscribe((res: any) => {
      if (res.returnId > 0) {
        this.helpSupport = JSON.parse(res.returnText);
        this.HelpDataLoaded = true;
      }
    })

    if (location.href.includes('provider/support')) {
      loading(false)
      this.showFAQ = true
      this.setFAQ()
    }
  }



  setFAQ() {
    loading(true)
    this._commonService.getSupportFAQs().subscribe((res: JsonResponse) => {
      loading(false)
      if (res.returnId > 0) {
        this.masterTutorials = cloneObject(res.returnObject.Tutorials)
          .map(_tut => ({
            ..._tut, ReadMore: false,
            ReadLessText: _tut.TutorialDesc && _tut.TutorialDesc.length > this.maxCharLenght ?
              _tut.TutorialDesc.slice(0, this.maxCharLenght) : null,
            SearchField: _tut.TutorialTitle + ',' + _tut.TutorialDesc
          }))
        this.tutorials = cloneObject(this.masterTutorials)
        this.faqs = res.returnObject.FAQs.map(_faq => ({ ..._faq, IsOpen: false, SearchField: _faq.QueText }))
        const { masterTutorials } = this
        const _cats = masterTutorials.filter(_tut => _tut.TutorialGroup)
          .map(_tut => ({ code: _tut.ProcessName, title: _tut.TutorialGroup }))
        const _unqCats = removeDuplicates(_cats, 'title')
        if (_unqCats.length > 0) {
          this.tutCats = [{ code: 'ALL', title: 'All' }, ..._unqCats]
        } else {
          this.tutCats = []
        }
      }
    }, () => loading(false))
  }

  filterTutorials = (_cat: string) => {
    this.selectedCat = _cat
    this.tutorials = _cat && _cat !== 'All' ?
      this.masterTutorials.filter(_tut => _tut.TutorialGroup.toLowerCase() === _cat.toLowerCase()) : this.masterTutorials
  }



  playPause(_vidElId) {
    const myVideo: any = document.getElementById(_vidElId)
    if (myVideo.paused) myVideo.play()
    else myVideo.pause()
  }
}
