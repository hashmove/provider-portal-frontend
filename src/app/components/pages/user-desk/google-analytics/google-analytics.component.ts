import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { basesPartnerUrl } from '../../../../constants/base.url';
import { JsonResponse } from '../../../../interfaces/JsonResponse';
import { SharedService } from '../../../../services/shared.service';
import { ReportsService } from '../reports/reports.service';

@Component({
  selector: 'app-google-analytics',
  templateUrl: './google-analytics.component.html',
  styleUrls: ['./google-analytics.component.scss']
})
export class GoogleAnalyticsComponent implements OnInit {

  public selectedTaglineColor: string = '#2883e9';
  public selectedStatisticsColor: string = '#2883e9';
  analyticsData = null
  key: string = null
  urlId: string = null
  iframeUrl: any = null
  portalUrl: string = null


  constructor(
    private _reportsService: ReportsService,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.setAnalyticsData()
  }

  setAnalyticsData() {
    try {

      this.urlId = SharedService.GET_PROFILE_ID()
      this.portalUrl = basesPartnerUrl + 'partner/' + this.urlId
      this._reportsService.getGoogleAnalyticsData(this.urlId).subscribe((res: JsonResponse) => {
        if (res.returnId > 0) {
          const gaChartData: IAnalyticsData = res.returnObject
          localStorage.setItem('gaChartData', JSON.stringify(gaChartData))
          this.key = gaChartData.gaToken
          this.iframeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(`assets//google.analytics.html?key=${this.key}&urlId=${this.urlId}`);
        }
      })
    } catch (error) {
      console.log(error)
    }
  }

}





export interface IGaChart {
  gaChartName: string;
  gaChartType: string;
  gaChartBusinessLogic: string;
  jsonGAChartQuery: string;
  jsonGAChartType: string;
}

export interface IAnalyticsData {
  gaViewID: string;
  gaToken: string;
  gaCharts: IGaChart[];
}