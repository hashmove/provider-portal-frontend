import { Injectable } from '@angular/core'
import { baseApi } from '../../../../constants/base.url'
import { HttpClient, HttpParams } from '@angular/common/http'

@Injectable()
export class ViewBookingService {
  constructor(private _http: HttpClient) { }

  getBookingDetails(bookingId) {
    const url = `booking/GetProviderBookingSummary/${bookingId}`
    return this._http.get(baseApi + url)
  }
  getDocReasons() {
    const url = 'Document/GetDocumentReason'
    return this._http.get(baseApi + url)
  }
  getDocStatuses() {
    const url = 'Status/GetDocumentStatus'
    return this._http.get(baseApi + url)
  }

  getBookingReasons() {
    const url = 'booking/GetBookingReason'
    return this._http.get(baseApi + url)
  }

  getRateRequestReason() {
    const url = `RateRequest/GetRateRequestReasonForProvider`
    return this._http.get(baseApi + url)
  }

  getBookingStatuses($bookingType: string) {
    const url = `Status/GetBookingStatus/${$bookingType}`
    return this._http.get(baseApi + url)
  }
  getBookingSubStatuses($bookingType: string, statusId: any) {
    const url = `Activity/GetBookingStatus?statusFor=${$bookingType}&StatusID=${statusId}`
    return this._http.get(baseApi + url)
  }

  updateBookingStatus(data) {
    const url = 'booking/AddBookingStatus'
    return this._http.post(baseApi + url, data)
  }
  updateBookingActivity(data) {
    const url = 'Activity/AddActivityStatus'
    return this._http.post(baseApi + url, data)
  }

  cancelBooking(data) {
    const url = 'booking/CancelBooking'
    return this._http.put(baseApi + url, data)
  }

  notifyCustomer(data) {
    const url = 'booking/NotifyCustomer'
    return this._http.post(baseApi + url, data)
  }

  uploadDocReason(obj) {
    const url = 'Document/AddReason'
    return this._http.post(baseApi + url, obj)
  }

  approvedDocx(data) {
    const url = 'Document/AddDocumentStatus'
    return this._http.post(baseApi + url, data)

  }

  updateAgentInfo(data) {
    const url = `booking/UpdateBookingContactInfo/${data.BookingNature}/${data.BookingID}/${data.LoginUserID}/${data.PortNature}/${data.ContactInfoFor}`
    return this._http.put(baseApi + url, data.BookingSupDistInfo)
  }

  getCarrierSchedule(data) {
    const url = `booking/GetCarrierSchedule/${data.BookingID}/${data.PolID}/${data.PodID}/${data.ContainerLoadType}/${data.ResponseType}/${data.CarrierID}/${data.fromEtdDate}/${data.toEtdDate}`
    return this._http.get(baseApi + url)
  }

  saveCarrierSchedule(data) {
    const url = `booking/SaveCarrierSchedule`
    return this._http.post(baseApi + url, data)
  }


  /**
   *
   * SAVE CONTAINER DETAILS
   * @param {object} data
   * @returns
   * @memberof ViewBookingService
   */
  saveContainerDetails(data) {
    const url = `booking/SaveContainerInfo`
    return this._http.post(baseApi + url, data)
  }


  /**
   *
   * SAVE B/L NUMBER
   * @param {object} data
   * @returns
   * @memberof ViewBookingService
   */
  saveBLNumber(data, userID) {
    const url = `booking/UpdateBLInformation/${userID}`
    return this._http.put(baseApi + url, data)
  }


  /**
   * GUEST USERS KEY FOR VIEW BOOKING EXTERNAL SHARING
   *
   * @param {number} userId
   * @returns
   * @memberof ViewBookingService
   */
  getGuestUserKey(userId) {
    const url = `general/GetGuestUserKey/${userId}`
    return this._http.get(baseApi + url)
  }


  getLCLUnits() {
    const url = 'general/GetUnitType'
    return this._http.get(baseApi + url)
  }

  shareBookUrl(data) {
    const url = `booking/ShareBookingURL`
    return this._http.post(baseApi + url, data)
  }

  getTrackingInformation(bookingKey) {
    const url = `booking/GetTrackingInformation/${bookingKey}`
    return this._http.get(baseApi + url)
  }
  getQualityMonitoringInformation(bookingKey) {
    const url = `booking/GetQualityMonitoringInformation/${bookingKey}`
    return this._http.get(baseApi + url)
  }

  /**
   * Getting the IOT params
   * @memberof BookingService
   */
  getIOTParams(bookingID, mode) {
    const url = `booking/GetBookingParametersOfSensor/${bookingID}/${mode}`
    return this._http.get(baseApi + url)
  }

  getIOTSensorActions() {
    const url = `general/GetIOTSensorActionType`
    return this._http.get(baseApi + url)
  }

  getAllCarriers() {
    const url = `carrier/GetDropDownDetail/100`
    return this._http.get(baseApi + url)
  }
  getAllCarriersSea() {
    const url = `carrier/GetDropDownDetail/100?$filter=type%20eq%20%27SEA%27&$select=id,code,title,shortName,imageName`
    return this._http.get(baseApi + url)
  }

  getAllProviders() {
    const url = 'provider/GetProviderList?$select=returnId,returnStatus,returnText,returnCode&$expand=returnObject($select=ProviderID,ProviderName,ProviderImage)'
    return this._http.get(baseApi + url)
  }

  getAllCompanies(providerID) {
    const url = `company/GetProviderCompanyList/${providerID}`
    return this._http.get(baseApi + url)
  }
  getBookingsByPage(data) {
    const url = 'booking/GetDashboardBookingList'
    return this._http.post(baseApi + url, data)
  }

  getInvoices(data) {
    const url = 'booking/GetBookingInvoiceList'
    return this._http.post(baseApi + url, data)
  }
  getSpecialBookings(data) {
    const url = 'booking/GetDashboardRequestList'
    return this._http.post(baseApi + url, data)
  }
  getSpecialBookingsV2(data) {
    const url = 'raterequest/GetDashboardRequestList/V2'
    // const url = 'booking/GetDashboardRequestList'
    return this._http.post(baseApi + url, data)
  }
  getBookingsCount(data) {
    const url = 'booking/GetDashboardBookingListCount'
    return this._http.post(baseApi + url, data)
  }
  getShipModes() {
    const url = 'shippingMode/GetDropDownDetail/0'
    return this._http.get(baseApi + url)
  }

  getPortsData($portType?: string) {
    let portType = $portType
    if (!portType) {
      portType = 'SEA'
    }
    const url = `ports/GetPortsByType/${0}/${portType}`
    return this._http.get(baseApi + url)
  }

  getRouteDetailForAIR($bookingkey) {
    const url = `booking/GetBookingRouteDetailForAIR/${$bookingkey}`
    return this._http.get(baseApi + url)
  }

  getAdjustmentLogs($bookingkey, $shippingMode) {
    const url = `booking/GetBookingAmountAdjustmentLog/${$bookingkey}/${$shippingMode}`
    return this._http.get(baseApi + url)
  }

  getLogisticServices(providerID) {
    const url = `provider/GetProviderLogisticService/${providerID}`
    return this._http.get(baseApi + url)
  }

  postSchedule(toSend) {
    toSend.UploadedBy = 'PROVIDER'
    const url = 'Document/PostReponseSchedule'
    return this._http.post(baseApi + url, toSend)
  }

  postScheduleRequest(toSend) {
    toSend.UploadedBy = 'PROVIDER'
    const url = 'Document/PostReponseSchedule_EmptyVesselIMO'
    return this._http.post(baseApi + url, toSend)
  }

  updateBlInfo(data, userId) {
    const url = `booking/UpdateBLInformation/${userId}`
    return this._http.put(baseApi + url, data)
  }

  getDocList(docSubProcess, bookingType) {
    const url = `DocumentType/GetBookingDocumentType/${docSubProcess}/${bookingType}`
    return this._http.get(baseApi + url)
  }

  getDocListV1(docSubProcess, bookingList) {
    const url = `DocumentType/GetDocumentTypeList/${docSubProcess}/${bookingList}`
    return this._http.get(baseApi + url)
  }

  getDocObject(docTypeID) {
    const url = `DocumentType/GetDocumentTypeDetail/${docTypeID}`
    return this._http.get(baseApi + url)
  }

  getDocListV2(docSubProcess, businessLogic) {
    const url = `DocumentType/GetDocumentTypeDetail/${docSubProcess}/${businessLogic}`
    return this._http.get(baseApi + url)
  }


  updateSupInfo(data) {
    const url = `booking/UpdateBookingContactInfo/${data.BookingNature}/${data.BookingID}/${data.LoginUserID}/${data.PortNature}/${data.ContactInfoFor}`
    return this._http.put(baseApi + url, data.BookingSupDistInfo)
  }

  getBookingMandatoryDocuments(bookingId, providerId, companyID) {
    const url = `booking/GetBookingMandatoryDocuments/${bookingId}/${providerId}/${companyID}`
    return this._http.get(baseApi + url)
  }

  getBookingDocumentCharges(bookingId, mode) {
    const url = `booking/GetBookingDocumentCharges/${bookingId}/${mode}`
    return this._http.get(baseApi + url)
  }

  saveDispatchReceipt(data) {
    const url = `WarehouseGoodsReceipt/SaveDispatchReceipt`
    return this._http.post(baseApi + url, data)
  }
  editDispatchReceipt(data) {
    const url = `WarehouseGoodsReceipt/EditDispatchReceipt`
    return this._http.post(baseApi + url, data)
  }

  getDispatchReceiptList(bookingID) {
    const url = `WarehouseGoodsReceipt/GetDispatchReceiptList/${bookingID}`
    return this._http.get(baseApi + url)
  }


  getExpectedDispatch(bookingID) {
    const url = `WarehouseGoodsReceipt/GetExpectedDispatch/${bookingID}`
    return this._http.get(baseApi + url)
  }

  deleteDispatchReceipt(data) {
    const url = `WarehouseGoodsReceipt/DeleteDispatchReceipt`
    return this._http.post(baseApi + url, data)
  }


  getCalcUnits() {
    const url = 'general/GetUnitType'
    return this._http.get(baseApi + url)
  }

  saveBookingInspection(data) {
    const url = `BookingInspection/SaveBookingInspection`
    return this._http.post(baseApi + url, data)
  }

  editBookingInspection(data) {
    const url = `BookingInspection/EditBookingInspection`
    return this._http.post(baseApi + url, data)
  }


  updateAcceptedStatus(data) {
    const url = `BookingInspection/UpdateAcceptedStatus`
    return this._http.post(baseApi + url, data)
  }

  updateInspectionStatus(data) {
    const url = `BookingInspection/UpdateInspectionStatus`
    return this._http.post(baseApi + url, data)
  }

  getAllUsers(userId) {
    const url = 'users/GetUserListByAdmin?userID=' + userId
    return this._http.get(baseApi + url)
  }

  updateBookingWarehouseContactPerson(bookingID, contactPersonUserID, loginUserID) {
    const url = `booking/UpdateBookingWarehouseContactPerson/${bookingID}/${contactPersonUserID}/${loginUserID}`
    return this._http.put(baseApi + url, null)
  }

  updateIsAllGoodsReceived(data) {
    const url = `booking/UpdateIsAllGoodsReceived`
    return this._http.post(baseApi + url, data)
  }

  saveCarrierSchedule_Air(data) {
    const url = `booking/SaveCarrierSchedule_Air`
    return this._http.post(baseApi + url, data)
  }


  discardSpotRateRequest(bookingID, userID, providerID, discardBy) {
    const url = `booking/DiscardSpotRateRequest/${bookingID}/${userID}/${providerID}/${discardBy}`
    return this._http.delete(baseApi + url)
  }

  getRelatedBookingSharedURL(bookingKey) {
    const url = `booking/GetRelatedBookingSharedURLByProvider/${bookingKey}`
    return this._http.get(baseApi + url)
  }
  getBookingAdditionalParameters(bookingKey, parameterType) {
    const url = `booking/GetBookingAdditionalParameters/${bookingKey}/${parameterType}`
    return this._http.get(baseApi + url)
  }

  updateBookingAdditionalParameters(userId, data) {
    const url = `booking/UpdateBookingAdditionalParameters/${userId}`
    return this._http.put(baseApi + url, data)
  }

  updateWarehouseBookingSchedule(userId, data) {
    const url = `booking/UpdateWarehouseBookingSchedule//${userId}`
    return this._http.put(baseApi + url, data)
  }

  getBookingRouteDetail(bookingKey) {
    const url = `booking/GetBookingRouteDetail/${bookingKey}`
    return this._http.get(baseApi + url)
  }

  removeCarrierScheduleByBooking(bookingId, userId) {
    const url = `booking/RemoveCarrierScheduleByBooking/${bookingId}/${userId}`
    return this._http.delete(baseApi + url)
  }

  discardRequest(_userID, _body) {
    const url = `RateRequest/CancelRateRequest/${_userID}`
    return this._http.put(baseApi + url, _body)
  }

  discardRequestV2(_userID, _body) {
    const url = `RateRequest/DiscardRateRequest/${_userID}`
    return this._http.put(baseApi + url, _body)
  }

  getDashboardBookingChildList(data) {
    const url = 'booking/GetDashboardBookingChildList'
    return this._http.post(baseApi + url, data)
  }

  getDashboardRequestChildList(data) {
    const url = 'booking/GetDashboardRequestChildList'
    return this._http.post(baseApi + url, data)
  }

  getRequestResponses(data) {
    const url = 'RateRequest/GetDashboardRequestResponseList/V2'
    return this._http.post(baseApi + url, data)
  }

  getResponseData(responseID) {
    const url = `RateReqResponseFCL/Get/${responseID}`
    return this._http.get(baseApi + url)
  }

  getRateRequestStatusList() {
    const url = `Status/GetRateRequestStatus`
    return this._http.get(baseApi + url)
  }

  getRequestCriteria(requestID) {
    const url = `RateRequest/GetRequestSearchCriteria/${requestID}`
    return this._http.get(baseApi + url)
  }

  getTermsCondition(customerID, searchMode) {
    const url = `provider/GetProviderTermsCondition/${searchMode}/${customerID}`
    return this._http.get(baseApi + url)
  }

  getRequestPaymentTerms(requestID, providerID, responseTagID) {
    let url = `RateRequest/GetCustomerPaymentTerms/${requestID}/${providerID}/${responseTagID}`
    return this._http.get(baseApi + url)
  }

  getBookingPaymentTerms(bookingID) {
    let url = `booking/GetBookingPaymentTerms/${bookingID}`
    return this._http.get(baseApi + url)
  }

  updateRequestTerms(userID, body) {
    const url = `RateRequest/UpdateCustomerPaymentTerms/${userID}`
    return this._http.put(baseApi + url, body)
  }

  updateBookingTerms(userID, body) {
    const url = `booking/UpdateBookingPaymentTerms/${userID}`
    return this._http.put(baseApi + url, body)
  }

  getRequestAddParams(requestKey: string, portalType: string, parameterType: string, responseTagId: number) {
    const url: string = `RateRequest/GetRequestAdditionalParameters/${requestKey}/${portalType}/${parameterType}/${responseTagId}`
    return this._http.get(baseApi + url)
  }

  getInvoiceDet(bookingID) {
    const url: string = `booking/GetBookingInvoiceDetail/${bookingID}`
    return this._http.get(baseApi + url)
  }

  getBookingInvoiceStatus() {
    const url: string = `Status/GetBookingInvoiceStatus`
    return this._http.get(baseApi + url)
  }

  getBookingPaymentStatus() {
    const url: string = `Status/GetBookingPaymentStatus`
    return this._http.get(baseApi + url)
  }

  uploadBookingInvoice(data) {
    const url: string = `booking/UploadBookingInvoice`
    return this._http.post(baseApi + url, data)
  }

  getAddedInvoice(invoiceID) {
    const url: string = `booking/GetBookingInvoicePayable/${invoiceID}`
    return this._http.get(baseApi + url)
  }

  updateActual(userID, data) {
    const url: string = `booking/UpdateBookingATD_ATA/${userID}`
    return this._http.put(baseApi + url, data)
  }

  getServerDate() {
    const url: string = `general/GetServerUTCDate`
    return this._http.get(baseApi + url)
  }

  uploadBookingInvoiceCustomer(data) {
    const url: string = `booking/UploadBookingInvoiceCustomer`
    return this._http.post(baseApi + url, data)
  }

  approveBookingInvoice(data) {
    const url: string = `booking/ApproveBookingInvoice`
    return this._http.post(baseApi + url, data)
  }

  saveBookingInvoiceReceipt(data) {
    const url: string = `booking/SaveBookingInvoiceReceipt`
    return this._http.post(baseApi + url, data)
  }

  getBookingInvoiceReceipt(receiptID) {
    const url: string = `booking/GetBookingInvoiceReceipt/${receiptID}`
    return this._http.get(baseApi + url)
  }


  saveBookingInvoicePayment(data) {
    const url: string = `booking/SaveBookingInvoicePayment`
    return this._http.post(baseApi + url, data)
  }

  getBookingInvoicePayment(paymenttID) {
    const url: string = `booking/GetBookingInvoicePayment/${paymenttID}`
    return this._http.get(baseApi + url)
  }

  rejectPartnerInvoice(obj) {
    const url = 'booking/RejectBookingInvoice'
    return this._http.post(baseApi + url, obj)
  }

  updateInvoiceStatus(userID, data) {
    const url: string = `booking/AddBookingInvoiceStatus/${userID}`
    return this._http.put(baseApi + url, data)
  }

  getRoleActions(roleID, objBL, objPortal, objType) {
    const url: string = `approlerights/GetObjectAction/${roleID}/${objBL}/${objPortal}/${objType}`
    return this._http.get(baseApi + url)
  }

  getBookingInvoicePaymentsDetail(invoiceID, paymentID) {
    const url = `booking/GetBookingInvoicePaymentsDetail/${invoiceID}/${paymentID}`
    return this._http.get(baseApi + url)
  }

  getBookingInvoicePayments(invoiceID, paymentID) {
    const url = `booking/GetBookingInvoicePayments/${invoiceID}/${paymentID}`
    return this._http.get(baseApi + url)
  }

}
