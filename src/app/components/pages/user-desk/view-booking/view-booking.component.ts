import { Component, OnInit, ViewEncapsulation, OnDestroy, trigger, transition, style, animate, ViewChild, ElementRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, NgbDateStruct, NgbDateParserFormatter, NgbAccordion, NgbPanelChangeEvent, NgbTooltipConfig } from '@ng-bootstrap/ng-bootstrap';
import { loading, getImagePath, ImageSource, ImageRequiredSize, statusCode, EMAIL_REGEX, isJSON, getLoggedUserData, encryptBookingID, getJsonDepartureDays, removeDuplicates, base64Decode, defaultReadRights, getAlphaNumString, getWhSpace, getQueryParams, changeCase, isArrayValid, disabledRights, isMobile } from '../../../../constants/globalFunctions';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { CommonService } from '../../../../services/common.service';
import { ViewBookingService } from './view-booking.service';
import { BookingInvoiceComponent } from '../booking-invoice/booking-invoice.component';
import { ReUploadDocComponent } from '../../../../shared/dialogues/re-upload-doc/re-upload-doc.component';
import { IconSequence } from '@agm/core/services/google-maps-types';
import { baseExternalAssets, portalOrigin } from '../../../../constants/base.url';
import { LatLngBounds } from '@agm/core';
import { BookingReason, BookingStatusUpdationComponent } from '../../../../shared/dialogues/booking-status-updation/booking-status-updation.component';
import { SharedService } from '../../../../services/shared.service';
import { cloneObject, extractColumn } from '../reports/reports.component';
import { JsonResponse } from '../../../../interfaces/JsonResponse';
import * as moment from 'moment'
import { DashboardService } from '../dashboard/dashboard.service';
import { UploadComponent } from '../../../../shared/upload-component/upload-component';
import { VesselScheduleDialogComponent } from '../../../../shared/dialogues/vessel-schedule-dialog/vessel-schedule-dialog.component';
import { Observable } from 'rxjs/Observable';
import { debounceTime, map } from 'rxjs/operators';
import { NgbDateFRParserFormatter } from '../../../../constants/ngb-date-parser-formatter';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { AddContainersComponent } from '../../../../shared/dialogues/add-containers/add-containers.component';
import { AddBlComponent } from '../../../../shared/dialogues/add-bl/add-bl.component';
import { ShareViewBooking } from '../../../../shared/dialogues/share-view-booking/share-view-booking.component';
import { GuestService, getDefaultHMUser, isUserLogin } from '../../../../services/jwt.injectable';
import { ConfirmDialogGenComponent, ConfirmDialogContent } from '../../../../shared/dialogues/confirm-dialog-generic/confirm-dialog-generic.component';
import { MultiAirlinesComponent } from '../multi-airlines/multi-airlines.component';
import { AdjustmentLogsComponent } from '../../../../shared/dialogues/adjustment-logs/adjustment-logs.component';
import { CertificateChargesComponent } from '../certificate-charges/certificate-charges.component';
import { AddDispatchComponent } from '../../../../shared/dialogues/add-dispatch/add-dispatch.component';
import { AssayerUpdateScheduleComponent } from '../assayer-update-schedule/assayer-update-schedule.component';
import { AssayerContainerInfoComponent } from '../assayer-container-info/assayer-container-info.component';
import { CountryDropdown } from '../../../../shared/country-select/country-select.component';
import { IWhUser } from '../warehouse/warehouse.component';
import { compareValues } from '../billing/billing.component';
import { TermsConditDialogComponent } from '../../../../shared/dialogues/terms-condition/terms-condition.component';
declare var google: any;
import { of } from 'rxjs/observable/of';
import { ICarrierFilter, IRelatedBookings, UserDocument, UserInfo, DocumentFile, BookingDocumentDetail, TrackingMonitoring, QaualitMonitorResp, QualityMonitoringAlertData, MetaInfoKeysDetail } from '../../../../interfaces';
import { ChatComponent, IChatBody, IChatResponse } from '../../../../shared/dialogues/chat/chat.component';
import { AddScheduleComponent } from '../../../../shared/dialogues/add-schedule/add-schedule.component';
import { isShareLogin } from '../../../../http-interceptors/interceptor';
import { PaymentTermsDialogComponent } from '../../../../shared/dialogues/payment-terms/payment-terms.component';
import { IPaymentTerms, ITermOption } from '../../../../shared/dialogues/payment-terms/payment-terms.interface';
@Component({
  selector: 'app-view-booking',
  templateUrl: './view-booking.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./view-booking.component.scss'],
  animations: [
    trigger(
      'enterAnimation', [
      transition(':enter', [
        style({ transform: 'translateY(-20%)', opacity: 0 }),
        animate('300ms', style({ transform: 'translateY(0)', opacity: 1 }))
      ]),
      transition(':leave', [
        style({ transform: 'translateY(0)', opacity: 1 }),
        animate('300ms', style({ transform: 'translateY(-20%)', opacity: 0 }))
      ])
    ]
    )
  ],
  providers: [{ provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter }]
})
export class ViewBookingComponent implements OnInit, OnDestroy {
  @ViewChild('dp') eventDate;
  public readMoreClass = false;
  public readMoreClass2 = false;
  public vendorAboutBtn: any = 'Read More';
  public vendorAboutBtn2: any = 'Read More';

  public statusCode: any = statusCode;
  public zoomlevel: number = 2;
  public location: any = { lat: undefined, lng: undefined };
  // public bookingDetails: BookingDetails;
  public bookingDetails: any;
  public paramSubscriber: any;
  public HelpDataLoaded: boolean;
  public ProviderEmails: any[];
  public ProviderPhones: any[];
  public helpSupport: any;
  public baseExternalAssets: string = baseExternalAssets;
  public certOrigin;
  public ladingBill;
  public invoiceDocOrigin;
  public invoiceDocDestination;
  public packingListDocOrigin;
  public packingListDocDestination;
  private userProfile;
  private bookingId;
  public mapOrgiToDest: any = [];
  public icon = {
    url: '../../../../../assets/images/icons/Icons_Location_blue.svg',
    scaledSize: {
      width: 25,
      height: 25
    }
  }
  isMobile = isMobile()
  public polyOptions: IconSequence = {
    icon: {
      path: 'M 0,-1 0,1',
      strokeOpacity: 1,
      scale: 4
    },
    offset: '0',
    repeat: '20px'
  }
  public bookingReasons = [];
  public bookingStatuses = [];
  public countryList: any[] = [];
  public selectedReason: any = {};
  private approvedStatus: any;

  // forms
  public SupInfoFormOrigin: any;
  public SupInfoFormDest: any;
  public AgentInfoFormOrigin: any;
  public AgentInfoFormDest: any;

  public supDestphoneCode: string;
  public supFlagImgDest: string;
  public supDestCountryId: string;
  public supOrgphoneCode: string;
  public supFlagImgOrg: string;
  public supOrgCountryId: string;
  public agentDestphoneCode: string;
  public agentFlagImgDest: string;
  public agentDestCountryId: string;
  public agentOrgphoneCode: string;
  public agentFlagImgOrg: string;
  public agentOrgCountryId: string;

  // togglers
  public editSupOrgToggler: boolean = false
  public editSupDestToggler: boolean = false
  public editAgentorgToggler: boolean = false
  public editAgentDestToggler: boolean = false

  public ShippingOrgInforeadMoreClass = false;
  public ShippingOrgInfoBtn: any = 'Read More';

  public wareHouse: any = {};
  public showScheduler: boolean = true
  public searchCriteria: any
  public isTruck: boolean = false;

  public isGuestLogin: boolean = false
  public objectRights: any
  public documentsRights: any = [{ ObjCode: 'UD', ObjName: 'UploadDocument', ObjRights: 'RW' }]
  public agentInfoRights: any = [{ ObjCode: 'AI', ObjName: 'AgentInformation', ObjRights: 'RW' }]
  public guestBookingKey: any
  public invoiceRights: any = [{ ObjCode: 'VI', ObjName: 'ViewInvoice', ObjRights: 'R' }]
  public bookingCnclRights: any = [{ ObjCode: 'BC', ObjName: 'BookingCancellation', ObjRights: 'RW' }]

  public providerBookingStatusRights = { ObjCode: 'PBS', ObjName: 'ProviderBookingStatus', ObjRights: 'RW' }
  public providerUploadDocumentRights = { ObjCode: 'PUD', ObjName: 'ProviderUploadDocument', ObjRights: 'RW' }
  public providerApproveDocumentRights = { ObjCode: 'PAD', ObjName: 'ProviderApproveDocument', ObjRights: 'RW' }
  public providerRejectDocumentRights = { ObjCode: 'PRD', ObjName: 'ProviderRejectDocument', ObjRights: 'RW' }
  lclViewContainers: Array<any> = []
  public hasAdjustedAmount: boolean = false
  searchCarrierList: ICarrierFilter[] = []
  public ShipDtlForm: any;
  shipDtl_invoice = false
  shipDtl_custom = false
  shipDtl_goods_dec = false
  shipDtl_bill_lading = false
  shipDtl_export_no = false
  shipDtl_awb_no = false

  editShipDetails
  showAcc = true
  hasContainerNumber: boolean = false
  enteredContainerNumber = null
  isPmex: boolean = false
  hasMandDocs: boolean = false
  manDocsString: string[] = []
  pmexCharges: PmexCharges
  showNormalDocs = true
  refreshSummary = false

  masterStatus: any[] = []
  statuses = [
    { code: 'PENDING', name: 'Pending', disabled: false },
    { code: 'APPROVED', name: 'Approved', disabled: false },
    { code: 'REJECTED', name: 'Rejected', disabled: false },
    { code: 'RESCHEDULED', name: 'Rescheduled', disabled: true },
  ]

  containerStuffingStatuses = [
    { code: 'PENDING', name: 'Pending', disabled: false },
    { code: 'APPROVED', name: 'Approved', disabled: false },
    { code: 'REJECTED', name: 'Rejected', disabled: false },
    { code: 'RESCHEDULED', name: 'Rescheduled', disabled: true },
    { code: 'ACCEPTED', name: 'Accepted', disabled: true },
    { code: 'PURPOSERESCHEDULE', name: 'Purpose New Time', disabled: true },
  ]

  commodityInspection: IBookingInpectionDetail
  containerInspection: IBookingInpectionDetail

  selectedCommodityStatus = 'PENDING'
  selectedContainerStatus = 'PENDING'
  cmdSchDate = null
  contSchDate = null

  hasCommodityChanged = false
  hasContainerChanged = false

  comoodityComments = ''
  containerComments = ''

  assayerCertificates: any[] = []

  //Warehouse Contact Form
  @ViewChild('_wh_name') _wh_name: ElementRef
  @ViewChild('_wh_phone') _wh_phone: ElementRef
  @ViewChild('_wh_email') _wh_email: ElementRef

  warehouseUserInfo
  countryFlagImage
  phoneCode
  transPhoneCode
  phoneCountryId
  selCountry: CountryDropdown

  whUsers: IWhUser[] = []
  selectedWHUser: IWhUser
  // warehouseDetail
  isWhEdit = false
  awbNumber = null
  isEnterprise: boolean = false

  // public airDtlForm: any;
  airDtl_airline = null
  airDtl_flight_code = null
  airDtl_flight_num = null
  airDtl_date = null
  hmCommisssion = 0
  showCommission = false
  relatedBookings: IRelatedBookings[] = []
  public editorContentAgent: any;
  public editorContentConsignee: any;
  private toolbarOptions = [
    ['bold', 'italic', 'underline'],        // toggled buttons

    [{ 'list': 'ordered' }, { 'list': 'bullet' }],
    [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
    [{ 'direction': 'rtl' }],                         // text direction

    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

    [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
    [{ 'align': [] }],

    ['clean']                                         // remove formatting button
  ];
  public editorOptionsLCL = {
    placeholder: 'insert content...',
    modules: {
      toolbar: this.toolbarOptions
    }
  };

  toggleInfoConsignee = false
  toggleInfoAgent = false
  instructionsData = {
    Shipper: null,
    Agent: null
  }
  isIssuedForBuyer: boolean = false
  lclContainerNo: any = null
  listNav = 'booking'
  chatResponse: IChatBody
  paymentTerm: IPaymentTerms
  paymentTermsOptions: ITermOption[] = []
  expiryDate: string = null
  bookDate: string = null
  showTimer = false
  isExpired = false
  showSchedule = false
  bookingSchedule
  editAtd = false
  atdDate = null

  editAta = false
  ataDate = null
  atdLimit = null
  selectedDocument: number
  // public displayMonths = 1;
  public navigation = 'select';
  public showWeekNumbers = false;
  public outsideDays = 'visible';
  public uploadToggleBTn = true;
  public uploadToggleBTn2 = true;
  public uploadToggleBTn3 = true;
  public currentDocObject: UserDocument

  //Document Upload Stuff
  public uploadForm: FormGroup
  public dateModel: NgbDateStruct
  public minDate
  public loading: boolean;
  public loginUser: UserInfo
  public additionalDocumentsCustomers: UserDocument[] = []
  // public additionalDocumentsProviders: UserDocument[] = []
  public add_docs_prov: UserDocument
  progress: number = 0
  //Form Validation Work
  sup_origin_name_error: boolean = false
  sup_origin_address_error: boolean = false
  sup_origin_contact_error: boolean = false
  sup_origin_email_error: boolean = false

  sup_dest_name_error: boolean = false
  sup_dest_address_error: boolean = false
  sup_dest_contact_error: boolean = false
  sup_dest_email_error: boolean = false

  agent_origin_name_error: boolean = false
  agent_origin_address_error: boolean = false
  agent_origin_contact_error: boolean = false
  agent_origin_email_error: boolean = false

  agent_dest_name_error: boolean = false
  agent_dest_address_error: boolean = false
  agent_dest_contact_error: boolean = false
  agent_dest_email_error: boolean = false
  displayMonths = this.isMobile ? 1 : 2


  constructor(
    private _modalService: NgbModal,
    private _toast: ToastrService,
    private _viewBookingService: ViewBookingService,
    private _activeRouter: ActivatedRoute,
    private _router: Router,
    private _commonService: CommonService,
    private _sharedService: SharedService,
    private _dashboardService: DashboardService,
    private dateParser: NgbDateParserFormatter,
    private _jwtService: GuestService,
    private _fileConfig: NgbTooltipConfig
  ) { }

  ngOnInit() {
    try {
      if (this.isMobile)
        this._fileConfig.disableTooltip = true
    } catch { }
    this.setBackNavigation()
    this.getCarreierList()
    this.init()
  }

  setBackNavigation() {
    try {
      const _state = getQueryParams(location.search) as any
      if (_state.dir_from === 'req_list') {
        this.listNav = 'request'
      } else {
        this.listNav = 'booking'
      }
    } catch {
      this.listNav = 'booking'
    }
  }

  changeSchedule() {
    this.showScheduler = !this.showScheduler
    this.getCarreierList()
  }

  openGallery() { }

  getCarreierList() {
    loading(true)
    this._viewBookingService.getAllCarriersSea().subscribe((res: any) => {
      loading(false)
      if (res) {
        this.searchCarrierList = res
      }
    }, error => loading(false))
  }

  async init() {
    // EXTERNAL BOOKING WORKING
    let url = window.location.href
    let x = url.split('booking-detail')
    let d3key = x[1].split('/')
    if (d3key.length > 2) {
      this.loading = true
      const res = await this._jwtService.shareLogin(d3key[2])
      this.isGuestLogin = true
      // localStorage.setItem('isGuestLogin', 'true')
      try {
        this.objectRights = JSON.parse(this._jwtService.getRightObj())
        if (this.objectRights.IsIssuedForBuyer) {
          this.isIssuedForBuyer = this.objectRights.IsIssuedForBuyer
        }
      } catch (error) { }
      if (this.objectRights && this.isGuestLogin) {
        this.setRights()
      }
      this.loading = false
    } else {
      this.userProfile = getLoggedUserData()
    }

    this.setInit()


    this._commonService.getCountryList().subscribe((res: any) => {
      try {
        let List: any = res;
        List.map(obj => {
          obj.desc = JSON.parse(obj.desc);
        });
        this.countryList = List;
      } catch (error) { }
      this.getParams();
    }, error => {
      this.getParams();
    })

    this.AgentInfoFormOrigin = new FormGroup({
      name: new FormControl(null, [Validators.required, Validators.pattern(/[a-zA-Z-][a-zA-Z -]*$/), Validators.minLength(2), Validators.maxLength(100)]),
      address: new FormControl(null, [Validators.maxLength(200), Validators.minLength(10)]),
      email: new FormControl(null, [
        Validators.required,
        Validators.pattern(EMAIL_REGEX),
        Validators.maxLength(320)
      ]),
      contact: new FormControl(null, [Validators.pattern(/^(?!(\d)\1+(?:\1+){0}$)\d+(\d+){0}$/), Validators.minLength(7), Validators.maxLength(13)]),
    });
    this.AgentInfoFormDest = new FormGroup({
      name: new FormControl(null, [Validators.required, Validators.pattern(/[a-zA-Z-][a-zA-Z -]*$/), Validators.minLength(2), Validators.maxLength(100)]),
      address: new FormControl(null, [Validators.maxLength(200), Validators.minLength(10)]),
      email: new FormControl(null, [
        Validators.required,
        Validators.pattern(EMAIL_REGEX),
        Validators.maxLength(320)
      ]),
      contact: new FormControl(null, [Validators.pattern(/^(?!(\d)\1+(?:\1+){0}$)\d+(\d+){0}$/), Validators.minLength(7), Validators.maxLength(13)]),
    });

    this.SupInfoFormOrigin = new FormGroup({
      name: new FormControl(null, [Validators.required, Validators.pattern(/[a-zA-Z-][a-zA-Z -]*$/), Validators.minLength(2), Validators.maxLength(100)]),
      address: new FormControl(null, [Validators.maxLength(200), Validators.minLength(10)]),
      email: new FormControl(null, [
        Validators.required,
        Validators.pattern(EMAIL_REGEX),
        Validators.maxLength(320)
      ]),
      contact: new FormControl(null, [Validators.required, Validators.pattern(/^(?!(\d)\1+(?:\1+){0}$)\d+(\d+){0}$/), Validators.minLength(7), Validators.maxLength(13)]),
    });
    this.SupInfoFormDest = new FormGroup({
      name: new FormControl(null, [Validators.required, Validators.pattern(/[a-zA-Z-][a-zA-Z -]*$/), Validators.minLength(2), Validators.maxLength(100)]),
      address: new FormControl(null, [Validators.maxLength(200), Validators.minLength(10)]),
      email: new FormControl(null, [
        Validators.required,
        Validators.pattern(EMAIL_REGEX),
        Validators.maxLength(320)
      ]),
      contact: new FormControl(null, [Validators.required, Validators.pattern(/^(?!(\d)\1+(?:\1+){0}$)\d+(\d+){0}$/), Validators.minLength(7), Validators.maxLength(13)]),
    });


    // this.airDtlForm = new FormGroup({
    //   airDtl_airline: new FormControl(null, [Validators.required, Validators.pattern(/[a-zA-Z-][a-zA-Z -]*$/), Validators.minLength(2), Validators.maxLength(100)]),
    //   airDtl_flight_num: new FormControl(null, [Validators.maxLength(200), Validators.minLength(10)]),
    //   airDtl_date: new FormControl(null, [Validators.required, Validators.pattern(/^(?!(\d)\1+(?:\1+){0}$)\d+(\d+){0}$/), Validators.minLength(7), Validators.maxLength(13)]),
    // });

    this.warehouseUserInfo = new FormGroup({
      name: new FormControl({ value: null }, [Validators.required, Validators.minLength(2), Validators.maxLength(100)]),
      email: new FormControl({ value: null, disabled: true }, [
        Validators.required,
        Validators.pattern(EMAIL_REGEX),
        Validators.minLength(1),
        Validators.maxLength(320)
      ]),
      contact: new FormControl({ value: null, disabled: true }, [Validators.pattern(/^(?!(\d)\1+(?:\1+){0}$)\d+(\d+){0}$/), Validators.minLength(7), Validators.maxLength(13)]),
    });


    this.ShipDtlForm = new FormGroup({
      invoice: new FormControl(null, [Validators.maxLength(20), Validators.minLength(1)]),
      custom: new FormControl(null, [Validators.maxLength(20), Validators.minLength(1)]),
      goods_dec: new FormControl(null, [Validators.maxLength(20), Validators.minLength(1)]),
      bill_lading: new FormControl(null, [Validators.maxLength(16), Validators.minLength(9)]),
      export_no: new FormControl(null, [Validators.maxLength(20), Validators.minLength(1)]),
      awb_no: new FormControl(null, [Validators.maxLength(11), Validators.minLength(11)]),
    })

    this._commonService.getHelpSupport(isUserLogin() ? this.loginUser.ProviderID : -1).subscribe((res: any) => {
      if (res.returnId > 0) {
        this.helpSupport = JSON.parse(res.returnText)
        this.HelpDataLoaded = true
      }
    })
  }

  setRights() {
    this.documentsRights = this.objectRights.JsonObjectRights.filter(e => e.ObjCode === 'UD')
    this.agentInfoRights = this.objectRights.JsonObjectRights.filter(e => e.ObjCode === 'AI')
    this.invoiceRights = this.objectRights.JsonObjectRights.filter(e => e.ObjCode === 'VI')
    this.bookingCnclRights = this.objectRights.JsonObjectRights.filter(e => e.ObjCode === 'BC')

    try { this.providerBookingStatusRights = this.objectRights.JsonObjectRights.filter(e => e.ObjCode === 'PBS')[0] } catch (error) { }
    try { this.providerUploadDocumentRights = this.objectRights.JsonObjectRights.filter(e => e.ObjCode === 'PUD')[0] } catch (error) { }
    try { this.providerApproveDocumentRights = this.objectRights.JsonObjectRights.filter(e => e.ObjCode === 'PAD')[0] } catch (error) { }
    try { this.providerRejectDocumentRights = this.objectRights.JsonObjectRights.filter(e => e.ObjCode === 'PRD')[0] } catch (error) { }
  }

  getParams() {
    this.paramSubscriber = this._activeRouter.params.pipe(untilDestroyed(this)).subscribe(params => {
      this.bookingId = params['id'];
      this.currentBookingId = params['id'];
      // (+) converts string 'id' to a number
      if (this.bookingId) {
        try {
          if (!this.isGuestLogin) {
            const _bookkey = base64Decode(this.bookingId)
            if (!_bookkey.includes(this.loginUser.ProviderID + '')) {
              this._toast.warning('This booking does not belongs to ' + this.loginUser.CompanyName, 'Unmatched Booking')
              this._router.navigate(['/provider/allbookings']);
              return
            }
          }
        } catch (error) {

        }
        this.getBookingDetail(this.bookingId);
        this.getdocStatus();
      }
    });
  }

  setAgentOrgInfo() {
    this.editAgentorgToggler = false;
    if (this.bookingDetails.JsonAgentOrgInfo && this.bookingDetails.JsonAgentOrgInfo.Name) {
      this.originToggleAgent = true
      this.originToggleClassAgent = 'Show'
    } else {
      this.originToggleAgent = false
      this.originToggleClassAgent = 'Hide'
    }
    if (this.bookingDetails.JsonAgentOrgInfo.Name) {
      this.AgentInfoFormOrigin.controls['name'].setValue(this.bookingDetails.JsonAgentOrgInfo.Name);
    }
    if (this.bookingDetails.JsonAgentOrgInfo.Email) {
      this.AgentInfoFormOrigin.controls['email'].setValue(this.bookingDetails.JsonAgentOrgInfo.Email);
    }
    if (this.bookingDetails.JsonAgentOrgInfo.Address) {
      this.AgentInfoFormOrigin.controls['address'].setValue(this.bookingDetails.JsonAgentOrgInfo.Address);
    }
    if (this.bookingDetails.JsonAgentOrgInfo.PhoneNumber) {
      this.AgentInfoFormOrigin.controls['contact'].setValue(this.bookingDetails.JsonAgentOrgInfo.PhoneNumber);
    }
    if (this.bookingDetails.JsonAgentOrgInfo.PhoneCountryID) {
      let object = this.countryList.find(obj => obj.id === this.bookingDetails.JsonAgentOrgInfo.PhoneCountryID);
      this.agentFlagImgOrg = object.code;
      let description = object.desc;
      this.agentOrgphoneCode = description[0].CountryPhoneCode;
      this.agentOrgCountryId = object.id
      this.selCountryOrigin = object
    }

  }
  setAgentDestInfo() {
    this.editAgentDestToggler = false;
    if (this.bookingDetails.JsonAgentDestInfo && this.bookingDetails.JsonAgentDestInfo.Name) {
      this.destinToggleAgent = true
      this.destinToggleClassAgent = 'Show'
    } else {
      this.destinToggleAgent = true
      this.destinToggleClassAgent = 'Show'
    }
    if (this.bookingDetails.JsonAgentDestInfo.Name) {
      this.AgentInfoFormDest.controls['name'].setValue(this.bookingDetails.JsonAgentDestInfo.Name);
    }
    if (this.bookingDetails.JsonAgentDestInfo.Email) {
      this.AgentInfoFormDest.controls['email'].setValue(this.bookingDetails.JsonAgentDestInfo.Email);
    }
    if (this.bookingDetails.JsonAgentDestInfo.Address) {
      this.AgentInfoFormDest.controls['address'].setValue(this.bookingDetails.JsonAgentDestInfo.Address);
    }
    if (this.bookingDetails.JsonAgentDestInfo.PhoneNumber) {
      this.AgentInfoFormDest.controls['contact'].setValue(this.bookingDetails.JsonAgentDestInfo.PhoneNumber);
    }
    if (this.bookingDetails.JsonAgentDestInfo.PhoneCountryID) {
      let object = this.countryList.find(obj => obj.id === this.bookingDetails.JsonAgentDestInfo.PhoneCountryID);
      this.agentFlagImgDest = object.code;
      let description = object.desc;
      this.agentDestphoneCode = description[0].CountryPhoneCode;
      this.agentDestCountryId = object.id
      this.selCountryDest = object
    }

  }
  setSupOrgInfo(_shipperInfo: { Name: any; Email: any; Address: any; PhoneNumber: any; PhoneCountryID: any; }) {
    this.editSupOrgToggler = false;
    if (this.bookingDetails.JsonShippingOrgInfo && _shipperInfo.Name) {
      this.originToggleSupp = true
      this.originToggleClassSupp = 'Show'
    } else {
      this.originToggleSupp = false
      this.originToggleClassSupp = 'Hide'
    }
    try {
      if (_shipperInfo.Name) {
        this.SupInfoFormOrigin.controls['name'].setValue(_shipperInfo.Name);
      }
      if (_shipperInfo.Email) {
        this.SupInfoFormOrigin.controls['email'].setValue(_shipperInfo.Email);
      }
      if (_shipperInfo.Address && _shipperInfo.Address !== 'N/A') {
        this.SupInfoFormOrigin.controls['address'].setValue(_shipperInfo.Address);
      } else {
        this.SupInfoFormOrigin.controls['address'].setValue(null);
      }
      if (_shipperInfo.PhoneNumber) {
        this.SupInfoFormOrigin.controls['contact'].setValue(_shipperInfo.PhoneNumber);
      }
      if (_shipperInfo.PhoneCountryID) {
        let object = this.countryList.find(obj => obj.id === _shipperInfo.PhoneCountryID);
        this.supFlagImgOrg = object.code;
        let description = object.desc;
        this.supOrgphoneCode = description[0].CountryPhoneCode;
        this.supOrgCountryId = object.id
        this.selCountryOrigin = object
      }
    } catch {
    }
  }
  setSupDestInfo(_shipperInfo: { Name: any; Email: any; Address: any; PhoneNumber: any; PhoneCountryID: any; }) {
    this.editSupDestToggler = false;
    if (this.bookingDetails.JsonShippingDestInfo && _shipperInfo.Name) {
      this.destinToggleSupp = true
      this.destinToggleClassSupp = 'Show'
    } else {
      this.destinToggleSupp = false
      this.destinToggleClassSupp = 'Hide'
    }
    try {
      if (_shipperInfo.Name) {
        this.SupInfoFormDest.controls['name'].setValue(_shipperInfo.Name);
      }
      if (_shipperInfo.Email) {
        this.SupInfoFormDest.controls['email'].setValue(_shipperInfo.Email);
      }
      if (_shipperInfo.Address && _shipperInfo.Address !== 'N/A') {
        this.SupInfoFormDest.controls['address'].setValue(_shipperInfo.Address);
      } else {
        this.SupInfoFormDest.controls['address'].setValue(null);
      }
      if (_shipperInfo.PhoneNumber) {
        this.SupInfoFormDest.controls['contact'].setValue(_shipperInfo.PhoneNumber);
      }
      if (_shipperInfo.PhoneCountryID) {
        let object = this.countryList.find(obj => obj.id === _shipperInfo.PhoneCountryID);
        this.supFlagImgDest = object.code;
        let description = object.desc;
        this.supDestphoneCode = description[0].CountryPhoneCode;
        this.supDestCountryId = object.id
        this.selCountryDest = object
      }
    } catch {

    }
  }

  updateSupInfoOrig() {

    if (this.SupInfoFormOrigin.invalid) {
      return;
    }

    // const userInfo = JSON.parse(localStorage.getItem('loginUser'))
    let obj = {
      BookingNature: (this.bookingDetails.ShippingModeCode === 'WAREHOUSE') ? 'WH' : 'SEA',
      BookingID: this.bookingDetails.BookingID,
      LoginUserID: this.userProfile.UserID,
      PortNature: 'Origin',
      ContactInfoFor: 'Supplier',
      BookingSupDistInfo: {
        BookingID: this.bookingDetails.BookingID,
        Name: this.SupInfoFormOrigin.value.name,
        Address: this.SupInfoFormOrigin.value.address,
        Email: this.SupInfoFormOrigin.value.email,
        PhoneNumber: this.SupInfoFormOrigin.value.contact,
        PhoneCountryCode: (this.agentOrgphoneCode && this.SupInfoFormOrigin.value.contact) ? this.agentOrgphoneCode : null,
        PhoneCountryID: (this.SupInfoFormOrigin.value.contact) ? this.agentOrgCountryId : null,
        InfoFor: 'Supplier',
      }
    }
    this._viewBookingService.updateSupInfo(obj).subscribe((res: any) => {
      if (res.returnStatus === 'Success') {
        this._toast.success('Supplier information is updated', '');
        this.editSupOrgToggler = true;
        if (res.returnText && isJSON(res.returnText)) {
          this.bookingDetails.JsonShippingOrgInfo = JSON.parse(res.returnText);
        }
      }
    })

  }

  updateSupInfoDest() {

    if (this.SupInfoFormDest.invalid) {
      return;
    }

    // const userInfo = JSON.parse(localStorage.getItem('loginUser'))
    let obj = {
      BookingNature: (this.bookingDetails.ShippingModeCode === 'WAREHOUSE') ? 'WH' : 'SEA',
      BookingID: this.bookingDetails.BookingID,
      LoginUserID: this.userProfile.UserID,
      PortNature: 'Destination',
      ContactInfoFor: 'Supplier',
      BookingSupDistInfo: {
        BookingID: this.bookingDetails.BookingID,
        Name: this.SupInfoFormDest.value.name,
        Address: this.SupInfoFormDest.value.address,
        Email: this.SupInfoFormDest.value.email,
        PhoneNumber: this.SupInfoFormDest.value.contact,
        PhoneCountryCode: (this.agentDestphoneCode && this.SupInfoFormDest.value.contact) ? this.agentDestphoneCode : null,
        PhoneCountryID: (this.SupInfoFormDest.value.contact) ? this.agentDestCountryId : null,
        InfoFor: 'Supplier'
      }
    }
    this._viewBookingService.updateSupInfo(obj).subscribe((res: any) => {
      if (res.returnStatus === 'Success') {
        this._toast.success('Supplier information is updated', '');
        this.editSupDestToggler = true;
        if (res.returnText && isJSON(res.returnText)) {
          this.bookingDetails.JsonShippingDestInfo = JSON.parse(res.returnText);
        }
      }
    })
  }

  updateAgentInfoOrig() {
    loading(true)
    let obj = null
    try {
      obj = {
        BookingNature: (this.bookingDetails.ShippingModeCode === 'WAREHOUSE') ? 'WH' : 'SEA',
        BookingID: this.bookingDetails.BookingID,
        LoginUserID: this.userProfile.UserID,
        PortNature: 'Origin',
        ContactInfoFor: 'Agent',
        BookingSupDistInfo: {
          BookingID: this.bookingDetails.BookingID,
          Name: this.AgentInfoFormOrigin.value.name,
          Address: this.AgentInfoFormOrigin.value.address,
          Email: this.AgentInfoFormOrigin.value.email,
          PhoneNumber: this.AgentInfoFormOrigin.value.contact,
          PhoneCountryCode: (this.agentOrgphoneCode && this.AgentInfoFormOrigin.value.contact) ? this.agentOrgphoneCode : null,
          PhoneCountryID: (this.AgentInfoFormOrigin.value.contact) ? this.agentOrgCountryId : null,
          InfoFor: 'Agent',
          AddressID: -1,
          UserID: this.loginUser.UserID,
          CompanyID: null,
          ProviderID: this.bookingDetails.ProviderID,
          LoginUserID: this.loginUser.UserID,
        }
      }
    } catch (error) {
      loading(false)
    }
    this._viewBookingService.updateAgentInfo(obj).subscribe((res: any) => {
      loading(false)
      if (res.returnStatus === 'Success') {
        this._toast.success('Agent information is updated', '');
        this.editAgentorgToggler = true;
        if (res.returnText && isJSON(res.returnText)) {
          this.bookingDetails.JsonAgentOrgInfo = JSON.parse(res.returnText);
        }
      }
    }, error => {
      loading(false)
    })


  }

  updateAgentInfoDest() {
    loading(true)
    let obj = null
    try {
      obj = {
        BookingNature: (this.bookingDetails.ShippingModeCode === 'WAREHOUSE') ? 'WH' : 'SEA',
        BookingID: this.bookingDetails.BookingID,
        LoginUserID: this.userProfile.UserID,
        PortNature: 'Destination',
        ContactInfoFor: 'Agent',
        BookingSupDistInfo: {
          BookingID: this.bookingDetails.BookingID,
          Name: this.AgentInfoFormDest.value.name,
          Address: this.AgentInfoFormDest.value.address,
          Email: this.AgentInfoFormDest.value.email,
          PhoneNumber: this.AgentInfoFormDest.value.contact,
          PhoneCountryCode: (this.agentDestphoneCode && this.AgentInfoFormDest.value.contact) ? this.agentDestphoneCode : null,
          PhoneCountryID: (this.AgentInfoFormDest.value.contact) ? this.agentDestCountryId : null,
          InfoFor: 'Agent',
          AddressID: -1,
          UserID: this.loginUser.UserID,
          CompanyID: null,
          ProviderID: this.bookingDetails.ProviderID,
          LoginUserID: this.loginUser.UserID,
        }
      }
    } catch (error) {
      loading(false)
    }
    this._viewBookingService.updateAgentInfo(obj).subscribe((res: any) => {
      loading(false)
      if (res.returnStatus === 'Success') {
        this._toast.success('Agent information is updated', '');
        this.editAgentDestToggler = true;
        if (res.returnText && isJSON(res.returnText)) {
          this.bookingDetails.JsonAgentDestInfo = JSON.parse(res.returnText);
        }
      }
    }, error => {
      loading(false)
    })


  }

  updateBlInfo() {
    const { ShipDtlForm } = this
    const toSend = {
      bookingID: this.bookingDetails.BookingID,
      shippingModeCode: this.bookingDetails.ShippingModeCode,
      comInvNo: ShipDtlForm.controls['invoice'].value,
      cusClearNo: ShipDtlForm.controls['custom'].value,
      gDeclareNo: ShipDtlForm.controls['goods_dec'].value,
      blNo: this.bookingDetails.ShippingModeCode === 'AIR' ? ShipDtlForm.controls['awb_no'].value : this.bookingDetails.BLNo,
      expFormNo: ShipDtlForm.controls['export_no'].value,
      blDate: this.bookingDetails.BLDate,
    }
    loading(true)
    this._viewBookingService.updateBlInfo(toSend, getLoggedUserData().UserID).subscribe((res: JsonResponse) => {
      loading(false)
      if (res.returnId > 0) {
        this._toast.success('Updated Successfully', 'Success')
        this.editShipDetails = false
        this.bookingDetails.ComInvNo = ShipDtlForm.controls['invoice'].value
        this.bookingDetails.CusClearNo = ShipDtlForm.controls['custom'].value
        this.bookingDetails.GDeclareNo = ShipDtlForm.controls['goods_dec'].value
        this.bookingDetails.BLNo = this.bookingDetails.ShippingModeCode === 'AIR' ? ShipDtlForm.controls['awb_no'].value : this.bookingDetails.BLNo
        this.setAwbNumber(this.bookingDetails.BLNo)
        this.bookingDetails.ExpFormNo = ShipDtlForm.controls['export_no'].value
        if (this.bookingDetails.ShippingModeCode === 'AIR') {
          this.bookingDetails.BLTrackingURL = res.returnObject.BLTrackingURL
        }
      } else {
        try {
          let str = ''
          if (res.returnObject && res.returnObject.length && res.returnObject.length > 0) {
            res.returnObject.forEach(element => {
              str += res.returnObject + '\n';
            });
            this._toast.error(str, 'Failed')
          } else {
            this._toast.error(res.returnText, 'Failed')
          }
        } catch {
          this._toast.error(res.returnText, 'Failed')
        }
      }
    }, error => loading(false))
  }

  selectPhoneCode(country, type) {
    if (type === 'origin') {
      this.agentFlagImgOrg = country.code;
      let description = country.desc;
      this.agentOrgphoneCode = description[0].CountryPhoneCode;
      this.agentOrgCountryId = country.id
      this.selCountryOrigin = country
    }
    else if (type === 'destination') {
      this.agentFlagImgDest = country.code;
      let description = country.desc;
      this.agentDestphoneCode = description[0].CountryPhoneCode;
      this.agentDestCountryId = country.id
      this.selCountryDest = country
    }
  }

  mapInit(map) {
    const bounds: LatLngBounds = new google.maps.LatLngBounds();
    for (const mm of this.mapOrgiToDest) {
      bounds.extend(new google.maps.LatLng(mm.lat, mm.lng));
    }
    if (this.searchCriteria.searchMode === 'warehouse-lcl') {
      map.setCenter(bounds.getCenter());
      map.setZoom(14);
    } else {
      map.fitBounds(bounds);
    }
  }

  ngOnDestroy() {
    // this.paramSubscriber.unsubscribe();
  }

  selCountryOrigin
  selCountryDest

  airlines: Array<any> = [];
  docTypeList: IDocType[] = []

  setDefailtRights() {
    try {
      if (this.bookingDetails.ShippingStatus.toLowerCase() === 'completed' || this.isExpired) {
        this.objectRights = this.isExpired ? disabledRights : defaultReadRights
        // this.setRights()
        this.SupInfoFormOrigin.disable()
        this.SupInfoFormDest.disable()
        this.AgentInfoFormOrigin.disable()
        this.AgentInfoFormDest.disable()
      } else {
        this.SupInfoFormOrigin.enable()
        this.SupInfoFormDest.enable()
        this.AgentInfoFormOrigin.enable()
        this.AgentInfoFormDest.enable()
      }
    } catch { }
  }

  hasRights(rightsCode, rigthsCode: string): boolean {
    try {
      const { JsonObjectRights } = this.objectRights
      const _rbiRights = JsonObjectRights.filter(_right => _right.ObjCode === rightsCode)
      if (_rbiRights && _rbiRights.length > 0 && _rbiRights[0].ObjRights && _rbiRights[0].ObjRights === rigthsCode) {
        return true
      } else {
        return false
      }
    } catch {
      return false
    }
  }

  getRelatedViewUrl($booking: IRelatedBookings) {
    let toReturn = ''
    switch ($booking.shippingModeCode) {
      case 'SEA':
        toReturn = this.hasRights('RBSF', 'R') ? $booking.bookingURL[0].bookingURL : (this.isIssuedForBuyer) ? $booking.bookingURL[1].bookingURL : null
        break;
      case 'WAREHOUSE':
        toReturn = this.hasRights('RBSW', 'R') ? $booking.bookingURL[0].bookingURL : (this.isIssuedForBuyer) ? $booking.bookingURL[1].bookingURL : null
        break;
      case 'ASSAYER':
        toReturn = this.hasRights('RBSA', 'R') ? $booking.bookingURL[0].bookingURL : (this.isIssuedForBuyer) ? $booking.bookingURL[1].bookingURL : null
        break;
      case 'INSURANCE':
        if (this.bookingDetails.ShippingModeCode === 'SEA' || this.bookingDetails.InsurancePolicyMode === 'SEA') {
          toReturn = this.hasRights('RBBIF', 'R') ? $booking.bookingURL[0].bookingURL : (this.isIssuedForBuyer) ? $booking.bookingURL[1].bookingURL : null
        } else if ((this.bookingDetails.ShippingModeCode === 'WAREHOUSE' || this.bookingDetails.ShippingModeCode === 'ASSAYER') || this.bookingDetails.InsurancePolicyMode === 'WAREHOUSE') {
          toReturn = this.hasRights('RBBIW', 'R') ? $booking.bookingURL[0].bookingURL : (this.isIssuedForBuyer) ? $booking.bookingURL[1].bookingURL : null
        } else if (this.bookingDetails.ShippingModeCode === 'GROUND' || this.bookingDetails.InsurancePolicyMode === 'GROUND') {
          toReturn = this.hasRights('RBBIG', 'R') ? $booking.bookingURL[0].bookingURL : (this.isIssuedForBuyer) ? $booking.bookingURL[1].bookingURL : null
        }
        break;
      case 'AIR':
        toReturn = this.hasRights('RBSAIR', 'R') ? $booking.bookingURL[0].bookingURL : (this.isIssuedForBuyer) ? $booking.bookingURL[1].bookingURL : null
        break;
      default:
        toReturn = $booking.bookingURL[0].bookingURL
        break;
    }
    return toReturn
  }

  getBookingDetail(bookingKey) {
    loading(true);
    this.isExpired = false
    this._viewBookingService.getBookingDetails(bookingKey).subscribe(async (res: any) => {
      loading(false);
      if (res.returnId > 0) {
        this.bookingDetails = JSON.parse(res.returnText);
        if (!this.isGuestLogin) this.setExpiry()

        try {
          if (this.bookingDetails.AtdLcl) {
            const _atdDate = new Date(this.bookingDetails.AtdLcl)
            this.atdDate = { year: _atdDate.getFullYear(), month: _atdDate.getMonth() + 1, day: _atdDate.getDate() }
          } else { this.atdDate = null }
          if (this.bookingDetails.AtaLcl) {
            const _ataDate = new Date(this.bookingDetails.AtaLcl)
            this.ataDate = { year: _ataDate.getFullYear(), month: _ataDate.getMonth() + 1, day: _ataDate.getDate() }
          } else { this.ataDate = null }
        } catch {
          this.atdDate = null
          this.ataDate = null
        }
        try { this.searchCriteria = JSON.parse(this.bookingDetails.JsonSearchCriteria) } catch { }
        this.isExpired = this.bookingDetails.BookingStatusBL === 'EXPIRED' ? true : false
        this.getShippingInstructions(bookingKey)
        this.getLclContainerNumber(bookingKey)
        this.getChatList()
        try { this.setHmCommission() } catch { }

        if (this.bookingDetails.BookingSourceVia && this.bookingDetails.BookingSourceVia.includes('ENTERPRISE'))
          this.isEnterprise = true

        try {
          this.ProviderEmails = this.bookingDetails.ProviderEmail.split(';');
          this.ProviderPhones = this.bookingDetails.ProviderPhone.split(';');
        } catch { }

        this._sharedService.dashboardDetail.subscribe((state: any) => {
          if (state) {
            try {
              if (state.FeatureToggleJson) {
                const { FeatureToggleJson } = state
                const _controls = JSON.parse(FeatureToggleJson)
                if (_controls.HMCommissionDisplayFor) {
                  if (_controls.HMCommissionDisplayFor === 'ALL')
                    this.showCommission = true
                  else if (_controls.HMCommissionDisplayFor === 'ELM' && this.isEnterprise)
                    this.showCommission = true
                  else
                    this.showCommission = false
                }
              }
            } catch { }
          }
        })
        this.editAirInfo = false
        if (this.bookingDetails.ShippingModeCode === 'AIR')
          this.setAirlineData()
        this.setAwbNumber(this.bookingDetails.BLNo)
        this.setDefailtRights()
        try {
          if (this.isGuestLogin && this.objectRights && this.objectRights) {
            if (this.hasRights('RBI', 'R')) {
              this._viewBookingService.getRelatedBookingSharedURL(bookingKey).subscribe((res: JsonResponse) => {
                if (res.returnId > 0) {
                  const _relatedBookings: IRelatedBookings[] = res.returnObject
                  this.relatedBookings = _relatedBookings
                    .filter(_booking => _booking.bookingID !== this.bookingDetails.BookingID)
                    .filter(_booking =>
                      (_booking.shippingModeCode === 'SEA' && ((this.isIssuedForBuyer) ? this.hasRights('RBBF', 'R') : this.hasRights('RBSF', 'R'))) ||
                      (_booking.shippingModeCode === 'WAREHOUSE' && ((this.isIssuedForBuyer) ? this.hasRights('RBBW', 'R') : this.hasRights('RBSW', 'R'))) ||
                      (_booking.shippingModeCode === 'ASSAYER' && ((this.isIssuedForBuyer) ? this.hasRights('RBBA', 'R') : this.hasRights('RBSA', 'R'))) ||
                      (_booking.shippingModeCode === 'INSURANCE' && ((this.isIssuedForBuyer) ? this.hasRights('RBBIF', 'R') : this.hasRights('RBSIF', 'R'))) ||
                      (_booking.shippingModeCode === 'INSURANCE' && ((this.isIssuedForBuyer) ? this.hasRights('RBBIW', 'R') : this.hasRights('RBSIW', 'R'))) ||
                      (_booking.shippingModeCode === 'INSURANCE' && ((this.isIssuedForBuyer) ? this.hasRights('RBBIG', 'R') : this.hasRights('RBSIG', 'R'))) ||
                      (_booking.shippingModeCode === 'AIR' && ((this.isIssuedForBuyer) ? this.hasRights('RBBAIR', 'R') : this.hasRights('RBSAIR', 'R')))
                    ).map(_booking => ({
                      ..._booking,
                      viewBookingURL: this.getRelatedViewUrl(_booking)
                    }))
                } else {
                  this.relatedBookings = []
                }
              })
            }
          } else {
            this._viewBookingService.getRelatedBookingSharedURL(bookingKey).subscribe((res: JsonResponse) => {
              if (res.returnId > 0) {
                const _relatedBookings: IRelatedBookings[] = res.returnObject
                this.relatedBookings = _relatedBookings
                  .filter(_booking => _booking.bookingID !== this.bookingDetails.BookingID)
                  .map(_booking => ({
                    ..._booking,
                    viewBookingURL: _booking.bookingURL[0].bookingURL
                  }))
              } else {
                this.relatedBookings = []
              }
            })
          }
        } catch { }

        if (this.bookingDetails.BookingSourceVia && this.bookingDetails.BookingSourceVia.length > 2) {
          SharedService.SET_VB_SOURCE_VIA(this.bookingDetails.BookingSourceVia)
          try {
            this.isPmex = this.bookingDetails.BookingSourceVia.toLowerCase().includes('pmex')
          } catch { }
        }
        const _manDocsToGet: number[] = []
        this.manDocsString = []
        if (!this.isExpired && this.bookingDetails.BookingStatus.toLowerCase() === 'in-review') {
          const _customerID = this.bookingDetails.CustomerID ? this.bookingDetails.CustomerID : 0
          try {
            const manDocRes = await this._viewBookingService.getBookingMandatoryDocuments(
              this.bookingDetails.BookingID, this.bookingDetails.ProviderID, _customerID
            ).toPromise() as JsonResponse
            const _manDoc = manDocRes.returnObject.filter(_doc => !_doc.isExist)
            if (manDocRes.returnId > 0 && isArrayValid(_manDoc, 0)) {
              this.hasMandDocs = true
              for (let index = 0; index < _manDoc.length; index++) {
                const { documentTypeName, documentTypeID } = _manDoc[index]
                _manDocsToGet.push(documentTypeID)
                this.manDocsString.push(documentTypeName)
              }
            } else {
              this.manDocsString = []
            }
          } catch { }
        }
        this.additionalDocumentsCustomers = []
        await this.getDocumentList()
        const defaultDocs = this.docTypeList
          .filter(_doc => _doc.isDefaultDocument).map(_doc => _doc.documentTypeID)
        setTimeout(async () => {
          if (isArrayValid(defaultDocs, 0)) {
            for (let index = 0; index < defaultDocs.length; index++) {
              const _doc2Get = defaultDocs[index];
              try {
                if (this.additionalDocumentsCustomers.filter(_doc => _doc.DocumentTypeID === _doc2Get).length === 0)
                  await this.getDocumentDtl(_doc2Get)
              } catch { }
            }
          }
          this.getUploadedDocuments()
        }, 0);

        if (this.bookingDetails.ShippingModeCode === 'WAREHOUSE') {
          this._viewBookingService.getAllUsers(this.bookingDetails.UserID).subscribe((res: JsonResponse) => {
            if (res.returnObject && res.returnObject.length > 0) {
              this.whUsers = res.returnObject
              this.whUsers.forEach(element => {
                if (element.userImage)
                  element.userImage = baseExternalAssets + element.userImage;
              });
              this.whUsers = this.whUsers.sort(compareValues('firstName', 'asc'))
              try {
                if (this.bookingDetails.WHContactInfo) {
                  const { WHContactInfo } = this.bookingDetails
                  const { whUsers } = this
                  this.selectedWHUser = whUsers.filter(_user => _user.userID === WHContactInfo.UserID)[0]
                  if (this.selectedWHUser) {
                    this.warehouseUserInfo.controls['name'].patchValue(this.selectedWHUser)
                    let selectedCountry = this.countryList.find(obj => obj.code === WHContactInfo.CountryCode)
                    this.selectPhoneCodeWH(selectedCountry)
                  } else {
                    setTimeout(() => { try { this._wh_name.nativeElement.value = '' } catch { } }, 10)
                    setTimeout(() => { try { this._wh_email.nativeElement.value = '' } catch { } }, 10)
                    setTimeout(() => { try { this._wh_phone.nativeElement.value = '' } catch { } }, 10)
                    this.warehouseUserInfo.reset()
                  }
                  this.isWhEdit = false
                } else {
                  this.isWhEdit = true
                  let selectedCountry = this.countryList.find(obj => obj.code.toLowerCase() === 'us')
                  this.selectPhoneCodeWH(selectedCountry)
                  setTimeout(() => { try { this._wh_name.nativeElement.value = '' } catch { } }, 10)
                  setTimeout(() => { try { this._wh_email.nativeElement.value = '' } catch { } }, 10)
                  setTimeout(() => { try { this._wh_phone.nativeElement.value = '' } catch { } }, 10)
                  this.warehouseUserInfo.reset()
                }
              } catch {
                this.isWhEdit = true
              }
            }
          })
        }

        this.setAssayerData()
        if (this.isGuestLogin) {
          try {
            this._sharedService.providerLogo.next(baseExternalAssets + JSON.parse(this.bookingDetails.ProviderImage)[0].DocumentFile)
          } catch { }
        } else {
          this._sharedService.providerLogo.next(null)
        }
        this.additionalDocumentsCustomers = []
        try { this.setShipDtlInfo() } catch (error) { }
        this.setAdjustCheck()
        try {
          if (this.bookingDetails.CarrierCode.toLowerCase() === 'multiple')
            this.airlines = await this.getMultiAirlines(bookingKey)
        } catch (error) { }
        this.searchCriteria = JSON.parse(this.bookingDetails.JsonSearchCriteria)
        this.getCarrierSchedule('summary')

        if (this.bookingDetails.ShippingModeCode !== 'ASSAYER') {

          if (this.bookingDetails.BookingContainerDetail) {
            this.bookingDetails.BookingContainerDetail.forEach(container => {
              container.parsedJsonContainerInfo = JSON.parse(container.JsonContainerInfo)
              try {
                const _data: any[] = container.parsedJsonContainerInfo.filter(_container => _container.ContainerNo)
                if (_data.length > 0) {
                  this.hasContainerNumber = true
                  this.enteredContainerNumber = _data[0].ContainerNo
                }
              } catch { }
              if (container.IsTrackingRequired) {
                this.isTrackingOnBooking = true;
                this.isMonitoringOnBooking = true;
              }
              if (container.IsQualityMonitoringRequired) {
                this.isMonitoringOnBooking = true;
                this.isTrackingOnBooking = true;
              }
            })
          }
          if (this.bookingDetails.VesselCode)
            this.showScheduler = false

          this.lclViewContainers = (this.searchCriteria.searchMode === 'sea-lcl') ? this.prepareLclContainers() : []
          this.isTruck = this.bookingDetails.ShippingModeCode.toLowerCase() === 'truck'
          if (this.searchCriteria.searchMode === 'air-lcl' && this.searchCriteria.cargoLoadType === 'by_unit')
            this.getWarehousingUnits()
        }
        if (this.bookingDetails.BookingUserInfo && this.bookingDetails.BookingUserInfo.UserImage && this.bookingDetails.BookingUserInfo.UserImage !== '[]' && isJSON(this.bookingDetails.BookingUserInfo.UserImage))
          this.bookingDetails.BookingUserInfo.UserImage = JSON.parse(this.bookingDetails.BookingUserInfo.UserImage)[0].DocumentFile;
        try {
          const { BookingDocumentDetail } = this.bookingDetails
          if (BookingDocumentDetail && BookingDocumentDetail.length > 0) {
            const adDocCustomers: any = BookingDocumentDetail.filter(doc => (doc.DocumentSubProcess === 'COMMON' || doc.DocumentNature === 'ADD_DOC') && doc.DocumentNature !== 'HM_BOOKING_INVOICE' && doc.DocumentID > 0) as any
            adDocCustomers.forEach(doc => {
              doc.ShowUpload = false;
              doc.DateModel = ''
            })
            this.additionalDocumentsCustomers = adDocCustomers
            try { this.resetAccordian() } catch { }
          }
        } catch (error) {
          console.warn(error)
        }
        if (this.bookingDetails.ShippingModeCode !== 'WAREHOUSE' && this.bookingDetails.ShippingModeCode !== 'ASSAYER' && this.bookingDetails.InsurancePolicyMode !== 'WAREHOUSE') {
          if (this.bookingDetails.JsonShippingDestInfo && isJSON(this.bookingDetails.JsonShippingDestInfo)) {
            this.bookingDetails.JsonShippingDestInfo = JSON.parse(this.bookingDetails.JsonShippingDestInfo);
            this.editSupDestToggler = true;
            this.destinToggleSupp = true
            this.destinToggleClassSupp = 'Show'
          } else if (!this.bookingDetails.JsonShippingDestInfo) {
            let object = this.countryList.find(obj => obj.title === this.bookingDetails.PodCountry);
            this.supFlagImgDest = object.code;
            let description = object.desc;
            this.supDestphoneCode = description[0].CountryPhoneCode;
            this.supDestCountryId = object.id
            this.selCountryDest = object
          }
          if (this.bookingDetails.JsonShippingOrgInfo && isJSON(this.bookingDetails.JsonShippingOrgInfo)) {
            this.bookingDetails.JsonShippingOrgInfo = JSON.parse(this.bookingDetails.JsonShippingOrgInfo)
            this.editSupOrgToggler = true;
            this.originToggleSupp = true
            this.originToggleClassSupp = 'Show'
          } else if (!this.bookingDetails.JsonShippingOrgInfo) {
            let object = this.countryList.find(obj => obj.title === this.bookingDetails.PolCountry);
            this.supFlagImgOrg = object.code;
            let description = object.desc;
            this.supOrgphoneCode = description[0].CountryPhoneCode;
            this.supOrgCountryId = object.id
            this.selCountryOrigin = object
          }
          if (this.bookingDetails.JsonAgentOrgInfo && isJSON(this.bookingDetails.JsonAgentOrgInfo)) {
            this.bookingDetails.JsonAgentOrgInfo = JSON.parse(this.bookingDetails.JsonAgentOrgInfo);
            this.editAgentorgToggler = true;
            this.originToggleAgent = true
            this.originToggleClassAgent = 'Show'
          } else if (!this.bookingDetails.JsonAgentOrgInfo) {
            const _country = this.countryList.find(obj => obj.title === this.bookingDetails.PolCountry);
            this.agentFlagImgOrg = _country.code;
            const description = _country.desc;
            this.agentOrgphoneCode = description[0].CountryPhoneCode;
            this.agentOrgCountryId = _country.id
            this.selCountryOrigin = _country
          }

          if (this.bookingDetails.JsonAgentDestInfo && isJSON(this.bookingDetails.JsonAgentDestInfo)) {
            this.bookingDetails.JsonAgentDestInfo = JSON.parse(this.bookingDetails.JsonAgentDestInfo);
            this.editAgentDestToggler = true;
            this.destinToggleAgent = true
            this.destinToggleClassAgent = 'Show'
          } else if (!this.bookingDetails.JsonAgentDestInfo) {
            const _country = this.countryList.find(obj => obj.title === this.bookingDetails.PodCountry);
            this.agentFlagImgDest = _country.code;
            const description = _country.desc;
            this.agentDestphoneCode = description[0].CountryPhoneCode;
            this.agentDestCountryId = _country.id
            this.selCountryDest = _country
          }

          try {
            this.bookingDetails.origin = this.bookingDetails.PolCountryCode.toLowerCase()
            this.bookingDetails.destination = this.bookingDetails.PodCountryCode.toLowerCase()
          } catch { }
          this.bookingDocs();
          this.mapOrgiToDest.push(
            { lat: Number(this.bookingDetails.PolLatitude), lng: Number(this.bookingDetails.PolLongitude) },
            { lat: Number(this.bookingDetails.PodLatitude), lng: Number(this.bookingDetails.PodLongitude) });
        } else if (this.bookingDetails.ShippingModeCode === 'WAREHOUSE' || this.bookingDetails.InsurancePolicyMode === 'WAREHOUSE') {
          const obj = {
            lat: Number(this.bookingDetails.WHLatitude),
            lng: Number(this.bookingDetails.WHLongitude)
          }
          this.mapOrgiToDest.push(obj);
          if (this.bookingDetails.WHCityName && this.bookingDetails.WHCountryName) {
            this.wareHouse.Location = this.bookingDetails.WHCityName + ', ' + this.bookingDetails.WHCountryName;
          }
          if (this.bookingDetails.WHMedia && this.bookingDetails.WHMedia !== '[]' && isJSON(this.bookingDetails.WHMedia)) {
            this.bookingDetails.WHMedia = JSON.parse(this.bookingDetails.WHMedia);
            const albumArr = [];
            this.bookingDetails.WHMedia.forEach((elem) => {
              const album = {
                src: baseExternalAssets + elem.DocumentFile,
                thumb: baseExternalAssets + elem.DocumentFile,
                DocumentUploadedFileType: elem.DocumentUploadedFileType
              };
              albumArr.push(album);
            })
            this.wareHouse.parsedGallery = albumArr;
          }
          if (this.bookingDetails.ActualScheduleDetail && isJSON(this.bookingDetails.ActualScheduleDetail)) {
            const facilities = JSON.parse(this.bookingDetails.ActualScheduleDetail).WHFacilities;
            if (facilities && isJSON(facilities))
              this.wareHouse.FacilitiesProviding = JSON.parse(facilities);
          }
          if (this.bookingDetails.BookingJsonDetail && this.bookingDetails.BookingJsonDetail !== '[]' && isJSON(this.bookingDetails.BookingJsonDetail)) {
            const wareHouseDetail = JSON.parse(this.bookingDetails.BookingJsonDetail)
            this.wareHouse.WHName = wareHouseDetail.WHName;
            this.wareHouse.WHDesc = wareHouseDetail.WHDesc;
            this.wareHouse.TotalCoveredArea = wareHouseDetail.AvailableSQFT;
          }
          if (this.bookingDetails.JsonSearchCriteria) {
            const searchCriteria = JSON.parse(this.bookingDetails.JsonSearchCriteria)
            this.wareHouse.storageType = searchCriteria.storageType;
            if (this.wareHouse.storageType !== 'full') {
              this.wareHouse.StoreFrom = searchCriteria.StoreFrom;
              this.wareHouse.StoreUntill = searchCriteria.StoreUntill;
              if (searchCriteria.searchBy === 'by_area') {
                this.wareHouse.TotalCoveredAreaUnit = 'sqft';
                this.wareHouse.bookedArea = getWhSpace(searchCriteria);
              } else if (searchCriteria.searchBy === 'by_pallet') {
                this.wareHouse.TotalCoveredAreaUnit = 'PLT';
                this.wareHouse.bookedArea = searchCriteria.PLT
              } else {
                this.wareHouse.TotalCoveredAreaUnit = 'cbm';
                this.wareHouse.bookedArea = searchCriteria.CBM
              }
            } else {
              this.wareHouse.minimumLeaseTerm = searchCriteria.minimumLeaseTermString;
              this.wareHouse.spaceReqWarehouse = searchCriteria.spaceReqString
              this.wareHouse.TotalCoveredAreaUnit = 'sqft';
            }
          }
        }
      } else {
        this._toast.error('Unable to find this booking. Please check the link and try again', 'Failed to Fetch Data')
      }
    }, () => { loading(false) })
  }

  setExpiry() {
    try {
      const { BookingExpiryHours, IsBookingExpiryApplied } = getLoggedUserData().ProviderConfig
      if (this.bookingDetails.BookingStatusBL.toUpperCase() === 'IN-REVIEW' &&
        IsBookingExpiryApplied && this.bookingDetails.IsBookingExpiryApplied_Customer) {
        this.showTimer = true
        const _bookDate = new Date(this.bookingDetails.HashMoveBookingDate + 'Z')

        const _48HDays = ['thursday', 'friday', 'saturday']
        const _24HDays = ['sunday']
        const _bookStrDay = moment(_bookDate).format('dddd').toLowerCase()
        const _addedHours = _48HDays.includes(_bookStrDay) ? 48 : _24HDays.includes(_bookStrDay) ? 24 : 0

        const _expHour = BookingExpiryHours + _addedHours;
        this.expiryDate = moment(_bookDate).add(_expHour, 'hours').format()
        this.bookDate = moment(_bookDate).format()
        // const distance = new Date(this.expiryDate).getTime() - new Date().getTime()
        // const second = 1000, minute = second * 60, hour = minute * 60
      }
    } catch (error) {
      console.log(error)
    }
  }

  getUploadedDocuments() {
    if (!this.bookingDetails.RequestResponseID || this.bookingDetails.RequestResponseID <= 0) {
      this._dashboardService.getOtherDocument('RATE_REQUEST', this.bookingDetails.RateRequestID ? this.bookingDetails.RateRequestID : 0).subscribe((res: JsonResponse) => {
        if (res.returnId > 0 && res.returnObject && typeof res.returnObject === 'object') {
          const { additionalDocumentsCustomers } = this
          this.additionalDocumentsCustomers = additionalDocumentsCustomers.concat(res.returnObject)
        }
      })
    }
  }

  setAssayerData() {
    if (this.bookingDetails.ShippingModeCode === 'ASSAYER') {
      try {
        const { BookingCertificateDetail } = this.bookingDetails
        const { BookingInpectionDetail } = this.bookingDetails
        try {
          let _finnArr = []
          BookingCertificateDetail.forEach(_certificate => {
            const _parsedData = JSON.parse(_certificate.JsonServices)
            _parsedData.forEach(_cert => {
              _finnArr.push(_cert.Name)
            })
          })
          this.assayerCertificates = _finnArr
        } catch (error) {
          console.log(error)
        }

        this.commodityInspection = BookingInpectionDetail.filter(_inpsect => _inpsect.InspectionNature === 'COMMODITY_INSPECTION')[0]
        if (this.commodityInspection) {
          this.selectedCommodityStatus = this.commodityInspection.InspectionStatus
          this.comoodityComments = this.commodityInspection.InspectionComments
        } else {
          this.selectedCommodityStatus = 'PENDING'
          this.comoodityComments = null
        }
        this._viewBookingService.getBookingStatuses('ASSAYER').subscribe((res: JsonResponse) => {
          if (res.returnId > 0) {
            try {
              this.masterStatus = res.returnObject
              this.setCommodityInspStatus()
              this.setStuffingInspStatus()
            } catch (error) { }
          }
        })

        if (this.commodityInspection && this.commodityInspection.InspectionHistoryJson) {
          const { commodityInspection } = this
          const parsedData: any[] = JSON.parse(commodityInspection.InspectionHistoryJson)
          if (parsedData && parsedData.length > 0) {
            this.commodityInspection = {
              ...commodityInspection,
              parsednspectionHistory: parsedData
            }
            const { parsednspectionHistory } = this.commodityInspection
            this.cmdSchDate = parsednspectionHistory[parsednspectionHistory.length - 1].ModifiedDateTime
          } else {
            this.cmdSchDate = null
          }
        } else {
          this.cmdSchDate = null
        }

        this.containerInspection = BookingInpectionDetail.filter(_inpsect => _inpsect.InspectionNature === 'CONTAINER_STUFFING')[0]
        if (this.containerInspection) {
          this.selectedContainerStatus = this.containerInspection.InspectionStatus
          this.containerComments = this.containerInspection.InspectionComments
        } else {
          this.selectedContainerStatus = 'PENDING'
          this.containerComments = null
        }
        if (this.containerInspection && this.containerInspection.InspectionHistoryJson) {
          const { containerInspection } = this
          const parsedData: any[] = JSON.parse(containerInspection.InspectionHistoryJson)
          if (parsedData && parsedData.length > 0) {
            this.containerInspection = {
              ...containerInspection,
              parsednspectionHistory: parsedData
            }
            const { parsednspectionHistory } = this.containerInspection
            this.contSchDate = parsednspectionHistory[parsednspectionHistory.length - 1].ModifiedDateTime
          } else {
            this.contSchDate = null
          }
        } else {
          this.contSchDate = null
        }
      } catch { }

      this.hasCommodityChanged = false
      this.hasContainerChanged = false
    }
  }

  getShippingInstructions($bookingKey) {
    this._viewBookingService.getBookingAdditionalParameters($bookingKey, 'SHIPPING_INSTRUCTIONS').subscribe((res: JsonResponse) => {
      if (res.returnId > 0)
        this.setShipInstrucrtions(res.returnObject.parameterValue)
    })
  }

  setShipInstrucrtions($insData) {
    this.instructionsData = JSON.parse($insData)
    if (this.instructionsData) {
      const { Agent, Shipper } = this.instructionsData
      this.editorContentAgent = Agent ? Agent.replaceAll('\n', '<br>') : Agent
      this.editorContentConsignee = Shipper ? Shipper.replaceAll('\n', '<br>') : Shipper
    }
  }

  setCommodityInspStatus() {
    const { masterStatus } = this
    const _statuses = masterStatus.filter(_status =>
      _status.BusinessLogic !== 'ACCEPTED' &&
      _status.BusinessLogic !== 'PURPOSERESCHEDULE'
    ).map(_status => ({
      code: _status.BusinessLogic,
      name: _status.StatusName,
      disabled: _status.BusinessLogic === 'RESCHEDULED' ? true : false
    }))
    this.statuses = (this.selectedCommodityStatus !== 'RESCHEDULED') ?
      _statuses.filter(_status => _status.code !== 'RESCHEDULED') : _statuses
  }

  setStuffingInspStatus() {
    const { masterStatus } = this
    const _statuses = masterStatus.filter(_status =>
      _status.BusinessLogic !== 'ACCEPTED' &&
      _status.BusinessLogic !== 'PURPOSERESCHEDULE'
    ).map(_status => ({
      code: _status.BusinessLogic,
      name: _status.StatusName,
      disabled: _status.BusinessLogic === 'RESCHEDULED' ? true : false
    }))

    this.containerStuffingStatuses = (this.selectedContainerStatus !== 'RESCHEDULED') ?
      _statuses.filter(_status => _status.code !== 'RESCHEDULED') : _statuses
  }

  setAwbNumber(bLNo) {
    try {
      if (bLNo && bLNo.length > 0) {
        const bl_before = bLNo.substring(0, 3)
        const bl_after = bLNo.substring(3, bLNo.length)
        this.awbNumber = bl_before + '-' + bl_after
      } else {
        this.awbNumber = null
      }
    } catch {
      this.awbNumber = null
    }
  }

  bookingDocs() {
    if (this.bookingDetails.BookingDocumentDetail) {
      this.bookingDetails.BookingDocumentDetail.forEach((obj) => {
        if (obj.DocumentNature === 'CERTIFICATE')
          this.certOrigin = obj;
        else if (obj.DocumentNature === 'CUSTOM_DOC')
          this.ladingBill = obj;
        else if (obj.DocumentNature === 'CUSTOM_DOC')
          this.ladingBill = obj;

        else if (obj.DocumentNature === 'INVOICE') {
          if (obj.DocumentSubProcess === 'ORIGIN')
            this.invoiceDocOrigin = obj;
          else
            this.invoiceDocDestination = obj;
        }
        else if (obj.DocumentNature === 'PACKING_LIST') {
          if (obj.DocumentSubProcess === 'ORIGIN')
            this.packingListDocOrigin = obj;
          else
            this.packingListDocDestination = obj;
        }
      })
    }
  }
  viewInvoice() {
    const modalRef = this._modalService.open(BookingInvoiceComponent, {
      size: 'lg', centered: true, windowClass: 'mid-size-popup bar-class', backdrop: 'static', keyboard: false
    })
    modalRef.componentInstance.BookingInvoiceDet = this.bookingDetails
    modalRef.componentInstance.searchCriteria = this.searchCriteria
    modalRef.componentInstance.isSpecialRequest = (this.bookingDetails.IsSpecialRequest && this.bookingDetails.IsRateNotFoundBooking && !this.bookingDetails.RequestResponseID) ? this.bookingDetails.IsSpecialRequest : false

    modalRef.result.then((result) => {
      if (result)
        this.getBookingDetail(this.bookingId)
    })
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open'))
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden'
    }, 0)
  }

  async viewAdjustmentLogs() {
    try {
      const { returnId, returnObject, returnText }: JsonResponse = await this._viewBookingService.getAdjustmentLogs(this.bookingDetails.BookingID, this.bookingDetails.ShippingModeCode).toPromise() as any
      if (returnId > 0) {
        const modalRef = this._modalService.open(AdjustmentLogsComponent, {
          size: 'lg', centered: true, windowClass: 'mid-size-popup bar-class', backdrop: 'static', keyboard: false
        })
        modalRef.componentInstance.logs = returnObject
      } else {
        this._toast.error(returnText, 'Failed')
      }
    } catch { }
  }

  downloadDoc(object, event) {
    if (object && object.DocumentFileName && object.IsDownloadable) {
      if (object.DocumentFileName.startsWith('[{')) {
        let document = JSON.parse(object.DocumentFileName)
        window.open(baseExternalAssets + document[0].DocumentFile, '_blank');
      } else {
        window.open(baseExternalAssets + object.DocumentFileName, '_blank');
      }
    } else {
      event.preventDefault();
    }
  }

  reuploadDoc(data: any, acc?: any, $id?: any) {
    if (acc)
      try { acc.toggle($id) } catch { }


    const modalRef = this._modalService.open(ReUploadDocComponent, {
      size: 'lg', centered: true, windowClass: 'medium-modal', backdrop: 'static', keyboard: false
    })

    modalRef.componentInstance.documentObj = {
      docTypeID: data.DocumentTypeID,
      docID: data.DocumentID,
      userID: (this.isGuestLogin) ? null : this.userProfile.UserID,
      createdBy: (this.isGuestLogin) ? getDefaultHMUser('').loginUserID : this.userProfile.UserID,
      bookingData: this.bookingDetails
    }
    modalRef.result.then((result) => {
      if (result.resType === 'Success')
        data.DocumentLastStatus = result.status;
    })

    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open'))
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden'
    }, 0)
  }

  printDetail() {
    const doc = window as any;
    doc.print()
  }

  openCancel(type) {

    if (this.isPmex) {
      const _modalData: ConfirmDialogContent = {
        messageTitle: 'Cancel Booking',
        messageContent: `Are you sure you would like to cancel the booking?`,
        buttonTitle: 'Yes',
        data: null
      }

      const modalRef = this._modalService.open(ConfirmDialogGenComponent, {
        size: 'lg', centered: true, windowClass: 'medium-modal', backdrop: 'static', keyboard: false
      })

      modalRef.result.then((result: string) => {
        if (result)
          this.openDialogue(type)
      })

      modalRef.componentInstance.modalData = _modalData;
      setTimeout(() => {
        if (document.getElementsByTagName('body')[0].classList.contains('modal-open'))
          document.getElementsByTagName('html')[0].style.overflowY = 'hidden'
      }, 0)
    } else {
      this.openDialogue(type)
    }
  }

  async expireBookingCall() {
    const res = await this._viewBookingService.getBookingReasons().toPromise() as JsonResponse
    if (isArrayValid(res.returnObject, 0)) {
      const _reasons: BookingReason[] = res.returnObject
      const _expiredReason = _reasons.filter(_reas => _reas.BusinessLogic.toUpperCase() === 'AUTO_BOOKING_EXPIRED_NOT_CONFIRMED')
      if (isArrayValid(_expiredReason, 0)) {
        const _expiredPayload = {
          bookingID: this.bookingDetails.BookingID,
          bookingStatus: 'EXPIRED',
          bookingStatusRemarks: 'Booking has been auto expired',
          createdBy: 100,
          modifiedBy: 100,
          approverID: getLoggedUserData().ProviderID,
          approverType: 'PROVIDER',
          reasonID: _expiredReason[0].ReasonID,
          reasonText: _expiredReason[0].ReasonName,
          providerName: this.bookingDetails.ProviderName,
          emailTo: (this.bookingDetails && this.bookingDetails.BookingUserInfo && this.bookingDetails.BookingUserInfo.PrimaryEmail) ? this.bookingDetails.BookingUserInfo.PrimaryEmail : '',
          phoneTo: (this.bookingDetails && this.bookingDetails.BookingUserInfo && this.bookingDetails.BookingUserInfo.PrimaryPhone) ? this.bookingDetails.BookingUserInfo.PrimaryPhone : '',
          userName: this.bookingDetails.UserName,
          hashMoveBookingNum: this.bookingDetails.HashMoveBookingNum,
          userCountryPhoneCode: this.bookingDetails.UserCountryPhoneCodeID,
          bookingType: this.bookingDetails.ShippingModeCode,
          currentBookingStatus: this.bookingDetails.BookingStatusBL,
          companyID: this.bookingDetails.BookingUserInfo ? this.bookingDetails.BookingUserInfo.CompanyID : 0
        }
        loading(true)
        this._viewBookingService.cancelBooking(_expiredPayload).subscribe((res: JsonResponse) => {
          if (res.returnId > 0) {
            this.getBookingDetail(this.currentBookingId)
          } else {
            loading(false)
            this._toast.error(res.returnText, res.returnStatus)
          }
        }, () => loading(false))
      }
    }
  }

  onTimerExpired(state) {
    if (state.bookingID && this.bookingDetails.BookingID === state.bookingID)
      this.expireBookingCall()
  }

  openDialogue(type) {
    if (type !== 'cancel' && isArrayValid(this.manDocsString, 0)) {
      this._toast.warning(this.manDocsString.join(', '), 'Upload Required Documents')
      return
    }
    const modalRef = this._modalService.open(BookingStatusUpdationComponent, {
      size: 'lg', windowClass: 'medium-modal', centered: true, backdrop: 'static', keyboard: false
    })
    modalRef.result.then((res) => {
      console.log(res, type)
      if (res && res === 'notify') {
        this._toast.success('Successfully notified to customer', 'Success')
      } else if (res && res !== 'notify') {
        if (res.modalType === 'sub-status') {
          this.bookingDetails.LastActivity = res.activityStatus
        } else {
          this.bookingDetails.BookingStatus = res.bookingStatus
          this.bookingDetails.ShippingStatus = res.shippingStatus
          this.bookingDetails.StatusID = res.statusID
          if (type === 'updated') {
            this.bookingDetails.HashMoveBookingNum = res.hashMoveBookingNum
            this.showScheduler = false
            this.getBookingDetail(this.currentBookingId)
          } else if (type === 'cancel') {
            this.getBookingDetail(this.currentBookingId)
          }
        }
      } else if (type === 'cancel') {
        this.getBookingDetail(this.currentBookingId)
      }
      this.setDefailtRights()
    })
    modalRef.componentInstance.modalData = {
      type: type,
      bookingID: this.bookingDetails.BookingID,
      bookingStatus: this.bookingDetails.BookingStatus,
      loginID: (this.isGuestLogin) ? getDefaultHMUser('').loginUserID : this.userProfile.LoginID,
      providerID: this.bookingDetails.ProviderID,
      booking: this.bookingDetails,
      searchCarrierList: this.searchCarrierList ? this.searchCarrierList : []
    }
  }

  approvedDoc(data, acc?, $id?) {
    if (acc)
      try { acc.toggle($id) } catch { }
    loading(true)
    if (data.DocumentLastStatus.toLowerCase() === 'approved' || data.DocumentLastStatus.toLowerCase() === 're-upload') return;
    let obj = {
      documentStatusID: 0,
      documentStatusCode: '',
      documentStatus: this.approvedStatus[0].StatusName,
      documentStatusRemarks: '',
      documentLastApproverID: this.bookingDetails.ProviderID,
      documentID: data.DocumentID,
      createdBy: (this.isGuestLogin) ? getDefaultHMUser('').loginUserID : this.userProfile.UserID,
      modifiedBy: '',
      approverIDType: 'PROVIDER',
      DocumentStatusID: 0,
      DocumentStatusCode: '',
      DocumentStatus: this.approvedStatus[0].StatusName,
      DocumentStatusRemarks: '',
      ModifiedBy: '',
      ApproverIDType: 'PROVIDER',
      ProviderName: this.bookingDetails.ProviderName,
      EmailTo: (this.bookingDetails && this.bookingDetails.BookingUserInfo && this.bookingDetails.BookingUserInfo.PrimaryEmail) ? this.bookingDetails.BookingUserInfo.PrimaryEmail : '',
      PhoneTo: (this.bookingDetails && this.bookingDetails.BookingUserInfo && this.bookingDetails.BookingUserInfo.PrimaryPhone) ? this.bookingDetails.BookingUserInfo.PrimaryPhone : '',
      UserName: this.bookingDetails.UserName,
      HashMoveBookingNum: this.bookingDetails.HashMoveBookingNum,
      UserCountryPhoneCode: this.bookingDetails.UserCountryPhoneCodeID
    }
    this._viewBookingService.approvedDocx(obj).subscribe((res: any) => {
      loading(false)
      if (res.returnStatus === 'Success') {
        data.DocumentLastStatus = this.approvedStatus[0].StatusName;
        this._toast.success('Document has been approved', '')
      } else {
        this._toast.error('There was an error while processing your request. Please try later', 'Failed')
      }
    }, error => {
      loading(false)
      this._toast.error('There was an error while processing your request. Please try later', 'Failed')
    })
  }


  getdocStatus() {
    this._viewBookingService.getDocStatuses().subscribe((res: JsonResponse) => {
      if (res.returnId > 0)
        this.approvedStatus = res.returnObject.filter(elem => elem.BusinessLogic === 'APPROVED')
    })
  }

  setInit() {
    try {
      this.loginUser = getLoggedUserData()
      let date = new Date()
      this.resetAccordian()
      this.minDate = {
        year: date.getFullYear(),
        month: date.getMonth() + 1,
        day: date.getDate()
      }
    } catch (error) { }
  }

  customDragCheck($fileEvent: DocumentFile) {
    let selectedFile: DocumentFile = $fileEvent
    let docCopy = cloneObject(this.currentDocObject)
    docCopy.DocumentName = selectedFile.fileName
    docCopy.DocumentFileContent = selectedFile.fileBaseString
    docCopy.DocumentUploadedFileType = selectedFile.fileType
    this.currentDocObject = docCopy
  }

  fileSelectFailedEvent = ($message: string) =>
    this._toast.error($message, 'Error')

  onDocumentClick($newDocument: UserDocument, index: number, $event) {
    let newDoc: UserDocument = $newDocument
    this.currentDocObject = $newDocument
    if (this.bookingDetails.BookingStatus.toLowerCase() === 'cancelled' || $newDocument.DocumentID === -1) {
      $event.preventDefault()
      $event.stopPropagation()
      return
    }
    try {
      if ($newDocument.DocumentTypeID === -1) {
        newDoc.MetaInfoKeysDetail.forEach((element: MetaInfoKeysDetail) => {
          if (element.DataType.toLowerCase() === 'datetime') {
            if (element.KeyValue)
              element.DateModel = this.generateDateStructure(element.KeyValue)
          }
        })
      }
    } catch { }

    this.resetAccordian(index, 'customer')
  }
  generateDateStructure(strDate: string): NgbDateStruct {
    let arr: Array<string> = strDate.split('/');
    let dateModel: NgbDateStruct = {
      day: parseInt(arr[1]),
      month: parseInt(arr[0]),
      year: parseInt(arr[2])
    }
    return dateModel
  }

  uploadDocument(acc: any, acc_name: string, index: number, type, document) {
    this.loading = true
    loading(true)
    let toSend: UserDocument = cloneObject(document)
    const { currentDocObject } = this
    try { toSend.DocumentName = currentDocObject.DocumentName; } catch { }
    try { toSend.DocumentFileContent = currentDocObject.DocumentFileContent; } catch { }
    try { toSend.DocumentUploadedFileType = currentDocObject.DocumentUploadedFileType; } catch { }
    let docName: string = ''
    try {
      if (toSend.DocumentNature === 'ADD_DOC') {
        docName = toSend.MetaInfoKeysDetail.filter(meta => meta.KeyName === 'DOCNAME')[0].KeyValue
        toSend.DocumentName = docName
        try {
          const _docDesc = toSend.MetaInfoKeysDetail.filter(meta => meta.KeyName === 'DOCDESC')[0].KeyValue
          if (_docDesc) {
            toSend.DocumentDesc = _docDesc
          }
        } catch { }
      }
    } catch { }


    if (!toSend.DocumentFileContent && !toSend.DocumentFileName) {
      this._toast.error('Please select a file to upload', 'Invalid Operation')
      this.loading = false
      loading(false)
      return
    }

    let emptyFieldFlag: boolean = false
    let hasInvalidLength: boolean = false
    let invalidLength: any
    let emptyFieldName: string = ''

    try {
      if (toSend.MetaInfoKeysDetail) {
        toSend.MetaInfoKeysDetail.forEach((element: MetaInfoKeysDetail) => {
          if (element.IsMandatory && !element.KeyValue) {
            emptyFieldFlag = true
            emptyFieldName = element.KeyNameDesc
            return;
          }

          if (element.IsMandatory && element.KeyValue && element.KeyValue.length > element.FieldLength) {
            hasInvalidLength = true
            invalidLength = element.FieldLength
            emptyFieldName = element.KeyNameDesc
            return;
          }
        })
      }
    } catch { }

    if (emptyFieldFlag) {
      this._toast.error(`${emptyFieldName} field is empty`, 'Invalid Operation')
      this.loading = false
      loading(false)
      return
    }

    if (hasInvalidLength) {
      this._toast.error(`${emptyFieldName} field length should be less or equal to ${invalidLength} charaters`, 'Invalid Operation')
      this.loading = false
      loading(false)
      return
    }

    try {
      if (this.additionalDocumentsCustomers.filter(_doc => _doc.DocumentID > 0 &&
        _doc.DocumentName.toLowerCase() === toSend.DocumentName.toLowerCase() &&
        _doc.DocumentID !== toSend.DocumentID && (_doc.BusinessLogic === 'BOOKING_ADD_DOC' || _doc.BusinessLogic === 'RATE_REQUEST_ADD_DOC')).length > 0
      ) {
        this._toast.error(`A document already exist with the same name.`, 'Duplicated Document')
        this.loading = false
        loading(false)
        return
      }
    } catch { }

    toSend.DocumentID = (toSend.DocumentID) ? toSend.DocumentID : -1;
    toSend.BookingID = this.bookingDetails.BookingID
    toSend.LoginUserID = (this.loginUser && this.loginUser.LoginID) ? this.loginUser.UserID : -1

    for (let ind = 0; ind < this.additionalDocumentsCustomers.length; ind++) {
      if (ind === index)
        this.additionalDocumentsCustomers[index].ShowUpload = false
    }

    toSend.DocumentLastStatus = (toSend.DocumentID > 0 && toSend.DocumentFileName) ? 'RESET' : ''
    toSend.IsApprovalRequired = false;
    try {
      toSend.ProviderName = this.bookingDetails.ProviderName
      toSend.EmailTo = (this.bookingDetails && this.bookingDetails.BookingUserInfo && this.bookingDetails.BookingUserInfo.PrimaryEmail) ? this.bookingDetails.BookingUserInfo.PrimaryEmail : '',
        toSend.PhoneTo = this.bookingDetails.ProviderPhone
      toSend.UserName = this.bookingDetails.UserName
      toSend.HashMoveBookingNum = this.bookingDetails.HashMoveBookingNum
      toSend.UserCompanyName = (this.bookingDetails && this.bookingDetails.BookingUserInfo && this.bookingDetails.BookingUserInfo.CompanyName) ? this.bookingDetails.BookingUserInfo.CompanyName : '';
      toSend.UserCompanyID = (this.bookingDetails && this.bookingDetails.BookingUserInfo && this.bookingDetails.BookingUserInfo.CompanyID) ? this.bookingDetails.BookingUserInfo.CompanyID : -1;
      toSend.UserCountryPhoneCode = this.bookingDetails.ProviderCountryPhoneID
    } catch { }

    this._dashboardService.saveUserDocument(toSend).subscribe((res: JsonResponse) => {
      if (res.returnId > 0) {
        this.loading = false
        loading(false)
        try {
          acc.toggle(acc_name + index)
        } catch (error) {
          this.resetAccordian()
        }
        this.getBookingDetail(this.bookingId)
        if (toSend.DocumentID > 0) {
          this._toast.success('Document Updated Successfully', res.returnStatus)
        } else {
          this._toast.success('Document Saved Successfully', res.returnStatus)
        }
      } else {
        this.loading = false
        loading(false)
        this._toast.error(res.returnText, res.returnStatus)
      }
    }, () => {
      this.loading = false
      loading(false)
      this._toast.error('An unexpected error occurred. Please try again later.', 'Failed')
    })
  }

  onKeyPress = ($event, index: number, length: number) => true

  dateChangeEvent($event: NgbDateStruct, index: number) {
    const formattedDate = moment(new Date(this.dateParser.format($event))).format('L')
    this.currentDocObject.MetaInfoKeysDetail[index].KeyValue = formattedDate
  }

  acDownloadAction($url: string, acc: any, acc_name: string, index: number, type: string, $event) {
    if ($url && $url.length > 0) {
      if ($url.startsWith('[{')) {
        let document = JSON.parse($url)
        window.open(baseExternalAssets + document[0].DocumentFile, '_blank');
      } else {
        window.open(baseExternalAssets + $url, '_blank');
      }
      if (this.bookingDetails.BookingStatus.toLowerCase() === 'cancelled') {
        $event.preventDefault()
        $event.stopPropagation()
        return;
      }
      acc.toggle(acc_name + index)
      this.resetAccordian(index, type)
    }
  }

  async resetAccordian(index?: number, type?: string) {
    if (index)
      this.additionalDocumentsCustomers[index].ShowUpload = !this.additionalDocumentsCustomers[index].ShowUpload
    for (let i = 0; i < this.additionalDocumentsCustomers.length; i++) {
      if (i !== index)
        this.additionalDocumentsCustomers[i].ShowUpload = false
    }
  }

  async addDocument($event, $type: string) {
    console.log('..')
    if (this.bookingDetails.BookingStatus.toLowerCase() === 'cancelled') {
      loading(false)
      return
    }
    this.selectedDocument = undefined
    setTimeout(() => {
      this.showAcc = false
      loading(true)
    }, 0)

    await this.getDocumentList()
    const { additionalDocumentsCustomers } = this
    console.log(additionalDocumentsCustomers)
    const _existDoc = additionalDocumentsCustomers.filter(doc => doc.DocumentID === -1)
    if (_existDoc && _existDoc.length > 0) {
      setTimeout(() => {
        this.showAcc = true
        loading(false)
      }, 10)
      return
    }
    const _custAddDoc: any = {
      DocumentID: -1,
      DocumentTypeID: -1,
      DocumentName: 'ADDITIONAL DOCUMENT',
      DocumentTypeDesc: 'ADDITIONAL DOCUMENT',
      DocumentTypeName: null,
      DocumentStausRemarks: '',
      DocumentUploadDate: '',
      DocumentFileName: null,
      IsDownloadable: false,
      DocumentUploadedFileType: null,
      DocumentLastStatus: '',
      MetaInfoKeysDetail: [] as any,
      BookingID: this.bookingDetails.BookingID
    }
    try {
      _custAddDoc.MetaInfoKeysDetail.forEach(inp => { inp.KeyValue = '' })
    } catch { }
    setTimeout(() => {
      this.showAcc = true
      loading(false)
    }, 10)
    this.additionalDocumentsCustomers.push(_custAddDoc)
  }

  async getDocumentList() {
    try {
      loading(true)
      if (!this.docTypeList || this.docTypeList.length === 0) {
        try {
          if (this.bookingDetails.ShippingModeCode === 'WAREHOUSE') {
            await this.getDocTypeList('COMMON', 'WAREHOUSE')
          } else if (this.bookingDetails.ShippingModeCode === 'AIR') {
            await this.getDocTypeList('COMMON', 'AIR')
          } else if (this.bookingDetails.ShippingModeCode === 'ASSAYER') {
            await this.getDocTypeList('COMMON', 'ASSAYER')
          } else if (this.bookingDetails.ShippingModeCode === 'INSURANCE') {
            await this.getDocTypeList('COMMON', 'INSURANCE')
          } else {
            await this.getDocTypeList('COMMON', this.bookingDetails.ContainerLoad)
          }
        } catch (error) { }
      }
      loading(false)
    } catch {
      loading(false)
    }
  }

  onDocInputChange($event: any) { }

  readMore() {
    this.readMoreClass = !this.readMoreClass;
    if (this.readMoreClass)
      this.vendorAboutBtn = 'Read Less'
    else
      this.vendorAboutBtn = 'Read More'
  }

  ShippingOrgInforeadMore() {
    this.ShippingOrgInforeadMoreClass = !this.ShippingOrgInforeadMoreClass;
    if (this.ShippingOrgInforeadMoreClass)
      this.ShippingOrgInfoBtn = 'Read Less'
    else
      this.ShippingOrgInfoBtn = 'Read More'
  }

  textValidation(event) {
    try {
      const pattern = /[a-zA-Z-][a-zA-Z -]*$/;
      const inputChar = String.fromCharCode(event.charCode);
      if (!pattern.test(inputChar)) {

        if (event.charCode === 0)
          return true;
        if (event.target.value) {
          const end = event.target.selectionEnd;
          if ((event.which === 32 || event.keyCode === 32) && (event.target.value[end - 1] === ' ' || event.target.value[end] === ' ')) {
            event.preventDefault();
            return false;
          }
        } else {
          event.preventDefault();
          return false;
        }
      } else {
        return true;
      }
    } catch (error) {
      return false
    }

  }

  NumberValid(evt) {
    let charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode !== 37 && charCode !== 39 && charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }

  errorMessages($type: string, $from) {
    if ($from === 'agent') {
      if ($type === 'origin') {
        if (this.AgentInfoFormOrigin.controls.name.status === 'INVALID' && this.AgentInfoFormOrigin.controls.name.touched) {
          this.agent_origin_name_error = true;
        }
        if (this.AgentInfoFormOrigin.controls.address.status === 'INVALID' && this.AgentInfoFormOrigin.controls.address.touched) {
          this.agent_origin_address_error = true;
        }
        if (this.AgentInfoFormOrigin.controls.contact.status === 'INVALID' && this.AgentInfoFormOrigin.controls.contact.touched) {
          this.agent_origin_contact_error = true;
        }
        if (this.AgentInfoFormOrigin.controls.email.status === 'INVALID' && this.AgentInfoFormOrigin.controls.email.touched) {
          this.agent_origin_email_error = true;
        }
      } else {
        if (this.AgentInfoFormDest.controls.name.status === 'INVALID' && this.AgentInfoFormDest.controls.name.touched) {
          this.agent_dest_name_error = true;
        }
        if (this.AgentInfoFormDest.controls.address.status === 'INVALID' && this.AgentInfoFormDest.controls.address.touched) {
          this.agent_dest_address_error = true;
        }
        if (this.AgentInfoFormDest.controls.contact.status === 'INVALID' && this.AgentInfoFormDest.controls.contact.touched) {
          this.agent_dest_contact_error = true;
        }
        if (this.AgentInfoFormDest.controls.email.status === 'INVALID' && this.AgentInfoFormDest.controls.email.touched) {
          this.agent_dest_email_error = true;
        }
      }
    } else if ($from === 'supplier') {
      if ($type === 'origin') {
        if (this.SupInfoFormOrigin.controls.name.status === 'INVALID' && this.SupInfoFormOrigin.controls.name.touched) {
          this.sup_origin_name_error = true;
        }
        if (this.SupInfoFormOrigin.controls.address.status === 'INVALID' && this.SupInfoFormOrigin.controls.address.touched) {
          this.sup_origin_address_error = true;
        }
        if (this.SupInfoFormOrigin.controls.contact.status === 'INVALID' && this.SupInfoFormOrigin.controls.contact.touched) {
          this.sup_origin_contact_error = true;
        }
        if (this.SupInfoFormOrigin.controls.email.status === 'INVALID' && this.SupInfoFormOrigin.controls.email.touched) {
          this.sup_origin_email_error = true;
        }
      } else {
        if (this.SupInfoFormDest.controls.name.status === 'INVALID' && this.SupInfoFormDest.controls.name.touched) {
          this.sup_dest_name_error = true;
        }
        if (this.SupInfoFormDest.controls.address.status === 'INVALID' && this.SupInfoFormDest.controls.address.touched) {
          this.sup_dest_address_error = true;
        }
        if (this.SupInfoFormDest.controls.contact.status === 'INVALID' && this.SupInfoFormDest.controls.contact.touched) {
          this.sup_dest_contact_error = true;
        }
        if (this.SupInfoFormDest.controls.email.status === 'INVALID' && this.SupInfoFormDest.controls.email.touched) {
          this.sup_dest_email_error = true;
        }
      }
    } else {
      if (this.ShipDtlForm.controls.invoice.status === 'INVALID' && this.ShipDtlForm.controls.invoice.touched) {
        this.shipDtl_invoice = true;
      }
      if (this.ShipDtlForm.controls.custom.status === 'INVALID' && this.ShipDtlForm.controls.custom.touched) {
        this.shipDtl_custom = true;
      }
      if (this.ShipDtlForm.controls.goods_dec.status === 'INVALID' && this.ShipDtlForm.controls.goods_dec.touched) {
        this.shipDtl_goods_dec = true;
      }
      if (this.ShipDtlForm.controls.bill_lading.status === 'INVALID' && this.ShipDtlForm.controls.bill_lading.touched) {
        this.shipDtl_bill_lading = true;
      }
      if (this.ShipDtlForm.controls.export_no.status === 'INVALID' && this.ShipDtlForm.controls.export_no.touched) {
        this.shipDtl_export_no = true;
      }
      if (this.ShipDtlForm.controls.awb_no.status === 'INVALID' && this.ShipDtlForm.controls.awb_no.touched) {
        this.shipDtl_awb_no = true;
      }
    }
  }

  setShipDtlInfo() {
    if (this.bookingDetails.ComInvNo) {
      this.ShipDtlForm.controls['invoice'].setValue(this.bookingDetails.ComInvNo);
    }
    if (this.bookingDetails.CusClearNo) {
      this.ShipDtlForm.controls['custom'].setValue(this.bookingDetails.CusClearNo);
    }
    if (this.bookingDetails.GDeclareNo) {
      this.ShipDtlForm.controls['goods_dec'].setValue(this.bookingDetails.GDeclareNo);
    }
    if (this.bookingDetails.BLNo && this.bookingDetails.ShippingModeCode === 'SEA') {
      this.ShipDtlForm.controls['bill_lading'].setValue(this.bookingDetails.BLNo);
    }
    if (this.bookingDetails.ExpFormNo) {
      this.ShipDtlForm.controls['export_no'].setValue(this.bookingDetails.ExpFormNo);
    }
    if (this.bookingDetails.BLNo && this.bookingDetails.ShippingModeCode === 'AIR') {
      this.ShipDtlForm.controls['awb_no'].setValue(this.bookingDetails.BLNo);
    }
  }


  readMore2() {
    this.readMoreClass2 = !this.readMoreClass2;
    if (this.readMoreClass2)
      this.vendorAboutBtn2 = 'Read Less'
    else
      this.vendorAboutBtn2 = 'Read More'
  }

  @ViewChild('acc1') acc1: NgbAccordion;
  public activePanelId = ''

  uploadDoc(doc: BookingDocumentDetail, type?: string) {
    const modalRef = this._modalService.open(UploadComponent, { size: 'lg', centered: true, windowClass: 'small-modal', backdrop: 'static' })
    if (type === 'reupload') {
      if (doc.DocumentLastStatus.toLowerCase() === 'approved') {
        doc.DocumentLastStatus = 'RESET'
      }
    }

    try {
      // console.log(this.activePanelId)
      this.acc1.toggle(this.activePanelId)
      if (this.activePanelId && this.activePanelId.length > 1) {
        this.activePanelId = ''
      }
    } catch { }

    doc.BookingID = this.bookingDetails.BookingID
    modalRef.componentInstance.passedData = doc
    modalRef.componentInstance.bookingData = this.bookingDetails
    modalRef.result.then((result) => {
      if (result === 'success') {
        this.getBookingDetail(this.bookingId)
      }
    })
  }

  openVesselSchedule(data) {
    const modalRef = this._modalService.open(VesselScheduleDialogComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'large-modal',
      backdrop: 'static',
      keyboard: false
    })
    const { departureDate } = this
    modalRef.componentInstance.data = {
      bookingID: this.bookingDetails.BookingID,
      date: cloneObject(departureDate),
      schedules: data,
      bookingDetails: this.bookingDetails,
      carriersList: this.searchCarrierList,
      from: 'VIEW_BOOKING'
    }
    modalRef.result.then(result => {
      if (result) {
        try {
          this.selectedShipping = null
          this.departureDate = ''
          this.showScheduler = false;
          this.bookingDetails.VoyageRefNum = result.voyageRefNo
          this.bookingDetails.VesselName = result.vesselName
          this.bookingDetails.VesselCode = result.vesselCode
          this.bookingDetails.CarrierName = result.carrierName
          this.bookingDetails.CarrierCode = result.carrierCode ? result.carrierCode : result.carrierName
          this.bookingDetails.CarrierImage = result.carrierImage
          this.bookingDetails.PortCutOffLcl = result.portCutOffDate
          this.bookingDetails.EtdLcl = result.etdDate
          this.bookingDetails.EtdUtc = result.etdDate
          this.bookingDetails.EtaLcl = result.etaDate
          this.bookingDetails.EtaUtc = result.etaDate
          try { this.bookingDetails.TransitTime = result.transitDays } catch (err) { console.log(err) }
          this._toast.success('Update Successful');
        } catch { }
      }
    })
  }

  public carriersList: any[] = []
  public selectedShipping: ICarrierFilter;
  public departureDate: any
  public selectedDate: string;

  /**
   *
   * Getting Shipping Lines
   * @param {object} reason
   * @memberof ViewBookingComponent
   */
  getCarrierSchedule(reason, date?) {
    let data = null
    try {
      data = {
        BookingID: this.bookingDetails.BookingID,
        PolID: this.bookingDetails.PolID,
        PodID: this.bookingDetails.PodID,
        ContainerLoadType: this.bookingDetails.ContainerLoad,
        ResponseType: reason,
        CarrierID: 0,
        fromEtdDate: this.bookingDetails.EtdUtc,
        toEtdDate: this.bookingDetails.EtdUtc,
      }
      if (reason === 'details') {
        data.CarrierID = this.selectedShipping.id
        data.fromEtdDate = moment.utc(date).format()
        data.toEtdDate = moment.utc(date).add(1, 'month').format()
      }
      if (!data.fromEtdDate) {
        const _date = moment.utc(new Date()).format()
        data.fromEtdDate = _date
      }
      if (!data.toEtdDate) {
        const _date = moment.utc(data.fromEtdDate).add(1, 'month').format()
        data.toEtdDate = _date
      }
    } catch (error) {
      loading(false)
    }
    this._viewBookingService.getCarrierSchedule(data).subscribe((res: any) => {
      // console.log(res)
      loading(false)
      if (reason === 'details') {
        this.openVesselSchedule(res.returnObject)
      } else {
        this.carriersList = res.returnObject;
      }
    }, (err: any) => {
      loading(false)
      console.log(err)
    })
  }

  shippings = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term =>
        !term || term.length < 3
          ? []
          : this.searchCarrierList.filter(
            v =>
              v.title &&
              v.title.toLowerCase().indexOf(term.toLowerCase()) > -1
          )
      )
    );
  formatter = (x: { title: string; imageName: string }) =>
    x.title;


  /**
   *
   * Getting Shiiping Line Image
   * @param {string} $image
   * @returns
   * @memberof ViewBookingComponent
   */
  getShippingLineImage($image: string) {
    return getImagePath(
      ImageSource.FROM_SERVER,
      '/' + $image,
      ImageRequiredSize.original
    );
  }


  /**
   *
   * Get Schedule List
   * @memberof ViewBookingComponent
   */
  viewSchedule() {
    loading(true)
    const { departureDate } = this
    const newDate = this.dateParser.format(cloneObject(departureDate))
    const date = moment(newDate).format('L');
    this.getCarrierSchedule('details', date)
  }


  resetForm($type: string, $from: string) {
    // this.toggleInfoSection($type, $from)
    if ($from === 'agent') {
      if ($type === 'origin') {
        this.AgentInfoFormOrigin.reset()
        if (this.bookingDetails.JsonAgentOrgInfo && this.bookingDetails.JsonAgentOrgInfo.Name) {
          this.editAgentorgToggler = true;
        } else {
          this.toggleInfoSection($type, $from)
        }
        this.agent_origin_name_error = false
        this.agent_origin_address_error = false
        this.agent_origin_contact_error = false
        this.agent_origin_email_error = false
      } else if ($type === 'destin') {
        if (this.bookingDetails.JsonAgentDestInfo && this.bookingDetails.JsonAgentDestInfo.Name) {
          this.editAgentDestToggler = true;
        } else {
          this.toggleInfoSection($type, $from)
        }
        this.AgentInfoFormDest.reset()
        this.agent_dest_name_error = false
        this.agent_dest_address_error = false
        this.agent_dest_contact_error = false
        this.agent_dest_email_error = false
      }
    } else if ($from === 'supplier') {
      if ($type === 'origin') {
        this.SupInfoFormOrigin.reset()
        if (this.bookingDetails.JsonShippingOrgInfo && this.bookingDetails.JsonShippingOrgInfo.Name) {
          this.editSupOrgToggler = true;
        } else {
          this.toggleInfoSection($type, $from)
        }
        this.sup_origin_name_error = false
        this.sup_origin_address_error = false
        this.sup_origin_contact_error = false
        this.sup_origin_email_error = false
      } else if ($type === 'destin') {
        this.SupInfoFormDest.reset()
        if (this.bookingDetails.JsonShippingDestInfo && this.bookingDetails.JsonShippingDestInfo.Name) {
          this.editSupDestToggler = true;
        } else {
          this.toggleInfoSection($type, $from)
        }
        this.sup_dest_name_error = false
        this.sup_dest_address_error = false
        this.sup_dest_contact_error = false
        this.sup_dest_email_error = false
      }
    } else if ($type === 'ship_dtl') {
      // console.log('herer')
      this.ShipDtlForm.reset()
      this.shipDtl_invoice = false
      this.shipDtl_custom = false
      this.shipDtl_goods_dec = false
      this.shipDtl_bill_lading = false
      this.shipDtl_export_no = false
      this.shipDtl_awb_no = false
      this.setShipDtlInfo()
    }
  }

  //Toogle work

  originToggleSupp: boolean = false
  originToggleClassSupp: string = 'Hide'

  originToggleAgent: boolean = false
  originToggleClassAgent: string = 'Hide'

  destinToggleSupp: boolean = false
  destinToggleClassSupp: string = 'Hide'

  destinToggleAgent: boolean = false
  destinToggleClassAgent: string = 'Hide'

  toggleInfoSection($type: string, $from) {
    if ($from === 'agent') {
      if ($type === 'origin') {
        this.originToggleAgent = !this.originToggleAgent;
        this.originToggleClassAgent = (!this.originToggleAgent) ? 'Hide' : 'Show';
      } else if ($type === 'destin') {
        this.destinToggleAgent = !this.destinToggleAgent;
        this.destinToggleClassAgent = (!this.destinToggleAgent) ? 'Hide' : 'Show';
      }
    } else {
      if ($type === 'origin') {
        this.originToggleSupp = !this.originToggleSupp;
        this.originToggleClassSupp = (!this.originToggleSupp) ? 'Hide' : 'Show';
      } else if ($type === 'destin') {
        this.destinToggleSupp = !this.destinToggleSupp;
        this.destinToggleClassSupp = (!this.destinToggleSupp) ? 'Hide' : 'Show';
      }
    }

  }

  closeFix(event, datePicker) {
    if (event.target.offsetParent === null)
      datePicker.close();
    else if (event.target.offsetParent.nodeName !== 'NGB-DATEPICKER')
      datePicker.close();
  }

  getCustomerImage($image: string) {
    if (isJSON($image)) {
      const providerImage = JSON.parse($image)
      return baseExternalAssets + '/' + providerImage[0].DocumentFile
    } else {
      return getImagePath(ImageSource.FROM_SERVER, $image, ImageRequiredSize.original)
    }
  }


  /**
   * ADD CONTAINER DETAILS
   *
   * @memberof ViewBookingComponent
   */
  openAddContainer() {
    const modalRef = this._modalService.open(AddContainersComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'large-modal',
      backdrop: 'static',
      keyboard: false
    });
    modalRef.componentInstance.containerDetails = this.bookingDetails;
    modalRef.result.then((result) => {
      if (result) {
        this.ngOnInit()
      }
    });
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);
  }

  /**
   * ADD B/L DETAILS
   *
   * @memberof ViewBookingComponent
   */
  openAddBL($type: string) {
    if (this.bookingDetails.ShippingStatus.toLowerCase() === 'completed') {
      return
    }
    // using BL popup to add LCL-Container Number
    const modalRef = this._modalService.open(AddBlComponent, {
      size: 'sm',
      centered: true,
      windowClass: 'small-modal',
      backdrop: 'static',
      keyboard: false
    });
    modalRef.componentInstance.data = this.bookingDetails;
    modalRef.componentInstance.type = $type
    modalRef.componentInstance.lclContainerNo = this.lclContainerNo
    modalRef.result.then((_result) => {
      if (_result !== 'close') {
        if ($type === 'BL_NO') {
          this.bookingDetails.BLNo = _result.blNo;
          this.bookingDetails.BLDate = _result.blDate;
          this.setAwbNumber(_result)
        } else {
          this.lclContainerNo = _result
        }
      }
    });
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);
  }

  onAccordChange(changes) {
    try {
      this.activePanelId = changes.panelId
    } catch { }
  }


  /**
   * SHARE VIEW BOOKING LINK
   *
   * @memberof ViewBookingComponent
   */
  shareViewBooking(component) {
    this.loading = true
    let shareURL: string
    let safeBookingId: any
    if (component === 'viewBooking') {
      safeBookingId = encryptBookingID(this.bookingDetails.BookingID, this.loginUser.UserID, this.bookingDetails.ShippingModeCode, 'GUEST')
      shareURL = portalOrigin + '/booking-detail/' + encodeURIComponent(safeBookingId)
    }
    this._viewBookingService.getGuestUserKey(this.loginUser.UserID).pipe(untilDestroyed(this)).subscribe((res: any) => {
      this.loading = false
      let finalURL = shareURL + '/' + res.returnText
      const modalRef = this._modalService.open(ShareViewBooking, {
        size: 'lg', centered: true, windowClass: 'small-modal', backdrop: 'static', keyboard: false
      });
      modalRef.componentInstance.shareObjectInfo = {
        url: finalURL
      }
      setTimeout(() => {
        if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
          document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
        }
      }, 0)
    }, (err: any) => {
    })
  }

  ngAfterViewChecked() {
    if (this.isGuestLogin && this._jwtService.getRightObj()) {
      this.objectRights = JSON.parse(this._jwtService.getRightObj())
      if (this.objectRights) {
        this.setRights()
      }
      this.setDefailtRights()
    }
  }

  getJsonDepartureDays($jsonData: string, type: string) {
    return getJsonDepartureDays($jsonData, type)
  }

  getUIImage($image: string, isProvider: boolean) {
    if (isJSON($image)) {
      const providerImage = JSON.parse($image)
      return baseExternalAssets + '/' + providerImage[0].DocumentFile
    } else {
      return getImagePath(ImageSource.FROM_SERVER, $image, ImageRequiredSize.original)
    }
  }


  lengthUnits
  volumeUnits

  getLenghtUnit(id) {
    let _return = 'cm'
    try {
      const { lengthUnits } = this
      const data = lengthUnits.filter(unit => unit.UnitTypeID === id)[0]
      _return = data.UnitTypeShortName
    } catch (error) { }
    return _return
  }

  getVolumeUnit(id) {
    let _return = 'cm'
    try {
      const { volumeUnits } = this
      const data = volumeUnits.filter(unit => unit.UnitTypeID === id)[0]
      _return = data.UnitTypeShortName
    } catch (error) { }
    return _return
  }


  getWarehousingUnits() {
    if (!localStorage.getItem('units')) {
      this._viewBookingService.getLCLUnits().subscribe(
        (res: JsonResponse) => {
          const unitsResponse = res;
          this.setWarehousingUnits(unitsResponse.returnObject);
          localStorage.setItem(
            'units',
            JSON.stringify(unitsResponse.returnObject)
          );
        },
        (err: HttpErrorResponse) => { }
      );
    } else {
      this.setWarehousingUnits(JSON.parse(localStorage.getItem('units')));
    }
  }

  setWarehousingUnits(unitsResponse) {
    this.lengthUnits = unitsResponse.filter(e => e.UnitTypeNature === 'LENGTH');
    this.volumeUnits = unitsResponse.filter(e => e.UnitTypeNature === 'VOLUME');
  }

  prepareLclContainers() {
    let finArr = []
    try {
      const { SearchCriteriaContainerDetail, LclChips } = this.searchCriteria
      const _filteredContainers = removeDuplicates(SearchCriteriaContainerDetail, 'contSpecID')
      const colArr = extractColumn(_filteredContainers, 'contSpecID')
      _filteredContainers.forEach(contaier => {
        const { contSpecID, containerCode, ContainerSpecDesc } = contaier

        const _filteredChips = LclChips.filter(chip => chip.contSpecID === contSpecID)

        const _toPush = {
          contSpecID,
          containerCode,
          containerSpecDesc: ContainerSpecDesc,
          LclChips: _filteredChips
        }
        finArr.push(_toPush)
      });
    } catch {

    }

    return finArr
  }




  shareUrl(type: string, $from) {
    if (!this.isGuestLogin) {

      let toSend: any
      if ($from === 'agent') {
        if (type === 'origin' && this.bookingDetails.JsonAgentOrgInfo && this.bookingDetails.JsonAgentOrgInfo.Email) {
          toSend = this.getShareSendObject(this.bookingDetails.JsonAgentOrgInfo.Email)
        } else if (type === 'destination' && this.bookingDetails.JsonAgentDestInfo && this.bookingDetails.JsonAgentDestInfo.Email) {
          toSend = this.getShareSendObject(this.bookingDetails.JsonAgentDestInfo.Email)
        }
      } else {
        if (type === 'origin' && this.bookingDetails.JsonShippingOrgInfo && this.bookingDetails.JsonShippingOrgInfo.Email) {
          toSend = this.getShareSendObject(this.bookingDetails.JsonShippingOrgInfo.Email)
        } else if (type === 'destination' && this.bookingDetails.JsonShippingDestInfo && this.bookingDetails.JsonShippingDestInfo.Email) {
          toSend = this.getShareSendObject(this.bookingDetails.JsonShippingDestInfo.Email)
        }
      }



      const { emailTo } = toSend

      const _modalData: ConfirmDialogContent = {
        messageTitle: 'Share Booking Details',
        messageContent: `Are you sure you would like to share this booking details with ${emailTo}?`,
        buttonTitle: 'Yes',
        data: null
      }

      const modalRef = this._modalService.open(ConfirmDialogGenComponent, {
        size: 'lg',
        centered: true,
        windowClass: 'medium-modal',
        backdrop: 'static',
        keyboard: false
      });

      modalRef.result.then((result: string) => {
        if (result) {
          this.shareUrlAction(toSend)
        }
      })

      modalRef.componentInstance.modalData = _modalData;
      setTimeout(() => {
        if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
          document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
        }
      }, 0);
    }
  }

  shareUrlAction(toSend: any) {
    loading(true)
    this._viewBookingService.shareBookUrl(toSend).subscribe((res: JsonResponse) => {
      loading(false)
      const { returnId, returnText } = res
      if (returnId > 0) {
        this._toast.success(returnText, 'Success')
      } else {
        this._toast.error(returnText)
      }
    }, err => {
      loading(false)
      this._toast.error('Error while processing request, please try again later')
    })
  }

  getShareSendObject(email: string) {
    return {
      userID: this.loginUser.UserID,
      userName: '',
      loginID: this.loginUser.LoginID,
      isProvider: true,
      providerName: this.bookingDetails.ProviderName,
      emailTo: email,
      baseUrl: this.generateBookingURL() + '/',
    }
  }

  generateBookingURL() {
    try {
      const safeBookingId = encryptBookingID(this.bookingDetails.BookingID, this.loginUser.UserID, this.bookingDetails.ShippingModeCode, 'GUEST')
      // const shareURL = localStorage.getItem('jwtOrigin') + '/booking-detail/' + encodeURIComponent(safeBookingId)
      const shareURL = portalOrigin + 'booking-detail/' + encodeURIComponent(safeBookingId)
      return shareURL
    } catch (error) {
      return ''
    }
  }

  public trackingData: TrackingMonitoring
  public qualityMonitorData: QaualitMonitorResp
  public isTrackingOnBooking: boolean = false
  public isMonitoringOnBooking: boolean = false
  shouldMapOpen: boolean = true
  public currentBookingId: number
  public isTracking: boolean = false
  public isContDtl: boolean = true
  public containerNumber: QualityMonitoringAlertData

  changeView($type: string) {
    switch ($type) {
      case 'track':
        if (!this.shouldMapOpen) {
          this._toast.warning('Tracking Cordinates Mismatched')
        }
        if (!this.isMonitoringOnBooking && !this.isTrackingOnBooking && !this.bookingDetails.EtdUtc) {
          this._toast.warning('No data available for this Booking')
          return
        }
        loading(true)
        const { currentBookingId } = this

        // if (this.isTrackingOnBooking) {
        if (this.searchCriteria.searchMode !== 'truck-ftl') {
          this._viewBookingService.getTrackingInformation(currentBookingId).subscribe((res: JsonResponse) => {
            const { returnId, returnText } = res
            loading(false)
            if (returnId > 0) {
              this.trackingData = JSON.parse(returnText)
            } else {
              this._toast.error(returnText, 'Error')
              this.trackingData = {
                ContainerDetails: [],
                EtaDetails: {
                  EtaLcl: '',
                  EtaUtc: '',
                  Status: ''
                },
                Polylines: [],
                RouteInformation: '',
                VesselInfo: {
                  VesselName: '',
                  VesselType: '',
                  VesselFlag: '',
                  VesselPhotos: [],
                }
              }
            }
            this.isTracking = true;
            this.isContDtl = false
          }, (error: HttpErrorResponse) => {
            this._toast.error(error.message, 'Error')
            loading(false)
            this.trackingData = {
              ContainerDetails: [],
              EtaDetails: {
                EtaLcl: '',
                EtaUtc: '',
                Status: ''
              },
              Polylines: [],
              RouteInformation: '',
              VesselInfo: {
                VesselName: '',
                VesselType: '',
                VesselFlag: '',
                VesselPhotos: [],
              }
            }
            this.isTracking = true;
            this.isContDtl = false
          })
        }


        if (this.isMonitoringOnBooking || this.isTrackingOnBooking) {
          this._viewBookingService.getQualityMonitoringInformation(currentBookingId).subscribe((res: JsonResponse) => {
            loading(false)
            const { returnId, returnText } = res
            if (returnId > 0) {
              this.qualityMonitorData = JSON.parse(returnText)
            } else {
              this._toast.error(returnText, 'Error')
              this.qualityMonitorData = {
                AlertData: [],
                MonitoringData: [],
                ProgressIndicators: [],
                bookingQualityDetails: [],
                qualityAlertCountDetails: []
              }
            }
            this.isTracking = true;
            this.isContDtl = false
          }, (error: HttpErrorResponse) => {
            loading(false)
            this._toast.error(error.message, 'Error')
            this.qualityMonitorData = {
              AlertData: [],
              MonitoringData: [],
              ProgressIndicators: [],
              bookingQualityDetails: [],
              qualityAlertCountDetails: []
            }
            this.isTracking = true;
            this.isContDtl = false
          })
        }

        break;
      case 'view':
        this.isTracking = false
        this.isContDtl = false
        break;
      case 'container':
        this.isTracking = true;
        this.isContDtl = true
        break;
      default:
        break;
    }
  }

  trackContrCallback($event: QualityMonitoringAlertData) {
    this.isTracking = true;
    this.isContDtl = true
    this.containerNumber = $event
  }

  async onMultiAirlineClick(carrier: any) {
    if (!carrier.CarrierCode || carrier.CarrierCode.toLowerCase() !== 'multiple') {
      return
    }
    const { airlines } = this
    if (!airlines || airlines.length === 0) {
      return
    }
    loading(true)
    if (airlines.length > 0) {
      loading(false)
      const modalRef = this._modalService.open(MultiAirlinesComponent, {
        size: 'lg',
        windowClass: 'medium-modal',
        centered: true,
        backdrop: 'static',
        keyboard: false
      });
      modalRef.componentInstance.airlines = airlines
    } else {
      loading(false)
    }
  }
  async getMultiAirlines($bookingkey) {
    let _carriers = []
    try {
      const res: JsonResponse = await this._viewBookingService.getRouteDetailForAIR($bookingkey).toPromise()
      const { returnId, returnText, returnObject, returnStatus } = res
      if (returnId > 0) {
        _carriers = res.returnObject
      } else {
        this._toast.error(returnText, returnStatus)
      }
    } catch (error) {
      this._toast.error('Error while processing request, please try again later', 'Error')
    }
    return _carriers
  }

  setAdjustCheck() {
    setTimeout(() => {
      try {
        // if ((this.bookingDetails.BookingStatus.toLowerCase() === 'in-review' || this.bookingDetails.BookingStatus.toLowerCase() === 'confirmed' || this.bookingDetails.ShippingStatus.toLowerCase() === 'in-transit')) {
        const _adjustment = this.bookingDetails.BookingPriceDetail.filter((element) => element.SurchargeCode === 'ADJMT' && element.TransMode.toLowerCase() === 'write')
        if (_adjustment && _adjustment.length > 0) {
          this.hasAdjustedAmount = true
        }
        // }
      } catch (error) {

      }
    }, 0);
  }

  editShipDtl() {
    this.editShipDetails = !this.editShipDetails
  }


  async getDocTypeList(subProcess, key) {
    const res = await this._viewBookingService.getDocList(subProcess, key).toPromise() as any
    if (res.returnId > 0)
      this.docTypeList = res.returnObject
  }

  fetchDocDtl($event, $acc, $id) {
    $event.preventDefault();
    $event.stopPropagation();

    const { additionalDocumentsCustomers } = this
    const { selectedDocument } = this

    if (!selectedDocument) {
      this._toast.warning('Please select a document first')
      return
    }

    if (additionalDocumentsCustomers.filter(_doc => _doc.DocumentNature !== 'ADD_DOC' && _doc.DocumentTypeID === parseInt(selectedDocument as any)).length > 0) {
      this._toast.warning('This document already exists.')
      return
    }

    loading(true)
    this._viewBookingService.getDocObject(selectedDocument).subscribe((res: JsonResponse) => {
      loading(false)
      if (res.returnId > 0) {
        const _newDoc: UserDocument = {
          ...res.returnObject,
          ShowUpload: true
        }

        this.currentDocObject = _newDoc
        const _arr = [
          ...additionalDocumentsCustomers,
          _newDoc
        ]
        this.additionalDocumentsCustomers = _arr.filter(_doc => _doc.DocumentTypeID > -1)
        setTimeout(() => { $acc.toggle($id) }, 100)
        this.selectedDocument = undefined
      } else {
        this._toast.error(res.returnText, res.returnStatus)
      }
    }, () => loading(false))
  }

  async getDocumentDtl(selectedDocument) {
    const { additionalDocumentsCustomers } = this
    try {
      const res = await this._viewBookingService.getDocObject(selectedDocument).toPromise() as any
      if (res.returnId > 0) {
        const _newDoc: UserDocument = {
          ...res.returnObject, ShowUpload: false, HideCancel: true
        }

        this.currentDocObject = _newDoc
        const _arr = [
          ...additionalDocumentsCustomers, _newDoc
        ]
        this.additionalDocumentsCustomers = _arr.filter(_doc => _doc.DocumentTypeID > -1)
      } else {
        this._toast.error(res.returnText, res.returnStatus)
      }
    } catch { }
  }

  removeDocObj($event, docId) {
    $event.preventDefault();
    $event.stopPropagation();
    setTimeout(() => {
      this.showAcc = false
      loading(true)
    }, 10);
    const { additionalDocumentsCustomers } = this
    this.additionalDocumentsCustomers = additionalDocumentsCustomers.filter(_doc => _doc.DocumentTypeID !== docId && _doc.DocumentTypeID !== -1)

    setTimeout(() => {
      this.showAcc = true
      loading(false)
    }, 20);

  }

  preventClick($event) {
    $event.preventDefault();
    $event.stopPropagation();
  }

  openContainerVesselTracking() {
    let _url = ''
    if (this.bookingDetails.BLNo && this.bookingDetails.BLTrackingURL) {
      const { BLTrackingURL } = this.bookingDetails
      _url = BLTrackingURL.replace('{BLNo}', this.bookingDetails.BLNo)
      window.open(_url, '_blank')
    } else if (this.enteredContainerNumber && this.bookingDetails.ContainerTrackingURL) {
      const { ContainerTrackingURL } = this.bookingDetails
      _url = ContainerTrackingURL.replace('{ContainerNo}', this.enteredContainerNumber)
      window.open(_url, '_blank')
    } else if (this.bookingDetails.BLNo && !this.bookingDetails.BLTrackingURL) {
      this._toast.warning('BLNo tracking URL not available')
    } else if (this.enteredContainerNumber && !this.bookingDetails.ContainerTrackingURL) {
      this._toast.warning('Container tracking URL not available')
    }
  }

  openBLNumberWindow() {
    if (this.bookingDetails.BLTrackingURL && this.bookingDetails.BLNo) {
      const { BLTrackingURL } = this.bookingDetails
      const _url = BLTrackingURL.replace('{BLNo}', this.bookingDetails.BLNo)
      window.open(_url, '_blank')
    }
  }

  openLCLContainerTracking() {
    if (this.bookingDetails.ContainerTrackingURL && this.lclContainerNo) {
      const { ContainerTrackingURL } = this.bookingDetails
      const _url = ContainerTrackingURL.replace('{ContainerNo}', this.lclContainerNo)
      window.open(_url, '_blank')
    }
  }

  openAirTrackingUrl() {
    let _url = ''
    const { BLTrackingURL } = this.bookingDetails
    _url = BLTrackingURL.replace('{BLNo}', this.bookingDetails.BLNo.slice(3))
    try { _url = _url.replace('{AWBCode}', this.bookingDetails.BLNo.substring(0, 3)) } catch { }
    window.open(_url, '_blank')
  }

  async viewCertificateCharges() {
    if (!this.pmexCharges) {
      loading(true)
      try {
        const res: JsonResponse = await this._viewBookingService.getBookingDocumentCharges(this.bookingDetails.BookingID, this.bookingDetails.ShippingModeCode).toPromise() as any
        setTimeout(() => { loading(false) }, 0)
        if (res.returnId > 0)
          this.pmexCharges = res.returnObject
      } catch {
        setTimeout(() => { loading(false) }, 0)
        this._toast.error('Please try again Later', 'Server Error')
        return
      }
    }

    const modalRef = this._modalService.open(CertificateChargesComponent, {
      size: 'lg', centered: true, windowClass: 'mid-size-popup bar-class', backdrop: 'static', keyboard: false
    });
    modalRef.componentInstance.pmexCharges = this.pmexCharges;
  }



  addReceipt() {
    const modalRef = this._modalService.open(AddDispatchComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'large-modal',
      backdrop: 'static',
      keyboard: false
    });
    modalRef.componentInstance.bookingData = this.bookingDetails
  }


  /**
   *
   * VIEW Assayer DETAILS
   * @memberof ViewBookingComponent
   */
  openStatusHistory($history) {
    const modalRef = this._modalService.open(AssayerContainerInfoComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'large-modal',
      backdrop: 'static',
      keyboard: false
    });
    modalRef.componentInstance.containerDetails = this.bookingDetails;
    modalRef.componentInstance.history = $history

    setTimeout(() => {
      // if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
      //   document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      // }
    }, 0);
  }



  updateContainerStatus() {
    const { containerInspection } = this
    if (!this.containerInspection || !this.containerInspection.BookingInspectionID) {
      this._toast.warning('This booking does not have an inspection id', 'Invalid Operation')
      return
    }


    if (!this.containerComments) {
      this._toast.warning('Comments cannot be empty', 'Invalid Operation')
      return
    }

    const _modalData: ConfirmDialogContent = {
      messageTitle: 'Update Container Stuffing',
      messageContent: `Are you sure you would like to update container stuffing data?`,
      buttonTitle: 'Yes',
      data: null
    }

    const modalRef = this._modalService.open(ConfirmDialogGenComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'medium-modal',
      backdrop: 'static',
      keyboard: false
    });

    modalRef.result.then((result: string) => {
      if (result) {
        const toSend = {
          'BookingInspectionID': containerInspection.BookingInspectionID,
          'BookingID': this.bookingDetails.BookingID,
          'InspectionStatus': this.selectedContainerStatus,
          'InspectionComments': this.containerComments,
          'InspectionNature': 'CONTAINER_STUFFING',
          'InspectionStatusUpdatedByUserID': getLoggedUserData().UserID

        }
        loading(true)

        // console.log(JSON.stringify(toSend))

        this._viewBookingService.updateInspectionStatus(toSend).subscribe((res: JsonResponse) => {
          loading(false)
          if (res.returnId > 0) {
            this.getBookingDetail(this.bookingId);
          }
        }, error => {
          console.log(error.message)
          loading(false)
        })
      }
    })

    modalRef.componentInstance.modalData = _modalData;
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);
  }

  updateCommodityStatus() {
    const { commodityInspection } = this
    if (!this.commodityInspection || !this.commodityInspection.BookingInspectionID) {
      this._toast.warning('This booking does not have an inspection id', 'Invalid Operation')
      return
    }

    if (!this.comoodityComments) {
      this._toast.warning('Comments cannot be empty', 'Invalid Operation')
      return
    }

    const _modalData: ConfirmDialogContent = {
      messageTitle: 'Update Commodity',
      messageContent: `Are you sure you would like to update commodity data?`,
      buttonTitle: 'Yes',
      data: null
    }

    const modalRef = this._modalService.open(ConfirmDialogGenComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'medium-modal',
      backdrop: 'static',
      keyboard: false
    });

    modalRef.result.then((result: string) => {
      if (result) {

        const toSend = {
          'BookingInspectionID': commodityInspection.BookingInspectionID,
          'BookingID': this.bookingDetails.BookingID,
          'InspectionStatus': this.selectedCommodityStatus,
          'InspectionComments': this.comoodityComments,
          'InspectionNature': 'COMMODITY_INSPECTION',
          'InspectionStatusUpdatedByUserID': getLoggedUserData().UserID

        }
        loading(true)
        this._viewBookingService.updateInspectionStatus(toSend).subscribe((res: JsonResponse) => {
          loading(false)
          if (res.returnId > 0) {
            this.getBookingDetail(this.bookingId);
          }
        }, error => {
          console.log(error.message)
          loading(false)
        })
      }
    })

    modalRef.componentInstance.modalData = _modalData;
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);


  }

  acceptContainerStatus() {
    const { containerInspection } = this
    if (!this.containerInspection || !this.containerInspection.BookingInspectionID) {
      this._toast.warning('This booking does not have an inspection id', 'Invalid Operation')
      return
    }


    const _modalData: ConfirmDialogContent = {
      messageTitle: 'Accept Schedule',
      messageContent: `Are you sure you would like to accept this schedule?`,
      buttonTitle: 'Yes',
      data: null
    }

    const modalRef = this._modalService.open(ConfirmDialogGenComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'medium-modal',
      backdrop: 'static',
      keyboard: false
    });

    modalRef.result.then((result: string) => {
      if (result) {
        const toSend = {
          'BookingInspectionID': containerInspection.BookingInspectionID,
          'BookingID': this.bookingDetails.BookingID,
          'AcceptedBy': 'ASSAYER',
          'AcceptedByUserID': getLoggedUserData().UserID,
          'AcceptedStatus': 'ACCEPTED',
          'InspectionNature': 'CONTAINER_STUFFING',
          'AcceptedStatusDateTime': new Date()
        }
        loading(true)
        this._viewBookingService.updateAcceptedStatus(toSend).subscribe((res: JsonResponse) => {
          loading(false)
          if (res.returnId > 0) {
            this.getBookingDetail(this.bookingId);
          }
        }, error => {
          console.log(error.message)
          loading(false)
        })
      }
    })

    modalRef.componentInstance.modalData = _modalData;
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);

  }


  /**
   *
   * UPDATE Assayer SCHEDULE
   * @memberof ViewBookingComponent
   */
  updateAssayerSchedule($data, $mode, $type) {
    if ($mode === 'edit' && (!$data || !$data.BookingInspectionID)) {
      this._toast.warning('This booking does not have an inspection id', 'Invalid Operation')
      return
    }
    const modalRef = this._modalService.open(AssayerUpdateScheduleComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'large-modal',
      backdrop: 'static',
      keyboard: false
    });
    modalRef.componentInstance.bookingDetails = this.bookingDetails;
    modalRef.componentInstance.schData = $data;
    modalRef.componentInstance.mode = $mode;
    modalRef.componentInstance.status = this.selectedCommodityStatus;
    modalRef.componentInstance.comments = this.comoodityComments;
    modalRef.componentInstance.commType = $type;

    modalRef.result.then((result) => {
      if (result) {
        this.ngOnInit()
      }
    });
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);
  }

  onCommodityChange() {
    this.hasCommodityChanged = true
  }
  containerChange() {
    this.hasContainerChanged = true
  }


  wh_name = false
  wh_contact = false
  wh_email = false

  errorMessagesWH() {

    if (this.warehouseUserInfo.controls.name.status === 'INVALID' && this.warehouseUserInfo.controls.name.touched) {
      this.wh_name = true;
    }
    if (this.warehouseUserInfo.controls.contact.status === 'INVALID' && this.warehouseUserInfo.controls.contact.touched) {
      this.wh_contact = true;
    }
    if (this.warehouseUserInfo.controls.email.status === 'INVALID' && this.warehouseUserInfo.controls.email.touched) {
      this.wh_email = true;
    }
  }

  resetFormWH() {
    this.warehouseUserInfo.reset()
    this.wh_name = false
    this.wh_contact = false
    this.wh_email = false
    try {
      if (this.bookingDetails.WHContactInfo) {
        const { WHContactInfo } = this.bookingDetails
        const { whUsers } = this
        this.selectedWHUser = whUsers.filter(_user => _user.userID === WHContactInfo.UserID)[0]
        const { selectedWHUser } = this
        this.warehouseUserInfo.controls['name'].patchValue(selectedWHUser);
        let selectedCountry = this.countryList.find(obj => obj.code === WHContactInfo.CountryCode);
        this.selectPhoneCodeWH(selectedCountry);
        this.isWhEdit = false
      } else {
        let selectedCountry = this.countryList.find(obj => obj.code.toLowerCase() === 'us');
        this.selectPhoneCodeWH(selectedCountry);
        this.isWhEdit = true
      }
    } catch {
      this.isWhEdit = true
    }

  }


  whUsersTypeAhead = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .map(term => (!term) ? this.whUsers
        : this.whUsers.filter(v => (v.firstName + ' ' + v.lastName).toLowerCase().indexOf(term.toLowerCase()) > -1));

  formatterUser = (_user: IWhUser) => {
    this.selectedWHUser = _user
    // console.log(_user)

    try {
      setTimeout(() => {
        this._wh_name.nativeElement.value = (_user && _user.firstName) ? _user.firstName + ' ' + _user.lastName : ''
      }, 10);
      setTimeout(() => {
        this._wh_email.nativeElement.value = _user.emailAddress ? _user.emailAddress : ''
      }, 10);
      setTimeout(() => {
        this._wh_phone.nativeElement.value = _user.phoneNumber ? _user.phoneNumber : ''
      }, 10);
    } catch (error) {

    }

    if (typeof _user === 'object') {
      let _selectedCountry = this.countryList.find(obj => obj.desc[0].CountryPhoneCode === _user.countryPhoneCode);
      this.selectPhoneCodeWH(_selectedCountry);
    }

    let _toReturn = ''
    if (_user && _user.firstName) {
      _toReturn = _user.firstName + ' ' + _user.lastName
      this.wh_name = false
    } else {
      _toReturn = ''
    }

    return _toReturn
  };


  setCountryListConfig(state) {
    this.countryList = state;
    const userProfile = getLoggedUserData()
    if (!this.bookingDetails || (this.bookingDetails && !this.bookingDetails.WHContactInfo)) {
      let selectedCountry = this.countryList.find(obj => obj.id === userProfile.CountryID);
      this.selectPhoneCodeWH(selectedCountry);
    } else {
      let selectedCountry = this.countryList.find(obj => obj.code === this.bookingDetails.WHContactInfo.CountryCode);
      this.selectPhoneCodeWH(selectedCountry);
    }
  }

  selectPhoneCodeWH(country) {
    try {
      this.countryFlagImage = country.code;
      let description = country.desc;
      this.phoneCode = description[0].CountryPhoneCode;
      this.transPhoneCode = description[0].CountryPhoneCode_OtherLang;
      this.phoneCountryId = country.id
      this.selCountry = country
    } catch (error) {

    }
    // console.log(this.selCountry)
  }

  updateWarehouseContact() {
    if (typeof this.warehouseUserInfo.value.name === 'object') {
      loading(true)
      this._viewBookingService.updateBookingWarehouseContactPerson(this.bookingDetails.BookingID, this.selectedWHUser.userID, getLoggedUserData().UserID).subscribe((res: JsonResponse) => {
        loading(false)
        if (res.returnId > 0) {
          this._toast.success(res.returnText, 'Success')
          this.getBookingDetail(this.bookingId);
        } else {
          this._toast.error(res.returnText, 'Error')
        }
      }, error => {
        this._toast.error('There was an error while processing your request, please try later.', 'Failed')
        loading(false)
      })
    } else {
      this._toast.warning('User must be selected from list', 'Invalid User')
      this.wh_name = true
    }
  }

  public onFocus(e: Event): void {
    e.stopPropagation();
    setTimeout(() => {
      const inputEvent: Event = new Event('input');
      e.target.dispatchEvent(inputEvent);
    }, 0);
  }

  refreshData() {
    this.getBookingDetail(this.bookingId);
  }

  openTermsAndCondid() {
    loading(true)
    const _id = encryptBookingID(this.bookingDetails.BookingID, this.bookingDetails.ProviderID, this.bookingDetails.ShippingModeCode);
    this._commonService.getBookingCustomerTermsCondition((_id)).subscribe((res: JsonResponse) => {
      loading(false)
      if (res.returnId > 0) {
        const _modalData: ConfirmDialogContent = {
          messageTitle: 'Confirm',
          messageContent: `Are you sure you want to confirm?`,
          buttonTitle: 'Yes',
          data: {
            termsCondition: res.returnText,
            action: 'view'
          }
        }

        const modalRef = this._modalService.open(TermsConditDialogComponent, {
          size: 'lg',
          centered: true,
          windowClass: 'large-modal',
          backdrop: 'static',
          keyboard: false
        });

        modalRef.result.then((result: string) => {
          if (result) {
          }
        })

        modalRef.componentInstance.modalData = _modalData;
        setTimeout(() => {
          if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
            document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
          }
        }, 0);
      } else {
        this._toast.warning('Terms and Conditions not available')
      }
    }, error => {
      loading(false)
    })
  }

  setHmCommission() {
    try {
      if (this.bookingDetails.BookingSpecialAmount && this.bookingDetails.BookingSpecialAmount > 0) {
        this.hmCommisssion = this.bookingDetails.BookingSpecialAmount * (this.bookingDetails.HMCommisionRate / 100)
      } else {
        this.hmCommisssion = this.bookingDetails.HMCommisionValue
      }
    } catch {
      this.hmCommisssion = this.bookingDetails.HMCommisionValue
    }
  }

  editAirInfo = false

  openAirFlightTrackingUrl() {
    let _url = ''

    const { FlightTrackingURL } = this.bookingDetails
    const flightData = this.bookingDetails.FlightNo.split('-')

    _url = FlightTrackingURL.replace('{FlightNo}', flightData[1])
    try { _url = _url.replace('{IATACode}', flightData[0]) } catch { }
    // console.log(_url)
    window.open(_url, '_blank')
  }

  setAirlineData() {
    try {
      const _airDate = this.bookingDetails.EtdLcl ? new Date(this.bookingDetails.EtdLcl) : new Date()
      this.departureDate = {
        year: (_airDate.getFullYear()),
        month: moment(_airDate).month() + 1,
        day: moment(_airDate).date()
      };
      this.airDtl_airline = {
        id: this.bookingDetails.CarrierID,
        code: this.bookingDetails.CarrierCode,
        title: this.bookingDetails.CarrierName,
        imageName: this.bookingDetails.CarrierImage,
      }
      if (this.bookingDetails.FlightNo) {
        const flightData = this.bookingDetails.FlightNo.split('-')
        this.airDtl_flight_code = flightData[0]
        this.airDtl_flight_num = flightData[1]
      } else if (this.bookingDetails.CarrierCode && this.bookingDetails.CarrierCode !== 'DEFAULT') {
        this.airDtl_flight_code = this.bookingDetails.CarrierCode
        this.airDtl_flight_num = null
      } else {
        this.airDtl_flight_code = null
        this.airDtl_flight_num = null
      }
    } catch (error) {
      console.log(error)
    }
  }

  editAirlineDetails() {
    this.editAirInfo = !this.editAirInfo
    this.setAirlineData()
  }

  updateAirLineData() {

    if (!this.airDtl_airline || !this.airDtl_airline.id) {
      this._toast.warning('Please Select Airline to update.')
      return
    }

    if (!this.airDtl_flight_code || this.airDtl_flight_code.length !== 2) {
      this._toast.warning('Flight Code should be 2 characters.')
      return
    }

    if (!this.airDtl_flight_num || this.airDtl_flight_num.length > 5) {
      this._toast.warning('Flight Number should be 1-5 characters long.')
      return
    }

    if (!this.departureDate || !this.departureDate.day) {
      this._toast.warning('Please select departure date.')
      return
    }

    const newDate = this.dateParser.format(cloneObject(this.departureDate))
    const toSend = {
      CarrierScheduleID: -1,
      CarrierID: this.airDtl_airline.id,
      CarrierName: this.airDtl_airline.title,
      CarrierImage: this.airDtl_airline.imageName,
      TransitDays: 0,
      EtdDate: newDate,
      EtaDate: newDate,
      FlightNo: this.airDtl_flight_code + '-' + this.airDtl_flight_num,
      PolID: this.bookingDetails.PolID,
      PodID: this.bookingDetails.PodID,
      JsonTransferVesselsDetails: '{}',
      bookingID: this.bookingDetails.BookingID
    }
    loading(true)
    this._viewBookingService.saveCarrierSchedule_Air(toSend).subscribe((res: JsonResponse) => {
      loading(false)
      if (res.returnId > 0) {
        this._toast.success('Updated successfully', 'Success')
        this.editAirInfo = false
        if (res.returnObject) {
          this.bookingDetails.CarrierID = this.airDtl_airline.id
          this.bookingDetails.CarrierCode = this.airDtl_airline.code
          this.bookingDetails.CarrierName = this.airDtl_airline.title
          this.bookingDetails.CarrierImage = this.airDtl_airline.imageName
          this.bookingDetails.FlightTrackingURL = res.returnObject[0].flightTrackingURL
          this.bookingDetails.FlightNo = res.returnObject[0].flightNo
          this.bookingDetails.EtdLcl = res.returnObject[0].etdDate
          this.bookingDetails.EtdUtc = res.returnObject[0].etdDate
        }
      } else {
        this._toast.error(res.returnText, res.returnStatus)
      }
    }, error => {
      loading(false)
    })
  }

  isCitySearching: boolean = false;
  hasCitySearchFailed: boolean = false;
  hasCitySearchSuccess: boolean = false;

  search2 = (text$: Observable<string>) =>
    text$
      .debounceTime(300) //debounce time
      .distinctUntilChanged()
      .do(() => {
        this.isCitySearching = true;
        this.hasCitySearchFailed = false;
        this.hasCitySearchSuccess = false;
      }) // do any action while the user is typing
      .mergeMap(term => {
        let some = of([]); //  Initialize the object to return
        if (term && term.length >= 2) {
          //search only if item are more than three
          some = this._commonService
            .getCarrierDropDownDetailAir(term)
            .do(res => {
              this.isCitySearching = false;
              this.hasCitySearchSuccess = true;
              return res;
            })
            .catch(() => {
              this.isCitySearching = false;
              this.hasCitySearchFailed = true;
              return [];
            });
        } else {
          this.isCitySearching = false;
          some = of([]);
        }
        return some;
      })
      .do(res => {
        this.isCitySearching = false;
        return res;
      })
      .catch(() => {
        this.isCitySearching = false;
        return of([]);
      }); // final server list

  onAirlineChange() {
    if (this.airDtl_airline) {
      this.airDtl_flight_code = this.airDtl_airline.code
    }
  }

  airCodeNumValidator($isCode: boolean) {
    if ($isCode) {
      this.airDtl_flight_code = getAlphaNumString(this.airDtl_flight_code)
    } else {
      this.airDtl_flight_num = getAlphaNumString(this.airDtl_flight_num)
    }
  }


  onEditorBlured(quill, type) {
  }

  onEditorFocused(quill, type) {
  }

  onEditorCreated(quill, type) {
  }

  onContentChanged($event, type) {
    if (type === 'agent') {
      this.editorContentAgent = $event.html
    } else {
      this.editorContentConsignee = $event.html
    }
  }

  discardInstructions() {
    this.toggleInfoAgent = false
    if (this.instructionsData) {
      const { Agent, Shipper } = this.instructionsData
      this.editorContentAgent = Agent ? Agent.replaceAll('\n', '<br>') : Agent
      this.editorContentConsignee = Shipper ? Shipper.replaceAll('\n', '<br>') : Shipper
    }
  }

  toggleShiperInfo($type) {
    if ($type === 'agent') {
      if (this.toggleInfoAgent) {
        if (this.editorContentAgent && this.editorContentAgent.length <= 500) {
          this.confirmationDialog('Agent').then(_state => {
            if (_state) {
              this.toggleInfoAgent = false
            }
          })
        } else {
          this._toast.warning('The Agent instruction size should be around 500 characters', 'Limit Exceeded')
        }
      } else {
        const { editorContentAgent } = this
        this.editorContentAgent = editorContentAgent ? editorContentAgent.replaceAll('<br>', '\n') : null
        this.toggleInfoAgent = true
      }
    } else {
      if (this.toggleInfoConsignee) {
        if (this.editorContentConsignee && this.editorContentConsignee.length <= 500) {
          this.confirmationDialog('Shipper').then(_state => {
            if (_state) {
              this.toggleInfoConsignee = false
            }
          })
        } else {
          this._toast.warning('The Consignee instruction size cannot exceed 500 characters', 'Limit Exceeded')
        }
      } else {
        const { editorContentConsignee } = this
        this.editorContentConsignee = editorContentConsignee.replaceAll('<br>', '\n')
        this.toggleInfoConsignee = true
      }
    }
  }

  confirmationDialog($type) {
    return new Promise((resolve, reject) => {
      const _modalData: ConfirmDialogContent = {
        messageTitle: `Update ${$type} Information`,
        messageContent: `Are you sure you would like to update ${$type} Information?`,
        buttonTitle: 'Yes',
        data: null
      }

      const modalRef = this._modalService.open(ConfirmDialogGenComponent, {
        size: 'lg',
        centered: true,
        windowClass: 'medium-modal',
        backdrop: 'static',
        keyboard: false
      });

      modalRef.result.then((result: string) => {
        if (result) {
          const jsonValue = {
            Shipper: this.instructionsData.Shipper,
            Agent: $type.toLowerCase() === 'agent' ? this.editorContentAgent : this.instructionsData.Agent
          }
          const _toSend = {
            bookingID: this.bookingDetails.BookingID,
            shippingMode: this.bookingDetails.ShippingModeCode,
            parameterType: 'SHIPPING_INSTRUCTIONS',
            parameterValue: JSON.stringify(jsonValue),
            providerID: this.bookingDetails.ProviderID,
            userID: this.bookingDetails.UserID,
            isUpdateByProvider: true,
          }
          loading(true)
          this._viewBookingService.updateBookingAdditionalParameters(getLoggedUserData().UserID, _toSend).subscribe((res: JsonResponse) => {
            loading(false)
            if (res.returnId > 0) {
              this._toast.success(`${$type} Infomation Updated Successfully`, 'Success')
              resolve(true)
              // this.setShipInstrucrtions(res.returnObject.parameterValue)
            } else {
              resolve(false)
              this._toast.error(res.returnText, res.returnStatus)
            }
          }, (error: any) => {
            loading(false)
            resolve(false)
            this._toast.error(error.error.ParameterValue[0], 'Failed')
          })
          // resolve(true)
        } else {
          resolve(false)
        }
      })

      modalRef.componentInstance.modalData = _modalData;
      setTimeout(() => {
        if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
          document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
        }
      }, 0);
    })
  }

  getRemainingInsCount() {
    if (this.editorContentAgent) {
      return 500 - this.editorContentAgent.length
    } else {
      return 500
    }
  }

  getLclContainerNumber($bookingKey) {
    this._viewBookingService.getBookingAdditionalParameters($bookingKey, 'LCL_CONTAINER_NO').subscribe((res: JsonResponse) => {
      try {
        const { parameterValue }: IChatResponse = res.returnObject
        if (parameterValue) {
          this.lclContainerNo = JSON.parse(parameterValue)[0].ContainerNo
        }
      } catch { }
    }, error => { })
  }

  getQuoteNumber() {
    try {
      return this.bookingDetails.SpotRateBookingKey.split('|')[0]
    } catch {
      return ''
    }
  }
  openAddSchedule() {
    try {
      const { BookingID, ProviderID, ShippingModeCode } = this.bookingDetails
      this._viewBookingService
        .getBookingRouteDetail(encryptBookingID(BookingID, ProviderID, ShippingModeCode)).subscribe((res: JsonResponse) => {
          if (res.returnId > 0) {
            const modalRef = this._modalService.open(AddScheduleComponent, {
              size: 'lg', centered: true, windowClass: 'schedule-modal', backdrop: 'static', keyboard: false
            })

            const _carrierList = this.searchCarrierList.filter(_ship => _ship.code.toLowerCase() !== 'default')
            modalRef.componentInstance.carrierList = _carrierList
            modalRef.componentInstance.PolID = this.bookingDetails.PolID
            modalRef.componentInstance.PodID = this.bookingDetails.PodID
            modalRef.componentInstance.bookingDetails = this.bookingDetails
            modalRef.componentInstance.bookCarrier = _carrierList.filter(_carrier => _carrier.id === this.bookingDetails.CarrierID)[0]
            modalRef.componentInstance.from = 'VIEW_BOOKING'
            modalRef.componentInstance.schedule = res.returnObject
            modalRef.componentInstance.isEdit = true
            modalRef.result.then(_schedule => {
              if (_schedule) {
                const _toSend = {
                  ...JSON.parse(_schedule.returnObject),
                  bookingID: this.bookingDetails.BookingID
                }
                this._viewBookingService.saveCarrierSchedule(_toSend).subscribe((res: JsonResponse) => {
                  console.log(res)
                  const { returnId, returnText } = res
                  if (returnId > 0) {
                    this.refreshData()
                  } else {
                    this._toast.error(returnText)
                  }
                }, (err: any) => {
                  this._toast.error('There was an error while processing your request, Please try again later.', 'Error')
                  console.log(err)
                })
              }
            })
          }
        })
    } catch (error) {
      console.log(error)
    }
  }

  getChatList(getUserData?: boolean) {
    const _bookKey = encryptBookingID(this.bookingDetails.BookingID, this.bookingDetails.ProviderID, this.bookingDetails.ShippingModeCode)
    this._dashboardService.getChatList(_bookKey, 'OFFLINE_CHAT').subscribe((res: JsonResponse) => {
      if (res.returnId > 0) {
        const { parameterValue }: IChatResponse = res.returnObject
        this.chatResponse = JSON.parse(parameterValue)
      }
    })
  }


  openChat(bookings) {
    const modalRef = this._modalService.open(ChatComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'chat-modal',
      backdrop: 'static',
      keyboard: false
    })
    modalRef.result.then(result => {
      if (!isShareLogin()) {
        this.chatResponse.providerCount = 0
      }
    })
    modalRef.componentInstance.data = { booking: this.bookingDetails, isEnabled: this.isGuestLogin, logs: [] }
    modalRef.componentInstance.from = 'details'
  }

  openPaymentTerms(action) {
    loading(true)
    this._viewBookingService.getBookingPaymentTerms(this.bookingDetails.BookingID).subscribe((res: JsonResponse) => {
      loading(false)
      try {
        if (res.returnId > 0) {
          this.paymentTermsOptions = res.returnObject.filter(_term => _term.IsSelected)
          this.openTermsModal(action)
        } else {
          if (res.returnCode === '2') {
            this._toast.info(res.returnText, res.returnStatus)
          } else {
            this._toast.error(res.returnText, res.returnStatus)
          }
        }
      } catch { }
    }, error => { loading(false) })
  }

  openTermsModal(action) {
    const modalRef = this._modalService.open(PaymentTermsDialogComponent, {
      size: 'lg', centered: true, windowClass: 'termsAndCondition', backdrop: 'static', keyboard: false
    })
    modalRef.componentInstance.paymentTerms = []
    modalRef.componentInstance.paymentTermsOptions = this.paymentTermsOptions
    modalRef.componentInstance.action = action
    modalRef.componentInstance.from = 'RR'
    modalRef.componentInstance.shippingModeCode = this.bookingDetails.ShippingModeCode
    modalRef.componentInstance.request = this.bookingDetails
  }

  toRequestList() {
    this._sharedService.rateRequestId = this.getQuoteNumber()
    SharedService.rateRequestIdV2 = this.getQuoteNumber()
    this._router.navigate(['/provider/rate-requests'])
  }

  getVesselSchedule() {
    this.showSchedule = !this.showSchedule
    if (this.showSchedule) {
      loading(true)
      this._viewBookingService.getBookingRouteDetail(encryptBookingID(this.bookingDetails.BookingID, this.bookingDetails.ProviderID, this.bookingDetails.ShippingModeCode)).subscribe((res: JsonResponse) => {
        loading(false)
        if (res.returnId > 0 && typeof res.returnObject === 'object') {
          setTimeout(() => {
            this.showSchedule = true
            this.bookingSchedule = res.returnObject
          }, 900);
        } else {
          this.showSchedule = false
          this.bookingSchedule = null
        }
      }, error => {
        this.showSchedule = false
      })
    }
  }

  getDatefromObj(dateObject) {
    try {
      return moment(new Date(dateObject.year, dateObject.month - 1, dateObject.day))
    } catch (error) { return null }
  }

  updateActualDate(type) {
    let toSend = null
    if (type === 'atd') {
      if (!this.atdDate || !this.atdDate.day) {
        this._toast.warning('Please enter Actual Time of Departure to proceed')
        return
      }
      const _atdDate = this.getDatefromObj(this.atdDate)
      const _cutOffDate = moment(new Date(this.bookingDetails.PortCutOffLcl))
      const _ataDate = this.ataDate && this.ataDate.day ? this.getDatefromObj(this.ataDate) : null
      if (_atdDate.isSameOrBefore(_cutOffDate)) {
        this._toast.warning('Acutal Time of Departure should be greater than Cut off Date')
        return
      }

      if (_ataDate && _atdDate.isSameOrAfter(_ataDate) && this.bookingDetails.AtaLcl) {
        this._toast.warning('Acutal Time of Departure should be less than Actual Time of Arrival')
        return
      }

      if (!this.bookingDetails.AtaLcl) {
        this.ataDate = null
      }

      toSend = {
        bookingID: this.bookingDetails.BookingID,
        atdUtc: moment(_atdDate).format('YYYY-MM-DD'),
        atdLcl: moment(_atdDate).format('YYYY-MM-DD'),
        ataUtc: this.bookingDetails.AtaUtc,
        ataLcl: this.bookingDetails.AtaLcl
      }

    } else {
      if (!this.atdDate || !this.atdDate.day || !this.bookingDetails.AtdLcl) {
        this._toast.warning('Please enter Actual Time of Departure to proceed')
        return
      }
      if (!this.ataDate || !this.ataDate.day) {
        this._toast.warning('Please enter Actual Time of Arrival to proceed')
        return
      }

      const _atdDate = this.getDatefromObj(this.atdDate)
      const _ataDate = this.getDatefromObj(this.ataDate)

      if (_ataDate.isSameOrBefore(_atdDate) && this.bookingDetails.AtdLcl) {
        this._toast.warning('Acutal Time of Arrival should be greater than Actual Time of Departure')
        return
      }

      toSend = {
        bookingID: this.bookingDetails.BookingID,
        atdUtc: this.bookingDetails.AtdUtc,
        atdLcl: this.bookingDetails.AtdLcl,
        ataUtc: moment(this.getDatefromObj(this.ataDate)).format('YYYY-MM-DD'),
        ataLcl: moment(this.getDatefromObj(this.ataDate)).format('YYYY-MM-DD')
      }
    }
    loading(true)
    this._viewBookingService.updateActual(this.userProfile.UserID, toSend).subscribe((res: JsonResponse) => {
      if (res.returnId > 0) {
        this.editAtd = false
        this.editAta = false
        if (type === 'atd') {
          this._toast.success('Acutal Time of Departure updated successfully', res.returnStatus)
        } else {
          this._toast.success('Acutal Time of Arrival updated successfully', res.returnStatus)
        }
        this.getBookingDetail(this.currentBookingId)
      } else {
        loading(false)
        this._toast.error(res.returnText, res.returnStatus)
      }
    }, error => loading(false))
  }

  removeActualDate(type: string) {
    let toSend = null
    let _confirmText = ''
    if (type === 'atd') {
      if (this.ataDate && this.ataDate.day) {
        this._toast.warning('Cannot remove Acutal Time of Departure when Acutal Time of Arrival is added')
        return
      }
      toSend = {
        bookingID: this.bookingDetails.BookingID,
        atdLcl: null,
        atdUtc: null,
        ataLcl: null,
        ataUtc: null
      }
      _confirmText = 'Are you sure you want remove Acutal Time of Departure from this booking?'
    } else {
      toSend = {
        bookingID: this.bookingDetails.BookingID,
        atdLcl: this.bookingDetails.AtdUtc,
        atdUtc: this.bookingDetails.AtdLcl,
        ataLcl: null,
        ataUtc: null
      }
      _confirmText = 'Are you sure you want remove Acutal Time of Arrival from this booking?'
    }

    const _modalData: ConfirmDialogContent = {
      messageTitle: `Remove ${type.toUpperCase()} Date`,
      messageContent: _confirmText,
      buttonTitle: 'Yes',
      data: null
    }

    const modalRef = this._modalService.open(ConfirmDialogGenComponent, {
      size: 'lg',
      centered: true,
      windowClass: 'medium-modal',
      backdrop: 'static',
      keyboard: false
    });

    modalRef.result.then((result: string) => {
      if (result) {
        loading(true)
        this._viewBookingService.updateActual(this.userProfile.UserID, toSend).subscribe((res: JsonResponse) => {
          if (res.returnId > 0) {
            this.editAtd = false
            this.editAta = false
            if (type === 'atd') {
              this._toast.success('Acutal Time of Departure removed successfully', res.returnStatus)
            } else {
              this._toast.success('Acutal Time of Arrival removed successfully', res.returnStatus)
            }
            this.getBookingDetail(this.currentBookingId)
          } else {
            loading(false)
            this._toast.error(res.returnText, res.returnStatus)
          }
        }, () => loading(false))
      }

    })

    modalRef.componentInstance.modalData = _modalData;
    setTimeout(() => {
      if (document.getElementsByTagName('body')[0].classList.contains('modal-open')) {
        document.getElementsByTagName('html')[0].style.overflowY = 'hidden';
      }
    }, 0);


  }
}

export interface IDocType {
  documentTypeID: number;
  documentTypeCode: string;
  documentTypeName: string;
  businessLogic?: string;
  isDefaultDocument?: boolean;
}

export interface ChargesDetail {
  documentTypeID: number;
  documentTypeName: string;
  providerPrice: number;
  basePrice: number;
  price: number;
}

export interface PmexCharges {
  priceBasis: string;
  providerCurrencyID: number;
  providerCurrencyCode: string;
  totalProviderPrice: number;
  baseCurrencyID: number;
  baseCurrencyCode: string;
  totalBasePrice: number;
  baseExchangeRate: number;
  currencyID: number;
  currencyCode: string;
  totalPrice: number;
  exchangeRate: number;
  chargesDetail: ChargesDetail[];
}


export interface IBookingInpectionDetail {
  BookingInspectionID: any;
  ScheduleDateTime: string;
  InspectionNature: string;
  InspectorName: string;
  InspectorMobileNo: string;
  CountryPhoneCode: string;
  CountryCode: string;
  InspectionStatus: string;
  InspectionComments?: any;
  InspectionHistoryJson: string;
  parsednspectionHistory?: IInspectionHistoryJson[];
  AcceptedBy?: any
  AcceptedByUserID?: any
  AcceptedStatus?: any
  AcceptedStatusDateTime?: any
}

export interface IInspectionHistoryJson {
  ScheduleDateTime: string;
  ScheduleDateTimeUtc: string;
  ScheduleByUserID: string;
  ScheduleByUserName: string;
  InspectorName: string;
  CountryID: number;
  CountryPhoneCode: string;
  CountryCode: string;
  InspectorMobileNo: string;
  InspectorEmail?: any;
  InspectionStatus: string;
  InspectionComments: string;
  InspectionNature: string;
  ModifiedDateTime: string;
  InspectionStatusDateTime: string;
  InspectionStatusUpdatedByUserID: string;
  InspectionStatusUpdatedByUserName: string;
  AcceptedBy?: any;
  AcceptedByUserID?: any;
  AcceptedByUserName?: any;
  AcceptedStatus?: any;
  AcceptedStatusDateTime?: any;
  StatusUpdatedByUserName: string;
  StatusUpdatedDateTime: string;
  ScheduleBy: string;
}

export interface IContainerHistory {
  ScheduleDateTime: string;
  InspectionNature: string;
  InspectorName: string;
  InspectorMobileNo: string;
  CountryPhoneCode: string;
  CountryCode: string;
  InspectorEmail?: any;
  InspectionStatus: string;
  InspectionComments?: any;
  CreatedDateTime: string;
  ModifiedDateTime?: any;
}
