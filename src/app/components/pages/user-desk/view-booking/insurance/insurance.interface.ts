
export interface IFormControl {
    icon?: string;
    controlType: string;
    controlName: string;
    label: string;
    hasError: boolean;
    isEditable: boolean;
    validation: any[]
}
export interface IPolicyHolder {
    PHName: string;
    PHAddress: string;
    PHContactPerson: string;
    PHTelephoneNo: string;
    PHCountryID: number;
    PHCountryPhoneCode: string;
    PHEmail: string;
    PHNTNNo: string;
    ItemCondition: string;
    PHShipperName: string;
    PHBankName: string;
    PHBranchName: string;
    PHBranchManager: string;
    TruckBiltyNo?: any;
    TruckRegdNo?: any;
    DispatchDate?: any;
    IsGuardDeputed: boolean;
    IsArmed: boolean;
    IsFireExtinguishers: boolean;
    WarehouseConstruction?: any;
    WarehouseOwnershipType?: any;
    SumCoveredCurrID: number;
    SumCovered: number;
    SumCoveredCurrCode: string;
    ExportingCountryID: number;
    ExportingCountryCode: string;
    ExportingCountry: string;
    ImportingCountryID: number;
    ImportingCountryCode: string;
    ImportingCountry: string;
    PolicyExpiry?: any;
    Imp_Exp: string;
    PolicyName: string;
    InsurancePolicyMode: string;
    CommodityDescription: string;
    PolicyExpiryDate?: string;
}
export interface IInsuranceUpdate {
    PHName: string;
    PHAddress: string;
    PHContactPerson: string;
    PHTelephoneNo: string;
    PHCountryID: number;
    PHCountryPhoneCode: string;
    PHEmail: string;
    PHNTNNo: string;
    ItemCondition: string;
    PHShipperName: string;
    PHBankName: string;
    PHBranchName: string;
    PHBranchManager: string;
    PolicyExpiryDate: string;
    DispatchDate: string;
    TruckBiltyNo: string;
    TruckRegdNo: string;
    IsGuardDeputed: boolean;
    IsArmed: boolean;
    IsFireExtinguishers: boolean;
    WarehouseConstruction: string;
    WarehouseOwnershipType: string;
}