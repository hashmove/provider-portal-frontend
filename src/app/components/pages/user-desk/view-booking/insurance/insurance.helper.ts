import { Validators } from "@angular/forms";
import { NgbDate } from "@ng-bootstrap/ng-bootstrap/datepicker/ngb-date";
import { EMAIL_REGEX } from "../../../../../constants/globalFunctions";
import { IFormControl } from "./insurance.interface";

export class InsuranceHelper {
    exportForm: IFormControl[] = [
        { controlType: 'text', controlName: 'PolicyName', label: "Policy Name", hasError: false, isEditable: false, validation: [] },
        { controlType: 'text', controlName: 'PHName', label: "Client Name", hasError: false, isEditable: true, validation: [Validators.required] },
        { controlType: 'text', controlName: 'PHAddress', label: "Client Address", hasError: false, isEditable: true, validation: [Validators.required] },
        { controlType: 'text', controlName: 'PHContactPerson', label: "Client's Contact Persona Name", hasError: false, isEditable: true, validation: [Validators.required] },
        {
            controlType: 'phone', controlName: 'PHTelephoneNo', label: "TELEPHONE NUMBER", hasError: false, isEditable: true, validation: [
                Validators.required,
                Validators.pattern(/^(?!(\d)\1+(?:\1+){0}$)\d+(\d+){0}$/),
                Validators.minLength(7),
                Validators.maxLength(13)]
        },
        {
            controlType: 'text', controlName: 'PHEmail', label: "Email Address", hasError: false, isEditable: true, validation: [
                Validators.required,
                Validators.pattern(EMAIL_REGEX),
                Validators.maxLength(320)
            ]
        },
        { controlType: 'text', controlName: 'PHNTNNo', label: "NTN Number", hasError: false, isEditable: true, validation: [Validators.required] },
        { controlType: 'text', controlName: 'PHBankName', label: "Bank Name", hasError: false, isEditable: true, validation: [Validators.required] },
        { controlType: 'text', controlName: 'PHBranchName', label: "Branch Name", hasError: false, isEditable: true, validation: [Validators.required] },
        { controlType: 'text', controlName: 'PHBranchManager', label: "Branch Manager", hasError: false, isEditable: true, validation: [Validators.required] },
        { controlType: 'text', controlName: 'CommodityDescription', label: "ITEM / COMMODIT DESCRIPTION", hasError: false, isEditable: false, validation: [] },
        { controlType: 'text', controlName: 'ItemCondition', label: "NEW OR USED ITEM", hasError: false, isEditable: false, validation: [] },
        { controlType: 'text', controlName: 'SumCovered', label: "SUM COVERED", hasError: false, isEditable: false, validation: [] },
        { controlType: 'text', controlName: 'imp_exp', label: "EXPORTING FROM PAKISTAN TO", hasError: false, isEditable: false, validation: [] },
        { controlType: 'text', controlName: 'InsurancePolicyMode', label: "MODE OF TRANSPORT", hasError: false, isEditable: false, validation: [] },
        { controlType: 'date', controlName: 'PolicyExpiry', label: "Policy Expiration Date", hasError: false, isEditable: true, validation: [Validators.required] }
    ]

    inLandForm: IFormControl[] = [
        { controlType: 'text', controlName: 'PolicyName', label: "Policy Name", hasError: false, isEditable: false, validation: [] },
        { controlType: 'text', controlName: 'PHName', label: "Client Name", hasError: false, isEditable: true, validation: [Validators.required] },
        { controlType: 'text', controlName: 'PHAddress', label: "Client Address", hasError: false, isEditable: true, validation: [Validators.required] },
        { controlType: 'text', controlName: 'PHContactPerson', label: "Client's Contact Person Name", hasError: false, isEditable: true, validation: [Validators.required] },
        {
            controlType: 'phone', controlName: 'PHTelephoneNo', label: "Telephone No", hasError: false, isEditable: true, validation: [
                Validators.required,
                Validators.pattern(/^(?!(\d)\1+(?:\1+){0}$)\d+(\d+){0}$/),
                Validators.minLength(7),
                Validators.maxLength(13)]
        },
        {
            controlType: 'text', controlName: 'PHEmail', label: "Email Id ", hasError: false, isEditable: true, validation: [
                Validators.required,
                Validators.pattern(EMAIL_REGEX),
                Validators.maxLength(320)
            ]
        },
        { controlType: 'text', controlName: 'PHNTNNo', label: "NTN No", hasError: false, isEditable: true, validation: [Validators.required] },
        { controlType: 'text', controlName: 'CommodityDescription', label: "Item / Commodity description ", hasError: false, isEditable: false, validation: [] },
        { controlType: 'text', controlName: 'ItemCondition', label: "New or Used Item", hasError: false, isEditable: false, validation: [] },
        { controlType: 'text', controlName: 'SumCovered', label: "Sum covered", hasError: false, isEditable: false, validation: [] },
        { controlType: 'text', controlName: 'ShipperName', label: "Shipper Name", hasError: false, isEditable: true, validation: [Validators.required] },
        { controlType: 'text', controlName: 'InsurancePolicyMode', label: "Mode of Transport  ", hasError: false, isEditable: false, validation: [] },
        { controlType: 'date', controlName: 'PolicyExpiry', label: "Policy Expiration Date", hasError: false, isEditable: true, validation: [Validators.required] }
    ]

    warehouseForm: IFormControl[] = [
        { controlType: 'text', controlName: 'PolicyName', label: "Policy Name", hasError: false, isEditable: false, validation: [] },
        { controlType: 'text', controlName: 'PHName', label: "Client Name", hasError: false, isEditable: true, validation: [Validators.required] },
        { controlType: 'text', controlName: 'PHAddress', label: "Client Address", hasError: false, isEditable: true, validation: [Validators.required] },
        {
            controlType: 'phone', controlName: 'PHTelephoneNo', label: "Telephone No", hasError: false, isEditable: true, validation: [
                Validators.required,
                Validators.pattern(/^(?!(\d)\1+(?:\1+){0}$)\d+(\d+){0}$/),
                Validators.minLength(7),
                Validators.maxLength(13)]
        },
        {
            controlType: 'text', controlName: 'PHEmail', label: "Email Id ", hasError: false, isEditable: true, validation: [
                Validators.required,
                Validators.pattern(EMAIL_REGEX),
                Validators.maxLength(320)
            ]
        },
        { controlType: 'text', controlName: 'PHNTNNo', label: "NTN No", hasError: false, isEditable: true, validation: [Validators.required] },
        { controlType: 'text', controlName: 'WarehouseConstruction', label: "Class of Construction", hasError: false, isEditable: false, validation: [] },
        { controlType: 'text', controlName: 'WarehouseOwnershipType', label: "Type", hasError: false, isEditable: false, validation: [] },
        { controlType: 'text', controlName: 'CommodityDescription', label: "Description of stock", hasError: false, isEditable: false, validation: [] },
        { controlType: 'text', controlName: 'SumCovered', label: "Sum Covered", hasError: false, isEditable: false, validation: [] },
        { controlType: 'text', controlName: 'IsGuardDeputed', label: "Guard deputed 24 hours", hasError: false, isEditable: false, validation: [] },
        { controlType: 'text', controlName: 'IsFireExtinguishers', label: "Fire Extinguishers Installed", hasError: false, isEditable: false, validation: [] },
        { controlType: 'date', controlName: 'PolicyExpiry', label: "Policy Expiration Date", hasError: false, isEditable: true, validation: [Validators.required] }
    ];

    NumberValid(evt) {
        let charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 37 && charCode != 39 && charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    getDateStr($parserFormatter, $date: NgbDate): string {
        try {
            return $parserFormatter.format($date);
        } catch {
            return null
        }
    }
}