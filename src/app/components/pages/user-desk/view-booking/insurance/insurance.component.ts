import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core'
import { FormControl, FormGroup } from '@angular/forms'
import { NgbDateParserFormatter, NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap'
import { ToastrService } from 'ngx-toastr'
import { getLoggedUserData, loading } from '../../../../../constants/globalFunctions'
import { NgbDateFRParserFormatter } from '../../../../../constants/ngb-date-parser-formatter'
import { JsonResponse } from '../../../../../interfaces/JsonResponse'
import { CountryDropdown } from '../../../../../shared/country-select/country-select.component'
import { cloneObject } from '../../reports/reports.component'
import { ViewBookingService } from '../view-booking.service'
import { InsuranceHelper } from './insurance.helper'
import { IFormControl, IPolicyHolder, IInsuranceUpdate } from './insurance.interface'

@Component({
    selector: 'app-insurance',
    templateUrl: './insurance.component.html',
    providers: [
        { provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter }, NgbDropdownConfig
    ],
    styleUrls: ['./insurance.component.scss']
})
export class InsuranceComponent extends InsuranceHelper implements OnInit, OnChanges {
    @Input() bookingDetails
    @Input() refreshSummary
    @Input() isGuestLogin
    @Input() countryList: CountryDropdown[] = [];
    @Output() refreshBooking = new EventEmitter<any>();
    editInsurance: boolean = false
    insuranceForm: FormGroup
    selectPhoneCountry: CountryDropdown
    currentFormList: IFormControl[] = []
    minDate: any
    policyHolder: IPolicyHolder

    constructor(
        private _bookingService: ViewBookingService,
        private _toastr: ToastrService,
        private _parserFormatter: NgbDateParserFormatter,
    ) {
        super()
    }

    ngOnInit() {
        let _date = new Date();
        this.minDate = { year: _date.getFullYear(), month: _date.getMonth() + 1, day: _date.getDate() };
        this.setForm()
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.bookingDetails) {
            setTimeout(() => {
                this.resetForm()
            }, 0);
        }
    }

    setForm() {
        this.policyHolder = this.bookingDetails.PolicyHolder
        const { InsurancePolicyMode } = this.bookingDetails
        const { policyHolder } = this
        this.insuranceForm = new FormGroup({})
        switch (InsurancePolicyMode) {
            case 'SEA':
                const { exportForm } = this
                this.currentFormList = cloneObject(exportForm)
                exportForm.forEach(_control => {
                    this.insuranceForm.addControl(_control.controlName, new FormControl(this.setFormValue(_control, policyHolder), _control.validation))
                })
                this.setImpExp(policyHolder)
                break;
            case 'WAREHOUSE':
                const { warehouseForm } = this
                this.currentFormList = cloneObject(warehouseForm)
                warehouseForm.forEach(_control => {
                    this.insuranceForm.addControl(_control.controlName, new FormControl(this.setFormValue(_control, policyHolder), _control.validation))
                })
                break;
            case 'GROUND':
                const { inLandForm } = this
                this.currentFormList = cloneObject(inLandForm)
                inLandForm.forEach(_control => {
                    this.insuranceForm.addControl(_control.controlName, new FormControl(this.setFormValue(_control, policyHolder), _control.validation))
                })
                break;
            default:
                break;
        }
    }

    setFormValue($control: IFormControl, $policyHolder: IPolicyHolder) {
        try {
            if ($control.controlType === 'date') {
                const _policyDate = new Date($policyHolder[$control.controlName])
                return { month: _policyDate.getMonth() + 1, day: _policyDate.getDate(), year: _policyDate.getFullYear() }
            } else if ($control.controlType === 'phone') {
                this.selectPhoneCountry = this.countryList.find(obj => obj.id == $policyHolder.PHCountryID);
                console.log(this.selectPhoneCountry)
            }
            return $policyHolder[$control.controlName]
        } catch (error) {
            console.log('insuranceFormSet:', error)
            return null
        }
    }

    setImpExp(_policyHolder: IPolicyHolder) {
        if (_policyHolder.ExportingCountry && _policyHolder.ImportingCountry) {
            const { ExportingCountry, ImportingCountry, } = _policyHolder
            this.currentFormList.forEach(_control => {
                if (_control.controlName === 'imp_exp') {
                    _control.label = `EXPORTING FROM ${ExportingCountry} TO`
                    this.insuranceForm.controls[_control.controlName].setValue(ImportingCountry)
                }
            })
        }
    }

    validate($control: IFormControl, onlyCheck?: boolean) {
        if (this.insuranceForm.controls[$control.controlName].status === "INVALID" && (this.insuranceForm.controls[$control.controlName].touched || onlyCheck)) {
            $control.hasError = true
            this.insuranceForm.controls[$control.controlName].markAsDirty()
            this.insuranceForm.controls[$control.controlName].markAsTouched()
        }
    }

    selectPhoneCode(country) {
        this.selectPhoneCountry = country
    }

    onDateSelect($event, $control: IFormControl) { }

    resetForm() {
        this.editInsurance = false
        this.setForm()
    }

    updateInsurance() {
        if (this.insuranceForm.invalid) {
            this.currentFormList.forEach(_formControl => {
                this.validate(_formControl, true)
            })
            return
        }
        console.log(this.insuranceForm.value)
        const _insuranceData: IInsuranceUpdate = {
            PHName: this.getFormValue('PHName'),
            PHAddress: this.getFormValue('PHAddress'),
            PHContactPerson: this.getFormValue('PHContactPerson'),
            PHTelephoneNo: this.getFormValue('PHTelephoneNo'),
            PHCountryPhoneCode: this.selectPhoneCountry.desc[0].CountryPhoneCode,
            PHCountryID: this.selectPhoneCountry.id,
            PHEmail: this.getFormValue('PHEmail'),
            PHNTNNo: this.getFormValue('PHNTNNo'),
            ItemCondition: this.getFormValue('ItemCondition'),
            PHShipperName: this.getFormValue('PHShipperName'),
            PHBankName: this.getFormValue('PHBankName'),
            PHBranchName: this.getFormValue('PHBranchName'),
            PHBranchManager: this.getFormValue('PHBranchManager'),
            PolicyExpiryDate: this.getDateStr(this._parserFormatter, this.getFormValue('PolicyExpiry')),
            DispatchDate: this.getFormValue('DispatchDate'),
            TruckBiltyNo: this.getFormValue('TruckBiltyNo'),
            TruckRegdNo: this.getFormValue('TruckRegdNo'),
            IsGuardDeputed: this.getFormValue('IsGuardDeputed'),
            IsArmed: this.getFormValue('IsArmed'),
            IsFireExtinguishers: this.getFormValue('IsFireExtinguishers'),
            WarehouseConstruction: this.getFormValue('WarehouseConstruction'),
            WarehouseOwnershipType: this.getFormValue('WarehouseOwnershipType'),
        }
        const _toSend = {
            bookingID: this.bookingDetails.BookingID,
            shippingMode: "INSURACNCE",
            parameterType: "POLICY_HOLDER",
            parameterValue: JSON.stringify(_insuranceData),
            providerID: this.bookingDetails.ProviderID,
            userID: this.bookingDetails.UserID
        }
        loading(true)
        this._bookingService.updateBookingAdditionalParameters(getLoggedUserData().UserID, _toSend).subscribe((res: JsonResponse) => {
            if (res.returnId > 0) {
                this._toastr.success('Information Updated Successfully', res.returnStatus)
                this.editInsurance = false
                this.refreshBooking.emit(true)
            } else {
                this._toastr.error('Information Updated Successfully', res.returnStatus)
            }
        }, error => {
            loading(false)
        })
    }

    getFormValue($key: string) {
        return this.insuranceForm.value[$key] ?
            this.insuranceForm.value[$key] : this.policyHolder[$key] ? this.policyHolder[$key] : ""
    }

    getDateFromObject($date) {
        return this.getDateStr(this._parserFormatter, $date)
    }
}
