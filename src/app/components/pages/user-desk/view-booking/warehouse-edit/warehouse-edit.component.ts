
import { Component, OnInit, ViewEncapsulation, OnDestroy, Input, EventEmitter, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { NgbDateParserFormatter, NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { getLoggedUserData, loading } from '../../../../../constants/globalFunctions';
import { NgbDateFRParserFormatter } from '../../../../../constants/ngb-date-parser-formatter';
import { JsonResponse } from '../../../../../interfaces/JsonResponse';
import { IFormControl } from '../insurance/insurance.interface';
import { ViewBookingService } from '../view-booking.service';
import { WarehouseEditHelper } from './warehouse-edit.helper';
import { IWarehouseData } from './warehouse-edit.interface';
@Component({
  selector: 'app-warehouse-edit',
  templateUrl: './warehouse-edit.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [
    { provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter }, NgbDropdownConfig
  ],
  styleUrls: ['./warehouse-edit.component.scss']
})
export class WarehouseEdit extends WarehouseEditHelper implements OnInit, OnDestroy {

  @Input() bookingDetails = null
  @Input() wareHouse: IWarehouseData
  @Input() isGuestLogin: boolean
  @Input() searchCriteria
  @Output() onWarehouseEdit = new EventEmitter<any>()
  isWarehouseEdit: boolean = false
  warehouseEditForm

  constructor(
    private viewBookingService: ViewBookingService,
    private _toastr: ToastrService,
    private _parserFormatter: NgbDateParserFormatter,
  ) {
    super()
  }

  ngOnInit() {
    this.setForm()
  }

  setForm() {
    this.warehouseEditForm = new FormGroup({})
    const { whFormControls, wareHouse } = this
    whFormControls.forEach(_control => {
      this.warehouseEditForm.addControl(_control.controlName,
        new FormControl({ value: this.setFormValue(_control, wareHouse), disabled: !_control.isEditable }, _control.validation))
    })
  }

  setFormValue($control: IFormControl, $wareHouse: IWarehouseData) {
    try {
      if ($control.controlType === 'date') {
        const _whDate = new Date($wareHouse[$control.controlName])
        return { month: _whDate.getMonth() + 1, day: _whDate.getDate(), year: _whDate.getFullYear() }
      }
      if ($control.controlName === 'BookedSpace') {
        return this.searchCriteria.searchBy === 'by_container' ? this.searchCriteria.CONT : $wareHouse.bookedArea
      }
      if ($control.controlName === 'StorageType') {
        return $wareHouse.storageType
      }
    } catch (error) {
      console.log('whEditForm:', error)
      return null
    }
  }

  resetForm() {
    this.isWarehouseEdit = false
    this.setForm()
  }

  toggleWHEdit() {
    this.isWarehouseEdit = !this.isWarehouseEdit
  }

  validate($control: IFormControl, onlyCheck?: boolean) {
    if (this.warehouseEditForm.controls[$control.controlName].status === "INVALID" && (this.warehouseEditForm.controls[$control.controlName].touched || onlyCheck)) {
      $control.hasError = true
      this.warehouseEditForm.controls[$control.controlName].markAsDirty()
      this.warehouseEditForm.controls[$control.controlName].markAsTouched()
    }
  }

  onDateSelect($event, $control: IFormControl) { }

  updateWarehouse() {
    const toSend = {
      bookingID: this.bookingDetails.BookingID,
      userID: this.bookingDetails.UserID,
      providerID: this.bookingDetails.ProviderID,
      comments: ' ',
      storedFrom: this.getFormValue('StoreFrom'),
      storedUntil: this.getDateStr(this._parserFormatter, this.getFormValue('StoreUntill')),
    }
    loading(true)
    this.viewBookingService.updateWarehouseBookingSchedule(
      getLoggedUserData().UserID, toSend)
      .subscribe((res: JsonResponse) => {
        loading(false)
        if (res.returnId > 0) {
          this._toastr.success('Warehouse Booked Till date updated Successfully.', res.returnStatus)
          this.onWarehouseEdit.emit(true)
          this.wareHouse.StoreUntill = toSend.storedUntil
          this.resetForm()
        } else {
          this._toastr.error(res.returnText, res.returnStatus)
        }
      }, error => {
        this._toastr.error('There was an error while processing your request, please try later.', 'Failed')
        loading(false)
      })
  }

  getFormValue($key: string) {
    console.log(this.warehouseEditForm.value[$key])
    return this.warehouseEditForm.value[$key] ?
      this.warehouseEditForm.value[$key] : this.wareHouse[$key] ? this.wareHouse[$key] : ""
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.bookingDetails) {
        setTimeout(() => {
            this.resetForm()
        }, 0);
    }
}

  ngOnDestroy() {

  }
}
