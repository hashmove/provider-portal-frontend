interface IParsedGallery {
    src: string;
    thumb: string;
    DocumentUploadedFileType: any;
    DocumentFile: any;
}

interface IFacilitiesProviding {
    FacilitiesTypeID: number;
    FacilitiesTypeTitle: string;
    BusinessLogic: string;
    IsAllowed: boolean;
}

export interface IWarehouseData {
    Location: string;
    parsedGallery: IParsedGallery[];
    FacilitiesProviding: IFacilitiesProviding[];
    WHName: string;
    WHDesc: string;
    TotalCoveredArea: number;
    storageType: string;
    StoreFrom: string;
    StoreUntill: string;
    TotalCoveredAreaUnit: string;
    bookedArea: number;
    spaceReqWarehouse: string;
    minimumLeaseTerm: any;
}
