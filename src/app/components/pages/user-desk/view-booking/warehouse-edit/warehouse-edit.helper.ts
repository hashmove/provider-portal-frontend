import { Validators } from "@angular/forms";
import { NgbDate } from "@ng-bootstrap/ng-bootstrap/datepicker/ngb-date";
import { IFormControl } from "../insurance/insurance.interface";

export class WarehouseEditHelper {
    whFormControls: IFormControl[] = [
        { controlType: 'date', controlName: 'StoreFrom', label: "Booked From", icon: 'dateIcon', hasError: false, isEditable: false, validation: [] },
        { controlType: 'date', controlName: 'StoreUntill', label: "Booked Until", icon: 'dateIcon', hasError: false, isEditable: true, validation: [Validators.required] },
        { controlType: 'text', controlName: 'BookedSpace', label: "Booked Space", icon: 'wh-areaIcon', hasError: false, isEditable: false, validation: [] },
        { controlType: 'text', controlName: 'StorageType', label: "Storage Type", icon: 'wh-spaceIcon', hasError: false, isEditable: false, validation: [] },
    ]
    getDateStr($parserFormatter, $date: NgbDate): string {
        try {
            return $parserFormatter.format($date);
        } catch {
            return null
        }
    }
}