import { Component, OnInit, OnChanges, SimpleChanges, Input, OnDestroy } from '@angular/core'
import { PaginationInstance } from 'ngx-pagination'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { ViewBookingService } from '../view-booking/view-booking.service'
import { IInspectionHistoryJson } from '../view-booking/view-booking.component'
@Component({
  selector: 'app-assayer-warehouse-table',
  templateUrl: './assayer-warehouse-table.component.html',
  styleUrls: ['./assayer-warehouse-table.component.scss']
})
export class AssayerWarhouseTableComponent implements OnInit, OnChanges, OnDestroy {

  @Input() data: IInspectionHistoryJson[] = []
  public selectedSort: any = {
    title: 'Rate For',
    value: 'CustomerName',
    column: 'CustomerID'
  }
  public maxSize: number = 7
  public directionLinks: boolean = true
  public autoHide: boolean = false
  public checkAllPublish: boolean
  public checkAllDrafts: boolean = false
  public LCLRecords: number
  public FCLRecords: number
  public page: number = 1
  public thList: Array<any> = [
    { title: 'DATE / TIME', activeClass: '', sortKey: 'CustomerName' },
    { title: 'ASSAYER DETAILS', activeClass: '', sortKey: 'CustomerName' },
    { title: 'STATUS', activeClass: '', sortKey: 'CustomerName' },
    { title: 'COMMENTS', activeClass: '', sortKey: 'CustomerName' },
    { title: 'UPDATED  BY', activeClass: '', sortKey: 'CustomerName' },
    { title: 'MODIFIED DATE', activeClass: '', sortKey: 'CustomerName' },
    { title: 'SCHEDULE BY', activeClass: '', sortKey: 'CustomerName' },
  ]

  public devicPageConfig: PaginationInstance = {
    id: 'dispatchSummary',
    itemsPerPage: 5,
    currentPage: 1
  }

  public labels: any = {
    previousLabel: '',
    nextLabel: '',
    screenReaderPaginationLabel: 'Pagination',
    screenReaderPageLabel: 'page',
    screenReaderCurrentLabel: `You're on page`
  }

  public totalCount: number
  public airFreightTypes: any[] = []
  public isVirtualAirline: boolean = false

  pgNum1 = 1
  pgNum2 = this.devicPageConfig.itemsPerPage

  constructor() { }

  ngOnDestroy(): void { }

  ngOnInit() { }

  ngOnChanges(changes: SimpleChanges): void { }

  getTotalPages = (pages, from) => Math.ceil(pages / this.devicPageConfig.itemsPerPage)

  onPageChange(number, from) {
    this.devicPageConfig.currentPage = number
    this.pgNum2 = this.devicPageConfig.itemsPerPage
  }

}
