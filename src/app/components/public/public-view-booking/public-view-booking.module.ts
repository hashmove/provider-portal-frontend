import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../../../shared/shared.module';
import { UiSwitchModule } from 'ngx-toggle-switch';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipeModule } from '../../../constants/pipe/pipe.module';
import { ScrollbarModule } from 'ngx-scrollbar';
import { AgmCoreModule } from '@agm/core';
import { PublicViewBookingComponent } from './public-view-booking.component';
import { ViewBookingComponent } from '../../pages/user-desk/view-booking/view-booking.component';
import { ViewBookingService } from '../../pages/user-desk/view-booking/view-booking.service';
import { DashboardService } from '../../pages/user-desk/dashboard/dashboard.service';
import { ViaPortComponent } from '../../pages/user-desk/via-ports/via-port.component';
import { TrackingMonitoringComponent } from '../../pages/user-desk/tracking/tracking-monitoring/tracking-monitoring.component';
import { TrackingOverviewComponent } from '../../pages/user-desk/tracking/tracking-overview/tracking-overview.component';
import { TrackingContainerDetailComponent } from '../../pages/user-desk/tracking/tracking-container-detail/tracking-container-detail.component';
import { ViewBookingTableComponent } from '../../pages/user-desk/tracking/view-booking-table/view-booking-table.component';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgxPaginationModule } from 'ngx-pagination';
import { QuillEditorModule } from 'ngx-quill-editor';
import { InsuranceComponent } from '../../pages/user-desk/view-booking/insurance/insurance.component';
import { WarehouseEdit } from '../../pages/user-desk/view-booking/warehouse-edit/warehouse-edit.component';
import { ScheduleCardComponent } from '../../pages/user-desk/schedule-card/schedule-card.component';

export const routes = [
  {
    path: '', component: PublicViewBookingComponent,
    children: [
      // { path: '', redirectTo: 'dashboard', pathMatch: 'full'},
      { path: '', component: ViewBookingComponent },
    ]
  },
];

@NgModule({
  imports: [
    CommonModule,
    NgbModule.forRoot(),
    FormsModule,
    QuillEditorModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(routes),
    UiSwitchModule,
    PipeModule,
    ScrollbarModule,
    NgxPaginationModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBs4tYXYGUA2kDvELgCYcbhYeoVgZCxumg',
      libraries: ["places", "geometry"]
    }),
    NgxEchartsModule,
  ],
  declarations: [
    PublicViewBookingComponent,
    ViewBookingComponent,
    ScheduleCardComponent,
    TrackingMonitoringComponent,
    TrackingOverviewComponent,
    TrackingContainerDetailComponent,
    ViewBookingTableComponent,
    InsuranceComponent,
    WarehouseEdit
    // ViaPortComponent
  ],
  providers: [
    ViewBookingService,
    DashboardService
  ]
})
export class PublicViewBookingModule { }
