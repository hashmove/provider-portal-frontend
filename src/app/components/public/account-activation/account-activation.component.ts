import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IObjectRights, JsonResponse } from '../../../interfaces';
import { AESModel, decryptStringAES, encryptStringAES, getDefaultHMUser, GuestService } from '../../../services/jwt.injectable';
import { UserCreationService } from '../../pages/user-creation/user-creation.service';
import { getImagePath, ImageRequiredSize, ImageSource, loading, matchOtherValidator, PASSWORD_REGEX, validatePassword } from '../../../constants/globalFunctions';

@Component({
  selector: 'app-account-activation',
  templateUrl: './account-activation.component.html',
  styleUrls: ['./account-activation.component.scss'],
  providers: [UserCreationService]
})
export class AccountActivationComponent implements OnInit, OnDestroy {

  updatePassword: FormGroup
  public isNewPass: boolean = false;
  public confirmPass: boolean = false;
  public requiredFields = "This field is required!";
  customerSettings
  headingColor: string = '#ffffff'
  isUpdating = false
  userData: IObjectRights
  isVerified = false
  isVerifiedByForm = false
  isLinkExpred = false
  hasVeriLinkSent = false
  newPasswordStatus = {
    lengthValid: false, upperCaseValid: false, lowerCaseValid: false,
    digitValid: false, specialCharValid: false
  }
  showPassValidator = false
  newPass


  constructor(
    private _activeRouter: ActivatedRoute,
    private _authService: UserCreationService,
    private _toastr: ToastrService,
    private _router: Router,
  ) { }

  ngOnInit() {
    // try { localStorage.removeItem('userInfo') } catch { }
    this.updatePassword = new FormGroup({
      newPass: new FormControl("", [Validators.required, Validators.pattern(PASSWORD_REGEX), Validators.minLength(8), Validators.maxLength(40)]),
      confirmPass: new FormControl("", [matchOtherValidator("newPass")])
    });
    this.setConfig()
    setTimeout(() => loading(false), 100)
  }

  async setConfig() {
    this._activeRouter.params.subscribe(async params => {
      const _accountKey = params['account_key']
      if (_accountKey) {
        const _guesObject = JSON.stringify(getDefaultHMUser(null))
        const _date = moment(Date.now()).format().substring(0, 16)
        const encObjectL: AESModel = encryptStringAES({
          d1: _date, d2: _guesObject, d3: _accountKey
        })
        const response: AESModel = await this._authService.guestLoginService(encObjectL).toPromise() as any
        this.userData = JSON.parse(JSON.parse(decryptStringAES(response)).objectRights)
        const _currentData = moment(new Date())
        const _validityData = moment(this.userData.Validity)
        if (_validityData.isBefore(_currentData))
          this.isLinkExpred = true
        this.checkVerified()
      }
    })

  }

  checkVerified() {
    this._authService.getUserValidity([this.userData.IssuedByUserID]).subscribe((res: JsonResponse) => {
      if (res.returnId > 0) {
        if (res.returnObject.isVerified) {
          this.isVerified = res.returnObject.isVerified
          this._toastr.success('You are already verified, please login', 'User Verified')
        }
      } else {
        this._toastr.success(res.returnText, res.returnStatus)
      }
    }, () => {
      // this._router.navigate(['login'])
    })
  }

  getUIImage = ($image: string) =>
    getImagePath(ImageSource.FROM_SERVER, $image, ImageRequiredSize.original)

  passwordMatchValidator() {
    if (this.updatePassword !== undefined && this.updatePassword !== null) {
      return this.updatePassword.get("newPass").value ===
        this.updatePassword.get("confirmPass").value ? null : { mismatch: true }
    }
  }

  errorMessagesPasswordUpdate() {
    if (
      this.updatePassword.controls["newPass"].touched &&
      this.updatePassword.controls["newPass"].status === "INVALID"
    ) {
      this.isNewPass = true;
    }
    if (
      this.updatePassword.controls["confirmPass"].touched &&
      this.updatePassword.controls["confirmPass"].status === "INVALID"
    ) {
      this.confirmPass = true;
    }
  }

  _updatePassword() {
    const toSend = {
      userID: this.userData.IssuedByUserID,
      isVerified: true,
      password: this.updatePassword.controls["newPass"].value,
      confirmPassword: this.updatePassword.controls["confirmPass"].value
    }
    this.isUpdating = true
    this._authService.activateUser(toSend).subscribe((res: JsonResponse) => {
      this.isUpdating = false
      if (res.returnId > 0) {
        this.isVerifiedByForm = true
        this.updatePassword.reset()
        this.newPass = null
        this._toastr.success(res.returnText, res.returnStatus)
      } else {
        if (res.returnCode === '2') {
          this._toastr.info(res.returnText, res.returnStatus)
        } else {
          this._toastr.error(res.returnText, res.returnStatus)
        }
        this.checkVerified()
      }
    }, () => {
      this.isUpdating = false
      this._toastr.error('There was an error while processing your request, please try again later', 'Failed')
    })
  }

  resendEmailRequest() {
    loading(true)
    this._authService.resendEmail(this.userData.IssuedByUserID).subscribe((res: JsonResponse) => {
      loading(false)
      if (res.returnId > 0) {
        this._toastr.success(res.returnText, res.returnStatus)
        this.hasVeriLinkSent = true
      } else
        this._toastr.warning(res.returnText, res.returnStatus)
    }, () => {
      loading(false)
    })
  }

  onNewPassChange(_value) {
    console.log('new pass:', _value)
    this.updatePassword.controls['newPass'].setValue(_value)
    this.newPasswordStatus = validatePassword(_value)
    console.log(this.newPasswordStatus)
    this.showPassValidator = true
  }

  navigateLogin() {
    this._router.navigate(['login'])
  }

  ngOnDestroy() { }

}
