import { Routes, RouterModule } from '@angular/router'
import { ModuleWithProviders } from '@angular/core'
import { AccountActivationComponent } from './account-activation.component'

export const routes: Routes = [
    { path: '', component: AccountActivationComponent }
]
export const routing: ModuleWithProviders = RouterModule.forChild(routes)
