import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { routing } from './account-activation.route'
import { AccountActivationComponent } from './account-activation.component'
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { NguCarouselModule } from '@ngu/carousel'
import { SharedModule } from '../../../shared/shared.module'

@NgModule({
  imports: [
    CommonModule,
    NgbModule.forRoot(),
    routing,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NguCarouselModule,
    //FileDropModule
  ],
  declarations: [
    AccountActivationComponent,
  ],
  exports: [],
  providers: []
})

export class AccountActivationModule { }
