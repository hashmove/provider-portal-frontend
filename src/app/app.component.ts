import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { CommonService } from './services/common.service';
import { SharedService } from './services/shared.service';
import { ScrollbarComponent } from 'ngx-scrollbar';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import '../assets/scss/_loader.css';
import { VERSION } from '../environments/version'
import { SetupService } from './services/setup.injectable';
import { setDefaultIp } from './constants/globalFunctions';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { ToastrService } from 'ngx-toastr';
import browser from "browser-detect";
import { CurrencyControl } from './services/currency.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  @ViewChild(ScrollbarComponent) scrollRef: ScrollbarComponent;
  public static version = VERSION;

  constructor(
    private _commonService: CommonService,
    private _sharedService: SharedService,
    private _router: Router,
    private _setup: SetupService,
    private _toast: ToastrService,
    private _currencyControl: CurrencyControl
  ) { }

  async ngOnInit() {
    this._router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe(() => {
      this.scrollTop();
    });

    this.browserDetection();

    this._sharedService.mainScrollListener.pipe(untilDestroyed(this)).subscribe(state => {
      if (state) {
        setTimeout(() => {
          this.scrollTop()
        }, 0);
        setTimeout(() => {
          this._sharedService.mainScrollListener.next(false)
        }, 0);
      }
    })

    this.clearStorage()

    let countryList: any = null
    try {
      countryList = await this._commonService.getCountry().toPromise() as any
      // console.log(countryList)
      if (countryList && countryList.length) {
        countryList.map((obj) => {
          if (typeof (obj.desc) == "string") {
            obj.desc = JSON.parse(obj.desc);
          }
        })
        this._sharedService.countryList.next(countryList);
      }
    } catch (error) {
    }
    try {
      const _masterCurrency = {
        fromCurrencyCode: 'USD', fromCurrencyID: 101, rate: 1,
        toCurrencyCode: 'USD', toCurrencyID: 101, toCountryID: 101
      }
      this._currencyControl.setMasterCurrency(_masterCurrency)
      localStorage.setItem('CURR_MASTER', JSON.stringify(_masterCurrency))
    } catch { }

    this._commonService.getRegions().subscribe((res: any) => {
      if (res && res.length) {
        this._sharedService.regionList.next(res);
      }
    });
    const state = {
      countryCode: 'us',
      query: '0.0.0.0',
      country: 'United States'
    }
    try {
      this._sharedService.setMapLocation(state);
      setDefaultIp(state.query)
      this._setup.setBaseCurrencyConfig(this._sharedService.countryList.getValue(), this._sharedService.getMapLocation().countryCode)
    } catch (error) {

    }
  }

  scrollTop() {
    if (this.scrollRef) {
      setTimeout(() => {
        this.scrollRef.scrollYTo(0, 20);
      }, 0)
    }
  }

  clearStorage() {
    let currVersion = AppComponent.version.version;
    let oldVersion = localStorage.getItem('version');
    if (!oldVersion || oldVersion !== currVersion) {
      localStorage.clear();
      localStorage.setItem('version', AppComponent.version.version);
    }
  }


  browserDetection() {
    let result = browser();
    console.log(result);
    let browserClass = document.querySelector("body");

    if (result.name === "firefox") {
      this._toast.warning('We recommend using Chrome, Edge or Safari.', 'Unsupported Browser', { positionClass: 'toast-top-center', disableTimeOut: false, timeOut: 0 })
      browserClass.classList.add("Firefox");
    } else if (result.name === "chrome") {
      browserClass.classList.add("Chrome");
    } else if (result.name === "safari") {
      browserClass.classList.add("safari");
    } else if (result.name === "opera") {
      browserClass.classList.add("opera");
    } else {
      // do something
    }
  }

  ngOnDestroy(): void {
  }

}

