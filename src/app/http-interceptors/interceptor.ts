import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs/Rx';
import { Injectable, Inject, forwardRef } from '@angular/core';
import { Router } from '@angular/router';
import { flatMap } from 'rxjs/operators';
import 'rxjs/add/operator/do';
import { ToastrService } from 'ngx-toastr';
import { GuestService } from '../services/jwt.injectable';
import { UserCreationService } from '../components/pages/user-creation/user-creation.service';
import { SharedService } from '../services/shared.service';

@Injectable()
export class Interceptor implements HttpInterceptor {
  private refreshTokenInProgress = false;
  // Refresh Token Subject tracks the current token, or is null if no token is currently
  // available (e.g. refresh pending).
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(
    null
  );
  constructor(
    @Inject(forwardRef(() => UserCreationService)) public _auth: UserCreationService,
    @Inject(forwardRef(() => GuestService)) public _jwtService: GuestService,
    // @Inject(forwardRef(() => ToastrService)) public _router: Router,
    public _router: Router,
    public _toastr: ToastrService
    // @Inject(forwardRef(() => ToastrService)) public _toastr: ToastrService
  ) { }

  intercept(mainRequest: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let request: HttpRequest<any>


    if (
      mainRequest.url.toLowerCase().includes('ip-api') ||
      mainRequest.url.toLowerCase().includes("resetjwt") ||
      mainRequest.url.toLowerCase().includes("guestjwt") ||
      mainRequest.url.toLowerCase().includes("stjwt_o") ||
      mainRequest.url.toLowerCase().includes("userlogout") ||
      mainRequest.url.toLowerCase().includes("google") ||
      mainRequest.url.toLowerCase().includes('validate')) {

      request = mainRequest.clone()
      if (mainRequest.url.toLowerCase().includes('refresh')) {
        request = this.addEncodeURLHeader(request)
      }
    } else {
      request = this.addAuthenticationToken(mainRequest)
    }


    return next.handle(request).catch(error => {

      const token: string = this._jwtService.getJwtTokenForInterceptor()
      const refreshToken: string = this._jwtService.getRefreshTokenForInterceptor()

      // We don't want to refresh token for some requests like login or refresh token itself
      // So we verify url and we throw an error if it's the case
      if (
        request.url.toLowerCase().includes("validate") ||
        request.url.toLowerCase().includes("resetjwt") ||
        request.url.toLowerCase().includes("guestjwt") ||
        !this._jwtService.getJwtTokenForInterceptor()
      ) {
        if (request.url.toLowerCase().includes("resetjwt")) {
          setTimeout(() => {
            this._toastr.warning('Redirecting to login page', 'Session Expired');
          }, 0);
          try {
            localStorage.removeItem('isVirtualAirline')
          } catch { }
          this._jwtService.sessionRefresh(null).then((res) => {
            try {
              location.reload()
            } catch (error) {

            }
          })
        }
        return Observable.throw(error);
      }

      // If error status is different than 401 we want to skip refresh token
      // So we check that and throw the error if it's the case
      if (error.status !== 401) {
        return Observable.throw(error);
      }
      if (this.refreshTokenInProgress) {
        return this.refreshTokenSubject
          .filter(result => result !== null)
          .take(1)
          .switchMap(() => next.handle(this.addAuthenticationToken(request)));
      } else {
        this.refreshTokenInProgress = true;

        this.refreshTokenSubject.next(null);
        const refreshObj = {
          token,
          refreshToken
        }

        // Call auth.revalidateToken(this is an Observable that will be returned)
        return this._auth.revalidateToken(refreshObj).flatMap((tokenResp: any) => {
          if (isShareLogin()) {
            this._jwtService.removeShareTokens()
          } else {
            this._jwtService.removeTokens()
          }          //When the call to refreshToken completes we reset the refreshTokenInProgress to false
          // for the next time the token needs to be refreshed
          this.refreshTokenInProgress = false;
          this._jwtService.saveJwtTokenForInterceptor(tokenResp.token)
          this._jwtService.saveRefershTokenForInterceptor(tokenResp.refreshToken)
          this.refreshTokenSubject.next(tokenResp.token);
          return next.handle(this.addAuthenticationToken(mainRequest));
        })
      }
    })
  }

  addAuthenticationToken(request: HttpRequest<any>) {
    // Get access token from Local Storage
    const accessToken = this._jwtService.getJwtTokenForInterceptor();

    // If access token is null this means that user is not logged in
    // And we return the original request
    if (!accessToken) {
      return request;
    }

    // We clone the request, because the original request is immutable
    const { url } = request
    return request.clone({
      setHeaders: {
        Authorization: 'Bearer ' + this._jwtService.getJwtTokenForInterceptor(),
        "Source-Via": SharedService.GET_SOURCE_VIA(url)
      }
    });
  }

  addEncodeURLHeader(request: HttpRequest<any>) {
    const { url } = request
    return request.clone({
      setHeaders: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Source-Via": SharedService.GET_SOURCE_VIA(url)
      }
    });
  }
}


export function isShareLogin(): boolean {
  let status = false
  try {
    const _url = location.href
    if (_url.includes('tracking') || _url.includes('booking-detail')) {
      const urlToProcess = (_url.includes('booking-detail')) ? 'booking-detail' : 'tracking'
      if (_url.includes(urlToProcess)) {
        const bookingKeys = _url.split(urlToProcess)
        let d3key = bookingKeys[1].split('/')
        if (d3key.length > 2) {
          status = true
        }
      }
    }
  } catch (error) { }
  return status
}